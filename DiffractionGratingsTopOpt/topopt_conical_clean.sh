#!/bin/bash
# File : topopt_conical_clean.sh
# Clean the folder by deleting all the files created by the computation

rm *.msh 2> /dev/null
rm *.txt 2> /dev/null
rm *.npz 2> /dev/null
rm *.dat 2> /dev/null
rm *.pre 2> /dev/null
rm *.out 2> /dev/null
rm *.pdf 2> /dev/null
rm *.pos 2> /dev/null
rm *.png 2> /dev/null
rm *.jpg 2> /dev/null
rm *.mp4 2> /dev/null
rm -rf topopt_conical_wavelength* 2> /dev/null
rm -rf __pycache__/ 2> /dev/null
rm -rf res_conical/ 2> /dev/null
