import numpy as np
import matplotlib.pyplot as plt
import os
import multiprocessing
import tabs_material
from config_topopt_conical_data import *

'''
© Simon Ans
File : resolution_functions.py

Functions that settle the data and launch the resoutions.
'''

##########################################################
##             FUNCTIONS USING THE TERMINAL             ##
##                  Use Gmsh and GetDP                  ##
##########################################################

def generate_mesh():
    ''' Once the file param.dat is created, run this line to generate the corresponding mesh '''
    print("######            Meshing...         ######")
    os.system(gmsh_path + "gmsh topopt_conical.geo -v 0 -")
    print("######       Meshing completed.      ######\n")

    return(0)

## -------------------------------------------------------------------------------------------------------------

def make_computation_directories():
    ''' Prepare the directories to run the resolution for every wavelength
        Adapt the param.dat file'''

    # Initial wavelength
    initial_wavelength = target_wavelength[0]
    # Initial permittivities and permeabilities
    initial_n_super, initial_k_super, initial_epsilon_r_super = tabs_material.epsilon(material_superstrate, target_wavelength[0]*1e-9)
    initial_n_min  , initial_k_min  , initial_epsilon_r_min   = tabs_material.epsilon(material_min, target_wavelength[0]*1e-9)
    initial_n_max  , initial_k_max  , initial_epsilon_r_max   = tabs_material.epsilon(material_max, target_wavelength[0]*1e-9)
    initial_n_subs , initial_k_subs , initial_epsilon_r_subs  = tabs_material.epsilon(material_substrate, target_wavelength[0]*1e-9)

    for i_wavelength in range(nb_wavelengths):
        ## Make directory and copy the useful files
        os.system("mkdir topopt_conical_wavelength" + str(i_wavelength))
        os.system("cp -r param.dat topopt_conical.geo topopt_conical.msh topopt_conical.pro tabs_material.py topopt_conical_wavelength" + str(i_wavelength))
    
        ## Adapt the param.dat file
        os.chdir("topopt_conical_wavelength" + str(i_wavelength))

        # Wavelength and iteration
        current_wavelength  = target_wavelength[i_wavelength]
        os.system("sed -i 's/lambda_wavelength = %.15e;/lambda_wavelength = %.15e;/g' param.dat"%(initial_wavelength, current_wavelength))
        os.system("sed -i 's/i_lambda = 0;/i_lambda = %d;/g' param.dat"%i_wavelength)

        # Permittivities and permeabilities for every wavelength
        current_n_super, current_k_super, current_epsilon_r_super = tabs_material.epsilon(material_superstrate, target_wavelength[i_wavelength]*1e-9)
        current_n_min  , current_k_min  , current_epsilon_r_min   = tabs_material.epsilon(material_min, target_wavelength[i_wavelength]*1e-9)
        current_n_max  , current_k_max  , current_epsilon_r_max   = tabs_material.epsilon(material_max, target_wavelength[i_wavelength]*1e-9)
        current_n_subs , current_k_subs , current_epsilon_r_subs  = tabs_material.epsilon(material_substrate, target_wavelength[i_wavelength]*1e-9)

        os.system("sed -i 's/epsilon_r_super_re = %.15e;/epsilon_r_super_re = %.15e;/g' param.dat"%(np.real(initial_epsilon_r_super), np.real(current_epsilon_r_super)))
        os.system("sed -i 's/epsilon_r_super_im = %.15e;/epsilon_r_super_im = %.15e;/g' param.dat"%(np.imag(initial_epsilon_r_super), np.imag(current_epsilon_r_super)))
        os.system("sed -i 's/n_super = %.15e;/n_super = %.15e;/g' param.dat"%(initial_n_super, current_n_super))
        os.system("sed -i 's/k_super = %.15e;/k_super = %.15e;/g' param.dat"%(initial_k_super, current_k_super))
        os.system("sed -i 's/epsilon_r_min_re = %.15e;/epsilon_r_min_re = %.15e;/g' param.dat"%(np.real(initial_epsilon_r_min), np.real(current_epsilon_r_min)))
        os.system("sed -i 's/epsilon_r_min_im = %.15e;/epsilon_r_min_im = %.15e;/g' param.dat"%(np.imag(initial_epsilon_r_min), np.imag(current_epsilon_r_min)))
        os.system("sed -i 's/n_min = %.15e;/n_min = %.15e;/g' param.dat"%(initial_n_min, current_n_min))
        os.system("sed -i 's/k_min = %.15e;/k_min = %.15e;/g' param.dat"%(initial_k_min, current_k_min))
        os.system("sed -i 's/epsilon_r_max_re = %.15e;/epsilon_r_max_re = %.15e;/g' param.dat"%(np.real(initial_epsilon_r_max), np.real(current_epsilon_r_max)))
        os.system("sed -i 's/epsilon_r_max_im = %.15e;/epsilon_r_max_im = %.15e;/g' param.dat"%(np.imag(initial_epsilon_r_max), np.imag(current_epsilon_r_max)))
        os.system("sed -i 's/n_max = %.15e;/n_max = %.15e;/g' param.dat"%(initial_n_max, current_n_max))
        os.system("sed -i 's/k_max = %.15e;/k_max = %.15e;/g' param.dat"%(initial_k_max, current_k_max))
        os.system("sed -i 's/epsilon_r_subs_re = %.15e;/epsilon_r_subs_re = %.15e;/g' param.dat"%(np.real(initial_epsilon_r_subs), np.real(current_epsilon_r_subs)))
        os.system("sed -i 's/epsilon_r_subs_im = %.15e;/epsilon_r_subs_im = %.15e;/g' param.dat"%(np.imag(initial_epsilon_r_subs), np.imag(current_epsilon_r_subs)))
        os.system("sed -i 's/n_subs = %.15e;/n_subs = %.15e;/g' param.dat"%(initial_n_subs, current_n_subs))
        os.system("sed -i 's/k_subs = %.15e;/k_subs = %.15e;/g' param.dat"%(initial_k_subs, current_k_subs))
        os.chdir("..")

    return(0)

## -------------------------------------------------------------------------------------------------------------

def solve(i_wavelength):
    ''' Launch GetDP to solve the problem on the mesh generated with the current data file '''

    current_wavelength = target_wavelength[i_wavelength]
    # Move into the corresponding directory
    os.chdir("topopt_conical_wavelength" + str(i_wavelength))

    os.system(GetDP_path + "getdp topopt_conical.pro -pre helmholtz_conical_direct_inverse -msh topopt_conical.msh -cal -petsc_prealloc 200 -setnumber lambda0 " + str(current_wavelength * nm) + " -v 1")

    # Move again into the main directory
    os.chdir("..")

    return(0)

##########################################################
##                 TOOLS FOR THE DATA                   ##
##            Functions to reduce set_data              ##
##########################################################

def check_quantites():
    ''' In the set_data program, check if the quantities are coherent '''

    if h_design > h_super:
        raise ValueError("h_design > h_super, please increase h_super.")
    if ((theta_i > 90) or (theta_i < 0)):
        raise ValueError("The angle of incidence out of the plane is not in the correct interval (0° < theta_i < 90°)")
    if ((phi_i > 180) or (phi_i < -180)):
        raise ValueError("The angle of incidence in the plane is not in the correct interval (-180° < phi_i < 180°)")
    if ((psi_i > 180) or (psi_i < -180)):
        raise ValueError("The angle of polarization is not in the correct interval (-180° < psi_i < 180°)")
    if multiprocessing.cpu_count() < num_threads_multilambda * num_threads_resolution :
        raise ValueError("The number of CPUs of the current machine is too low regarding the number of threads allocated (for the multi-wavelength and the Finite Element resolutions).")
    if paramaille < 10:
        print("Warning : The number of mesh elements per wavelength is quite low (minimum should be 10). The result might not be precise enough.")
        wait = input("Do you want to continue anyway ? (Press Enter if yes)")

    return(0)

## -------------------------------------------------------------------------------------------------------------

def write_param(integer_parameters_name, integer_parameters_value, float_parameters_name, float_parameters_value):
    ''' Write in a file called param.dat all the data contained in parameters '''

    param = open("param.dat","w")

    length_integers = len(integer_parameters_name)
    length_float    = len(float_parameters_name)

    for i_integers in range (length_integers):
        param.write("%s = %d;\n"%(integer_parameters_name[i_integers], integer_parameters_value[i_integers]))
    
    for i_float in range (length_float):
        param.write("%s = %.15e;\n"%(float_parameters_name[i_float], float_parameters_value[i_float]))

    param.close()

    return(0)

##########################################################
##                   MAIN FUNCTIONS                     ##
##       Parameters and resolution of the problem       ##
##########################################################

def set_data():
    ''' Input the data into a file to create the periodic 2D geometry with its physical parameters
        It is read by the .geo and .pro files '''    

    ## Check if the quantities are coherent  -------------------------------------------------------------------

    check_quantites()

    ## Permittivities ------------------------------------------------------------------------------------------
    n_super, k_super, epsilon_r_super = tabs_material.epsilon(material_superstrate, target_wavelength[0]*1e-9)
    n_min, k_min, epsilon_r_min = tabs_material.epsilon(material_min, target_wavelength[0]*1e-9)
    n_max, k_max, epsilon_r_max = tabs_material.epsilon(material_max, target_wavelength[0]*1e-9)
    n_subs, k_subs, epsilon_r_subs    = tabs_material.epsilon(material_substrate, target_wavelength[0]*1e-9)

    epsilon_r_super_re = epsilon_r_super.real
    epsilon_r_super_im = epsilon_r_super.imag
    mu_r_super_re = 1
    mu_r_super_im = 0
    epsilon_r_min_re = epsilon_r_min.real
    epsilon_r_min_im = epsilon_r_min.imag
    epsilon_r_max_re = epsilon_r_max.real
    epsilon_r_max_im = epsilon_r_max.imag
    mu_r_voxel_re = 1
    mu_r_voxel_im = 0
    epsilon_r_subs_re = epsilon_r_subs.real
    epsilon_r_subs_im = epsilon_r_subs.imag
    mu_r_subs_re = 1
    mu_r_subs_im = 0    

    ## Resize the quantities -----------------------------------------------------------------------------------

    Period  = nm * period
    hpmlsup = nm * h_PML_sup
    hsuper  = nm * h_super
    hdesign = nm * h_design
    hsubs   = nm * h_subs
    hpmlinf = nm * h_PML_inf

    ## Write in the file ---------------------------------------------------------------------------------------

    # list of the names of the integer parameters to write the param.dat file
    integer_parameters_name = ["i_lambda", "target_order", "nb_orders", "Flag_o2_geom", "Flag_o2_interp", "Flag_pml_inf", "Flag_design_metal", "PML_SUP", "SUPERSTRATE", "SUBSTRATE", "PML_INF", "SURF_LEFT", "SURF_RIGHT", "SURF_UP", "SURF_DOWN", "SURF_INTEG_SUP", "SURF_INTEG_SUB", "SURF_PLOT", "PRINT_POINT", "DESIGN"]

    # list of the values of the integers parameters to write the param.dat file
    integer_parameters_value = [0, target_order, nb_orders, Flag_o2_geom, Flag_o2_interp, Flag_pml_inf, Flag_design_metal, PML_SUP,  SUPERSTRATE, SUBSTRATE, PML_INF, SURF_LEFT, SURF_RIGHT, SURF_UP, SURF_DOWN, SURF_INTEG_SUP, SURF_INTEG_SUB, SURF_PLOT, PRINT_POINT, DESIGN]

    # list of the names of the float parameters to write the param.dat file
    float_parameters_name = ["lambda_wavelength", "nm", "ep0", "mu0", "cel", "deg2rad", "period", "h_PML_sup", "h_super", "h_design", "h_subs", "h_PML_inf", "paramaille", "paramaille_pmlSup", "paramaille_superstrate", "paramaille_design", "paramaille_substrate", "paramaille_pmlInf", "theta_incidence", "phi_incidence", "psi_incidence", "A"]
    float_parameters_name += ["epsilon_r_super_re", "epsilon_r_super_im", "n_super", "k_super", "mu_r_super_re", "mu_r_super_im", "epsilon_r_min_re", "epsilon_r_min_im", "n_min", "k_min", "epsilon_r_max_re", "epsilon_r_max_im", "n_max", "k_max", "mu_r_voxel_re", "mu_r_voxel_im", "epsilon_r_subs_re", "epsilon_r_subs_im", "n_subs", "k_subs", "mu_r_subs_re", "mu_r_subs_im"]
    
    # list of the values of the float parameters to write the param.dat file
    float_parameters_value = [target_wavelength[0], nm, ep0, mu0, cel, deg2rad, Period, hpmlsup, hsuper, hdesign, hsubs, hpmlinf, paramaille, paramaille_pmlSup, paramaille_superstrate, paramaille_design, paramaille_substrate, paramaille_pmlInf, theta_i, phi_i, psi_i, amplitude]
    float_parameters_value += [epsilon_r_super_re, epsilon_r_super_im, n_super, k_super, mu_r_super_re, mu_r_super_im, epsilon_r_min_re, epsilon_r_min_im, n_min, k_min, epsilon_r_max_re, epsilon_r_max_im, n_max, k_max, mu_r_voxel_re, mu_r_voxel_im, epsilon_r_subs_re, epsilon_r_subs_im, n_subs, k_subs, mu_r_subs_re, mu_r_subs_im]

    write_param(integer_parameters_name, integer_parameters_value, float_parameters_name, float_parameters_value)

    return(0)

## -------------------------------------------------------------------------------------------------------------

def print_sum_energies():
    ''' Check the sum of the efficiencies and losses (must be 1 is nb_orders is high) '''

    sum_efficiency_r = np.zeros(nb_wavelengths)
    sum_efficiency_t = np.zeros_like(sum_efficiency_r)
    losses_tot       = np.zeros_like(sum_efficiency_r)
    sum_energies     = np.zeros_like(sum_efficiency_r)
    for i_wavelength in range (nb_wavelengths):
        for i_order in range (2*nb_orders+1):
            sum_efficiency_r[i_wavelength] += np.loadtxt(res_folder_name + "efficiency_r_%d_%d.txt"%(i_order-nb_orders,i_wavelength))[1]
            sum_efficiency_t[i_wavelength] += np.loadtxt(res_folder_name + "efficiency_t_%d_%d.txt"%(i_order-nb_orders,i_wavelength))[1]
        losses_tot[i_wavelength] = np.loadtxt(res_folder_name + "absorption-Q_tot_%d.txt"%i_wavelength)[1]
        sum_energies[i_wavelength] = sum_efficiency_r[i_wavelength] + sum_efficiency_t[i_wavelength] + losses_tot[i_wavelength]

    print("Sum of the efficiencies and losses for the wavelengths of target_wavelength :")
    print(sum_energies)

    return(0)