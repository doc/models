import numpy as np
import matplotlib.pyplot as plt
import meshio as mio
import os
import re
import time
from joblib import Parallel, delayed
from sklearn.metrics.pairwise import euclidean_distances
from config_topopt_conical_data import *
import resolution_functions
import tabs_material

'''
© Simon Ans
File : optimization_functions.py
'''

##########################################################
##                   DENSITY FUNCTIONS                  ##
##   Settle and change the density field on the mesh    ##
##########################################################

def init_density(templatefile, outfile, initfunc):
    ''' Initialize a density field on the mesh with an analytic function '''
    
    print("###### Initializing template file... ######")

    # Solve a P^0 problem in order to spot the triangles of the mesh
    os.system(GetDP_path + 'getdp topopt_conical.pro -pre res_init_densities -msh topopt_conical.msh -cal -v 1')

    # Prepare the files that read and store the positions of the triangles
    f_in  = open(templatefile, 'r')
    f_out = open(outfile, 'w')
    lines_in = f_in.readlines()
    f_out.write(lines_in[0])
    vals = []

    # Store the value of the density inside each triangle of the mesh (detected through its barycentre)
    for line in lines_in[1:-1]:
        bary = np.mean(np.array(re.search(r'\((.*?)\)',line).group(1).split(','),float).reshape(3,3),axis=0)
        val = initfunc(bary[0],bary[1])
        f_out.write(re.sub('{.*?}','{%.10f,%.10f,%.10f}'%(val,val,val),line))
        vals.append(val)

    f_out.write(lines_in[-1])
    f_in.close()
    f_out.close()

    print('######  Template file initialized.   ######\n')

    return(np.array(vals, dtype=float))

## -------------------------------------------------------------------------------------------------------------

def get_barycenters():
    ''' Get the barycenter matrix of the triangles of the mesh topopt_conical.msh '''

    msh = mio.read(mesh_file)
    res=[]
    for k in range(len(msh.cells_dict['triangle'])):
        t = msh.cells_dict['triangle'][k]
        a,b,c = msh.points[t[0]],msh.points[t[1]],msh.points[t[2]]
        res.append(np.mean(np.array([a,b,c]),axis=0).tolist())
    
    return np.array(res)

## -------------------------------------------------------------------------------------------------------------

def update_density(templatefile, outfile, tab):
    ''' Update the density field with the current density in the optimization process '''

    # Same as init_density
    f_in  = open(templatefile, 'r')
    f_out = open(outfile, 'w')
    lines_in = f_in.readlines()
    f_out.write(lines_in[0])
        
    # Update (read the table of the template mesh and put the values of the current densities)
    counter = 0
    for line in lines_in[1:-1]:
        template_density = tab[counter]
        f_out.write(re.sub('{.*?}','{%.10f,%.10f,%.10f}'%(template_density,template_density,template_density),line))
        counter += 1
    f_out.write(lines_in[-1])
    f_in.close()
    f_out.close()

    return(0)

## -------------------------------------------------------------------------------------------------------------

def read_real_posfile(posfile):
    ''' Read a complex .pos file to make an array '''

    # Same as init_density
    f_in  = open(posfile, 'r')
    lines_in = f_in.readlines()
    real_table = []

    # Read the accurate inputs of the .pos file
    for line in lines_in[1:-1]:
        current_line = np.array(re.search(r'\{(.*?)\}',line).group(1).split(','),float)
        real_table.append(current_line[0])
    f_in.close()

    real_table = np.array(np.real(real_table), dtype=float)
    
    return(real_table)

## -------------------------------------------------------------------------------------------------------------

def read_complex_posfile(posfile):
    ''' Read a complex .pos file to make an array '''

    # Same as init_density
    f_in  = open(posfile, 'r')
    lines_in = f_in.readlines()
    complex_table = []

    # Read the accurate inputs of the .pos file
    for line in lines_in[1:-2]:
        current_line = np.array(re.search(r'\{(.*?)\}',line).group(1).split(','),float)
        complex_table.append(current_line[0]+1j*current_line[3])
    f_in.close()

    complex_table = np.array(np.real(complex_table), dtype=float) + 1j*np.array(np.imag(complex_table), dtype=float)
    
    return(complex_table)

##########################################################
##                CONSTRAINTS FUNCTIONS                 ##
##   Apply the diffusion and/or binarization filters    ##
##########################################################

def diffusion_filter(density):
    ''' Filter of the mesh to avoid the isolated points of density '''

    midpoint = get_barycenters()
    distance_temp = filter_radius*nm - euclidean_distances(midpoint, midpoint)
    distance_temp[distance_temp < 0] = 0
    distance_mat = distance_temp.copy()
    distance_sum = distance_mat.sum(1)
    rho_diffusion_Filtered = np.divide(distance_mat @ (density.flatten()), distance_sum)
    return rho_diffusion_Filtered

## -------------------------------------------------------------------------------------------------------------

def diffusion_filter_jacobian():
    ''' Jacobian of the diffusion filter '''

    midpoint = get_barycenters()
    distance_temp = filter_radius*nm - euclidean_distances(midpoint, midpoint)
    distance_temp[distance_temp < 0] = 0
    distance_mat = distance_temp.copy()
    distance_sum = distance_mat.sum(1)
    diffusion_jacobian = np.divide(distance_mat, distance_sum)
    return diffusion_jacobian

## -------------------------------------------------------------------------------------------------------------

def binarize_filter(density, iter_bin):
    ''' Binarize the mesh with a degree given by iter_bin '''

    beta_f = 2**iter_bin
    return ( (np.tanh(beta_f*nu) + np.tanh(beta_f*(density - nu))) / (np.tanh(beta_f*nu) + np.tanh(beta_f*(1 - nu))) )

## -------------------------------------------------------------------------------------------------------------

def binarize_filter_jacobian(density, iter_bin):
    ''' Jacobian of the binarization filter '''

    beta_f = 2**iter_bin
    return np.diag( (beta_f * (1 - np.tanh(beta_f*(density-nu))**2)) / (np.tanh(beta_f*nu) + np.tanh(beta_f*(1 - nu))) )

## -------------------------------------------------------------------------------------------------------------

def composition_filters(density, iter_bin):
    ''' Compose the filters previously defined depending on the flags '''

    raw_density = density.copy()
    if ((Flag_diffusion == 0) and (Flag_binarize == 0)): return raw_density
    if ((Flag_diffusion == 1) and (Flag_binarize == 0)): return diffusion_filter(raw_density)
    if ((Flag_diffusion == 0) and (Flag_binarize == 1)): return binarize_filter(raw_density, iter_bin)
    if ((Flag_diffusion == 1) and (Flag_binarize == 1)): return binarize_filter(diffusion_filter(raw_density), iter_bin)

## -------------------------------------------------------------------------------------------------------------

def composition_filters_jacobian(density, iter_bin):
    ''' Compute the Jacobian matrix of the composition of the filters depending on the flags '''

    raw_density = density.copy()
    problem_size = np.size(raw_density)
    if ((Flag_diffusion == 0) and (Flag_binarize == 0)): return np.identity(problem_size)
    if ((Flag_diffusion == 1) and (Flag_binarize == 0)): return diffusion_filter_jacobian()
    if ((Flag_diffusion == 0) and (Flag_binarize == 1)): return binarize_filter_jacobian(raw_density, iter_bin)
    if ((Flag_diffusion == 1) and (Flag_binarize == 1)): return np.matmul( diffusion_filter_jacobian(), binarize_filter_jacobian(diffusion_filter(raw_density), iter_bin) )

## -------------------------------------------------------------------------------------------------------------

def final_binarization(templatefile, nonbin_posfile):
    ''' After the optimization, a final binarization to counter the last diffusion filter applied '''

    rho_opt_nonbin = read_real_posfile(nonbin_posfile)
    rho_opt_bin = binarize_filter(rho_opt_nonbin, nb_binarizations)
    np.savetxt("rho_opt.txt", rho_opt_bin)
    update_density(templatefile, "rho_opt.pos", rho_opt_bin)
    last_iter = (nb_binarizations+1)*maxiter+1
    plot_msh_density(rho_opt_bin, "myimage_%03d.png"%last_iter)

    return(0)

##########################################################
##                OPTIMIZATION FUNCTIONS                ##
##     Main functions for the topology optimization     ##
##########################################################

def compute_target_and_jacobian(i_wavelength):
    ''' Calculate the Jacobian of the target using the complex amplitudes and their derivatives '''

    wavelength = target_wavelength[i_wavelength]

    # Components of the wave vectors (same reasoning as in the .pro file)
    lambda0 = wavelength*nm
    k0      = 2*np.pi / lambda0
    n_super, k_super, epsilon_r_super = np.real(tabs_material.epsilon(material_superstrate, wavelength*1e-9))
    deg2rad = np.pi/180

    alpha      = -k0*n_super*np.sin(theta_i*deg2rad)*np.sin(phi_i*deg2rad)
    beta_super = -k0*n_super*np.cos(theta_i*deg2rad)
    gamma      = -k0*n_super*np.sin(theta_i*deg2rad)*np.cos(phi_i*deg2rad)

    alpha_n      = alpha + 2*np.pi/(period*nm) * wanted_order
    beta_n_super = -np.sqrt(epsilon_r_super*k0**2 - alpha_n**2 - gamma**2)

    # Read the result files in order to get the complex amplitudes and their derivatives
    comp_amp_x_tab = np.loadtxt(res_folder_name + "comp_amp_x_r_%d_%d.txt"%(wanted_order,i_wavelength))
    comp_amp_z_tab = np.loadtxt(res_folder_name + "comp_amp_z_r_%d_%d.txt"%(wanted_order,i_wavelength))
    comp_amp_x     = comp_amp_x_tab[1] + 1j*comp_amp_x_tab[2]
    comp_amp_z     = comp_amp_z_tab[1] + 1j*comp_amp_z_tab[2]

    comp_amp_x_derivative = read_complex_posfile(res_folder_name + "comp_amp_deriv_x_r_project_%d.pos"%i_wavelength)
    comp_amp_z_derivative = read_complex_posfile(res_folder_name + "comp_amp_deriv_z_r_project_%d.pos"%i_wavelength)

    target_jacobian = -2/(beta_n_super*beta_super*amplitude**2) * np.real((beta_n_super**2 + alpha_n**2) * comp_amp_x_derivative*np.conjugate(comp_amp_x)
                                                                        + (beta_n_super**2 + gamma**2)   * comp_amp_z_derivative*np.conjugate(comp_amp_z)
                                                                        + alpha_n*gamma * (comp_amp_x_derivative*np.conjugate(comp_amp_z) + comp_amp_x*np.conjugate(comp_amp_z_derivative)))

    return(np.array(target_jacobian, dtype=float))

## -------------------------------------------------------------------------------------------------------------

def get_sum_targetJacobian(target_jacobian):
    ''' Solve the direct and adjoint problems for each wavelength and add them together
        in order to get the wanted efficiencies and their Jacobian '''

    # Enable each wavelength to access the current density
    for i_wavelength in range(nb_wavelengths):
        os.system("cp -r rho_opt.pos topopt_conical_wavelength" + str(i_wavelength))

    # Run the direct and adjoint problem to compute the target and its Jacobian 
    temp_target_jacobian = np.zeros(np.size(target_jacobian))
    temp_target   = 0
    Parallel(n_jobs=num_threads_multilambda)(delayed(resolution_functions.solve)(i_wavelength) for i_wavelength in range(nb_wavelengths))
    for i_wavelength in range(nb_wavelengths) :
        temp_target_jacobian += compute_target_and_jacobian(i_wavelength)
        temp_target   += np.loadtxt(res_folder_name + "efficiency_r_%d_%d.txt"%(wanted_order,i_wavelength))[1]

    return temp_target, temp_target_jacobian

## -------------------------------------------------------------------------------------------------------------

def target(input_density, target_jacobian):
    ''' Definition of the target function and its jacobian for the optimization problem
        The fact that 'target_jacobian' is an input is a requirement of the NLopt class '''

    iter_bin = int(np.loadtxt("iter_bin.txt"))

    # Filter the densities depending on the filtering flags
    updated_density  = composition_filters(input_density, iter_bin)
    # Compute the Jacobian of this filter
    filters_jacobian = composition_filters_jacobian(input_density, iter_bin)

    # Store the filtered density file for the solver
    update_density(res_folder_name + "template_density_table.pos", "rho_opt.pos", updated_density)

    if (Flag_test == 0):
        # Display the updated densities on the pattern
        display_density(updated_density)
    # Access the efficiencies and their Jacobian for each wavelength, and add them together
    temp_target, temp_target_jacobian = get_sum_targetJacobian(target_jacobian)

    # Update the Jacobian
    if (np.size(target_jacobian) > 0):
        target_jacobian[:] = 1/nb_wavelengths * np.matmul(filters_jacobian, temp_target_jacobian)
        np.savetxt("jacobian.txt", target_jacobian)

    # Update the target
    target = 1 - temp_target/nb_wavelengths

    return(target)

## -------------------------------------------------------------------------------------------------------------

def feedback_result(min_value, successful_termination):
    ''' Displays the termination of the optimization with sentences '''

    if (successful_termination > 0):
        print("######              Optimization ended successfully.          ######\n")
        if (successful_termination == 3):
            print("The tolerance on the target function is reached.")
        elif (successful_termination == 5):
            print("The maximal number of iterations is reached.")
        print("Reflection efficiency : %.6f"%(1-min_value))
    else :
        print("######          Issue encountered during optimization.        ######\n")
        print(("Value of the termination : %d (see the nlopt reference https://nlopt.readthedocs.io/en/latest/ for details).\n"%successful_termination))
    
    return(0)

##########################################################
##                   TEST FUNCTIONS                     ##
##    Comparison adjoint / finite element derivatives   ##
##########################################################

def test_jacobian(rho_init, iter_bin):
    ''' Compare the derivatives computed with the adjoint problem and with the finite differences for the target function
        * The first one is given by the Jacobian computed by target(rho_init, jacobian)
        * The second one is the result of (target(rho_init,...) - target(rho_init + h_0,...)) / h_0 on each triangle of the mesh '''

    print("######  Test of the adjoint method   ######\n")
    plot_msh_density(rho_init, "rho_init.pdf")
    np.savetxt("iter_bin.txt", np.array([iter_bin]))
    problem_size = np.size(rho_init)

    jacobian = np.zeros(problem_size)
    target_rho_plus_h0 = np.zeros_like(jacobian)

    # Estimation of the test duration ---------------------------------------------
    t1 = time.time()

    # Solve the direct and adjoint problems in order to access the target with the initial densities
    # and the Jacobian computed by the adjoint method
    target_rho = target(rho_init, jacobian)
    numerical_jacobian_adjoint = np.loadtxt("jacobian.txt")

    t2 = time.time()
    print("## Finite differences computation : beginning the loop on %d triangles."%problem_size)
    print("## Estimated remaining time : %dmin%02ds"%((t2-t1)*problem_size//60, (t2-t1)*problem_size%60))
    # -----------------------------------------------------------------------------
    
    # Loop on the triangles to compute the finite differences for each one    
    for i_triangle in range(problem_size):
        rho_plus_h0 = rho_init.copy()
        rho_plus_h0[i_triangle] += h_0

        # Compute the target with the slightly different densities
        target_rho_plus_h0[i_triangle] = target(rho_plus_h0, jacobian)

    # Final computation of the finite differences
    numerical_jacobian_finite_diff = (target_rho_plus_h0 - target_rho) / h_0
    # Relative difference between both methods
    diff_rel = (numerical_jacobian_finite_diff - numerical_jacobian_adjoint) / np.mean(np.abs(numerical_jacobian_finite_diff))

    # Save the results in text files
    np.savetxt("adjoint.txt", numerical_jacobian_adjoint)
    np.savetxt("finite_diff.txt", numerical_jacobian_finite_diff)
    np.savetxt("diff_rel.txt", diff_rel)

    # Plot the results on the pattern
    plot_msh_density(numerical_jacobian_adjoint, "adjoint_jacobian_Target_n.pdf")
    plot_msh_density(numerical_jacobian_finite_diff, "finite_diff_jacobian_Target_n.pdf")
    plot_msh_density(np.abs(diff_rel), "diff_jacobian_Target_n.pdf")

    print("######         End of the test       ######\n")

    return(0)

## -------------------------------------------------------------------------------------------------------------

def test_jacobian_soloPoint(rho_init, iter_bin, x_point, y_point):
    ''' Compare the derivatives computed with the adjoint problem and with the finite differences for the target function,
        but only for one specific point '''

    print("######  Test of the adjoint method   ######\n")
    plot_msh_density(rho_init, "rho_init.pdf")
    np.savetxt("iter_bin.txt", np.array([iter_bin]))
    problem_size = np.size(rho_init)
    jacobian = np.zeros(problem_size)

    # Spot the closest triangle to the point (x_point, y_point)
    midpoint = get_barycenters()/nm
    closest_point = np.argmin( np.sqrt((x_point-midpoint[:,0])**2 + (y_point-midpoint[:,1])**2) )

    # Solve the direct and adjoint problems in order to access the target with the initial densities
    # and the Jacobian computed by the adjoint method
    target_rho = target(rho_init, jacobian)
    numerical_jacobian_adjoint = np.loadtxt("jacobian.txt")[closest_point]

    # Compute the target on the triangle closest_point
    rho_plus_h0 = rho_init.copy()
    rho_plus_h0[closest_point] += h_0
    target_rho_plus_h0 = target(rho_plus_h0, jacobian)

    # Final computation of the finite differences
    numerical_jacobian_finite_diff = (target_rho_plus_h0 - target_rho) / h_0
    # Relative difference between both methods
    diff_rel = (numerical_jacobian_finite_diff - numerical_jacobian_adjoint) / np.abs(numerical_jacobian_finite_diff)
    print("######         End of the test       ######\n")
    print("Jacobian computed : * with the adjoint method     on the point (%.0f,%.0f) : %.5f"%(x_point,y_point,numerical_jacobian_adjoint))
    print("                    * with the finite differences on the point (%.0f,%.0f) : %.5f"%(x_point,y_point,numerical_jacobian_finite_diff))
    print("Relative differences between both Jacobian methods : %.5f"%diff_rel)
    
    return(0)