/*
© Simon Ans
File : topopt_conical.geo

Construction of the geometry of the periodical mesh-based density grating using
the data in param.dat. The geometry is detailed in resolution_functions.set_data
*/

Include "param.dat";
SetFactory("OpenCASCADE");

ref_paramaille = 400 * nm / paramaille;
lc_pmlSup      = ref_paramaille / paramaille_pmlSup;
lc_superstrate = ref_paramaille / paramaille_superstrate;
lc_slide       = ref_paramaille / paramaille_design;
lc_substrate   = ref_paramaille / paramaille_substrate;
lc_pmlInf      = ref_paramaille / paramaille_pmlInf;

geometry_sup = h_super + h_PML_sup;
geometry_inf = h_subs  + h_PML_inf;

// Box
Rectangle(1) = {-period/2, h_super      , 0, period, h_PML_sup};
Rectangle(2) = {-period/2, h_design     , 0, period, h_super - h_design};
Rectangle(3) = {-period/2, 0            , 0, period, h_design};
Rectangle(4) = {-period/2, -h_subs      , 0, period, h_subs};
Rectangle(5) = {-period/2, -geometry_inf, 0, period, h_PML_inf};

Coherence;

Lines_Left()    = Line In BoundingBox{-period/2 - 1, -geometry_inf - 1, -1, -period/2 + 1,  geometry_sup + 1, 1};
Lines_Right()   = Line In BoundingBox{ period/2 - 1, -geometry_inf - 1, -1,  period/2 + 1,  geometry_sup + 1, 1};
Lines_Up()      = Line In BoundingBox{-period/2 - 1,  geometry_sup - 1, -1,  period/2 + 1,  geometry_sup + 1, 1};
Lines_Down()    = Line In BoundingBox{-period/2 - 1, -geometry_inf - 1, -1,  period/2 + 1, -geometry_inf + 1, 1};
Lines_Super()   = Line In BoundingBox{-period/2 - 1,  h_super      - 1, -1,  period/2 + 1,  h_super      + 1, 1};
Lines_Sub()     = Line In BoundingBox{-period/2 - 1, -h_subs       - 1, -1,  period/2 + 1, -h_subs       + 1, 1};

phys_plot_bnd()  = Lines_Super();
phys_plot_bnd() += Line In BoundingBox{-period/2 - 1, h_design - 1, -1,  period/2 + 1, h_design + 1, 1};
phys_plot_bnd() += Line In BoundingBox{-period/2 - 1,         - 1, -1,  period/2 + 1,         + 1, 1};
phys_plot_bnd() += Lines_Sub() ;

Physical Line(SURF_LEFT)  = Lines_Left();
Physical Line(SURF_RIGHT) = Lines_Right();
Physical Line(SURF_UP)    = Lines_Up();
Physical Line(SURF_DOWN)  = Lines_Down();

Physical Line(SURF_INTEG_SUP) = Lines_Super(); // superstrate/pml cut
Physical Line(SURF_INTEG_SUB) = Lines_Sub();   // substrate/pml cut
Physical Line(SURF_PLOT) = phys_plot_bnd[];    // final plot

Physical Surface(PML_SUP)     = {1};
Physical Surface(SUPERSTRATE) = {2};
Physical Surface(DESIGN)      = {3};
Physical Surface(SUBSTRATE)   = {4};
Physical Surface(PML_INF)     = {5};

Characteristic Length{PointsOf{Physical Surface{PML_SUP};}}     = lc_pmlSup;
Characteristic Length{PointsOf{Physical Surface{PML_INF};}}     = lc_pmlInf;
Characteristic Length{PointsOf{Physical Surface{SUPERSTRATE};}} = lc_superstrate;
Characteristic Length{PointsOf{Physical Surface{SUBSTRATE};}}   = lc_substrate;
Characteristic Length{PointsOf{Physical Surface{DESIGN};}}      = lc_slide;

Periodic Line {Lines_Left()} = {Lines_Right()} Translate {period,0,0};

Mesh 2;
Save "topopt_conical.msh";

Delete Physicals;
Physical Surface(DESIGN) = {3};
Save "design_region.msh"; 