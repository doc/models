import numpy as np
import matplotlib.pyplot as plt
import meshio as mio
import random

'''
© Simon Ans
File : config_topopt_conical_main.py

Gathers all the parameters of the simulation.
        ____________________________________________________
       | /!\ The reference unity for distances is nanometer |
       ------------------------------------------------------
'''

## Paths to the softwares and documents -----------------------------------------------------------------------------------------------

res_folder_name = "res_conical/"
gmsh_path = ""
GetDP_path = "~/programs/getdp/build/"
mesh_file = "design_region.msh"

## Global constants -------------------------------------------------------------------------------------------------------------------

nm           = 1000      # resizing factor for GetDP
ep0          = 8.854187817e-3 * nm
mu0          = 400 * np.pi * nm
cel          = 1 / (np.sqrt(ep0 * mu0))
deg2rad      = np.pi/180

## Parameters of the simulation -------------------------------------------------------------------------------------------------------

## Dimensions ##
period    = 3300
h_PML_sup = 700          # superior PML thickness
h_super   = 1000         # superstrate thickness
h_subs    = 200          # substrate thickness
h_PML_inf = 200          # inferior PML thickness

h_design = 650           # thickness of the design space

## Incident field ##
target_wavelength = [700]
#target_wavelength = [400,425,450,475,500,525,550,575,600,650,700,750,800,850,900,950,1000,1050,1100,1150,1200,1300,1400,1500]
nb_wavelengths    = len(target_wavelength)
amplitude         = 1
# Incidence angles (in degrees)
theta_i           = 5
phi_i             = -66
psi_i             = 90
# phi = -90 and psi = 90  : 2D case with E// polarization
#               psi = 180 : 2D case with H// polarization

nb_orders    = 1
wanted_order = -1                       # (integer) diffraction order on which the optimization is carried out
target_order = nb_orders + wanted_order # (>= 0 integer) shifting of the wanted order for numerical purpose

## Materials ##
material_superstrate = 'air'        #'silicon_dioxide'  #'niobium_pentoxide'  #'PMMA'  #'silver_IEF' #silver_widerange  #'silver_Johnson_Christy'
                                    #'ITO_Refractive'  #'BK7_schott' #'ITO_GT'  #'gold_ief_V2'  #'titanium'  #'titanium_dioxyde'
                                    #'fused_silica'  #'silicon_nitride' #'silicon_internet2'  #'silver_palik'  #'copper_palik'
                                    #'alu_palik'  #'gold_johnson' #'gold_olmon_evap'  #'chromium_BBM'  #'solar_etr'  #'solar_global_tilt'
                                    #'solar_direct_circumsolar' #'silicon_salzberg'  #'silicon_odp0'  #'silicon'
                                    #'silicon_webber'  #'silicon_internet'
material_min    = 'air'           # minimal epsilon
material_max    = 'fused_silica'  # maximal epsilon
                                  # /!\ depending on the type of material, change the Flag_design_metal below !
material_substrate = 'silver_widerange'

## Numerical parameters ##
paramaille  = 15         # minimum number of mesh elements per wavelength
# Refinement coefficients on each region of the geometry (fraction of paramaille)
paramaille_pmlSup      = 1
paramaille_superstrate = 1
paramaille_design      = 1.5
paramaille_substrate   = 2
paramaille_pmlInf      = 1

## Parallel computation
num_threads_multilambda = nb_wavelengths
num_threads_resolution  = 1
# /!\ num_threads_multilambda * num_threads_resolution must be < number of available threads on the machine

## Initial density field ##
def analytic_init_density(x,y):
    return -(x - period*nm/2)/(period*nm)
    # return random.random()

## Optimization parameter ##
# Boundaries of the densities for the simulation
low_bnd = 0.
up_bnd  = 1.

# Stopping criteria
tol = 1e-5
maxiter = 100

# Filters parameter
filter_radius = 200   # diffusion
nu = 1/2              # binarization
nb_binarizations = 7

# (For the finite differences only) Step of the finite differences
h_0 = 1e-2

## Choices for the simulation ##
# Flags
Flag_diffusion    = 1      # Impose the connectedness (1) or not (0)
Flag_binarize     = 1      # Binarize the densities (1) or not (0)
Flag_design_metal = 0      # Is the nanostructure made of metal (1) or not (0)
Flag_pml_inf      = 0      # Do we need an inferior PML (1) or not (0)?
Flag_test         = 0      # Launch an optimization (0) or a test on the Jacobian (1)
Flag_o2_geom      = 0      # Type of finite elements (0 : tetrahedron ; 1 : curved)
Flag_o2_interp    = 1      # Type of interpolation method (0 : P^2 ; 1 : more)

## Identification of the surfaces of integration --------------------------------------------------------------------------------------
#  (normally one does not have to change these values)
PML_SUP     = 2000
SUPERSTRATE = 2100
DESIGN      = 2200
SUBSTRATE   = 2300
PML_INF     = 2400

SURF_LEFT      = 100
SURF_RIGHT     = 101
SURF_UP        = 102
SURF_DOWN      = 103
SURF_INTEG_SUP = 120
SURF_INTEG_SUB = 121
SURF_PLOT      = 140
PRINT_POINT    = 150

## Optimization feedbacks -------------------------------------------------------------------------------------------------------------

# Tool for the feedback
optim_iter = 0
np.savetxt("optim_iter.txt", np.array([optim_iter]))

def plot_msh_density(current_msh_density, fig_msh_density_filename):
    ''' Plot and save the density field written in a Python simple array '''

    mesh = mio.read("design_region.msh")
    fig  = plt.figure(figsize=(6,6))
    im   = plt.tripcolor(mesh.points[:, 0],mesh.points[:, 1],mesh.cells_dict['triangle'], facecolors=current_msh_density, cmap='Blues', vmin=0, vmax=1)
    plt.axis('scaled')
    plt.axis('off')
    plt.colorbar(im, fraction=0.046, pad=0.04, shrink=0.82)
    plt.savefig(fig_msh_density_filename, bbox_inches='tight')
    plt.close(fig)

    return(0)

def display_density(current_rho):
    ''' Update the iteration of the optimization process and store the current density '''

    global optim_iter
    optim_iter += 1
    print("###   Solving problem (direct and inverse) - iteration %03d...   ###"%optim_iter)
    plot_msh_density(current_rho, "myimage_%03d.png"%optim_iter)
    np.savetxt("optim_iter.txt", np.array([optim_iter]))

    return(0)