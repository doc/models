/*  Ishape3d.pro
Material description (for getdp) of Ishape3D test for ECE
For details, see comments in the Ishape3d_data.pro file
The Ishape3d geometry and meshing parameters are in the Ishape3d.geo file
This file contains the setings that depend on the particular problem and
it includes 1 general files
*/

Include "Ishape3d_data.pro"

Group{
  MaterialX = Region[100]; // MaterialX - homogenous domain
  Ground = Region[120];    // Boundary - various parts
  Terminal = Region[121];
  BoundaryNotTerminal = Region[131];

  Vol_FW = Region[{MaterialX}]; // domain without the boundary
  Sur_Terminals_FWece = Region[{Ground, Terminal}]; // all terminals
  Sur_FW = Region[{Sur_Terminals_FWece, BoundaryNotTerminal}]; //all boundary
  Dom_FW = Region[ {Vol_FW, Sur_FW} ];
 }


 Function{
   /*
   Material properties are defined piecewise, for elementary groups
   the values in the right hand side are defined in Ishape2d_data.pro
   */

   nu[#{MaterialX,Sur_FW}] = nu;             // magnetic reluctivity [m/H]
   epsilon[#{MaterialX,Sur_FW}] = eps;       // permittivity [F/m]
   sigma[#{MaterialX,Sur_FW}]  = sigma;      // conductivity [S/m]

   // This is a problem with 2 terminals, out of which one is grounded.
   // The non-grounded can be excited in voltage or in current.
   // Depending on the excitation type, one of the following functions will be useful.
   //
   // When imposing voltage or current via a constraint, the region appears there
   // If you have a complex, the value has to be a function => use square brackets []
   If(Flag_AnalysisType==0)
     VTerminal1[] = -VTermRMS*Complex[Cos[VTermPhase],Sin[VTermPhase]];
   Else
     ITerminal1[] = -ITermRMS*Complex[Cos[ITermPhase],Sin[ITermPhase]];
   EndIf
 }

 Constraint{

   // ece BC
   { Name SetTerminalPotential; Type Assign; // voltage excited terminals
     Case {
       { Region Ground; Value 0.; }
       If(Flag_AnalysisType==0)
         { Region Terminal; Value VTerminal1[]; }
       EndIf
     }
   }
   { Name SetTerminalCurrent; Type Assign; // current excited terminals
     Case {
       If(Flag_AnalysisType==1)
         { Region Terminal; Value ITerminal1[]; }
       EndIf
     }
   }

}

// For convenience, saving results in different directories
// Dir can be empty: Dir ="";
 If (Flag_AnalysisType==0)
   Dir = "res/FWeceBC_voltExc/";
 Else
   Dir = "res/FWeceBC_crtExc/";
 EndIf

 // The formulation and its tools
Include "FullWave_E_ece_SISO.pro"


// Some Macros for changing Gmsh options
// Strictly not necessary => for aestetics or easying result treatment
Macro Change_post_options
Echo[Str[ "nv = PostProcessing.NbViews;",
    "For v In {0:nv-1}",
    "View[v].NbIso = 25;",
    "View[v].RangeType = 3;" ,// per timestep
    "View[v].ShowTime = 3;",
    "View[v].IntervalsType = 3;",
    "EndFor"
  ], File "post.opt"];
Return

Macro WriteZ_header
  Echo[ Str[Sprintf("# Hz Z RI R %g", 50)], Format Table, File > StrCat[Dir, "test_Z_RI.s1p"] ];
Return
Macro WriteY_header
  Echo[ Str[Sprintf("# Hz Y RI R %g", 50)], Format Table, File > StrCat[Dir, "test_Y_RI.s1p"] ];
Return



PostOperation {
  { Name Maps; NameOfPostProcessing FW_E_ece ;
    Operation {
      Print [ gradV, OnElementsOf ElementsOf[Vol_FW,OnOneSideOf Sur_FW], File StrCat[Dir,"map_gradV.pos" ]];

      Print [ E, OnElementsOf Vol_FW, File StrCat[Dir,"map_E.pos" ]];
      // Print [ J, OnElementsOf Vol_FW, File StrCat[Dir,"map_J.pos" ]];
      // Print [ rmsJ, OnElementsOf Vol_FW, File StrCat[Dir,"map_absJ.pos" ]];

      Print [ H, OnElementsOf Vol_FW, File StrCat[Dir,"map_H.pos" ]];
      Print [ Vterminals, OnElementsOf ElementsOf[Sur_Terminals_FWece], File StrCat[Dir,"map_Vterminals.pos" ]];
      Print [ VnotTerminals, OnElementsOf Vol_FW, File StrCat[Dir,"map_VnotTerminals.pos" ]];

      Call Change_post_options;
    }
  }


  { Name TransferMatrix; NameOfPostProcessing FW_E_ece ;
    Operation {
      If(Flag_AnalysisType==1)  // current excitation
        If (Freq==freqs(0))
          Call WriteZ_header;
          Printf("======================> Writting header with first Freq=%g",Freq);
        EndIf
        Print [ Vterminals, OnRegion Terminal,
          Format Table , File  > StrCat[Dir, "test_Z_RI.s1p"]] ;
      Else
        If (Freq==freqs(0))
          Call WriteY_header;
          Printf("======================> Writting header with first Freq=%g",Freq);
        EndIf
        Print [ I, OnRegion Terminal,
          Format Table , File  > StrCat[Dir, "test_Y_RI.s1p"]] ;
      EndIf
    }
  }

}
