/* Ishape3d_data.pro
=========================== Ishape3D - test problem
A cylindrical domain in space, one circular face in the plane z = 0, circle of radius a,
axis of the cylinder is Oz, length = l

The domain is filled with homogeneous and linear material, with epsilon, mu, sigma

There is no no field source inside the domein (the "device" is passive).
The field sources are only boundary conditions (BC) which are ECE.

The problem has 2 terminals (on top and bottom), one has to be grounded, the other two can be either
voltage excited or current excited.

This is a test problem - used to validate the ECE implementation in onelab for a 3D problem

===========================
Names of input data - to be given by the user
Geometry:
a = radius of the cylinder [m]
l = height of the cylinder [m]
==
Material
epsr = relative permittivity
mur = relative permeability
sigma = conductivity [S/m]

==
Type of BC - always ece, top terminal is gnd; bottom terminal can be ev = electric voltage forced terminal; ec = electric current forced terminal
typeBC = 0,1,
0 = bottom ev
1 = bottom ec
==
Excitation signals
imposed by the user, on non-grounded terminals; default values - is 1 (V or A)
=========================== */

colorro = "LightGrey";

/* new menu for the interface - holds geometrical data */
cGUI= "{Ishape3D-ProblemData/";
mGeo = StrCat[cGUI,"Geometrical parameters/0"];
colorMGeo = "Ivory";
close_menu = 1;
DefineConstant[
  a = {2.5e-6, Name StrCat[mGeo, "0a=0.5*Dimension along Ox"], Units "m", Highlight Str[colorMGeo], Closed close_menu},
  l = {10e-6, Name StrCat[mGeo, "1l=Dimension along Oy"], Units "m", Highlight Str[colorMGeo]}
];

/* new menu for the interface - holds material data */
mMat = StrCat[cGUI,"Material parameters/0"];
colorMMat = "AliceBlue";
DefineConstant[
  epsr = {1, Name StrCat[mMat, "0Relative permittivity T part"], Units "-", Highlight Str[colorMMat], Closed close_menu},
  mur = {1, Name StrCat[mMat, "1Relative permeability T part"], Units "-", Highlight Str[colorMMat]},
  sigma = {6.6e7, Name StrCat[mMat, "2Conductivity T part"], Units "S/m", Highlight Str[colorMMat]}
];

eps0 = 8.854187818e-12; // F/m - absolute permitivity of vacuum
mu0  = 4.e-7 * Pi;      // H/m - absolute permeability of vacuum
nu0  = 1/mu0;           // m/H - absolute reluctivity of vacuum

eps = eps0*epsr;        // F/m - absolute permitivity
mu = mu0*mur;           // F/m - absolute permeability
nu = 1/mu;              // m/H - absolute reluctivity


// new menu for the interface - type of BC
mTypeBC = StrCat[cGUI,"Type of BC/0"];
colorMTypeBC = "Red";
DefineConstant[
  Flag_AnalysisType = { 0,
    Choices{
      0="bottomTerm ev",
      1="bottomTerm ec"},
    Name Str[mTypeBC], Highlight Str[colorMTypeBC], Closed !close_menu,
    ServerAction Str["Reset", "GetDP/1ResolutionChoices"]}
];


// new menu for the interface - values of excitations - significance depends on the type of BC
mValuesBC = StrCat[cGUI,"Values of BC (frequency analysis)/0"];
colorMValuesBC = "Ivory";

fmin = 1e7;   // Hz
fmax = 100e9; // Hz

nop = 10;
//freqs()=LogSpace[Log10[fmin],Log10[fmax],nop];
freqs()=LinSpace[fmin,fmax,nop];
DefineConstant[
  Freq  = {freqs(0), Choices{freqs()}, Loop 0, Name StrCat[mValuesBC, "0Working Frequency"],
    Units "Hz", Highlight  Str[colorMValuesBC], Closed !close_menu }

  // Flag_AnalysisType
  // ==0 bottom ev
  // ==1 bottom ec

  VGroundRMS  = {0, Name StrCat[mValuesBC, "2Top/1Potential on bottom (rms)"],
    Units "V", ReadOnly 1, Highlight Str[colorro],
    Visible (Flag_AnalysisType==0) || (Flag_AnalysisType==1)}
  VGroundPhase  = {0, Name StrCat[mValuesBC, "2Top/1Potential on bottom, phase"],
    Units "rad",  ReadOnly 1, Highlight Str[colorro],
    Visible (Flag_AnalysisType==0) || (Flag_AnalysisType==1)}

  VTermRMS  = {1, Name StrCat[mValuesBC, "1Bottom/1Potential on Term1 (rms)"],
    Units "V", ReadOnly 0, Highlight Str[colorMValuesBC],
    Visible (Flag_AnalysisType==0)}
  VTermPhase  = {0, Name StrCat[mValuesBC, "1Bottom/1Potential on Term1, phase"],
    Units "rad",  ReadOnly 0, Highlight Str[colorMValuesBC],
    Visible (Flag_AnalysisType==0)}

  ITermRMS  = {1, Name StrCat[mValuesBC, "1Bottom/1Current through Term1 (enters the domain) (rms)"],
    Units "A", ReadOnly 0, Highlight Str[colorMValuesBC],
    Visible (Flag_AnalysisType==1) }
  ITermPhase  = {0, Name StrCat[mValuesBC, "1Bottom/Current through Term1 (enters the domain), phase"],
    Units "rad",  ReadOnly 0, Highlight Str[colorMValuesBC],
    Visible (Flag_AnalysisType==1) }

  //_use_transfinite = {0, Choices{0,1}, Name "{MeshSettings/0Transfinite mesh?", Highlight "Yellow", Closed !close_menu}
   s = {1, Name "{MeshSettings/1Mesh factor s"}
   nbDelta = {1, Name "{MeshSettings/2Nb of elems per skin depth"}
];
