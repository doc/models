/*  Ishape3d.geo
Geometrical description (for gmsh) of Ishape3D test for ECE
For details, see comments in the Ishape3d_data.pro file
Meshing information is also defined here.  

This uses OpenCascade
*/


Include "Ishape3d_data.pro";
SetFactory("OpenCASCADE");

xc = 0; yc = 0; zc = 0; 
vx = 0; vy = 0; vz = l;
radius = a;

volDom = newv; Cylinder(newv) = {xc, yc, zc, vx, vy, vz, radius};

b() = Boundary{ Volume{volDom}; };
Printf("    surfaces", b());

// In order to identify which surface is which I played with the GUI!!!

lateralCyl = b(0);
bottomSurf = b(1);
topSurf = b(2);


//bs() = Boundary{ Surface{bottomSurf}; };
//Printf("    curves", bs());
//circle1 = bs(0);


lc() = Boundary{ Surface{lateralCyl}; };
Printf("    curves", lc());
circle1 = lc(0);
lineCyl = lc(1);
circle2 = lc(2);
//Transfinite Line {lineCyl} = 20;
//Transfinite Curve{circle1} = 50;

// Physical regions
// --------------------------
Physical Volume ("MaterialX", 100) = {volDom} ;      /* MaterialX */

Physical Surface ("Ground",   120) = {bottomSurf} ;              /* Ground */
Physical Surface ("Terminal", 121) = {topSurf} ;                 /* Terminal */
Physical Surface ("BoundaryNotTerminal", 131) = {lateralCyl} ;  


/* Definition of parameters for local mesh dimensions */
//lcar = s*l/10; // characteristic length of mesh element

// lcar dep on Freq, wavelenth - 10 elements per wavelength and dimension
//lambda = c0/Freq; // Label "Wavelength [m]"}
//lcar  = lambda/nbla
// nbDelta from _data.geo,  number of elements per skin depth

delta = Sqrt(2.0/(2*Pi*Freq*mu*sigma));
lcarDelta = delta/nbDelta;
lcar = s*l/10;


	
		//Field[1] = Distance;
		//Field[1].CurvesList = {circle1};
		//Field[1].SurfacesList = {lateralCyl};
		//Field[1].Sampling = 200;
	
		//Field[2] = Threshold;
		//Field[2].InField = 1;
		//Field[2].SizeMin = a/10; //lcar;
		//Field[2].SizeMax = a/2;
		//Field[2].DistMin = a/8; //delta;  
		//Field[2].DistMax = a/4; //delta;
		
		//Field[2] = Box;
        //Field[2].VIn = a/2; 
        //Field[2].VOut = a/10; //lcar;
        //Field[2].XMin = -2*a/3;
        //Field[2].XMax = 2*a/3;
        //Field[2].YMin = -2*a/3;
        //Field[2].YMax = 2*a/3;
		//Field[2].ZMin = 0;
        //Field[2].ZMax = l;
        //Field[2].Thickness = 0.3;

        Field[1] = Cylinder;
        Field[1].VIn = lcar; 
        Field[1].VOut = lcar;
        Field[1].Radius = a;
        Field[1].XCenter = 0;
        Field[1].YCenter = 0;
        Field[1].ZCenter = l/2;
		Field[1].XAxis = 0;
        Field[1].YAxis = 0;
		Field[1].ZAxis = l;
		
		Field[2] = Cylinder;
        Field[2].VIn = a/3; 
        Field[2].VOut = lcarDelta;
        Field[2].Radius = a - delta;
        Field[2].XCenter = 0;
        Field[2].YCenter = 0;
        Field[2].ZCenter = l/2;
		Field[2].XAxis = 0;
        Field[2].YAxis = 0;
		Field[2].ZAxis = l;
		

		Field[3] = Min;
		Field[3].FieldsList = {1,2};
		Background Field = 3;
		Mesh.MeshSizeExtendFromBoundary = 0;
		Mesh.MeshSizeFromPoints = 0;
		Mesh.MeshSizeFromCurvature = 0;
		Mesh.Algorithm = 5;
	

/* Definition of parameters for local mesh dimensions */
//lcar = s*l/10; // characteristic length of mesh element
// Assign a mesh size to all the points of all the volumes:
//MeshSize{ PointsOf{ Volume{:}; } } = lcar;
