DefineConstant[
  cm = 1e-2,
  mm = 1e-3
  rw  = {5*mm/2, Name "Param. Geometry/03radius or wire", Highlight "Pink", ReadOnly}
  rplateC = {6*cm, Name "Param. Geometry/10radius of capacitor plates", Highlight "LightCyan", ReadOnly}
  dplateC = {3*mm, Name "Param. Geometry/11distance between plates", Highlight "LightCyan", ReadOnly}
];

sigma_cond = 1e7;
mur_core = 1000;
epsr_diel = 1e6;
sigma_diel = 0;

mu0 = 4.e-7 * Pi ;
nu0  = 1/mu0;
eps0 = 8.854187818e-12;

// --------------------

nuAir = nu0;
epsAir = eps0;
sigmaAir = 0;

nuCore = 1/(mu0*mur_core);
epsCore = eps0;
sigmaCore = 0;

nuDiel = nu0;
epsDiel = eps0*epsr_diel;
sigmaDiel = sigma_diel;

nuCond = nu0;
epsCond = eps0;
sigmaCond = sigma_cond;


//----------------------
// Physical numbers...
//----------------------
CONDUCTOR     = 1000;  // volume, conductor touching the boundary at the excited termimnal, including the coil wire and one plate
BND_CONDUCTOR = 1111;  // volume, conductor touching the boundary at the grounded terminal and including the other plate

ELEC_IN  = 111; // surface, excited terminal
ELEC_OUT = 112; // surface, grounded terminal

DIELECTRIC   = 2000; // volume, the dielectric of the capacitor
ELEC_DIEL_IN = 221;
ELEC_DIEL_OUT = 222;

CORE = 3000;  // volume, the core of the coil

AIR = 4000;  // volume, the air
BND_AIR = 4444; // surface, boundary touched by air

// -----------------------
cGUI= "{CL - Excitation/";
close_menu = 1;
colorro = "LightGrey";
mTypeBC = StrCat[cGUI,"Type of ECE Excitation/0"];

colorMTypeBC = "Red";
DefineConstant[
  Flag_AnalysisType = { 0,
    Choices{
      0="ev",
      1="ec"},
    Name Str[mTypeBC], Highlight Str[colorMTypeBC], Closed !close_menu,
    ServerAction Str["Reset", "GetDP/1ResolutionChoices"]}
];

/* new menu for the interface - values of excitations - significance depends on the type of BC */
mValuesBC = StrCat[cGUI,"Frequency values/0"];
colorMValuesBC = "Ivory";

fmin = 1000; // Hz
fmax = 80000; // Hz

nop = 10;
freqs()=LogSpace[Log10[fmin],Log10[fmax],nop];
//freqs()=LinSpace[fmin,fmax,nop];
DefineConstant[
  Freq  = {freqs(0), Choices{freqs()}, Loop 0, Name StrCat[mValuesBC, "0Working Frequency"], Units "Hz", Highlight  Str[colorMValuesBC], Closed !close_menu }
];


If (Flag_AnalysisType==1) // current exc
  Printf("======================> Terminal is current => Z transfer function");
  ITermRMS = 1;
  ITermPhase = 0;
Else  // voltage exc
  Printf("======================> Terminal is voltage excited => Y transfer function");
  VTermRMS = 1;
  VTermPhase = 0;
EndIf
