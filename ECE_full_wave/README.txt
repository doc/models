Electric circuit element  (ECE) boundary conditions in full-wave electromagnetic problems.

  Models developed by G. Ciuprina, D. Ioan and R.V. Sabariego.

This directory contains the pro and geo files necessary to run the test cases in article:

G. Ciuprina, D. Ioan, and R.V. Sabariego. Electric circuit element boundary conditions
in the finite element method for full-wave passive electromagnetic devices.
Journal of Mathematics in Industry, 12:1-13, Art no. 7, 2022. DOI: 10.1186/s13362-022-00122-1.

Common formulation files:
-------------------------
  "FullWave_E_ece_SISO.pro" - file with function spaces, formulation, resolution and postprocessing.

Cylinder benchmark:
-------------------
  "Ishape3D_data.pro" - contains geometrical and physical parameters used by the geo and pro files.
  "Ishape3D.geo" - contains the geometry description.
  "Ishape3D.pro" - contains the problem definition.

Inductor-Capacitor (LC) benchmark:
-------------
  "LC_data.pro" - contains geometrical and physical parameters used by the geo and pro files.
  "LC_ostrowski.stp" - STEP geometry courtesy of J. Ostrowski.
  "LC.geo" - adapts STEP file to use with Gmsh. Note that the computation of BooleanFragments is very slow.
  "LC.pro" - contains the problem definition.

Quick start
-----------
  Open "Ishape3D.pro" with Gmsh.
  Open "LC.pro" with Gmsh.
