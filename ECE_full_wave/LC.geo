Include "LC_data.pro";

SetFactory("OpenCASCADE");

// convert geometry read from STEP file to meters
Geometry.OCCTargetUnit = "M";

Geometry.NumSubEdges = 100; // Visualisation

// We can activate the calculation of mesh element sizes based on curvature:
Mesh.MeshSizeFromCurvature = 1; // Needed for spiral/helix

// And we set the minimum number of elements per 2*Pi radians:
Mesh.MinimumElementsPerTwoPi = 20/2;

// We can constraint the min and max element sizes to stay within reasonnable
// values (see `t10.geo' for more details):
Mesh.MeshSizeMin = rw/4;
Mesh.MeshSizeMax = rplateC;



// read STEP file and store volume(s)
v() = ShapeFromFile("LC_ostrowski.stp");

vol_air() = v({10,11});
vol_cond() = v({0,1,3:6,8,9});
vol_diel() = v(7);
vcore=v(2);

// Doing intersections that are not in the initial step
vol_aux()=BooleanFragments{Volume{vol_cond(),vol_diel()};Delete;}{}; //Plates and dielectric have to be intersected with connectors
Coherence; // Air around

// define physical groups
Physical Volume("DomainAir", AIR) = vol_air();
Physical Volume("DomainConductor",  CONDUCTOR) = vol_cond(); // 6 and 8 are the capacitor plates
Physical Volume("DomainDielectric", DIELECTRIC) = vol_diel();
Physical Volume("DomainCore", CORE) = vcore;

// outside boundaries
bnd_air() = Abs(CombinedBoundary{Volume{Volume{:}};});
surf_elec_in = 130; //side L
surf_elec_out = 144; // side C
bnd_air() -= {surf_elec_in,surf_elec_out};

Physical Surface("Terminal",  ELEC_IN)  = surf_elec_in;
Physical Surface("Ground", ELEC_OUT) = surf_elec_out;
Physical Surface("SurBCnotTerminalAir", BND_AIR) = bnd_air();


// Adapting the mesh
// get bounding box
bb() = BoundingBox Volume {v()};

// size along axes (useful for adapting mesh)
sizeX = bb(3)-bb(0);
sizeY = bb(4)-bb(1);
sizeZ = bb(5)-bb(2);


scale_factor=2;
lc_ref = scale_factor*sizeX/200;
ptos_cond() = PointsOf{Volume{vol_cond()};};
ptos_air_in() = PointsOf{Volume{v(10)};};
ptos_core() = PointsOf{Volume{vcore};};
ptos_air_in() -= {ptos_cond(),ptos_core()};
ptos_air_out() = PointsOf{Surface{152,153};}; // only the side

MeshSize{ptos_air_out()} = 10*lc_ref;
MeshSize{ptos_air_in()} = 3*lc_ref;
MeshSize{ptos_core()}   = 1.5*lc_ref;
MeshSize{ptos_cond()}   = lc_ref;

// For aestetics
Recursive Color SkyBlue { Volume{vol_air()};}
Recursive Color Green  { Volume{vcore};}
Recursive Color Red  { Volume{vol_cond()};}
Recursive Color Yellow  { Volume{vol_diel()};}
