/*  LC.pro
Linked to step file from Joerg Ostrowski
FW in E and V, ECE
*/

Include "LC_data.pro"

Group{

  Ground = Region[{ELEC_OUT}];
  Terminal = Region[{ELEC_IN}];
  SurBCnotTerminalAir = Region[{BND_AIR}];

  DomainConductor = Region[{CONDUCTOR}];
  DomainDielectric = Region[{DIELECTRIC}];
  DomainCore = Region[{CORE}];
  DomainAir = Region[{AIR}];

  Vol_FW = Region[{DomainConductor,DomainDielectric,DomainCore,DomainAir}];
  Sur_Terminals_FWece = Region[{Ground, Terminal}]; // all terminals
  Sur_FW = Region[{Sur_Terminals_FWece, SurBCnotTerminalAir}]; //all boundary
  Dom_FW = Region[ {Vol_FW, Sur_FW} ];
 }


 Function{
   /*
   Material properties are defined piecewise, for elementary groups
   the values in the right hand side are defined in CL_data.pro
   */

   // conductor
   nu[#{DomainConductor,Ground,Terminal}] = nuCond;             // magnetic reluctivity [m/H]
   epsilon[#{DomainConductor,Ground,Terminal}] = epsCond;       // permittivity [F/m]
   sigma[#{DomainConductor,Ground,Terminal}]  = sigmaCond;      // conductivity [S/m]

   // dielectric
   nu[#{DomainDielectric}] = nuDiel;
   epsilon[#{DomainDielectric}] = epsDiel;
   sigma[#{DomainDielectric}]  = sigmaDiel;

   // core
   nu[#{DomainCore}] = nuCore;
   epsilon[#{DomainCore}] = epsCore;
   sigma[#{DomainCore}]  = sigmaCore;

   // air
   nu[#{DomainAir,SurBCnotTerminalAir}] = nuAir;
   epsilon[#{DomainAir,SurBCnotTerminalAir}] = epsAir;
   sigma[#{DomainAir,SurBCnotTerminalAir}]  = sigmaAir;

   If(Flag_AnalysisType==0)
     VTerminal1[] = -VTermRMS*Complex[Cos[VTermPhase],Sin[VTermPhase]];
   Else
     ITerminal1[] = -ITermRMS*Complex[Cos[ITermPhase],Sin[ITermPhase]];
   EndIf
 }

 Constraint{

   // ece BC
   { Name SetTerminalPotential; Type Assign;  // voltage excited terminals
     Case {
       { Region Ground; Value 0.; }
       If(Flag_AnalysisType==0)
         { Region Terminal; Value VTerminal1[]; }
       EndIf
     }
   }
   { Name SetTerminalCurrent; Type Assign;   // current excited terminals
     Case {
       If(Flag_AnalysisType==1)
         { Region Terminal; Value ITerminal1[]; }
       EndIf
     }
   }

}


If (Flag_AnalysisType==0)
  Dir = "res/FWeceBC_voltExc/";
Else
  Dir = "res/FWeceBC_crtExc/";
EndIf

// The formulation and its tools
Include "FullWave_E_ece_SISO.pro"

// Output results
Macro Change_post_options
Echo[Str[ "nv = PostProcessing.NbViews;",
    "For v In {0:nv-1}",
    "View[v].NbIso = 25;",
    "View[v].RangeType = 3;" ,// per timestep
    "View[v].ShowTime = 3;",
    "View[v].IntervalsType = 3;",
    "EndFor"
  ], File "post.opt"];
Return

Macro WriteZ_header
  Echo[ Str[Sprintf("# Hz Z RI R %g", 50)], Format Table, File > StrCat[Dir, "test_Z_RI.s1p"] ];
Return
Macro WriteY_header
  Echo[ Str[Sprintf("# Hz Y RI R %g", 50)], Format Table, File > StrCat[Dir, "test_Y_RI.s1p"] ];
Return


Macro Plugin_cuts
  Echo[ Str[
      "nview = PostProcessing.NbViews-1;",
      "vname = Str[View[nview].Name];",
      "Plugin(CutPlane).A        = 1;",
      "Plugin(CutPlane).B        = 0;",
      "Plugin(CutPlane).C        = 0;",
      "Plugin(CutPlane).D        = -0.0289;", // half wire L side
      "Plugin(CutPlane).View     = nview;",
      "Plugin(CutPlane).Run;",
      "View[nview+1].RangeType = 2;", // custom
      "View[nview+1].CustomMax = View[nview+1].Max;", // max
      "View[nview+1].CustomMin = View[nview+1].MinVisible;", // min
      "View[nview+1].ScaleType = 2;", // logarithmic scale
      "View[nview+1].Name = StrCat[vname, ' on YZ plane'];"
    ], File StrCat[Dir,"cuts.geo"] ] ;
Return

PostOperation {

  { Name Maps; NameOfPostProcessing FW_E_ece ;
    Operation {
      Print [ gradV, OnElementsOf ElementsOf[Vol_FW,OnOneSideOf Sur_FW], File StrCat[Dir,"map_gradV.pos" ]];

      Print [ Vterminals, OnElementsOf ElementsOf[Sur_FW], File StrCat[Dir,"map_Vterminals.pos" ]];
      Print [ VnotTerminals, OnElementsOf Vol_FW, File StrCat[Dir,"map_VnotTerminals.pos" ]];
      Print [ H, OnElementsOf Vol_FW, File StrCat[Dir,"map_H.pos" ]];
      Print [ E, OnElementsOf Vol_FW, File StrCat[Dir,"map_E.pos" ]];
      Call Change_post_options;

      Print [ normE, OnElementsOf Vol_FW, File StrCat[Dir,"map_normE.pos" ]];
      Call Plugin_cuts;
    }
  }

  { Name TransferMatrix; NameOfPostProcessing FW_E_ece ;
    Operation {
      If(Flag_AnalysisType==1)  // current excitation
        If (Freq==freqs(0))
          Call WriteZ_header;
          Printf("======================> Writting header with first Freq=%g",Freq);
        EndIf
        Print [ Vterminals, OnRegion Terminal,
          Format Table , File  > StrCat[Dir, "test_Z_RI.s1p"]] ;
      Else
        If (Freq==freqs(0))
          Call WriteY_header;
          Printf("======================> Writting header with first Freq=%g",Freq);
        EndIf
        Print [ I, OnRegion Terminal,
          Format Table , File  > StrCat[Dir, "test_Y_RI.s1p"]] ;
      EndIf
    }
  }

}

// What follows only works if your solver is GetDP => adapt name if necessary

DefineConstant[
  // always solve this resolution (it's the only one provided)
  // R_ = {"Analysis", Name "GetDP/1ResolutionChoices", Visible 1}
  // set some command-line options for getdp
  C_ = {"-solve -v 4 -pos -v2", Name "GetDP/9ComputeCommand", Visible 1}
  // don't do the post-processing pass (some already included in Resolution)
  P_ = {"Maps", Name "GetDP/2PostOperationChoices", Visible 1}
];
