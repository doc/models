/* FullWave_E_ece_SISO.pro

 Problem independent pro file
 for passive, linear domains in Full Wave with ECE boundary conditions.

 It uses a formulation with vec{E} strictly inside the domain and V strictly on the boundary.
 The domain is simply connected, it has only electric terminals.
 The terminals can be voltage or current excited.
 The material inside is linear from all points of view: electric, magnetic, conduction
 It is compulsory that there exists a ground terminal (its voltage is set to zero).
 The post-operation assumes that there is one excited (active) terminal  (excitation equal to 1 - it can be in
 voltage or current excited terminal).
 Frequency domain simulations.
*/


Jacobian {
  { Name Vol;
    Case {
      { Region All; Jacobian Vol; }
    }
  }
  { Name Sur;
    Case {
      { Region All; Jacobian Sur; }
    }
  }
}

Integration {
  { Name Int;
    Case {
      { Type Gauss;
        Case {
          { GeoElement Point; NumberOfPoints  1; }
          { GeoElement Line; NumberOfPoints  3; }
          { GeoElement Triangle; NumberOfPoints  3; }
          { GeoElement Quadrangle; NumberOfPoints  4; }
          { GeoElement Tetrahedron; NumberOfPoints  4; }
          { GeoElement Hexahedron; NumberOfPoints  6; }
          { GeoElement Prism; NumberOfPoints  9; }
          { GeoElement Pyramid; NumberOfPoints  8; }
	}
      }
    }
  }
}

FunctionSpace {

  { Name Hcurl_E; Type Form1;
    BasisFunction {
      { Name se; NameOfCoef ee; Function BF_Edge;
        Support Dom_FW ; Entity EdgesOf[All, Not Sur_FW]; }
      { Name sn; NameOfCoef vn; Function BF_GradNode;
        Support Dom_FW ; Entity NodesOf[Sur_FW, Not Sur_Terminals_FWece]; }
      { Name sf; NameOfCoef vf; Function BF_GradGroupOfNodes;
        Support Dom_FW ; Entity GroupsOfNodesOf[Sur_Terminals_FWece]; }
    }
    GlobalQuantity {
      { Name TerminalPotential; Type AliasOf       ; NameOfCoef vf; }
      { Name TerminalCurrent  ; Type AssociatedWith; NameOfCoef vf; }
    }

    SubSpace {
      { Name dv ; NameOfBasisFunction {sn}; } // Subspace, it maybe use in equations or post-pro
    }

    Constraint {
      { NameOfCoef TerminalPotential; EntityType GroupsOfNodesOf; NameOfConstraint SetTerminalPotential; }
      { NameOfCoef TerminalCurrent;   EntityType GroupsOfNodesOf; NameOfConstraint SetTerminalCurrent; }
    }
  }

}

Formulation {

  { Name FullWave_E_ece; Type FemEquation;
    Quantity {
      { Name e;  Type Local; NameOfSpace Hcurl_E; }
      { Name dv; Type Local; NameOfSpace Hcurl_E[dv]; } // Just for post-processing issues
      { Name V;  Type Global; NameOfSpace Hcurl_E[TerminalPotential]; }
      { Name I;  Type Global; NameOfSpace Hcurl_E[TerminalCurrent]; }
    }
    Equation {
      // \int_D curl(\vec{E}) \cdot  curl(\vec{e}) dv
      Galerkin { [ nu[] * Dof{d e} , {d e} ]; In Vol_FW; Jacobian Vol; Integration Int; }

      // \int_D j*\omega*(\sigma + j*\omega*\epsilon) \vec{E} \cdot \vec{e} dv
      Galerkin { DtDof   [ sigma[]   * Dof{e} , {e} ]; In Vol_FW; Jacobian Vol; Integration Int; }
      Galerkin { DtDtDof [ epsilon[] * Dof{e} , {e} ]; In Vol_FW; Jacobian Vol; Integration Int; }

      //    alternative // j*\omega*sum (vk Ik);  for k - current excited terminals
      //    GlobalTerm {DtDof [ -Dof{I} , {V} ]; In SurBCec; }

      // j*\omega*sum (vk Ik);  for k - all terminals so that the currents through the terminals will be computed as well
      GlobalTerm {DtDof [ -Dof{I} , {V} ]; In Sur_Terminals_FWece; }
    }
  }

}


Resolution {

  { Name FullWave_E_ece;
    System {
      { Name Sys_Ele; NameOfFormulation FullWave_E_ece; Type Complex; Frequency Freq;}
    }
    Operation {
      CreateDir[Dir];
      SetTime[Freq];  // useful to save (and print) the current frequency
      Generate[Sys_Ele]; Solve[Sys_Ele]; SaveSolution[Sys_Ele];
    }
  }

}

PostProcessing {

  { Name FW_E_ece; NameOfFormulation FullWave_E_ece;
    Quantity {
      { Name E;
        Value {
          Local { [ {e} ]; In Vol_FW; Jacobian Vol; }
        }
      }

      { Name normE;
        Value {
          Local { [ Norm[{e}] ]; In Vol_FW; Jacobian Vol; }
        }
      }


      { Name J;
        Value {
          Local { [ sigma[]*{e} ]; In Vol_FW; Jacobian Vol; }
        }
      }

      { Name rmsJ;
        Value {
          Local { [ Norm[sigma[]*{e}] ]; In Vol_FW; Jacobian Vol; }
        }
      }

      { Name gradV;
        Value {
          Local { [ {dv} ]; In Vol_FW; Jacobian Vol; }
        }
      }

      { Name Vterminals;
        Value {
          // This is a global variable, you do not need to indicate the Jacobian
          // It is only defined at the terminal and it is only one value for the whole line (surface in 3D)
          // If the support you give here is wider, you will get 0 where it is not defined
          Term { [ -1*{V} ]; In Sur_Terminals_FWece; } // => unknowns only in Sur_Terminals_FWece, outside it will be zero
        }
      }

      { Name I;
        Value {
          Term { [ -1*{I} ]; In Sur_Terminals_FWece; }
        }
      }

      { Name VnotTerminals;
        Value {
          Local { [ -{dInv dv} ]; In Vol_FW; Jacobian Vol; }
        }
      }

      { Name B;
        Value {
          Local { [ {d e}/(2*Pi*Freq*Complex[0,1]) ]; In Vol_FW; Jacobian Vol; }
        }
      }

      { Name H;
        Value {
          Local { [ nu[]*{d e}/(2*Pi*Freq*Complex[0,1]) ]; In Vol_FW; Jacobian Vol; }
        }
      }



      { Name Bz;
        Value {
          Local { [ CompZ[{d e}]/(2*Pi*Freq*Complex[0,1]) ]; In Vol_FW; Jacobian Vol; }
        }
      }

      { Name Hz;
        Value {
          Local { [ nu[]*CompZ[{d e}]/(2*Pi*Freq*Complex[0,1]) ]; In Vol_FW; Jacobian Vol; }
        }
      }

    }
   }
 }
