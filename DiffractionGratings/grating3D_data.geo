
DefineConstant[
  test_case = {"skewed_lattice",
    Name "1Geometry",
    Choices {"half_ellipsoid", "hole", "pyramid", "U", "bi_sinusoidal", "retrieve_2D_lamellar", "nanowire_solarcell", "convergence", "skewed_lattice"},
    GmshOption "Reset", Autocheck 0
  }
];

Include StrCat["grating3D_data_",test_case,".geo"];
