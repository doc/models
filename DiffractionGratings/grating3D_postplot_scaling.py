import numpy as np
import matplotlib.pyplot as plt
plt.figure()
temp = np.genfromtxt('temp_scaling_runtime_MUMPS.out',delimiter=' ',dtype=None,encoding=None)[:,7]
runtime_MUMPS  = np.array([np.float(temp[i][:-2]) for i in range(len(temp))])


tab_procs      = np.loadtxt('temp_scaling_nbproc.out')
tab_paramaille = np.loadtxt('temp_scaling_paramaille.out')
runtime_MUMPS = runtime_MUMPS.reshape((len(tab_paramaille),len(tab_procs)))

temp = np.genfromtxt('temp_scaling_NDOFs.out',delimiter=' ',dtype=None,encoding=None)
NDofs = [temp[k][6] for k in range(0,len(tab_procs)*len(tab_paramaille),len(tab_procs))]


plt.figure()
for k in range(len(tab_paramaille)):
    plt.plot(tab_procs,runtime_MUMPS[k,:],label='%g'%NDofs[k])
plt.legend()
plt.xlabel("nprocs")
plt.xlabel("MUMPS wall time (s)")
plt.savefig('runtime.jpg')

plt.figure()
for k in range(len(tab_paramaille)):
    plt.plot(tab_procs,runtime_MUMPS[k,0]/runtime_MUMPS[k,:],label='%g'%NDofs[k])
plt.legend(title="Matrix size")
plt.xlabel("number of procs")
plt.ylabel("speedup")
plt.savefig('speedup.jpg')
# plt.show()