rm -r res3D*
export OMP_NUM_THREADS=1
export OPENBLAS_NUM_THREADS=1

for t in bi_sinusoidal checker half_ellipsoid hole pyramid U retrieve_2D_lamellar nanowire_solarcell convergence skewed_lattice
do
    for f in 0 1
    do
        gmsh grating3D.pro -setstring test_case $t -setnumber FLAG_TOTAL $f -
        mv res3D res3D_$t"_FLAG_TOTAL_"$f
    done
done

for t in bi_sinusoidal checker half_ellipsoid hole pyramid U retrieve_2D_lamellar nanowire_solarcell convergence skewed_lattice
do
    for f in 0 1
    do
        # gmsh grating3D.geo -setstring test_case $t res3D_$t"_FLAG_TOTAL_"$f"/*.pos" &
        echo "____________\ntest case "$t" - FLAG_TOT="$f
        python grating3D_postplot.py res3D_$t"_FLAG_TOTAL_"$f
    done
done
