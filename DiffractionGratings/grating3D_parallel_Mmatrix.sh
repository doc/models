#!/bin/bash

### set threads to 1: parallelization is handled by gnuparallel (NPROC) only
export OPENBLAS_NUM_THREADS=1
export OMP_NUM_THREADS=1
export NPROC=96

### set gmsh/getdp path if necessary
# export mygmsh='/path/to/onelabdir/gmsh'
# export mygetdp='/path/to/onelabdir/getdp'
export mygmsh='gmsh'
export mygetdp='getdp'

### choose configuration
## theta loop study for the "U" case
## config 1
# export flag_angle_study="theta"
# export loop_angle_max=50
# export fixed_angle=90
# export GRATING_CASE="U"

## phi loop study for the "U" case
## config 2
export flag_angle_study="phi"
export loop_angle_max=360
export fixed_angle=50
export GRATING_CASE="U"

export nb_angle=40
export nb_lam=50
export lambda_min=400
export lambda_max=1200
export FLAG_TOTAL=0
export myDir="res_Matrix_nb_lam"$nb_lam"_nb_"$flag_angle_study$nb_angle"_total"$FLAG_TOTAL

rm -r $myDir
mkdir $myDir
$mygmsh grating3D.geo -setstring test_case $GRATING_CASE -3 -o grating3D.msh

myfunc() {
    local mysubDir=$myDir/run_lam$1_$flag_angle_study$2_psi$3
    mkdir $mysubDir
    cp grating3D.msh grating3D.pro grating3D_data_$GRATING_CASE.geo grating3D_data.geo grating3D_materials.pro $mysubDir
    local lam=$(echo "scale=10;$lambda_min+($lambda_max-$lambda_min)/($nb_lam-1)*$1" | bc )
    if [ $flag_angle_study = "phi" ]; then
        local phi=$(echo "scale=10;$loop_angle_max/($nb_angle-1)*$2" | bc )
        local theta=$(echo "scale=10;$fixed_angle" | bc )
    else
        local theta=$(echo "scale=10;$loop_angle_max/($nb_angle-1)*$2" | bc )
        local phi=$(echo "scale=10;$fixed_angle" | bc )
    fi
    local psi=$(echo "scale=10;90*$3" | bc )
    cd $mysubDir
    $mygetdp grating3D.pro -pre helmholtz_vector -msh grating3D.msh -cal -pos postop_helmholtz_vector -petsc_prealloc 200 -setstring test_case $GRATING_CASE -setnumber lambda0 $lam -setnumber thetadeg $theta -setnumber phideg $phi -setnumber psideg $psi -setnumber FLAG_TOTAL $FLAG_TOTAL
    if [ $3 -eq 0 ]; then cp res3D/rs.txt ../r_pin_sout_lam$1_$flag_angle_study$2.out ; fi
    if [ $3 -eq 0 ]; then cp res3D/rp.txt ../r_pin_pout_lam$1_$flag_angle_study$2.out ; fi
    if [ $3 -eq 1 ]; then cp res3D/rs.txt ../r_sin_sout_lam$1_$flag_angle_study$2.out ; fi
    if [ $3 -eq 1 ]; then cp res3D/rp.txt ../r_sin_pout_lam$1_$flag_angle_study$2.out ; fi
    if [ $3 -eq 0 ]; then cp res3D/ts.txt ../t_pin_sout_lam$1_$flag_angle_study$2.out ; fi
    if [ $3 -eq 0 ]; then cp res3D/tp.txt ../t_pin_pout_lam$1_$flag_angle_study$2.out ; fi
    if [ $3 -eq 1 ]; then cp res3D/ts.txt ../t_sin_sout_lam$1_$flag_angle_study$2.out ; fi
    if [ $3 -eq 1 ]; then cp res3D/tp.txt ../t_sin_pout_lam$1_$flag_angle_study$2.out ; fi
    cp res3D/eff_t1.txt ../eff_t1_lam$1_$flag_angle_study$2_psi$3.out
    cp res3D/eff_r1.txt ../eff_r1_lam$1_$flag_angle_study$2_psi$3.out
    cp res3D/eff_t2.txt ../eff_t2_lam$1_$flag_angle_study$2_psi$3.out
    cp res3D/eff_r2.txt ../eff_r2_lam$1_$flag_angle_study$2_psi$3.out
    cp res3D/temp-Q_scat.txt ../Q_scat_lam$1_$flag_angle_study$2_psi$3.out
    cd ../..
    rm -r $mysubDir
}

export -f myfunc
parallel -j $NPROC myfunc ::: $(seq 0 $(($nb_lam-1))) ::: $(seq 0 $(($nb_angle-1))) ::: 0 1

python grating3D_postplot_Mmatrix.py -flag_angle_study $flag_angle_study -nb_angle $nb_angle -loop_angle_max $loop_angle_max -fixed_angle $fixed_angle -nb_lam $nb_lam -lambda_min $lambda_min -lambda_max $lambda_max
