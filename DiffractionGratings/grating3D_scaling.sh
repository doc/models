#!/bin/bash
export OPENBLAS_NUM_THREADS=1
export OMP_NUM_THREADS=1
export GRATING_CASE="halfellipsoid"

rm -rf res3D temp_scaling_nbproc.out temp_scaling_paramaille.out temp_scaling_runtime_MUMPS.out rm temp.out

for p in 5 10 15 20 25 30 35 40
do
    echo $p >> temp_scaling_paramaille.out
    gmsh grating3D.geo -3 -o grating3D.msh -setstring test_case $GRATING_CASE -setnumber paramaille $p
    for i in 1 2 4 6 8 10 12 14 16 20 24 28 32 40 44
    do
        if [ "$p" -eq "1" ]; then echo $i >> temp_scaling_nbproc.out ; fi
        export OPENBLAS_NUM_THREADS=$i
        getdp grating3D.pro -pre helmholtz_vector -msh grating3D.msh -cal -pos postop_helmholtz_vector -petsc_prealloc 200 -setstring test_case $GRATING_CASE
    done
done
grep 'N:' nohup.out > temp_scaling_NDOFs.out 
grep -A1 'N:' nohup.out > temp.out && grep 'Wall' temp.out > temp_scaling_runtime_MUMPS.out
rm temp.out
