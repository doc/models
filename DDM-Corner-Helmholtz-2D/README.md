# ABC and Transmission CondtionCorner

## Requierement

Two open-source software are requiered to be installed:

- [GMSH](https://gmsh.info), *latest automatic Gmsh snapshot* (Version > 4.5.6 (Not the 4.5.6))
- [GetDP](http://getdp.info/) : Version >=  3.3.0

GMSH is a mesh generator with post-processing facilities while GetDP is a finite element solver.

## How to use

Launch the script `main.pro` with GMSH, either through the GUI (Graphical User Interface) or from CLI (Command Line Interface):
```bash
gmsh main.pro
```

On the left-sided menu, choose the `Problem` ("Absorbing BC" or "Transmission Condition"), set the `Resolution` and the different parameters in the `Input` sub-menu and finally click on `Run`. A consol log can be show by clicking on the bottom line of the windows on the left part.

> See the wiki for an animated summary!

## Parameters and Options

### ABC

The script produces three functions:
- `u`: Scattered field (not the total field!)
- `uNorm`: Absolute value of the scattered field
- `err`: Error (difference) between `u` and an analytic solution computed thanks to Mie Serie decomposition


![Comparaison of result with `DDM-1` algorithm, with 2 conditions at corners: homog. neumann and our corner correction.](img/ABC.png)

Here are the main options that can be tuned by the user:

- `ABC`: Order of the condition (0 or 2) and the type of corner condition (Homogeneous Neumann or with our "corner correction")
- `DDM`: Number of subdomain: 2,3 or 6 (if launched with a DD-Algorithm). The monodomain problem can  be launched independently of the geometry (see section `GetDP`). The `beta` parameter cannot be changed.
- `Geometry`: the radii of the ABC and obstacle and the position of the obstacle.
- `GetDP`: The `Resolution` value:
  - `MonoDomain`: Direct solver / Mono domain problem (every sub-domains are merged)
  - `DDM-1`: First algorithm presented in the paper: auxiliary function `phi` is global
  - `DDM-2`: Second algorithm where `phi` is local to each subdomain
- `GMSH`: nothing
- `Input` : `Incident angle` of the plane wave and `Wavenumber`
- `IterativeSolver`
  - `Solver`: `Jacobi` ("Parallel Schwarz") or `gmres`. The `print` is only for debugging purpose and might not work on your configuration.
  - `Tolerance`, `Max it` and `Restart` (for GMRES) are classical parameter
- `Mesh`: `NLambda` is the number of discretization points per wavelength
- `Output`: `Output Directory` of the results

## Transmission Condition

The script returns the total field `u` in the waveguide.

![The transmission condition problem: a waveguide divided in 2 subdomains with a possibly broken line as a border.](img/TC.png)

Here are the available options:

- `GetDP`: The `Resolution` value:
  - `MonoDomain`: Direct solver / Mono domain problem (every sub-domains are merged)
  - `DDM`: Domain Decomposition Algorithm
- `GMSH`: nothing
- `DDM`: 
  - `Order 2` (continuous auxiliary function), `Homog. Neumann` or (our) `Corner Correction`
  - `Corner Condition`: `Dirichlet` (continuous auxiliary function), `Homog. Neumann` or (our) `Corner Correction`
- `Geometry`: 
  - `X-width` and `Y-width`: resp. X-length and Y-length of the waveguide
  - `Type of border line`: `Broken Line` or `Straight Line`
  - `X-coord of the pick point`: Move the middle (peak) point on the x-line
  - `Y-coord of bottom point`: Move the bottom point on the y-line. The top point is moved symmetrically
- `Boundary Conditions`:
  - `Incoming (left)`: the boundary condition of the left side of the square, where the incoming wave is sent. It can be either `Fourier` (dn u + iwu) or `Dirichlet`.
  - `Outgoing (right, =0)`: either `Fourier` (dn u + iwu), `Neumann` or `Dirichlet`. The condition is homogeneous (=0) and set on the right side of the square.
  - `Top (=0)`: either `Neumann` or `Dirichlet`, homogeneous in both case (=0), for the top side of the square.
  - `Bottom (=0)`: either `Neumann` or `Dirichlet`, homogeneous in both case (=0), for the bottom side of the square.
- `Input`: 
  - `wavenumber`
  - `Type of incident wave`: `Plane wave` (exp^{i*k*(alpha*x)}, alpha = `Incident angle`) or Fourier mode (with m = `Mode number`)
- `IterativeSolver`
  - `Solver`: `Jacobi` ("Parallel Schwarz") or `gmres`. The `print` is only for debugging purpose and might not work on your configuration.
  - `Tolerance`, `Max it` and `Restart` (GMRES only) are classical parameter
- `Mesh`: `NLambda` is the number of discretization points per wavelength. The quantity `h` is the diameter of an element.
- `Output`: 
  - `Prefix for filename`: prefix applied to every saved file
  - `Output Directory` of the results
  - `Print every phi`: print on disk every auxiliary functions

# Authors

Bruno Després, Anouk Nicolopoulos, Bertrand Thierry
# License

GNU General Public License v3.0 or later

See COPYING to see the full text.