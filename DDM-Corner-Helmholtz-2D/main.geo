WorkingDir = CurrentDir;
Include "main.data";

If(!StrCmp(PROBLEM, "Absorbing BC"))
  Include "src/ABC_main.data";
  Include "src/ABC_main.geo";
Else
  Include "src/TC_main.data";
  Include "src/TC_main.geo";
EndIf
