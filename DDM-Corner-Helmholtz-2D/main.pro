WorkingDir = CurrentDir;
Include "main.data";

Include "src/Common.pro";

If(!StrCmp(PROBLEM,"Absorbing BC"))
  Include "src/ABC_main.data";
  Include "src/ABC_main.pro";
Else
  Include "src/TC_main.data";
  Include "src/TC_main.pro";
EndIf


