import sys
import os, os.path
import numpy as np
import subprocess
import matplotlib.pyplot as plt

def sumNorm(foldername, rootname):
  # Find number of files
  nfile = 0
  for name in os.listdir(foldername):
    if os.path.isfile(os.path.join(foldername, name)):
      if rootname in name:
        nfile += 1
  print("Number of files : " + str(nfile))

  normL2 =[]
  for n in range(nfile):
    norm = 0.
    f = open(os.path.join(foldername, rootname +str(n)), "r")
    for line in f:
      if(len(line) != 1):
        values = line.split()
        norm += float(values[1])
    f.close()
    normL2.append(np.sqrt(norm))

  print(normL2)
  return(normL2)

foldername = "out_ABC/"
rootname = "ABC_DDM_normg.pos"
NDom = 6
MaxIt = 100

subprocess.run(["rm", "-rf", foldername])
subprocess.run(["gmsh", "ABC_main.geo", "-2"])
subprocess.run(["getdp", "main.pro", "-solve", "DDM_1",
                        "-v", "3",
                        "-setstring", "PROBLEM", "Absorbing BC",
                        "-setstring", "SOLVER", "jacobi",
                        "-setstring", "ABC_DIRREL", foldername,
                        "-setnumber", "MAXIT", str(MaxIt),
                        "-setnumber", "Ndom", str(NDom)])

errorL2 = sumNorm(foldername, rootname)

plt.plot(errorL2)
plt.show()