Function{
  myD = {} ; // the domains that I (as a MPI process) am in charge of

  ListOfFields_g = {};
  ListOfConnectedFields_g = {};

// myD~{idom} : list of neightbor domain
  For idom In {0:(NDom-1)}
    myD~{idom} = {};
    If (idom % MPI_Size == MPI_Rank)
      myD() += D(idom); // D(idom) = index of subdomain (indexing could be different than 0,1,2,...)
      myD~{idom} += D~{idom}();
    EndIf
  EndFor

// Auxiliary functions
  For ii In {0:#myD()-1}
    i = myD(ii);

    For jj In {0:#myD~{i}()-1}
      j = myD~{i}(jj);

      For r In {0:(NDelta~{i}~{j}-1)}
        rj = NDelta~{i}~{j}-1-r;

        tag_g~{j}~{i}~{r} = 100 + j * 30 + r + i;
        tag_g~{i}~{j}~{rj} = 100 + i * 30 + rj + j;
        
        // Fields received / Needed
        ListOfFields_g() += tag_g~{i}~{j}~{rj};
        g~{i}~{j}~{rj}[ Delta~{j}~{i}~{rj} ] = ComplexScalarField[XYZ[]]{ tag_g~{i}~{j}~{rj} };

        // Fields sent
        ListOfConnectedFields_g() += 1;
        ListOfConnectedFields_g() += tag_g~{j}~{i}~{r};

        // Phi functions before jacobi
        tag_phiInit~{i}~{j}~{r} = 10000 + i * 30 + rj + j;
        phiInit~{i}~{j}~{r}[ Delta~{i}~{j}~{r} ] = ComplexScalarField[XYZ[]]{ tag_phiInit~{i}~{j}~{r} };
      EndFor
    EndFor
  EndFor

  //MPI_Printf["ListOfFields = ", ListOfFields_g(),ListOfFields_h()];
  //MPI_Printf["ListOfConnectedFields = ", ListOfConnectedFields_g(),ListOfConnectedFields_h()];
}
