Function{
  //Angles
  For ii In {0:#myD()-1}
    i = myD(ii);
    theta~{i}[] = ScalarField[XYZ[]]{i};
  EndFor
}

Group {
  TrGammaIncoming = ElementsOf[Omega, OnOneSideOf GammaIncoming];
}

Constraint {
  For ii In {0:#myD()-1}
    i = myD(ii);
    {Name DirichletIncoming~{i}; Type Assign; Case{{Region GammaIncoming~{i}; Value $PhysicalSource?uInc[]:0.; } } }
    {Name DirichletTop~{i}; Type Assign; Case{{Region GammaTop~{i}; Value 0.; } } }
    {Name DirichletBottom~{i}; Type Assign; Case{{Region GammaBottom~{i}; Value 0.; } } }
    {Name DirichletOutgoing~{i}; Type Assign; Case{{Region GammaOutgoing~{i}; Value 0. ;} } }
  EndFor
}

FunctionSpace{
  For ii In {0:#myD()-1}
    i = myD(ii);
    {Name H_grad~{i}; Type Form0;
      BasisFunction{
        {Name u~{i}; NameOfCoef ui; Function BF_Node;
          Support Region[{Omega~{i}, GammaOutgoing~{i}, GammaIncoming~{i}, GammaTop~{i}, GammaBottom~{i}, Delta~{i}}]; Entity NodesOf[All];}
        }
      // Incoming wave if enforced as Dirichlet condition
      If(StrCmp(INCOMING_CONDITION, "Dirichlet") == 0)
        Constraint{
          {NameOfCoef ui; EntityType NodesOf; NameOfConstraint DirichletIncoming~{i};}
        }
      EndIf
      If(StrCmp(TOP_CONDITION, "Dirichlet") == 0)
        Constraint{
          {NameOfCoef ui; EntityType NodesOf; NameOfConstraint DirichletTop~{i};}
        }
      EndIf
      If(StrCmp(BOTTOM_CONDITION, "Dirichlet") == 0)
        Constraint{
          {NameOfCoef ui; EntityType NodesOf; NameOfConstraint DirichletBottom~{i};}
        }
      EndIf
      If(StrCmp(OUTGOING_CONDITION, "Dirichlet") == 0)
        Constraint{
          {NameOfCoef ui; EntityType NodesOf; NameOfConstraint DirichletOutgoing~{i};}
        }
      EndIf
    }

    For jj In {0:#myD~{i}()-1}
      j = myD~{i}(jj);
      For r In {0:(NDelta~{i}~{j}-1)}
        {Name H_g~{j}~{i}~{r}; Type Form0;
          BasisFunction{
            {Name g~{j}~{i}~{r}; NameOfCoef gj; Function BF_Node;
              Support Region[{Delta~{i}~{j}~{r}}];Entity NodesOf[All];}
          }
        }
        {Name Hphi_grad~{i}~{j}~{r}; Type Form0;
          BasisFunction{
            {Name phi~{i}~{j}~{r}; NameOfCoef phiij; Function BF_Node;
              Support Region[{Delta~{i}~{j}~{r}, dDelta~{i}~{j}~{r}}];Entity NodesOf[All];}
          }
        }
        {Name Hpsi_grad~{i}~{j}~{r}; Type Form0;
          BasisFunction{
            {Name psi~{i}~{j}~{r}; NameOfCoef psiji; Function BF_Node;
              Support Region[{Delta~{i}~{j}~{r}, dDelta~{i}~{j}~{r}}];Entity NodesOf[All];}
          }
        }
      EndFor
    EndFor
  EndFor
}

Formulation {
  { Name DDM; Type FemEquation;
    Quantity{
      { Name u ; Type Local; NameOfSpace H_grad;}
			For ii In {0:#myD()-1}
				i = myD(ii);
        { Name u~{i} ; Type Local; NameOfSpace H_grad~{i};}
        For jj In {0:#myD~{i}()-1}
          j = myD~{i}(jj);
          For r In {0:(NDelta~{i}~{j}-1)}
            { Name phi~{i}~{j}~{r} ; Type Local; NameOfSpace Hphi_grad~{i}~{j}~{r};}
            { Name psi~{i}~{j}~{r} ; Type Local; NameOfSpace Hpsi_grad~{i}~{j}~{r};}
          EndFor
        EndFor
     	EndFor
    }
    Equation{
      For ii In {0:#myD()-1}
		  i = myD(ii);
		  //copy u~{i} in u
      Galerkin{[Dof{u}, {u}];
      In Omega~{i}; Jacobian JVol; Integration I1;}
      Galerkin{[-Dof{u~{i}}, {u}];
      In Omega~{i}; Jacobian JVol; Integration I1;}

      //Helmholtz equation
      Galerkin{[Dof{Grad u~{i}}, {Grad u~{i}}];
      In Omega~{i}; Jacobian JVol; Integration I1;}
      Galerkin{[-kap^2*Dof{u~{i}}, {u~{i}}];
      In Omega~{i}; Jacobian JVol; Integration I1;}
      Galerkin{[-I[]*kap*Dof{u~{i}}, {u~{i}}];
      In GammaOutgoing~{i}; Jacobian JSur; Integration I1;}

      // Incoming wave is enforced through a Fourier-type condition
      If(StrCmp(INCOMING_CONDITION, "Fourier") == 0)
        Galerkin{[-I[]*kap*Dof{u~{i}}, {u~{i}}];
          In GammaIncoming~{i}; Jacobian JSur; Integration I1;}
        Galerkin{[$PhysicalSource?-(-dx_uInc[]-I[]*kap*uInc[]):0, {u~{i}}];
          In GammaIncoming~{i}; Jacobian JSur; Integration I1;}
      EndIf

      //Transmission condition
      For jj In {0:#myD~{i}()-1}
        j = myD~{i}(jj);
        For r In {0:(NDelta~{i}~{j}-1)}
          rj = (NDelta~{i}~{j}-1) - r; // Index from the other side
          Galerkin{[-I[]*kap*Dof{phi~{i}~{j}~{r}}, {u~{i}}];
          In Delta~{i}~{j}~{r}; Jacobian JSur; Integration I1;}
          Galerkin{[$TCSource?-g~{i}~{j}~{rj}[]:0., {u~{i}}];
          In Delta~{i}~{j}~{r}; Jacobian JSur; Integration I1;}

          //Rij(ui)
          Galerkin{[Dof{phi~{i}~{j}~{r}}, {phi~{i}~{j}~{r}}];
          In Delta~{i}~{j}~{r}; Jacobian JSur; Integration I1;}
          Galerkin{[-Dof{u~{i}}, {phi~{i}~{j}~{r}}];
          In Delta~{i}~{j}~{r}; Jacobian JSur; Integration I1;}

          //Rji(ui)
          Galerkin{[Dof{psi~{i}~{j}~{r}}, {psi~{i}~{j}~{r}}];
            In Delta~{i}~{j}~{r}; Jacobian JSur; Integration I1;}
          Galerkin{[-Dof{u~{i}}, {psi~{i}~{j}~{r}}];
            In Delta~{i}~{j}~{r}; Jacobian JSur; Integration I1;}

          If (TC_ORDER==2)
            //Rij(ui)
            Galerkin{[1/(2*kap*kap)*Dof{d phi~{i}~{j}~{r}}, {d phi~{i}~{j}~{r}}];
            In Delta~{i}~{j}~{r}; Jacobian JSur; Integration I1;}

            //Rji(ui)
            Galerkin{[1/(2*kap*kap)*Dof{d psi~{i}~{j}~{r}}, {d psi~{i}~{j}~{r}}];
            In Delta~{i}~{j}~{r}; Jacobian JSur; Integration I1;}
            
            If(CORNER_CONDITION == 2)
              //Loop on Corner points (possibly empty list!)
              For index In {0:#ListdDelta~{i}~{j}~{r}()-1}
                s = ListdDelta~{i}~{j}~{r}(index);
                //Rij(ui)
                Galerkin{[-I[]/(4*kap)*(Cos[theta~{i}[]/2] + Cos[theta~{i}[]]/Cos[theta~{i}[]/2])*Dof{phi~{i}~{j}~{r}}, {phi~{i}~{j}~{r}}];
                  In Q~{i}~{j}~{r}~{s}; Jacobian JLin; Integration I1;}

                Galerkin{[-I[]/(4*kap)*(Cos[theta~{i}[]/2] - Cos[theta~{i}[]]/Cos[theta~{i}[]/2])*Dof{phi~{i}~{j}~{s}}, {phi~{i}~{j}~{r}}];
                  In Q~{i}~{j}~{r}~{s}; Jacobian JLin; Integration I1;}

                //Rji(ui) (same but conjugated coefficient)
                Galerkin{[-I[]/(4*kap)*(-Cos[theta~{i}[]/2] - Cos[theta~{i}[]]/Cos[theta~{i}[]/2])*Dof{psi~{i}~{j}~{r}}, {psi~{i}~{j}~{r}}];
                In Q~{i}~{j}~{r}~{s}; Jacobian JLin; Integration I1;}

                Galerkin{[-I[]/(4*kap)*(-Cos[theta~{i}[]/2] + Cos[theta~{i}[]]/Cos[theta~{i}[]/2])*Dof{psi~{i}~{j}~{s}}, {psi~{i}~{j}~{r}}];
                  In Q~{i}~{j}~{r}~{s}; Jacobian JLin; Integration I1;}
              EndFor
            EndIf
          EndIf
        EndFor
      EndFor
		EndFor
    }
  }

  //Resolution g
  For ii In {0:#myD()-1}
    i = myD(ii);
    For jj In {0:#myD~{i}()-1}
      j = myD~{i}(jj);
      For r In {0:(NDelta~{i}~{j}-1)}         
        rj = (NDelta~{i}~{j}-1) - r; // Index from the other side
      	{ Name G~{j}~{i}~{r}; Type FemEquation;
	        Quantity{
            { Name u~{i} ; Type Local; NameOfSpace H_grad~{i};}
            { Name g~{j}~{i}~{r} ; Type Local; NameOfSpace H_g~{j}~{i}~{r};}
            { Name phi~{i}~{j}~{r} ; Type Local; NameOfSpace Hphi_grad~{i}~{j}~{r};}
            { Name psi~{i}~{j}~{r} ; Type Local; NameOfSpace Hpsi_grad~{i}~{j}~{r};}
          }
			    Equation{
            Galerkin{[Dof{g~{j}~{i}~{r}},{g~{j}~{i}~{r}}];
            In Delta~{i}~{j}~{r}; Jacobian JSur; Integration I1;}

            Galerkin{[$TCSource?+g~{i}~{j}~{rj}[]:0.,{g~{j}~{i}~{r}}];
            In Delta~{j}~{i}~{rj}; Jacobian JSur; Integration I1;}

            Galerkin{[I[]*kap*{phi~{i}~{j}~{r}},{g~{j}~{i}~{r}}];
            In Delta~{i}~{j}~{r}; Jacobian JSur; Integration I1;}

            Galerkin{[I[]*kap*{psi~{i}~{j}~{r}},{g~{j}~{i}~{r}}];
            In Delta~{i}~{j}~{r}; Jacobian JSur; Integration I1;}
			    }
        }
      EndFor
    EndFor
  EndFor
}

Resolution{
  {Name DDM;
    System{
      {Name DDM; NameOfFormulation DDM; Type Complex; }
      For ii In {0:#myD()-1}
        i = myD(ii);
	      For jj In {0:#myD~{i}()-1}
          j = myD~{i}(jj);          
          For r In {0:(NDelta~{i}~{j}-1)}
	          {Name G~{j}~{i}~{r}; NameOfFormulation G~{j}~{i}~{r}; Type Complex; }
          EndFor
        EndFor
      EndFor
    }
    Operation{
      // Initialisation
      CreateDir[TC_DIR];
      //Angles between segments
      For ii In {0:#myD()-1}
        i = myD(ii);
        GmshRead[StrCat[WorkingDir, Sprintf["aux/TC_Theta_%g.pos",i+1]]];
      EndFor
      Evaluate[$PhysicalSource = 1];
      Evaluate[$TCSource = 0];
      UpdateConstraint[DDM, GammaIncoming, Assign];
      Generate[DDM];
      Solve[DDM];

      Evaluate[$ItCount = 0];
      If(PRINT_PHI)
        //Iteration 0 value
        PostOperation[Phi_Init];
      EndIf
      For ii In {0:#myD()-1}
	      i = myD(ii);
        For jj In {0:#myD~{i}()-1}
          j = myD~{i}(jj);
          For r In {0:(NDelta~{i}~{j}-1)}
	          Generate[G~{j}~{i}~{r}];
	          Solve[G~{j}~{i}~{r}];
            PostOperation[G~{j}~{i}~{r}];
          EndFor
        EndFor
      EndFor
      Evaluate[$PhysicalSource = 0];
      Evaluate[$TCSource = 1];
      If(StrCmp(INCOMING_CONDITION, "Dirichlet") == 0)
        UpdateConstraint[DDM, GammaIncoming, Assign];
        GenerateRHSGroup[DDM, Region[{TrGammaIncoming}]];
      Else
        GenerateRHSGroup[DDM, Region[{GammaIncoming}]];
      EndIf

      IterativeLinearSolver["I-A",SOLVER, TOL, MAXIT, RESTART, {ListOfFields_g()}, {ListOfConnectedFields_g()}, {}]
      {
        GenerateRHSGroup[DDM, Region[{Sigma}]];
        SolveAgain[DDM];

        Evaluate[$ItCount=$ItCount+1];
        If(PRINT_PHI)
          PostOperation[Phi];
        EndIf

        For ii In {0:#myD()-1}
          i = myD(ii);
          For jj In {0:#myD~{i}()-1}
            j = myD~{i}(jj);
            For r In {0:(NDelta~{i}~{j}-1)}
              GenerateRHSGroup[G~{j}~{i}~{r}, Region[{Delta~{i}~{j}~{r}}]];
              SolveAgain[G~{j}~{i}~{r}];
            EndFor
          EndFor
        EndFor
        For ii In {0:#myD()-1}
          i = myD(ii);
          For jj In {0:#myD~{i}()-1}
            j = myD~{i}(jj);
            For r In {0:(NDelta~{i}~{j}-1)}
              PostOperation[G~{j}~{i}~{r}];
            EndFor
          EndFor
        EndFor
  
      }
      // Compute solution
      Evaluate[$PhysicalSource = 1];
      Evaluate[$TCSource = 1];
      If(StrCmp(INCOMING_CONDITION, "Dirichlet") == 0)
        UpdateConstraint[DDM, GammaIncoming, Assign];
        GenerateRHSGroup[DDM, Region[{Delta, TrGammaIncoming}]];
      Else
        GenerateRHSGroup[DDM, Region[{Delta, GammaIncoming}]];
      EndIf
      SolveAgain[DDM];
      PostOperation[DDM];
    }
  }
}

PostProcessing{
  For ii In {0:#myD()-1}
    i = myD(ii);
    For jj In {0:#myD~{i}()-1}
      j = myD~{i}(jj);
      For r In {0:(NDelta~{i}~{j}-1)}
        {Name G~{j}~{i}~{r}; NameOfFormulation G~{j}~{i}~{r};
          Quantity {
            {Name g~{j}~{i}~{r}; Value {Local { [{g~{j}~{i}~{r}}] ; In Delta~{i}~{j}~{r}; Jacobian JSur; }}}
          }
        }
      EndFor
    EndFor
  EndFor

  {Name DDM; NameOfFormulation DDM;
    Quantity {
      {Name u_ddm; Value {
	      For ii In {0:#myD()-1}
	        i = myD(ii);
	        Local { [{u~{i}}] ; In Omega~{i}; Jacobian JVol; }
	      EndFor
	      }
      }
    }
  }

  {Name Phi_Init; NameOfFormulation DDM;
    Quantity {
	      For ii In {0:#myD()-1}
          i = myD(ii);
          For jj In {0:#myD~{i}()-1}
            j = myD~{i}(jj);
            For r In {0:(NDelta~{i}~{j}-1)}
              {Name phiInit~{i}~{j}~{r}; Value {
                Local { [{phi~{i}~{j}~{r}}] ; In Delta~{i}~{j}~{r}; Jacobian JSur; }
              }  }
            EndFor
          EndFor
	      EndFor      
    }
  }
  {Name Phi; NameOfFormulation DDM;
    Quantity {
	      For ii In {0:#myD()-1}
          i = myD(ii);
          For jj In {0:#myD~{i}()-1}
            j = myD~{i}(jj);
            For r In {0:(NDelta~{i}~{j}-1)}
              {Name phi~{i}~{j}~{r}; Value {
                Local { [{phi~{i}~{j}~{r}}+ phiInit~{i}~{j}~{r}[]] ; In Delta~{i}~{j}~{r}; Jacobian JSur; }
              }  }
            EndFor
          EndFor
	      EndFor      
    }
  }
}

PostOperation{
  For ii In {0:#myD()-1}
    i = myD(ii);
    For jj In {0:#myD~{i}()-1}
      j = myD~{i}(jj);
      For r In {0:(NDelta~{i}~{j}-1)}
        {Name G~{j}~{i}~{r}; NameOfPostProcessing G~{j}~{i}~{r};
	        Operation { Print [g~{j}~{i}~{r}, OnElementsOf Delta~{i}~{j}~{r}, StoreInField tag_g~{j}~{i}~{r}];}
        }
      EndFor
    EndFor
  EndFor

  {Name DDM; NameOfPostProcessing DDM;
    Operation {
      Print [u_ddm, OnElementsOf Omega, File StrCat[TC_DIR,StrCat[TC_PREFIX_,"u_ddm.pos"]]];
    }
  }

  {Name Phi_Init; NameOfPostProcessing Phi_Init;
    Operation {
      For ii In {0:#myD()-1}
        i = myD(ii);
        For jj In {0:#myD~{i}()-1}
          j = myD~{i}(jj);
          For r In {0:(NDelta~{i}~{j}-1)}
            Print [phiInit~{i}~{j}~{r}, OnElementsOf Delta~{i}~{j}~{r}, StoreInField tag_phiInit~{i}~{j}~{r}];
          EndFor
        EndFor
      EndFor
    }
  }
  {Name Phi; NameOfPostProcessing Phi;
    Operation {
      For ii In {0:#myD()-1}
        i = myD(ii);
        For jj In {0:#myD~{i}()-1}
          j = myD~{i}(jj);
          For r In {0:(NDelta~{i}~{j}-1)}
            Print [phi~{i}~{j}~{r}, OnElementsOf Delta~{i}~{j}~{r}, Format SimpleTable,  AppendExpressionToFileName $ItCount, File >> StrCat[TC_DIR,StrCat[TC_PREFIX_,Sprintf["phi_%g_%g_%g.pos", i,j,r]]]];
          EndFor
        EndFor
      EndFor
    }
  }
}
