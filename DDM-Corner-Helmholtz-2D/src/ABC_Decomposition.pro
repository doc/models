// Dispatch subdomain to (MPI) processes (list myD())
// Provide a unique tag to every transmitted data (e.g.: g_{i,j})
Function{
  myD = {} ; // the domains current process will treat (if MPI_NUM_PROC = 1 then myD()= {0,1,2,..., NDOM-1}
  For i In {0:(NDom-1)}
    myD~{i} = {};
    If (i % MPI_Size == MPI_Rank)
      myD() += D(i);
      myD~{i} += D~{i}();
    EndIf
  EndFor
  // Field managed by current MPI Process
  // If sequential then every field
  ListOfFields_g = {};
  ListOfConnectedFields_g = {};
  ListOfFields_h = {};
  ListOfConnectedFields_h = {};

  // Loop on subdomains
  For ii In {0:#myD()-1}
    i = myD(ii);
    //Neighbors
    For jj In {0:#myD~{i}()-1}
      j = myD~{i}(jj);

      // Quantities coming from "the other side"
      tag_g~{i}~{j} = 10000 + i*50 + j;
      ListOfFields_g() += tag_g~{i}~{j};
      g~{i}~{j}[ Sigma~{i}~{j} ] = ComplexScalarField[XYZ[]]{ tag_g~{i}~{j} };
      // Quantities to be transmitted to domain j
      tag_g~{j}~{i} = 10000 + j*50 + i;
      ListOfConnectedFields_g() += 1;
      ListOfConnectedFields_g() += tag_g~{j}~{i};

      // RHS (for jacobi)
      tag_b~{i}~{j} = 11000 + i*50 + j;
      tag_b~{j}~{i} = 11000 + j*50 + i;
      b~{i}~{j}[ Sigma~{i}~{j} ] = ComplexScalarField[XYZ[]]{ tag_b~{i}~{j} };
    EndFor
    // For DDM-2
    For kk In {0:#ListGammaExt~{i}()-1}
      k=ListGammaExt~{i}(kk);
      For index In {0:#ListdGammaExt~{i}~{k}()-1:2}
        j=ListdGammaExt~{i}~{k}(index);
        l=ListdGammaExt~{i}~{k}(index+1);
        // Quantities coming from "the other side" of Aijkl
        tag_h~{i}~{j}~{k}~{l} = 100000 + i*1000 + j*100 + 10*k + l;
        ListOfFields_h() += tag_h~{i}~{j}~{k}~{l};
        h~{i}~{j}~{k}~{l}[ Apt~{i}~{j}~{k}~{l} ] = ComplexScalarField[XYZ[]]{ tag_h~{i}~{j}~{k}~{l} };
        // Quantities to be transmitted to domain j
        tag_h~{j}~{i}~{l}~{k} = 100000 + j*1000 + i*100 + 10*l + k;
        ListOfConnectedFields_h() += 1;
        ListOfConnectedFields_h() += tag_h~{j}~{i}~{l}~{k};
      EndFor
    EndFor
  EndFor
}
