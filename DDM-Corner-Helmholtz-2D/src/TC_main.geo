//Rectangle separe en deux avec un coin

//4 sommets
Point(1) = {L, l/2, 0,h};
Point(2) = {0, l/2, 0,h};
Point(3) = {0, -l/2,0,h};
Point(4) = {L, -l/2,0,h};

dx_geo = TC_BROKEN_LINE?dx:0;
dy_geo = TC_BROKEN_LINE?dy:l/2-1;
//5 points interface
Point(11) = {L/2,       l/2,   0,h};
Point(12) = {L/2,       dy_geo,0,h};
Point(13) = {L/2+dx_geo,0,     0,h}; //pointe
Point(14) = {L/2,-dy_geo,      0,h};
Point(15) = {L/2,-l/2,         0,h};
// Point{13} In Surface{1}; //force pointe a etre noeud du maillage

Line(2) = {2,3}; //segments bords rectangle
Line(4) = {4,1}; //segments bords rectangle

Line(21) = {11,2}; // Gamma Top inter. Omega_1
Line(24) = {3,15}; // Gamma Bottom inter. Omega_1

Line(22) = {1,11}; // Gamma Top inter. Omega_2
Line(25) = {15,4}; // Gamma Bottom inter. Omega_2

Line(30) = {11,12}; //interface Sigma
Line(31) = {12,13}; 
Line(32) = {13,14}; 
Line(33) = {14,15}; 


Curve Loop(1) = {-33,-32,-31,-30,21,2,24}; //bord Omega1
Plane Surface(1) = {1}; //Omega1

Curve Loop(2) = {4,22,30,31,32,33,25}; //bord Omega2
Plane Surface(2) = {2}; //Omega2

Physical Surface(0) = {1}; //Omega1
Physical Surface(1) = {2}; //Omega2

Physical Curve(31) = {2}; // Left Boundary (incoming) 
Physical Curve(33) = {4}; // Right Boundary (outcoming)

Physical Curve(100) = {21}; // Gamma_Top on Omega_1 
Physical Curve(101) = {22}; // Gamma_Top on Omega_2

Physical Curve(200) = {24}; // Gamma_Bottom on Omega_1
Physical Curve(201) = {25}; // Gamma_Bottom on Omega_2

For i In {0:3}
	Physical Point(40+i) = {i+1}; //noeuds Gamma 
	Physical Point(50+i) = {10+i+1}; //noeuds Sigma
	Physical Curve(20+i) = {30+i}; //interface Sigma
EndFor
Physical Point(54) = {15};


// Due to TC_main.geo particular case, Mesh mush not be automatically computed
Printf(OnelabAction);
Solver.AutoMesh = -1; // the .geo file creates the mesh
If(StrCmp(OnelabAction, "compute") == 0)
	Mesh 2;
	Save StrCat[WorkingDir, "main.msh"];
	CreateDir StrCat[WorkingDir, "aux"];
	Plugin(BoundaryAngles).Remove = 1;
	Plugin(BoundaryAngles).Save = 1;
	Plugin(BoundaryAngles).Dir = "aux";
	Plugin(BoundaryAngles).Filename = "TC_Theta";
	Plugin(BoundaryAngles).Run;
EndIf
