//Hexagone regulier et point source/obstacle disque
Mesh.CharacteristicLengthMin = h;
Mesh.CharacteristicLengthMax = h;

Point(10) = {xs,ys,0,h}; //Center of the disk and hexagone
For i In {0:5}
	Point(i+1) = {r*Cos[i*Pi/3],r*Sin[i*Pi/3],0,h}; //6 points hexagone
	Point(10+i+1) = {xs+rs*Cos[i*Pi/3],ys+rs*Sin[i*Pi/3],0,h}; //6 points disk (obstacle)
EndFor
For i In {1:6}
	Line(i) = {i,(i%6)+1}; //segments of the hexagone
	Circle(10+i) = {10+i,10,10+(i%6)+1}; //circle arc of the disk
EndFor

// Segments of the hexagone (numbering does not depend on the decomposition)
For k In {0:(NGammaExt-1)}
	Physical Curve(30+k) = {k+1}; //segments Gamma k ext
	Physical Point(40+k) = {k+1}; //noeud k-1,k
	Physical Point(50+k) = {10+k+1}; //Noeuds Gamma Int
EndFor

// Numbering depends on the number of subdomains
If (NDom==1)
	Curve Loop(1) = {1:6}; //hexagone
	Curve Loop(2) = {11:16}; // disk
	Plane Surface(1) = {1,2}; // Hexagone minus disk (=propagation domain)

	Physical Surface(0) = {1}; //Propagation domaine
	Physical Curve(10) = {11:16}; // Boundary of disk (Gamma Int)
EndIf

// With 2 subdomaines, Omega_0 and Omega_1 share two boundaries in common
If (NDom==2)
	Line(21) = {1,11};
	Line(22) = {4,14};
	Curve Loop(1) = {1,2,3,22,-13,-12,-11,-21}; //Boundary of Omega 0
	Plane Surface(1) = {1}; // Omega 0
	Curve Loop(2) = {21,-16,-15,-14,-22,4,5,6}; //Boundary of Omega 1
	Plane Surface(2) = {2}; //Omega 1

	Physical Surface(0) = {1}; //Omega 0
	Physical Surface(1) = {2}; //Omega 1

	Physical Curve(10) = {11,12,13}; //Gamma Int 0
	Physical Curve(11) = {14,15,16}; //Gamma Int 1

	Physical Curve(20) = {21,22}; //Sigma_{0,1}
EndIf

If (NDom==3)
	For i In {0:(NDom-1)}
		Line(21+i) = {1+i*(NGammaExt/NDom),11+i*(NGammaExt/NDom)};
	EndFor

	Curve Loop(1) = {1,2,22,-12,-11,-21}; // Boundary of Omega 0
	Plane Surface(1) = {1}; //Omega 0

	Curve Loop(2) = {3,4,23,-14,-13,-22};
	Plane Surface(2) = {2};

	Curve Loop(3) = {21,-16,-15,-23,5,6};
	Plane Surface(3) = {3};

	For i In {0:(NDom-1)}
		Physical Surface(i) = {i+1}; //Omega_i
		Physical Curve(20+i) = {20+i+1}; //Sigma_{i, (i-1)}
		Physical Curve(10+i) = {11 + 2*i, 12 + 2*i}; //Gamma Int of Omega_i
	EndFor
EndIf

If (NDom==6)
	For j In {1:NDom}
		Line(20+j) = {j,10+j}; // Sigma
	EndFor

	For i In {1:(NDom-1)}
		Curve Loop(i) = {i,20+i+1,-10-i,-20-i}; //Boundary Omega_i
		Plane Surface(i) = {i}; //Omega_i
	EndFor
	Curve Loop(6) = {6,21,-16,-26}; //Boundary Omega_5
	Plane Surface(6) = {6}; //Omega_5

	For i In {0:(NDom-1)}
		Physical Surface(i) = {i+1}; //Omega_i
		Physical Curve(10+i) = {10+i+1}; //Gamma Interior of Omega_i
		Physical Curve(20+i) = {20+i+1}; //Sigma_{i-1, i}		
	EndFor
EndIf

Solver.AutoMesh = 2; // auto mesh
