Constraint {
  {Name DirichletIncoming; Type Assign; Case{{Region GammaIncoming; Value uInc[]; } } }
  {Name DirichletOutgoing; Type Assign; Case{{Region GammaOutgoing; Value 0.; } } }
  {Name DirichletTop; Type Assign; Case{{Region GammaTop; Value 0.; } } }
  {Name DirichletBottom; Type Assign; Case{{Region GammaBottom; Value 0.; } } }
}

FunctionSpace{
  {Name H_grad; Type Form0;
    BasisFunction{
      {Name u; NameOfCoef ui; Function BF_Node;
    	  Support Region[{Omega, GammaIncoming, GammaOutgoing, GammaTop, GammaBottom}]; Entity NodesOf[All];}
      }
    If(StrCmp(INCOMING_CONDITION, "Dirichlet") == 0)
      Constraint{
        {NameOfCoef ui; EntityType NodesOf; NameOfConstraint DirichletIncoming;}
      }
    EndIf
    If(StrCmp(OUTGOING_CONDITION, "Dirichlet") == 0)
      Constraint{
        {NameOfCoef ui; EntityType NodesOf; NameOfConstraint DirichletOutgoing;}
      }
    EndIf
    If(StrCmp(TOP_CONDITION, "Dirichlet") == 0)
      Constraint{
        {NameOfCoef ui; EntityType NodesOf; NameOfConstraint DirichletTop;}
      }
    EndIf
    If(StrCmp(BOTTOM_CONDITION, "Dirichlet") == 0)
      Constraint{
        {NameOfCoef ui; EntityType NodesOf; NameOfConstraint DirichletBottom;}
      }
    EndIf
  }
}

Formulation {
  { Name MonoDomain; Type FemEquation;
    Quantity{
      { Name u ; Type Local; NameOfSpace H_grad;}
    }
    Equation{
      //Helmholtz equation
      Galerkin{[Dof{Grad u}, {Grad u}];
	      In Omega; Jacobian JVol; Integration I1;}
      Galerkin{[-kap^2*Dof{u}, {u}];
	      In Omega; Jacobian JVol; Integration I1;}
	    //Sommerfeld radiation condition (approx.)
      If(StrCmp(OUTGOING_CONDITION, "Fourier") == 0)
        Galerkin{[-I[]*kap*Dof{u}, {u}];
          In GammaOutgoing; Jacobian JSur; Integration I1;}
      EndIf
      // Incoming wave is enforced through a Fourier-type condition
      If(StrCmp(INCOMING_CONDITION, "Fourier") == 0)
        Galerkin{[-I[]*kap*Dof{u}, {u}];
          In GammaIncoming; Jacobian JSur; Integration I1;}
        Galerkin{[-(-dx_uInc[]-I[]*kap*uInc[]), {u}];
          In GammaIncoming; Jacobian JSur; Integration I1;}
      EndIf
    }
  }
}

Resolution{
  {Name MonoDomain;
    System{ {Name A; NameOfFormulation MonoDomain; Type Complex; } }
    Operation{
      CreateDir[TC_DIR];
      Generate[A];
      Solve[A];
      PostOperation[MonoDomain];
    }
  }
}

PostProcessing{
  {Name MonoDomain; NameOfFormulation MonoDomain;
    Quantity {
      {Name u; Value {Local { [{u}] ; In Omega; Jacobian JVol; }}}
    }
  }
}

PostOperation{
  {Name MonoDomain; NameOfPostProcessing MonoDomain;
    Operation {
      Print [u, OnElementsOf Omega, File StrCat[TC_DIR, StrCat[TC_PREFIX_,"u_mono.pos"]]];
    }
  }
}
