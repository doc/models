// DDM-algorithms
// Common quantities and operations are defined here
// DDM algorithm are included below 

// Numbering of the unknowns of the DD algorithms
Include "ABC_Decomposition.pro";

Group {
  For ii In {0:#myD()-1}
    i = myD(ii);
    TrdOmegaInt~{i} = ElementsOf[Omega~{i}, OnOneSideOf dOmegaInt~{i}];
  EndFor
}

Constraint {
  For ii In {0:#myD()-1}
    i = myD(ii);
    {Name Dirichlet~{i}; Type Assign; Case{{Region dOmegaInt~{i}; Value $PhysicalSource?-uInc[]:0.; } } }
  EndFor
}

FunctionSpace{
  For ii In {0:#myD()-1}
    i = myD(ii);
    {Name H_grad~{i}; Type Form0;
      BasisFunction{
        {Name u~{i}; NameOfCoef ui; Function BF_Node;
      	Support Region[{Omega~{i}, dOmegaExt~{i}, dOmegaInt~{i}, Sigma~{i}}]; Entity NodesOf[All];}
        }
      Constraint{
        {NameOfCoef ui; EntityType NodesOf; NameOfConstraint Dirichlet~{i};}
      }
    }

    For jj In {0:#myD~{i}()-1}
      j = myD~{i}(jj);
      {Name H_g~{i}~{j}; Type Form0;
        BasisFunction{
        {Name g~{i}~{j}; NameOfCoef gj; Function BF_Node;
	        Support Region[{Sigma~{i}~{j}}];Entity NodesOf[Sigma~{i}~{j}];}
        }
      }
    EndFor
  EndFor
}

// ============ DDM Algorithms
Include "ABC_DDM_1.pro";
Include "ABC_DDM_2.pro";
// ===============

// Common Post-Processings to every DDM algorithms
// ===============================================
PostProcessing{
  // Transmitted Data
  For ii In {0:#myD()-1}
    i = myD(ii);
    For jj In {0:#myD~{i}()-1}
      j = myD~{i}(jj);
      {Name G~{j}~{i}; NameOfFormulation G~{j}~{i};
        Quantity {
          {Name g~{j}~{i}; Value {Local { [{g~{j}~{i}}] ; In Sigma~{i}~{j}; Jacobian JSur; }}}
        }
      }
      {Name NormL2G~{j}~{i}; NameOfFormulation G~{j}~{i};
        Quantity {
          {Name normL2g~{j}~{i}; Value { Integral { [ Abs[g~{j}~{i}[] - {g~{j}~{i}} - b~{j}~{i}[]]^2 ]; In Sigma~{i}~{j}; Jacobian JSur; Integration I1; }}}
        }
      }
    EndFor
  EndFor
  // Local solutions (whatever the algorithm)
  {Name DDM; NameOfFormulation DDM_1;
    Quantity {
      {Name u_ddm; Value {
    	  For ii In {0:#myD()-1}
	        i = myD(ii);
	        Local { [{u~{i}}] ; In Omega~{i}; Jacobian JVol; }
	      EndFor
    	  }
      }
      {Name u_ddm_abs; Value {
	      For ii In {0:#myD()-1}
	        i = myD(ii);
	        Local { [Norm[{u~{i}}]] ; In Omega~{i}; Jacobian JVol; }
	      EndFor
      	}
      }
      //Error between solution and exact solution (exact in free space!)
      {Name uerr_ddm; Value {
	      For ii In {0:#myD()-1}
	        i = myD(ii);
	        Local { [Norm[{u~{i}} - uex[]]] ; In Omega~{i}; Jacobian JVol; }
	      EndFor
    	  }
      }
    }
  }
}

// Common Post-Operations to every DDM-algorithms
// ===============================================
PostOperation{
  // Compute the transmitted data + Store it in memory
  For ii In {0:#myD()-1}
    i = myD(ii);
    For jj In {0:#myD~{i}()-1}
      j = myD~{i}(jj);
      {Name G~{j}~{i}; NameOfPostProcessing G~{j}~{i};
      	Operation { Print [g~{j}~{i}, OnElementsOf Sigma~{i}~{j}, StoreInField tag_g~{j}~{i}];}
      }
      //Store RHS to compute L2 error (Jacobi)
      {Name B~{j}~{i}; NameOfPostProcessing G~{j}~{i};
      	Operation { Print [g~{j}~{i}, OnElementsOf Sigma~{i}~{j}, StoreInField tag_b~{j}~{i}];}
      }
      {Name NormL2G~{j}~{i}; NameOfPostProcessing NormL2G~{j}~{i};
        Operation{
          Print [normL2g~{j}~{i}[Sigma~{i}~{j}], OnRegion Sigma~{i}~{j}, Format Table, AppendExpressionToFileName $ItCount, File >> StrCat[ABC_DIR,"ABC_DDM_normg.pos"]];
        }
      }
    EndFor
  EndFor
  // Print the solution on HDD + error wrt exact solution
  {Name DDM; NameOfPostProcessing DDM;
    Operation {
      Print [u_ddm, OnElementsOf Omega, File StrCat[ABC_DIR, StrCat[ABC_PREFIX, "DDM_Scattered_Field.pos"]]];
      Print [u_ddm_abs, OnElementsOf Omega, File StrCat[ABC_DIR, StrCat[ABC_PREFIX, "DDM_Scattered_Field_(abs._value).pos"]]];
      Print [uerr_ddm, OnElementsOf Omega, File StrCat[ABC_DIR, StrCat[ABC_PREFIX, "DDM_Error_numeric_vs_analytic.pos"]]];
    }
  }
}
