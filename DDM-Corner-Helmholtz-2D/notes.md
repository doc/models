# Notations

## GetDP Group (`hexa.pro`)

Les indices `i` et `j` sont réservés aux sous domaines tandis que `k` et `l` le sont pour les segments du bord extérieur.

### Numbers

| Nom| Fonction|
| ----- | ----- |
|`NGammaExt` |Number of segments of exterior boundary $\Gamma$ |
|`NGammaExt~{i}` |Number of segments of $\Gamma\cap\partial\Omega_i$ |
|`NDom` |  Number of subdomains|

### Indices

| Nom| Fonction|
| ----- | ----- |
|`Omega~{i}` | Domaine $\Omega_i$|
|`GammaExt` | Bord $\Gamma$ en entier|
|`GammaExt~{k}` | Segment $\Gamma_k$|
| `GammaExt~{i}~{k}`| Segment $\Gamma_k^i$|
|`GammaInt` | Bord intérieur (obstacle) en entier|
|`GammaInt~{i}` | `GammaInt`$\cap\partial\Omega_i$|
| `Apt~{i}~{j}~{k}~{l}` | Point $A_{k\ell}^{ij}$ |
| `dGammaExt~{i}~{k}~{0}` | Same as one of the `Apt~{i}~{j}~{k}~{l}` but listed differently |
| `Cset~{i}~{k}` | End-points of $\Gamma_k^i$|
| `dGammaExt~{i}~{k}` | = `Cset~{i}~{k}`|
| `dOmegaInt~{i}` | $=$`GammaInt~{i}`$=\partial\Omega_i \cap$`GammaInt`|
| `dOmegaExt~{i}` | $=\partial\Omega_i \cap$`GammaExt`$=\cup_k\Gamma_k^i$|
| `Sigma~{i}~{j}` | $=\Sigma_{ij} = \partial\Omega_i\cap\partial\Omega_j$|
| `Sigma~{i}` | $=\cap_j$ `Sigma~{i}~{j}`|
| `dSigmaExt~{i}~{j}~{n}` <br> with `n = 0,1,2...` | $n^{th}$ (End-) Point of $\Sigma_{ij}$ such that it also belons to $\Gamma$ (could be more than 1 end-point !)|
| `dSigmaInt~{i}~{j}~{n}` <br> with `n = 0,1,2...` | Same but switch the role of `GammaExt`($=\Gamma$) with `GammaInt`|
| `dSigmaExt~{i}~{j}` | $=\partial\Sigma_{ij}\cap\Gamma= \cup_n$`dSigmaExt~{i}~{j}~{n}`|
| `dSigmaInt~{i}~{j}` |  $=\partial\Sigma_{ij}\cap\Gamma^{int}= \cup_n$`dSigmaInt~{i}~{j}~{n}`|
| `dSigma~{i}~{j}` |  $=\partial\Sigma_{ij}=$`dSigmaExt~{i}~{j}`$\cup$`dSigmaInt~{i}~{j}`|

### Lists

> A list in GetDP is always monodimensional. When a list is said to be of dimension 2, explanation are provided to explain how to access the data.

| Nom| Dim | Fonction|
| ----- | ----- |----- |
|`D()` | 1 |Indices $i$ of subdomains $\Omega_{\texttt{D(index)}}$ with `index=0,1,2...,Ndom-1`. The index `i` of the subdomain is obtained through: `i= D(index)`. When subdomains are numbered from 0 to `Ndom`-1, then `D()` is simply equal to `[0,1,2,...Ndom-1]`.|
|`myD()` | 1 |Indices `index=0,1,2,...` in `D()` such that $\Omega_{\texttt{D(index)}}$ is  a subdmain the current MPI process has to treat (sequential case: `myD() = 0,1,2,...,Ndom-1`)|
| `D~{i}`| 1 |Indices $j$ of neighbor : *i.e.* such that $\partial\Omega_j\cap\partial\Omega_i\neq\emptyset$. Note that `i` and `j` are the "real" subdomain numbers |
| `ListGammaExt~{i}()` | 1 |Indices $k$ (global) of $\Gamma_k^i$ ($\neq \emptyset$ by definition) |
| `ListdGammaExt~{i}~{k}()` | 2 | Couple of indices $(j, \ell)$ of the $n^{th}$ end-point of $\Gamma_k^i$ (*i.e.* the $j$ and $\ell$ of $A_{k\ell}^{ij}$. Store contiguously : `[j,l, j,l, j,l,...]` |
| `ListdSigmaExt~{i}~{j}()` | 2 | Couple of indices $(k, \ell)$ of the $n^{th}$ end-point of $\Sigma_{ij}$ such that it also belongs to $\Gamma$ (*i.e.* the $k$ and $\ell$ of $A_{k\ell}^{ij}$. Store contiguously : `[j,l, j,l, j,l,...]` (same role as `ListdGammaExt~{i}~{k}()`) | 

