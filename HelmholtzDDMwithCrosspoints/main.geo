Include "main.dat" ;

CreateDir Str(DIR);

Solver.AutoMesh = -1;

SetOrder ORDER;
Mesh.ElementOrder = ORDER;
Mesh.SecondOrderLinear = 0;

Include Str[LinkGeo];
