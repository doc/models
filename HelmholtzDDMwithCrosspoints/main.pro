Include "main.dat" ;

// =================================================================================================
// GENERAL OPTIONS
// =================================================================================================

DefineConstant[
  SOLVER = {"gmres", Choices {"gmres", "fgmres", "bcgs", "print", "jacobi"},
    Name "Iterative Solver/0Solver"},
  TOLlog10 = {-6, Max -1, Min -16, Step -1,
    Name "Iterative Solver/1Tolerance (log10)"},
  TOL = 10^(TOLlog10),
  MAXIT = {500, Min 1, Step 1, Max 100000,
    Name "Iterative Solver/2Max. iterations"},
  RESTART_MAXIT = {0, Choices {0,1}, Visible 0,
    Name "Iterative Solver/31Force Restart = Max. iterations"},
  RESTART = {RESTART_MAXIT ? MAXIT : MAXIT, Min 0, Max 100000, Step 1, Visible 0,
    Name "Iterative Solver/30Restart", ReadOnly RESTART_MAXIT }
];

If(RESTART > MAXIT || RESTART == 0)
  RESTART = MAXIT;
EndIf

// =================================================================================================
// JACOBIAN & INTEGRATION
// =================================================================================================

Jacobian {
  { Name JVol; Case {{ Region All; Jacobian Vol; }}}
  { Name JSur; Case {{ Region All; Jacobian Sur; }}}
  { Name JLin; Case {{ Region All; Jacobian Lin; }}}
}

If (ORDER==1)
  NumGaussPointsPNT = 1;
  NumGaussPointsLIN = 4; // NumberOfPoints
  NumGaussPointsTRI = 6; // NumberOfPoints
EndIf
If (ORDER==2)
  NumGaussPointsPNT = 1;
  NumGaussPointsLIN = 6;  // NumberOfPoints
  NumGaussPointsTRI = 12; // NumberOfPoints
EndIf

Integration {
  { Name I1;
    Case {
      { Type Gauss;
        Case {
          { GeoElement Point;      NumberOfPoints NumGaussPointsPNT; }
          { GeoElement Line;       NumberOfPoints NumGaussPointsLIN; }
          { GeoElement Line2;      NumberOfPoints NumGaussPointsLIN; }
          { GeoElement Triangle;   NumberOfPoints NumGaussPointsTRI; }
          { GeoElement Triangle2;  NumberOfPoints NumGaussPointsTRI; } // 1 3 4 6 7 12 13 16
          { GeoElement Quadrangle; NumberOfPoints 7; }
        }
      }
    }
  }
}

// =================================================================================================
// SPECIFIC
// =================================================================================================

Include Str[LinkPro];
Include Str[LinkDDM];
