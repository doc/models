Include "domCircle.dat";

// ====================================================================================================
// FUNCTIONS
// ====================================================================================================

Function{
  R[] = Sqrt[X[]*X[]+Y[]*Y[]];
  f_inc[] = Complex[Cos[WAVENUMBER*X[]], Sin[WAVENUMBER*X[]]];
  dfdx_inc[] = WAVENUMBER * Complex[-Sin[WAVENUMBER*X[]], Cos[WAVENUMBER*X[]]];
  
  // Dirichlet
If(BC_SCATT == BC_Dir)
  f_ref[] = AcousticFieldSoftCylinder[XYZ[]]{WAVENUMBER, R_SCA};
  f_dir[] = -f_inc[];
EndIf

  // Neumann
If(BC_SCATT == BC_Neu)
  f_ref[] = AcousticFieldHardCylinder[XYZ[]]{WAVENUMBER, R_SCA, 0, 0, 50};
  f_neu[] = dfdx_inc[] * (X[]/R[]);
EndIf
}

// ====================================================================================================
// LISTS ABC/TBC
// ====================================================================================================

// Is there an ABC for main domain? (1=yes, 0=no)
isEdgeABC~{0} = 1;
isEdgeABC~{1} = 1;
isEdgeABC~{2} = 1;
isEdgeABC~{3} = 1;
isEdgeRad~{0} = 1;
isEdgeRad~{1} = 1;
isEdgeRad~{2} = 1;
isEdgeRad~{3} = 1;

ListOfSubdom = {};
For iPie In {0:Npie_DOM-1}
For iLay In {0:Nlay_DOM-1}
  iDom = iLay + Nlay_DOM*iPie;

If (iDom % MPI_Size == MPI_Rank)
  ListOfSubdom += iDom;
EndIf

  iPiePrev = (iPie+Npie_DOM-1)%Npie_DOM;
  iPieNext = (iPie+1)%Npie_DOM;

  iDomNeigh~{iDom}~{0} = iLay + Nlay_DOM*iPiePrev;  // Right
  iDomNeigh~{iDom}~{1} = (iLay+1) + Nlay_DOM*iPie;  // Top
  iDomNeigh~{iDom}~{2} = iLay + Nlay_DOM*iPieNext;  // Left
  iDomNeigh~{iDom}~{3} = (iLay-1) + Nlay_DOM*iPie;  // Down
  iEdgeNeigh~{iDom}~{0} = 2;
  iEdgeNeigh~{iDom}~{1} = 3;
  iEdgeNeigh~{iDom}~{2} = 0;
  iEdgeNeigh~{iDom}~{3} = 1;
  isEdgeRad~{iDom}~{0} = 0;
  isEdgeRad~{iDom}~{1} = 1;
  isEdgeRad~{iDom}~{2} = 0;
  isEdgeRad~{iDom}~{3} = 1;

  // Is there an ABC or TBC? (1=yes, 0=no)
  For iEdge In {0:3}
    isEdgeABC~{iDom}~{iEdge} = 0;
    isEdgeTBC~{iDom}~{iEdge} = 1;
  EndFor
  If(iLay == 0)
    isEdgeTBC~{iDom}~{3} = 0;
  EndIf
  If(iLay == Nlay_DOM-1)
    isEdgeABC~{iDom}~{1} = 1;
    isEdgeTBC~{iDom}~{1} = 0;
    isEdgeRad~{iDom}~{1} = isEdgeRad~{1};
  EndIf
  If(Npie_DOM == 1)
    isEdgeTBC~{iDom}~{0} = 0;
    isEdgeTBC~{iDom}~{2} = 0;
  EndIf

  // List of Edges for Absorbing/Transmission Boundary Condition
  ListOfEdgesWithABC~{iDom} = {};
  ListOfEdgesWithTBC~{iDom} = {};
  For iEdge In {0:3}
    If(isEdgeABC~{iDom}~{iEdge} == 1)
      ListOfEdgesWithABC~{iDom} += iEdge;
    EndIf
    If(isEdgeTBC~{iDom}~{iEdge} == 1)
      ListOfEdgesWithTBC~{iDom} += iEdge;
    EndIf
  EndFor

EndFor
EndFor

// ====================================================================================================
// GROUPS
// ====================================================================================================

Group {

// === REFERENCE CASE ===

  Omega = Region[{}];
  GammaD = Region[{}];
  GammaD0 = Region[{}];
  GammaN = Region[{}];
  GammaN0 = Region[{}];
  GammaPoint = Region[{}];
  For iEdge In {0:3}
    SigmaMain~{iEdge} = Region[{}];
    SigmaMainSides~{iEdge} = Region[{}];
    SigmaMainSide~{iEdge}~{0} = Region[{}];
    SigmaMainSide~{iEdge}~{1} = Region[{}];
  EndFor
  For iCorner In {0:3}
    CornerMain~{iCorner} = Region[{}];
  EndFor

For iPie In {0:Npie_DOM-1}
For iLay In {0:Nlay_DOM-1}
  iDom = iLay + Nlay_DOM*iPie;

  // DOMAIN
  Omega += Region[TAG_DOM~{iDom}];

  // BOUNDARY (Dirichlet/Neumann BC)
  If(iLay == 0)
  If(BC_SCATT == BC_Dir)
    GammaD += Region[TAG_BND~{iDom}~{3}];
  EndIf
  If(BC_SCATT == BC_Neu)
    GammaN += Region[TAG_BND~{iDom}~{3}];
  EndIf
  EndIf

  // BOUNDARY (Absorbing BC)
  If(iLay == Nlay_DOM-1)
    SigmaMain~{1} += Region[{TAG_BND~{iDom}~{1}}];
  EndIf

EndFor
EndFor

  SigmaMain = Region[{ SigmaMain~{0}, SigmaMain~{1}, SigmaMain~{2}, SigmaMain~{3} }];
  CornerMain = Region[{ CornerMain~{0}, CornerMain~{1}, CornerMain~{2}, CornerMain~{3} }];

// === DDM CASE ===

For iPie In {0:Npie_DOM-1}
For iLay In {0:Nlay_DOM-1}
  iDom = iLay + Nlay_DOM*iPie;

  // DOMAIN
  Omega~{iDom} = Region[TAG_DOM~{iDom}];

  // SOURCE
  GammaPoint~{iDom} = Region[{}];
  
  // BOUNDARY (Dirichlet/Neumann BC)
  GammaD0~{iDom} = Region[{}];
  GammaN0~{iDom} = Region[{}];
  GammaD~{iDom} = Region[{}];
  GammaN~{iDom} = Region[{}];
  If(iLay == 0)
  If(BC_SCATT == BC_Dir)
    GammaD~{iDom} = Region[ TAG_BND~{iDom}~{3} ];  // Down
  EndIf
  If(BC_SCATT == BC_Neu)
    GammaN~{iDom} = Region[ TAG_BND~{iDom}~{3} ];  // Down
  EndIf
  EndIf

  // BOUNDARY (Absorbing BC + Transmission BC)
  For iEdge In {0:3}
    Sigma~{iDom}~{iEdge}         = Region[{}];
    SigmaSide~{iDom}~{iEdge}~{0} = Region[{}];
    SigmaSide~{iDom}~{iEdge}~{1} = Region[{}];
    GammaD~{iDom}~{iEdge} = Region[{}];
    GammaD0~{iDom}~{iEdge} = Region[{}];
    GammaN~{iDom}~{iEdge} = Region[{}];
    GammaN0~{iDom}~{iEdge} = Region[{}];
  If((isEdgeABC~{iDom}~{iEdge} == 1) || (isEdgeTBC~{iDom}~{iEdge} == 1))
    iCornerSide~{0} = iEdge;
    iCornerSide~{1} = (iEdge+1)%4;
    iEdgeSide~{0} = (iEdge+3)%4;
    iEdgeSide~{1} = (iEdge+1)%4;
    
    Sigma~{iDom}~{iEdge}         = Region[{TAG_BND~{iDom}~{iEdge}}];
    SigmaSide~{iDom}~{iEdge}~{0} = Region[{TAG_CRN~{iDom}~{iCornerSide~{0}}}];
    SigmaSide~{iDom}~{iEdge}~{1} = Region[{TAG_CRN~{iDom}~{iCornerSide~{1}}}];
    
  For iSide In {0:1}
  If((isEdgeABC~{iDom}~{iEdgeSide~{iSide}} == 0) && (isEdgeTBC~{iDom}~{iEdgeSide~{iSide}} == 0))
  If(BC_SCATT == BC_Dir)
    GammaD~{iDom}~{iEdge} = Region[{TAG_CRN~{iDom}~{iCornerSide~{iSide}}}];
  EndIf
  If(BC_SCATT == BC_Neu)
    GammaN~{iDom}~{iEdge} = Region[{TAG_CRN~{iDom}~{iCornerSide~{iSide}}}];
  EndIf
  EndIf
  EndFor
  
  EndIf
  EndFor
  Sigma~{iDom} = Region[{ Sigma~{iDom}~{0}, Sigma~{iDom}~{1}, Sigma~{iDom}~{2}, Sigma~{iDom}~{3} }];

  // CORNER (Absorbing BC + Transmission BC)
  For iCorner In {0:3}
    Corner~{iDom}~{iCorner} = Region[{TAG_CRN~{iDom}~{iCorner}}];
  EndFor
  Corner~{iDom} = Region[{ Corner~{iDom}~{0}, Corner~{iDom}~{1}, Corner~{iDom}~{2}, Corner~{iDom}~{3} }];
EndFor
EndFor

}
