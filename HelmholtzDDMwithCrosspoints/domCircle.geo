Include "domCircle.dat";

Mesh.CharacteristicLengthMin = LC;
Mesh.CharacteristicLengthMax = LC;

// =================================================================================================
// Build POINTS, RADIAL LINES, CIRCULAR LINES and SURFACES
// =================================================================================================

If(Npie_DOM == 1)

  // Circular lines
  pCenter = newp;
  Point(pCenter) = {0,0,0};
  For iLay In {0:Nlay_DOM}
    radius = R_INT + iLay * (R_EXT-R_INT)/Nlay_DOM;
    p~{iLay}~{0} = newp;
    Point(p~{iLay}~{0}) = { radius, 0, 0};
    p~{iLay}~{1} = newp;
    Point(p~{iLay}~{1}) = {-radius, 0, 0};
    l_curve~{iLay}~{0} = newl;
    Circle(l_curve~{iLay}~{0}) = {p~{iLay}~{0}, pCenter, p~{iLay}~{1}};
    l_curve~{iLay}~{1} = newl;
    Circle(l_curve~{iLay}~{1}) = {p~{iLay}~{1}, pCenter, p~{iLay}~{0}};
  EndFor

  // Surfaces
  For iLay In {0:Nlay_DOM-1}
    ll = newll;
    Line Loop(ll) = {l_curve~{iLay+1}~{0}, l_curve~{iLay+1}~{1}, -l_curve~{iLay}~{0}, -l_curve~{iLay}~{1} };
    omega~{iLay}~{0} = news;
    Plane Surface(omega~{iLay}~{0}) = {ll};
  EndFor

Else

  // Points
  
  radiusTwist~{0} = -TWIST;
  radiusTwist~{1} =  TWIST;
  radiusTwist~{2} = -TWIST;
  radiusTwist~{3} =  TWIST;
  radiusTwist~{4} = -TWIST;
  radiusTwist~{5} =  TWIST;
  radiusTwist~{6} = -TWIST;
  radiusTwist~{7} =  TWIST;
  
  pCenter = newp;
  Point(pCenter) = {0,0,0};
  For iPie In {0:Npie_DOM-1}
  For iLay In {0:Nlay_DOM}
    theta  = iPie * (2*Pi/Npie_DOM) - THETA_INC + radiusTwist~{iLay};
    radius = R_INT + iLay * (R_EXT-R_INT)/Nlay_DOM;
    p~{iLay}~{iPie} = newp;
    Point(p~{iLay}~{iPie}) = {radius*Cos(theta), radius*Sin(theta), 0};
  EndFor
  EndFor

  // Radial lines
  For iPie In {0:Npie_DOM-1}
  For iLay In {0:Nlay_DOM-1}
    l_straight~{iLay}~{iPie} = newl;
    Line(l_straight~{iLay}~{iPie}) = {p~{iLay}~{iPie}, p~{iLay+1}~{iPie}};
  EndFor
  EndFor

  // Circular lines
  For iPie In {0:Npie_DOM-1}
  For iLay In {0:Nlay_DOM}
    iPieNext = (iPie+1)%Npie_DOM;
    l_curve~{iLay}~{iPie} = newl;
    Circle(l_curve~{iLay}~{iPie}) = {p~{iLay}~{iPie}, pCenter, p~{iLay}~{iPieNext}};
  EndFor
  EndFor

  // Surfaces
  For iPie In {0:Npie_DOM-1}
  For iLay In {0:Nlay_DOM-1}
    iPieNext = (iPie+1)%Npie_DOM;
    ll = newll;
    Line Loop(ll) = { l_straight~{iLay}~{iPie},      l_curve~{iLay+1}~{iPie},
                     -l_straight~{iLay}~{iPieNext}, -l_curve~{iLay  }~{iPie} };
    omega~{iLay}~{iPie} = news;
    Plane Surface(omega~{iLay}~{iPie}) = {ll};
  EndFor
  EndFor

EndIf

// =================================================================================================
// Generate MESH for each subdomain and reference domain
// =================================================================================================

// Mesh
If(StrCmp(OnelabAction, "check")) // only mesh if not in onelab check mode
  Mesh 2;
EndIf

For iPie In {0:Npie_DOM-1}
For iLay In {0:Nlay_DOM-1}
  iDom = iLay + Nlay_DOM*iPie;
  iPieNext = (iPie+1)%Npie_DOM;

  Delete Physicals;

If(Npie_DOM == 1)
  Physical Line(TAG_BND~{iDom}~{1}) = { l_curve~{iLay+1}~{0}, l_curve~{iLay+1}~{1} };  // Up
  Physical Line(TAG_BND~{iDom}~{3}) = { l_curve~{iLay  }~{0}, l_curve~{iLay  }~{1} };  // Down
Else
  Physical Point(TAG_CRN~{iDom}~{0}) = { p~{iLay}~{iPie} };
  Physical Point(TAG_CRN~{iDom}~{1}) = { p~{iLay+1}~{iPie} };
  Physical Point(TAG_CRN~{iDom}~{2}) = { p~{iLay+1}~{iPieNext} };
  Physical Point(TAG_CRN~{iDom}~{3}) = { p~{iLay}~{iPieNext} };
  Physical Line(TAG_BND~{iDom}~{0}) = { l_straight~{iLay}~{iPie}     };  // Right
  Physical Line(TAG_BND~{iDom}~{1}) = { l_curve~{iLay+1}~{iPie}      };  // Up
  Physical Line(TAG_BND~{iDom}~{2}) = {-l_straight~{iLay}~{iPieNext} };  // Left
  Physical Line(TAG_BND~{iDom}~{3}) = { l_curve~{iLay  }~{iPie}      };  // Down
EndIf

  Physical Surface(TAG_DOM~{iDom}) = omega~{iLay}~{iPie};

  Printf("Saving subdomain %g...", iDom);
  Save StrCat(MSH_NAME, Sprintf("_%g.msh", iDom));
  Printf("Done.");

EndFor
EndFor

Delete Physicals;

For iPie In {0:Npie_DOM-1}
For iLay In {0:Nlay_DOM-1}
  iDom = iLay + Nlay_DOM*iPie;
  iPieNext = (iPie+1)%Npie_DOM;

If(Npie_DOM == 1)
If(iLay == 0)
  Physical Line(TAG_BND~{iDom}~{3}) = { l_curve~{iLay  }~{0}, l_curve~{iLay  }~{1} };  // Down
EndIf
If(iLay == Nlay_DOM-1)
  Physical Line(TAG_BND~{iDom}~{1}) = { l_curve~{iLay+1}~{0}, l_curve~{iLay+1}~{1} };  // Up
EndIf
Else
If(iLay == 0)
  Physical Line(TAG_BND~{iDom}~{3}) = { l_curve~{iLay  }~{iPie} };  // Down
EndIf
If(iLay == Nlay_DOM-1)
  Physical Line(TAG_BND~{iDom}~{1}) = { l_curve~{iLay+1}~{iPie} };  // Up
EndIf
EndIf

  Physical Surface(TAG_DOM~{iDom}) = omega~{iLay}~{iPie};

EndFor
EndFor

Printf("Saving domain");
Save StrCat(MSH_NAME, Sprintf(".msh"));
Printf("Done.");
