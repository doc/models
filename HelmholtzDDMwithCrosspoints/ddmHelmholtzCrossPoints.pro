CROSSPOINT_NO   = 0;
CROSSPOINT_Full = 2;

DefineConstant[
  nPadeABC = {0, Choices {0, 1, 2, 3, 4, 5, 6}, Highlight "Yellow", // 6
    Name "Input/3ABC and TBC/3Pade ABC: Number of fields"},
  thetaPadeInputABC = {0, Min 0, Max 0.5, Step 0.05, // 0.3
    Name "Input/3ABC and TBC/4Pade ABC: Rotation of branch cut"},
  nPadeTBC = {3, Choices {0, 2, 4, 6}, Highlight "Yellow",// Loop 1,
    Name "Input/3ABC and TBC/5Pade TBC: Number of fields"},
  thetaPadeInputTBC = {0.3, Min 0.1, Max 0.5, Step 0.1,
    Name "Input/3ABC and TBC/6Pade TBC: Rotation of branch cut"},
  stretchPadeInputTBC = {1, Choices {0.01, 0.02, 0.05, 0.1, 0.2, 0.5, 1, 2, 5, 10, 20, 50},
    Name "Input/3ABC and TBC/7Pade ABC: Stretch of branch cut"},
  CROSSPOINT_TREAT = {CROSSPOINT_Full, Highlight "Blue", //Loop 2,
    Choices {CROSSPOINT_NO   = "Nothing",
             CROSSPOINT_Full = "Compatibility everywhere"},
    Name "Input/3ABC and TBC/8Compatibility Cross-Points"}
];

// ====================================================================================================
// FUNCTIONS
// ====================================================================================================

DefineConstant[ R_SCA, f_sou ];
For iDom In {0:N_DOM-1}
  DefineConstant[ f_sou~{iDom} ];
EndFor

Function{

  DefineFunction[ f_dir, f_neu ];

// === Imaginary unit and wavenumber

  I[] = Complex[0, 1];
If (FLAG_PBM == PBM_MARMOUSI)
  k[] = 2*Pi*FREQ/cData[];
Else
  k[] = 2*Pi*FREQ;
EndIf

// === Functions for the Pade ABC and Pade TBC

  mPadeABC = 2*nPadeABC+1;
  mPadeTBC = 2*nPadeTBC+1;

  For i In {1:nPadeABC}
    cPadeABC~{i} = Tan[i*Pi/mPadeABC]^2;
  EndFor
  For i In {1:nPadeTBC}
    cPadeTBC~{i} = Tan[i*Pi/mPadeTBC]^2;
  EndFor

  For iEdge In {0:3}
    kEps~{iEdge}[] = k[];
  If(isEdgeRad~{iEdge} == 1)
    //alphaBT[] = 0.5/R_EXT - (1/R_EXT^2) / (8*(1/R_EXT - I[]*k));
    //betaBT[] = 0.5/(1/R_EXT - I[]*k);

    paramCurv1~{iEdge}[] = - 0.5 * (1/R[]) + (1/R[])^2 / (8*((1/R[]) - I[]*k[]));
    paramCurv2~{iEdge}[] = - 0.5 / ((1/R[]) - I[]*k[]);
    //paramCurv2~{iEdge}[] = - 0.5 / (R[]*k[]*k[]);
  Else
    paramCurv1~{iEdge}[] = 0;
    paramCurv2~{iEdge}[] = 0;
  EndIf
  EndFor

  // Gander/Magoules/Nataf OO0
  kMin[] = Pi/7.5;
  kMinus[] = k[] - kMin[];
  kPlus[]  = k[] + kMin[];
  kMax[] = Pi/LC;
  //alphaPadeTBC[] = Sqrt[Sqrt[(kMax[]*kMax[]-k[]*k[]) * (k[]*k[]-kMinus[]*kMinus[])]]/k[] * Complex[Cos[Pi/4],Sin[Pi/4]];

  alphaPadeABC[] = Complex[Cos[thetaPadeInputABC*Pi/2.],Sin[thetaPadeInputABC*Pi/2.]];
  alphaPadeTBC[] = Complex[stretchPadeInputTBC*Cos[thetaPadeInputTBC*Pi/2.],stretchPadeInputTBC*Sin[thetaPadeInputTBC*Pi/2.]];

  For iDom In {0:N_DOM-1}
  For iEdge In {0:3}
  If((isEdgeABC~{iDom}~{iEdge} == 1) && (isEdgeRad~{iDom}~{iEdge} == 1))
    paramCurv1~{iDom}~{iEdge}[] = - 0.5 * (1/R[]) + (1/R[])^2 / (8*((1/R[]) - I[]*k[]));
    paramCurv2~{iDom}~{iEdge}[] = - 0.5 / ((1/R[]) - I[]*k[]);
    //paramCurv2~{iDom}~{iEdge}[] = - 0.5/(R[]*k[]*k[]);
  Else
    paramCurv1~{iDom}~{iEdge}[] = 0;
    paramCurv2~{iDom}~{iEdge}[] = 0;
  EndIf
  If((isEdgeABC~{iDom}~{iEdge} == 0) && (isEdgeRad~{iDom}~{iEdge} == 1))
    kEps~{iDom}~{iEdge}[] = k[] + I[] * 0.4 * k[]^(1/3) * (1/R[])^(2/3);  // or 0.6
  //ElseIf((isEdgeABC~{iDom}~{iEdge} == 0) && (isEdgeRad~{iDom}~{iEdge} == 0))
  //  kEps~{iDom}~{iEdge}[] = k[] + I[] * k[]/4;
  Else
    kEps~{iDom}~{iEdge}[] = k[];
  EndIf
  If(isEdgeTBC~{iDom}~{iEdge} == 1)
    nPade~{iDom}~{iEdge} = nPadeTBC;
    mPade~{iDom}~{iEdge} = mPadeTBC;
    For i In {1:nPadeTBC}
      cPade~{i}~{iDom}~{iEdge} = cPadeTBC~{i};
    EndFor
    alphaPade~{iDom}~{iEdge}[] = alphaPadeTBC[];
  Else
    nPade~{iDom}~{iEdge} = nPadeABC;
    mPade~{iDom}~{iEdge} = mPadeABC;
    For i In {1:nPadeABC}
      cPade~{i}~{iDom}~{iEdge} = cPadeABC~{i};
    EndFor
    alphaPade~{iDom}~{iEdge}[] = alphaPadeABC[];
  EndIf
  EndFor
  EndFor

// === Numerotation of fields

  index = 1;
  For iDom In {0:N_DOM-1}
    SaveSOL~{iDom} = index; index = index+1;
  For iEdge In {0:3}
    FIELD~{iDom}~{iEdge} = index; index = index+1;

  iEdgeSide~{0} = (iEdge+3)%4;
  iEdgeSide~{1} = (iEdge+1)%4;
  For iSide In {0:1}
  For i In {1:nPade~{iDom}~{iEdgeSide~{iSide}}}
    FIELD~{i}~{iDom}~{iEdge}~{iSide} = index; index = index+1;
  EndFor
  EndFor
  EndFor
  EndFor

// === Connectivity of fields

  ListOfFields          = {}; // Tableau flottant contenant les fields
  ListOfConnectedFields = {}; // fields connected to my fields

  For i0 In {0:#ListOfSubdom()-1}
  iDom  = ListOfSubdom(i0);
  For i1 In {0:#ListOfEdgesWithTBC~{iDom}()-1}
  iEdge = ListOfEdgesWithTBC~{iDom}(i1);
  iEdgeSide~{0} = (iEdge+3)%4;
  iEdgeSide~{1} = (iEdge+1)%4;
  iDomNeigh  = iDomNeigh~{iDom}~{iEdge};
  iEdgeNeigh = iEdgeNeigh~{iDom}~{iEdge};
  iSideNeigh~{0} = 1;
  iSideNeigh~{1} = 0;

    g_in~{iDom}~{iEdge}[]  = ComplexScalarField[XYZ[]]{ FIELD~{iDomNeigh}~{iEdgeNeigh}() };
    ListOfFields          += FIELD~{iDom}~{iEdge};
    ListOfConnectedFields += { 1, FIELD~{iDomNeigh}~{iEdgeNeigh}() };

  For iSide In {0:1}
  If (CROSSPOINT_TREAT == CROSSPOINT_Full)
  For j In {1:nPade~{iDom}~{iEdgeSide~{iSide}}}
    g_in~{j}~{iDom}~{iEdge}~{iSide}[]  = ComplexScalarField[XYZ[]]{ FIELD~{j}~{iDomNeigh}~{iEdgeNeigh}~{iSideNeigh~{iSide}}() };
    ListOfFields                      += FIELD~{j}~{iDom}~{iEdge}~{iSide};
    ListOfConnectedFields             += { 1, FIELD~{j}~{iDomNeigh}~{iEdgeNeigh}~{iSideNeigh~{iSide}}() };
  EndFor
  EndIf
  EndFor

  EndFor
  EndFor

// === Reference solution

  FIELD_REF = index+1; index = index+1;
  If(FLAG_ERROR == ERROR_ANA)
    f_refErr[] = f_ref[];
  EndIf
  If(FLAG_ERROR == ERROR_NUM)
    f_refErr[] = ComplexScalarField[XYZ[]]{ FIELD_REF() };
  EndIf

}

// ====================================================================================================
// CONSTRAINTS
// ====================================================================================================

Constraint{
  { Name Dirichlet_u;
    Case {
      { Region GammaD;  Value f_dir[]; }
      { Region GammaD0; Value 0; }
    }
  }
For i0 In {0:#ListOfSubdom()-1}
iDom = ListOfSubdom(i0);
  { Name Dirichlet_u~{iDom};
    Case {
      { Region GammaD~{iDom};  Value $PhysicalSource ? f_dir[] : 0.; }
      { Region GammaD0~{iDom}; Value 0.; }
    }
  }
For iEdge In {0:3}
For i In {1:nPade~{iDom}~{iEdge}}
  { Name Dirichlet_u~{i}~{iDom}~{iEdge};
    Case {
      { Region GammaD~{iDom}~{iEdge};  Value $PhysicalSource ? f_dir[] : 0.; }   // SigmaSide~{iDom}~{iEdge}~{0}, SigmaSide~{iDom}~{iEdge}~{1}
      { Region GammaD0~{iDom}~{iEdge}; Value 0.; }
    }
  }
EndFor
EndFor
EndFor
}

// ====================================================================================================
// FUNCTION SPACE
// ====================================================================================================

FunctionSpace {

  { Name Href; Type Form0;
    BasisFunction {{ Name sn; NameOfCoef un; Function BF_Node; Support Region[ {Omega, SigmaMain, CornerMain, GammaPoint, GammaN} ]; Entity NodesOf[All]; }}
    Constraint {{ NameOfCoef un; EntityType NodesOf; NameOfConstraint Dirichlet_u; }}
  }
For iEdge In {0:3}
  { Name Href_du~{iEdge}; Type Form0;
    BasisFunction {{ Name sn; NameOfCoef un; Function BF_Node; Support Region[ {SigmaMain~{iEdge}, SigmaMainSide~{iEdge}~{0}, SigmaMainSide~{iEdge}~{1}} ]; Entity NodesOf[All]; }}
  }
For i In {1:nPadeABC}
  { Name Href~{i}~{iEdge}; Type Form0;
    BasisFunction {{ Name sn; NameOfCoef un; Function BF_Node; Support Region[ {SigmaMain~{iEdge}, SigmaMainSide~{iEdge}~{0}, SigmaMainSide~{iEdge}~{1}} ]; Entity NodesOf[All]; }}
  }
For iSide In {0:1}
  { Name Href_du~{i}~{iEdge}~{iSide}; Type Form0;
    BasisFunction {{ Name sn; NameOfCoef un; Function BF_Node; Support Region[ {SigmaMainSide~{iEdge}~{iSide}} ]; Entity NodesOf[All]; }}
  }
EndFor
EndFor
EndFor
For iCorner In {0:3}
For i In {1:nPadeABC}
For j In {1:nPadeABC}
  { Name Href~{i}~{j}~{iCorner}; Type Form0;
    BasisFunction {{ Name sn; NameOfCoef un; Function BF_Node; Support Region[ {CornerMain~{iCorner}} ]; Entity NodesOf[All]; }}
  }
EndFor
EndFor
EndFor

For i0 In {0:#ListOfSubdom()-1}
iDom = ListOfSubdom(i0);

  { Name Hddm_u~{iDom}; Type Form0;
    BasisFunction {{ Name sn; NameOfCoef un; Function BF_Node; Support Region[ {Omega~{iDom}, Sigma~{iDom}, Corner~{iDom}, GammaPoint~{iDom}, GammaN~{iDom}} ]; Entity NodesOf[All]; }}
    Constraint {{ NameOfCoef un; EntityType NodesOf; NameOfConstraint Dirichlet_u~{iDom}; }}
  }
For iEdge In {0:3}
  { Name Hddm_du~{iDom}~{iEdge}; Type Form0;
    BasisFunction {{ Name sn; NameOfCoef un; Function BF_Node; Support Region[ {Sigma~{iDom}~{iEdge}, SigmaSide~{iDom}~{iEdge}~{0}, SigmaSide~{iDom}~{iEdge}~{1}} ]; Entity NodesOf[All]; }} // , Not {GammaD~{iDom}}
  }
For i In {1:nPade~{iDom}~{iEdge}}
  { Name Hddm_u~{i}~{iDom}~{iEdge}; Type Form0;
    BasisFunction {{ Name sn; NameOfCoef un; Function BF_Node; Support Region[ {Sigma~{iDom}~{iEdge}, SigmaSide~{iDom}~{iEdge}~{0}, SigmaSide~{iDom}~{iEdge}~{1}} ]; Entity NodesOf[All]; }}
//    Constraint {{ NameOfCoef un; EntityType NodesOf; NameOfConstraint Dirichlet_u~{i}~{iDom}~{iEdge}; }}
  }
For iSide In {0:1}
  { Name Hddm_du~{i}~{iDom}~{iEdge}~{iSide}; Type Form0;
    BasisFunction {{ Name sn; NameOfCoef un; Function BF_Node; Support Region[ {SigmaSide~{iDom}~{iEdge}~{iSide}} ]; Entity NodesOf[All]; }}
  }
EndFor
EndFor
EndFor
For iCorner In {0:3}
iEdgeSide~{0} = (iCorner+3)%4;
iEdgeSide~{1} = iCorner;
For i In {1:nPade~{iDom}~{iEdgeSide~{0}}}
For j In {1:nPade~{iDom}~{iEdgeSide~{1}}}
  { Name Hddm_u~{i}~{j}~{iDom}~{iCorner}; Type Form0;
    BasisFunction {{ Name sn; NameOfCoef un; Function BF_Node; Support Region[ {Corner~{iDom}~{iCorner}} ]; Entity NodesOf[All]; }}
  }
EndFor
EndFor
EndFor

For iEdge In {0:3}
iEdgeSide~{0} = (iEdge+3)%4;
iEdgeSide~{1} = (iEdge+1)%4;
  { Name Hddm_g_out~{iDom}~{iEdge}; Type Form0;
    BasisFunction {{ Name sn; NameOfCoef un; Function BF_Node; Support Region[ {Sigma~{iDom}~{iEdge}, SigmaSide~{iDom}~{iEdge}~{0}, SigmaSide~{iDom}~{iEdge}~{1}} ]; Entity NodesOf[All]; }} // , Not {GammaD~{iDom}}
  }
For iSide In {0:1}
For i In {1:nPade~{iDom}~{iEdgeSide~{iSide}}}
  { Name Hddm_g_out~{i}~{iDom}~{iEdge}~{iSide}; Type Form0;
    BasisFunction {{ Name sn; NameOfCoef un; Function BF_Node; Support Region[ {SigmaSide~{iDom}~{iEdge}~{iSide}} ]; Entity NodesOf[All]; }}
  }
EndFor
EndFor
EndFor

EndFor
}


// ====================================================================================================
// FORMULATION
// ====================================================================================================

Formulation {

// ====================================================================================================
// Reference SOLVER without DDM
// ====================================================================================================

  { Name FormRef; Type FemEquation;
    Quantity {
      { Name uRef; Type Local; NameOfSpace Href; }
    For iEdge In {0:3}
      { Name duRef~{iEdge}; Type Local; NameOfSpace Href_du~{iEdge}; }
    For i In {1:nPadeABC}
      { Name uRef~{i}~{iEdge}; Type Local; NameOfSpace Href~{i}~{iEdge}; }
    For iSide In {0:1}
      { Name duRef~{i}~{iEdge}~{iSide}; Type Local; NameOfSpace Href_du~{i}~{iEdge}~{iSide}; }
    EndFor
    EndFor
    EndFor
    For iCorner In {0:3}
    For i In {1:nPadeABC}
    For j In {1:nPadeABC}
      { Name uRef~{i}~{j}~{iCorner}; Type Local; NameOfSpace Href~{i}~{j}~{iCorner}; }
    EndFor
    EndFor
    EndFor
    }
    Equation {

// Field on SURFACE

      Galerkin { [ Dof{d uRef}        , {d uRef} ]; In Omega;             Jacobian JSur; Integration I1; }
      Galerkin { [ - k[]^2 * Dof{uRef}  , {uRef} ]; In Omega;             Jacobian JSur; Integration I1; }
      Galerkin { [ - f_sou              , {uRef} ]; In GammaPoint;        Jacobian JSur; Integration I1; }
      Galerkin { [ - f_neu[]            , {uRef} ]; In GammaN;            Jacobian JLin; Integration I1; }
    For iEdge In {0:3}
      Galerkin { [ - Dof{duRef~{iEdge}} , {uRef} ]; In SigmaMain~{iEdge}; Jacobian JLin; Integration I1; }
    EndFor

// DtN on EDGES

    For iEdge In {0:3}
      Galerkin { [   Dof{duRef~{iEdge}}                                                          , {duRef~{iEdge}} ]; In SigmaMain~{iEdge}; Jacobian JLin; Integration I1; }
      Galerkin { [ - paramCurv1~{iEdge}[] * Dof{uRef}                                            , {duRef~{iEdge}} ]; In SigmaMain~{iEdge}; Jacobian JLin; Integration I1; }
      Galerkin { [ - paramCurv2~{iEdge}[] * Dof{d uRef}                                        , {d duRef~{iEdge}} ]; In SigmaMain~{iEdge}; Jacobian JLin; Integration I1; }
      Galerkin { [ - I[]*k[]*alphaPadeABC[] * Dof{uRef}                                          , {duRef~{iEdge}} ]; In SigmaMain~{iEdge}; Jacobian JLin; Integration I1; }
    For i In {1:nPadeABC}
      Galerkin { [ - I[]*k[]*alphaPadeABC[] * 2./mPadeABC * cPadeABC~{i} * Dof{uRef}             , {duRef~{iEdge}} ]; In SigmaMain~{iEdge}; Jacobian JLin; Integration I1; }
      Galerkin { [ - I[]*k[]*alphaPadeABC[] * 2./mPadeABC * cPadeABC~{i} * Dof{uRef~{i}~{iEdge}} , {duRef~{iEdge}} ]; In SigmaMain~{iEdge}; Jacobian JLin; Integration I1; }
    EndFor
    EndFor

// Fields on EDGES

    For iEdge In {0:3}
    iCornerSide~{0} = iEdge;
    iCornerSide~{1} = (iEdge+1)%4;
    For i In {1:nPadeABC}

      Galerkin { [ Dof{d uRef~{i}~{iEdge}}                                         , {d uRef~{i}~{iEdge}} ]; In SigmaMain~{iEdge};                Jacobian JLin; Integration I1; }
      Galerkin { [ - kEps~{iEdge}[]^2*(alphaPadeABC[]^2*cPadeABC~{i}+1) * Dof{uRef~{i}~{iEdge}} , {uRef~{i}~{iEdge}} ]; In SigmaMain~{iEdge};                Jacobian JLin; Integration I1; }
      Galerkin { [ - kEps~{iEdge}[]^2*alphaPadeABC[]^2*(cPadeABC~{i}+1) * Dof{uRef}             , {uRef~{i}~{iEdge}} ]; In SigmaMain~{iEdge};                Jacobian JLin; Integration I1; }
    For iSide In {0:1}
      Galerkin { [ - Dof{duRef~{i}~{iEdge}~{iSide}}                                  , {uRef~{i}~{iEdge}} ]; In CornerMain~{iCornerSide~{iSide}}; Jacobian JVol; Integration I1; }
    EndFor

    EndFor
    EndFor

// DtN on EDGES

    For iEdge In {0:3}
    iCornerSide~{0} = iEdge;
    iCornerSide~{1} = (iEdge+1)%4;
    For i In {1:nPadeABC}

    For iSide In {0:1}
      Galerkin { [   Dof{duRef~{i}~{iEdge}~{iSide}}                                                                , {duRef~{i}~{iEdge}~{iSide}} ]; In CornerMain~{iCornerSide~{iSide}}; Jacobian JVol; Integration I1; }
      Galerkin { [ - I[]*k[]*alphaPadeABC[] * Dof{uRef~{i}~{iEdge}}                                                , {duRef~{i}~{iEdge}~{iSide}} ]; In CornerMain~{iCornerSide~{iSide}}; Jacobian JVol; Integration I1; }
    For j In {1:nPadeABC}
      Galerkin { [ - I[]*k[]*alphaPadeABC[] * 2./mPadeABC * cPadeABC~{j} * Dof{uRef~{i}~{iEdge}}                   , {duRef~{i}~{iEdge}~{iSide}} ]; In CornerMain~{iCornerSide~{iSide}}; Jacobian JVol; Integration I1; }
    If (iSide == 0)
      Galerkin { [ - I[]*k[]*alphaPadeABC[] * 2./mPadeABC * cPadeABC~{j} * Dof{uRef~{j}~{i}~{iCornerSide~{iSide}}} , {duRef~{i}~{iEdge}~{iSide}} ]; In CornerMain~{iCornerSide~{iSide}}; Jacobian JVol; Integration I1; }
    Else
      Galerkin { [ - I[]*k[]*alphaPadeABC[] * 2./mPadeABC * cPadeABC~{j} * Dof{uRef~{i}~{j}~{iCornerSide~{iSide}}} , {duRef~{i}~{iEdge}~{iSide}} ]; In CornerMain~{iCornerSide~{iSide}}; Jacobian JVol; Integration I1; }
    EndIf
    EndFor
    EndFor

    EndFor
    EndFor

// Fields on CORNERS

    For iCorner In {0:3}
    iEdgeSide~{0} = (iCorner+3)%4;
    iEdgeSide~{1} = iCorner;
    For i In {1:nPadeABC}
    For j In {1:nPadeABC}
      Galerkin { [ Dof{uRef~{i}~{j}~{iCorner}}                                                                                                             , {uRef~{i}~{j}~{iCorner}} ]; In CornerMain~{iCorner}; Jacobian JVol; Integration I1; }
      Galerkin { [ alphaPadeABC[]^2*(cPadeABC~{j}+1) * Dof{uRef~{i}~{iEdgeSide~{0}}} / (alphaPadeABC[]^2*cPadeABC~{i} + alphaPadeABC[]^2*cPadeABC~{j} + 1) , {uRef~{i}~{j}~{iCorner}} ]; In CornerMain~{iCorner}; Jacobian JVol; Integration I1; }
      Galerkin { [ alphaPadeABC[]^2*(cPadeABC~{i}+1) * Dof{uRef~{j}~{iEdgeSide~{1}}} / (alphaPadeABC[]^2*cPadeABC~{i} + alphaPadeABC[]^2*cPadeABC~{j} + 1) , {uRef~{i}~{j}~{iCorner}} ]; In CornerMain~{iCorner}; Jacobian JVol; Integration I1; }
    EndFor
    EndFor
    EndFor

    }
  }

// ====================================================================================================
// SOLVER for subdomain 'iDom'
// ====================================================================================================

For i0 In {0:#ListOfSubdom()-1}
iDom = ListOfSubdom(i0);

  { Name FormVol~{iDom}; Type FemEquation;
    Quantity {
      { Name u~{iDom}; Type Local; NameOfSpace Hddm_u~{iDom}; }
    For iEdge In {0:3}
      { Name du~{iDom}~{iEdge}; Type Local; NameOfSpace Hddm_du~{iDom}~{iEdge}; }
    For i In {1:nPade~{iDom}~{iEdge}}
      { Name u~{i}~{iDom}~{iEdge}; Type Local; NameOfSpace Hddm_u~{i}~{iDom}~{iEdge}; }
    For iSide In {0:1}
      { Name du~{i}~{iDom}~{iEdge}~{iSide}; Type Local; NameOfSpace Hddm_du~{i}~{iDom}~{iEdge}~{iSide}; }
    EndFor
    EndFor
    EndFor
    For iCorner In {0:3}
    iEdgeSide~{0} = (iCorner+3)%4;
    iEdgeSide~{1} = iCorner;
    For i In {1:nPade~{iDom}~{iEdgeSide~{0}}}
    For j In {1:nPade~{iDom}~{iEdgeSide~{1}}}
      { Name u~{i}~{j}~{iDom}~{iCorner}; Type Local; NameOfSpace Hddm_u~{i}~{j}~{iDom}~{iCorner}; }
    EndFor
    EndFor
    EndFor
    }
    Equation {

// Field on SURFACE

      Galerkin { [ Dof{d u~{iDom}}                                , {d u~{iDom}} ]; In Omega~{iDom};         Jacobian JSur; Integration I1; }
      Galerkin { [ - k[]^2 * Dof{u~{iDom}}                          , {u~{iDom}} ]; In Omega~{iDom};         Jacobian JSur; Integration I1; }
      Galerkin { [ ($PhysicalSource ? -f_sou~{iDom} : 0)            , {u~{iDom}} ]; In GammaPoint~{iDom};    Jacobian JSur; Integration I1; }
      Galerkin { [ ($PhysicalSource ? -f_neu[] : 0)                 , {u~{iDom}} ]; In GammaN~{iDom};        Jacobian JLin; Integration I1; }
    For iEdge In {0:3}
      Galerkin { [ - Dof{du~{iDom}~{iEdge}}                         , {u~{iDom}} ]; In Sigma~{iDom}~{iEdge}; Jacobian JLin; Integration I1; }
    If (isEdgeTBC~{iDom}~{iEdge})
      Galerkin { [ ($ArtificialSource ? -g_in~{iDom}~{iEdge}[] : 0) , {u~{iDom}} ]; In Sigma~{iDom}~{iEdge}; Jacobian JLin; Integration I1; }
    EndIf
    EndFor

// DtN on EDGES

    For iEdge In {0:3}
      Galerkin { [   Dof{du~{iDom}~{iEdge}}                                                                                              , {du~{iDom}~{iEdge}} ]; In Sigma~{iDom}~{iEdge}; Jacobian JLin; Integration I1; }
      Galerkin { [ - paramCurv1~{iDom}~{iEdge}[] * Dof{u~{iDom}}                                                                         , {du~{iDom}~{iEdge}} ]; In Sigma~{iDom}~{iEdge}; Jacobian JLin; Integration I1; }
      Galerkin { [ - paramCurv2~{iDom}~{iEdge}[] * Dof{d u~{iDom}}                                                                     , {d du~{iDom}~{iEdge}} ]; In Sigma~{iDom}~{iEdge}; Jacobian JLin; Integration I1; }
      Galerkin { [ - I[]*k[]*alphaPade~{iDom}~{iEdge}[] * Dof{u~{iDom}}                                                                  , {du~{iDom}~{iEdge}} ]; In Sigma~{iDom}~{iEdge}; Jacobian JLin; Integration I1; }
    For i In {1:nPade~{iDom}~{iEdge}}
      Galerkin { [ - I[]*k[]*alphaPade~{iDom}~{iEdge}[] * 2./mPade~{iDom}~{iEdge} * cPade~{i}~{iDom}~{iEdge} * Dof{u~{iDom}}             , {du~{iDom}~{iEdge}} ]; In Sigma~{iDom}~{iEdge}; Jacobian JLin; Integration I1; }
      Galerkin { [ - I[]*k[]*alphaPade~{iDom}~{iEdge}[] * 2./mPade~{iDom}~{iEdge} * cPade~{i}~{iDom}~{iEdge} * Dof{u~{i}~{iDom}~{iEdge}} , {du~{iDom}~{iEdge}} ]; In Sigma~{iDom}~{iEdge}; Jacobian JLin; Integration I1; }
    EndFor
    EndFor

// Fields on EDGES

    For iEdge In {0:3}
    iEdgeSide~{0} = (iEdge+3)%4;
    iEdgeSide~{1} = (iEdge+1)%4;
    iCornerSide~{0} = iEdge;
    iCornerSide~{1} = (iEdge+1)%4;
    iSideSide~{0} = 1;
    iSideSide~{1} = 0;
    For i In {1:nPade~{iDom}~{iEdge}}

      Galerkin { [ Dof{d u~{i}~{iDom}~{iEdge}}                                                                 , {d u~{i}~{iDom}~{iEdge}} ]; In Sigma~{iDom}~{iEdge};                Jacobian JLin; Integration I1; }
      Galerkin { [ - kEps~{iDom}~{iEdge}[]^2*(alphaPade~{iDom}~{iEdge}[]^2*cPade~{i}~{iDom}~{iEdge}+1) * Dof{u~{i}~{iDom}~{iEdge}} , {u~{i}~{iDom}~{iEdge}} ]; In Sigma~{iDom}~{iEdge};                Jacobian JLin; Integration I1; }
      Galerkin { [ - kEps~{iDom}~{iEdge}[]^2*alphaPade~{iDom}~{iEdge}[]^2*(cPade~{i}~{iDom}~{iEdge}+1) * Dof{u~{iDom}}             , {u~{i}~{iDom}~{iEdge}} ]; In Sigma~{iDom}~{iEdge};                Jacobian JLin; Integration I1; }
    For iSide In {0:1}
    If ((CROSSPOINT_TREAT == CROSSPOINT_Full) || (isEdgeABC~{iDom}~{iEdge} && isEdgeABC~{iDom}~{iEdgeSide~{iSide}}))
      Galerkin { [ - Dof{du~{i}~{iDom}~{iEdge}~{iSide}}                                                          , {u~{i}~{iDom}~{iEdge}} ]; In Corner~{iDom}~{iCornerSide~{iSide}}; Jacobian JVol; Integration I1; }
    If (isEdgeTBC~{iDom}~{iEdgeSide~{iSide}})
      Galerkin { [ ($ArtificialSource ? -g_in~{i}~{iDom}~{iEdgeSide~{iSide}}~{iSideSide~{iSide}}[] : 0)          , {u~{i}~{iDom}~{iEdge}} ]; In Corner~{iDom}~{iCornerSide~{iSide}}; Jacobian JVol; Integration I1; }
    EndIf
    EndIf
    EndFor
    EndFor
    EndFor

// DtN at CORNERS

    For iEdge In {0:3}
    iEdgeSide~{0} = (iEdge+3)%4;
    iEdgeSide~{1} = (iEdge+1)%4;
    iCornerSide~{0} = iEdge;
    iCornerSide~{1} = (iEdge+1)%4;
    For i In {1:nPade~{iDom}~{iEdge}}

    For iSide In {0:1}
      Galerkin { [   Dof{du~{i}~{iDom}~{iEdge}~{iSide}}                                                                                                                                        , {du~{i}~{iDom}~{iEdge}~{iSide}} ]; In Corner~{iDom}~{iCornerSide~{iSide}}; Jacobian JVol; Integration I1; }
      Galerkin { [ - I[]*k[]*alphaPade~{iDom}~{iEdgeSide~{iSide}}[] * Dof{u~{i}~{iDom}~{iEdge}}                                                                                                , {du~{i}~{iDom}~{iEdge}~{iSide}} ]; In Corner~{iDom}~{iCornerSide~{iSide}}; Jacobian JVol; Integration I1; }
    For j In {1:nPade~{iDom}~{iEdgeSide~{iSide}}}
      Galerkin { [ - I[]*k[]*alphaPade~{iDom}~{iEdgeSide~{iSide}}[] * 2./mPade~{iDom}~{iEdgeSide~{iSide}} * cPade~{j}~{iDom}~{iEdgeSide~{iSide}} * Dof{u~{i}~{iDom}~{iEdge}}                   , {du~{i}~{iDom}~{iEdge}~{iSide}} ]; In Corner~{iDom}~{iCornerSide~{iSide}}; Jacobian JVol; Integration I1; }
    If (iSide == 0)
      Galerkin { [ - I[]*k[]*alphaPade~{iDom}~{iEdgeSide~{iSide}}[] * 2./mPade~{iDom}~{iEdgeSide~{iSide}} * cPade~{j}~{iDom}~{iEdgeSide~{iSide}} * Dof{u~{j}~{i}~{iDom}~{iCornerSide~{iSide}}} , {du~{i}~{iDom}~{iEdge}~{iSide}} ]; In Corner~{iDom}~{iCornerSide~{iSide}}; Jacobian JVol; Integration I1; }
    Else
      Galerkin { [ - I[]*k[]*alphaPade~{iDom}~{iEdgeSide~{iSide}}[] * 2./mPade~{iDom}~{iEdgeSide~{iSide}} * cPade~{j}~{iDom}~{iEdgeSide~{iSide}} * Dof{u~{i}~{j}~{iDom}~{iCornerSide~{iSide}}} , {du~{i}~{iDom}~{iEdge}~{iSide}} ]; In Corner~{iDom}~{iCornerSide~{iSide}}; Jacobian JVol; Integration I1; }
    EndIf
    EndFor
    EndFor

    EndFor
    EndFor

// Fields at CORNERS

    For iCorner In {0:3}
    iEdge0 = (iCorner+3)%4;
    iEdge1 = iCorner;
    For i In {1:nPade~{iDom}~{iEdge0}}
    For j In {1:nPade~{iDom}~{iEdge1}}
      Galerkin { [ Dof{u~{i}~{j}~{iDom}~{iCorner}}                                                                                                                                                                                , {u~{i}~{j}~{iDom}~{iCorner}} ]; In Corner~{iDom}~{iCorner}; Jacobian JVol; Integration I1; }
      Galerkin { [ alphaPade~{iDom}~{iEdge1}[]^2*(cPade~{j}~{iDom}~{iEdge1}+1) * Dof{u~{i}~{iDom}~{iEdge0}} / (alphaPade~{iDom}~{iEdge0}[]^2*cPade~{i}~{iDom}~{iEdge0} + alphaPade~{iDom}~{iEdge1}[]^2*cPade~{j}~{iDom}~{iEdge1} + 1) , {u~{i}~{j}~{iDom}~{iCorner}} ]; In Corner~{iDom}~{iCorner}; Jacobian JVol; Integration I1; }
      Galerkin { [ alphaPade~{iDom}~{iEdge0}[]^2*(cPade~{i}~{iDom}~{iEdge0}+1) * Dof{u~{j}~{iDom}~{iEdge1}} / (alphaPade~{iDom}~{iEdge0}[]^2*cPade~{i}~{iDom}~{iEdge0} + alphaPade~{iDom}~{iEdge1}[]^2*cPade~{j}~{iDom}~{iEdge1} + 1) , {u~{i}~{j}~{iDom}~{iCorner}} ]; In Corner~{iDom}~{iCorner}; Jacobian JVol; Integration I1; }
    EndFor
    EndFor
    EndFor

    }
  }

EndFor // loop on iDom

// ====================================================================================================
// SOLVER for outgoing data for side 'iEdge' of 'iDom'
// ====================================================================================================

For i0 In {0:#ListOfSubdom()-1}
iDom = ListOfSubdom(i0);
For i1 In {0:#ListOfEdgesWithTBC~{iDom}()-1}
iEdge = ListOfEdgesWithTBC~{iDom}(i1);
iEdgeSide~{0} = (iEdge+3)%4;
iEdgeSide~{1} = (iEdge+1)%4;
iCornerSide~{0} = iEdge;
iCornerSide~{1} = (iEdge+1)%4;
iSideSide~{0} = 1;
iSideSide~{1} = 0;

  { Name FormSur~{iDom}~{iEdge}; Type FemEquation;
    Quantity {
      { Name du~{iDom}~{iEdge};    Type Local; NameOfSpace Hddm_du~{iDom}~{iEdge}; }
      { Name g_out~{iDom}~{iEdge}; Type Local; NameOfSpace Hddm_g_out~{iDom}~{iEdge}; }
    For iSide In {0:1}
    For i In {1:nPade~{iDom}~{iEdgeSide~{iSide}}}
      { Name du~{i}~{iDom}~{iEdgeSide~{iSide}}~{iSideSide~{iSide}}; Type Local; NameOfSpace Hddm_du~{i}~{iDom}~{iEdgeSide~{iSide}}~{iSideSide~{iSide}}; }
      { Name g_out~{i}~{iDom}~{iEdge}~{iSide};                      Type Local; NameOfSpace Hddm_g_out~{i}~{iDom}~{iEdge}~{iSide}; }
    EndFor
    EndFor
    }
    Equation {

      Galerkin { [ Dof{g_out~{iDom}~{iEdge}}                     , {g_out~{iDom}~{iEdge}} ]; In Sigma~{iDom}~{iEdge}; Jacobian JSur; Integration I1; }
      Galerkin { [ $ArtificialSource ? g_in~{iDom}~{iEdge}[] : 0 , {g_out~{iDom}~{iEdge}} ]; In Sigma~{iDom}~{iEdge}; Jacobian JSur; Integration I1; }
      Galerkin { [ 2 * {du~{iDom}~{iEdge}}                       , {g_out~{iDom}~{iEdge}} ]; In Sigma~{iDom}~{iEdge}; Jacobian JSur; Integration I1; }

    For iSide In {0:1}
    If (CROSSPOINT_TREAT == CROSSPOINT_Full)
    For i In {1:nPade~{iDom}~{iEdgeSide~{iSide}}}
      Galerkin { [ Dof{g_out~{i}~{iDom}~{iEdge}~{iSide}}                       , {g_out~{i}~{iDom}~{iEdge}~{iSide}} ]; In Corner~{iDom}~{iCornerSide~{iSide}}; Jacobian JLin; Integration I1; }
      Galerkin { [ $ArtificialSource ? g_in~{i}~{iDom}~{iEdge}~{iSide}[] : 0   , {g_out~{i}~{iDom}~{iEdge}~{iSide}} ]; In Corner~{iDom}~{iCornerSide~{iSide}}; Jacobian JLin; Integration I1; }
      Galerkin { [ 2 * {du~{i}~{iDom}~{iEdgeSide~{iSide}}~{iSideSide~{iSide}}} , {g_out~{i}~{iDom}~{iEdge}~{iSide}} ]; In Corner~{iDom}~{iCornerSide~{iSide}}; Jacobian JLin; Integration I1; }
    EndFor
    EndIf
    EndFor

    }
  }

EndFor // loop on iEdge
EndFor // loop on iDom

}

// ====================================================================================================
// RESOLUTION
// ====================================================================================================

Resolution {
  { Name ResoRef;
    System {
        { Name SysRef; NameOfFormulation FormRef; Type Complex; NameOfMesh Sprintf[StrCat[MSH_NAME, ".msh"]]; }
    }
    Operation {
        Generate[SysRef]; Solve[SysRef]; PostOperation[PostOpRef];
    }
  }

  { Name ResoDDM;
    System {
      If(FLAG_ERROR == ERROR_NUM)
        { Name SysRef; NameOfFormulation FormRef; Type Complex; NameOfMesh Sprintf[StrCat[MSH_NAME, ".msh"]]; }
      EndIf
      For i0 In {0:#ListOfSubdom()-1}
      iDom = ListOfSubdom(i0);
        { Name SysVol~{iDom}; NameOfFormulation FormVol~{iDom}; Type Complex; NameOfMesh Sprintf[StrCat[MSH_NAME, "_%g.msh"], iDom]; }
      For i1 In {0:#ListOfEdgesWithTBC~{iDom}()-1}
      iEdge = ListOfEdgesWithTBC~{iDom}(i1);
        { Name SysSur~{iDom}~{iEdge}; NameOfFormulation FormSur~{iDom}~{iEdge}; Type Complex; NameOfMesh Sprintf[StrCat[MSH_NAME, "_%g.msh"], iDom]; }
      EndFor
      EndFor
    }
    Operation {

      If(FLAG_ERROR == ERROR_NUM)
        Generate[SysRef]; Solve[SysRef]; PostOperation[PostOpRef];
      EndIf

//    For MAXITeff In {1:50}

      // =========================================================
      // BUILD local part of distributed rhs b for Krylov solver using physical sources only, and update surface data
      // =========================================================

      SetCommSelf;

      // SolveVolumePDE (without artificial/with physical sources)
      Evaluate[$ArtificialSource = 0];
      Evaluate[$PhysicalSource = 1];
      For i0 In {0:#ListOfSubdom()-1}
      iDom = ListOfSubdom(i0);
        UpdateConstraint[SysVol~{iDom}, GammaD~{iDom}, Assign];
        Generate[SysVol~{iDom}];
        Solve[SysVol~{iDom}];
      EndFor

    If(#ListOfSubdom() > 1)

      // SolveSurfacePDE
      For i0 In {0:#ListOfSubdom()-1}
      iDom = ListOfSubdom(i0);
      For i1 In {0:#ListOfEdgesWithTBC~{iDom}()-1}
      iEdge = ListOfEdgesWithTBC~{iDom}(i1);
        Generate[SysSur~{iDom}~{iEdge}];
        Solve[SysSur~{iDom}~{iEdge}];
      EndFor
      EndFor

      // UpdateSurfaceFields
      For i0 In {0:#ListOfSubdom()-1}
      iDom = ListOfSubdom(i0);
      For i1 In {0:#ListOfEdgesWithTBC~{iDom}()-1}
      iEdge = ListOfEdgesWithTBC~{iDom}(i1);
        PostOperation[PostOpSur~{iDom}~{iEdge}];
      EndFor
      EndFor

      // =========================================================
      // Krylov solver using artificial sources only. IterativeLinearSolver solves (I-A) g = b
      // ListOfFields() initially stores the local part of b; then stores each local part of iterate g^n.
      // =========================================================

      // UpdateConstraint (with artificial/without physical sources)
      Evaluate[$ArtificialSource = 1];
      Evaluate[$PhysicalSource = 0];
      For i0 In {0:#ListOfSubdom()-1}
      iDom = ListOfSubdom(i0);
        UpdateConstraint[SysVol~{iDom}, GammaD~{iDom}, Assign];
      EndFor

      SetCommWorld;

      IterativeLinearSolver["I-A", SOLVER, TOL, MAXIT, RESTART, {ListOfFields()}, {ListOfConnectedFields()}, {}]
//      IterativeLinearSolver["I-A", SOLVER, TOL, MAXITeff, RESTART, {ListOfFields()}, {ListOfConnectedFields()}, {}]
      {

        SetCommSelf;

        // SolveVolumePDE
        For i0 In {0:#ListOfSubdom()-1}
        iDom = ListOfSubdom(i0);
          GenerateRHSGroup[SysVol~{iDom}, Region[{GammaD~{iDom}, Sigma~{iDom}, Corner~{iDom}}] ];
          SolveAgain[SysVol~{iDom}];
        EndFor

        // SolveSurfacePDE
        For i0 In {0:#ListOfSubdom()-1}
        iDom = ListOfSubdom(i0);
        For i1 In {0:#ListOfEdgesWithTBC~{iDom}()-1}
        iEdge = ListOfEdgesWithTBC~{iDom}(i1);
          GenerateRHSGroup[SysSur~{iDom}~{iEdge}, Region[{Sigma~{iDom}~{iEdge}, SigmaSide~{iDom}~{iEdge}~{0}, SigmaSide~{iDom}~{iEdge}~{1}}]];
          SolveAgain[SysSur~{iDom}~{iEdge}];
        EndFor
        EndFor

        // UpdateSurfaceFields
        For i0 In {0:#ListOfSubdom()-1}
        iDom = ListOfSubdom(i0);
        For i1 In {0:#ListOfEdgesWithTBC~{iDom}()-1}
        iEdge = ListOfEdgesWithTBC~{iDom}(i1);
          PostOperation[PostOpSur~{iDom}~{iEdge}];
        EndFor
        EndFor

        SetCommWorld;

        Barrier;
        Test[ $KSPIteration == 0 ]{
          Print[ {$KSPResidual}, Format "%12.5e", File Sprintf("out/STAT_residualInit_%g_%g_%g_%g_%g_%g.dat", FLAG_PBM, N_DOM, nPadeABC, thetaPadeInputABC, nPadeTBC, CROSSPOINT_TREAT) ];
          //Print[ {$KSPResidual}, Format "%12.5e", File Sprintf("out/STAT_residualInit_%g_%g_%g_%g_%g_%g_%g.dat", FLAG_PBM, N_DOM, nPadeABC, thetaPadeInputABC, nPadeTBC, thetaPadeInputTBC, CROSSPOINT_TREAT) ];
        }
        //Print[ {$KSPResidual}, Format "%12.5e", File Sprintf("out/STAT_solverRes_%g_%g_%g_%g_%g_%g_%g_%g_%g_%g.dat", FLAG_PBM, N_DOM, nPadeABC, thetaPadeInputABC, nPadeTBC, thetaPadeInputTBC, CROSSPOINT_TREAT, WAVENUMBER, N_LAMBDAtextsf) ];
      }

      //Print[ {FLAG_PBM, N_DOM, nPadeABC, thetaPadeInputABC, nPadeTBC, thetaPadeInputTBC, CROSSPOINT_TREAT, WAVENUMBER, N_LAMBDA, $KSPIts}, Format "%g %g %g %g %g %g %g %g %g %g", File Sprintf("out/STAT_solverNumIter.dat") ];
      Evaluate[$NumIter = $KSPIts];
      Evaluate[$ResidualFinal = $KSPResidual];

      //Print[ {$KSPResidual}, Format "%12.5e", File Sprintf("out/STAT_solverRes_%g_%g_%g_%g_%g_%g_%g_%g_%g.dat", FLAG_PBM, N_DOM, nPadeABC, thetaPadeInputABC, nPadeTBC, thetaPadeInputTBC, CROSSPOINT_TREAT, WAVENUMBER, N_LAMBDA) ];

    EndIf

      SetCommSelf;

      // Compute final solution
      Evaluate[$ArtificialSource = 1];
      Evaluate[$PhysicalSource = 1];
      For i0 In {0:#ListOfSubdom()-1}
      iDom = ListOfSubdom(i0);
        UpdateConstraint[SysVol~{iDom}, GammaD~{iDom}, Assign];
        Generate[SysVol~{iDom}];
        Solve[SysVol~{iDom}];
      EndFor

      // Save final solution
      For i0 In {0:#ListOfSubdom()-1}
      iDom = ListOfSubdom(i0);
        PostOperation[PostOpVol~{iDom}];
      EndFor

      SetCommWorld;

      // Compute error
      If(FLAG_ERROR != ERROR_NO)
        Evaluate[$error2Var = 0];
        Evaluate[$energy2Var = 0];
        For i0 In {0:#ListOfSubdom()-1}
        iDom = ListOfSubdom(i0);
          PostOperation[PostOpStat~{iDom}];
          Evaluate[$error2Var = $error2Var + $error2Var~{iDom}];
          Evaluate[$energy2Var = $energy2Var + $energy2Var~{iDom}];
        EndFor
      EndIf
      PostOperation[PostOpStat];

//    EndFor

    }
  }
}


// ====================================================================================================
// POST-PROCESSING
// ====================================================================================================

PostProcessing {

// REFERENCE SOLVER

  { Name PostProRef; NameOfFormulation FormRef;
    PostQuantity {
//      { Name cNumREF; Value { Local { [ cData[] ];              In Omega; Jacobian JVol; }}}
      { Name uNumREF; Value { Local { [ {uRef} ];               In Omega; Jacobian JVol; }}}
    If((FLAG_PBM == PBM_SCATT_CIRC) || (FLAG_PBM == PBM_SCATT_RECT))
      { Name uRefREF; Value { Local { [ f_ref[] ];              In Omega; Jacobian JVol; }}}
      { Name uErrREF; Value { Local { [ Norm[f_ref[]-{uRef}] ]; In Omega; Jacobian JVol; }}}
    EndIf
    }
  }

// SUBDOMAIN SOLVER

For i0 In {0:#ListOfSubdom()-1}
iDom = ListOfSubdom(i0);
  { Name PostProVol~{iDom}; NameOfFormulation FormVol~{iDom};
    PostQuantity {
      { Name uNum~{iDom};    Value { Local { [ {u~{iDom}} ];                    In Omega~{iDom}; Jacobian JVol; }}}
    If(FLAG_ERROR != ERROR_NO)
      { Name uRef~{iDom};    Value { Local { [ f_refErr[] ];                    In Omega~{iDom}; Jacobian JVol; }}}
      { Name uErr~{iDom};    Value { Local { [ Norm[f_refErr[]-{u~{iDom}}]^2 ]; In Omega~{iDom}; Jacobian JVol; }}}
    EndIf
    }
  }
EndFor

// INTERFACE CONDITION

For i0 In {0:#ListOfSubdom()-1}
iDom = ListOfSubdom(i0);
For i1 In {0:#ListOfEdgesWithTBC~{iDom}()-1}
iEdge = ListOfEdgesWithTBC~{iDom}(i1);
iEdgeSide~{0} = (iEdge+3)%4;
iEdgeSide~{1} = (iEdge+1)%4;
  { Name PostProSur~{iDom}~{iEdge}; NameOfFormulation FormSur~{iDom}~{iEdge};
    PostQuantity {
      { Name g_out~{iDom}~{iEdge}; Value { Local { [ {g_out~{iDom}~{iEdge}} ]; In Sigma~{iDom}~{iEdge}; Jacobian JSur; }}}
    For iSide In {0:1}
    If (CROSSPOINT_TREAT == CROSSPOINT_Full)
    For i In {1:nPade~{iDom}~{iEdgeSide~{iSide}}}
      { Name g_out~{i}~{iDom}~{iEdge}~{iSide}; Value { Local { [ {g_out~{i}~{iDom}~{iEdge}~{iSide}} ]; In SigmaSide~{iDom}~{iEdge}~{iSide}; Jacobian JLin; }}}
    EndFor
    EndIf
    EndFor
    }
  }
EndFor
EndFor

// STATS (subdomain)

For i0 In {0:#ListOfSubdom()-1}
iDom = ListOfSubdom(i0);
  { Name PostProStat~{iDom}; NameOfFormulation FormVol~{iDom};
    PostQuantity {
    If(FLAG_ERROR != ERROR_NO)
      { Name energy2~{iDom}; Value { Integral { [ Norm[f_refErr[]]^2 ];            In Omega~{iDom}; Jacobian JVol; Integration I1; }}}
      { Name error2~{iDom};  Value { Integral { [ Norm[f_refErr[]-{u~{iDom}}]^2 ]; In Omega~{iDom}; Jacobian JVol; Integration I1; }}}
    EndIf
    }
  }
EndFor

// STATS (global)

If (MPI_Rank == 0)
  { Name PostProStat; NameOfFormulation FormVol~{0};
    PostQuantity {
    If(FLAG_ERROR != ERROR_NO)
      { Name energAbs; Value { Term { Type Global; [Sqrt[$energy2Var]] ;            In Omega~{0}; Jacobian JVol; }}}
      { Name errorAbs; Value { Term { Type Global; [Sqrt[$error2Var]] ;             In Omega~{0}; Jacobian JVol; }}}
      { Name errorRel; Value { Term { Type Global; [Sqrt[$error2Var/$energy2Var]] ; In Omega~{0}; Jacobian JVol; }}}
    EndIf
      { Name numIter;  Value { Term { Type Global; [$NumIter];                      In Omega~{0}; Jacobian JVol; }}}
      { Name residual; Value { Term { Type Global; [$ResidualFinal];                In Omega~{0}; Jacobian JVol; }}}
    }
  }
EndIf
}

// ====================================================================================================
// POST-OPERATIONS
// ====================================================================================================

PostOperation {

// REFERENCE SOLVER

  { Name PostOpRef; NameOfPostProcessing PostProRef;
    Operation {
//      Print [uNumREF, OnElementsOf Omega, StoreInField (FIELD_REF), AtGaussPoints NumGaussPointsTRI ];
      Print [uNumREF, OnElementsOf Omega, StoreInField (FIELD_REF)];
//      Print [uNumREF, OnElementsOf Omega, File StrCat(DIR, "uNumREF.pos") ];
//      Print [cNumREF, OnElementsOf Omega, File StrCat(DIR, "cNumREF.pos") ];
//    If((FLAG_PBM == PBM_SCATT_CIRC) || (FLAG_PBM == PBM_SCATT_RECT))
//      Print [uRefREF, OnElementsOf Omega, File StrCat(DIR, "uRef.pos") ];
//      Print [uErrREF, OnElementsOf Omega, File StrCat(DIR, "uErr.pos") ];
//    EndIf
    }
  }

// SUB-DOMAIN SOLVER

For i0 In {0:#ListOfSubdom()-1}
iDom = ListOfSubdom(i0);
  { Name PostOpVol~{iDom}; NameOfPostProcessing PostProVol~{iDom};
    Operation {
      Print [uNum~{iDom}, OnElementsOf Omega~{iDom}, File StrCat(DIR, Sprintf("uNum_%g.pos", iDom))];
    If(FLAG_ERROR != ERROR_NO)
      Print [uRef~{iDom}, OnElementsOf Omega~{iDom}, File StrCat(DIR, Sprintf("uRef_%g.pos", iDom))];
      Print [uErr~{iDom}, OnElementsOf Omega~{iDom}, File StrCat(DIR, Sprintf("uErr_%g.pos", iDom))];
    EndIf
    }
  }
EndFor

// INTERFACE CONDITION

For i0 In {0:#ListOfSubdom()-1}
iDom = ListOfSubdom(i0);
For i1 In {0:#ListOfEdgesWithTBC~{iDom}()-1}
iEdge = ListOfEdgesWithTBC~{iDom}(i1);
iEdgeSide~{0} = (iEdge+3)%4;
iEdgeSide~{1} = (iEdge+1)%4;
  { Name PostOpSur~{iDom}~{iEdge}; NameOfPostProcessing PostProSur~{iDom}~{iEdge};
    Operation {
      Print [ g_out~{iDom}~{iEdge}, OnElementsOf Sigma~{iDom}~{iEdge}, StoreInField (FIELD~{iDom}~{iEdge}), AtGaussPoints NumGaussPointsLIN ];
    For iSide In {0:1}
    If (CROSSPOINT_TREAT == CROSSPOINT_Full)
    For i In {1:nPade~{iDom}~{iEdgeSide~{iSide}}}
      Print [ g_out~{i}~{iDom}~{iEdge}~{iSide}, OnElementsOf SigmaSide~{iDom}~{iEdge}~{iSide}, StoreInField (FIELD~{i}~{iDom}~{iEdge}~{iSide}), AtGaussPoints NumGaussPointsPNT ];
    EndFor
    EndIf
    EndFor
    }
  }
EndFor
EndFor

// STATS (subdomain)

For i0 In {0:#ListOfSubdom()-1}
iDom = ListOfSubdom(i0);
  { Name PostOpStat~{iDom}; NameOfPostProcessing PostProStat~{iDom};
    Operation {
    If(FLAG_ERROR != ERROR_NO)
      Print [energy2~{iDom}[Omega~{iDom}], OnRegion Omega~{iDom}, Format Table, StoreInVariable $energy2Var~{iDom}];
      Print [error2~{iDom}[Omega~{iDom}],  OnRegion Omega~{iDom}, Format Table, StoreInVariable $error2Var~{iDom}];
    EndIf
    }
  }
EndFor

// STATS (global)

If (MPI_Rank == 0)
  { Name PostOpStat; NameOfPostProcessing PostProStat;
    Operation {
      tmp0 = Sprintf("out/STAT_energAbs_%g_%g_%g_%g_%g_%g.dat",      FLAG_PBM, N_DOM, nPadeABC, thetaPadeInputABC, nPadeTBC, CROSSPOINT_TREAT);
      tmp1 = Sprintf("out/STAT_errorAbs_%g_%g_%g_%g_%g_%g.dat",      FLAG_PBM, N_DOM, nPadeABC, thetaPadeInputABC, nPadeTBC, CROSSPOINT_TREAT);
      tmp2 = Sprintf("out/STAT_errorRel_%g_%g_%g_%g_%g_%g.dat",      FLAG_PBM, N_DOM, nPadeABC, thetaPadeInputABC, nPadeTBC, CROSSPOINT_TREAT);
      tmp3 = Sprintf("out/STAT_numIter_%g_%g_%g_%g_%g_%g.dat",       FLAG_PBM, N_DOM, nPadeABC, thetaPadeInputABC, nPadeTBC, CROSSPOINT_TREAT);
      tmp4 = Sprintf("out/STAT_residualFinal_%g_%g_%g_%g_%g_%g.dat", FLAG_PBM, N_DOM, nPadeABC, thetaPadeInputABC, nPadeTBC, CROSSPOINT_TREAT);
    If(FLAG_ERROR != ERROR_NO)
      Print [energAbs, OnRegion Omega~{0}, SendToServer "Output/1energAbs",       Format Table, File > tmp0];
      Print [errorAbs, OnRegion Omega~{0}, SendToServer "Output/2errorAbs",       Format Table, File > tmp1];
      Print [errorRel, OnRegion Omega~{0}, SendToServer "Output/3errorRel",       Format Table, File > tmp2];
    EndIf
      Print [numIter,  OnRegion Omega~{0}, SendToServer "Output/4Num iterations", Format Table, File > tmp3];
      Print [residual, OnRegion Omega~{0}, SendToServer "Output/5Residual",       Format Table, File > tmp4];
    }
  }
EndIf
}

// ====================================================================================================

DefineConstant[
  R_ = {"ResoDDM", Choices {"ResoRef", "ResoDDM"}, Name "GetDP/1ResolutionChoices", Visible 1},
  C_ = {"-solve -v 3 -bin -v2 -ksp_monitor", Name "GetDP/9ComputeCommand", Visible 0}
  P_ = {"", Name "GetDP/2PostOperationChoices", Visible 1}
];
