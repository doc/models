A non-overlapping HABC-based domain decomposition method (DDM) with
cross-point treatment for 2D Helmholtz problems.

Models developed by Axel Modave, based on GetDDM codes.


Quick start
-----------

Open `main.pro' with Gmsh.


Additional info
---------------

Used in:

  A. Modave, A. Royer, X. Antoine, X. Geuzaine. An optimized Schwarz domain
  decomposition method with cross-point treatment for time-harmonic acoustic
  scattering.  Preprint: https://hal.archives-ouvertes.fr/hal-02432422
