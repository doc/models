// Monopole
// Geometries in separate .geo files
//              

DefineConstant [ // allowing an external call (-set_number), e.g. from Matlab, python or a shell	
	
	// 1) spherical bnd, no PML, voltage excitation, ECE radiant, 2.5D, formulation in E, 1st order elements 
	BND_SHAPE = 2             // 1 for cylindrical, 2 for spherical, 2 for capsular
	USE_PML = 0               // 1 for yes, 0 for no (radiant ECE)
	TERMINAL_EXC = 0          // 0 for voltage excitation, 1 for current excitation
	TYPE_BC_FictitiousBnd = 2 // 0 for non-terminal, 1 for ground, 2 for radiant (Silver-Muller)
	FLAG_MODEL = 0            // 0 for 2.5 D, 1 for 3D
	FORMULATION = 0           // 0 for E, 1 for H
	FE_ORDER = 1              // 1 for order 1, 2 for order 2
	//
	FMIN = 0.5e9              // min freq [Hz]
	FMAX = 4.5e9              // max freq [Hz]   
	NOP_FREQ = 50             // no of computed points
	USE_FINE_MESH = 1         //1 - some very fine mesh coded in the geo file, 0 - only use the info below about nbLambda and nbDelta
	NB_DELTA = 1              // no of elements per skin depth in the monopole, if USE_FINE_MESH = 1, otherwise not used 
	NOP_perLAMBDA_AIR = 5     // no of elements per minimum lambda in air; near the monopole it is finer, see the mesh description in the geo file
	SCALE_CL = 1              // scale the characteristic length
];

// Geometry
a = 1.52e-3;    // monopole radius [m]
b = 2.3*a;      // minimum radius of the ground terminal
L = 50e-3;      // monopole length [m]
rb = 4*L;       // position of the fictitious boundary
xb = rb;        // needed for the cylindrical bnd and the capsular bnd
yb = rb;        // needed for the cylindrical bnd (not needed for the capsular bnd where yb = xb + L)
zb = rb;        // needed in 3D

// ===============================
// value of d indicates how the feeding is done:
// d = 0 means no gap
// d > 0 means a gap, the lenght of the gap is d
// d < 0 is the coax case, -d is the length of the coax that feeds the monopole

// uncomment the appropriate line below

// no gap
DefineConstant[
	d = { 0, Choices{-4*a="-4*a", -2*a="-2*a", -a="-a", 0="0", a/2="a/2", a="a", 2*a="2*a"}, 
		Name "{Geometrical Param./0Choose type of gap", 
		Help "if positive: gap with hard source feeding; if negative: coaxial with length -d",
    	Highlight "Red", Visible 1}

	Flag_bndShape = {BND_SHAPE, 
		Choices{1="Cylindrical",2="Spherical", 3="Capsular"}, 
		Name "{Geometrical Param./1Choose fictitious bnd shape",
    	Highlight "Tomato",Visible (!d)}

	_use_pml = {USE_PML, 
		Choices{0,1}, Name "{Geometrical Param./2Use PML?",
    	Highlight "Pink",Visible (!d)}

 	 Flag_AnalysisType = {TERMINAL_EXC, 
		Choices{0="voltage excitation", 1="current excitation"}, Highlight "ForestGreen",
    	Name "{Feed/Choose excitation type", ServerAction Str["Reset","GetDP/1ResolutionChoices"]}

	Flag_AnalysisTypeExtBnd = {TYPE_BC_FictitiousBnd, 
		Choices{0="non-terminal",1="ground (PEC)", 2="Radiant ECE"}, 
	Name "{Geometrical Param./3Choose BC backing the PML layer",
	Highlight "Tomato",ServerAction Str["Reset", "Geometrical Param."],Visible (_use_pml)}

	Flag_3Dmodel = {FLAG_MODEL, 
		Choices {0,1}, 
		Name "{Geometrical Param./0Use 3D model?", Highlight "Pink",Visible (!d)}
	ang_section_degrees = {360, Max 360, Name "{Geometrical Param./Wedge angle", Units "°", Visible Flag_3Dmodel,	
    	Highlight "Pink"} // => cuts not be added for constraints

	Flag_AnalysisTypeFormulation = {FORMULATION, 
	Choices{0="E-formulation", 1="H-formulation"}, Highlight "ForestGreen",
    Name "{FE Param./Choose formulation", ServerAction Str["Reset","GetDP/1ResolutionChoices"]},
  
	FE_Order = {FE_ORDER, Choices{1="1st order elements", 2="2nd order elements (hierarchical)"}, Highlight "ForestGreen",
    Name "{FE Param./Choose order", ServerAction Str["Reset","GetDP/1ResolutionChoices"]}	
];

// equivalent to previous If-Else-EndIf
_use_radiantECE = (Flag_AnalysisTypeExtBnd >= 2);
Flag_SilverMuller = (Flag_AnalysisTypeExtBnd == 2);
Flag_PEC = (Flag_AnalysisTypeExtBnd == 3);

modelDim = (!Flag_3Dmodel)? 2: 3;
Flag_Axi = (!Flag_3Dmodel)? 1: 0; // If flag set to 0, problem is either 2D planar or 3D
h2Ddepth = (!Flag_3Dmodel)? 2*Pi:360/ang_section_degrees;

// Frequencies
fmin = FMIN;
fmax = FMAX;
nop = NOP_FREQ;
//freqs() = LogSpace[Log10[fmin],Log10[fmax],nop];
freqs() = LinSpace[fmin,fmax,nop];

cGUI2= "{Monopole antenna1";
mGeoF = StrCat[cGUI2,"Frequency information/0"];
colorMGeoF = "Orange";
close_menu = 1;
DefineConstant[
	_use_freq_loop = {0,  Choices{0,1}, Name StrCat[mGeoF, "0Loop on frequencies?"],
	ServerAction Str["Reset", StrCat[mGeoF, "0Working Frequency"]], Highlight Str[colorMGeoF]}
	Freq  = {freqs(0), Choices{freqs()},
		Loop _use_freq_loop, Name StrCat[mGeoF, "0Working Frequency"],
		Units "Hz", Highlight  Str[colorMGeoF], Closed !close_menu }

	PmlDelta = {L/2, Visible _use_pml, // Always defined, but used only if PML, possible freq dependance
		Name StrCat[mGeoF, "0PML thickness"], Units "m"	}

	_use_fineMesh = {USE_FINE_MESH, Choices{0="coarse", 1="fine"}, Highlight "ForestGreen",
    	Name "{FE Mesh/Choose fineness", ServerAction Str["Reset","GetDP/1ResolutionChoices"]}
];

nopPerLambdaAir = NOP_perLAMBDA_AIR;   
s = SCALE_CL; // a factor that multiplies each characteristic length to easily refine the mesh
nbDelta = (!_use_fineMesh) ? NB_DELTA :1;     

// ================================= Material
// Monopole
epsrMonopole = 1;
murMonopole = 1;
sigmaMonopole = 6.6e7;
// Air
epsrAir = 1;
murAir = 1;
sigmaAir = 0;
// Coax conductor  - used only if d < 0 
epsrCoaxCond = 1;
murCoaxCond = 1;
sigmaCoaxCond = 6.6e7;
// Coax dielectric - used only if d < 0
epsrCoaxDiel = 1;
murCoaxDiel = 1;
sigmaCoaxDiel = 1e-3;

eps0 = 8.854187818e-12; // [F/m] - absolute permitivity of vacuum
mu0  = 4.e-7 * Pi;      // [H/m] - absolute permeability of vacuum
nu0 = 1/mu0;

// epsilon and nu for all the materials
epsMonopole = eps0*epsrMonopole;
nuMonopole = 1/(mu0*murMonopole);

epsAir = eps0*epsrAir;
nuAir = 1/(mu0*murAir);

epsCoaxCond = eps0*epsrCoaxCond;   
nuCoaxCond = 1/(mu0*murCoaxCond);

epsCoaxDiel = eps0*epsrCoaxDiel;
nuCoaxDiel = 1/(mu0*murCoaxDiel);

c0 = 3e8;   // [m/s] speed of light in vacuum
lambda = c0/Freq;
k0 = 2*Pi/lambda; // Wave number


// ================================= Some quantities output to GUI
cGUI8= "{Monopole antenna/0";
If (Flag_AnalysisTypeFormulation == 0) // E
	pp0 = StrCat[cGUI8,"Output-e/0"];
	pp1 = StrCat[cGUI8,"Output-e/1"];
	pp2 = StrCat[cGUI8,"Output-e/2"];
	pp3 = StrCat[cGUI8,"Output-e/3"];
Else
	pp0 = StrCat[cGUI8,"Output-h/0"];
	pp1 = StrCat[cGUI8,"Output-h/1"];
	pp2 = StrCat[cGUI8,"Output-h/2"];
	pp3 = StrCat[cGUI8,"Output-h/3"];
EndIf

//==================================
// Indexes of physical entities
// Surfaces for 2.5D and volumes for 3D 
MONOPOLE = 1000;
AIR = 3000;
PML = 3100 ;

// boundaries - lines for 2.5D and surfaces for 3D
TERMINAL       = 2000 ;
GROUND         = 2100 ;
CUT            = 2300 ;

SURFAIRINF = 3333 ;
AXIS = 4444 ;

// used in 3D 
NONTERM = 4000;
CUT1H   = NONTERM+1;

//=======================================================================
// Indexes of physical entities when Gap is non zero or Coax feeding is used
// Surfaces
COAXCONDUCTOR  = 1001; // for d < 0
COAXDIELECTRIC = 3001;

