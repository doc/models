/* FullWave_H_ece_SISO_cplx_2D.pro/

 Problem independent pro file
 for passive, linear domains in Full Wave with radiant ECE boundary conditions.

 It uses a formulation with vec{H} strictly inside the domain and V strictly on the boundary.
 The domain is simply connected, it has only electric terminals.
 The terminals can be voltage or current excited.
 The material inside is linear from all points of view: electric, magnetic, conduction
 It is compulsory that there exists a ground terminal (its voltage is set to zero).
 The post-operation assumes that there is one excited (active) terminal  (excitation equal to 1 - it can be in
 voltage or current excited terminal).
 Frequency domain simulations.
*/

Include "Jacobian_and_Integration.pro"
Include "only_FunctionSpace_and_Formulation_FullWave_H_eceRadiant_cplx_2D_and_AXI_secondOrder.pro"

Resolution {
	{ Name FullWave_H_ece;
		System {
			{ Name Sys_Ele; NameOfFormulation FullWave_H_ece; Type Complex; Frequency Freq;}
		}
		Operation {
			CreateDir[StrCat[modelDir,Dir]];
			SetTime[Freq];               // usefull to save (and print) the current frequency
			//GenerateSeparate[Sys_Ele]; // separate matrices
			Generate[Sys_Ele]; Solve[Sys_Ele]; SaveSolution[Sys_Ele];
			//Print[Sys_Ele];            // save in matlab format or change the format when running getdp 
		}
	}
}


PostProcessing {

	{ Name FW_H_ece; NameOfFormulation FullWave_H_ece;
		Quantity {
			{ Name H;    Value { Local { [ {h} ];        In Vol_FW; Jacobian Vol; }}} 
			{ Name Hz;   Value { Local { [ CompZ[{h}] ]; In Vol_FW; Jacobian Vol; }}}
			{ Name rmsH; Value { Local { [ Norm[{h}] ];  In Vol_FW; Jacobian Vol; }}}
			

			{ Name E;	 Value { Local { [ {d h}/(sigma[]+Complex[0,1]*2*Pi*Freq*epsilon[]) ]; In Vol_FW; Jacobian Vol; }}}
			{ Name Ey;	 Value { Local { [ CompY[{d h}/(sigma[]+Complex[0,1]*2*Pi*Freq*epsilon[])] ]; In Vol_FW; Jacobian Vol; }}}
			
			{ Name J;	 Value { Local { [ {d h}*sigma[]/(sigma[]+Complex[0,1]*2*Pi*Freq*epsilon[]) ]; In Vol_FW; Jacobian Vol; }}}
			{ Name rmsJ; Value { Local { [ Norm[{d h}*sigma[]/(sigma[]+Complex[0,1]*2*Pi*Freq*epsilon[])] ]; In Vol_FW; Jacobian Vol; }}}
			

			{ Name Vterminals;	Value {	Term { [ -{V} ]; In Cut1H; }}}

			{ Name Rin;	Value { Term { [ Re[-{V}] ]; In Cut1H; }}}
			{ Name Xin;	Value { Term { [ Im[-{V}] ]; In Cut1H; }}}
			{ Name AbsZin; Value { Term { [ Norm[ -{V} ] ]; In Cut1H; }}}
			{ Name ArgZin; Value { Term { [Atan2[Im[-{V} ],Re[ -{V} ]]]; In Cut1H; }}}
	  
			{ Name I;         	     
				Value { // h is the depth for 2D problems,  and it has to be 1 for 3D problems
					If(Flag_Axi == 0) 
						Term { [ -2*{I}*h2Ddepth ]; In Cut1H; } 
					Else  // 2D axisymmetric
						Term { [ -{I}*h2Ddepth ]; In Cut1H; } 
					EndIf
				}   
			} 

			{ Name Gin;         	     
				Value { 
					If(Flag_Axi == 0) // 2D 
						Term { [Re[ -2*{I}*h2Ddepth ]]; In Cut1H; } 
					Else
						Term { [Re[ -{I}*h2Ddepth ]]; In Cut1H; } 
					EndIf
				}  
			}	

			{ Name Bin;         	     
				Value { 
					If(Flag_Axi == 0) // 2D 
						Term { [Im[ -2*{I}*h2Ddepth ]]; In Cut1H; } 
					Else
						Term { [Im[ -{I}*h2Ddepth ]]; In Cut1H; } 
					EndIf
				}  
			}

			{ Name AbsYin; 
				Value { 
					If(Flag_Axi == 0) // 2D 
						Term { [ Norm[ -2*{I}*h2Ddepth ] ]; In Cut1H; } 
					Else
						Term { [ Norm[ -{I}*h2Ddepth ] ]; In Cut1H; } 
					EndIf  
				}
			}

			{ Name ArgYin; 
				Value { 
					If(Flag_Axi == 0) // 2D 
						Term { [Atan2[Im[ -2*{I}*h2Ddepth ],Re[ -2*{I}*h2Ddepth ]]]; In Cut1H; } 
					Else
						Term { [Atan2[Im[ -{I}*h2Ddepth ],Re[ -{I}*h2Ddepth ]]]; In Cut1H; } 
					EndIf  
				}
			} 	    
		}
	}
 }
 
 
	  
	
