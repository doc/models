Jacobian {
	{ Name Vol;
		Case {
			If(Flag_Axi) // 2D axisymetric problems
				{ Region All; Jacobian VolAxiSqu; }  
			Else
				{ Region All; Jacobian Vol; }
			EndIf
		}
	}
	{ Name Sur;
		Case {
			If(Flag_Axi) // 2D axisymetric problems
				{ Region All; Jacobian SurAxi; }       
			Else
				{ Region All; Jacobian Sur; }
			EndIf
		}
	}
}

Integration {
	{ Name Int;
		If(FE_Order !=2)  // Adapt gauss integration points with higher order
			Case {
				{ Type Gauss;
					Case {
						{ GeoElement Point; NumberOfPoints  1; }
						{ GeoElement Line; NumberOfPoints  3; }
						{ GeoElement Triangle; NumberOfPoints  3; }
						{ GeoElement Quadrangle; NumberOfPoints  4; }
						{ GeoElement Tetrahedron; NumberOfPoints  4; }
						{ GeoElement Hexahedron; NumberOfPoints  6; }
						{ GeoElement Prism; NumberOfPoints  9; }
						{ GeoElement Pyramid; NumberOfPoints  8; }

						// 2nd order with geometry
						{ GeoElement Line2; NumberOfPoints 4; }
						{ GeoElement Triangle2; NumberOfPoints 12; }
						{ GeoElement Quadrangle2; NumberOfPoints 12; }
						{ GeoElement Tetrahedron2; NumberOfPoints 17; }
					}
				}
			}
		Else
			Case {
				{ Type Gauss;
					Case {
						{ GeoElement Point; NumberOfPoints  1; }
						{ GeoElement Line; NumberOfPoints  10; }
						{ GeoElement Triangle; NumberOfPoints 16 ; }
						{ GeoElement Quadrangle; NumberOfPoints  7; }
						{ GeoElement Tetrahedron; NumberOfPoints  15; }
						{ GeoElement Hexahedron; NumberOfPoints  6; }
						{ GeoElement Prism; NumberOfPoints  9; }
						{ GeoElement Pyramid; NumberOfPoints  8; }

						// 2nd order with geometry
						{ GeoElement Line2; NumberOfPoints 4; }
						{ GeoElement Triangle2; NumberOfPoints 12; }
						{ GeoElement Quadrangle2; NumberOfPoints 12; }
						{ GeoElement Tetrahedron2; NumberOfPoints 17; }
					}
				}
			}
		EndIf
	}
}
