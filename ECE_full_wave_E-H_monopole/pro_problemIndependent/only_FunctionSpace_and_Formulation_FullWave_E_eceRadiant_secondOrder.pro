FunctionSpace {

	{ Name Hcurl_E; Type Form1;
		BasisFunction {
			{ Name se; NameOfCoef ee; Function BF_Edge; 
				Support Dom_FW ; Entity EdgesOf[All, Not Sur_FW]; }
				// additional basis functions for 2nd order interpolation (hierachical)
			If(FE_Order == 2)
				{ Name se2; NameOfCoef ee2; Function BF_Edge_2E;  
					Support Dom_FW; Entity EdgesOf[All, Not Sur_FW]; }        
				// Reduced 2nd order
				{ Name se3a ; NameOfCoef ee3a ; Function BF_Edge_3F_a ;
					Support Dom_FW ; Entity FacetsOf[All, Not Sur_FW] ; }
				{ Name se3b ; NameOfCoef ee3b ; Function BF_Edge_3F_b ;
					Support Dom_FW ; Entity FacetsOf[ All, Not Sur_FW] ; }
				// 2nd order
				{ Name se4; NameOfCoef ee4; Function BF_Edge_4E;
					Support Dom_FW ; Entity EdgesOf[All, Not Sur_FW]; }
			EndIf
			If (_use_radiantECE)
				{ Name sn; NameOfCoef vn; Function BF_GradNode; 
					Support Dom_FW ; Entity NodesOf[Sur_FW, Not Sur_Terminals_FWeceAndInf]; }
				If(FE_Order == 2)
					// GradNode BFs
				{ Name sn2; NameOfCoef vn2; Function BF_GradNode_2E;
					Support Dom_FW ; Entity EdgesOf[Sur_FW, Not Sur_Terminals_FWeceAndInf]; }
				EndIf				 
			Else
				{ Name sn; NameOfCoef vn; Function BF_GradNode; 
					Support Dom_FW ; Entity NodesOf[Sur_FW, Not Sur_Terminals_FWece]; } 
				If(FE_Order == 2)
					// GradNode BFs
					{ Name sn2; NameOfCoef vn2; Function BF_GradNode_2E;
					Support Dom_FW ; Entity EdgesOf[Sur_FW, Not Sur_Terminals_FWece]; }
				EndIf		 
			EndIf	 
	  
			{ Name sf; NameOfCoef vf; Function BF_GradGroupOfNodes; 
				Support Dom_FW ; Entity GroupsOfNodesOf[Sur_Terminals_FWece]; }
			If (_use_radiantECE)
				{ Name seInf; NameOfCoef eeInf; Function BF_Edge; 
					Support Dom_FW ; Entity EdgesOf[SurfAirInf]; }
				If (1==0) // (FE_Order == 2)
					{ Name se2Inf; NameOfCoef ee2Inf; Function BF_Edge_2E;  
						Support Dom_FW; Entity EdgesOf[SurfAirInf]; }        
					// Reduced 2nd order
					{ Name se3aInf ; NameOfCoef ee3aInf ; Function BF_Edge_3F_a ;
						Support Dom_FW ; Entity FacetsOf[SurfAirInf] ; }
					{ Name se3bInf ; NameOfCoef ee3bInf ; Function BF_Edge_3F_b ;
						Support Dom_FW ; Entity FacetsOf[SurfAirInf] ; }
					// 2nd order
					{ Name se4Inf; NameOfCoef ee4Inf; Function BF_Edge_4E;
						Support Dom_FW ; Entity EdgesOf[SurfAirInf]; }
				EndIf
			EndIf				 
		}
		GlobalQuantity {
			{ Name TerminalPotential; Type AliasOf       ; NameOfCoef vf; }
			{ Name TerminalCurrent  ; Type AssociatedWith; NameOfCoef vf; }
		}

		SubSpace {
			{ Name dv ; NameOfBasisFunction {sn}; } // Subspace, it maybe use in equations or post-pro
			{ Name dvf ; NameOfBasisFunction {sf}; } 
		}

		Constraint {
			{ NameOfCoef TerminalPotential; EntityType GroupsOfNodesOf;
				NameOfConstraint SetTerminalPotential; }
			{ NameOfCoef TerminalCurrent; EntityType GroupsOfNodesOf;
				NameOfConstraint SetTerminalCurrent; }
			If (_use_radiantECE)	
				If (Flag_SilverMuller==0) // classic, PEC
					If (Flag_PEC  == 1)
						{ NameOfCoef eeInf; EntityType EdgesOf;  //ee are voltages along edges
						NameOfConstraint ElectricVoltages;} // impose voltages along edges with known Et
					EndIf
				EndIf
			EndIf	  
		}
	}
}
 
Formulation {

	{ Name FullWave_E_ece; Type FemEquation;
		Quantity {
			{ Name e;  Type Local; NameOfSpace Hcurl_E; }
			{ Name dv; Type Local; NameOfSpace Hcurl_E[dv]; } // Just for post-processing issues
			{ Name dvf; Type Local; NameOfSpace Hcurl_E[dvf]; } 
			{ Name V;  Type Global; NameOfSpace Hcurl_E[TerminalPotential]; }
			{ Name I;  Type Global; NameOfSpace Hcurl_E[TerminalCurrent]; }
		}
		Equation {
			// \int_D curl(\vec{E}) \cdot  curl(\vec{e}) dv
			Galerkin { [ nu[] * Dof{d e} , {d e} ]; In Vol_FW; Jacobian Vol; Integration Int; }

			// \int_D j*\omega*(\sigma + j*\omega*\epsilon) \vec{E} \cdot \vec{e} dv
			Galerkin { DtDof   [ sigma[]   * Dof{e} , {e} ]; In Vol_FW; Jacobian Vol; Integration Int; }
			Galerkin { DtDtDof [ epsilon[] * Dof{e} , {e} ]; In Vol_FW; Jacobian Vol; Integration Int; }

			// j*\omega*sum (vk Ik);  for k - all terminals so that the currents through the terminals will be computed as well
			GlobalTerm {DtDof [ -Dof{I} , {V} ]; In Sur_Terminals_FWece; }  
	  
			If (_use_radiantECE && Flag_SilverMuller)
					Galerkin { DtDof [ Sqrt[epsilonNu[]] * ( Normal[] /\ Dof{e} ) /\ Normal[] , {e} ];
						In SurfAirInf;  Jacobian Sur; Integration Int; }
			EndIf

		}
	}
}
