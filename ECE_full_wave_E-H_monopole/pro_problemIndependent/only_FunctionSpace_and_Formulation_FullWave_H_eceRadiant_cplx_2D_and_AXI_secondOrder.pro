//=========================================================
// Function spaces
//=========================================================

FunctionSpace {
  
	{ Name Hcurl_H; Type Form1P;  // Hspace for 2D problems with H perpendicular to the plane
		BasisFunction {
	
			If (_use_radiantECE)
				{ Name se; NameOfCoef he; Function BF_PerpendicularEdge;
					Support Dom_FW; Entity NodesOf[All, Not Cut1H]; }
				{ Name sc; NameOfCoef ic; Function BF_GroupOfPerpendicularEdges;
					Support Dom_FW; Entity GroupsOfNodesOf[Cut1H]; }
      
				If(FE_Order == 2) // hierarchical basis functions for 2nd order interpolation 
					{ Name se2; NameOfCoef he2; Function BF_PerpendicularEdge_2E;
						Support Dom_FW; Entity EdgesOf[All, Not Cut1H]; }
					{ Name sf2; NameOfCoef hf2; Function BF_PerpendicularEdge_2F;
						Support Dom_FW; Entity FacetsOf[All, Not Cut1H]; }
				EndIf
			Else
				{ Name se; NameOfCoef he; Function BF_PerpendicularEdge;
					Support Dom_FW; Entity NodesOf[All, Not BoundaryNotTerminal]; }
				{ Name sc; NameOfCoef ic; Function BF_GroupOfPerpendicularEdges;
					Support Dom_FW; Entity GroupsOfNodesOf[BoundaryNotTerminal]; }
      
				If(FE_Order == 2) // hierarchical basis functions for 2nd order interpolation 
					{ Name se2; NameOfCoef he2; Function BF_PerpendicularEdge_2E;
						Support Dom_FW; Entity EdgesOf[All, Not BoundaryNotTerminal]; }
					{ Name sf2; NameOfCoef hf2; Function BF_PerpendicularEdge_2F;
						Support Dom_FW; Entity FacetsOf[All, Not BoundaryNotTerminal]; }
				EndIf
			EndIf  
		}

		GlobalQuantity {
			{ Name TerminalCurrent;   Type AliasOf       ; NameOfCoef ic; }
			{ Name TerminalPotential; Type AssociatedWith; NameOfCoef ic; }
		}

		Constraint {
			{ NameOfCoef TerminalPotential; EntityType Auto; NameOfConstraint SetTerminalPotentialH; }
			{ NameOfCoef TerminalCurrent;   EntityType Auto; NameOfConstraint SetTerminalCurrentH; }
     
			{ NameOfCoef he; EntityType Auto; NameOfConstraint ImposeHonAxis; }
			If(FE_Order==2)
				{ NameOfCoef he2; EntityType Auto; NameOfConstraint ImposeHonAxis; }  
				{ NameOfCoef hf2; EntityType Auto; NameOfConstraint ImposeHonAxis; }
			EndIf
			If (_use_radiantECE && Flag_SilverMuller==0  && Flag_PEC  == 0)
				Printf(" ======================================================================== mg voltages set to zero on SurfInf");
				{ NameOfCoef he; EntityType EdgesOf;    // he are magnetic voltages along edges
					NameOfConstraint MagneticVoltages;} // impose voltages along edges with known Ht
			EndIf	
		}
	}
}

Function{
  Omega = 2*Pi*Freq;
  jOmega[] = Complex[0,1]*Omega;
  jOmega2[] = -Omega^2;
  
  tensSuma[] = sigma[]/jOmega[] + epsilon[];
  alpha[] = 1/tensSuma[];
  alphaIm[] = Complex[0,1]*Im[alpha[]];
  alphaRe[] = Re[alpha[]];
  
}

Formulation {
  
	{ Name FullWave_H_ece; Type FemEquation;
		Quantity {
			{ Name h; Type Local;  NameOfSpace Hcurl_H; }
			{ Name V; Type Global; NameOfSpace Hcurl_H[TerminalPotential]; }
			{ Name I; Type Global; NameOfSpace Hcurl_H[TerminalCurrent]; }
		}
		Equation {
			// multiplying with j omega
			// \int_D \alpha * curl(\vec{H}) \cdot  curl(\vec{h}) dv
	  
			Galerkin { [ alpha[] * Dof{d h} , {d h} ];      In Vol_FW; Jacobian Vol; Integration Int; }
	    
			// \int_D j*\omega*(j*\omega*\mu) \vec{H} \cdot \vec{h} dv
			Galerkin { [ jOmega2[] * mu[] * Dof{h} , {h} ]; In Vol_FW; Jacobian Vol; Integration Int; }
	  
			// sum (Vk ik);  for k - all terminals so that the terminal voltages will be computed as well
			GlobalTerm { [ jOmega[]*Dof{V}, {I} ]; In Cut1H; }
	  
			If (_use_radiantECE && Flag_SilverMuller)
				Galerkin { DtDof [ Sqrt[1/epsilonNu[]] * ( Normal[] /\ Dof{h} ) /\ Normal[] , {h} ];
					In SurfAirInf;  Jacobian Sur; Integration Int; }
			EndIf
		}
	}
}
