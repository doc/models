Electric circuit element  (ECE) boundary conditions in full-wave electromagnetic problems: a monopole.

Models developed by G. Ciuprina, D. Ioan and R.V. Sabariego.


This directory contains the pro and geo files necessary to run the test cases in preprint:

Dual Full-Wave Formulations with Radiant Electric Circuit Element Boundary Conditions. Application to Monopole Antenna Modeling

https://papers.ssrn.com/sol3/papers.cfm?abstract_id=4404813


Quick start
-----------
  Open "monopole.pro" with Gmsh.
