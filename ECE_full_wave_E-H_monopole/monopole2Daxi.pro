
Group{
    // Definition of the interface between the mesh file and GetDP
    // Surfaces 
    Monopole = Region[{ MONOPOLE }] ;
    Air = Region[{ AIR }] ;
    Pml = Region[{}] ;
    If (_use_pml)
        Pml += Region[{ PML }] ;  
    EndIf  
    If (d < 0)
        CoaxCond = Region[{ COAXCONDUCTOR }];
        CoaxDiel = Region[{ COAXDIELECTRIC}];
    EndIf

    // Boundary
    Terminal = Region[{ TERMINAL }] ;
    Cut1H = Region[{ CUT }] ;

    Axis = Region[{ AXIS }] ;
    Ground = Region[{ GROUND }] ;

    Sur_Terminals_FWece = Region[{Ground, Terminal}]; // all purely conducting terminals

    // (Default: Flag_AnalysisTypeExtBnd==1) conducting ECE, fictitious boundary set to ground terminal - ~PEC 
    BoundaryNotTerminal = Region[{Cut1H}];
    SurfAirInf = Region[{}];
    If (Flag_AnalysisTypeExtBnd != 1)   
        SurfAirInf += Region[{ SURFAIRINF }] ;
        BoundaryNotTerminal += Region[{SurfAirInf}];
    EndIf

    Sur_FW = Region[{Sur_Terminals_FWece, BoundaryNotTerminal}]; //all boundary
    If(_use_radiantECE)
        Sur_Terminals_FWeceAndInf  =  Region[{Sur_Terminals_FWece, SurfAirInf}];
    EndIf

    Vol_FW = Region[{Monopole,Air}];
    If (d<0)    
        Vol_FW += Region[{CoaxCond,CoaxDiel}];
    EndIf  
    If (_use_pml)
        Vol_FW += Region[{Pml}];
    EndIf  
    Dom_FW = Region[ {Vol_FW, Sur_FW, Axis} ];
}

Function{
    // Material properties are defined piecewise, for elementary groups
    // the values in the right hand side are defined in monopole_data.pro

    // monopole
    nu[#{Monopole}] = nuMonopole;        // magnetic reluctivity [m/H]
    epsilon[#{Monopole}] = epsMonopole;  // permittivity [F/m]
    sigma[#{Monopole}] = sigmaMonopole;  // conductivity [S/m]
    If (d>=0)
        nu[#{Terminal}] = nuMonopole;
        epsilon[#{Terminal}] = epsMonopole;
        sigma[#{Terminal}] = sigmaMonopole;
    EndIf

    // air 
    nu[#{Air,Ground}] = nuAir;
    epsilon[#{Air,Ground}] = epsAir;
    sigma[#{Air,Ground}] = sigmaAir;
    If (d>=0)
        nu[#{Cut1H}] = nuAir;
        epsilon[#{Cut1H}] = epsAir;
        sigma[#{Cut1H}] = sigmaAir;
    EndIf
 
    // functions needed for PML only - begin
    k[]= k0; 
    If (Flag_bndShape == 2) // SPHERICAL DOMAIN
        Printf('=========================================> PML - (2) spherical boundary');
        R[] = Sqrt[X[]^2 + Y[]^2 + Z[]^2];
        rLoc[] = R[]-rb;
        absFuncS[] = 1/(PmlDelta-rLoc[]);
        absFuncF[] = -Log[1-rLoc[]/PmlDelta];
      
        s1[] = Complex[1,-absFuncS[]/k[]] ; //cR
        s2[] = Complex[1,-(1/R[])*absFuncF[]/k[]] ; //cStretch
        nVec[] = XYZ[]/R[];
        tVec[] = nVec[] /\ Vector[0,0,1];
        nTen[] = SquDyadicProduct[nVec[]];
        tTen[] = SquDyadicProduct[tVec[]];
        pmlScal[] = s1[]*s2[];
        pmlTens[] = (s2[]/s1[]) * nTen[] + (s1[]/s2[]) * tTen[];
        tens[] = TensorSym[CompXX[pmlTens[]], CompXY[pmlTens[]], 0, 
                    CompYY[pmlTens[]], 0, 
                    (CompZZ[pmlTens[]]!=0) ? CompZZ[pmlTens[]]: pmlScal[] ];    
    ElseIf (Flag_bndShape == 1) // CYLINDRICAL DOMAIN
        Printf('=========================================> PML - (1) Cylindrical boundary');
        xLoc[] = Fabs[X[]]-xb;
        yLoc[] = Fabs[Y[]]-yb;
        zLoc[] = Fabs[Z[]]-zb;
        DampingProfileX[] = (xLoc[]>=0) ? 1 / (PmlDelta-xLoc[]) : 0 ;
        DampingProfileY[] = (yLoc[]>=0) ? 1 / (PmlDelta-yLoc[]) : 0 ;
        DampingProfileZ[] = (zLoc[]>=0) ? 1 / (PmlDelta-zLoc[]) : 0 ;

        cX[] = Complex[1,-DampingProfileX[]/k0] ;
        cY[] = Complex[1,-DampingProfileY[]/k0] ;
        cZ[] = (Flag_3Dmodel==1) ? Complex[1,-DampingProfileZ[]/k0] : 1. ;

        t11[] = cY[]*cZ[]/cX[];
        t22[] = cX[]*cZ[]/cY[];
        t33[] = cX[]*cY[]/cZ[] ;
        tens[] = TensorDiag[ t11[], t22[], t33[] ] ;
    Else // CAPSULAR DOMAIN 
        Printf('=========================================> PML - (3) capsular boundary');
        R[] = Sqrt[X[]^2 + (Y[]-L)^2 + Z[]^2]; 
        rLoc[] = R[]-rb;
        absFuncS[] = 1/(PmlDelta-rLoc[]);
        absFuncF[] = -Log[1-rLoc[]/PmlDelta];
   
        s1[] = Complex[1,-absFuncS[]/k[]] ; //cR
        s2[] = Complex[1,-1/R[]*absFuncF[]/k[]] ; //cStretch
    
        xLoc[] = X[]-rb;
        absFuncSX[] = (xLoc[]>=0) ? 1/(PmlDelta-xLoc[]) : 0 ;
        s1X[] = Complex[1,-absFuncSX[]/k[]] ; //cX[]
    
        yLoc[] = Y[]-rb;
        absFuncSY[] = (yLoc[]>=0) ? 1/(PmlDelta-yLoc[]) : 0 ;
        s1Y[] = Complex[1,-absFuncSY[]/k[]] ; //cY[]

        nVec[] = XYZ[]/R[];
        tVec[] = nVec[] /\ Vector[0,0,1];
        nTen[] = SquDyadicProduct[nVec[]];
        tTen[] = SquDyadicProduct[tVec[]];
        pmlScal[] = s1[]*s2[];
        pmlTens[] = (s2[]/s1[]) * nTen[] + (s1[]/s2[]) * tTen[];

        tens[] = TensorSym[(Fabs[Y[]]>=L) ? CompXX[pmlTens[]]: s1Y[]/s1X[], 
                    (Fabs[Y[]]>=L) ? CompXY[pmlTens[]]: 0, 
                    0, 
                    (Fabs[Y[]]>=L) ? CompYY[pmlTens[]]: s1X[]/s1Y[], 
                    0, 
                    (Fabs[Y[]]>=L) ? ((CompZZ[pmlTens[]]!=0) ? CompZZ[pmlTens[]]:pmlScal[]) : s1X[]*s1Y[]];
    EndIf
  
    epsilon[ #{Pml}  ] = eps0 * tens[] ;
    nu[ #{Pml}  ] = nu0 / tens[] ;

    epsilon[ #{SurfAirInf}  ] = eps0 ;
    nu[ #{SurfAirInf}  ] = nu0;

    t11d[] = 1;
    sigma[ #{Pml,SurfAirInf} ] = sigmaAir * TensorDiag[ t11d[], t11d[], t11d[] ];
  
    // coax 
    If (d < 0)
        nu[#{CoaxCond}] = nuCoaxCond;
        epsilon[#{CoaxCond}] = epsCoaxCond;
        sigma[#{CoaxCond}] = sigmaCoaxCond;

        nu[#{CoaxDiel}] = nuCoaxDiel;
        epsilon[#{CoaxDiel}] = epsCoaxDiel;
        sigma[#{CoaxDiel}] = sigmaCoaxDiel;
    EndIf

    mu[] = 1/nu[];

    epsilonNu[] = epsilon[]*nu[] ;

    // excitation - voltage or current
    // No need of Flag_AnalysisType here => constraints
    VTerminal1[] = (Flag_AnalysisTypeFormulation==0) ? -1 : 1;
    ITerminal1[] = (Flag_AnalysisTypeFormulation==0) ? -1 : 1;
}

 Constraint{

    // voltage excited terminals, formulation in E
    { Name SetTerminalPotential; Type Assign;  
        Case {
            { Region Ground; Value 0.; }               
            If(Flag_AnalysisType == 0)
                { Region Terminal; Value VTerminal1[]; }
            EndIf
        }
    }
  
    // current excited terminals, formulation in E
    { Name SetTerminalCurrent; Type Assign;  
        Case {
            If(Flag_AnalysisType == 1)
                { Region Terminal; Value ITerminal1[]/h2Ddepth; }
            EndIf
        }
    }
  
    // current excited terminals, formulation in H, constraints are on the cut
    { Name SetTerminalCurrentH; Type Assign;  
        Case {
            If(Flag_AnalysisType == 1)
                { Region Cut1H; Value ITerminal1[]/h2Ddepth; }
            EndIf
        }
    }
  
    // voltage excited terminals, formulation in H, constraints are on the cut
    { Name SetTerminalPotentialH ;
        Case {
            If(Flag_AnalysisType == 0)
                { Region Cut1H; Value VTerminal1[] ; }
            EndIf
        }
    }
  
    // formulation in H, 2.5D axi, magnetic field strength has to be imposed as zero on the axis
    { Name ImposeHonAxis ;
        Case {
            If(Flag_AnalysisTypeFormulation == 1 && Flag_Axi == 1) // H
                { Region Axis; Value 0 ; }
            EndIf
        }
    }
  
    // formulation in E, classical PEC on the fictitious boundary (classical, used only for some checks)
    { Name ElectricVoltages ;
        Case {
            If(Flag_AnalysisTypeFormulation == 0) // E
                If (_use_radiantECE) // here it means no pml
                    If (Flag_SilverMuller == 0) // no radiant
                        If (Flag_PEC)
                            { Region SurfAirInf; Value 0 ; }  //Et will be imposed to zero on the fictitious boundary
                        EndIf
                    EndIf
                EndIf
            EndIf
        }
    }
  
    // formulation in H, classical PMC on the fictitious boundary (classical, used only for some checks
    { Name MagneticVoltages ;
        Case {
            If(Flag_AnalysisTypeFormulation==1) // H
                If (_use_radiantECE)
                    If (Flag_SilverMuller == 0)
                        If (Flag_PEC==0) // this means PMC
                            { Region SurfAirInf; Value 0 ; }  // radiant, classical, PMC
                        EndIf
                    EndIf
                EndIf
            EndIf
        }
    }
}

modelDir = "../";
// folders where the frequency responses ins Touchstone files will be saved
If (Flag_AnalysisType == 0)                 // voltage excitation 
    If (Flag_AnalysisTypeFormulation == 0)
        Dir = "res/FWeceBC_voltExc/";           // E 
    Else
        Dir = "res/H-FWeceBC_voltExc/";         // H
    EndIf
Else                                        // current excitation
    If (Flag_AnalysisTypeFormulation ==0)
        Dir = "res/FWeceBC_crtExc/";            // E 
    Else
        Dir = "res/H-FWeceBC_crtExc/";          // H 
    EndIf
EndIf

// The formulation and its tools
If (Flag_AnalysisTypeFormulation == 0)     // E 
    Include "./pro_problemIndependent/FullWave_E_eceRadiant_SISO_secondOrder.pro"
Else                                       // H, 2.5D
    Include "./pro_problemIndependent/FullWave_H_eceRadiant_SISO_cplx_2D_and_AXI_secondOrder.pro"
EndIf

/* Output results */
Macro Change_post_options
Echo[Str[ "nv = PostProcessing.NbViews;",
    "For v In {0:nv-1}",
    "View[v].NbIso = 25;",
    "View[v].RangeType = 3;" ,// per timestep
    "View[v].ShowTime = 3;",
    "View[v].IntervalsType = 3;",
    "EndFor"
    ], File "post.opt"];
Return

Macro WriteZ_header  // useful for current excitation
    If(Flag_AnalysisTypeFormulation == 0) // E
        Echo[ Str[Sprintf("# Hz Z RI R %g", 50)], Format Table, File > StrCat[Dir, "test_Z_RI_formE.s1p"] ];
    Else                                  // H
        Echo[ Str[Sprintf("# Hz Z RI R %g", 50)], Format Table, File > StrCat[Dir, "test_Z_RI_formH.s1p"] ];
    EndIf
Return

Macro WriteY_header  // useful for voltage excitation
    If(Flag_AnalysisTypeFormulation == 0) // E
        Echo[ Str[Sprintf("# Hz Y RI R %g", 50)], Format Table, File > StrCat[Dir, "test_Y_RI_formE.s1p"] ];
    Else                                  // H
        Echo[ Str[Sprintf("# Hz Y RI R %g", 50)], Format Table, File > StrCat[Dir, "test_Y_RI_formH.s1p"] ];
    EndIf
Return

PostOperation {
    If (Flag_AnalysisTypeFormulation == 0)     // formulation in E

        { Name TransferMatrix; NameOfPostProcessing FW_E_ece ;
            Operation {
                If(Flag_AnalysisType == 1)  // current excitation
                    If (Freq == freqs(0))     // write the headear in the s1p file
                        Call WriteZ_header;
                        Printf("======================> Writting header with first Freq=%g",Freq);
                    EndIf
                    Print [ Vterminals, OnRegion Terminal, Format Table , File  > StrCat[Dir, "test_Z_RI_formE.s1p"]] ;
                Else                       // voltage excitation
                    If (Freq == freqs(0))   // write the headear in the s1p file
                        Call WriteY_header;
                        Printf("======================> Writting header with first Freq=%g",Freq);
                    EndIf
                    Print [ I, OnRegion Terminal, Format Table , File  > StrCat[Dir, "test_Y_RI_formE.s1p"]] ;
                EndIf
        
                If(Flag_AnalysisType==1)  // current excitation
                    Print[ Rin, OnRegion Terminal, Format Table, SendToServer StrCat[pp0,"R=re(Z)"]{0}, Color "Ivory", File StrCat[Dir,"tempRin.dat"] ] ;
                    Print[ Xin, OnRegion Terminal, Format Table, SendToServer StrCat[pp1,"X=im(Z)"]{0}, Color "Ivory", File StrCat[Dir,"tempXin.dat"] ] ;
                    Print[ AbsZin, OnRegion Terminal, Format Table, SendToServer StrCat[pp2,"|Z|"]{0}, Color "Ivory", File StrCat[Dir,"tempAbsZin.dat"] ];
                    Print[ ArgZin, OnRegion Terminal, Format Table, SendToServer StrCat[pp3,"arg(Z) [rad]"]{0}, Color "Ivory", File StrCat[Dir,"tempArgZin.dat"] ] ;
                Else // voltage excitation
                    Print[ Yin, OnRegion Terminal, Format Table, SendToServer StrCat[pp0,"G=re(Y)"]{0}, Color "Ivory", File StrCat[Dir,"tempGin.dat"] ] ;
                    Print[ Yin, OnRegion Terminal, Format Table, SendToServer StrCat[pp1,"B=im(Y)"]{1}, Color "Ivory", File StrCat[Dir,"tempBin.dat"] ] ;

                    Print[ AbsYin, OnRegion Terminal, Format Table, SendToServer StrCat[pp2,"|Y|"]{0}, Color "Ivory", File StrCat[Dir,"tempAbsYin.dat"] ];
                    Print[ ArgYin, OnRegion Terminal, Format Table, SendToServer StrCat[pp3,"arg(Y) [rad]"]{0}, Color "Ivory", File StrCat[Dir,"tempArgYin.dat"] ] ;
                EndIf
            }
        }

        { Name Maps; NameOfPostProcessing FW_E_ece ;
            Operation {
                Print [ gradV, OnElementsOf ElementsOf[Vol_FW,OnOneSideOf Sur_FW], File StrCat[Dir,"map_gradV.pos" ]];
                Print [ Vterminals, OnElementsOf ElementsOf[Sur_FW], File StrCat[Dir,"map_Vterminals.pos" ]];

                Print [ E, OnElementsOf Vol_FW, File StrCat[Dir,"map_E.pos" ]];
                Print [ J, OnElementsOf Vol_FW, File StrCat[Dir,"map_J.pos" ]];
                Print [ rmsJ, OnElementsOf Vol_FW, File StrCat[Dir,"map_absJ.pos" ]];

                Print [ H, OnElementsOf Region[{Vol_FW,-Pml}] , File StrCat[Dir,"map_H.pos" ]];

                Print [ VnotTerminals, OnElementsOf Vol_FW, File StrCat[Dir,"map_VnotTerminals.pos" ]];

                If(Flag_AnalysisType == 1)  // current excitation
                    Print[ Rin, OnRegion Terminal, Format Table, SendToServer StrCat[pp0,"R=re(Z)"]{0}, Color "Ivory", File StrCat[Dir,"tempRin.dat"] ] ;
                    Print[ Xin, OnRegion Terminal, Format Table, SendToServer StrCat[pp1,"X=im(Z)"]{0}, Color "Ivory", File StrCat[Dir,"tempXin.dat"] ] ;
                    Print[ AbsZin, OnRegion Terminal, Format Table, SendToServer StrCat[pp2,"|Z|"]{0}, Color "Ivory", File StrCat[Dir,"tempAbsZin.dat"] ];
                    Print[ ArgZin, OnRegion Terminal, Format Table, SendToServer StrCat[pp3,"arg(Z) [rad]"]{0}, Color "Ivory", File StrCat[Dir,"tempArgZin.dat"] ] ;
                Else // voltage excitation
                    Print[ Yin, OnRegion Terminal, Format Table, SendToServer StrCat[pp0,"G=re(Y)"]{0}, Color "Ivory", File StrCat[Dir,"tempGin.dat"] ] ;
                    Print[ Yin, OnRegion Terminal, Format Table, SendToServer StrCat[pp1,"B=im(Y)"]{1}, Color "Ivory", File StrCat[Dir,"tempBin.dat"] ] ;

                    Print[ AbsYin, OnRegion Terminal, Format Table, SendToServer StrCat[pp2,"|Y|"]{0}, Color "Ivory", File StrCat[Dir,"tempAbsYin.dat"] ];
                    Print[ ArgYin, OnRegion Terminal, Format Table, SendToServer StrCat[pp3,"arg(Y) [rad]"]{0}, Color "Ivory", File StrCat[Dir,"tempArgYin.dat"] ] ;
                EndIf
                Call Change_post_options;
            }
        }
    Else                                      // formulation in H
        { Name TransferMatrix; NameOfPostProcessing FW_H_ece ;
            Operation {
                If(Flag_AnalysisType == 1)  // current excitation
                    If (Freq == freqs(0))     // write the headear in the s1p file
                        Call WriteZ_header;
                        Printf("======================> Writting header with first Freq=%g",Freq);
                    EndIf
                    Print [ Vterminals, OnRegion Cut1H, Format Table , File  > StrCat[Dir, "test_Z_RI_formH.s1p"]] ;
                Else                        // voltage excitation
                    If (Freq == freqs(0))      // write the headear in the s1p file
                        Call WriteY_header;
                        Printf("======================> Writting header with first Freq=%g",Freq);
                    EndIf
                    Print [ I, OnRegion Cut1H, Format Table , File  > StrCat[Dir, "test_Y_RI_formH.s1p"]] ;
                EndIf
        
                If(Flag_AnalysisType == 1)  // current excitation
                    Print[ Rin, OnRegion Cut1H, Format Table, SendToServer StrCat[pp0,"R=re(Z)"]{0}, Color "Ivory", File StrCat[Dir,"tempRin.dat"] ] ;
                    Print[ Xin, OnRegion Cut1H, Format Table, SendToServer StrCat[pp1,"X=im(Z)"]{0}, Color "Ivory", File StrCat[Dir,"tempXin.dat"] ] ;
                    Print[ AbsZin, OnRegion Cut1H, Format Table, SendToServer StrCat[pp2,"|Z|"]{0}, Color "Ivory", File StrCat[Dir,"tempAbsZin.dat"] ];
                    Print[ ArgZin, OnRegion Cut1H, Format Table, SendToServer StrCat[pp3,"arg(Z) [rad]"]{0}, Color "Ivory", File StrCat[Dir,"tempArgZin.dat"] ] ;
                Else // voltage excitation
                    Print[ Gin, OnRegion Cut1H, Format Table, SendToServer StrCat[pp0,"G=re(Y)"]{0}, Color "Ivory", File StrCat[Dir,"tempGin.dat"] ] ;
                    Print[ Bin, OnRegion Cut1H, Format Table, SendToServer StrCat[pp1,"B=im(Y)"]{0}, Color "Ivory", File StrCat[Dir,"tempBin.dat"] ] ;
                    Print[ AbsYin, OnRegion Cut1H, Format Table, SendToServer StrCat[pp2,"|Y|"]{0}, Color "Ivory", File StrCat[Dir,"tempAbsYin.dat"] ] ;
                    Print[ ArgYin, OnRegion Cut1H, Format Table, SendToServer StrCat[pp3,"arg(Y) [rad]"]{0}, Color "Ivory", File StrCat[Dir,"tempArgYin.dat"] ] ;
                EndIf
            }
        }
    
        { Name Maps; NameOfPostProcessing FW_H_ece ;
            Operation {
                Print [ E, OnElementsOf Region[{Vol_FW,-Pml}], File StrCat[Dir,"map_E.pos" ]];
                Print [ J, OnElementsOf Region[{Vol_FW,-Pml}], File StrCat[Dir,"map_J.pos" ]];
                Print [ rmsJ, OnElementsOf Region[{Vol_FW,-Pml}], File StrCat[Dir,"map_absJ.pos" ]];

                Print [ H, OnElementsOf Vol_FW, File StrCat[Dir,"map_H.pos" ]];
                
                If(Flag_AnalysisType == 1)  // current excitation
                    Print[ Rin, OnRegion Cut1H, Format Table, SendToServer StrCat[pp0,"R=re(Z)"]{0}, Color "Ivory", File StrCat[Dir,"tempRin.dat"] ] ;
                    Print[ Xin, OnRegion Cut1H, Format Table, SendToServer StrCat[pp1,"X=im(Z)"]{0}, Color "Ivory", File StrCat[Dir,"tempXin.dat"] ] ;
                Else // voltage excitation
                    Print[ Gin, OnRegion Cut1H, Format Table, SendToServer StrCat[pp0,"G=re(Y)"]{0}, Color "Ivory", File StrCat[Dir,"tempGin.dat"] ] ;
                    Print[ Bin, OnRegion Cut1H, Format Table, SendToServer StrCat[pp1,"B=im(Y)"]{0}, Color "Ivory", File StrCat[Dir,"tempBin.dat"] ] ;
                    Print[ AbsYin, OnRegion Cut1H, Format Table, SendToServer StrCat[pp2,"|Y|"]{0}, Color "Ivory", File StrCat[Dir,"tempAbsYin.dat"] ] ;
                    Print[ ArgYin, OnRegion Cut1H, Format Table, SendToServer StrCat[pp3,"arg(Y) [rad]"]{0}, Color "Ivory", File StrCat[Dir,"tempArgYin.dat"] ] ;
                EndIf
                Call Change_post_options;
            }
        }
    EndIf
}
