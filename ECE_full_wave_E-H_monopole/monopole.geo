Include "monopole_data.pro";

If(Flag_3Dmodel==0)         // 2D model
	If (d == 0) // NO GAP
		If (_use_pml == 0)        // no PML
			If (Flag_bndShape == 1) // cylindrical bnd
				Printf("2.5D, cylindrical boundary, ECE radiant");   // 2.5D, cylindrical, ECE radiant
				Include "./geo/monopole2D_cyl_rad.geo";
			ElseIf (Flag_bndShape == 2) // spherical bnd
				Printf("2.5D, spherical boundary, ECE radiant");     // 2.5D, spherical, ECE radiant
				Include "./geo/monopole2D_sph_rad.geo";
			Else // capsular bnd
				Printf("2.5D, capsular boundary, ECE radiant");     // 2.5D, capsular, ECE radiant
				Include "./geo/monopole2D_cap_rad.geo";
			EndIf
		Else  // with PML
			If (Flag_bndShape == 1) // cylindrical bnd
				Printf("2.5D, cylindrical boundary, PML");   		// 2.5D, cylindrical, PML
				Include "./geo/monopole2D_cyl_pml.geo";
			ElseIf (Flag_bndShape == 2) // spherical bnd
				Printf("2D, spherical, PML");
				Include "./geo/monopole2D_sph_pml.geo";            // 2.5D, spherical, PML
			Else // capsular bnd
				Printf("2.5D, capsular boundary, PML");     		// 2.5D, capsular, PML
				Include "./geo/monopole2D_cap_pml.geo";
			EndIf
		EndIf
	Else
		Printf("2.5D, spherical boundary, ECE radiant, gap > 0 or coax feeding");     // 2.5D, spherical, ECE radiant, gap or coax feeding
		If ((Flag_bndShape == 2)&(_use_pml == 0))
			If (d > 0) // gap (hard source feeding)
				Include "./geo/monopole2Dgap_sph_rad.geo";
			Else
				Include "./geo/monopole2Dcoax_sph_rad.geo";
			EndIf
		Else
			Printf("coax and gap, implemented in 2Daxi only with a circular boundary and radECE");
		EndIf
    EndIf
Else // 3D model - only  no gap, with spherical bnd  
    If (d == 0) // NO GAP
		If (_use_pml == 0)        // no PML
			If (Flag_bndShape == 1) // cylindrical bnd
				Printf("no gap, 3D, radiant ECE, cylindrical bnd - not implemented");
				//Include "./geo/monopole3D_cyl_rad.geo";
			ElseIf (Flag_bndShape == 2) // spherical bnd
			    Printf("no gap, 3D, radiant ECE, spherical bnd");  // 3D, spherical, ECE radiant
                //Include "./geo/monopole3D_sph_rad.geo";
			    Include "./geo/monopole3D_sph_rad_new.geo";
			Else // capsular bnd
			    Printf("no gap, 3D, radiant ECE, capsular bnd - not implemented");
				//Include "./geo/monopole3D_cap_rad.geo";
			EndIf
		Else // with PML
			If (Flag_bndShape == 1) // cylindrical bnd
			    Printf("no gap, 3D, PML, cylindrical bnd - not implemented");
				//Include "./geo/monopole3D_cyl_pml.geo";
			ElseIf (Flag_bndShape == 2) // spherical bnd
			    Printf("no gap, 3D, PML, spherical bnd");
				Include "./geo/monopole3D_sph_pml.geo";
			Else // capsular bnd
			    Printf("no gap, 3D, PML, capsular bnd - not implemented");
				//Include "./geo/monopole3D_cap_pml.geo";
			EndIf
		EndIf
	Else
		Printf("coax and gap, not implemented in 3D");
    EndIf
EndIf
