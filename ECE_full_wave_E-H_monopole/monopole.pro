Include "monopole_data.pro"

If(Flag_3Dmodel==0)         // 2D axi model
    Include "monopole2Daxi.pro";
ElseIf (d==0)
    Include "monopole3D.pro";
Else
    Printf("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
    Printf("!!!!!!!!!!!!!!!!   coax and gap, not implemented in 3D    !!!!!!!!!!!!!!!!");
    Printf("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
EndIf
