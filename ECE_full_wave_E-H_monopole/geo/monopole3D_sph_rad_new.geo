Include "../monopole_data.pro";  

//****************************************************** start of geometry
SetFactory("OpenCASCADE");

//Mesh.MeshSizeFactor = s ;
// Mesh.Algorithm3D = 10; // 3D mesh algorithm (1=Delaunay, 4=Frontal, 5=Frontal Delaunay, 6=Frontal Hex, 7=MMG3D, 9=R-tree, 10=HXT)
Mesh.Optimize = 1 ;

// And we set the minimum number of elements per 2*Pi radians:
// We can activate the calculation of mesh element sizes based on curvature:
Mesh.MeshSizeFromCurvature = 1;
Mesh.MinimumElementsPerTwoPi = (_use_fineMesh ? 16 : 8) ; // 24

// Mesh.MeshSizeMax = s*lambda/nopPerLambdaAir/5;
// Mesh.MeshSizeMin = Mesh.MeshSizeMax/10;

ang_section = ang_section_degrees*Pi/180; // => cuts not be added if a section is considered

idxCyl = newv; Cylinder(idxCyl) = {0,0,0,0,0,L,a,ang_section};  // monopole along Oz
// idxDisk = news; Disk(idxDisk) = {0,0,0,b};
idxCylDisk = newv; Cylinder(idxCylDisk) = {0,0,0,0,0,L,b,ang_section};  // for better controlling the mesh
idxSph = newv; Sphere(idxSph) = {0,0,0,rb,0,Pi/2,ang_section};

// For keeping the same rotation axis as in 2D
Rotate{ {1, 0, 0}, {0, 0, 0}, -Pi/2 } { Surface{:};Volume{:}; }

BooleanFragments{Volume{:}; Surface{:}; Delete;}{} // to intersect everything

vol() = Volume{:}; // Not really necessary but... 
Printf("Volumes in the geometry after BooleanFragments for volumes",vol());

// Hunting the volume numbers from the interface (GUI: Tools -> Visibility -> list, click on volumes and see which is which
volMonopole()  = vol(0);
volAir() = vol({1,2});

surf_bnd() = Abs(CombinedBoundary{Volume{:};});
Printf("all boundary surfaces", surf_bnd());

If(ang_section_degrees == 360)
    surfaceFeed() = surf_bnd(0);    
    surfaceAirInf() = surf_bnd(2); 
    surfaceGround() = surf_bnd(3);
    surfaceNonTerm() = surf_bnd(1);
EndIf
If(ang_section_degrees < 360)
    surfaceFeed() = surf_bnd(0);    
    surfaceAirInf() = surf_bnd(6); 
    surfaceGround() = surf_bnd(8);
    surfaceNonTerm() = surf_bnd(4);
    surfaceCutA() = surf_bnd({1,5,9}); 
    surfaceCutB() = surf_bnd({2,3,7}); 
EndIf
  
//=================================================
// Physical regions
//=================================================
// volumes
Physical Volume("Monopole", MONOPOLE) = {volMonopole[]};          // MONOPOLE
Physical Volume("Air",AIR) = {volAir[]};                    // AIR
// surfaces
Physical Surface("Terminal", TERMINAL) = {surfaceFeed[]};         // FEED
Physical Surface("Fictitious bnd - radiant", SURFAIRINF) = {surfaceAirInf[]};
Physical Surface("Ground",GROUND) = {surfaceGround[]};         // GROUND
Physical Surface("Non terminal", NONTERM) = {surfaceNonTerm[]};       // NON Terminal

If(ang_section_degrees < 360)
    // Most likely not needed for the E-formulation
    Periodic Surface {surfaceCutA[]} = {surfaceCutB[]} Rotate {{0,1,0},{0,0,0}, -ang_section};
    Physical Surface("Cut A", SURFAIRINF*10) = {surfaceCutA[]}; 
    Physical Surface("Cut B", SURFAIRINF*10+1) = {surfaceCutB[]}; 
EndIf


// Adapting the mesh... 
// ==============================
If(1)  // frequency dependent lcair
    lcair = s*lambda/nopPerLambdaAir;
Else
    // the same mesh for all the freq
    lambdaMin = c0/fmax; 
    lcair = s*lambdaMin/nopPerLambdaAir;  // the same mesh for all the frequencies, not reccomended in 3d , especially for order 2
EndIf

// nopL = Ceil[L/(a/4)];  // to make the antenna very fine along L
lcMonopol = s*L/10;
MeshSize{PointsOf{Volume{:};}} = (_use_fineMesh) ? lcMonopol*6 : lcMonopol*8;
MeshSize{PointsOf{Volume{volMonopole[]};}}    = (_use_fineMesh) ? lcMonopol : lcMonopol*4;
MeshSize{PointsOf{Surface{surfaceAirInf[]};}} = (_use_fineMesh) ? lcair/2 : lcair; 

If(ang_section_degrees < 360)
    Transfinite Line {9,12,24} = (_use_fineMesh) ? 21 : 11; // div length dipole
    Transfinite Line {19,21,23,22} = (_use_fineMesh) ? 5 : 3; // div radius
    Transfinite Line {6,11} = ang_section_degrees/45*((_use_fineMesh) ? 6 : 3); // lines cylinder
    Transfinite Surface {4,5}; // cuts A & B
    Transfinite Surface {11}; // cylindrical side
    Transfinite Surface {2} = {12,6,5}; // top of terminal
    Transfinite Surface {3} = {13,7,8}; // terminal
    Transfinite Volume {volMonopole[]};
EndIf

// cut for the H formulation
Cohomology(1) {{NONTERM},{}}; // this will generate the cut needed by the formulation in H, with no NONTERM + 1
