Include "../monopole_data.pro";  

//****************************************************** start of geometry
SetFactory("OpenCASCADE");

//Mesh.MeshSizeFactor = s ;
// Mesh.Algorithm3D = 10; // 3D mesh algorithm (1=Delaunay, 4=Frontal, 5=Frontal Delaunay, 6=Frontal Hex, 7=MMG3D, 9=R-tree, 10=HXT)
Mesh.Optimize = 1 ;

ang_section = ang_section_degrees*Pi/180; // => cuts not be added if a section is considered

idxCyl = newv; Cylinder(newv) = {0,0,0,0,0,L,a,ang_section};  // monopole along Oz
idxDisk = news; Disk(news) = {0,0,0,b};
idxSph = newv; Sphere(newv) = {0,0,0,rb,0,Pi/2,ang_section};
idxSphPml = newv; Sphere(newv) = {0,0,0,rb+PmlDelta,0,Pi/2,ang_section};

// For keeping the same rotation axis as in 2D
Rotate{ {1, 0, 0}, {0, 0, 0}, -Pi/2 } { Surface{:};Volume{:}; }

BooleanFragments{Volume{:}; Surface{:}; Delete;}{} // to intersect everything
// Avoid the use of BooleanDifference, it is not trustworthy
If(ang_section_degrees<360)
    Recursive Delete{Surface{17};}
EndIf

vol() = Volume{:}; // Not really necessary but... 
Printf("Volumes in the geometry after BooleanFragments for volumes",vol());

// Hunting the volume numbers from the interface (GUI: Tools -> Visibility -> list, click on volumes and see which is which
volMonopole[]  = vol(0);
volAir[] = vol(1);
volPml[] = vol(2);

surf_bnd() = Abs(CombinedBoundary{Volume{:};});
Printf("all boundary surfaces", surf_bnd());

// to better see the surfaces,Tools -> Options -> Visibility -> Geometry -> Surfaces, Surface Labels and you will see the number of surfaces
// to better see the selected surfaces, Tools -> Options -> Geometry -> Aspect -> Surface Display -> Solid

If(ang_section_degrees == 360)
    surfaceFeed[] = surf_bnd(0);    
    surfaceAirInf[] = surf_bnd(3); 
    surfaceGround[] = surf_bnd({1,4});
    surfaceNonTerm[] = surf_bnd(2);
EndIf
If(ang_section_degrees < 360)
    surfaceFeed[] = surf_bnd(0);    
    surfaceAirInf[] = surf_bnd(7); 
    surfaceGround[] = surf_bnd({4,9});
    surfaceNonTerm[] = surf_bnd(6);
    surfaceCutA[] = surf_bnd({1,5,10});
    surfaceCutB[] = surf_bnd({2,3,8});
EndIf
  
//=================================================
// Physical regions
//=================================================
// volumes
Physical Volume("Monopole", MONOPOLE) = {volMonopole[]}; 
Physical Volume("Air",AIR)  = {volAir[]};
Physical Volume("PML", PML) = {volPml[]};

// surfaces
Physical Surface("Terminal", TERMINAL) = {surfaceFeed[]};      
Physical Surface("Fictitious bnd - radiant", SURFAIRINF) = {surfaceAirInf[]};
Physical Surface("Ground",GROUND) = {surfaceGround[]};         
Physical Surface("Non terminal", NONTERM) = {surfaceNonTerm[]};  

If(ang_section_degrees < 360)
    // Most likely not needed for the E-formulation
    Periodic Surface {surfaceCutA[]} = {surfaceCutB[]} Rotate {{0,1,0},{0,0,0}, -ang_section};

    Physical Surface("Cut A", SURFAIRINF*10) = {surfaceCutA[]}; 
    Physical Surface("Cut B", SURFAIRINF*10+1) = {surfaceCutB[]}; 
EndIf

If (_use_fineMesh == 1)
    // Adapting the mesh if necesary
    MeshSize{PointsOf{Volume{:};}} = L/10;
    MeshSize{PointsOf{Volume{volMonopole[]};}} = L/40;
    MeshSize{PointsOf{Volume{volPml[]};}} = s*lambda/nopPerLambdaAir/4;
    MeshSize{PointsOf{Surface{surfaceAirInf[]};}} = s*lambda/nopPerLambdaAir/2;

    // We can activate the calculation of mesh element sizes based on curvature:
    // Mesh.MeshSizeFromCurvature = 1;

    // And we set the minimum number of elements per 2*Pi radians:
    Mesh.MinimumElementsPerTwoPi = (ang_section_degrees < 360) ? 4*360/ang_section_degrees : 24/2;
    // Mesh.MeshSizeMax = s*lambda/nopPerLambdaAir/5;
    // Mesh.MeshSizeMin = Mesh.MeshSizeMax/10;
Else
    lcair = s*lambda/nopPerLambdaAir;
    nopL = (Ceil[L/(a/4)]);  // to make the antenna very fine along L
    
    no90 = Ceil[Pi*(rb)/2/lcair]; 
	If (no90 < 10)
		lcair = Pi*(rb)/2/10;
	EndIf
    
    MeshSize{PointsOf{Volume{:};}} = lcair;   // does this mean that at first all the volumes have this setting? Why did you chose L/10 and not something depeding on the  frequency?
    Printf("L = %g",L);
    Printf("L/10 = %g",L/10);
    Printf("lcair = %g",lcair);
    Printf("nopL = %g",nopL);
    lcMonopol = L/10; // changed to something smaller so that I can solve it quicker on my laptop
    MeshSize{PointsOf{Volume{volMonopole[]};}} = lcMonopol;
    MeshSize{PointsOf{Volume{volPml[]};}} = lcair;
    MeshSize{PointsOf{Surface{surfaceAirInf[]};}} = lcair;
EndIf

// Transfinite PML
If(ang_section_degrees < 360)
    gg = ang_section_degrees/30;
    ff = 1;
    Transfinite Line {21,22,23} = 4*ff; // width
    Transfinite Line {2, 4, 18, 20} = 10*ff; // arcs radial
    Transfinite Line {3,19} = 5*gg*ff;

    Transfinite Surface {14,15,16};  // sides
    Transfinite Surface {6} = {1,2,3};
    Transfinite Surface {13} ={11,12,13}; 
    Transfinite Volume {volPml[]};
    _use_recombine=0;
    If(_use_recombine)
        Recombine Surface {14,15,16};
        Recombine Surface {6,13};   
        Recombine Volume {volPml[]};
    EndIf   
EndIf

Cohomology(1) {{NONTERM},{}}; // this will generate the cut needed by the formulation in H, with no NONTERM + 1

