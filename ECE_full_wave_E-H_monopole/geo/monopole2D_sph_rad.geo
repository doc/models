Include "../monopole_data.pro";  

fac = s;
flag = 1; // if 0, extruded mesh becomes free in the monopole

If (_use_fineMesh == 1)
	lcdAntennaZero = a/4;
    lcdAntennaA = lcdAntennaZero;
	lambdaMin = c0/fmax; // the same mesh for all the frequencies
	lcd = fac*lambdaMin/nopPerLambdaAir;
	lcair = lcd;

	nopL = flag*(Ceil[L/lcdAntennaZero]);  // to make the antenna very fine along L
Else
	lcdAntennaZero = a;
	lambdaMin = lambda;  // for a frequency dependent mesh, you should uncomment this line
	lcair = fac*lambdaMin/nopPerLambdaAir;
	
	no90 = Ceil[Pi*(rb)/2/lcair];
	lcair = (no90 < 10)? Pi*(rb)/2/10 : no90;
	
	delta = Sqrt(2.0*nuMonopole/(2*Pi*Freq*sigmaMonopole));
	lcdAntennaA = fac*delta/nbDelta;
	
	lcair1 = lambdaMin/(nopPerLambdaAir*2);  
	nopL = fac*3*flag*(Ceil[L/lcair1]);
EndIf
nopL = (nopL < 10) ? 10 : nopL;

Printf("====================================> nopL %g",nopL);
Printf("lcdAntennaA %g lcdAntennaZero %g ratio %g", lcdAntennaA, lcdAntennaZero, lcdAntennaZero/lcdAntennaA);

//=================================================
// Monopole - no gap
//=================================================

p0 = newp ; Point(p0) = {0, 0, 0, lcdAntennaZero};
p1 = newp ; Point(p1) = {a, 0, 0, lcdAntennaA};
LineMonopoleBottom = newl ; Line(LineMonopoleBottom) = {p0,p1};

surf[] = Extrude {0, L, 0} {
  Line{LineMonopoleBottom}; Layers{nopL} ;
};
surfMonopole[0] = surf[1] ; LineMonopoleTop = surf[0] ;

p_[] = Boundary{Line{LineMonopoleTop};};

interfaceMonopoleAir[] = Boundary{ Surface{surfMonopole[]};} ;

axisMonopole[] = interfaceMonopoleAir[{3}] ;
feedMonople[] = interfaceMonopoleAir[{0}] ;
interfaceMonopoleAir[] -= axisMonopole[];
interfaceMonopoleAir[] -= feedMonople[];

//=================================================
// Air 
//=================================================

pZero = newp; Point(pZero) = { 0, 0, 0, lcdAntennaZero};

pp[]+=p1;
pp[]+=newp; Point(newp) = {  b, 0, 0, lcdAntennaZero};

pp[]+=newp; Point(newp) = { rb, 0, 0, lcair};
pp[]+=newp; Point(newp) = {  0, rb, 0, lcair};
pp[]+=p_[0];

For k In {0:1}
	lbox[]+=newl; Line(newl) = {pp[k],pp[k+1]};
EndFor
lbox[]+=newl; Circle(newl) = {pp[2],pZero,pp[3]};
lbox[]+=newl; Line(newl) = {pp[3],pp[4]};
interfaceAirPML[] = lbox[{2}];  // it will be the inf boundary if no PML is used

surfAirLL = newll ; Line Loop(surfAirLL) = {lbox[],-interfaceMonopoleAir[]};
surfAir = news ; Plane Surface(surfAir) = {surfAirLL};


//=================================================
// Physical regions
//=================================================
Include "PhysicalRegions.geo";
