Include "../monopole_data.pro";  

//****************************************************** start of geometry
SetFactory("OpenCASCADE");

//Mesh.MeshSizeFactor = s ;
// Mesh.Algorithm3D = 10; // 3D mesh algorithm (1=Delaunay, 4=Frontal, 5=Frontal Delaunay, 6=Frontal Hex, 7=MMG3D, 9=R-tree, 10=HXT)
Mesh.Optimize = 1 ;

ang_section = ang_section_degrees*Pi/180; // => cuts not be added if a section is considered

idxCyl = newv; Cylinder(idxCyl) = {0,0,0,0,0,L,a,ang_section};  // monopole along Oz
idxDisk = news; Disk(idxDisk) = {0,0,0,b};
idxSph = newv; Sphere(idxSph) = {0,0,0,rb,0,Pi/2,ang_section};

// For keeping the same rotation axis as in 2D
Rotate{ {1, 0, 0}, {0, 0, 0}, -Pi/2 } { Surface{:};Volume{:}; }

BooleanFragments{Volume{:}; Surface{:}; Delete;}{} // to intersect everything
// Avoid the use of BooleanDifference, it is not trustworthy
If(ang_section_degrees<360)
    Recursive Delete{Surface{13};}
EndIf

vol() = Volume{:}; // Not really necessary but... 
Printf("Volumes in the geometry after BooleanFragments for volumes",vol());

// Hunting the volume numbers from the interface (GUI: Tools -> Visibility -> list, click on volumes and see which is which
volMonopole[]  = vol(0);
volAir[] = vol(1);

surf_bnd() = Abs(CombinedBoundary{Volume{:};});
Printf("all boundary surfaces", surf_bnd());

// to better see the surfaces,Tools -> Options -> Visibility -> Geometry -> Surfaces, Surface Labels and you will see the number of surfaces
// to better see the selected surfaces, Tools -> Options -> Geometry -> Aspect -> Surface Display -> Solid

If(ang_section_degrees == 360)
    surfaceFeed[] = surf_bnd(0);    
    surfaceAirInf[] = surf_bnd(1); 
    surfaceGround[] = surf_bnd(2);
    surfaceNonTerm[] = surf_bnd(3);
EndIf
If(ang_section_degrees < 360)
    surfaceFeed[] = surf_bnd(0);    
    surfaceAirInf[] = surf_bnd(3); 
    surfaceGround[] = surf_bnd(5);
    surfaceNonTerm[] = surf_bnd(7);
    surfaceCutA[] = surf_bnd({1,6});
    surfaceCutB[] = surf_bnd({2,4});
EndIf
  
//=================================================
// Physical regions
//=================================================
// volumes
Physical Volume("Monopole", MONOPOLE) = {volMonopole[]};          // MONOPOLE
Physical Volume("Air",AIR) = {volAir[]};                    // AIR
// surfaces
Physical Surface("Terminal", TERMINAL) = {surfaceFeed[]};         // FEED
Physical Surface("Fictitious bnd - radiant", SURFAIRINF) = {surfaceAirInf[]};
Physical Surface("Ground",GROUND) = {surfaceGround[]};         // GROUND
Physical Surface("Non terminal", NONTERM) = {surfaceNonTerm[]};       // NON Terminal

If(ang_section_degrees < 360)
    // Most likely not needed for the E-formulation
    Periodic Surface {surfaceCutA[]} = {surfaceCutB[]} Rotate {{0,1,0},{0,0,0}, -ang_section};
    Physical Surface("Cut A", SURFAIRINF*10) = {surfaceCutA[]}; 
    Physical Surface("Cut B", SURFAIRINF*10+1) = {surfaceCutB[]}; 
EndIf

If (_use_fineMesh == 1)
    // Adapting the mesh if necesary
    MeshSize{PointsOf{Volume{:};}} = L/10;
    MeshSize{PointsOf{Volume{volMonopole[]};}} = L/40;
    MeshSize{PointsOf{Surface{surfaceAirInf[]};}} = s*lambda/nopPerLambdaAir/2; 
    // We can activate the calculation of mesh element sizes based on curvature:
    // Mesh.MeshSizeFromCurvature = 1;

    // And we set the minimum number of elements per 2*Pi radians:
    Mesh.MinimumElementsPerTwoPi = (ang_section_degrees < 360) ? 4*360/ang_section_degrees : 24/2; 
    // Mesh.MeshSizeMax = s*lambda/nopPerLambdaAir/5;
    // Mesh.MeshSizeMin = Mesh.MeshSizeMax/10;
Else
    If(1)  // frequency dependent lcair
        lcair = s*lambda/nopPerLambdaAir;
    Else
        // the same mesh for all the freq
        lambdaMin = c0/fmax; 
        lcair = s*lambdaMin/nopPerLambdaAir;  // the same mesh for all the frequencies, not reccomended in 3d , especially for order 2
    EndIf
    
    //
    nopL = (Ceil[L/(a/4)]);  // to make the antenna very fine along L
    
    no90 = Ceil[Pi*(rb)/2/lcair]; 
	If (no90 < 10)
		lcair = Pi*(rb)/2/10;
	EndIf
    
    MeshSize{PointsOf{Volume{:};}} = lcair;   // does this mean that at first all the volumes have this setting? Why did you chose L/10 and not something depeding on the  frequency?
    Printf("L = %g",L);
    Printf("L/10 = %g",L/10);
    Printf("lcair = %g",lcair);
    Printf("nopL = %g",nopL);
    lcMonopol = L/10; // changed to something smaller so that I can solve it quicker on my laptop
    MeshSize{PointsOf{Volume{volMonopole[]};}} = lcMonopol;
    MeshSize{PointsOf{Surface{surfaceAirInf[]};}} = lcair;
EndIf



// cut for the H formulation

Cohomology(1) {{NONTERM},{}}; // this will generate the cut needed by the formulation in H, with no NONTERM + 1
