Include "../monopole_data.pro";  

fac = s;
flag = 1; // if 0, extruded mesh becomes free in the monopole
_use_structured_pml = 0 ;

lcdAntennaZero = a/4;
lcdAntennaA = lcdAntennaZero;

If (_use_fineMesh == 1)
	lambdaMin = c0/fmax;
	lcd = fac*lambdaMin/nopPerLambdaAir;
	lcair = lcd;

	nopL = flag*(Ceil[L/lcdAntennaZero]);  // to make the antenna very fine along L
Else
	lcdAntennaZero = 4*lcdAntennaZero;
	lambdaMin = c0/fmax;
	lcair = fac*lambdaMin/nopPerLambdaAir;

	delta = Sqrt(2.0*nuMonopole/(2*Pi*Freq*sigmaMonopole));
	lcdAntennaA = fac*delta/nbDelta;

	lcair1 = lambdaMin/10;
	nopL = fac*6*flag*(Ceil[L/lcair1]);
EndIf
nopL = (nopL < 10) ? 10 : nopL;

Printf("nopL %g",nopL);
Printf("lcdAntennaA %g lcdAntennaZero %g ratio %g", lcdAntennaA, lcdAntennaZero, lcdAntennaZero/lcdAntennaA);


//=================================================
// Monopole
//=================================================

d = 0;  // no gap
yd = (d >= 0)? d:0.;
p0 = newp ; Point(p0) = {0, yd, 0, lcdAntennaZero};
p1 = newp ; Point(p1) = {a, yd, 0, lcdAntennaA};
LineMonopoleBottom = newl ; Line(LineMonopoleBottom) = {p0,p1};

surf[] = Extrude {0, L, 0} {
  Line{LineMonopoleBottom}; Layers{nopL} ;
};
surfMonopole[0] = surf[1] ; LineMonopoleTop = surf[0] ;

p_[] = Boundary{Line{LineMonopoleTop};};

interfaceMonopoleAir[] = Boundary{ Surface{surfMonopole[]};} ;

axisMonopole[] = interfaceMonopoleAir[{3}] ;
feedMonople[] = interfaceMonopoleAir[{0}] ;
interfaceMonopoleAir[] -= axisMonopole[];
interfaceMonopoleAir[] -= feedMonople[];

//=================================================
// Air and PML
//=================================================

pZero = newp; Point(pZero) = { 0, 0, 0, lcdAntennaZero};

pp[]+=p1;
pp[]+=newp; Point(newp) = {  b, 0, 0, lcdAntennaZero};

// reduce air around
cc = 1; // 4
pp[]+=newp; Point(newp) = { xb/cc, 0, 0, lcair};
pp[]+=newp; Point(newp) = { xb/cc, L, 0, lcair};
pp[]+=newp; Point(newp) = { 0,  xb/cc+L, 0, lcair};
pp[]+=p_[0];

For k In {0:2}
	lbox[]+=newl; Line(newl) = {pp[k],pp[k+1]};
EndFor
lbox[]+=newl; Circle(newl) = {pp[3],p_[0],pp[4]};
lbox[]+=newl; Line(newl) = {pp[4],pp[5]};
interfaceAirPML[] = lbox[{2,3}];  // it will be the inf boundary if no PML is used	
	
// pml begin
ppml[]+=pp[2];
ppml[]+=newp; Point(newp) = {xb/cc+PmlDelta, 0, 0, lcair};
ppml[]+=newp; Point(newp) = {xb/cc+PmlDelta, L, 0, lcair};
ppml[]+=pp[3];

For k In {0:2}
	lpml1[]+=newl; Line(newl) = {ppml[k],ppml[k+1]};
EndFor
ppml2[]+=newp; Point(newp) = {0, xb/cc+L+PmlDelta, 0, lcair};

lpml2[]+=newl; Circle(newl) = {ppml[2],p_[0],ppml2[0]};
lpml2[]+=newl; Line(newl) = {ppml2[0],pp[4]};
//pml end

surfAirLL = newll ; Line Loop(surfAirLL) = {lbox[],-interfaceMonopoleAir[]};
surfAir = news ; Plane Surface(surfAir) = {surfAirLL};

// pml begin
surfPmlLL1 = newll ; Line Loop(surfPmlLL1) = {lpml1[],-interfaceAirPML[0] };
surfPml1 = news; Plane Surface(surfPml1) = {surfPmlLL1};

surfPmlLL2 = newll ; Line Loop(surfPmlLL2) = {-lpml1[2],lpml2[],-interfaceAirPML[1] };
surfPml2 = news; Plane Surface(surfPml2) = {surfPmlLL2};

If(_use_structured_pml)
	bndPml() = Boundary{Surface{surfPml};};

	nn90 = Ceil[Pi*(rb+PmlDelta)/2/lcair];
	nnDelta = Ceil[PmlDelta/lcair];

	Transfinite Line {bndPml({0,2})} =  nnDelta;
	Transfinite Line {bndPml({1,3})} =  nn90;
	Transfinite Surface{surfPml}; 
EndIf
// pml_end


// For aestetics
Mesh.Light = 0;  // so that to see mesh colors in the coax part (normal to surface has to be outward to see the colors with light :-))

Recursive Color SkyBlue { Surface{surfAir};}
Recursive Color Black  { Surface{surfMonopole};}
Recursive Color Yellow { Surface{surfPml1};}
Recursive Color Yellow { Surface{surfPml2};}

//=================================================
// Physical regions
//=================================================

// surfaces
Physical Surface(MONOPOLE) = {surfMonopole[]};          // MONOPOLE
Physical Surface(AIR) = surfAir;                        // AIR
Physical Surface(PML) = {surfPml1,surfPml2};            // PML

// boundaries
Physical Line(TERMINAL) = {feedMonople[]}; // Feeding
Physical Line(CUT) = {lbox[{0}]};
Physical Line(AXIS) = {lbox[{4}],axisMonopole[],lpml2[{1}]}; 

// GROUND and SURFAIRINF
If (Flag_AnalysisTypeExtBnd == 1) // SURFAIRINF is set to terminal ground (PEC type)
	Physical Line(GROUND) = {lbox[{1}],lpml1[{0}],lpml1[{1}],lpml2[{0}]};
Else
	Physical Line(SURFAIRINF) = {lpml1[{1}],lpml2[{0}]};
	Physical Line(GROUND) = {lbox[{1}],lpml1[{0}]};
EndIf

