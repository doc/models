//=================================================
// Physical regions
//=================================================

// surfaces
Physical Surface(MONOPOLE) = {surfMonopole[]};          
Physical Surface(AIR) = surfAir;                        

// boundaries
Physical Line(TERMINAL) = {feedMonople[]}; // Feeding

Physical Line(CUT) = {lbox[{0}]};
Physical Line(AXIS) = {lbox[{3}],axisMonopole[]};

If (Flag_AnalysisTypeExtBnd == 1) 
	Physical Line(GROUND) = {lbox[{1}],lbox[{2}]};
Else
	Physical Line(SURFAIRINF) = interfaceAirPML;
    Physical Line(GROUND) = {lbox[{1}]};
EndIf


// For aestetics
Mesh.Light = 0;  // so that to see mesh colors in the coax part

Recursive Color SkyBlue { Surface{surfAir};}
Recursive Color Black  { Surface{surfMonopole};}