Include "../monopole_data.pro";  

fac = s;
flag = 1; // if 0, extruded mesh becomes free in the monopole
_use_structured_pml = 1 ;

If (_use_fineMesh == 1)
	lcdAntennaZero = a/4;
    lcdAntennaA = lcdAntennaZero;
	lambdaMin = c0/fmax; // the same mesh for all the frequencies
	lcd = fac*lambdaMin/nopPerLambdaAir;
	lcair = lcd;

	nopL = flag*(Ceil[L/lcdAntennaZero]); 
Else
	lcdAntennaZero = a;
	lambdaMin = lambda;  // for a frequency dependent mesh, you should uncomment this line
	lcair = fac*lambdaMin/nopPerLambdaAir;
	
	no90 = Ceil[Pi*(rb)/2/lcair];
	lcair = (no90 < 10)? Pi*(rb)/2/10 : no90;

	delta = Sqrt(2.0*nuMonopole/(2*Pi*Freq*sigmaMonopole));
	lcdAntennaA = fac*delta/nbDelta;

	lcair1 = lambdaMin/(nopPerLambdaAir*2);  
	nopL = fac*3*flag*(Ceil[L/lcair1]);
EndIf
nopL = (nopL < 10) ? 10 : nopL;

Printf("nopL %g",nopL);
Printf("lcdAntennaA %g lcdAntennaZero %g ratio %g", lcdAntennaA, lcdAntennaZero, lcdAntennaZero/lcdAntennaA);


//=================================================
// Monopole
//=================================================

d = 0;  // no gap
yd = (d >= 0)? d:0.;
p0 = newp ; Point(p0) = {0, yd, 0, lcdAntennaZero};
p1 = newp ; Point(p1) = {a, yd, 0, lcdAntennaA};
LineMonopoleBottom = newl ; Line(LineMonopoleBottom) = {p0,p1};

surf[] = Extrude {0, L, 0} {
  Line{LineMonopoleBottom}; Layers{nopL} ;
};
surfMonopole[0] = surf[1] ; LineMonopoleTop = surf[0] ;

p_[] = Boundary{Line{LineMonopoleTop};};

interfaceMonopoleAir[] = Boundary{ Surface{surfMonopole[]};} ;

axisMonopole[] = interfaceMonopoleAir[{3}] ;
feedMonople[] = interfaceMonopoleAir[{0}] ;
interfaceMonopoleAir[] -= axisMonopole[];
interfaceMonopoleAir[] -= feedMonople[];

//=================================================
// Air and PML
//=================================================

pZero = newp; Point(pZero) = { 0, 0, 0, lcdAntennaZero};

pp[]+=p1;
pp[]+=newp; Point(newp) = {  b, 0, 0, lcdAntennaZero};

pp[]+=newp; Point(newp) = { rb, 0, 0, lcair};
pp[]+=newp; Point(newp) = {  0, rb, 0, lcair};
pp[]+=p_[0];

For k In {0:1}
	lbox[]+=newl; Line(newl) = {pp[k],pp[k+1]};
EndFor
lbox[]+=newl; Circle(newl) = {pp[2],pZero,pp[3]};
lbox[]+=newl; Line(newl) = {pp[3],pp[4]};
interfaceAirPML[] = lbox[{2}];  // it will be the inf boundary if no PML is used

// pml begin
ppml[]+=pp[2];
ppml[]+=newp; Point(newp) = {rb+PmlDelta, 0, 0, lcair};
ppml[]+=newp; Point(newp) = {0, rb+PmlDelta, 0, lcair};
ppml[]+=pp[3];

lpml[]+=newl; Line(newl) = {ppml[0],ppml[1]};
lpml[]+=newl; Circle(newl) = {ppml[1],pZero,ppml[2]};
lpml[]+=newl; Line(newl) = {ppml[2],ppml[3]};
//pml end
	
surfAirLL = newll ; Line Loop(surfAirLL) = {lbox[],-interfaceMonopoleAir[]};
surfAir = news ; Plane Surface(surfAir) = {surfAirLL};

// pml begin
surfPmlLL = newll ; Line Loop(surfPmlLL) = {lpml[],-interfaceAirPML[] };
surfPml = news; Plane Surface(surfPml) = {surfPmlLL};

If(_use_structured_pml)
	bndPml() = Boundary{Surface{surfPml};};

	nn90 = Ceil[Pi*(rb+PmlDelta)/2/lcair];
	nnDelta = Ceil[PmlDelta/lcair];
	Printf("*** *** *** ==================================================================> nnDelta = %e",nnDelta);
	If (nnDelta < 4)
			nnDelta = 4;
	EndIf
	Transfinite Line {bndPml({0,2})} =  nnDelta;
	Transfinite Line {bndPml({1,3})} =  nn90;
	Transfinite Surface{surfPml}; 
	// Recombine Surface {surfPml};  // to obtain quadrangles - it does not work for formulation in E order 2 - because BF_Edge_4E is not ready for this
EndIf
// pml_end

// For aestetics
Mesh.Light = 0;  // so that to see mesh colors in the coax part

Recursive Color SkyBlue { Surface{surfAir};}
Recursive Color Black  { Surface{surfMonopole};}
Recursive Color Yellow { Surface{surfPml};}

//=================================================
// Physical regions
//=================================================

Include "PhysicalRegions.geo";
