Include "../monopole_data.pro"; 

fac = s;
nopPerLambda = NOP_perLAMBDA_AIR;
lc  = lambda/nopPerLambda ; // rule of thumb (min 10 divisions)
lcd = (lc < a/2) ? lc :a/2 ;

flag = 1; // if 0, extruded mesh becomes free
lcdAntennaZero = a/4;  
lcdAntennaA = lcdAntennaZero;

nopL = flag*Ceil[L/lcd/2/fac] ;

If (_use_fineMesh == 1)
	lambdaMin = c0/fmax;
	lcd = fac*lambdaMin/nopPerLambdaAir;
	lcair = lcd;

	nopL = flag*(Ceil[L/lcdAntennaZero]);  // to make the antenna very fine along L
Else
	
		lambdaMin = c0/fmax;
		lcair = fac*lambdaMin/nopPerLambdaAir;
		lcdAntennaZero = lcair;
		xxx = 1;
		lcdAntennaZero = lcdAntennaZero/xxx;
		lcdAntennaA = lcair;
	
		lcair1 = lcair;
		lcair1 = lcair1/xxx;
		nopL = fac*flag*(Ceil[L/lcair1]);  
		If (d < 0)
			nopdCoax = flag*Ceil[Abs[d]*nopL/L] ;
		EndIf
EndIf

Printf("====================================> nopL %g",nopL);
Printf("lcdAntennaA %g lcdAntennaZero %g ratio %g", lcdAntennaA, lcdAntennaZero, lcdAntennaZero/lcdAntennaA);

lcdAntennaZeroB = lcdAntennaZero;

//=================================================
// Monopole - gap > 0 
//=================================================

// d is assumed > 0 
p0 = newp ; Point(p0) = {0, d, 0, lcdAntennaZero};
p1 = newp ; Point(p1) = {a, d, 0, lcdAntennaZeroB};

LineMonopoleBottom = newl ; Line(LineMonopoleBottom) = {p0,p1};

surf[] = Extrude {0, L, 0} {
	Line{LineMonopoleBottom}; Layers{nopL} ;
};
surfMonopole[0] = surf[1] ; LineMonopoleTop = surf[0] ;

p_[] = Boundary{Line{LineMonopoleTop};};

interfaceMonopoleAir[] = Boundary{ Surface{surfMonopole[]};} ;

axisMonopole[] = interfaceMonopoleAir[{3}] ;
feedMonople[] = interfaceMonopoleAir[{0}] ;
interfaceMonopoleAir[] -= axisMonopole[];
interfaceMonopoleAir[] -= feedMonople[];

//=================================================
// Air, circular truncation boundary
//=================================================

pZero = newp; Point(pZero) = { 0, 0, 0, lcd};

pp[]+=p1;
pp[]+=newp; Point(newp) = {  a, 0, 0, lcdAntennaA};
pp[]+=newp; Point(newp) = { rb, 0, 0, lcair};
pp[]+=newp; Point(newp) = {  0, rb, 0, lcair};
pp[]+=p_[0];

For k In {0:1}
	lbox[]+=newl; Line(newl) = {pp[k],pp[k+1]};
EndFor
lbox[]+=newl; Circle(newl) = {pp[2],pZero,pp[3]};
lbox[]+=newl; Line(newl) = {pp[3],pp[4]};
interfaceAirPML[] = lbox[{2}];  // it will be the inf boundary if no PML is used

surfAirLL = newll ; Line Loop(surfAirLL) = {lbox[],-interfaceMonopoleAir[]};
surfAir = news ; Plane Surface(surfAir) = {surfAirLL};

// For aestetics
Mesh.Light = 0;  // so that to see mesh colors in the coax part

Recursive Color SkyBlue { Surface{surfAir};}
Recursive Color Black  { Surface{surfMonopole};}

//=================================================
// Physical regions
//=================================================

// surfaces
Physical Surface(MONOPOLE) = {surfMonopole[]};    
Physical Surface(AIR) = surfAir;                  

// boundaries
Physical Line(TERMINAL) = {feedMonople[]}; // Feeding
Physical Line(CUT) = {lbox[{0}]};

Physical Line(AXIS) = {lbox[{3}],axisMonopole[]}; 

If (Flag_AnalysisTypeExtBnd==1) 
	Physical Line(GROUND) = {lbox[{1}],lbox[{2}]};
Else
	Physical Line(SURFAIRINF) = interfaceAirPML;
	Physical Line(GROUND) = {lbox[{1}]};
EndIf
