///////////////////////////////
// Author : Guillaume Demesy //
///////////////////////////////

Include "QNM_expansion_dispersive_media_data.geo";
lc_cell	  = lambda_m/paramaille;
lc_sq     = lc_cell/2.;
lc_src    = lc_cell;

Point(1)  = {-a_lat    , -a_lat/2, 0. , lc_cell};
Point(2)  = {-a_lat    ,  a_lat/2, 0. , lc_cell};
Point(3)  = {     0    , -a_lat/2, 0. , lc_sq};
Point(4)  = {     0    ,  a_lat/2, 0. , lc_sq};
Point(5)  = { a_lat    , -a_lat/2, 0. , lc_cell};
Point(6)  = { a_lat    ,  a_lat/2, 0. , lc_cell};
Point(12)  = {-a_lat+0.7 *2*a_lat    , -a_lat/2, 0. , lc_sq};
Point(13)  = {-a_lat+0.48*2*a_lat    ,  a_lat/2, 0. , lc_sq};

Point(7)  = { cx_src  ,  cy_src , 0.       , lc_src};
Point(8)  = { cx_src+r_src  ,  cy_src , 0. , lc_src};
Point(9)  = { cx_src  ,  cy_src+r_src , 0. , lc_src};
Point(10) = { cx_src-r_src  ,  cy_src , 0. , lc_src};
Point(11) = { cx_src  ,  cy_src-r_src , 0. , lc_src};

Circle(8)  = {10, 7, 9};
Circle(9)  = {9, 7, 8};
Circle(10) = {8, 7, 11};
Circle(11) = {11, 7, 10};

Line(12) = {7, 8};
Line(13) = {7, 9};
Line(14) = {7, 10};
Line(15) = {7, 11};

Line Loop(1) = {8, -13, 14};
Plane Surface(1) = {-1};
Line Loop(2) = {13, 9, -12};
Plane Surface(2) = {-2};
Line Loop(3) = {10, -15, 12};
Plane Surface(3) = {-3};
Line Loop(4) = {14, -11, -15};
Plane Surface(4) = {4};

Line(16) = {1, 3};
Line(17) = {3, 12};
Line(18) = {12, 5};
Line(19) = {5, 6};
Line(20) = {6, 4};
Line(21) = {4, 13};
Line(22) = {13, 2};
Line(23) = {2, 1};
Line(24) = {3, 13};
Line(25) = {12, 4};
Curve Loop(5) = {23, 16, 24, 22};
Plane Surface(5) = {5};
Curve Loop(6) = {24, -21, -25, -17};
Plane Surface(6) = {-6};
Curve Loop(7) = {25, -20, -19, -18};
Curve Loop(8) = {8, 9, 10, 11};
Plane Surface(7) = {-7, 8};

Physical Surface(1) = {6};       // Dispersive medium
Physical Surface(2) = {5,7};     // Background
Physical Surface(3) = {1,2,3,4}; // disk green
Physical Line(10)   = {16,17,18,19,20,21,22,23}; // Outer box
Physical Line(20)   = {24,25};   // bound lag
Physical Line(30)   = {12};      // Edge Source Green X
Physical Line(31)   = {13};      // Edge Source Green Y
Physical Point(100) = {7};

// dummy lines to see expansion and direct solutions side by side
pp=newp;
Point(pp+1)  = {-a_lat+ xshift*2*a_lat    , -a_lat/2 , 0. , lc_cell};
Point(pp+2)  = {-a_lat+ xshift*2*a_lat    ,  a_lat/2 , 0. , lc_cell};
Point(pp+3)  = {     0+ xshift*2*a_lat    , -a_lat/2 , 0. , lc_sq};
Point(pp+4)  = {     0+ xshift*2*a_lat    ,  a_lat/2 , 0. , lc_sq};
Point(pp+5)  = { a_lat+ xshift*2*a_lat    , -a_lat/2 , 0. , lc_cell};
Point(pp+6)  = { a_lat+ xshift*2*a_lat    ,  a_lat/2 , 0. , lc_cell};
Point(pp+12)  = {-a_lat+0.7 *2*a_lat+ xshift*2*a_lat    , -a_lat/2, 0. , lc_sq};
Point(pp+13)  = {-a_lat+0.48*2*a_lat+ xshift*2*a_lat    ,  a_lat/2, 0. , lc_sq};

c=newl;
Line(c+16) = {pp+1, pp+3};
Line(c+17) = {pp+3, pp+12};
Line(c+18) = {pp+12, pp+5};
Line(c+19) = {pp+5, pp+6};
Line(c+20) = {pp+6, pp+4};
Line(c+21) = {pp+4, pp+13};
Line(c+22) = {pp+13, pp+2};
Line(c+23) = {pp+2, pp+1};
Line(c+24) = {pp+3, pp+13};
Line(c+25) = {pp+12, pp+4};

// dummy point to enforce window center 
pp=newp;
Point(pp+1)  = {-a_lat+ xshift*2*a_lat , 1.5*a_lat , 0. , lc_cell};

Geometry.Points = 0;
