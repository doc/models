///////////////////////////////
// Author : Guillaume Demesy //
///////////////////////////////

pp = "Model Options/";
DefineConstant[
  flag_vector_case = {1   , Name StrCat[pp, "0polarization case"], Choices {0="(0,0,Ez)",1="(Ex,Ey,0)"} },
  flag_o2          = {1   , Name StrCat[pp, "1FE order"], Choices {0="order 1",1="order 2"} },
  neig             = {100 , Name StrCat[pp, "0number of eigenvalues"]}
];
  
nm            = 0.1;
N_true_poles  = 1;
paramaille    = 5;

a_lat         = 482.5*nm;
lambda_m      = a_lat;

eps0      = 8.854187817e-3*nm;
mu0       = 400.*Pi*nm;
cel       = 1.0/Sqrt[eps0 * mu0];
norm      = a_lat/(2.*Pi*cel);

om_p_josab    = .4*norm;
om_0_josab    = 0.3*norm;
gama_josab    = 0.05*norm;
om_lorentz_1_im  = -gama_josab/2;
om_lorentz_1_re  = Sqrt[om_0_josab^2-om_lorentz_1_im^2];
a_lorentz_1      = -om_p_josab^2/(2*om_lorentz_1_re);
om_lorentz_1_mod = Sqrt[om_lorentz_1_re^2+om_lorentz_1_im^2];

eig_target_re =  om_lorentz_1_im/4;
eig_target_im =  1/norm;
eig_max_im    =  8/norm;
eig_min_im    =  0.01/norm;
eig_min_re    =  0;
eig_max_re    =  0.8*Fabs[om_lorentz_1_im];

cx_src = 0.75*2*a_lat - a_lat;
cy_src = 0.75*a_lat - a_lat/2;
r_src  = a_lat/100;

nb_Omegas     = 50;
RealOmega_min = 0.1;
RealOmega_max = 1.1;
tabrealomegasexpansion = LinSpace[RealOmega_min,RealOmega_max,nb_Omegas];
Freq = 0;

xshift = 1.1;
