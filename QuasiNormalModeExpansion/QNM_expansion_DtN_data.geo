///////////////////////////////
// Author : Guillaume Demesy //
///////////////////////////////

pp = "Model Options/";
DefineConstant[
    flag_DtN         = {2   , Name StrCat[pp, "0DtN"], Choices {0="PML",1="ABC",2="BT2"} , ServerAction "Reset"},
    flag_vector_case = {0   , Name StrCat[pp, "1polarization case"], Choices {0="(0,0,Ez)",1="(Ex,Ey,0)"} },
    flag_o2          = {1   , Name StrCat[pp, "2FE order"], Choices {0="order 1",1="order 2"} },
    neig             = {120  , Name StrCat[pp, "3number of eigenvalues"] }
];
  
nm         = 0.1;
paramaille = 10;

neig_PML = neig;
neig_BT  = neig;

a_lat      = 482.5*nm;
lambda_m   = a_lat;

lambda0   = 500*nm;
dx_scat   = 200*nm;
dy_scat   = 400*nm;
epsr_scat_re = 9;
epsr_scat_im = 0.5;
epsr_bg      = 1;

r_domain  = 0.5*Sqrt[dx_scat^2+dy_scat^2]+100*nm;
r_pml     = r_domain+lambda0;

eps0      = 8.854187817e-3*nm;
mu0       = 400.*Pi*nm;
cel       = 1.0/Sqrt[eps0 * mu0];

norm      = dy_scat/(Pi*cel);

omega_target_re  = 0.7/norm;
omega_target_im  = 1/norm;

target_re_PML = omega_target_re^2 - omega_target_im^2;
target_im_PML = 2*omega_target_re*omega_target_im;
target_re_BT  = omega_target_im;
target_im_BT  = omega_target_re;

eig_min_re = -5/norm;
eig_max_re =  5/norm;
eig_min_im = -8/norm;
eig_max_im =  8/norm;

If (flag_DtN==0)
  flag_PML = 1;
  flag_ABC_BAYLISS = 0;
  slepc_options_rg = Sprintf[" -rg_interval_endpoints %.8lf,%.8lf,%.8lf,%.8lf ",eig_min_re,eig_max_re,eig_min_im,eig_max_im];
  str_res = "res_modal_PML";
  str_slepc_solver = " ";
  domain_tot = 2*r_pml;
Else
  flag_PML = 0;
  flag_ABC_BAYLISS = 1;
  slepc_options_rg = " ";
  If (flag_DtN==1)
    str_res = "res_modal_AR_mur";
  Else
    str_res = "res_modal_AR_BT2";
  EndIf
  str_slepc_solver = " -nep_type nleigs -nep_rational -nep_target_magnitude -nep_view_pre -nep_monitor_all :temp_eigenvalues.txt ";
  domain_tot = 2*r_domain;
EndIf

flag_PRINTVECTOR = 1;
flag_PRINTFIELDS = 0;

nb_Omegas     = 60;
RealOmega_min = 0.5 / norm;
RealOmega_max =   3 / norm;
theta0        =  30 * Pi/180;

tabrealomegasexpansion = LinSpace[RealOmega_min,RealOmega_max,nb_Omegas];
// Freq = 0;

// FIXME
flag_manualexp = 1;

yshift = 1.1;
