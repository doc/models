///////////////////////////////
// Author : Guillaume Demesy //
///////////////////////////////

Include "QNM_expansion_DtN_data.geo";
stored_exp_field_AR_PML     = 10000;
stored_exp_field_AR_mur     = 20000;
stored_exp_field_AR_BT2     = 30000;
stored_exp_field_manual_PML = 40000;
CreateDir["out"];

Group {
  // Domains    
  Scat  = Region[1];
  Bg    = Region[2];
  Src   = Region[4];
  If (flag_PML==0)
    PML   = Region[{}];
    Surf  = Region[10];
  Else
    PML   = Region[{3}];
    Surf  = Region[{10}];
  EndIf
  Omega  = Region[{Bg,Scat,PML,Src}];
  PrintPoint  = Region[100];
}

Function{
  I[]              = Complex[0.0,1.0];
  
  k0[]     = $realomega/cel;
  omega0[] = k0[]*cel;

  spml_re          = 1;
  spml_im          = 1;

  R_pml_in         = r_domain;
  sr[]             = Complex[spml_re,spml_im];
  rho[]            = Sqrt[X[]*X[]+Y[]*Y[]];// Norm[ XYZ[  ] ];
  phi[]            = Atan2[ Y[] , X[] ];
  rr[]             = R_pml_in + (rho[] - R_pml_in) * sr[];
  drr[]            = sr[];
  pmlcirctens[]    = Rotate[TensorDiag[rr[]/(drr[]*rho[]), (drr[]*rho[])/rr[], (drr[]*rr[])/rho[]],0,0,-phi[]];
  
  epsr[Scat]       = Complex[epsr_scat_re,epsr_scat_im];
  epsr[Bg]         = epsr_bg;
  epsr[Src]        = epsr_bg;
  epsr[PML]        = pmlcirctens[];

  epsr_annex[Scat] = epsr_bg;
  epsr_annex[Bg]   = epsr_bg;
  epsr_annex[Src]  = epsr_bg;
  epsr_annex[PML]  = pmlcirctens[];

  mur[Scat]        = 1;
  mur[Bg]          = 1;
  mur[Src]         = 1;
  mur[PML]         = pmlcirctens[];

  alpha[]          = k0[]*Cos[theta0];
  beta[]           = k0[]*Sin[theta0];
  siwt             = 1;
  ui[]             = Vector[0, 0, Exp[I[]*siwt*(alpha[]*X[]+beta[]*Y[])] ];
  Hi[]             = I[]/(omega0[]*mu0) * Vector[alpha[],beta[],0]  /\ ui[];
  Pinc[]           = 0.5*1^2*Sqrt[eps0*epsr_bg/mu0] * 2*r_domain;
  
  THETA[] = Atan2[Y[],X[]];
  TG[]    = Vector[-Sin[THETA[]], Cos[THETA[]],0];

  source_PW[]      = k0[]^2 * (epsr_annex[]-epsr[]) * ui[];

  For k In {0:#tabrealomegasexpansion()-1}
    k0~{k}[]        = tabrealomegasexpansion(k)/cel;
    alpha~{k}[]     = k0~{k}[]*Cos[theta0];
    beta~{k}[]      = k0~{k}[]*Sin[theta0];
    ui~{k}[]        = Vector[0, 0, Exp[I[]*siwt*(alpha~{k}[]*X[]+beta~{k}[]*Y[])] ];
    source_PW~{k}[] = k0~{k}[]^2 * (epsr_annex[]-epsr[]) * ui~{k}[];
    us_exp_PML_modal_manual~{k}[]    = ComplexScalarField[XYZ[]]{stored_exp_field_manual_PML+k};
    us_exp_PML_modal~{k}[]           = ComplexScalarField[XYZ[]]{stored_exp_field_AR_PML+k};
    us_exp_mur_modal~{k}[]           = ComplexScalarField[XYZ[]]{stored_exp_field_AR_mur+k};
    us_exp_BT2_modal~{k}[]           = ComplexScalarField[XYZ[]]{stored_exp_field_AR_BT2+k};
  EndFor


  source_G[Src]    = Vector[0, 0, 1];
  source_G[Scat]   = 0;
  source_G[Bg]     = 0;
  source_G[PML]    = 0;

  Rout             = r_domain;

  alphaBT[]        = 1/(2*Rout) - I[]/(8*k0[]*Rout^2*(1+I[]/(k0[]*Rout)));
  betaBT[]         = - 1/(2*I[]*k0[]*(1+I[]/(k0[]*Rout)));
}

Constraint {
}

Jacobian {
  { Name JVol ;
    Case { 
      { Region All ; Jacobian Vol ; }
    }
  }
  { Name JSur ;
    Case {
      { Region All ; Jacobian Sur ; }
    }
  }
}

Integration {
  { Name Int_1 ;
    Case { 
      { Type Gauss ;
      	Case { 
      	  { GeoElement Point    ; NumberOfPoints  1 ; }
      	  { GeoElement Line     ; NumberOfPoints  4 ; }
      	  { GeoElement Triangle ; NumberOfPoints  12 ; }
      	}
      }
    }
  }
}

FunctionSpace {
  { Name Hgrad_P; Type Form1P;
    BasisFunction {
      { Name sn;  NameOfCoef un;  Function BF_PerpendicularEdge_1N; Support Region[{Omega,Surf}]; Entity NodesOf[Omega]; }
      { Name sn2; NameOfCoef un2; Function BF_PerpendicularEdge_2E; Support Region[{Omega,Surf}]; Entity EdgesOf[Omega]; }
    }
    Constraint {
    }
  }
}

Formulation {
  {Name form_direct_PML; Type FemEquation;
    Quantity{{ Name u; Type Local; NameOfSpace Hgrad_P;}}
    Equation{
      Galerkin{[  k0[]^2*epsr[]*Dof{u} ,      {u}]; In Omega; Jacobian JVol; Integration Int_1;}
      Galerkin{[ -1/mur[]*Dof{Curl u}  , {Curl u}]; In Omega; Jacobian JVol; Integration Int_1;}
      Galerkin{[ -source_PW[]          ,      {u}]; In Omega; Jacobian JVol; Integration Int_1;}
    }
  }

  {Name form_direct_mur; Type FemEquation;
    Quantity{{ Name u; Type Local; NameOfSpace Hgrad_P;}}
    Equation{
      Galerkin{[  k0[]^2*epsr[]*Dof{u} ,      {u}]; In Omega; Jacobian JVol; Integration Int_1;}
      Galerkin{[ -1/mur[]*Dof{Curl u}  , {Curl u}]; In Omega; Jacobian JVol; Integration Int_1;}
      Galerkin{[ -source_PW[]          ,      {u}]; In Omega; Jacobian JVol; Integration Int_1;}
      Galerkin{[       I[]*k0[]*Dof{u} ,      {u}]; In  Surf; Jacobian JSur; Integration Int_1;}
    }
  }

  {Name form_direct_BT2; Type FemEquation;
    Quantity{{ Name u; Type Local; NameOfSpace Hgrad_P;}}
    Equation{
      Galerkin{[  k0[]^2*epsr[]*Dof{u}     ,      {u}]; In Omega; Jacobian JVol; Integration Int_1;}
      Galerkin{[ -1/mur[]*Dof{Curl u}      , {Curl u}]; In Omega; Jacobian JVol; Integration Int_1;}
      Galerkin{[ -source_PW[]              ,      {u}]; In Omega; Jacobian JVol; Integration Int_1;}
      Galerkin{[       I[]*k0[]*Dof{u}     ,      {u}]; In  Surf; Jacobian JSur; Integration Int_1;}
      Galerkin{[  -alphaBT[] * Dof{u}      ,      {u}]; In  Surf; Jacobian JSur; Integration Int_1;}
      Galerkin{[  -betaBT[]  * Dof{Curl u} , {Curl u}]; In  Surf; Jacobian JSur; Integration Int_1;}
    }
  }

  {Name form_modal_linear_PML; Type FemEquation;
    Quantity{{ Name u; Type Local; NameOfSpace Hgrad_P;}}
    Equation{
      Galerkin{Eig[-1/cel^2*epsr[]*Dof{u} ,      {u}]; Order 2 ; In Omega; Jacobian JVol; Integration Int_1;}
      Galerkin{   [  -1/mur[]*Dof{Curl u} , {Curl u}];           In Omega; Jacobian JVol; Integration Int_1;}
    }
  }

  {Name form_modal_PML; Type FemEquation;
    Quantity{{ Name u; Type Local; NameOfSpace Hgrad_P;}}
    Equation{
      Galerkin{Eig[-1/cel^2*epsr[]*Dof{u} ,      {u}]; Rational 1 ; In Omega; Jacobian JVol; Integration Int_1;}
      Galerkin{Eig[  -1/mur[]*Dof{Curl u} , {Curl u}]; Rational 2 ; In Omega; Jacobian JVol; Integration Int_1;}
    }
  }
  {Name form_modal_mur_poly; Type FemEquation;
    Quantity{{ Name u; Type Local; NameOfSpace Hgrad_P;}}
    Equation{
      Galerkin{ Eig[-1/cel^2*epsr[]*Dof{u} ,      {u}]; Order 2 ; In Omega; Jacobian JVol; Integration Int_1;}
      Galerkin{    [-1/mur[]*Dof{Curl u}   , {Curl u}];           In Omega; Jacobian JVol; Integration Int_1;}
      Galerkin{ Eig[ 1/cel*Dof{u}          ,      {u}]; Order 1 ; In Surf ; Jacobian JSur; Integration Int_1;}
    }
  }
  {Name form_modal_mur; Type FemEquation;
    Quantity{{ Name u; Type Local; NameOfSpace Hgrad_P;}}
    Equation{
      Galerkin{ Eig[-1/cel^2*epsr[]*Dof{u} ,      {u}]; Rational 1 ; In Omega; Jacobian JVol; Integration Int_1;}
      Galerkin{ Eig[-1/mur[]*Dof{Curl u}   , {Curl u}]; Rational 2 ; In Omega; Jacobian JVol; Integration Int_1;}
      Galerkin{ Eig[ 1/cel*Dof{u}          ,      {u}]; Rational 3 ; In Surf ; Jacobian JSur; Integration Int_1;}
    }
  }
  {Name form_modal_BT2; Type FemEquation;
    Quantity{{ Name u; Type Local; NameOfSpace Hgrad_P;}}
    Equation{
      // {-1/cel^2,0,0}/{1} --------- (i\omega)^2/c^2
      Galerkin{Eig[   epsr[]*     Dof{u} ,      {u}]; Rational 1; In Omega; Jacobian JVol; Integration Int_1;}
      // {1}/{1} 1
      Galerkin{Eig[ -1/mur[]*Dof{Curl u} , {Curl u}]; Rational 2; In Omega; Jacobian JVol; Integration Int_1;}
      // {1/cel,0}/{1} ---------  i\omega/c
      Galerkin{Eig[               Dof{u} ,      {u}]; Rational 3; In Surf ; Jacobian JSur; Integration Int_1;}
      // {-4*Rout/cel,3}/{-8*Rout^2/cel,8*Rout} --------- \frac{-4Ri\omega/c+3}{-8R^2i\omega/c+8R}
      Galerkin{Eig[               Dof{u} ,      {u}]; Rational 4; In Surf ; Jacobian JSur; Integration Int_1;}
      // {-Rout}/{2*Rout/cel,-2} --------- \frac{-R}{2Ri\omega/c-2}
      Galerkin{Eig[          Dof{Curl u} , {Curl u}]; Rational 5; In Surf ; Jacobian JSur; Integration Int_1;}
    }
  }
  {Name form_RHS; Type FemEquation;
    Quantity{{ Name u; Type Local; NameOfSpace Hgrad_P;}}
    Equation{ 
      // Galerkin { [   1. *    Dof{u}    , { u} ]; In Omega    ; Jacobian JSur; Integration Int_1;}
      Galerkin{[-source_PW[] , {u}]; In Omega; Jacobian JVol; Integration Int_1;} 
      
    }
  }
}

Resolution {
  { Name res_modal_PML;
    System {
      { Name M1; NameOfFormulation form_modal_linear_PML; Type ComplexValue; }
      { Name M2; NameOfFormulation form_direct_PML      ; Type ComplexValue; }
    }
    Operation {
      GenerateSeparate[M1] ; 
      EigenSolve[M1,neig_PML,target_re_PML,target_im_PML];
      PostOperation[postop_modal_PML];
      For k In {0:#tabrealomegasexpansion()-1}
        Evaluate[$realomega=tabrealomegasexpansion(k)];
        Generate[M2];
        Solve[M2];
        PostOperation[postop_direct_PML];
      EndFor
      For k In {0:#tabrealomegasexpansion()-1}
        PostOperation[postop_gmsh~{k}];
        Sleep[0.3];
      EndFor
    }
  }

  { Name res_modal_AR_BT2;
    System{
        { Name V ; NameOfFormulation form_RHS   ; Type ComplexValue; }
        { Name M1; NameOfFormulation form_modal_BT2 ; Type ComplexValue; }
        { Name M2; NameOfFormulation form_direct_BT2 ; Type ComplexValue; }
  
      }
    Operation{
      For k In {0:#tabrealomegasexpansion()-1}
        Evaluate[$realomega=tabrealomegasexpansion(k)];
        GenerateListOfRHS[V,Omega,#tabrealomegasexpansion()];
      EndFor
      GenerateSeparate[M1];
      EigenSolveAndExpand[M1,neig_BT,target_re_BT,target_im_BT,
        {{-1/cel^2,0,0},{1},{1/cel,0},{ 4*Rout/cel,-3}       ,{Rout}},
        {           {1},{1},      {1},{-8*Rout^2/cel,8*Rout},{2*Rout/cel,-2}},
        tabrealomegasexpansion(),
        V];
      PostOperation[postop_modal_AR_BT2];
      For k In {0:#tabrealomegasexpansion()-1}
        Evaluate[$realomega=tabrealomegasexpansion(k)];
        Generate[M2];
        Solve[M2];
        PostOperation[postop_direct_BT2];
      EndFor
      For k In {0:#tabrealomegasexpansion()-1}
        PostOperation[postop_gmsh~{k}];
        Sleep[0.5];
      EndFor
    }
  }  

  { Name res_modal_AR_PML;
    System{
        { Name M1; NameOfFormulation form_modal_PML ; Type ComplexValue; }
        { Name V ; NameOfFormulation form_RHS       ; Type ComplexValue; }
      }
    Operation{
      For k In {0:#tabrealomegasexpansion()-1}
        Evaluate[$realomega=tabrealomegasexpansion(k)];
        GenerateListOfRHS[V,Omega,#tabrealomegasexpansion()];
      EndFor
      GenerateSeparate[M1];
      EigenSolveAndExpand[M1,neig_PML,target_re_BT,target_im_BT,
        {{1,0,0},{1}},
        {    {1},{1}},
        tabrealomegasexpansion(),
        V];
    }
  }  
  { Name res_modal_AR_mur;
    System{
      { Name V; NameOfFormulation form_RHS         ; Type ComplexValue; }
      { Name M1; NameOfFormulation form_modal_mur  ; Type ComplexValue; }
      { Name M2; NameOfFormulation form_direct_mur ; Type ComplexValue; }
      }
    Operation{
      For k In {0:#tabrealomegasexpansion()-1}
        Evaluate[$realomega=tabrealomegasexpansion(k)];
        GenerateListOfRHS[V,Omega,#tabrealomegasexpansion()];
      EndFor
      GenerateSeparate[M1];
      EigenSolveAndExpand[M1,neig_BT,target_re_BT,target_im_BT,
        {{1,0,0},{1},{1,0}},
        {    {1},{1},  {1}},
        tabrealomegasexpansion(),
        V];
      PostOperation[postop_modal_AR_mur];
      For k In {0:#tabrealomegasexpansion()-1}
        Evaluate[$realomega=tabrealomegasexpansion(k)];
        Generate[M2];
        Solve[M2];
        PostOperation[postop_direct_mur];
      EndFor
      For k In {0:#tabrealomegasexpansion()-2}
        PostOperation[postop_gmsh~{k}];
        Sleep[0.5];
      EndFor
    }
  }
}

PostProcessing {
  { Name postpro_direct_PML; NameOfFormulation form_direct_PML;
    Quantity {
      { Name us ; Value { Local{ [CompZ[{u}     ]]; In Omega; Jacobian JVol; } } }
	    { Name ut ; Value { Local{ [CompZ[{u}+ui[]]]; In Omega; Jacobian JVol; } } }
      { Name intnorm2utot ; Value { Integral { [SquNorm[{u}+ui[]]]; In Scat ; Jacobian JVol; Integration Int_1 ; } } }
	  }
  }
  { Name postpro_direct_mur; NameOfFormulation form_direct_mur;
    Quantity {
      { Name us ; Value { Local{ [CompZ[{u}     ]]; In Omega; Jacobian JVol; } } }
	    { Name ut ; Value { Local{ [CompZ[{u}+ui[]]]; In Omega; Jacobian JVol; } } }
      { Name intnorm2utot ; Value { Integral { [SquNorm[{u}+ui[]]]; In Scat ; Jacobian JVol; Integration Int_1 ; } } }
	  }
  }
  { Name postpro_direct_BT2; NameOfFormulation form_direct_BT2;
    Quantity {
      { Name us ; Value { Local{ [CompZ[{u}     ]]; In Omega; Jacobian JVol; } } }
	    { Name ut ; Value { Local{ [CompZ[{u}+ui[]]]; In Omega; Jacobian JVol; } } }
      { Name intnorm2utot ; Value { Integral { [SquNorm[{u}+ui[]]]; In Scat ; Jacobian JVol; Integration Int_1 ; } } }
	  }
  }

  // ANCHOR POSTPRO_modal_PML
  { Name postpro_modal_PML; NameOfFormulation form_modal_PML;
    Quantity {
      { Name u       ; Value { Local{ [CompZ[{u}]     ]      ; In Omega     ; Jacobian JVol; } } }
      { Name normu   ; Value { Local{ [ Norm[{u}]     ]      ; In Omega     ; Jacobian JVol; } } }
      { Name ev_re   ; Value { Local{ [$EigenvalueReal]      ; In PrintPoint; Jacobian JVol; } } }
      { Name ev_im   ; Value { Local{ [$EigenvalueImag]      ; In PrintPoint; Jacobian JVol; } } }
      { Name vol ; Value { Integral { [1]   ; In PrintPoint       ; Jacobian JVol; Integration Int_1 ; } } }
      { Name eig_re ; Value { Integral { [$EigenvalueReal/$vol] ; In PrintPoint ; Jacobian JVol; Integration Int_1 ; } } }
      { Name eig_im ; Value { Integral { [$EigenvalueImag/$vol] ; In PrintPoint ; Jacobian JVol; Integration Int_1 ; } } }
      { Name ev  ; Value { Integral { [Complex[$EigenvalueReal,$EigenvalueImag]/$vol]   ; In PrintPoint; Jacobian JVol; Integration Int_1 ; } } }
      For i In {0:#tabrealomegasexpansion()-1}
        { Name overlap~{i} ; Value { Integral { [{u}*source_PW~{i}[]] ; In Scat ; Jacobian JVol; Integration Int_1 ; } } }
      EndFor
      { Name den    ; Value { Integral { [{u}*epsr[]*{u}] ; In Omega ; Jacobian JVol; Integration Int_1 ; } } }
      For i In {0:#tabrealomegasexpansion()-1}
        { Name us_PML_modal_manual~{i};
          Value {
            For j In {0:(neig_PML-1)}
              // cel^2/(w^2-w_k^2)
              Local { [ CompZ[cel^2/(tabrealomegasexpansion(i)^2-$ev~{j}^2) * $overlap~{i}~{j}/$den~{j} * {u}[neig_PML-1-j]]] ; In Omega; Jacobian JVol; }
              // // cel^2/(2*w*(w-w_k))
              // Local { [ CompZ[cel^2/(2*tabrealomegasexpansion(i)*(tabrealomegasexpansion(i)-$ev~{j})) * $overlap~{i}~{j}/$den~{j} * {u}[neig_PML-1-j]]] ; In Omega; Jacobian JVol; }
              // // NEP style : cel^2/(2*w_k*(w-w_k))
              // Local { [ CompZ[cel^2/(2*$ev~{j}*(tabrealomegasexpansion(i)-$ev~{j})) * $overlap~{i}~{j}/$den~{j} * {u}[neig_PML-1-j]]] ; In Omega; Jacobian JVol; }
            EndFor
          }
        }
        { Name us_PML_modal_manual_stored~{i}; Value {Local { [us_exp_PML_modal_manual~{i}[]] ; In Omega; Jacobian JVol; } } }
      EndFor
      For i In {0:#tabrealomegasexpansion()-1}
        { Name intnorm2utot~{i} ; Value { Integral { [SquNorm[us_exp_PML_modal_manual~{i}[] + CompZ[ui~{i}[]]]]; In Scat; Jacobian JVol; Integration Int_1 ; } } }
      EndFor
    }
  }

  { Name postpro_modal_AR_BT2; NameOfFormulation form_modal_BT2;
    Quantity {
      { Name u       ; Value { Local{ [CompZ[{u}]     ]      ; In Omega     ; Jacobian JVol; } } }
      { Name normu   ; Value { Local{ [ Norm[{u}]     ]      ; In Omega     ; Jacobian JVol; } } }
      { Name ev_re   ; Value { Local{ [$EigenvalueReal]      ; In PrintPoint; Jacobian JVol; } } }
      { Name ev_im   ; Value { Local{ [$EigenvalueImag]      ; In PrintPoint; Jacobian JVol; } } }
      { Name vol ; Value { Integral { [1]   ; In PrintPoint       ; Jacobian JVol; Integration Int_1 ; } } }
      { Name eig_re ; Value { Integral { [$EigenvalueReal/$vol] ; In PrintPoint ; Jacobian JVol; Integration Int_1 ; } } }
      { Name eig_im ; Value { Integral { [$EigenvalueImag/$vol] ; In PrintPoint ; Jacobian JVol; Integration Int_1 ; } } }
      For i In {0:#tabrealomegasexpansion()-1}
        { Name intnorm2utot~{i} ; Value { Integral { [SquNorm[us_exp_BT2_modal~{i}[] + CompZ[ui~{i}[]]]] ; In Scat ; Jacobian JVol; Integration Int_1 ; } } }
      EndFor
	  }
  }
  { Name postpro_modal_AR_PML; NameOfFormulation form_modal_PML;
    Quantity {
      { Name u       ; Value { Local{ [CompZ[{u}]     ] ; In Omega     ; Jacobian JVol; } } }
      { Name ev_re   ; Value { Local{ [$EigenvalueReal] ; In PrintPoint; Jacobian JVol; } } }
      { Name ev_im   ; Value { Local{ [$EigenvalueImag] ; In PrintPoint; Jacobian JVol; } } }
      For i In {0:#tabrealomegasexpansion()-1}
        { Name intnorm2utot~{i} ; Value { Integral { [SquNorm[us_exp_PML_modal~{i}[] + CompZ[ui~{i}[]]]] ; In Scat ; Jacobian JVol; Integration Int_1 ; } } }
      EndFor
    }
  }
  { Name postpro_modal_AR_mur; NameOfFormulation form_modal_mur;
    Quantity {
      { Name u       ; Value { Local{ [CompZ[{u}]     ] ; In Omega     ; Jacobian JVol; } } }
      { Name ev_re   ; Value { Local{ [$EigenvalueReal] ; In PrintPoint; Jacobian JVol; } } }
      { Name ev_im   ; Value { Local{ [$EigenvalueImag] ; In PrintPoint; Jacobian JVol; } } }
      { Name vol ; Value { Integral { [1]   ; In PrintPoint       ; Jacobian JVol; Integration Int_1 ; } } }
      { Name eig_re ; Value { Integral { [$EigenvalueReal/$vol] ; In PrintPoint ; Jacobian JVol; Integration Int_1 ; } } }
      { Name eig_im ; Value { Integral { [$EigenvalueImag/$vol] ; In PrintPoint ; Jacobian JVol; Integration Int_1 ; } } }
      For i In {0:#tabrealomegasexpansion()-1}
        { Name intnorm2utot~{i} ; Value { Integral { [SquNorm[us_exp_mur_modal~{i}[] + CompZ[ui~{i}[]]]] ; In Scat ; Jacobian JVol; Integration Int_1 ; } } }
      EndFor
	  }
  }
}

PostOperation {
  { Name postop_direct_PML; NameOfPostProcessing postpro_direct_PML ;
    Operation {
      Print[ us   , OnElementsOf Omega, File "out/us_PML_direct.pos", ChangeOfCoordinates {$X+domain_tot*yshift,$Y,$Z}, Name "us_PML_direct"];
      Print[ intnorm2utot[Scat] , OnGlobal, File "out/ACC_PML_direct.out", Format SimpleTable];
      If(flag_PRINTFIELDS==1)
        Print[ us , OnElementsOf Omega, File "out/us_PML.pos", Name "us_PML"];
      EndIf
	  }
  }
  { Name postop_direct_mur; NameOfPostProcessing postpro_direct_mur ;
    Operation {
      Print[ us   , OnElementsOf Omega, File "out/us_mur_direct.pos", ChangeOfCoordinates {$X+domain_tot*yshift,$Y,$Z} , Name "us_mur_direct"];
      Print[ intnorm2utot[Scat] , OnGlobal, File "out/ACC_mur_direct.out", Format SimpleTable];
      If(flag_PRINTFIELDS==1)
        Print[ us   , OnElementsOf Omega, File "out/us_mur.pos", Name "us_mur"];
      EndIf
	  }
  }
  { Name postop_direct_BT2; NameOfPostProcessing postpro_direct_BT2 ;
    Operation {
      Print[ us   , OnElementsOf Omega, File "out/us_BT2_direct.pos", ChangeOfCoordinates {$X+domain_tot*yshift,$Y,$Z} ,  Name "us_BT2_direct"];
      Print[ intnorm2utot[Scat] , OnGlobal, File "out/ACC_BT2_direct.out", Format SimpleTable];      
      If(flag_PRINTFIELDS==1)
        Print[ us   , OnElementsOf Omega, File "out/us_BT2.pos", Name "us_BT2"];
      EndIf
	  }
  }
  { Name postop_modal_PML; NameOfPostProcessing postpro_modal_PML ;
    Operation {
      Print [ev_re , OnElementsOf PrintPoint , Format TimeTable, File "out/EigenValuesReal_PML.txt"];
      Print [ev_im , OnElementsOf PrintPoint , Format TimeTable, File "out/EigenValuesImag_PML.txt"];
      Print [vol[PrintPoint] , OnGlobal, TimeStep 0, StoreInVariable $vol,Format Table];
      For j In {0:(neig-1)}
        Print [eig_re[PrintPoint] , OnGlobal, TimeStep j , Format Table , SendToServer "GetDP/eig_re"];
        Print [eig_im[PrintPoint] , OnGlobal, TimeStep j , Format Table , SendToServer "GetDP/eig_im"];
      EndFor
      Print [u     , OnElementsOf Omega, File "out/u_PML.pos", Name "u_PML"];
      If (flag_manualexp==1)
        Print [vol[PrintPoint] , OnGlobal, TimeStep 0, StoreInVariable $vol,Format Table];
        For i In {0:#tabrealomegasexpansion()-1}
          For j In {0:(neig_PML-1)}
            Print [overlap~{i}[Scat] , OnGlobal, TimeStep j, StoreInVariable $overlap~{i}~{j},Format Table];
          EndFor
        EndFor
        For j In {0:(neig_PML-1)}
          Print [den[Omega] , OnGlobal, TimeStep j, StoreInVariable $den~{j},Format Table];
        EndFor
        For j In {0:(neig_PML-1)}
          Print [ev[PrintPoint] , OnGlobal, TimeStep j, StoreInVariable $ev~{j}, Format SimpleTable];
        EndFor
        For i In {0:(#tabrealomegasexpansion()-1)}
          Print [us_PML_modal_manual~{i} , OnElementsOf Omega, StoreInField (stored_exp_field_manual_PML+i), File Sprintf["out/us_PML_modal_manual_%03g.pos",i], Name Sprintf["us_PML_modal_manual_%03g.pos",i], LastTimeStepOnly];
        EndFor
        For i In {0:(#tabrealomegasexpansion()-1)}
          Print [intnorm2utot~{i}[Scat] , OnGlobal , Format SimpleTable,File Sprintf["out/ACC_PML_modal_manual_%03g.out",i],LastTimeStepOnly];
        EndFor
      EndIf
	  }
  }
  { Name postop_modal_AR_BT2; NameOfPostProcessing postpro_modal_AR_BT2 ;
    Operation {
      Print [ev_re        , OnElementsOf PrintPoint , Format TimeTable, File "out/EigenValuesReal_BT2.txt"];
      Print [ev_im        , OnElementsOf PrintPoint , Format TimeTable, File "out/EigenValuesImag_BT2.txt"];
      Print [vol[PrintPoint] , OnGlobal, TimeStep 0, StoreInVariable $vol,Format Table];
      For j In {0:(neig-1)}
        Print [eig_re[PrintPoint] , OnGlobal, TimeStep j , Format Table , SendToServer "GetDP/eig_re"];
        Print [eig_im[PrintPoint] , OnGlobal, TimeStep j , Format Table , SendToServer "GetDP/eig_im"];
      EndFor
      Print [u     , OnElementsOf Omega, File "out/u_BT2.pos", Name "u_BT2"];
      For k In {0:#tabrealomegasexpansion()-1}
        Print [u , OnElementsOf Omega, StoreInField (stored_exp_field_AR_BT2+k), File Sprintf["out/us_BT2_modal_%03g.pos",k],Name "us_BT2_modal",TimeStep {neig_BT+k}];
        Print [intnorm2utot~{k}[Scat] , OnGlobal, File Sprintf["out/ACC_BT2_modal_%03g.out",k], Format SimpleTable, TimeStep {neig_BT+k}];
      EndFor
	  }
  }
  { Name postop_modal_AR_PML; NameOfPostProcessing postpro_modal_AR_PML ;
    Operation {
      Print [ev_re        , OnElementsOf PrintPoint , Format TimeTable, File "out/EigenValuesReal_PML.txt"];
      Print [ev_im        , OnElementsOf PrintPoint , Format TimeTable, File "out/EigenValuesImag_PML.txt"];
      // Print [u     , OnElementsOf Omega, File "out/u_PML.pos", Name "u_BT2"];
      For k In {0:#tabrealomegasexpansion()-1}
        Print [u , OnElementsOf Omega, StoreInField (stored_exp_field_AR_PML+k), File Sprintf["out/us_PML_modal_%03g.pos",k], Name "us_pml_modal",TimeStep {neig_PML+k}];
        Print [intnorm2utot~{k}[Scat], OnGlobal , Format SimpleTable,File Sprintf["out/ACC_PML_modal_%03g.out",k],LastTimeStepOnly];
      EndFor
	  }
  }
  { Name postop_modal_AR_mur; NameOfPostProcessing postpro_modal_AR_mur ;
    Operation {
      Print [ev_re        , OnElementsOf PrintPoint , Format TimeTable, File "out/EigenValuesReal_mur.txt"];
      Print [ev_im        , OnElementsOf PrintPoint , Format TimeTable, File "out/EigenValuesImag_mur.txt"];
      Print [vol[PrintPoint] , OnGlobal, TimeStep 0, StoreInVariable $vol,Format Table];
      For j In {0:(neig-1)}
        Print [eig_re[PrintPoint] , OnGlobal, TimeStep j , Format Table , SendToServer "GetDP/eig_re"];
        Print [eig_im[PrintPoint] , OnGlobal, TimeStep j , Format Table , SendToServer "GetDP/eig_im"];
      EndFor
      // Print [u     , OnElementsOf Omega, File "out/u_mur.pos", Name "u_BT2"];
      For k In {0:#tabrealomegasexpansion()-1}
        Print [u , OnElementsOf Omega,StoreInField (stored_exp_field_AR_mur+k), File Sprintf["out/us_mur_modal_%03g.pos",k], Name "us_mur_modal",TimeStep {neig_BT+k}];
        Print [intnorm2utot~{k}[Scat] , OnGlobal, File Sprintf["out/ACC_mur_modal_%03g.out",k], Format SimpleTable, TimeStep {neig_BT+k}];
      EndFor
	  }
  }
  For k In {0:#tabrealomegasexpansion()-1}
    { Name postop_gmsh~{k} ; NameOfPostProcessing postpro_modal_AR_BT2;
      Operation {
        Echo[Str["Geometry.Points = 0;","Geometry.Color.Curves = {0,0,0};","Mesh.SurfaceEdges = 0;",
        "k=PostProcessing.NbViews-1;","View[k].Visible = 0;","View.Visible = 0;","View[0].Visible = 1;",
        Sprintf["View[%g].Visible = 0;",k],
        Sprintf["View[%g].Visible = 0;",nb_Omegas+k],
        Sprintf["View[%g].Visible = 1;",k+1],
        Sprintf["View[%g].ColormapNumber = 7;",k+1],
        Sprintf["View[%g].IntervalsType = 3;",k+1],
        Sprintf["View[%g].NbIso = 30;",k+1],
        Sprintf["View[%g].Visible = 1;",nb_Omegas+k+1],
        Sprintf["View[%g].ColormapNumber = 7;",nb_Omegas+k+1],
        Sprintf["View[%g].IntervalsType = 3;",nb_Omegas+k+1],
        Sprintf["View[%g].NbIso = 30;",nb_Omegas+k+1]], File "tmp0.geo"];
        // Echo[Str["Plugin(Annotate).Text= \"TOP : DIRECT -- BOTTOM : EXPANSION\";" , "Plugin(Annotate).Run;"]];
      }
    }  
  EndFor
}

slepc_options_rg  = Sprintf[" -rg_interval_endpoints %.8lf,%.8lf,%.8lf,%.8lf ",eig_min_re,eig_max_re,eig_min_im,eig_max_im];
DefineConstant[
  R_ = {"res_nleig_rat_exp", Name "GetDP/1ResolutionChoices", Visible 1},
  C_ = {StrCat["-pre ",str_res," -cal -slepc ",str_slepc_solver," ",slepc_options_rg] , Name "GetDP/9ComputeCommand", Visible 1}
];

DefineConstant[
  eig_re_  = {0, Name "GetDP/eig_re", ReadOnly 1, Graph "10", Visible 1},
  eig_im_  = {0, Name "GetDP/eig_im", ReadOnly 1, Graph "01", Visible 1}
];
