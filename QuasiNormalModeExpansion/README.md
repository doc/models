ONELAB models for the expansion of electromagnetic problems in Quasi-Normal Modes in open and/or dispersive media.

## Quick Install
Open `QNM_expansion_dispersive_media.pro` with e.g. your precompiled version of [ONELAB](https://onelab.info) to compute Quasi-Normal Modes of a dispersive structure. The numerical resolvent [1] is then applied to a specific source and the results are compared to those of the corresponding direct problem side by side [2].

## What's inside
These models heavily rely on slepc for the solving of non-linear eigenvalue problems [1-3].

## Authors
Guillaume Demésy

## References
[1] See [slepc doc](https://slepc.upv.es/documentation/current/docs/manualpages/NEP/NEPApplyResolvent.html).

[2] See "[Photonics in highly dispersive media: The exact modal expansion](https://arxiv.org/abs/1807.02355)" for details about Dispersive QNMs.

[3] See "[NEP: a module for the parallel solution of nonlinear eigenvalue problems in SLEPc](https://arxiv.org/abs/1910.11712)".
