///////////////////////////////
// Author : Guillaume Demesy //
///////////////////////////////

Include "QNM_expansion_DtN_data.geo";
SetFactory("OpenCASCADE");
lc_bg = lambda0/paramaille;
lc_sc = lambda0/(paramaille*3);

Rectangle(1) = {-dx_scat/2, -dy_scat/2, 0, dx_scat, dy_scat, 0};
Disk(2) = {0, 0, 0, r_domain, r_domain};
If (flag_PML==1)
  Disk(3) = {0, 0, 0, r_pml, r_pml};
EndIf
Coherence;

Physical Point("PrintPoint",100) = {1};

Physical Surface("SCAT",1)       = {1};
If (flag_PML==1)
  Physical Surface("BG",2)  = {2};
  Characteristic Length {6} = lc_bg;
  Physical Surface("PML",3) = {3};
  Physical Surface("SRC",4) = {};
  Characteristic Length {2,3,4,5} = lc_sc;
  Characteristic Length {1}       = lc_bg;
  Characteristic Length {7}       = lc_bg;
  Physical Line("SURF_SM",10)      = {};
  Else
  Physical Surface("BG",2)  = {2};
  Physical Surface("SRC",4) = {};
  Characteristic Length {2,3,4,5} = lc_sc;
  Characteristic Length {1}       = lc_bg;
  Physical Line("SURF_SM",10)      = {1};
EndIf

Rectangle(10) = {-dx_scat/2+domain_tot*yshift, -dy_scat/2, 0, dx_scat, dy_scat, 0};
Disk(20) = {domain_tot*yshift, 0, 0, r_domain, r_domain};
Delete{Surface{10,20};}
If (flag_PML==1)
  Disk(30) = {domain_tot*yshift, 0, 0, r_pml, r_pml};
  Delete{Surface{30};}
EndIf

If (flag_PML==1)
  Point(newp) = {-dx_scat/2+domain_tot*yshift, 2*r_pml, 0};
Else
  Point(newp) = {-dx_scat/2+domain_tot*yshift, 2*r_domain, 0};
EndIf
Geometry.Points = 0;

