///////////////////////////////
// Author : Guillaume Demesy //
///////////////////////////////

Include "QNM_expansion_dispersive_media_data.geo";

Group {
  // Domains
  Om_1		   = Region[1];
  Om_src     = Region[3];
  Om_2		   = Region[{2,3}];
  Om	  	   = Region[{Om_1,Om_2}];
  Om_but_src = Region[{Om,-Om_src}];

  Surfdiri     = Region[10];
  Bound        = Region[20];
  pt_src       = Region[100];
  green_x_src  = Region[30];
  green_y_src  = Region[31];
  PrintPoint   = Region[100];
  Om_plot     = Region[{Om,-Om_src}];
}

Function{
  I[]               = Complex[0,1];
  k0[]              = $realomega/cel;
  mur[]             = TensorDiag[1,1,1];
  epsr_nod[]        = TensorDiag[1,1,1];
  om_lorentz_1[]    = Complex[om_lorentz_1_re,om_lorentz_1_im];
  mur_direct[]      = 1;
  epsr_direct[Om_1] = 1 + a_lorentz_1/($realomega-om_lorentz_1[]) - a_lorentz_1/($realomega+Conj[om_lorentz_1[]]);
  epsr_direct[Om_2] = 1;
  epsr_mau1[]       = 1 + I[]*a_lorentz_1/($Eigenvalue-I[]*om_lorentz_1[]) - I[]*a_lorentz_1/($Eigenvalue+I[]*Conj[om_lorentz_1[]]);
  
  If (flag_vector_case==0) 
      source_RHS[Om_src]     =  Vector[0,0,1];
      source_RHS[Om_but_src] =  Vector[0,0,0];
  Else
      source_RHS[Om_src]     =  Vector[1,0,0];
      source_RHS[Om_but_src] =  Vector[0,0,0];
  EndIf
}

Constraint {
	{Name Dirichlet; Type Assign;
		Case {
			{ Region Surfdiri ; Value 0.; }
		}
	}
  
  { Name Dirichlet_src;
    Case {
      { Region green_x_src;
        Type Assign; Value 0. ;
      }
    }
  }
}

Jacobian {
  {Name JVol ; Case { { Region All ; Jacobian Vol ; } } }
  {Name JSur ; Case { { Region All ; Jacobian Sur ; } } }
  {Name JLin ; Case { { Region All ; Jacobian Lin ; } } }
}

Integration {
  { Name Int_1 ;
    Case {
      { Type Gauss ;
        Case {
	        { GeoElement Point       ; NumberOfPoints  1 ; }
	        { GeoElement Line        ; NumberOfPoints  4 ; }
	        { GeoElement Triangle    ; NumberOfPoints  12 ; }
			  }
      }
    }
  }
}

FunctionSpace {
  { Name Hgrad_perp; Type Form1P;
    BasisFunction {
      { Name un;  NameOfCoef un;  Function BF_PerpendicularEdge_1N; Support Region[Om]; Entity NodesOf[All]; }
      { Name un2; NameOfCoef un2; Function BF_PerpendicularEdge_2E; Support Region[Om]; Entity EdgesOf[All]; }
      If(flag_o2==1)
        { Name un3; NameOfCoef un3; Function BF_PerpendicularEdge_2F; Support Region[Om]; Entity FacetsOf[All]; }
        { Name un4; NameOfCoef un4; Function BF_PerpendicularEdge_3E; Support Region[Om]; Entity EdgesOf[All]; }
        { Name un5; NameOfCoef un5; Function BF_PerpendicularEdge_3F; Support Region[Om]; Entity FacetsOf[All]; }
      EndIf
     }
    Constraint {
      { NameOfCoef un;  EntityType NodesOf ; NameOfConstraint Dirichlet; }
      { NameOfCoef un2; EntityType EdgesOf ; NameOfConstraint Dirichlet; }
      If(flag_o2==1)
        { NameOfCoef un3; EntityType FacetsOf ; NameOfConstraint Dirichlet; }
        { NameOfCoef un4; EntityType EdgesOf  ; NameOfConstraint Dirichlet; }
        { NameOfCoef un5; EntityType FacetsOf ; NameOfConstraint Dirichlet; }
      EndIf
    }
  }

  { Name Hcurl; Type Form1;
    BasisFunction {
      { Name sn;  NameOfCoef un;  Function BF_Edge   ; Support Region[Om]; Entity EdgesOf[All]; }
      { Name sn2; NameOfCoef un2; Function BF_Edge_2E; Support Region[Om]; Entity EdgesOf[All]; }
      If(flag_o2==1)
        { Name sn3; NameOfCoef un3; Function BF_Edge_3F_a; Support Region[Om]; Entity FacetsOf[All]; }
        { Name sn4; NameOfCoef un4; Function BF_Edge_3F_b; Support Region[Om]; Entity FacetsOf[All]; }
        { Name sn5; NameOfCoef un5; Function BF_Edge_4E  ; Support Region[Om]; Entity EdgesOf[All]; }
      EndIf
    }
    Constraint {
      { NameOfCoef un ; EntityType EdgesOf ; NameOfConstraint Dirichlet; }
      { NameOfCoef un2; EntityType EdgesOf ; NameOfConstraint Dirichlet; }
      { NameOfCoef un3; EntityType FacetsOf; NameOfConstraint Dirichlet; }
      { NameOfCoef un4; EntityType FacetsOf; NameOfConstraint Dirichlet; }
      { NameOfCoef un5; EntityType EdgesOf ; NameOfConstraint Dirichlet; }

    }
  }
  
  { Name Hcurl_direct_green_lin; Type Form1;
    BasisFunction {
      { Name sn;  NameOfCoef un ; Function BF_Edge   ; Support Region[{Om,green_x_src}]; Entity EdgesOf[All]; }
      { Name sn2; NameOfCoef un2; Function BF_Edge_2E; Support Region[{Om,green_x_src}]; Entity EdgesOf[All]; }
      If(flag_o2==1)
        { Name sn3; NameOfCoef un3; Function BF_Edge_3F_a; Support Region[Om]; Entity FacetsOf[All]; }
        { Name sn4; NameOfCoef un4; Function BF_Edge_3F_b; Support Region[Om]; Entity FacetsOf[All]; }
        { Name sn5; NameOfCoef un5; Function BF_Edge_4E  ; Support Region[Om]; Entity EdgesOf[All]; }
      EndIf
    }
    Constraint{
      { NameOfCoef un2; EntityType EdgesOf ; NameOfConstraint Dirichlet_src; }
      { NameOfCoef un3; EntityType FacetsOf; NameOfConstraint Dirichlet_src; }
      { NameOfCoef un4; EntityType FacetsOf; NameOfConstraint Dirichlet_src; }
      { NameOfCoef un5; EntityType EdgesOf ; NameOfConstraint Dirichlet_src; }
    }
  }
}

Formulation {
  {Name modal_helmholtz; Type FemEquation;
    Quantity{
      If (flag_vector_case==0) 
        { Name u; Type Local; NameOfSpace Hgrad_perp;}
      Else
        { Name u; Type Local; NameOfSpace Hcurl;}
      EndIf
    }
    Equation {
      Galerkin { Eig[  1/mur[] *    Dof{Curl u}, {Curl u} ]; Rational 1; In Om  ; Jacobian JSur; Integration Int_1;}
      Galerkin { Eig[ epsr_nod[]/cel^2 * Dof{u}, {u}      ]; Rational 2; In Om_1; Jacobian JSur; Integration Int_1;}
      Galerkin { Eig[ epsr_nod[]/cel^2 * Dof{u}, {u}      ]; Rational 3; In Om_2; Jacobian JSur; Integration Int_1;}
    }
  }

  // if other matrices, fails to print eigs (conflict between M1 matrices)
  {Name form_RHS; Type FemEquation;
    Quantity{
      If (flag_vector_case==0) 
        { Name u; Type Local; NameOfSpace Hgrad_perp;}
      Else
        { Name u; Type Local; NameOfSpace Hcurl;}
      EndIf
    }
    Equation{ 
      Galerkin { [   1. * Dof{u} , {u} ]; In Om; Jacobian JVol; Integration Int_1;}
      Galerkin { [ source_RHS[] , {u} ]; In Om; Jacobian JVol; Integration Int_1;  }
    }
  }
  
  {Name direct_helmholtz; Type FemEquation;
    Quantity{
      If (flag_vector_case==0) 
        { Name u; Type Local; NameOfSpace Hgrad_perp;}
      Else
        { Name u; Type Local; NameOfSpace Hcurl;}
      EndIf
    }
    Equation {
      Galerkin { [ -1/mur_direct[]*Dof{Curl u}  , {Curl u}]; In Om           ; Jacobian JSur; Integration Int_1; }
      Galerkin { [ k0[]^2*epsr_direct[]*Dof{u}    , {u}]   ; In Om           ; Jacobian JSur; Integration Int_1; }
      Galerkin { [ -source_RHS[]                , {u}]     ; In Om_src; Jacobian JVol; Integration Int_1;  }
    }
  }
}

Resolution {
  { Name res_nleig_rat_exp;
    System{
      { Name V ; NameOfFormulation form_RHS   ; Type ComplexValue; }
      { Name M1; NameOfFormulation modal_helmholtz ; Type ComplexValue; }
      { Name M2; NameOfFormulation direct_helmholtz ; Type ComplexValue; }
    }
    Operation{
      CreateDir["out"];
      For k In {0:#tabrealomegasexpansion()-1}
        Evaluate[$realomega=tabrealomegasexpansion(k)];
        GenerateListOfRHS[V,Om,#tabrealomegasexpansion()];
      EndFor
      GenerateSeparate[M1];
      EigenSolveAndExpand[M1,neig,eig_target_re,eig_target_im,
          { {1}, {-1,-2*om_lorentz_1_im,2*a_lorentz_1*om_lorentz_1_re-om_lorentz_1_mod^2,0,0}, {1,0,0} } ,
          { {1}, {-1,-2*om_lorentz_1_im,-om_lorentz_1_mod^2},                                  {1} } ,
          tabrealomegasexpansion(),
          V];
      PostOperation[postop_lorentz_nleig_rat_exp];
      For k In {0:#tabrealomegasexpansion()-1}
        Evaluate[$realomega=tabrealomegasexpansion(k)];
        Generate[M2];
        Solve[M2];
        PostOperation[postop_direct_helmholtz];
      EndFor
      For k In {0:#tabrealomegasexpansion()-1}
        PostOperation[postop_gmsh~{k}];
        Sleep[0.3];
      EndFor
    }
  }
}

PostProcessing {
  { Name postpro_lorentz_nleig_rat_exp; NameOfFormulation modal_helmholtz;
    Quantity {
      { Name intnormu       ; Value { Integral { [ Norm[{u}]    ] ; In Om    ; Jacobian JSur; Integration Int_1 ; } } }
      { Name intnormu_rod   ; Value { Integral { [ Norm[{u}]    ] ; In Om_1    ; Jacobian JSur; Integration Int_1 ;} } }
      { Name u   ; Value { Local { [ {u}    ] ; In Om; Jacobian JSur; } } }
      { Name u_im; Value { Local { [ Im[{u}]    ] ; In Om; Jacobian JSur; } } }
      { Name EigenValuesReal;  Value { Local{ [$EigenvalueReal]; In PrintPoint; Jacobian JSur; } } }
      { Name EigenValuesImag;  Value { Local{ [$EigenvalueImag]; In PrintPoint; Jacobian JSur; } } }
      { Name vol ; Value { Integral { [1]   ; In PrintPoint       ; Jacobian JVol; Integration Int_1 ; } } }
      { Name eig_re ; Value { Integral { [$EigenvalueReal/$vol] ; In PrintPoint ; Jacobian JVol; Integration Int_1 ; } } }
      { Name eig_im ; Value { Integral { [$EigenvalueImag/$vol] ; In PrintPoint ; Jacobian JVol; Integration Int_1 ; } } }
    }
  }

  { Name postpro_direct_helmholtz; NameOfFormulation direct_helmholtz;
    Quantity {
      { Name eps          ; Value { Local { [ epsr_direct[]] ; In Om   ; Jacobian JSur; } } }
      { Name source_RHS   ; Value { Local { [ source_RHS[] ] ; In Om; Jacobian JSur; } } }
      { Name u_direct     ; Value { Local { [ {u}          ] ; In Om; Jacobian JSur; } } }
      { Name u_direct_im  ; Value { Local { [ Im[{u}]      ] ; In Om; Jacobian JSur; } } }
      { Name intnormu_rod ; Value { Integral { [ Norm[{u}] ] ; In Om_1 ; Jacobian JSur; Integration Int_1 ;} } }
	  }
  }
}


PostOperation { 
  { Name postop_lorentz_nleig_rat_exp; NameOfPostProcessing postpro_lorentz_nleig_rat_exp ;
    Operation {
      Print [vol[PrintPoint] , OnGlobal, TimeStep 0, StoreInVariable $vol,Format Table];
      For j In {0:(neig-1)}
        Print [eig_re[PrintPoint] , OnGlobal, TimeStep j , Format Table , SendToServer "GetDP/eig_re"];
        Print [eig_im[PrintPoint] , OnGlobal, TimeStep j , Format Table , SendToServer "GetDP/eig_im"];
      EndFor
      Print [EigenValuesReal, OnElementsOf PrintPoint, Format TimeTable, File "out/EigenValuesReal.txt"];
      Print [EigenValuesImag, OnElementsOf PrintPoint, Format TimeTable, File "out/EigenValuesImag.txt"];
      Print [ u  , OnElementsOf Om, File Sprintf["out/u.pos"], EigenvalueLegend];
      Print [ u  , OnPoint {cx_src,-cy_src,0}, Format TimeTable, File "out/u_expand_on_probe_point.txt"];
      For k In {0:#tabrealomegasexpansion()-1}
        // Print [ u_im, OnElementsOf Om, File Sprintf["out/u_exp_%03g.out",k], Name Sprintf["u_expand_imag_%03g (left)",k] , EigenvalueLegend, TimeStep {neig+k}];
        Print [ u_im, OnGrid {$A,$B, 0} {-a_lat:a_lat:a_lat/30, -a_lat/2:a_lat/2:a_lat/15, 0}, File Sprintf["out/u_exp_%03g.out",k], Name Sprintf["u_expand_imag_%03g (left)",k] , EigenvalueLegend, TimeStep {neig+k}];
      EndFor
    }
  }
  { Name postop_direct_helmholtz; NameOfPostProcessing postpro_direct_helmholtz ;
    Operation {
      Print[ u_direct_im, OnGrid {$A,$B, 0} {-a_lat:a_lat:a_lat/30, -a_lat/2:a_lat/2:a_lat/15, 0}, File "out/u_direct.pos" , ChangeOfCoordinates {$X+xshift*2*a_lat,$Y,$Z} , Name "u_direct_imag (right)"];      
      Print[ u_direct , OnPoint {cx_src,-cy_src,0}, Format TimeTable, File >> "out/u_direct_on_probe_point.txt"];
      Print [intnormu_rod[Om_1], OnGlobal, Format TimeTable, File "out/intnormu_direct_on_src_point.out" ];
		}
  }
  For k In {0:#tabrealomegasexpansion()-1}
    { Name postop_gmsh~{k}; NameOfPostProcessing postpro_direct_helmholtz ;
      Operation {
        Echo[Str["Geometry.Points = 0;","Geometry.Color.Curves = {0,0,0};","Mesh.SurfaceEdges = 0;",
        "k=PostProcessing.NbViews-1;","View[k].Visible = 0;",
        "View.Visible = 0;","View[0].Visible = 1;", "View[0].ColormapNumber = 7;",
        Sprintf["View[%g].Visible = 0;",k],
        Sprintf["View[%g].Visible = 0;",nb_Omegas+k],
        Sprintf["View[%g].ColormapNumber = 7;",k+1],
        Sprintf["View[%g].ColormapNumber = 7;",nb_Omegas+k+1],
        Sprintf["View[%g].Visible = 1;",k+1],
        Sprintf["View[%g].Visible = 1;",nb_Omegas+k+1]], File "tmp0.geo"];
        // Echo[Str["Plugin(Annotate).Text= "TOP : DIRECT -- BOTTOM : EXPANSION\";" , "Plugin(Annotate).Run;"]];
      }
    }  
  EndFor
}


slepc_options_rg  = Sprintf[" -rg_interval_endpoints %.8lf,%.8lf,%.8lf,%.8lf ",eig_min_re,eig_max_re,eig_min_im,eig_max_im];
DefineConstant[
  R_ = {"res_nleig_rat_exp", Name "GetDP/1ResolutionChoices", Visible 1},
  C_ = {StrCat["-pre res_nleig_rat_exp -cal -slepc -nep_type nleigs -nep_rational -nep_target_magnitude -nep_view_pre -nep_ncv 300 -nep_monitor_all :temp_eigenvalues.txt",slepc_options_rg] , Name "GetDP/9ComputeCommand", Visible 1}
];

DefineConstant[
  eig_re_  = {0, Name "GetDP/eig_re", ReadOnly 1, Graph "10", Visible 0},
  eig_im_  = {0, Name "GetDP/eig_im", ReadOnly 1, Graph "01", Visible 0}
];

// DefineConstant[
//   R_ = {"res_nleig_rat_exp", Name "GetDP/1ResolutionChoices", Visible 1},
//   C_ = {"-pre res_nleig_rat_exp -cal -slepc -nep_max_it 1000 -nep_type nleigs -nep_rational -nep_target_magnitude -nep_ncv 100 -rg_interval_endpoints 0.00000000,0.05763407,0.00390394,3.12315286 -nep_view_pre -nep_monitor_all :temp_eigenvalues.txt", Name "GetDP/9ComputeCommand", Visible 1}
// ];
