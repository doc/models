# getDP model of a helicoidal symmetric power cable
This getDP model is an implementation answer of the question _"how to solve a helicoidal symmetric, time-harmonic eddy current boundary value problem (BVP)?"_ In a nutshell: Here, a special coordinate transformation is used to transform a 3D BVP into an equivalent 2D BVP, which is then solved by the finite element method (based on a **H**-φ formulation using proper manipulated cohomology basis fuctions). This is demonstrated for a cable with 13 conductors, carrying each the same total current of √2/13 Amperes at frequency 50 Hz. The resulting magnetic field **H** and current density **J** are plotted in the _physical_ coordinate system (x, y, z) as well as in the helicoidal coordinate system (u, v, w).
The model had been developed by Julien Dular (University of Liege) and Albert Piwonski (Technische Universität Berlin).

Here you can find further information about the model and ansatz:
- [IEEE Transactions on Magnetics paper version](https://ieeexplore.ieee.org/document/10006756)
- [arXiv paper version](https://arxiv.org/abs/2301.03370)

This directory contains:
- getDP solver file: helicoidal_power_cable.pro (extensively documented and close to the paper notation as possible)
→ open this file using Gmsh
- Gmsh mesh file: helicoidal_power_cable.msh

I.e., the .msh file was not generated using a .geo scripting file, but using the [Julia API for Gmsh](https://gmsh.info/). This is because the calculation of the conductors' cross-sections was performed in Julia as well. Also, the .pro file may not look like a _typical_ getDP .pro file, since [Julia's string interpolation functions](https://docs.julialang.org/en/v1/manual/strings/#string-interpolation) were used extensively to create it automatically.
