Include "param_EnergHyst.dat";

Function {
 
  dim = dim00;
  N   = N00; //nbr de cercle

  FLAG_ANHYLAW = FLAG_ANHYLAW00; 
/* 
FLAG_ANHYLAW = 1 --> hyperbolic tangent
FLAG_ANHYLAW = 2 --> double langevin function
*/
  FLAG_APPROACH = FLAG_APPROACH00; 
/*
FLAG_APPROACH = 1 --> variational approach (Jk)
FLAG_APPROACH = 2 --> vector play model approach (hrk)
FLAG_APPROACH = 3 --> full differential approach (hrk)
*/
  FLAG_INVMETHOD = FLAG_INVMETHOD00; 
/*
FLAG_INVMETHOD = 1 --> NR_ana (homemade)
FLAG_INVMETHOD = 2 --> NR_num (homemade)
FLAG_INVMETHOD = 3 --> bfgs (homemade)
*/
  FLAG_JACEVAL = FLAG_JACEVAL00; 
/*
FLAG_JACEVAL = 1 --> analytical Jacobian
FLAG_JACEVAL = 2 --> numerical Jacobian
*/
  FLAG_MINMETHOD = FLAG_MINMETHOD00; 
/*
FLAG_MINMETHOD = 1 --> steepest descent (homemade)
FLAG_MINMETHOD = 2 --> conjugate fr (gsl)
FLAG_MINMETHOD = 3 --> conjugate pr (gsl)
FLAG_MINMETHOD = 4 --> bfgs2 (gsl)
FLAG_MINMETHOD = 5 --> bfgs (gsl)
FLAG_MINMETHOD = 6 --> steepest descent (gsl)
FLAG_MINMETHOD = 11   --> steepest descent+ (homemade)\n"
FLAG_MINMETHOD = 22   --> conjugate Fletcher-Reeves (homemade)\n"
FLAG_MINMETHOD = 33   --> conjugate Polak-Ribiere (homemade)\n"
FLAG_MINMETHOD = 333  --> conjugate Polak-Ribiere+ (homemade)\n"
FLAG_MINMETHOD = 1999 --> conjugate Dai Yuan 1999 (p.85) (homemade)\n"
FLAG_MINMETHOD = 2005 --> conjugate Hager Zhang 2005 (p.161) (homemade)\n"
FLAG_MINMETHOD = 77   --> newton (homemade)\n", ::FLAG_MINMETHOD);
*/

  FLAG_WARNING = FLAG_WARNING00 ;     // SHOW WARNING 
/*
#define FLAG_WARNING_INFO_INV         1
#define FLAG_WARNING_INFO_APPROACH    2
#define FLAG_WARNING_STOP_INV         10
#define FLAG_WARNING_DISPABOVEITER    1
*/
  TOLERANCE_JS = TOLERANCE_JS00;  // SENSITIVE_PARAM (1.e-3) // 1.e-4 
  TOLERANCE_0  = TOLERANCE_000;  // SENSITIVE_PARAM (1.e-7)
  TOLERANCE_NR = TOLERANCE_NR00;  // SENSITIVE_PARAM (1.e-7) // 1.e-8 needed for diff with NR,1.e-5  
  MAX_ITER_NR  = MAX_ITER_NR00;    // SENSITIVE_PARAM (200)
  TOLERANCE_OM = TOLERANCE_OM00; // SENSITIVE_PARAM (1.e-11)// 1.e-15 allows to work for square if TOLERANCE_NJ=1.e-3 & DELTA_0=1.e-5 for numjac)
  MAX_ITER_OM  = MAX_ITER_OM00;    // SENSITIVE_PARAM (700)
  FLAG_ANA     = FLAG_ANA00;      // SENSITIVE_PARAM (0='only numerical jacobian')
  TOLERANCE_NJ = TOLERANCE_NJ00;  // SENSITIVE_PARAM (1.e-5 for square;
                        //                  1.e-3 for VinchT.pro & transfo.pro)
  DELTA_0      = DELTA_000;  // SENSITIVE_PARAM (1.e-3 for square; 
                        //                  1.e0 for VinchT & transfo)
  FLAG_HOMO    = FLAG_HOMO00;

  If(FLAG_ANHYLAW==1)
  // Material M25050A
    Ja   = 1.22;
    ha   = 65;
    Jb   = 0.;
    hb   = 0.;   
  EndIf
  If(FLAG_ANHYLAW==2)
/* 
    // Material M23535A (default for square) #CHECK here
    Ja   = 0.595;
    ha   = 4100;
    Jb   = 1.375;
    hb   = 17.5;
/*/
/*    //team32_RD_z_anhyshift (abstract ISEM 2017)
    Ja   = 1.03838;  
    ha   = 16.9403; 
    Jb   = 0.600978; 
    hb   = 240.578; 
//*/
/*    //Double Langevin for team32_RD_z_anhyFab (new try)
    Ja=1.03643 ;
    ha=14.0374 ;
    Jb=0.549518 ; 
    hb=188.516; 
//*/
///*   //FH(xini=400) // NEW 1/9/2017 (default for t32) #CHECK here
    Ja   = 0.792234;  
    ha   = 9.082095; 
    Jb   = 0.790993; 
    hb   = 137.121351; 
//*/
/* //TD
Ja   = 1.08427393613
ha   = 29.1025536684
Jb   = 0.572282990517
hb   = 399.474553851
*/
  EndIf

  If(N==1)
    kappa_1 = 0.;
    w_1   = 1.;

  ElseIf (N==3)
/*
  // Material M25050A (default for square) #CHECK here
    Js_1  = 0.11;
    Js_2  = 0.8;
    Js_3  = 0.31;
    kappa_1 = 0;
    kappa_2 = 16;
    kappa_3 = 47;
    w_1   = 0.090163934426230;
    w_2   = 0.655737704918033;
    w_3   = 0.254098360655738;
/*/

/*    //team32_RD_z_anhyshift (abstract ISEM 2017)
    kappa_1 = 0*11.6413;
    kappa_2 = 50.1413;
    kappa_3 = 107.518;
    w_1   = 0.173524;
    w_2   = 0.591535;
    w_3   = 0.234941;
//*/ 
/*    //FH(xini=400) // THIS IS PROBLEMATIC (CONVERGENCE PROBLEM ok now) (new try)
    kappa_1 = 0.;
    kappa_2 = 53.779614004;
    kappa_3 = 233.384447699;
    w_1   = 0.1048; //+0.1 to converge
    w_2   = 0.817943263499; // -0.1 to converge
    w_3   = 0.0772371911006;
//*/
/*    //FH(xini=400) // NEW 1/9/2017 (chamonix) (default for t32) #CHECK here
    kappa_1 = 0.;
    kappa_2 = 53.789872;
    kappa_3 = 233.910892;
    w_1   = 0.0280702473932;
    w_2   = 0.896565361832;
    w_3   = 0.0753643907746;
//*/
/*//FH(xini=400) TD // NEW 1/9/2017 (chamonix)
0 0.0565084014449 0.0
1 0.839372763961 65.418004
2 0.104118834594 246.564466
*/
///*   // NEW 8/8/2018  for team32
    kappa_1 = 0.;
    kappa_2 = 54.705531;
    kappa_3 = 199.248421;
    w_1   = 0.028745;
    w_2   = 0.921797;
    w_3   = 0.049458;
//*/
ElseIf (N==5)   //FH(xini=400) // NEW 1/9/2017
    kappa_1 = 0.;
    kappa_2 = 14.81413;
    kappa_3 = 52.317547;
    kappa_4 = 102.345013;
    kappa_5 = 247.012801;
    w_1   = 0.028258057159;
    w_2   = 0.087100091228;
    w_3   = 0.702372713403;
    w_4   = 0.134380357677;
    w_5   = 0.0478887805338;

  /*//FH(xini=400) TD // NEW 1/9/2017
  0 0.0574009326857 0.0
  1 0.111924243476 16.835878
  2 0.576936798861 62.494953
  3 0.189127398303 123.191766
  4 0.0646106266745 259.419756*/
EndIf

  If (N==1)
  param_EnergHyst={ dim, N,  
                    Ja, ha, Jb, hb, 
                    w_1, kappa_1,
                    FLAG_INVMETHOD,
                    FLAG_JACEVAL, 
                    FLAG_APPROACH, 
                    FLAG_MINMETHOD,
                    FLAG_ANHYLAW,
                    FLAG_WARNING,
                    TOLERANCE_JS,
                    TOLERANCE_0,
                    TOLERANCE_NR,
                    MAX_ITER_NR,
                    TOLERANCE_OM,
                    MAX_ITER_OM,
                    FLAG_ANA,
                    TOLERANCE_NJ,
                    DELTA_0,
                    FLAG_HOMO
                   };
  ElseIf (N==3)
  param_EnergHyst={ dim, N,  
                    Ja, ha, Jb, hb, 
                    w_1, kappa_1,
                    w_2, kappa_2,
                    w_3, kappa_3,
                    FLAG_INVMETHOD,
                    FLAG_JACEVAL, 
                    FLAG_APPROACH, 
                    FLAG_MINMETHOD,
                    FLAG_ANHYLAW,
                    FLAG_WARNING,
                    TOLERANCE_JS,
                    TOLERANCE_0,
                    TOLERANCE_NR,
                    MAX_ITER_NR,
                    TOLERANCE_OM,
                    MAX_ITER_OM,
                    FLAG_ANA,
                    TOLERANCE_NJ,
                    DELTA_0,
                    FLAG_HOMO
                   };
  ElseIf (N==5)
  param_EnergHyst={ dim, N,  
                    Ja, ha, Jb, hb, 
                    w_1, kappa_1,
                    w_2, kappa_2,
                    w_3, kappa_3,
                    w_4, kappa_4,
                    w_5, kappa_5,
                    FLAG_INVMETHOD,
                    FLAG_JACEVAL, 
                    FLAG_APPROACH, 
                    FLAG_MINMETHOD,
                    FLAG_ANHYLAW,
                    FLAG_WARNING,
                    TOLERANCE_JS,
                    TOLERANCE_0,
                    TOLERANCE_NR,
                    MAX_ITER_NR,
                    TOLERANCE_OM,
                    MAX_ITER_OM,
                    FLAG_ANA,
                    TOLERANCE_NJ,
                    DELTA_0,
                    FLAG_HOMO
                   };
  Else
    Printf["N=%g is not valid",N];
    Error["The parameters (w_k, kappa_k) for the asked number of cells (N) is not written yet in 'param_EnergHyst.pro'"];
  EndIf
                   
//*******************************************************************
// DISPLAY PARAMETERS SETTING
//*******************************************************************
Label_FLAG_INVMETHOD="...";
Label_FLAG_JACEVAL="...";
Label_FLAG_ANHYLAW="...";
Label_FLAG_APPROACH="...";
Label_FLAG_MINMETHOD="...";
Label_FLAG_ANA="...";
  If (FLAG_INVMETHOD == 1)
  Label_FLAG_INVMETHOD="FLAG_INVMETHOD=1    --> NR_ana (homemade)";
  EndIf
  If (FLAG_INVMETHOD == 2)
  Label_FLAG_INVMETHOD="FLAG_INVMETHOD=2    --> NR_num (homemade)";
  EndIf
  If (FLAG_INVMETHOD == 3)
  Label_FLAG_INVMETHOD="FLAG_INVMETHOD=3    --> bfgs (homemade)";
  EndIf
  If(FLAG_JACEVAL == 1)
  Label_FLAG_JACEVAL="FLAG_JACEVAL=1      --> analytical Jacobian";
  EndIf
  If(FLAG_JACEVAL == 2)
  Label_FLAG_JACEVAL="FLAG_JACEVAL=2      --> numerical Jacobian";
  EndIf
  If (FLAG_ANHYLAW == 1)
  Label_FLAG_ANHYLAW="FLAG_ANHYLAW=1     --> tangent hyperbolic";
  EndIf
  If (FLAG_ANHYLAW == 2)
  Label_FLAG_ANHYLAW="FLAG_ANHYLAW=2     --> double langevin function";
  EndIf
  If (FLAG_APPROACH == 1)
  Label_FLAG_APPROACH="FLAG_APPROACH=1     --> variational approach (Jk)";
  EndIf
  If (FLAG_APPROACH == 2)
  Label_FLAG_APPROACH="FLAG_APPROACH=2     --> vector play model approach (hrk)";
  Label_FLAG_MINMETHOD ="...";
  EndIf
  If (FLAG_APPROACH == 3)
  Label_FLAG_APPROACH="FLAG_APPROACH=3     --> full differential approach (hrk)";
  Label_FLAG_MINMETHOD ="...";
  EndIf
  If (FLAG_MINMETHOD == 1 && FLAG_APPROACH==1)
  Label_FLAG_MINMETHOD ="FLAG_MINMETHOD=1     --> steepest descent (homemade)";
  EndIf
  If (FLAG_MINMETHOD == 11 && FLAG_APPROACH==1)
  Label_FLAG_MINMETHOD ="FLAG_MINMETHOD=11    --> NEW steepest descent naive (homemade)";
  EndIf
  If (FLAG_MINMETHOD == 22 && FLAG_APPROACH==1)
  Label_FLAG_MINMETHOD ="FLAG_MINMETHOD=11    --> NEW conjugate Fletcher-Reeves (homemade)";
  EndIf
  If (FLAG_MINMETHOD == 33 && FLAG_APPROACH==1)
  Label_FLAG_MINMETHOD ="FLAG_MINMETHOD=11    --> NEW conjugate Polak-Ribiere (homemade)";
  EndIf
  If (FLAG_MINMETHOD == 333 && FLAG_APPROACH==1)
  Label_FLAG_MINMETHOD ="FLAG_MINMETHOD=11    --> NEW conjugate Polak-Ribiere+ (homemade)";
  EndIf
  If (FLAG_MINMETHOD == 1999 && FLAG_APPROACH==1)
  Label_FLAG_MINMETHOD ="FLAG_MINMETHOD=11    --> NEW conjugate Dai Yuan 1999 (p.85) (homemade)";
  EndIf
  If (FLAG_MINMETHOD == 2005 && FLAG_APPROACH==1)
  Label_FLAG_MINMETHOD ="FLAG_MINMETHOD=11    --> NEW conjugate Hager Zhang 2005 (p.161) (homemade)";
  EndIf
  If (FLAG_MINMETHOD == 77 && FLAG_APPROACH==1)
  Label_FLAG_MINMETHOD ="FLAG_MINMETHOD=11    --> NEW newton (homemade)";
  EndIf
  If (FLAG_MINMETHOD == 2 && FLAG_APPROACH==1)
  Label_FLAG_MINMETHOD ="FLAG_MINMETHOD=2     --> conjugate fr (gsl)";
  EndIf
  If (FLAG_MINMETHOD == 3 && FLAG_APPROACH==1)
  Label_FLAG_MINMETHOD ="FLAG_MINMETHOD=3     --> conjugate pr (gsl)";
  EndIf
  If (FLAG_MINMETHOD == 4 && FLAG_APPROACH==1)
  Label_FLAG_MINMETHOD ="FLAG_MINMETHOD=4     --> bfgs2 (gsl)";
  EndIf
  If (FLAG_MINMETHOD == 5 && FLAG_APPROACH==1)
  Label_FLAG_MINMETHOD ="FLAG_MINMETHOD=5     --> bfgs (gsl)";
  EndIf
  If (FLAG_MINMETHOD == 6 && FLAG_APPROACH==1)
  Label_FLAG_MINMETHOD ="FLAG_MINMETHOD=6     --> steepest descent (gsl)";
  EndIf
  If (FLAG_ANA ==0)
  Label_FLAG_ANA ="FLAG_ANA=0           --> dJkdh computed numerically";
  EndIf
  If (FLAG_ANA ==1)
  Label_FLAG_ANA ="...";
  EndIf

  Printf["------------------------------------------------------------"];
  Printf["Dimension            --> %g",dim];
  Printf["Number of cells      --> %g",N];
  Printf["k   omega    kappa    "];
  For iSub In {1:N}
    Printf["%g   %g   %g   ",iSub,w~{iSub},kappa~{iSub}];
  EndFor
  Printf[""];
  Printf[StrCat[Label_FLAG_ANHYLAW]];
  Printf["Ja   ha   Jb   hb"];
  Printf["%g   %g   %g   %g",Ja,ha,Jb,hb];
  Printf[""];
  Printf[StrCat[Label_FLAG_APPROACH]];
  Printf[StrCat[Label_FLAG_INVMETHOD]];
  Printf[StrCat[Label_FLAG_JACEVAL]];
  Printf[StrCat[Label_FLAG_MINMETHOD]];
  Printf[StrCat[Label_FLAG_ANA]];
  Printf["FLAG_HOMO            --> %g",FLAG_HOMO];
  Printf[""];
  Printf["TOLERANCE_JS         --> %g",TOLERANCE_JS];
  Printf["TOLERANCE_0          --> %g",TOLERANCE_0];
  Printf[""];
  Printf["TOLERANCE_NR         --> %g",TOLERANCE_NR];
  Printf["MAX_ITER_NR          --> %g",MAX_ITER_NR];
  If(FLAG_APPROACH==1)
  Printf["TOLERANCE_NJ         --> %g",TOLERANCE_NJ];
  Printf["DELTA_0              --> %g",DELTA_0];
  Printf[""];
  Printf["TOLERANCE_OM         --> %g",TOLERANCE_OM];
  Printf["MAX_ITER_OM          --> %g",MAX_ITER_OM];
  EndIf
  Printf["------------------------------------------------------------"];

}