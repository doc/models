import numpy as np
import sys

def load_getdp_integral(filename):
    return np.loadtxt(filename)[1]+1j*np.loadtxt(filename)[2]

n_max = int(sys.argv[1])
p_max = n_max*n_max+2*n_max

Tmatrix = np.zeros((2*p_max,2*p_max),dtype=complex)

for k1 in range(2*p_max):
    for k2 in range(2*p_max):
        if k1<p_max and k2<p_max: 
            Tmatrix[k1,k2] = load_getdp_integral('resT/fhM_pe%gpo%g.dat'%(k1+1,k2+1))
        if k1<p_max and k2>=p_max:
            Tmatrix[k1,k2] = load_getdp_integral('resT/feM_pe%gpo%g.dat'%(k1+1,k2+1-p_max))
        if k1>=p_max and k2<p_max:
            Tmatrix[k1,k2] = load_getdp_integral('resT/fhN_pe%gpo%g.dat'%(k1+1-p_max,k2+1))
        if k1>=p_max and k2>=p_max:
            Tmatrix[k1,k2] = load_getdp_integral('resT/feN_pe%gpo%g.dat'%(k1+1-p_max,k2+1-p_max))

np.save('Tmatrix.npy',Tmatrix)
