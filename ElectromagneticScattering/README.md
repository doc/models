ONELAB models for 3D scattering problems in nanophotonics.

## Quick Install
Open `scattererTmatrix.pro` with e.g. your precompiled version of [ONELAB](https://onelab.info) to compute the T-matrix of an isolated scatterer of arbitrary shape as described in [1].

## Synopsis
This project contains [ONELAB](https://onelab.info) models for solving various 3D electromagnetic problems on an isolated arbitrary object:
* T-matrix computation [1]
* Quasi-normal modes
* Plane wave response
* Green's function (e.g. to compute the Local Density Of States)

It contains various usefull features in electromag:
* Vector partial waves (AKA vector spherical harmonics)
* Total field formuation with a point source illumination ("an oriented delta")
* Linear eigenvalue problems
* Scattered field formulation
* Spherical PMLs


## Installation
The model `scattererTmatrix.pro` runs with precompiled version of ONELAB.

The model `scattering.pro` requires GetDP with python support (see install notes below):
* [gmsh](https://gmsh.info)
* [getdp](https://getdp.info) compiled with python support (see below)
* python (>3.5.x) with numpy, scipy and matplotlib

## Running the model

Open `scattererTmatrix.pro` or `scattering.pro` with Gmsh.

The model `scattererTmatrix.pro` computes the scattering (T-)matrix solely and does not require any Python support from GetDP. It is quite fast and accurate since it :
* features spherical PML
* features curved elements + high order Webb elements
* re-uses the factorization for each new input harmonic (i.e. one single inversion to get the T-matrix)
* computes the coefficients by integration within GetDP 

The default parameters in `scattering.pro` are also set to compute the T-matrix of a sphere.

Both models retrieve the results from [1].

## Authors
Guillaume Demésy and Brian Stout

## References
[1] See "[Scattering matrix of arbitrarily shaped objects: Combining Finite Elements and Vector Partial Waves](https://arxiv.org/abs/1802.00596)" for details about T-matrices.

## Installation notes
To enable python support (Python[] function) in GetDP, follow [these instructions (with complex arithmetic)](https://gitlab.onelab.info/getdp/getdp/wikis/GetDP-compilation) and add to the final cmake line:

`-DENABLE_PYTHON=ON -DPYTHON_LIBRARY=/path/to/pythonlib -DPYTHON_INCLUDE_DIR=/path/to/pythoninclude`
*  On Debian/Ubuntu systems, for python3.6 installed with apt-get,
    * `/path/to/pythonlib` is `/usr/lib/x86_64-linux-gnu/libpython3.6m.so`
    * `/path/to/python/include` is `/usr/include/python3.6m`
* For python versions installed through anaconda in some environment (e.g. env py36 below), a common location is:
    * `/somepath/anaconda3/envs/py36/lib/libpython3.6m.so`
    * `/somepath/anaconda3/envs/py36/include/python3.6m`
