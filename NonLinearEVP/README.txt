A demo of non-linear eigenvalue problems in ONELAB/Gmsh/GetDP.

This model computes one selected eigenfrequency of a grating made of a
frequency-dispersive material. Five different formulations are given,
calling the polynomial and (rational) non-linear solvers of the SLEPc library
thanks to the unified Eig operator.

Quick start
-----------

1- Download a recent ONELAB version from https://onelab.info/
2- Open `NonLinearEVP.pro' with Gmsh
3- Click `Run' in Gmsh

Files
-----

This model contains three files:
- NonLinearEVP_data.geo: ONELAB data and constants shared by Gmsh and GetDP
- NonLinearEVP.geo: Geometry file
- NonLinearEVP.pro: Finite Element formulations

Documentation
---------------

- Some documentation about the new features for non-linear EVPs is available here:
  https://arxiv.org/abs/1802.02363
- General GetDP documentation: https://getdp.info/doc/texinfo/getdp.html
- General Gmsh  documentation: https://gmsh.info/doc/texinfo/gmsh.html

Additional info
---------------

By default, this model outputs the eigenvalue targeted in the convergence test
of the above reference, with the "Rational Non-Linear Eigenvalue Problem"
(NEP) SLEPc solver.
