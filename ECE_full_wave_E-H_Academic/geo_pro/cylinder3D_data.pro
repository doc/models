/*
Geometry:
a = radius of the cylinder [m]
l = height of the cylinder [m]
==
Material 
epsr = relative permittivity
mur = relative permeability
sigma = conductivity [S/m]
*/
colorro = "LightGrey";

cGUI= "{TEST-Cilinder3D-ProblemData/";
cGUI0 = StrCat[cGUI, "0"];
cGUI1 = StrCat[cGUI, "1"];
cGUI2 = StrCat[cGUI, "2"];
cGUI3 = StrCat[cGUI, "3"];
cGUI4 = StrCat[cGUI, "4"];
close_menu = 1;

/* new menu for the interface - which test */
mTypeTest = StrCat[cGUI0,"Test/0"];
colorMTypeTest = "Orange";
/* new menu for the interface - frequencies */
mValuesBC = StrCat[cGUI1,"Frequency/0"];
colorMValuesBC = "Ivory";
/* new menu for the interface - type of terminal excitation */
mTypeBC = StrCat[cGUI2,"Type of BC/0"];
colorMTypeBC = "Red";
/* new menu for the interface - type of formulation */
mTypeBC2 = StrCat[cGUI3,"Type of formulation/0"];
colorMTypeBC = "Blue";
/* new menu for the interface - type of formulation */
mTypeMesh = StrCat[cGUI4,"MeshSettings/"];
colorMTypeMesh = "Red";

/* Test */
DefineConstant[
  Flag_WhichTest = { 0,
    Choices{
      0="a = 2.5 um, l = 10 um",
      1="a = 4 mm, l = 10 mm"},
    Name Str[mTypeTest], Highlight Str[colorMTypeTest], Closed !close_menu,
	ServerAction Str["Reset", "GetDP/1ResolutionChoices" , ',' , StrCat[mValuesBC, "0Working Frequency"] ]}
	];
	
If (Flag_WhichTest==0)
	/* dimensions used in the SCEE 2020, 2022 and JMI papers */
	a = 2.5e-6;
	l = 10e-6;
	fmin = 1e7;   // Hz
	fmax = 100e9; // Hz
	nop = 10;
    //freqs()=LogSpace[Log10[fmin],Log10[fmax],nop];
    freqs()=LinSpace[fmin,fmax,nop];
Else
	/* a inspired  from Wu coax, but l is short */
	a = 4e-3;
	l = 10e-3;
	fmin = 1;   // Hz
	//fmax = 600e6; // Hz
	fmax = 100e6; // Hz
	//fmax = 1e4; 
	nop = 20;
	freqs()=LogSpace[Log10[fmin],Log10[fmax],nop];
    //freqs()=LinSpace[fmin,fmax,nop];
EndIf

epsr = 1;
mur = 1;
sigma = 6.6e7;

eps0 = 8.854187818e-12; // F/m - absolute permitivity of vacuum
mu0  = 4.e-7 * Pi;      // H/m - absolute permeability of vacuum
nu0  = 1/mu0;           // m/H - absolute reluctivity of vacuum

eps = eps0*epsr;        // F/m - absolute permitivity
mu = mu0*mur;           // F/m - absolute permeability
nu = 1/mu;   	
	
// Frequencies
DefineConstant[
_use_freq_loop = {0,  Choices{0,1}, Name StrCat[mValuesBC, "0Loop on frequencies?"],
    ServerAction Str["Reset", StrCat[mValuesBC, "0Working Frequency"]], Highlight "Yellow"}
  Freq  = {freqs(0), Choices{freqs()}, Loop _use_freq_loop, Name StrCat[mValuesBC, "0Working Frequency"],
    Units "Hz", Highlight  Str[colorMValuesBC], Closed !close_menu}
];

// Excitation type
DefineConstant[
  Flag_AnalysisType = { TERMINAL_EXC,
    Choices{
      0="bottomTerm ev",
      1="bottomTerm ec"},
    Name Str[mTypeBC], Highlight Str[colorMTypeBC], Closed !close_menu,
    ServerAction Str["Reset", "GetDP/1ResolutionChoices"]}
	];


// Which formulation
DefineConstant[
  Flag_AnalysisTypeFormulation = { FORMULATION,
    Choices{
      0="0: formulation in E inside + ECE",
      1="1: formulation in H inside + ECE"
    },
    Name Str[mTypeBC2], Highlight Str[colorMTypeBC], Closed !close_menu,
    Help Str["E inside, electric V on the boundary",
      "H inside, reduced magnetic V on the boundary"],
    ServerAction Str["Reset", "GetDP/1ResolutionChoices"]}
];


meshSettings = StrCat[cGUI,"5MeshSettings/0"];

DefineConstant[
   s = {1, Name StrCat[meshSettings, "1Mesh factor/1Mesh factor for Oz (1 means 10 nodes on Oz)"]}
   nbDelta = {1, Name StrCat[meshSettings, "1Mesh factor/2Nb of elems per skin depth (lcar is delta over nbDelta)"]}
   Flag_ElementOrder = { FE_ORDER,
    Choices{
      1="1: first order elements",
      2="2: second order elements"
    },
    Name StrCat[meshSettings, "2Element order"], Highlight "Red", Closed !close_menu,
    ServerAction Str["Reset", "GetDP/1ResolutionChoices"]}
	_use_recombine = {0, Choices{0,1}, Name StrCat[meshSettings, "3Use recombine?"], Highlight "Yellow", Closed !close_menu}
];

FE_Order = Flag_ElementOrder;
modelDim = 3; 
Flag_Axi = 0; // 1 for AXI - it makes sense only for modelDim = 2

h2Ddepth = (modelDim==3) ? 1 : (Flag_Axi ? 2*Pi : l);

DefineConstant[
  // Flag_AnalysisType
  // ==0 bottom ev
  // ==1 bottom ec

  VGroundRMS  = {0, Name StrCat[mValuesBC, "2Top/1Potential on bottom (rms)"], Units "V" , ReadOnly 1, Highlight Str[colorro],
    Visible (Flag_AnalysisType==0) || (Flag_AnalysisType==1)}
  VGroundPhase  = {0, Name StrCat[mValuesBC, "2Top/1Potential on bottom, phase"], Units "rad",  ReadOnly 1, Highlight Str[colorro],
    Visible (Flag_AnalysisType==0) || (Flag_AnalysisType==1)}
	
  VTermRMS  = {1, Name StrCat[mValuesBC, "1Bottom/1Potential on Term1 (rms)"], Units "V" , ReadOnly 0, Highlight Str[colorMValuesBC],
    Visible (Flag_AnalysisType==0)}
  VTermPhase  = {0, Name StrCat[mValuesBC, "1Bottom/1Potential on Term1, phase"], Units "rad",  ReadOnly 0, Highlight Str[colorMValuesBC],
    Visible (Flag_AnalysisType==0)}
	
  ITermRMS  = {1, Name StrCat[mValuesBC, "1Bottom/1Current through Term1 (enters the domain) (rms)"], Units "A" , ReadOnly 0, Highlight Str[colorMValuesBC],
    Visible (Flag_AnalysisType==1) }
  ITermPhase  = {0, Name StrCat[mValuesBC, "1Bottom/Current through Term1 (enters the domain), phase"], Units "rad",  ReadOnly 0, Highlight Str[colorMValuesBC],
    Visible (Flag_AnalysisType==1) } 		
];


//=============================
// Indexes of physical entities
// Surfaces
MATERIALX = 100;

// boundaries
GROUND   = 120 ;
TERMINAL = 121 ;
BOUNDARYNOTTERMINAL = 131 ;

BOUNDARY  = 331; 
