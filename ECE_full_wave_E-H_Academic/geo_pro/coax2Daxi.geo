Include "coax2Daxi_data.pro";

// Definition of parameters for local mesh dimensions
p0 = s*l/10; // characteristic length of mesh element

// Definition of gemetrical points
Point(1) = { 0, 0, 0, p0} ;
Point(2) = { a, 0, 0, p0} ;
Point(3) = { a, l, 0, p0} ;
Point(4) = { 0, l, 0, p0} ;
Point(5) = { b, 0, 0, p0} ;
Point(6) = { b, l, 0, p0} ;

// Definition of gemetrical lines
Line(1) = {1,2};
Line(2) = {2,3};
Line(3) = {3,4};
Line(4) = {4,1};
Line(5) = {2,5};
Line(6) = {5,6};
Line(7) = {3,6};

// Definition of geometrical surfaces
Line Loop(5) = {1, 2, 3, 4};
Plane Surface(6) = {5};
Line Loop(7) = {5, 6, -7, -2};
Plane Surface(8) = {7};

If(_use_transfinite)
  Transfinite Line {2,4,6} = 9*s; // 8  discretizations
  Transfinite Line {1,3,5,7} = 4*s; 
  Transfinite Surface {6,8};
  If (_use_recombine)
    Recombine Surface{6,8};
  EndIf
EndIf

/* Definition of Physical entities (surfaces, lines). The Physical
   entities tell GMSH the elements and their associated region numbers
   to save in the file 'Ishape2d.msh'. */

Physical Surface ("Conductor", 100) = {6} ;   
Physical Surface ("Air", 101) = {8} ;    

Physical Line ("Terminal1", 121) = {1} ;          
Physical Line ("Terminal2", 122) = {3} ;          
Physical Line ("Ground",   120) = {6} ;          

Physical Line ("LeftBoundary", 132) = {4} ;      
Physical Line ("BottomBoundary", 133) = {5} ; 
Physical Line ("TopBoundary", 134) = {7} ; 

// For aestetics
Recursive Color SkyBlue { Surface{8};}
Recursive Color Red  { Surface{6};}