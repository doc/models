Include "coax2Daxi_data.pro";

Group{
  Conductor = Region[100]; 
  Air = Region[101];
  
  Ground = Region[120];    /* Boundary - various parts */
  Terminal1 = Region[121];
  Terminal2 = Region[122];
  

  BottomBoundary = Region[133];
  TopBoundary = Region[134];
  BoundaryNotTerminal = Region[{BottomBoundary,TopBoundary}];

  If(Flag_Axi == 0) // 2D
    LeftBoundary = Region[132];
    Axis = Region[{}];
    BoundaryNotTerminal += Region[{LeftBoundary}];
  Else // axi
    LeftBoundary = Region[{}];
    Axis = Region[132];
  EndIf
  
  Cut1Hterminal1 = Region[{BottomBoundary}];
  Cut1Hterminal2 = Region[{TopBoundary}];
  
  Cut1H = Region[{Cut1Hterminal1,Cut1Hterminal2}];
  
  Sur_Terminals_FWece = Region[{Ground, Terminal1, Terminal2}]; // all terminals
  
  Vol_FW = Region[{Conductor,Air}]; // domain without the boundary
  Sur_FW = Region[{Sur_Terminals_FWece, TopBoundary, BottomBoundary, LeftBoundary, Axis}];
  Dom_FW = Region[ {Vol_FW, Sur_FW} ];

  If (sigmaExt == 0) // used only with H formulation
    Vol_FW_SigmaZero = Region[{Air}];
    Vol_FW_SigmaNonZero = Region[{Conductor}];
  Else
    Vol_FW_SigmaZero = Region[{}];
    Vol_FW_SigmaNonZero = Region[{Air,Conductor}];
  EndIf
  
}

Function{
   /* Material properties are defined piecewise, for elementary groups */
   /* the values in the right hand side are defined in Ishape2d_data.pro */
   
   nu[#{Conductor,Terminal1,Terminal2,Axis}] = nuInt;  // magnetic reluctivity [m/H]
   mu[#{Conductor,Terminal1,Terminal2,Axis}] = muInt;  // permeability [H/m]
   epsilon[#{Conductor,Terminal1,Terminal2,Axis}] = epsInt;       // permittivity [F/m]
   sigma[#{Conductor,Terminal1,Terminal2,Axis}]  = sigmaInt;      // conductivity [S/m]
   
   nu[#{Air,BoundaryNotTerminal}] = nuExt;  // magnetic reluctivity [m/H]
   mu[#{Air,BoundaryNotTerminal}] = muExt;  // permeability [H/m]
   epsilon[#{Air,BoundaryNotTerminal}] = epsExt;       // permittivity [F/m]
   sigma[#{Air,BoundaryNotTerminal}]  = sigmaExt;      // conductivity [S/m]
   
   
   // This is a problem with 2 terminals, out of which one is grounded.
   // The non-grounded can be excited in voltage or in current.
   // Depending on the excitation type, one of the following functions will be useful.
   //
   // When imposing voltage or current via a constraint, the region appears there
   // If you have a complex, the value has to be a function => use square brackets []
   Printf(" ************************ Flag_AnalysisTypeFormulation = %g",Flag_AnalysisTypeFormulation);
   Printf(" ************************ Flag_AnalysisType = %g",Flag_AnalysisType);
   If (Flag_AnalysisTypeFormulation==0) // E
	   If((Flag_AnalysisType==0)||(Flag_AnalysisType==1))
			VTerminal1[] = -VTerm1RMS*Complex[Cos[VTerm1Phase],Sin[VTerm1Phase]];
	   Else
			ITerminal1[] = -ITerm1RMS*Complex[Cos[ITerm1Phase],Sin[ITerm1Phase]];
	   EndIf
	   If((Flag_AnalysisType==0)||(Flag_AnalysisType==2))
			VTerminal2[] = -VTerm2RMS*Complex[Cos[VTerm2Phase],Sin[VTerm2Phase]];
	   Else
			ITerminal2[] = -ITerm2RMS*Complex[Cos[ITerm2Phase],Sin[ITerm2Phase]];
	   EndIf
	Else // H
	   If((Flag_AnalysisType==0)||(Flag_AnalysisType==1))
			VTerminal1[] = VTerm1RMS*Complex[Cos[VTerm1Phase],Sin[VTerm1Phase]];
	   Else
			ITerminal1[] = ITerm1RMS*Complex[Cos[ITerm1Phase],Sin[ITerm1Phase]];
	   EndIf
	   If((Flag_AnalysisType==0)||(Flag_AnalysisType==2))
			VTerminal2[] = VTerm2RMS*Complex[Cos[VTerm2Phase],Sin[VTerm2Phase]];
	   Else
			ITerminal2[] = ITerm2RMS*Complex[Cos[ITerm2Phase],Sin[ITerm2Phase]];
	   EndIf
	EndIf
   
 }


 Constraint{

   // ece BC
   { Name SetTerminalPotential; Type Assign;  // voltage excited terminals
     Case {
       { Region Ground; Value 0.; }
       If((Flag_AnalysisType==0)||(Flag_AnalysisType==1))
         { Region Terminal1; Value VTerminal1[]; }
       EndIf
	   If((Flag_AnalysisType==0)||(Flag_AnalysisType==2))
         { Region Terminal2; Value VTerminal2[]; }
       EndIf
     }
   }
   { Name SetTerminalCurrent; Type Assign;   // current excited terminals
     Case {
       If((Flag_AnalysisType==2)|| (Flag_AnalysisType==3))
         { Region Terminal1; Value ITerminal1[]/h2Ddepth; }  // here the depth is needed
       EndIf
	   If((Flag_AnalysisType==1)|| (Flag_AnalysisType==3))
         { Region Terminal2; Value ITerminal2[]/h2Ddepth; }  // here the depth is needed
       EndIf
     }
   }
   
   { Name ImposeE ;
     Case {
     }
   }
   
   
      { Name SetTerminalCurrentH; Type Assign;   // current excited terminals
     Case {
       If((Flag_AnalysisType==2)|| (Flag_AnalysisType==3))
         { Region Cut1Hterminal1; Value ITerminal1[]/h2Ddepth; }  // here the depth is needed
       EndIf
	   If((Flag_AnalysisType==1)|| (Flag_AnalysisType==3))
         { Region Cut1Hterminal2; Value ITerminal2[]/h2Ddepth; }  // here the depth is needed
       EndIf
     }
   }
   
   
  
  { Name SetTerminalPotentialH; Type Assign;  // voltage excited terminals
     Case {
       If((Flag_AnalysisType==0)||(Flag_AnalysisType==1))
         { Region Cut1Hterminal1; Value VTerminal1[]; }
       EndIf
	   If((Flag_AnalysisType==0)||(Flag_AnalysisType==2))
         { Region Cut1Hterminal2; Value VTerminal2[]; }
       EndIf
     }
   }
  
  
   { Name ImposeHonAxis ;
     Case {
       If(Flag_AnalysisTypeFormulation==1) // H
   	    If(Flag_Axi == 1) // axi
            { Region Axis; Value 0 ; }
   		EndIf
        EndIf
    }
   }
  
}

modelDir = "../";
Dir = "res/";

// The formulation and its tools

 If (Flag_AnalysisTypeFormulation==0)  // E inside
    Include "../formulations/FullWave_E_ece_MIMO2terminals_secondOrder.pro"
 Else // H inside
    Include "../formulations/FullWave_H_ece_MIMO2terminals_cplx_2D_and_AXI_secondOrder.pro"
 EndIf
 
Macro Change_post_options
Echo[Str[ "nv = PostProcessing.NbViews;",
    "For v In {0:nv-1}",
    "View[v].NbIso = 25;",
    "View[v].RangeType = 3;" ,// per timestep
    "View[v].ShowTime = 3;",
    "View[v].IntervalsType = 3;",
    "EndFor"
  ], File "post.opt"];
Return

PostOperation {
  If (Flag_AnalysisTypeFormulation==0)   // E

    { Name Maps; NameOfPostProcessing FW_E_ece;
      Operation {
      Print [ gradV, OnElementsOf ElementsOf[Vol_FW,OnOneSideOf Sur_FW], File StrCat[Dir,"map_gradV.pos" ]];
      Print [ E, OnElementsOf Vol_FW, File StrCat[Dir,"map_E.pos" ]];
      Print [ J, OnElementsOf Vol_FW, File StrCat[Dir,"map_J.pos" ]];
      Print [ H, OnElementsOf Vol_FW, File StrCat[Dir,"map_H.pos" ]];
      Print [ Vterminal, OnElementsOf ElementsOf[Sur_FW], File StrCat[Dir,"map_Vterminals.pos" ]];
      Print [ VnotTerminals, OnElementsOf Vol_FW, File StrCat[Dir,"map_VnotTerminals.pos" ]];
      Call Change_post_options;
     }
    }     

    { Name TransferMatrix; NameOfPostProcessing FW_E_ece ;
      Operation {
        If(Flag_AnalysisType==3)  // current excitation
          Print [ Vterminal, OnRegion Terminal1, Format Table , File  > StrCat[Dir,"test_Z1_RI_formE.txt"]] ;
		      Print [ Vterminal, OnRegion Terminal2, Format Table , File  > StrCat[Dir,"test_Z2_RI_formE.txt"]] ;  
        ElseIf(Flag_AnalysisType==0)  // voltage excitation
		      Print [ Iterminal, OnRegion Terminal1, Format Table , File  > StrCat[Dir,"test_Y1_RI_formE.txt"]] ;
		      Print [ Iterminal, OnRegion Terminal2, Format Table , File  > StrCat[Dir,"test_Y2_RI_formE.txt"]] ;  
        ElseIf(Flag_AnalysisType==2)  // hybrid excitation (first ec, second ev)
          Print [ Vterminal, OnRegion Terminal1, Format Table , File  > StrCat[Dir,"test_H1_RI_formE.txt"]] ;
		      Print [ Iterminal, OnRegion Terminal2, Format Table , File  > StrCat[Dir,"test_H2_RI_formE.txt"]] ;  
	      Else // reverse hybrid excitation (first ev, second ec)
          Print [ Iterminal, OnRegion Terminal1, Format Table , File  > StrCat[Dir,"test_G1_RI_formE.txt"]];
		      Print [ Vterminal, OnRegion Terminal2, Format Table , File  > StrCat[Dir,"test_G2_RI_formE.txt"]] ;  
	      EndIf
      } 
    }
  
  Else // H

    { Name Maps; NameOfPostProcessing FW_H_ece ;
      Operation {
      Print [ E, OnElementsOf Vol_FW, File StrCat[Dir,"map_E.pos" ]];
      Print [ J, OnElementsOf Vol_FW, File StrCat[Dir,"map_J.pos" ]];
      Print [ rmsJ, OnElementsOf Vol_FW, File StrCat[Dir,"map_absJ.pos" ]];
      // Print [ B, OnElementsOf Vol_FW, File StrCat[Dir,"map_B.pos" ]];
      Print [ H, OnElementsOf Vol_FW, File StrCat[Dir,"map_H.pos" ]];
      Print [ rmsH, OnElementsOf Vol_FW, File StrCat[Dir,"map_absH.pos" ]];
      Call Change_post_options;
      }
    }

    { Name TransferMatrix; NameOfPostProcessing FW_H_ece ;
      Operation {
        If(Flag_AnalysisType==3)  // current excitation
          Print [ Vterminal, OnRegion Cut1Hterminal1, Format Table , File  > StrCat[Dir,"test_Z1_RI_formH.txt"]] ;
          Print [ Vterminal, OnRegion Cut1Hterminal2, Format Table , File  > StrCat[Dir,"test_Z2_RI_formH.txt"]] ;   
        ElseIf(Flag_AnalysisType==0)  // voltage excitation
          Print [ Iterminal, OnRegion Cut1Hterminal1, Format Table , File  > StrCat[Dir,"test_Y1_RI_formH.txt"]] ;
          Print [ Iterminal, OnRegion Cut1Hterminal2, Format Table , File  > StrCat[Dir,"test_Y2_RI_formH.txt"]] ;  
        ElseIf(Flag_AnalysisType==2)  // hybrid excitation (first ec, second ev)
          Print [ Vterminal, OnRegion Cut1Hterminal1, Format Table , File  > StrCat[Dir,"test_H1_RI_formH.txt"]] ;
          Print [ Iterminal, OnRegion Cut1Hterminal2, Format Table , File  > StrCat[Dir,"test_H2_RI_formH.txt"]] ;  
        Else // reverse hybrid excitation (first ev, second ec)
          Print [ Iterminal, OnRegion Cut1Hterminal1, Format Table , File  > StrCat[Dir,"test_G1_RI_formH.txt"]] ;
          Print [ Vterminal, OnRegion Cut1Hterminal2, Format Table , File  > StrCat[Dir,"test_G2_RI_formH.txt"]] ;  
        EndIf
      }  
    }
 
  EndIf

}
