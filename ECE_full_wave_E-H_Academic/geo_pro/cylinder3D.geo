Include "cylinder3D_data.pro";

delta = Sqrt(2.0/(2*Pi*Freq*mu*sigma));
Printf("Freq = %e",Freq);
If (delta < a)
	Printf("delta less than a");
	lcar = delta/nbDelta;
Else
	Printf("delta GREATER than a");
	lcar = l/10;
EndIf


lcarCenter = a/3;

p0 = newp; Point(p0) = {0,0,0,lcarCenter};
p1 = newp; Point(p1) = {a,0,0,lcar};
p2 = newp; Point(p2) = {0,a,0,lcar};
p3 = newp; Point(p3) = {-a,0,0,lcar};
p4 = newp; Point(p4) = {0,-a,0,lcar};

p0l = newp; Point(p0l) = {0,0,l,lcarCenter};
p1l = newp; Point(p1l) = {a,0,l,lcar};
p2l = newp; Point(p2l) = {0,a,l,lcar};
p3l = newp; Point(p3l) = {-a,0,l,lcar};
p4l = newp; Point(p4l) = {0,-a,l,lcar};


c1 = newc; Circle(c1) = {p1,p0,p2};
c2 = newc; Circle(c2) = {p2,p0,p3};
c3 = newc; Circle(c3) = {p3,p0,p4};
c4 = newc; Circle(c4) = {p4,p0,p1};

c1l = newc; Circle(c1l) = {p1l,p0l,p2l};
c2l = newc; Circle(c2l) = {p2l,p0l,p3l};
c3l = newc; Circle(c3l) = {p3l,p0l,p4l};
c4l = newc; Circle(c4l) = {p4l,p0l,p1l};

l1 = newl; Line(l1) = {p1, p1l};
l2 = newl; Line(l2) = {p2, p2l};
l3 = newl; Line(l3) = {p3, p3l};
l4 = newl; Line(l4) = {p4, p4l};

cL = newll;   Curve Loop(cL) = {c1,c2,c3,c4};      scL = news;  Surface(scL) = {cL};
cLl = newll;  Curve Loop(cLl) = {c1l,c2l,c3l,c4l}; scLl = news; Surface(scLl) = {cLl};
cL12 = newll; Curve Loop(cL12) = {c1,l2,-c1l,-l1}; scL12 = news; Surface(scL12) = {cL12};
cL23 = newll; Curve Loop(cL23) = {c2,l3,-c2l,-l2}; scL23 = news; Surface(scL23) = {cL23};
cL34 = newll; Curve Loop(cL34) = {c3,l4,-c3l,-l3}; scL34 = news; Surface(scL34) = {cL34};
cL41 = newll; Curve Loop(cL41) = {c4,l1,-c4l,-l4}; scL41 = news; Surface(scL41) = {cL41};

closedS = newsl; Surface Loop(closedS) = {scL,scLl,scL12,scL23,scL34,scL41};
volDom = newv; Volume(volDom) = {closedS};

Transfinite Line{l1} = 10*s;
Transfinite Line{l2} = 10*s;
Transfinite Line{l3} = 10*s;
Transfinite Line{l4} = 10*s;
Transfinite Surface {scL12};
Transfinite Surface {scL23};
Transfinite Surface {scL34};
Transfinite Surface {scL41};

Field[1] = Distance;
Field[1].SurfacesList = {scL12,scL23,scL34,scL41};
Field[1].Sampling = 50;
Field[2] = Threshold;
Field[2].InField = 1;
Field[2].SizeMin = lcar;
Field[2].SizeMax = a/3;
Field[2].DistMin = 0;
Field[2].DistMax = 3*delta;

Background Field = 2; 
Mesh.MeshSizeExtendFromBoundary = 0;
Mesh.MeshSizeFromPoints = 0;
Mesh.MeshSizeFromCurvature = 0;
Mesh.Algorithm = 5;



Physical Volume ("MaterialX", 100) = {volDom} ;      

Physical Surface ("Ground",   120) = {scLl} ;          
Physical Surface ("Terminal", 121) = {scL} ;          
Physical Surface ("BoundaryNotTerminal", 131) = {scL12,scL23,scL34,scL41} ;  

// cut for the H formulation
Cohomology(1) {{131},{}}; // bnd fara term
CUT1H = 132;