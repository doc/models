/*
Geometry:
a = radius of the cylinder [m]
l = height of the cylinder [m]
==
Material 
epsr = relative permittivity
mur = relative permeability
sigma = conductivity [S/m]
*/

cGUI= "{TEST-Cilinder2Daxi-ProblemData/";
cGUI0 = StrCat[cGUI, "0"];
cGUI1 = StrCat[cGUI, "1"];
cGUI2 = StrCat[cGUI, "2"];
cGUI3 = StrCat[cGUI, "3"];
cGUI4 = StrCat[cGUI, "4"];
close_menu = 1;

/* new menu for the interface - which test */
mTypeTest = StrCat[cGUI0,"Test/0"];
colorMTypeTest = "Orange";
/* new menu for the interface - frequencies */
mValuesBC = StrCat[cGUI1,"Frequency/0"];
colorMValuesBC = "Ivory";
/* new menu for the interface - type of terminal excitation */
mTypeBC = StrCat[cGUI2,"Type of BC/0"];
colorMTypeBC = "Red";
/* new menu for the interface - type of formulation */
mTypeBC2 = StrCat[cGUI3,"Type of formulation/0"];
colorMTypeBC = "Blue";
/* new menu for the interface - type of formulation */
mTypeMesh = StrCat[cGUI4,"MeshSettings/"];
colorMTypeMesh = "Red";

/* Test */
DefineConstant[
  Flag_WhichTest = { 0,
    Choices{
      0="a = 2.5 um, l = 10 um",
      1="a = 4 mm, l = 10 mm"},
    Name Str[mTypeTest], Highlight Str[colorMTypeTest], Closed !close_menu,
	ServerAction Str["Reset", "GetDP/1ResolutionChoices" , ',' , StrCat[mValuesBC, "0Working Frequency"] ]}
	];
	
If (Flag_WhichTest==0)
	// dimensions used in the SCEE 2020 and JMI papers
	a = 2.5e-6;
	l = 10e-6;
	fmin = 1e7;   // Hz
	fmax = 100e9; // Hz
	nop = 20;
  freqs()=LogSpace[Log10[fmin],Log10[fmax],nop];
Else
	// a inspired  from Wu coax, but l is short
	a = 4e-3;
	l = 10e-3;
	fmin = 1;   // Hz
	fmax = 100e6; // Hz
	nop = 20;
	freqs()=LogSpace[Log10[fmin],Log10[fmax],nop];
EndIf
	
epsr = 1;
mur = 1;
sigma = 6.6e7;

eps0 = 8.854187818e-12; // F/m - absolute permitivity of vacuum
mu0  = 4.e-7 * Pi;      // H/m - absolute permeability of vacuum
nu0  = 1/mu0;           // m/H - absolute reluctivity of vacuum

eps = eps0*epsr;        // F/m - absolute permitivity
mu = mu0*mur;           // F/m - absolute permeability
nu = 1/mu;   	
	
/* Frequencies */
DefineConstant[
_use_freq_loop = {0,  Choices{0,1}, Name StrCat[mValuesBC, "0Loop on frequencies?"],
    ServerAction Str["Reset", StrCat[mValuesBC, "0Working Frequency"]], Highlight "Yellow"}
  Freq  = {freqs(0), Choices{freqs()}, Loop _use_freq_loop, Name StrCat[mValuesBC, "0Working Frequency"],
    Units "Hz", Highlight  Str[colorMValuesBC], Closed !close_menu}
];

/* Excitation type */
DefineConstant[
  Flag_AnalysisType = {  TERMINAL_EXC,
    Choices{
      0="bottomTerm ev",
      1="bottomTerm ec"},
    Name Str[mTypeBC], Highlight Str[colorMTypeBC], Closed !close_menu,
    ServerAction Str["Reset", "GetDP/1ResolutionChoices"]}
	];
	
	
	

/* Which formulation */
DefineConstant[
  Flag_AnalysisTypeFormulation = { FORMULATION,
    Choices{
      0="0: formulation in E inside + ECE",
      1="1: formulation in H inside + ECE"
    },
    Name Str[mTypeBC2], Highlight Str[colorMTypeBC], Closed !close_menu,
    Help Str["E inside, electric V on the boundary",
      "H inside, reduced magnetic V on the boundary"],
    ServerAction Str["Reset", "GetDP/1ResolutionChoices"]}
];

s_min = 1;
s_max = 1.414213562373095; 
nops = 7;

s_values()=LinSpace[s_min,s_max,nops];

DefineConstant[
   _use_uniformMesh = {1, Choices{0,1}, Name StrCat[mTypeMesh,"0Uniform mesh?"], Highlight "Pink", Closed !close_menu},
   _use_progression = {0, Choices{0,1}, Name StrCat[mTypeMesh,"1Use geometric progression on the bottom line (only for non-uniform mesh)?"], Highlight "Pink", Closed !close_menu, ReadOnly _use_uniformMesh},
   _use_transfinite = {0, Choices{0,1}, Name StrCat[mTypeMesh,"2One element along Oy ; Transfinite for uniform mesh?"], Highlight "Pink", Closed !close_menu, ReadOnly _use_uniformMesh},
   _use_recombine = {0, Choices{0,1}, Name StrCat[mTypeMesh,"3Use recombine"], Highlight "Pink", Closed !close_menu},
   s = {s_min, Name StrCat[mTypeMesh,"6Mesh factor"]}
   
   FE_Order = { FE_ORDER,
    Choices{
      1="1: first order elements",
      2="2: second order elements"
    },
    Name StrCat[mTypeMesh,"7Element order"], Highlight Str[colorMTypeMesh], Closed !close_menu,
    ServerAction Str["Reset", "GetDP/1ResolutionChoices"]}
];

Printf("a = %g  m",a);
Printf("l = %g  m",l);
Printf("fmin = %g  Hz",fmin);
Printf("fmax = %g  Hz",fmax);
Printf("s = %g ",s);
Printf("FE order = %g ",FE_Order);


modelDim = 2; // 
Flag_Axi = 1; // 1 for AXI - it makes sense only for modelDim = 2

If ((modelDim == 2)&&(Flag_Axi == 0)) // 2D
	h2Ddepth = h;
ElseIf ((modelDim == 2)&&(Flag_Axi == 1))   // 2D AXI // 2.5 D
	h2Ddepth = 2*Pi;
Else // 3D
    h2Ddepth = 1;
EndIf

//=============================
// Indexes of physical entities
// Surfaces
MATERIALX = 100;

// boundaries
GROUND   = 120 ;
TERMINAL = 121 ;
RIGHTBOUNDARY = 131 ;
LEFTBOUNDARY  = 132; // axis

BOUNDARY  = 331; 

