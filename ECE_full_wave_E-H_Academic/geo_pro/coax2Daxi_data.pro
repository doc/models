colorro = "LightGrey";

// new menu for the interface - holds geometrical data
cGUI= "{TEST-Coax2Daxi-ProblemData/";
mGeo = StrCat[cGUI,"0Geometrical parameters/0"];
colorMGeo = "Ivory";
close_menu = 1;
DefineConstant[
  a = {4e-3, Name StrCat[mGeo, "0a=radius of inner conductor"], Units "m", Highlight Str[colorMGeo], Closed close_menu},
  b = {8e-3, Name StrCat[mGeo, "0a=radius of outer conductor"], Units "m", Highlight Str[colorMGeo], Closed close_menu},
  l = {1, Name StrCat[mGeo, "1l=cable length"], Units "m", Highlight Str[colorMGeo]}
];

// new menu for the interface - holds material data
mMat = StrCat[cGUI,"1Material parameters/0"];
colorMMat = "AliceBlue";
DefineConstant[
  epsrInt = {1, Name StrCat[mMat, "0Relative permittivity int part"], Units "-", Highlight Str[colorMMat], Closed close_menu},
  murInt = {1, Name StrCat[mMat, "1Relative permeability int part"], Units "-", Highlight Str[colorMMat]},
  sigmaInt = {6.6e7, Name StrCat[mMat, "2Conductivity int part"], Units "S/m", Highlight Str[colorMMat]},
  epsrExt = {1, Name StrCat[mMat, "3Relative permittivity ext part"], Units "-", Highlight Str[colorMMat], Closed close_menu},
  murExt = {1, Name StrCat[mMat, "4Relative permeability ext part"], Units "-", Highlight Str[colorMMat]},
  sigmaExt = {0, Name StrCat[mMat, "5Conductivity ext part"], Units "S/m", Highlight Str[colorMMat]}
];

eps0 = 8.854187818e-12; // F/m - absolute permitivity of vacuum
mu0  = 4.e-7 * Pi;      // H/m - absolute permeability of vacuum
nu0  = 1/mu0;           // m/H - absolute reluctivity of vacuum

epsInt = eps0*epsrInt;        // F/m - absolute permitivity  - int
muInt = mu0*murInt;           // F/m - absolute permeability - int
nuInt = 1/muInt;              // m/H - absolute reluctivity  - int

epsExt = eps0*epsrExt;        // F/m - absolute permitivity  - ext
muExt = mu0*murExt;           // F/m - absolute permeability - ext
nuExt = 1/muExt;              // m/H - absolute reluctivity  - ext

// new menu for the interface - type of BC
mTypeBC = StrCat[cGUI,"2Type of BC/0"];
colorMTypeBC = "Red";

// new menu for the interface - type of formulation
mTypeBC2 = StrCat[cGUI,"3Type of formulation/0"];
colorMTypeBC = "Blue";
DefineConstant[
  Flag_AnalysisTypeFormulation = { FORMULATION,
    Choices{
      0="0: formulation in E inside + ECE",
      1="1: formulation in H inside + ECE"
    },
    Name Str[mTypeBC2], Highlight Str[colorMTypeBC], Closed !close_menu,
    Help Str["E inside, electric V on the boundary",
      "H inside, reduced magnetic V on the boundary"],
    ServerAction Str["Reset", "GetDP/1ResolutionChoices"]}
];

// new menu for the interface - values of excitations - significance depends on the type of BC
mValuesBC = StrCat[cGUI,"4Values of BC (frequency analysis)/0"];
colorMValuesBC = "Ivory";

If (MODEL_ID2 == 1) // HF simulations
	fmin = 1e7; fmax = 6e8; nop = 100; 
	freqs()=LinSpace[fmin,fmax,nop];
EndIf
If  (MODEL_ID2 == 2) // LF simulations
	fmin = 1;   fmax = 1e7; nop = 20;
	freqs()=LogSpace[Log10[fmin],Log10[fmax],nop];
EndIf

DefineConstant[
  _use_freq_loop = {0,  Choices{0,1}, Name StrCat[mValuesBC, "0Loop on frequencies?"],
    ServerAction Str["Reset", StrCat[mValuesBC, "0Working Frequency"]], Highlight "Yellow"}
  Freq  = {freqs(0), Choices{freqs()}, Loop _use_freq_loop, Name StrCat[mValuesBC, "0Working Frequency"],
    Units "Hz", Highlight  Str[colorMValuesBC], Closed !close_menu }
];

NbTerminals = 2; // No of terminals which are not grounded

Type1 = (TERMINAL_EXC == 0)  ? 2 : 1 ;  // 2 ==> voltage exc
Type2 = (TERMINAL_EXC2 == 0) ? 2 : 1 ; 
	
DefineConstant[
	TypeExcTerminal1 = {Type1,Choices{1="ec",2="ev"},Name StrCat[mValuesBC, "4Exitation type/0Terminal 1 (bottom)"]}
	TypeExcTerminal2 = {Type2,Choices{1="ec",2="ev"},Name StrCat[mValuesBC, "4Exitation type/1Terminal 2 (top)"]}
	ActiveTerminal = {1,Choices{1="1",2="2"},Name StrCat[mValuesBC, "4Exitation type/Active terminal"]}

	VGroundRMS  = {0, Name StrCat[mValuesBC, "3Ground-CylSurf/1Potential on gnd (rms)"], Units "V" , 
		ReadOnly 1, Highlight Str[colorro]}
  	VGroundPhase  = {0, Name StrCat[mValuesBC, "3Ground-CylSurf/1Potential on gnd, phase"], Units "rad",  
		ReadOnly 1, Highlight Str[colorro]}
];

If (ActiveTerminal == 1)
	PassiveTerminal = 2;
Else
	PassiveTerminal = 1;
EndIf
  
If ((TypeExcTerminal1==1)&(TypeExcTerminal2==1))  // current exc
	Flag_AnalysisType = 3;
	Printf("======================> Both terminals are current excited => Z transfer matrix");
	Printf("======================> Active terminal is %g",ActiveTerminal);
	Printf("======================> Two s1p files are created, containing Z%g%g and Z%g%g",ActiveTerminal,ActiveTerminal,PassiveTerminal,ActiveTerminal);
	If (ActiveTerminal == 1)
		ITerm1RMS = 1; ITerm1Phase = 0;
		ITerm2RMS = 0; ITerm2Phase = 0;
		If (Flag_AnalysisTypeFormulation==1)   // H - to understand why I have to do this
		   ITerm1RMS = -1;
		EndIf
	Else
	    ITerm1RMS = 0; ITerm1Phase = 0;
		ITerm2RMS = 1; ITerm2Phase = 0;
	EndIf
ElseIf ((TypeExcTerminal1==2)&(TypeExcTerminal2==2))  // voltage exc
	Flag_AnalysisType = 0;
	Printf("======================> Both terminals are voltage excited => Y transfer matrix");
	Printf("======================> Active terminal is %g",ActiveTerminal);
	Printf("======================> Two s1p files are created, containing Y%g%g and Y%g%g",ActiveTerminal,ActiveTerminal,PassiveTerminal,ActiveTerminal);
	If (ActiveTerminal == 1)
		VTerm1RMS = 1; VTerm1Phase = 0;
		VTerm2RMS = 0; VTerm2Phase = 0;
	Else
	    VTerm1RMS = 0; VTerm1Phase = 0;
		VTerm2RMS = (Flag_AnalysisTypeFormulation==1) ? -1 : 1; 
		// H - constraint is linked to an automatically generated cut, which is oriented and depends on geo/msh
		// the change of sign here is due to the orientation of this cut. It may need to be adapted with another geo/msh
		VTerm2Phase = 0;  
	EndIf
ElseIf ((TypeExcTerminal1==1)&(TypeExcTerminal2==2))  //hybrid H exc
	Flag_AnalysisType = 2;
	Printf("======================> Terminal 1 (top) is current excited, Terminal 2 (right) is voltage excited => H transfer matrix");
	Printf("======================> Active terminal is %g",ActiveTerminal);
	Printf("======================> Two s1p files are created, containing H%g%g and H%g%g",ActiveTerminal,ActiveTerminal,PassiveTerminal,ActiveTerminal);
	If (ActiveTerminal == 1)
		ITerm1RMS = (Flag_AnalysisTypeFormulation==1)?-1:1; // change of sign due to cut orientation
		ITerm1Phase = 0;
		VTerm2RMS = 0; VTerm2Phase = 0;
	Else
	    ITerm1RMS = 0; ITerm1Phase = 0;
		VTerm2RMS = 1; VTerm2Phase = 0;  
	EndIf
Else  //hybrid G exc
	Flag_AnalysisType = 1;
	Printf("======================> Terminal 1 (top) is voltage excited, Terminal 2 (right) is current excited => G transfer matrix");
	Printf("======================> Active terminal is %g",ActiveTerminal);
	Printf("======================> Two s1p files are created, containing G%g%g and G%g%g",ActiveTerminal,ActiveTerminal,PassiveTerminal,ActiveTerminal);
	If (ActiveTerminal == 1)
		VTerm1RMS = 1; VTerm1Phase = 0;
		ITerm2RMS = 0; ITerm2Phase = 0;
	Else
	    VTerm1RMS = 0; VTerm1Phase = 0;
		ITerm2RMS = 1; ITerm2Phase = 0;
	EndIf
EndIf

meshSettings = StrCat[cGUI,"5MeshSettings/0"];

DefineConstant[
  _use_transfinite = {1, Choices{0,1}, Name StrCat[meshSettings, "0Transfinite mesh?"], Highlight "Yellow", Closed !close_menu}
   s = {1, Name  StrCat[meshSettings, "1Mesh factor"]}
   Flag_ElementOrder = { FE_ORDER,
    Choices{
      1="1: first order elements",
      2="2: second order elements"
    },
	Name StrCat[meshSettings, "2Element order"], Highlight "Red", Closed !close_menu,
    ServerAction Str["Reset", "GetDP/1ResolutionChoices"]}
	_use_recombine = {0, Choices{0,1}, Name StrCat[meshSettings, "3Use recombine?"], Highlight "Yellow", Closed !close_menu}
];

FE_Order = Flag_ElementOrder;

Printf("FE_Order = %g",FE_Order);

modelDim = 2; 
Flag_Axi = 1; 
h2Ddepth = (modelDim==3) ? 1 : (Flag_Axi ? 2*Pi : l);

