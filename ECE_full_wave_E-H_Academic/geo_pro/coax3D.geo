Include "coax3D_data.pro";

p =0.25; // portion of a

nop = 4/4;  // no of slices per 1/4 circle
nopR = s*4; //5; //5*2; // no of points on the Oa and atob points
nopA = s*5; //5*4;


pc = newp; Point(pc) = {0,0,0};
angle = Pi/2/nop;

For k1 In {0:4*nop-1}
  alpha = angle*k1;
  pa[] += newp ; Point(pa[k1]) = {a*Cos[alpha],a*Sin[alpha],0};

  // for controlling the mesh a bit better
  pp[] += newp ; Point(pp[k1]) = {p*a*Cos[alpha],p*a*Sin[alpha],0};
  la[] += newl ; Line(la[k1]) = {pp[k1],pa[k1]};

  pb[] += newp ; Point(pb[k1]) = {b*Cos[alpha],b*Sin[alpha],0};
  lb[] += newl ; Line(lb[k1]) = {pa[k1],pb[k1]};
EndFor

For k1 In {0:4*nop-1}
  k2 = (k1==4*nop-1) ? 0 : k1+1;
  cla[] += newl ; Circle(newl) = {pa[k1],pc,pa[k2]};
  clp[] += newl ; Circle(newl) = {pp[k1],pc,pp[k2]}; 
  clb[] += newl ; Circle(newl) = {pb[k1],pc,pb[k2]};

  //In
  Curve Loop(newcl) = {la[k1],cla[k1],-la[k2], -clp[k1]};
  scable_a[] += news; Plane Surface(news) = newcl-1;
  // Out
  Curve Loop(newcl) = {lb[k1],clb[k1],-lb[k2],-cla[k1]};
  scable_b[] += news; Plane Surface(news) = newcl-1;
EndFor
Curve Loop(newcl) = {clp[]};
scable_a[] += news; Plane Surface(news) = newcl-1; // center

Transfinite Curve{la[]} = nopR; 
Transfinite Curve{lb[]} = nopR;

Transfinite Curve{cla[],clb[],clp[]} = nopA;
Transfinite Surface{scable_a[]};
Transfinite Surface{scable_b[]};

If (_use_recombine)
  Recombine Surface{scable_a[]};
  Recombine Surface{scable_b[]};
EndIf

nbPointAlongOz = Ceil[l/lcarLambda];
Printf("nbPointAlongOz = %g",nbPointAlongOz);
//nbPointAlongOz = 16;  // just to see if I can generate second order systems

// You may use a loop, much easier
For k1 In {0:4*nop}
  aux_a[]=Extrude {0,0,l} {
    Surface{scable_a[k1]}; Layers{ {nbPointAlongOz}, {1} }; Recombine _use_recombine;
  }; // array containing: 0=top surface; 1=volume; the rest of the surfaces
  vcable_a[] += aux_a[1]; // volume
  scable_aa[] += aux_a[0]; // top surface after extruding
EndFor

For k1 In {0:4*nop-1}
  aux_b[]=Extrude {0,0,l} {
    Surface{scable_b[k1]}; Layers{ {nbPointAlongOz}, {1} }; Recombine _use_recombine;
  };
  vcable_b[] += aux_b[1]; // volume
  scable_bb[] += aux_b[0]; // top surface after extruding
EndFor

// and no need of hunting :-)
ground[] = Abs[CombinedBoundary{Volume{:};}] ; // Total boundary + getting rid of possible 'signed' surfaces
ground[] -= {scable_a[],scable_aa[], scable_b[], scable_bb[]}; // Removing top and bottom from array



// Physical regions
// --------------------------

Physical Volume ("Conductor", 100) = {vcable_a[]} ;         
Physical Volume ("Air", 101) = {vcable_b[]} ;

Physical Surface ("Ground",   120) = {ground[]} ;          
Physical Surface ("Terminal1", 121) = {scable_a[]} ;       
Physical Surface ("Terminal2", 122) = {scable_aa[]} ;      
Physical Surface ("BottomBoundary", 133) = {scable_b[]} ;
Physical Surface ("TopBoundary", 134) = {scable_bb[]} ;

Physical Surface ("BoundaryNotTerminal", 131) = {scable_b[],scable_bb[]} ;

// cut for the H formulation
Cohomology(1) {{131},{}}; 

// For aestetics
Recursive Color SkyBlue { Volume{vcable_b[]};}
Recursive Color Red  { Volume{vcable_a[]};}
