Include "cylinder2Daxi_data.pro";

// hereafter there are two variants of code, both are correct

delta = Sqrt(2.0/(2*Pi*Freq*mu*sigma));
If (delta < a)
	lcar_p0 = a - delta;
    lcar_p1 = delta;
	If (lcar_p0 < lcar_p1)
		lcar_p1 = lcar_p0;
	    Printf(" ------------------- > lcar_p0 < l_car_p1 at Freg = %g",Freq);
	Else
		Printf(" ==== > lcar_p0 > l_car_p1 at Freg = %g",Freq);
	EndIf
Else
	Printf(" :::::::::::: delta > a at Freg = %g",Freq);
	lcar_p0 = a;
    lcar_p1 = a;
EndIf

lcar_p0 = s*lcar_p0 ;
lcar_p1 = s*lcar_p1 ;

If (_use_uniformMesh==1)
    lcar = s*a/10;

	pp[]+=newp; Point(newp) = {0, 0, 0, lcar};
	pp[]+=newp; Point(newp) = {a, 0, 0, lcar};
	pp[]+=newp; Point(newp) = {a, l, 0, lcar};
	pp[]+=newp; Point(newp) = {0, l, 0, lcar};
	For k In {0:3}
		If (k == 3)
			ll[]+=newl; Line(newl) = {pp[k],pp[0]};
		Else
			ll[]+=newl; Line(newl) = {pp[k],pp[k+1]};
		EndIf
	EndFor

	cl = newll;  Line Loop(cl) = ll[];
	surfRectangle = news; Plane Surface(surfRectangle) = {cl};
	
	If(_use_transfinite)
		nopa = Ceil[a/lcar] ;
		Transfinite Line {ll[0],ll[2]} = nopa; 
		nopl = Ceil[l/lcar] ;
		Transfinite Line {ll[1],ll[3]} = nopl; 
		Transfinite Surface(surfRectangle);
	EndIf

	If (_use_recombine)
		Recombine Surface{surfRectangle};
	EndIf
	
	/* Definition of Physical entities (surfaces, lines). The Physical
       entities tell GMSH the elements and their associated region numbers
       to save in the file 'Ishape2d.msh'. */

	Physical Surface ("MaterialX", MATERIALX) = {surfRectangle};     

	Physical Line ("Ground",   GROUND) = {ll[2]};                 
	Physical Line ("Terminal", TERMINAL) = {ll[0]};               
	Physical Line ("RightBoundary", RIGHTBOUNDARY) = {ll[1]} ;     
	Physical Line ("LeftBoundary", LEFTBOUNDARY) = {ll[3]} ;      

	Physical Line ("Boundary", BOUNDARY) = {ll[]} ; 
	
Else

    // non-uniform mesh
	
	pp[]+=newp; Point(newp) = {0, 0, 0, lcar_p0};
	pp[]+=newp; Point(newp) = {a, 0, 0, lcar_p1};
	pp[]+=newp; Point(newp) = {a, l, 0, lcar_p1};
	pp[]+=newp; Point(newp) = {0, l, 0, lcar_p0};
	For k In {0:3}
		If (k == 3)
			ll[]+=newl; Line(newl) = {pp[k],pp[0]};
		Else
			ll[]+=newl; Line(newl) = {pp[k],pp[k+1]};
		EndIf
	EndFor

	cl = newll;  Line Loop(cl) = ll[];
	surfRectangle = news; Plane Surface(surfRectangle) = {cl};
	
	Printf("================================ _use_progression = %g",_use_progression);
	
	
    If (_use_progression == 1)
		Printf("================================ delta = %g",delta);
		    
		pr1 =  0.5;
		pr2 =  2;
		
		noptest = Log[1-a*(1-pr2)/delta]/Log[pr2]-1;
		noptest = Ceil[noptest];
		Printf("================================ noptest = %g",noptest);
	    If (noptest < 4)
					noptest = 4;
		EndIf
		Printf("================================ noptest2 = %g",noptest);
 		nopp = noptest;
		Transfinite Line {ll[0]}= nopp*s Using Progression pr1; 
		Transfinite Line {ll[2]}= nopp*s Using Progression pr2; 
	EndIf	
		
		
	
	If(_use_transfinite)
		nopl = 2; 
		Transfinite Line {ll[1],ll[3]} = nopl; 
		Transfinite Surface(surfRectangle);
		If (_use_recombine)
			Recombine Surface{surfRectangle};
		EndIf
	EndIf

	/* Definition of Physical entities (surfaces, lines). The Physical
       entities tell GMSH the elements and their associated region numbers
       to save in the file 'Ishape2d.msh'. */

	Physical Surface ("MaterialX", MATERIALX) = {surfRectangle};      

	Physical Line ("Ground",   GROUND) = {ll[2]};                 
	Physical Line ("Terminal", TERMINAL) = {ll[0]};               
	Physical Line ("RightBoundary", RIGHTBOUNDARY) = {ll[1]} ;     
	Physical Line ("LeftBoundary", LEFTBOUNDARY) = {ll[3]} ;       

	Physical Line ("Boundary", BOUNDARY) = {ll[]} ; 
	
EndIf

If(_use_transfinite)
	If (_use_recombine)
		Recursive Color Red { Surface{surfRectangle};}
	Else
		Recursive Color Blue { Surface{surfRectangle};}
	EndIf
Else
	If (_use_recombine)
		Recursive Color Orange { Surface{surfRectangle};}
	Else
		Recursive Color Black { Surface{surfRectangle};}
	EndIf
EndIf
