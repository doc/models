Include "cylinder3D_data.pro";

Group{
   MaterialX = Region[MATERIALX]; /* MaterialX - homogenous domain */
   Ground = Region[GROUND];       /* Boundary - various parts */
   Terminal = Region[TERMINAL];
   
   BoundaryNotTerminal = Region[BOUNDARYNOTTERMINAL];
   Cut1H = Region[132];  // for the H formulation  // this is CUT1H

   Vol_FW = Region[{MaterialX}]; // domain without the boundary
   Sur_Terminals_FWece = Region[{Ground, Terminal}]; // all terminals
   Sur_FW = Region[{Sur_Terminals_FWece, BoundaryNotTerminal}]; //all boundary
   Dom_FW = Region[ {Vol_FW, Sur_FW} ];
 }

 Function{
   /* Material properties are defined piecewise, for elementary groups */
   /* the values in the right hand side are defined in Ishape2d_data.pro */

   nu[#{MaterialX,Sur_FW}] = nu;  // magnetic reluctivity [m/H]
   mu[#{MaterialX,Sur_FW}] = mu;  // permeability [H/m]
   epsilon[#{MaterialX,Sur_FW}] = eps;       // permittivity [F/m]
   sigma[#{MaterialX,Sur_FW}]  = sigma;      // conductivity [S/m]

   // FW in E, ece BC, voltage excitation on bottom, ground on top
   // When imposing voltage or current via a constraint, the region appears there
   // If you have a complex, the value  has to be a function => use square brackets []
   If (Flag_AnalysisTypeFormulation==0) // E
      If(Flag_AnalysisType==0) // voltage excitation
         VTerminal1[] = -VTermRMS*Complex[Cos[VTermPhase],Sin[VTermPhase]];
      Else  // current excitation
         ITerminal1[] = -ITermRMS*Complex[Cos[ITermPhase],Sin[ITermPhase]];
      EndIf
   Else // H
      If(Flag_AnalysisType==0) // voltage excitation
         VTerminal1[] = VTermRMS*Complex[Cos[VTermPhase],Sin[VTermPhase]];
      Else  // current excitation
         ITerminal1[] = ITermRMS*Complex[Cos[ITermPhase],Sin[ITermPhase]];
      EndIf
   EndIf
 }

 Constraint{

   // ece BC or electrokinetics
   { Name SetTerminalPotential; Type Assign;  // voltage excited terminals
     Case {
       { Region Ground; Value 0.; }
       If(Flag_AnalysisType==0)
         { Region Terminal; Value VTerminal1[]; }
       EndIf
     }
   }
   { Name SetTerminalCurrent; Type Assign;   // current excited terminals
     Case {
       If(Flag_AnalysisType==1)
         { Region Terminal; Value ITerminal1[]/h2Ddepth; }  
       EndIf
     }
   }
   
   { Name SetTerminalCurrentH; Type Assign;   // current excited terminals - formulation in H
     Case {
       If(Flag_AnalysisType==1)
         { Region Cut1H; Value ITerminal1[]/h2Ddepth; }  
       EndIf
     }
   }
   
   { Name SetTerminalPotentialH ;
    Case {
      If(Flag_AnalysisType==0)
      { Region Cut1H; Value VTerminal1[] ; }
      EndIf
    }
  }

}

modelDir = "../";
Dir = "res/";

// The formulation and its tools
If (Flag_AnalysisTypeFormulation==0)  // E inside
  Include "../formulations/FullWave_E_ece_SISO_secondOrder.pro"
Else // H inside
  Include "../formulations/FullWave_H_ece_SISO_cplx_3D_secondOrder.pro"
EndIf

Macro Change_post_options
Echo[Str[ "nv = PostProcessing.NbViews;",
    "For v In {0:nv-1}",
    "View[v].NbIso = 25;",
    "View[v].RangeType = 3;" ,// per timestep
    "View[v].ShowTime = 3;",
    "View[v].IntervalsType = 3;",
    "EndFor"
  ], File "post.opt"];
Return


PostOperation {
  If (Flag_AnalysisTypeFormulation==0)   // E

    { Name Maps; NameOfPostProcessing FW_E_ece ;
      Operation {
        Print [ gradV, OnElementsOf ElementsOf[Vol_FW,OnOneSideOf Sur_FW], File StrCat[Dir,"map_gradV.pos" ]];
        Print [ Vterminal, OnElementsOf ElementsOf[Sur_FW], File StrCat[Dir,"map_Vterminals.pos" ]];
        Print [ E, OnElementsOf Vol_FW, File StrCat[Dir,"map_E.pos" ]];
        Print [ J, OnElementsOf Vol_FW, File StrCat[Dir,"map_J.pos" ]];
        Print [ rmsJ, OnElementsOf Vol_FW, File StrCat[Dir,"map_absJ.pos" ]];
        // Print [ B, OnElementsOf Vol_FW, File StrCat[Dir,"map_B.pos" ]];
        Print [ H, OnElementsOf Vol_FW, File StrCat[Dir,"map_H.pos" ]];
        Print [ VnotTerminals, OnElementsOf Vol_FW, File StrCat[Dir,"map_VnotTerminals.pos" ]];

        Call Change_post_options;
      }
    }

    { Name TransferMatrix; NameOfPostProcessing FW_E_ece ;
      Operation {
        If(Flag_AnalysisType==1)  // current excitation        
          Print [ Vterminal, OnRegion Terminal,
            Format Table , File  > StrCat[Dir, "test_Z_RI_formE.s1p"]] ;
        Else 
          Print [ I, OnRegion Terminal,
            Format Table , File  > StrCat[Dir, "test_Y_RI_formE.s1p"]] ;
        EndIf
      }
    }
  
  Else // H
 
    { Name TransferMatrix; NameOfPostProcessing FW_H_ece ;
      Operation {
        If(Flag_AnalysisType==1)  // current excitation        
          Print [ Vterminal, OnRegion Cut1H,
            Format Table , File  > StrCat[Dir, "test_Z_RI_formH.s1p"]] ;
          Else      
          Print [ I, OnRegion Cut1H,
            Format Table , File  > StrCat[Dir, "test_Y_RI_formH.s1p"]] ;
        EndIf
      }
    }

    { Name Maps; NameOfPostProcessing FW_H_ece ;
      Operation {
        Print [ E, OnElementsOf Vol_FW, File StrCat[Dir,"map_E.pos" ]];
        Print [ J, OnElementsOf Vol_FW, File StrCat[Dir,"map_J.pos" ]];
        Print [ rmsJ, OnElementsOf Vol_FW, File StrCat[Dir,"map_absJ.pos" ]];

        // Print [ B, OnElementsOf Vol_FW, File StrCat[Dir,"map_B.pos" ]];
        Print [ H, OnElementsOf Vol_FW, File StrCat[Dir,"map_H.pos" ]];
        Call Change_post_options;
      }
    }  
  EndIf

}