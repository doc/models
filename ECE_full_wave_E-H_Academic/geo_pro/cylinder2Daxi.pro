Include "cylinder2Daxi_data.pro";

Group{
   MaterialX = Region[MATERIALX]; /* MaterialX - homogenous domain */
   Ground = Region[GROUND];       /* Boundary - various parts */
   Terminal = Region[TERMINAL];
   RightBoundary = Region[RIGHTBOUNDARY];
   Axis = Region[LEFTBOUNDARY];                 // the axis is taken out of the domain
   Cut1H = Region[RIGHTBOUNDARY];               // for the H formulation
   BoundaryNotTerminal = Region[{RIGHTBOUNDARY}];
  
   Sur_Terminals_FWece = Region[{Ground, Terminal}]; // all terminals
  
   Vol_FW = Region[{MaterialX}]; // domain without the boundary
   Sur_FW = Region[{Sur_Terminals_FWece, RightBoundary}]; //all boundary
   Dom_FW = Region[ {Vol_FW, Sur_FW, Axis} ];
 }
 
 Function{
  nu[#{MaterialX,Sur_FW}] = nu;             // magnetic reluctivity [m/H]
  mu[#{MaterialX,Sur_FW}] = mu;             // permeability [H/m]
  epsilon[#{MaterialX,Sur_FW}] = eps;       // permittivity [F/m]
  sigma[#{MaterialX,Sur_FW}]  = sigma;      // conductivity [S/m]

  // This is a problem with 2 terminals, out of which one is grounded.
  // The non-grounded can be excited in voltage or in current.

  If (Flag_AnalysisType == 0)    // voltage excitation 
    // (Flag_AnalysisTypeFormulation==0) => formulation in E
    VTerminal1[] = (Flag_AnalysisTypeFormulation==0) ? -1 : 1;
  Else                            // current excitation
    ITerminal1[] = (Flag_AnalysisTypeFormulation==0) ? -1 : 1;
  EndIf

 }

Constraint{

  // ece BC
  { Name SetTerminalPotential; Type Assign;  // voltage excited terminals
    Case {
      { Region Ground; Value 0.; }
      If((Flag_AnalysisType==0))    
        { Region Terminal; Value VTerminal1[]; }
      EndIf
    }
  }
  { Name SetTerminalCurrent; Type Assign;   // current excited terminals
    Case {
      If((Flag_AnalysisType==1))
        { Region Terminal; Value ITerminal1[]/h2Ddepth; }  // here the depth is needed
      EndIf
    }
  }
  { Name SetTerminalCurrentH; Type Assign;   // current excited terminals - formulation in H
    Case {
      If(Flag_AnalysisType==1)
        { Region Cut1H; Value ITerminal1[]/h2Ddepth; }  
      EndIf
    }
  }
   
  { Name SetTerminalPotentialH ;
    Case {
    If(Flag_AnalysisType==0)
      { Region Cut1H; Value VTerminal1[] ; }
    EndIf
    }
  }
  
  { Name ImposeHonAxis ;
    Case {
      If( Flag_AnalysisTypeFormulation==1 && Flag_Axi==1 ) // H
          { Region Axis; Value 0 ; }
      EndIf
    }
	}
	
  { Name ImposeHorder2onAxis ;
    Case {
      If( Flag_AnalysisTypeFormulation==1 && Flag_Axi==1 ) // H
        { Region Axis; Value 0 ; }
      EndIf
    }
  }
   
  { Name ImposeE ;
  }

}

modelDir = "../";
Dir = "res/";

// The formulation and its tools
If (Flag_AnalysisTypeFormulation==0)          
  Include "../formulations/FullWave_E_ece_SISO_secondOrder.pro"                    // formulation in E
Else 
  Include "../formulations/FullWave_H_ece_SISO_cplx_2D_and_AXI_secondOrder.pro"    // formulation in H
EndIf

 
Macro Change_post_options
Echo[Str[ "nv = PostProcessing.NbViews;",
    "For v In {0:nv-1}",
    "View[v].NbIso = 25;",
    "View[v].RangeType = 3;" ,// per timestep
    "View[v].ShowTime = 3;",
    "View[v].IntervalsType = 3;",
    "EndFor"
  ], File "post.opt"];
Return

PostOperation {
  If (Flag_AnalysisTypeFormulation==0)   // E

    { Name Maps; NameOfPostProcessing FW_E_ece ;
      Operation {
        Print [ gradV, OnElementsOf ElementsOf[Vol_FW,OnOneSideOf Sur_FW], File StrCat[Dir,"map_gradV.pos" ]];
        Print [ Vterminal, OnElementsOf ElementsOf[Sur_FW], File StrCat[Dir,"map_Vterminals.pos" ]];

        Print [ E, OnElementsOf Vol_FW, File StrCat[Dir,"map_E.pos" ]];
        Print [ J, OnElementsOf Vol_FW, File StrCat[Dir,"map_J.pos" ]];
        Print [ rmsJ, OnElementsOf Vol_FW, File StrCat[Dir,"map_absJ.pos" ]];

        // Print [ B, OnElementsOf Vol_FW, File StrCat[Dir,"map_B.pos" ]];
        Print [ H, OnElementsOf Vol_FW, File StrCat[Dir,"map_H.pos" ]];

        Print [ VnotTerminals, OnElementsOf Vol_FW, File StrCat[Dir,"map_VnotTerminals.pos" ]];

        Call Change_post_options;
      }
    }   

    { Name TransferMatrix; NameOfPostProcessing FW_E_ece ;
      Operation {
        If(Flag_AnalysisType==1)  // current excitation
          Print [ Vterminal, OnRegion Terminal,
            Format Table , File  > StrCat[Dir, "test_Z_RI_formE.s1p"]] ;
        Else
          Print [ I, OnRegion Terminal,
            Format Table , File  > StrCat[Dir, "test_Y_RI_formE.s1p"]] ;
        EndIf
      }  
    }
  
  Else // H

    { Name Maps; NameOfPostProcessing FW_H_ece ;
      Operation {
        Print [ E, OnElementsOf Vol_FW, File StrCat[Dir,"map_E.pos" ]]; 
	      Print [ J, OnElementsOf Vol_FW, File StrCat[Dir,"map_J.pos" ]];
        Print [ rmsJ, OnElementsOf Vol_FW, File StrCat[Dir,"map_absJ.pos" ]];

        // Print [ B, OnElementsOf Vol_FW, File StrCat[Dir,"map_B.pos" ]];
        Print [ H, OnElementsOf Vol_FW, File StrCat[Dir,"map_H.pos" ]];
        Call Change_post_options;
      }    
    }

    { Name TransferMatrix; NameOfPostProcessing FW_H_ece ;
      Operation {
        If(Flag_AnalysisType==1)  // current excitation
          Print [ Vterminal, OnRegion Cut1H,
            Format Table , File  > StrCat[Dir, "test_Z_RI_formH.s1p"]] ;
        Else
          Print [ I, OnRegion Cut1H,
             Format Table , File  > StrCat[Dir, "test_Y_RI_formH.s1p"]] ;
        EndIf
      }  
    }  

  EndIf
}

