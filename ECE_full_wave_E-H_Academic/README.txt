Electric circuit element  (ECE) boundary conditions in full-wave electromagnetic problems.

  Models developed by G. Ciuprina, D. Ioan and R.V. Sabariego.


This directory contains the pro and geo files necessary to run the test cases in article:

G. Ciuprina, D. Ioan, and R.V. Sabariego. Implementation and validation of the dual full-wave E and H
  formulations with electric circuit element boundary conditions.
  Scientific Computing in Electrical Engineering SCEE 2022. Under review, Springer International Publishing.


Quick start
-----------
  Open "cyliderCoax.pro" with Gmsh.

Cylinder benchmark in geo_pro/
------------------------------
  "cylinder2Daxi_data.pro" - contains geometrical and physical parameters used by the geo and pro files.
  "cylinder2Daxi.geo" - contains the geometry description.
  "cylinder2Daxi.pro" - contains the problem definition.

  "cylinder3D_data.pro" - contains geometrical and physical parameters used by the geo and pro files.
  "cylinder3D.geo" - contains the geometry description.
  "cylinder3D.pro" - contains the problem definition.

Coaxial cable benchmark in geo_pro/
-----------------------------------
  "coax2Daxi_data.pro" - contains geometrical and physical parameters used by the geo and pro files.
  "coax2Daxi.geo" - contains the geometry description.
  "coax2Daxi.pro" - contains the problem definition.

  "coax3D_data.pro" - contains geometrical and physical parameters used by the geo and pro files.
  "coax3D.geo" - contains the geometry description.
  "coax3D.pro" - contains the problem definition.

Common formulation files in formulations/
----------------------------------------
  "only_FunctionSpace_and_Formulation_FullWave_E_ece_secondOrder.pro" - function spaces and E-formulation
  "FullWave_E_ece_MIMO2terminals_secondOrder.pro" - resolution and postprocessing for E-formulation, MIMO terminals
  "FullWave_E_ece_SISO_secondOrder.pro" - resolution and postprocessing for E-formulation, SISO terminals

  "only_FunctionSpace_and_Formulation_FullWave_H_ece_cplx_2D_and_AXI_secondOrder.pro" - function spaces and H-formulation in 2D \& 2D axi
  "FullWave_H_ece_MIMO2terminals_cplx_2D_and_AXI_secondOrder.pro" - resolution and postprocessing for H-formulation, MIMO terminals, 2D \& 2D axi
  "FullWave_H_ece_SISO_cplx_2D_and_AXI_secondOrder.pro" - resolution and postprocessing for H-formulation, SISO terminals, 2D \& 2D axi

  "only_FunctionSpace_and_Formulation_FullWave_H_ece_cplx_3D_secondOrder.pro" - function spaces and H-formulation in 3D
  "FullWave_H_ece_MIMO2terminals_cplx_3D_secondOrder.pro" - resolution and postprocessing for H-formulation, MIMO terminals, 2D \& 2D axi
  "FullWave_H_ece_SISO_cplx_3D_secondOrder.pro" - resolution and postprocessing for H-formulation, SISO terminals, 3D
