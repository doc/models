// Cylinder and Coaxial Cable with ECE
// uncomment only one of the lines TEST = ..... below

// The geometries are in separate .geo files 2.5D / 3D   cylinder/coax => 4 geo files in the geo folder
//                                           
// They use separate pro files => see pro folder

// The following tests should work without errors
// TEST code of the test 7 digits - their meaning is given in the following order:
// 1234567
// 1 model ID: 1 for cylinder, 2 for coaxial cable
// 2 is the submodel number 
// 3 is 0 for voltage excitation and 1 for current excitation of terminal no 1 (cylinder - there is only one excited terminal)
// 4 is 0 for voltage excitation and 1 for current excitation of terminal no 1 (it has a meaning only for the coaxial cable example)
// 5 is for the model flag 0 for 2.5D and 1 for 3D
// 6 is for the formulation 0 for E and 1 for H
// 7 id for FE order 1 or  2

DefineConstant [ // allowing an external call (-set_number), e.g. from Matlab, python or a shell	
    TEST = 0  // for no predefined tests
	
	// TEST code of the test 7 digits - their meaning is given in the following order:
	// 1234567
	// 1 - model ID: 1 for cylindrical, 2 for coax
	// 2 - is the submodel ID
	// 3 is 0 for voltage excitation and 1 for current excitation - for the first excited terminal (the only one for the cylinder test)
	// 4 is 0 for voltage excitation and 1 for current excitation - for the second excited terminal (relevant only for the coax test)
	// 5 is for the model flag 0 for 2.5D and 1 for 3D
	// 6 is for the formulation 0 for E and 1 for H
	// 7 id for FE order 1 or  2

	// TESTS - you can look in the s1p files in the res folder
	//------ cylinder HF, v1, 2.5D ------
	//TEST = 1100001    // 1) cylinder, v1, voltage excitation, - , 2.5D, formulation in E, 1st order elements  
	//TEST = 1100002    // 2) cylinder, v1, voltage excitation, - , 2.5D, formulation in E, 2nd order elements 
	//TEST = 1100011    // 3) cylinder, v1, voltage excitation, - , 2.5D, formulation in H, 1st order elements 
	//TEST = 1100012    // 4) cylinder, v1, voltage excitation, - , 2.5D, formulation in H, 2nd order elements 
    //TEST = 1110001    // 5) cylinder, v1, current excitation, - , 2.5D, formulation in E, 1st order elements  
    //TEST = 1110002    // 6) cylinder, v1, current excitation, - , 2.5D, formulation in E, 2nd order elements
	//TEST = 1110011    // 7) cylinder, v1, current excitation, - , 2.5D, formulation in H, 1st order elements   
	//TEST = 1110012    // 8) cylinder, v1, current excitation, - , 2.5D, formulation in H, 2nd order elements 
    //------ coax HF, 2.5D ------
	//TEST = 2100001    // 9) coax HF, voltage excitation, - , 2.5D, formulation in E, 1st order elements       // needed for fig 5 in the SCEE 2022 paper
	//TEST = 2100002    // 10) coax HF, voltage excitation, - , 2.5D, formulation in E, 2nd order elements      // needed for fig 5 in the SCEE 2022 paper
	//TEST = 2100011    // 11) coax HF, voltage excitation, - , 2.5D, formulation in H, 1st order elements      // needed for fig 5 in the SCEE 2022 paper
	//TEST = 2100012    // 12) coax HF, voltage excitation, - , 2.5D, formulation in H, 2nd order elements      // needed for fig 5 in the SCEE 2022 paper
    //TEST = 2111001    // 13) coax HF, current excitation, - , 2.5D, formulation in E, 1st order elements  
    //TEST = 2111002    // 14) coax HF, current excitation, - , 2.5D, formulation in E, 2nd order elements
	//TEST = 2111011    // 15) coax HF, current excitation, - , 2.5D, formulation in H, 1st order elements   
	//TEST = 2111012    // 16) coax HF, current excitation, - , 2.5D, formulation in H, 2nd order elements 
    //------ coax LF, 2.5D ------
	//TEST = 2200001    // 17) coax LF, voltage excitation, - , 2.5D, formulation in E, 1st order elements      // needed for fig 6 in the SCEE 2022 paper
	//TEST = 2200002    // 18) coax LF, voltage excitation, - , 2.5D, formulation in E, 2nd order elements      // needed for fig 6 in the SCEE 2022 paper
	//TEST = 2200011    // 19) coax LF, voltage excitation, - , 2.5D, formulation in H, 1st order elements      // needed for fig 6 in the SCEE 2022 paper
	//TEST = 2200012    // 20) coax LF, voltage excitation, - , 2.5D, formulation in H, 2nd order elements      // needed for fig 6 in the SCEE 2022 paper
    //TEST = 2211001    // 21) coax LF, current excitation, - , 2.5D, formulation in E, 1st order elements  
    //TEST = 2211002    // 22) coax LF, current excitation, - , 2.5D, formulation in E, 2nd order elements
	//TEST = 2211011    // 23) coax LF, current excitation, - , 2.5D, formulation in H, 1st order elements   
	//TEST = 2211012    // 24) coax LF, current excitation, - , 2.5D, formulation in H, 2nd order elements 

	//------ 3D
	//------ cylinder, v1, 3D ------
	//TEST = 1100101    // 101) cylinder, v1, voltage excitation, - , 3D, formulation in E, 1st order elements  
	//TEST = 1100102    // 102) cylinder, v1, voltage excitation, - , 3D, formulation in E, 2nd order elements 
	//TEST = 1100111    // 103) cylinder, v1, voltage excitation, - , 3D, formulation in H, 1st order elements 
	//TEST = 1100112    // 104) cylinder, v1, voltage excitation, - , 3D, formulation in H, 2nd order elements 
    //TEST = 1110101    // 105) cylinder, v1, current excitation, - , 3D, formulation in E, 1st order elements   // needed for fig 1 in the SCEE 2022 paper
    //TEST = 1110102    // 106) cylinder, v1, current excitation, - , 3D, formulation in E, 2nd order elements
	//TEST = 1110111    // 107) cylinder, v1, current excitation, - , 3D, formulation in H, 1st order elements   // needed for fig 1 in the SCEE 2022 paper
	//TEST = 1110112    // 108) cylinder, v1, current excitation, - , 3D, formulation in H, 2nd order elements 
];



If (TEST == 0)  // some default setting if no predefined test is used
	MODEL_ID = 0;   // no model chosen
	FLAG_MODEL = 0; // 2D axi
	
	TERMINAL_EXC = 1;
	TERMINAL_EXC2 = 1; 
	FORMULATION = 0;
	FE_ORDER = 1;
	
	MODEL_ID = 1 ;               // 1 for cylindrical, 2 for coax
	MODEL_ID2 =1 ;              // 1 vor version v1 - not really used for the moment
	TERMINAL_EXC = 0;        // 0 for voltage excitation, 1 for current excitation
	TERMINAL_EXC2 = 0;       // second terminal - relevant only for the coax test
	FLAG_MODEL = 1;          // 0 for 2.5 D, 1 for 3D
	FORMULATION = 0;         // 0 for E, 1 for H
	FE_ORDER = 1;            // 1 for order 1, 2 for order 2
EndIf

/* 2.5D */
// Include "./tests/tests_1to8_data.pro";         /* cylinder - version v1 (SCEE 2020, JMI) - 2.5D */
// Include "./tests/tests_9to16_data.pro";        /* coaxial cable [Wu04] with conductor inside, HF (SCEE 2022)  - 2.5D */
// Include "./tests/tests_17to24_data.pro";       /* coaxial cable [Wu04] with conductor inside, LF (SCEE 2022)  - 2.5D */

/* 3D */
// Include "./tests/tests_101to108_data.pro";    /* cylinder - version v1 (SCEE 2020, 2022, JMI) - 3D */
//Include "./tests/tests_109to116_data.pro";       /* coaxial cable [Wu04] with conductor inside - 3D */

// If TEST == 0 , all the tests above were commented 
/* _______Choice 1_______  chose the model 2.5D (2D axi) or 3D  */
DefineConstant[
	Flag_3Dmodel = {0, Choices {0,1}, 
		Name "{Model/1Use 3D model?", Highlight "Pink"}
	ang_section_degrees = 360
	Flag_model = {1, Choices{1="Cylinder (SISO)",2="Coaxial cable (MIMO)"}, 
		Name "{Model/2Choose model", Highlight "Tomato"}
];	

modelDim = (!Flag_3Dmodel)? 2: 3;
Flag_Axi = (!Flag_3Dmodel)? 1:-1; // 2.5 D; if this flag is zero, this would mean a 2D problem, not an axisymmetric one
h2Ddepth = (!Flag_3Dmodel)? 2*Pi:360/ang_section_degrees;


// just to be sure that the parameters are correct
Printf("========================= MAIN PARAMETERS =========================");
Printf("*** *** *** ==> Flag_model   (1-Cylinder, 2-Coaxial cable, 0-Not selected) = %e",Flag_model);
Printf("*** *** *** ==> Flag_3Dmodel (0-for 2.5D, 1 for 3D) = %e",Flag_3Dmodel);
