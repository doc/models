//=========================================================
// Function spaces
//=========================================================

FunctionSpace {

	{ Name Hcurl_H; Type Form1;   // Hspace for 3D
		BasisFunction {	
      { Name se; NameOfCoef he; Function BF_Edge;
        Support Dom_FW;	Entity EdgesOf[All, Not BoundaryNotTerminal]; }
        
      { Name sn; NameOfCoef phin; Function BF_GradNode;
        Support Dom_FW; Entity NodesOf[BoundaryNotTerminal]; }

      { Name sc; NameOfCoef ic; Function BF_GroupOfEdges;
        Support Dom_FW;	Entity GroupsOfEdgesOf[Cut1H]; }

      If(FE_Order == 2) // hierarchical basis functions for 2nd order interpolation 
        // Edge BFs => FEorder = {0.5, Choices{0.5="Lowest",1="1st order",1.5="Reduced 2nd order",2="2nd order"}
        // 1st order
        { Name se2; NameOfCoef ee2; Function BF_Edge_2E;  
        Support Dom_FW; Entity EdgesOf[All, Not BoundaryNotTerminal]; }        
        // Reduced 2nd order
        { Name se3a ; NameOfCoef ee3a ; Function BF_Edge_3F_a ;
        Support Dom_FW ; Entity FacetsOf[All, Not BoundaryNotTerminal] ; }
        { Name se3b ; NameOfCoef ee3b ; Function BF_Edge_3F_b ;
        Support Dom_FW ; Entity FacetsOf[ All, Not BoundaryNotTerminal] ; }
        // 2nd order
        { Name se4; NameOfCoef ee4; Function BF_Edge_4E;
        Support Dom_FW ; Entity EdgesOf[All, Not BoundaryNotTerminal]; }
      
        // GradNode BFs
        { Name sn2; NameOfCoef vn2; Function BF_GradNode_2E;
        Support Dom_FW ; Entity EdgesOf[BoundaryNotTerminal]; }
      EndIf	
    }
	
    GlobalQuantity {
      { Name TerminalCurrent;   Type AliasOf       ; NameOfCoef ic; }
      { Name TerminalPotential; Type AssociatedWith; NameOfCoef ic; }
    }
	
    Constraint {
      { NameOfCoef TerminalPotential; EntityType GroupsOfEdgesOf;
        NameOfConstraint SetTerminalPotentialH; }
      { NameOfCoef TerminalCurrent; EntityType GroupsOfEdgesOf;
        NameOfConstraint SetTerminalCurrentH; }
    }
  }  
}

Function{
  Omega = 2*Pi*Freq;
  jOmega[] = Complex[0,1]*Omega;
  jOmega2[] = -Omega^2;

  alphaIm[] = jOmega[]*sigma[]/(sigma[]^2+Omega^2*epsilon[]^2);
  alphaRe[] = Omega^2*epsilon[]/(sigma[]^2+Omega^2*epsilon[]^2);
  
  alphaImSigmaZero[] = 0;
  alphaReSigmaZero[] = 1/epsilon[];
  
}

Formulation {

   { Name FullWave_H_ece; Type FemEquation;
    Quantity {
      { Name h; Type Local;  NameOfSpace Hcurl_H; }
      { Name V; Type Global; NameOfSpace Hcurl_H[TerminalPotential]; }
      { Name I; Type Global; NameOfSpace Hcurl_H[TerminalCurrent]; }
    }
    Equation {
      // multiplying with j omega
      // \int_D \alpha * curl(\vec{H}) \cdot  curl(\vec{h}) dv
	
      Galerkin { [ alphaRe[] * Dof{d h} , {d h} ];      In Vol_FW; Jacobian Vol; Integration Int; }
      Galerkin { [ alphaIm[] * Dof{d h} , {d h} ];      In Vol_FW; Jacobian Vol; Integration Int; }

      // \int_D j*\omega*(j*\omega*\mu) \vec{H} \cdot \vec{h} dv
      Galerkin { [ jOmega2[] * mu[] * Dof{h} , {h} ]; In Vol_FW; Jacobian Vol; Integration Int; }
	  
      // sum (Vk ik);  for k - all terminals so that the terminal voltages will be computed as well
      GlobalTerm { [ jOmega[]*Dof{V}, {I} ]; In Cut1H; }
    }
  }
  
  { Name FullWave_H_ece_G; Type FemEquation;
    Quantity {
      { Name h; Type Local;  NameOfSpace Hcurl_H; }
      { Name V; Type Global; NameOfSpace Hcurl_H[TerminalPotential]; }
      { Name I; Type Global; NameOfSpace Hcurl_H[TerminalCurrent]; }
    }
    Equation {
      // without multiplying with j omega
      Galerkin { [ (1/(sigma[]+Complex[0,1]*2*Pi*Freq*epsilon[])) * Dof{d h} , {d h} ]; In Vol_FW; Jacobian Vol; Integration Int; }
      Galerkin { [ Complex[0,1]*2*Pi*Freq* mu[] * Dof{h} , {h} ]; In Vol_FW; Jacobian Vol; Integration Int; }

      // sum (Vk ik);  for k - all terminals so that the terminal voltages will be computed as well
      GlobalTerm { [ Dof{V}, {I} ]; In Cut1H; }
    }
  }
}
