/* FullWave_E_ece_MIMO2terminals.pro  (old name "formulationFW_E_eceBC_v6_MIMO2terminals")  

 Problem independent pro file
 for passive, linear domains in Full Wave with ECE boundary conditions.

 It uses a formulation with vec{E} strictly inside the domain and V strictly on the boundary.
 The domain is simply connected, it has only electric terminals.
 The terminals can be voltage or current excited.
 The material inside is linear from all points of view: electric, magnetic, conduction
 It is compulsory that there exists a ground terminal (its voltage is set to zero).
 The post-operation assumes that there is one excited (active) terminal  (excitation equal to 1 - it can be in 
 voltage or current excited terminal) and the other one is set to zero.
 Frequency domain simulations.
*/

Include "Jacobian_and_Integration.pro"
Include "only_FunctionSpace_and_Formulation_FullWave_E_ece_secondOrder.pro"

Resolution {

  { Name FullWave_E_ece;
    System {
      { Name Sys_Ele; NameOfFormulation FullWave_E_ece;
        Type Complex; Frequency Freq;}
    }
    Operation {
      CreateDir[StrCat[modelDir,Dir]];
      SetTime[Freq]; // usefull to save (and print) the current frequency
      Generate[Sys_Ele]; Solve[Sys_Ele]; SaveSolution[Sys_Ele];
    }
  }

}

PostProcessing {

  { Name FW_E_ece; NameOfFormulation FullWave_E_ece;
    Quantity {
      { Name E; Value {
            Local { [ {e} ]; In Vol_FW; Jacobian Vol; } // complex
          }
      }
      
      { Name J; Value {
          Local { [ sigma[]*{e} ]; In Vol_FW; Jacobian Vol; } // complex
        }
      }
      
      { Name rmsJ; Value {
          Local { [ Norm[sigma[]*{e}] ]; In Vol_FW; Jacobian Vol; } // real
        }
      }

      { Name gradV; Value {
          Local { [ {dv} ]; In Vol_FW; Jacobian Vol; } // complex
        }
      }

      { Name Vterminal; Value {
        Term { [ -{V} ]; In Terminal1; } 
        Term { [ -{V} ]; In Terminal2; } 
        }
      }

      { Name Iterminal; Value { 
        Term { [ -1*{I}*h2Ddepth ]; In Terminal1; } 
        Term { [ -1*{I}*h2Ddepth ]; In Terminal2; } 
        }  
      } 
      
      { Name VnotTerminals; Value {
          Local { [ -{dInv dv} ]; In Vol_FW; Jacobian Vol; }
        }
      }
	  
      { Name B; Value {
          Local { [ CompZ[{d e}]/(2*Pi*Freq*Complex[0,1]) ]; In Vol_FW; Jacobian Vol; }
        }
      }

      { Name H; Value {
        Local { [ -nu[]*{d e}/(2*Pi*Freq*Complex[0,1]) ]; In Vol_FW; Jacobian Vol; }
        }
      }
      
      { Name Hz; Value {
        Local { [ nu[]*CompZ[{d e}]/(2*Pi*Freq*Complex[0,1]) ]; In Vol_FW; Jacobian Vol; }
        }
      }
    }
  }
}
