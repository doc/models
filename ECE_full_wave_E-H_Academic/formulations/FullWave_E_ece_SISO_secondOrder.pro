/* FullWave_E_ece_SISO.pro 

 Problem independent pro file
 for passive, linear domains in Full Wave with radiant ECE boundary conditions.

 It uses a formulation with vec{E} strictly inside the domain and V strictly on the boundary.
 The domain is simply connected, it has only electric terminals.
 The terminals can be voltage or current excited.
 The material inside is linear from all points of view: electric, magnetic, conduction
 It is compulsory that there exists a ground terminal (its voltage is set to zero).
 The post-operation assumes that there is one excited (active) terminal  (excitation equal to 1 - it can be in
 voltage or current excited terminal).
 Frequency domain simulations.
*/

Include "Jacobian_and_Integration.pro"
Include "only_FunctionSpace_and_Formulation_FullWave_E_ece_secondOrder.pro"

Resolution {
	{ Name FullWave_E_ece;
		System {
			{ Name Sys_Ele; NameOfFormulation FullWave_E_ece; Type Complex; Frequency Freq;}
		}
		Operation {
			CreateDir[StrCat[modelDir,Dir]];
			SetTime[Freq];  // usefull to save (and print) the current frequency
			Generate[Sys_Ele]; Solve[Sys_Ele]; SaveSolution[Sys_Ele];
		}
	}
}

PostProcessing {

	{ Name FW_E_ece; NameOfFormulation FullWave_E_ece;
		Quantity {
			{ Name E; Value {Local { [ {e} ]; In Vol_FW; Jacobian Vol; }}} // complex
			{ Name rmsE;  Value { Local { [ Norm[{e}] ];         In Vol_FW; Jacobian Vol; }}} // complex
			{ Name J;     Value { Local { [ sigma[]*{e} ];       In Vol_FW; Jacobian Vol; }}} // complex
			{ Name rmsJ;  Value { Local { [ Norm[sigma[]*{e}] ]; In Vol_FW; Jacobian Vol; }}} // real
			{ Name gradV; Value { Local { [ {dv} ];              In Vol_FW; Jacobian Vol; }}} // complex

			{ Name Vterminal; Value {
				  Term { [ -{V} ]; In Sur_Terminals_FWece; } // => unknowns only in Sur_Terminals_FWece, outside it will be zero
				}
			}

			{ Name Rin;  Value { Term { [ Re[-{V}] ]; In Sur_Terminals_FWece; }}}
			{ Name Xin;  Value { Term { [ Im[-{V}] ]; In Sur_Terminals_FWece; }}}
      { Name AbsZin; Value { Term { [Norm[-{V}] ]; In Sur_Terminals_FWece; }}}
			{ Name ArgZin; Value { Term { [Atan2[ Im[-{V}], Re[-{V}]] ]; In Sur_Terminals_FWece; }}}

			{ Name I; Value {
        Term { [ -{I}*h2Ddepth ]; In Sur_Terminals_FWece; }
				}
			}  // h2Ddepth is the depth for 2D problems,  and it has to be 1 for 3D problems

			// Yin = I/V = Gin + j Bin
			{ Name Yin;    Value { Term { [ -{I}*h2Ddepth * Conj[-{V}]/SquNorm[{V}] ]; In Sur_Terminals_FWece; }}}
			{ Name AbsYin; Value { Term { [ Norm[-{I}*h2Ddepth * Conj[-{V}]/SquNorm[{V}]] ]; In Sur_Terminals_FWece; }}}
			{ Name ArgYin; Value { Term { [ Atan2[ Im[-{I}*h2Ddepth * Conj[-{V}]/SquNorm[{V}]], Re[ -{I}*h2Ddepth * Conj[-{V}]/SquNorm[{V}]] ] ]; In Sur_Terminals_FWece; }}}

			{ Name Gin;  Value { Term { [Re[ -{I}*h2Ddepth ]]; In Sur_Terminals_FWece; }}}
			{ Name Bin;  Value { Term { [Im[ -{I}*h2Ddepth ]]; In Sur_Terminals_FWece; }}}
			{ Name AbsYin_; Value { Term { [ Norm[-{I}*h2Ddepth ] ]; In Sur_Terminals_FWece; }}}
			{ Name ArgYin_; Value { Term { [ Atan2[ Im[-{I}*h2Ddepth ], Re[-{I}*h2Ddepth ]] ]; In Sur_Terminals_FWece; }}}

			{ Name VnotTerminals; Value { 
				Local { [ -{dInv dv} ];  In Vol_FW; Jacobian Vol; }
				Local { [ -{dInv dvf} ]; In Vol_FW; Jacobian Vol; }
			}}

			{ Name B; Value {
					Local { [ {d e}/(2*Pi*Freq*Complex[0,1]) ]; In Vol_FW; Jacobian Vol; }
				}
			}

			{ Name H; Value {
					Local { [ -nu[]*{d e}/(2*Pi*Freq*Complex[0,1]) ]; In Vol_FW; Jacobian Vol; }
				}
			}

			{ Name rmsH; Value {
          Local { [Norm[ -nu[]*{d e}/(2*Pi*Freq*Complex[0,1]) ]]; In Vol_FW; Jacobian Vol; }
				}
			}
			
		}
	}
 }
