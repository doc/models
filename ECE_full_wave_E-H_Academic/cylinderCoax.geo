Include "cylinderCoax_data.pro";

If (Flag_model == 1)     // cylinder
  If (Flag_3Dmodel == 0) // 2D axi
    Include "./geo_pro/cylinder2Daxi.geo";
  Else // 3D
    Include "./geo_pro/cylinder3D.geo";
  EndIf
EndIf

If (Flag_model == 2)  // coaxial cable
  If (Flag_3Dmodel == 0) // 2D axi
    Include "./geo_pro/coax2Daxi.geo";
  Else // 3D
    Include "./geo_pro/coax3D.geo";
  EndIf
EndIf
