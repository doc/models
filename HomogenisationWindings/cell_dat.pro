mm=1e-3;

r_sh = 9.5*1e-2;
d_sh = 0.2;

DefineConstant[
  CellType = {0, Choices{0="Square",1="Hexa"}, Name "Cell/00Type of Cell"}
  NbrLayers = {0, Min 0, Max 1, Name "Cell/01Number of layers"}

  //Rc  = { (CellType==0) ? Sqrt[1/Pi] : Sqrt[1/Pi]/Sqrt[3],
  Rc  = { (CellType==0) ? r_sh : Sqrt[1/Pi]/Sqrt[3],
    Name "Cell/10Conductor radius (mm)"} //Cefc06
  // Dex = { 2.2*Rc, Min 2.01*Rc, Max 4*Rc, Name "Cell/11Packing side (mm)", Visible (CellType==0)}
  Dex = { d_sh, Min 2.01*Rc, Max 4*Rc, Name "Cell/11Packing side (mm)", Visible (CellType==0)}
];


If(CellType==0)
  UndefineConstant["Cell/20Fill factor"];
  UndefineConstant["Cell/11Outer radius hexagonal cell (mm)"];
  DefineConstant[
    Fill = {Pi*Rc^2/(Dex*Dex), Name "Cell/20Fill factor", ReadOnly 1, Highlight "LightGrey"}
  ];
  Dex = Dex*mm;
  Dey = Dex;
EndIf
If(CellType==1)
  UndefineConstant["Cell/20Fill factor"];
  DefineConstant[
    Fill = {0.9, Name "Cell/20Fill factor", ReadOnly 0}
    Rx = {Rc/Sin[Pi/3]/Fill, Name "Cell/11Outer radius hexagonal cell (mm)",
          ReadOnly 1, Visible CellType==1, Highlight "LightGrey"}
  ];
  Rx = Rx*mm;
  Ry = Rx;
EndIf
Rc  = Rc*mm;

AreaCond = Pi*Rc^2;
AreaCell = AreaCond/Fill;


NbrCond = (NbrLayers==0) ? 1 : ((CellType==0)?9:7) ;

// Printf("Round conductor, radius = %g mm", Rc*1e3);
// Printf("Square packing Fill factor = %g mm^2 / %g mm^2 = %g ",AreaCond*1e6,AreaCell*1e6,Fill);

If (Rc > Dex/2)
  Printf("Square packing: Two big fill factor!!! Rc %g > Dex/2 %g",Rc*1e6,Dex/2*1e6);
  Printf("limit Fill factor %g",AreaCond/(4*Rc*Rc));
  Dex = 2*(Rc+Rc*1e-2) ; Dey = Dex ;
EndIf

// characteristic lengths
// ======================

DefineConstant[
  MD = {1/1.5, Name "Cell/99Mesh density factor"}
];

p = Rc/10*MD  ;
pc = Rc/10*MD ;


ISOL  =  200;
COND  = 1000;
BOUND = 2000;
