// Time  domain + fine model:
// gmsh  ind_axi.geo -setnumber Flag_HomogenisedModel 0 -2 -o fine.msh -v 3
// getdp ind_axi -msh fine.msh -setnumber Flag_HomogenisedModel 0 -sn Flag_FD 0 -sn Flag_SrcType 2 -sn NbT 1 -sol Analysis

// Resistance (Ohms): p 'SF_s0.dat' u 1:2 w l lw 2, 'SH.dat' u 1:2 w p ps 2 pt 6 lw 2
// Inductance (mH):   p 'SF_s0.dat' u 1:($3*1e3) w l lw 2, 'SH.dat' u 1:($3*1e3) w p ps 2 pt 6 lw 2

Include "ind_axi_dat.pro"
Include "BH.pro"

la = Fill; // fill factor

// new files, finer mesh, max X=8
file_ZSkinRe  = Sprintf("coeff/pI_RS_la%.2g.dat", la);
file_ZSkinIm  = Sprintf("coeff/qI_RS_la%.2g.dat", la);
file_NuProxRe = Sprintf("coeff/qB_RS_la%.2g.dat", la);
file_NuProxIm = Sprintf("coeff/pB_RS_la%.2g.dat", la);

// time domain ()
file_TDcoeffs = Sprintf("coeff/CoeffsTD_RS_la%.2g.pro", la);
Include file_TDcoeffs;

DirRes = "res/";
po = "{Output/";

DefineConstant[
  visu = {0, Choices{0, 1}, AutoCheck 0,
    Name StrCat[mfem,"Visu/Real-time visualization"], Highlight "LightPink"}

  Flag_FD = { 1, Choices{0,1}, Name StrCat[mfem,"2Frequency domain analysis?"], Highlight "AliceBlue" }
  Flag_TD = !Flag_FD

  Flag_IronCore    = {1, Choices{0,1}, Name StrCat[mfem, "3Core/Iron core?"], Highlight Str[col1]}

  Flag_NL  = {0, Choices{0,1}, Name StrCat[mfem, "3Core/Nonlinear bh-curve?"],
              Highlight Str[col1], Visible Flag_IronCore, ReadOnly Flag_FD}

  Nb_max_iter = {100, Name StrCat[mfem, "3Core/Nonlinear solver/Max. num. iterations"],
    Visible Flag_NL, Highlight Str[col3]}
  iter_max = Nb_max_iter
  relaxation_factor = {1., Name StrCat[mfem, "3Core/Nonlinear solver/Relaxation factor"],
    Visible Flag_NL, Highlight Str[col3]}
  stop_criterion = {1e-8, Name StrCat[mfem, "3Core/Nonlinear solver/Stopping criterion"],
    Visible Flag_NL, Highlight Str[col3]} //1e-4

  Flag_ImposedVoltage = {1, Choices{0,1}, Name StrCat[mfem,"Source/001Imposed Voltage?"]}
  Flag_Circuit = {Flag_ImposedVoltage, Choices{0,1}, Name StrCat[mfem,"Source/002Use circuit"],
    ReadOnly (Flag_ImposedVoltage==1)}

  Flag_imposedRr   = {0, Choices{0,1},
    Name StrCat[mfem, "Source/1Imposed reduced frequency X"], Highlight Str[col1]}
  Flag_imposedFreq = !Flag_imposedRr

  ORDER = {1, Choices{
      1="order 1",
      2="order 2",
      3="order 3",
      4="order 4"},
    Name StrCat[mfem,"1Order of homog. approx. (time)"], Visible (Flag_TD==1 && Flag_HomogenisedModel==1),
           ReadOnly !(Flag_TD==1 && Flag_HomogenisedModel==1), Highlight "Red"}

  ExtGmsh = ".pos"
  ExtGnuplot = ".dat"
];

If(Flag_imposedRr) // Reduced frequency
  DefineConstant[
    Rr = {4, Min 0.1, Max 5, Step 0.1, Name StrCat[mfem,"Source/1Reduced frequency"], ReadOnly 0, Highlight "Ivory"}
    delta = {Rc/Rr, Name StrCat[mfem,"Source/2Skin depth [m]"], ReadOnly 1, Highlight "LightGrey"}
    Freq  = {1/(delta*delta*mu0*SigmaCu*Pi), Name StrCat[mfem,"Source/3Frequency [Hz]"], ReadOnly 1, Highlight "LightGrey"}
  ];
Else
  DefineConstant[
    Freq = { 50000, Min 0.1, Max 500e3, Name StrCat[mfem,"Source/3Frequency [Hz]"], ReadOnly 0, Highlight "Ivory"}
    delta = {1/Sqrt[mu0*SigmaCu*Freq*Pi], Name StrCat[mfem,"Source/2Skin depth [m]"], ReadOnly 1, Highlight "LightGrey"}
    Rr = {Rc/delta, Name StrCat[mfem,"Source/1Reduced frequency"], ReadOnly 1, Highlight "LightGrey"}
  ];
EndIf


DefineConstant[
  Omega  = 2*Pi*Freq,
  Period = 1./Freq,

  Vdc = 50 // amplitude of PWM voltage
  Val_EE = { Vdc, Name StrCat[mfem,"Source/003source amplitude"],  Highlight "Ivory", Visible 1}

  //------------------------------------------------------

  // Time stepping variables...
  NbT = {1, Name StrCat[mfem,"Source/5Nb periods"],  Highlight "Ivory", Visible Flag_TD}//5
  NbSteps = { 120, Name StrCat[mfem,"Source/6Nb steps per period"],
    Highlight "LightGrey", Visible Flag_TD, ReadOnly 1}
  tinit = {0.,  Name StrCat[mfem,"Source/7Initial time"], Highlight "Ivory", Visible Flag_TD}
  tend = {tinit+NbT*Period ,  Name StrCat[mfem,"Source/8Final time"], ReadOnly 1, Highlight "LightGrey", Visible Flag_TD}
  deltat = {Period/NbSteps, Name StrCat[mfem,"Source/9Step size"], ReadOnly 1, Highlight "LightGrey", Visible Flag_TD}
  thetav = 1.
];


Group{
  Air  = Region[{AIR, AIRGAP}];
  Insulation = Region[{INSULATION}];

  If(Flag_IronCore)
    Iron = Region[{IRON}];
  Else
    Iron = Region[{}];
    Air  += Region[{IRON}];
  EndIf

  OuterBoundary = Region[{OUTBND}]; // including symmetry

  Winding =  Region[{}] ;

  DomainCC = Region[{Air, Insulation, Iron}] ;

  SymFactor = Flag_HalfModel ? 2.:1. ; //half inductor with axisymmetry

  nbturns = (Flag_HomogenisedModel==0) ? NbrCond/SymFactor : 1  ; // number of turns

  If (Flag_HomogenisedModel==0) // Fine case
    For iF In {1:nbturns}
      Turn~{iF} = Region[{(iCOND+iF-1)}] ;
      Winding  += Region[{(iCOND+iF-1)}] ;
    EndFor

    DomainC = Region[{Winding}] ;
    DomainS = Region[{}] ;
  EndIf

  If (Flag_HomogenisedModel==1) //Homogenised case
    Turn~{1} = Region[{iCOND}] ;
    Winding = Region[{iCOND}];
    DomainC = Region[{}] ;
    DomainS = Region[{Winding}] ;
  EndIf

  DomainCC += Region[{DomainS}] ;

  If(Flag_NL)
    Domain_Lin      = Region[{Air, Insulation, Winding}];
    Domain_Lin_NoJs = Region[{Air, Insulation}];
    Domain_NonLin   = Region[{Iron}];
  Else
    Domain_Lin      = Region[{Air, Insulation, Winding, Iron}];
    Domain_Lin_NoJs = Region[{Air, Insulation, Iron}];
    Domain_NonLin   = Region[{}];
  EndIf

  Domain = Region[{DomainC, DomainCC}] ;

  //--------------------------------------------------------
  //--------------------------------------------------------

  // Groups related to source circuit
  Input = # 12345 ;

   // Groups related to the circuit
  Input = # 12345 ;
  iZH    = 10000 ;
  iLH    = 20000 ; // Zskin in homog. coil
  iLHp   = 30000 ;

  For k In {1:ORDER}
    Zh~{k} = Region[{(iZH+k)}];
    Lh~{k} = Region[{(iLH+k)}];
  EndFor
  For k In {2:ORDER}
    Lhp~{k-1} = Region[{(iLHp+k-1)}];
  EndFor

  Resistance_Cir  = Region[{}];
  If(Flag_FD && Flag_HomogenisedModel==1) // Frequency domain
    Resistance_Cir += Region[{Zh~{1}}];
  EndIf
  If(Flag_TD && Flag_HomogenisedModel==1) // Time domain
    For k In {1:ORDER}
      Resistance_Cir += Region[{Zh~{k}}];
    EndFor
  EndIf

  Inductance_Cir  = Region[{}];
  If(Flag_TD && Flag_HomogenisedModel==1) // Time domain
    For k In {1:ORDER}
      Inductance_Cir += Region[{Lh~{k}}];
    EndFor
    For k In {2:ORDER}
      Inductance_Cir += Region[{Lhp~{k-1}}];
    EndFor
  EndIf

  Capacitance1_Cir = Region[ {} ] ;
  Capacitance2_Cir = Region[ {} ] ;
  Capacitance_Cir = Region[ {Capacitance1_Cir, Capacitance2_Cir} ] ;

  SourceV_Cir = Region[ {Input} ] ;
  SourceI_Cir = Region[ {} ] ;

  DomainZ_Cir = Region[ {Resistance_Cir, Inductance_Cir, Capacitance_Cir} ] ;

  DomainSource_Cir = Region[ {SourceV_Cir, SourceI_Cir} ] ;
  DomainZt_Cir = Region[ {DomainZ_Cir, DomainSource_Cir} ] ;

}


Function {

  CoefGeo = 2*Pi*SymFactor ; // axisymmetry + symmetry factor

  sigma[#{Winding}] = SigmaCu ;
  sigma[#{Air, Insulation, Iron}] = 0.;
  rho[] = 1/sigma[];

  nu[#{Air, Insulation}] = nu0;

  If (!Flag_NL)
    nu[#{Iron}]   = nu0/1000;
  Else
    nu[ #{Iron} ] = nu_3kW[$1] ;
    h[ #{Iron} ]  = h_3kW[$1];
    dhdb_NL[ #{Iron} ]= dhdb_3kW_NL[$1] ;
    dhdb[ #{Iron} ]   = dhdb_3kW[$1] ;
  EndIf


  //==================================================================
  If(Flag_FD) // Frequency domain
    FSinusoidal[] = Complex_MH[1,0]{Freq} ; //Cos F_Cos_wt_p[]{2*Pi*Freq, 0};
  Else // Time domain
    FSinusoidal[] = Complex_MH[0,-1]{Freq} ; //Sin F_Sin_wt_p[]{2*Pi*Freq, 0};
  EndIf
  //------------------------------------------------------

  Fct_Src[] = FSinusoidal[];

  //==================================================================

  // Homogenization coefficients: round conductor & square packing
  // Frequency domain
  skin_rhor_list() = ListFromFile[ file_ZSkinRe ];
  skin_rhoi_list() = ListFromFile[ file_ZSkinIm ];
  prox_nur_list()  = ListFromFile[ file_NuProxRe ];
  prox_nui_list()  = ListFromFile[ file_NuProxIm ];

  skin_rhor[] = InterpolationLinear[$1]{ skin_rhor_list() };
  skin_rhoi[] = InterpolationLinear[$1]{ skin_rhoi_list() };

  prox_nur[]  = InterpolationLinear[$1]{ prox_nur_list() } ;
  prox_nui[]  = InterpolationLinear[$1]{ prox_nui_list() } ;


  If(Flag_HomogenisedModel==0)
    nu[Winding] = nu0 ;
  Else
    //Proximity effect
    nu[Winding] = nu0*Complex[prox_nur[Rr], prox_nui[Rr]*Fill*Rr^2/2];
  EndIf
  If(Flag_FD) // used if Flag_HomogenisedModel==1
    // Skin effect => complex impedance
    Zskin[] = 1/SymFactor*Complex[ skin_rhor[Rr]*Rdc, 2*Pi*Freq*skin_rhoi[Rr]*mu0*Len/(8*Pi*Fill)] ;
  EndIf
  //==================================================================
  // Auxiliary functions for post-processing
  nuOm[#{Air, Insulation}] = -nu[]*Complex[0.,1.];
  nuOm[#{Iron}] = -nu[$1]*Complex[0.,1.];
  nuOm[#{Winding}] = Complex[ Omega * Im[nu[]], -Re[nu[]] ];

  kkk[] =  SymFactor * skin_rhor[Rr] / Fill /SigmaCu ;
  //==================================================================

  // For time-domain homogenization
  // Prox effect
  For k In {1:ORDER}
    Ca~{k}[] = nu0 * Ca_Sq~{ORDER}~{k} ;
    Cb~{k}[] = SigmaCu * Fill * Rc^2/4 * Cb_Sq~{ORDER}~{k} ;
  EndFor
  For k In {2:ORDER}
    Cc~{k-1}[] = SigmaCu * Fill * Rc^2/4 * Cc_Sq~{ORDER}~{k-1} ;
  EndFor
  // Skin effect
  For k In {1:ORDER}
    Sa~{k}[] = Rdc * Sa_Sq~{ORDER}~{k} ;
    Sb~{k}[] = Rdc * SigmaCu * mu0* Rc^2/(8*Fill) * Sb_Sq~{ORDER}~{k} ;
  EndFor
  For k In {2:ORDER}
    Sc~{k-1}[] = Rdc * SigmaCu * mu0 * Rc^2/(8*Fill) * Sc_Sq~{ORDER}~{k-1} ;
  EndFor

  //==================================================================

  If(Flag_TD) // used if Flag_HomogenisedModel==1
    For k In {1:ORDER}
      Zskin~{k}[] = 1/SymFactor * Sa~{k}[];
      Lskin~{k}[] = 1/SymFactor * Sb~{k}[];
    EndFor
    For k In {2:ORDER}
      Lskin_p~{k-1}[] = 2*1/SymFactor * Sc~{k-1}[];
    EndFor

    Zskin[] = Zskin~{1}[];
    Lskin[] = Lskin~{1}[];
  EndIf

  DefineFunction[
    Resistance, Inductance, Capacitance
  ];

  Ns[] = (Flag_HomogenisedModel==0) ? 1 : NbrCond/SymFactor ;
  Sc[] =  SurfaceArea[]{iCOND};

  If (Flag_HomogenisedModel==1)
    // Accounting for eddy currents (homogenization)
    If(Flag_FD)
      Resistance[Zh~{1}] = Zskin[] ;
    EndIf

    If(Flag_TD)
      For k In {1:ORDER}
        Resistance[Zh~{k}] = Zskin~{k}[] ;
        Inductance[Lh~{k}] = Lskin~{k}[] ;
      EndFor
      For k In {2:ORDER}
        Inductance[Lhp~{k-1}] = Lskin_p~{k-1}[] ;
      EndFor
    EndIf
  EndIf


  // List of nodes related to circuit
  N1() = {1:nbturns};   // Node 1 for each turn
  N2() = {2:nbturns+1}; // Node 2 for each turn

}


Constraint {

  { Name MVP_2D ;
    Case {
      { Region OuterBoundary ; Type Assign ;  Value 0. ; }
    }
  }

  // Massive/stranded conductor constraints
  { Name Current_2D ;
    Case {
      If(Flag_Circuit==0)
        { Region Winding ; Value Val_EE; TimeFunction Fct_Src[] ; }
      EndIf
    }
  }

  { Name Voltage_2D ;
    Case{
    }
  }

  { Name Voltage_Cir ;
    Case {
      If(Flag_Circuit && Flag_ImposedVoltage)
        { Region Input ; Value Val_EE; TimeFunction Fct_Src[] ; }
      EndIf
    }
  }
  { Name Current_Cir ;
    Case {
      If(Flag_Circuit && !Flag_ImposedVoltage)
        { Region Input ; Value -Val_EE; TimeFunction Fct_Src[] ; }
      EndIf
    }
  }

  { Name ElectricalCircuit ; Type Network ;
    // Common to fine and homogenised models
    Case Circuit1 {
      If(Flag_HomogenisedModel==0)
        { Region Input ;  Branch {N1(0), N2(nbturns-1)} ; }
      EndIf
      If(Flag_HomogenisedModel==1 && Flag_FD)
          { Region Input  ; Branch {777, N2(nbturns-1)} ; }
          { Region Zh~{1} ; Branch {777, N1(0)}; } // Complex impedance: Zskin
      EndIf
      If(Flag_HomogenisedModel==1 && Flag_TD)
        { Region Input ; Branch {777, N2(nbturns-1)} ; }
        If(ORDER==1)
          { Region Zh~{1}; Branch {777, 800}; }
          { Region Lh~{1}; Branch {800, N1(0)}; }
        EndIf
        If(ORDER==2)
          { Region Zh~{1} ; Branch {777, 800}; }
          { Region Lh~{1} ; Branch {800, 801}; }

          { Region Lhp~{1}; Branch {801, N1(0)}; }

          { Region Zh~{2} ; Branch {801, 802}; }
          { Region Lh~{2} ; Branch {802, N1(0)}; }
        EndIf
        If(ORDER==3)
          { Region Zh~{1} ; Branch {777, 800}; }
          { Region Lh~{1} ; Branch {800, 801}; }

          { Region Lhp~{1}; Branch {801, N1(0)}; }

          { Region Zh~{2} ; Branch {801, 802}; }
          { Region Lh~{2} ; Branch {802, 803}; }

          { Region Lhp~{2}; Branch {803, N1(0)}; }

          { Region Zh~{3} ; Branch {803, 804}; }
          { Region Lh~{3} ; Branch {804, N1(0)}; }
        EndIf
        If(ORDER==4)
          { Region Zh~{1} ; Branch {777, 800}; }
          { Region Lh~{1} ; Branch {800, 801}; }

          { Region Lhp~{1}; Branch {801, N1(0)}; }

          { Region Zh~{2} ; Branch {801, 802}; }
          { Region Lh~{2} ; Branch {802, 803}; }

          { Region Lhp~{2}; Branch {803, N1(0)}; }

          { Region Zh~{3} ; Branch {803, 804}; }
          { Region Lh~{3} ; Branch {804, 805}; }

          { Region Lhp~{3}; Branch {805, N1(0)}; }

          { Region Zh~{4} ; Branch {805, 806}; }
          { Region Lh~{4} ; Branch {806, N1(0)}; }
        EndIf
      EndIf
      For k In {0:nbturns-1} // list indexes start at 0
        { Region Turn~{k+1} ; Branch {N1(k), N2(k)} ; }
      EndFor
    }
  }

}

//-----------------------------------------------------------------------------

Jacobian {
  { Name Vol ; Case { { Region All ; Jacobian VolAxiSqu ; } } }
  { Name Sur ; Case { { Region All ; Jacobian SurAxi ; } } }
}

Integration {
  { Name II ; Case {
      { Type Gauss ; Case {
          { GeoElement Triangle ;    NumberOfPoints 4 ; }
          { GeoElement Quadrangle  ; NumberOfPoints 4 ; }
        } }
    } }
}

//-----------------------------------------------------------------------------

FunctionSpace {

  { Name Hcurl_a_2D ; Type Form1P ; // Split for TD + homog: subspace isolating unknowns
    BasisFunction {
      { Name se ; NameOfCoef ae ; Function BF_PerpendicularEdge ;
        Support Domain ; Entity NodesOf[ All, Not Winding ] ; }
      { Name seh ; NameOfCoef aeh ; Function BF_PerpendicularEdge ;
        Support Domain ; Entity NodesOf[ Winding ] ; }
    }
    SubSpace {
      { Name aH ; NameOfBasisFunction {seh}; } // Subspace only used in TD homog
    }

    Constraint {
      { NameOfCoef ae  ; EntityType NodesOf ; NameOfConstraint MVP_2D ; }
     }
  }

  { Name Hregion_u_2D ; Type Form1P ; // Gradient of Electric scalar potential (2D)
    BasisFunction {
      { Name sr ; NameOfCoef ur ; Function BF_RegionZ ;
        Support DomainC ; Entity DomainC ; }
    }
    GlobalQuantity {
      { Name U ; Type AliasOf        ; NameOfCoef ur ; }
      { Name I ; Type AssociatedWith ; NameOfCoef ur ; }
    }
    Constraint {
      { NameOfCoef U ; EntityType Region ; NameOfConstraint Voltage_2D ; }
      { NameOfCoef I ; EntityType Region ; NameOfConstraint Current_2D ; }
    }
  }


  { Name Hregion_i_2D ; Type Vector ;
    BasisFunction {
      { Name sr ; NameOfCoef ir ; Function BF_RegionZ ;
        Support DomainS ; Entity DomainS ; }
    }
    GlobalQuantity {
      { Name Is ; Type AliasOf        ; NameOfCoef ir ; }
      { Name Us ; Type AssociatedWith ; NameOfCoef ir ; }
    }
    Constraint {
      { NameOfCoef Us ; EntityType Region ; NameOfConstraint Voltage_2D ; }
      { NameOfCoef Is ; EntityType Region ; NameOfConstraint Current_2D ; }
    }
  }


  // For circuit equations
  { Name Hregion_Z ; Type Scalar ;
    BasisFunction {
      { Name sr ; NameOfCoef ir ; Function BF_Region ;
        Support DomainZt_Cir ; Entity DomainZt_Cir ; }
    }
    GlobalQuantity {
      { Name Iz ; Type AliasOf        ; NameOfCoef ir ; }
      { Name Uz ; Type AssociatedWith ; NameOfCoef ir ; }
    }
    Constraint {
      { NameOfCoef Uz ; EntityType Region ; NameOfConstraint Voltage_Cir ; }
      { NameOfCoef Iz ; EntityType Region ; NameOfConstraint Current_Cir ; }
    }
  }

  // Time domain basis functions with homogenisation
  For k In {2:ORDER}
    { Name Hcurl_a~{k} ; Type Form1P ;
      BasisFunction {
        { Name se ; NameOfCoef ae ; Function BF_PerpendicularEdge ;
          Support Winding ; Entity NodesOf[ All ] ; }
      }
    }
  EndFor

}


Formulation {
  { Name MagDyn_a ; Type FemEquation ;
    Quantity {
       { Name a  ; Type Local  ; NameOfSpace Hcurl_a_2D ; }

       { Name ur ; Type Local  ; NameOfSpace Hregion_u_2D  ; }
       { Name I  ; Type Global ; NameOfSpace Hregion_u_2D[I] ; }
       { Name U  ; Type Global ; NameOfSpace Hregion_u_2D[U] ; }

       { Name ir ; Type Local  ; NameOfSpace Hregion_i_2D ; }
       { Name Us ; Type Global ; NameOfSpace Hregion_i_2D[Us] ; }
       { Name Is ; Type Global ; NameOfSpace Hregion_i_2D[Is] ; }

       { Name Uz ; Type Global ; NameOfSpace Hregion_Z [Uz] ; }
       { Name Iz ; Type Global ; NameOfSpace Hregion_Z [Iz] ; }
    }

    Equation {
      Galerkin { [ nu[] * Dof{d a} , {d a} ]  ;
        In Domain_Lin ; Jacobian Vol ; Integration II ; }
      If(Flag_NL)
        Galerkin { [ nu[{d a}] * Dof{d a} , {d a} ]  ;
          In Domain_NonLin ; Jacobian Vol ; Integration II ; }
        Galerkin { JacNL [ dhdb_NL[{d a}] * Dof{d a} , {d a} ] ;
          In Domain_NonLin ; Jacobian Vol ; Integration II ; }
      EndIf

      Galerkin { DtDof [ sigma[] * Dof{a} , {a} ] ;
        In DomainC ; Jacobian Vol ; Integration II ; }
      Galerkin { [ sigma[] * Dof{ur}/CoefGeo , {a} ] ;
        In DomainC ; Jacobian Vol ; Integration II ; }

      Galerkin { DtDof [ sigma[] * Dof{a} , {ur} ] ;
        In DomainC ; Jacobian Vol ; Integration II ; }
      Galerkin { [ sigma[] * Dof{ur}/CoefGeo , {ur} ] ;
        In DomainC ; Jacobian Vol ; Integration II ; }
      GlobalTerm { [ Dof{I}, {U} ] ; In DomainC ; }

      Galerkin { [ -Ns[]/Sc[] * Dof{ir}, {a} ] ;
        In DomainS ; Jacobian Vol ; Integration II ; }
      Galerkin { DtDof [ CoefGeo*Ns[]/Sc[] * Dof{a}, {ir} ] ;
        In DomainS ; Jacobian Vol ; Integration II ; }

      Galerkin { [ Ns[]/Sc[] / sigma[] * Ns[]/Sc[]* Dof{ir} , {ir} ] ; // resistance term
        In DomainS ; Jacobian Vol ; Integration II ; }
      GlobalTerm { [ Dof{Us}/CoefGeo , {Is} ] ; In DomainS ; }

      If(Flag_Circuit)
        GlobalTerm { NeverDt[ Dof{Uz}                , {Iz} ] ; In Resistance_Cir ; }
        GlobalTerm { NeverDt[ Resistance[] * Dof{Iz} , {Iz} ] ; In Resistance_Cir ; }

        GlobalTerm { [ Dof{Uz}                      , {Iz} ] ; In Inductance_Cir ; }
        GlobalTerm { DtDof [ Inductance[] * Dof{Iz} , {Iz} ] ; In Inductance_Cir ; }

        GlobalTerm { [ Dof{Iz}        , {Iz} ] ; In Capacitance1_Cir ; }
        GlobalTerm { NeverDt[ Dof{Iz} , {Iz} ] ; In Capacitance2_Cir ; }
        GlobalTerm { DtDof [ Capacitance[] * Dof{Uz} , {Iz} ] ; In Capacitance_Cir ; }

        GlobalEquation {
          Type Network ; NameOfConstraint ElectricalCircuit ;
          { Node {I};  Loop {U};  Equation {I};  In DomainC ; }
          { Node {Is}; Loop {Us}; Equation {Us}; In DomainS ; }
          { Node {Iz}; Loop {Uz}; Equation {Uz}; In DomainZt_Cir ; }
        }
      EndIf
    }
  }

  { Name MagDyn_a_Homog ; Type FemEquation ;
    Quantity {
      { Name a  ; Type Local  ; NameOfSpace Hcurl_a_2D ; }

      { Name ir ; Type Local  ; NameOfSpace Hregion_i_2D ; }
      { Name Us ; Type Global ; NameOfSpace Hregion_i_2D[Us] ; }
      { Name Is ; Type Global ; NameOfSpace Hregion_i_2D[Is] ; }

      { Name Uz ; Type Global ; NameOfSpace Hregion_Z [Uz] ; }
      { Name Iz ; Type Global ; NameOfSpace Hregion_Z [Iz] ; }
    }

    Equation {

      Galerkin { [ nu[] * Dof{d a} , {d a} ]  ;
        In Domain_Lin ; Jacobian Vol ; Integration II ; }

      If(Flag_NL)
        Galerkin { [ nu[{d a}] * Dof{d a} , {d a} ]  ;
          In Domain_NonLin ; Jacobian Vol ; Integration II ; }
        Galerkin { JacNL [ dhdb_NL[{d a}] * Dof{d a} , {d a} ] ;
          In Domain_NonLin ; Jacobian Vol ; Integration II ; }
      EndIf

      Galerkin { [ -1/AreaCell * Dof{ir}, {a} ] ;
        In DomainS ; Jacobian Vol ; Integration II ; }
      Galerkin { DtDof [ 1/AreaCell * Dof{a}, {ir} ] ;
        In DomainS ; Jacobian Vol ; Integration II ; }
      GlobalTerm { [ Dof{Us}/CoefGeo, {Is} ]     ; In DomainS ; }

      // Circuit equations
      If(Flag_Circuit)
        GlobalTerm { NeverDt[ Dof{Uz}                , {Iz} ] ; In Resistance_Cir ; }
        GlobalTerm { NeverDt[ Resistance[] * Dof{Iz} , {Iz} ] ; In Resistance_Cir ; }

        GlobalTerm { [ Dof{Uz}                      , {Iz} ] ; In Inductance_Cir ; }
        GlobalTerm { DtDof [ Inductance[] * Dof{Iz} , {Iz} ] ; In Inductance_Cir ; }

        GlobalTerm { [ Dof{Iz}        , {Iz} ] ; In Capacitance1_Cir ; }
        GlobalTerm { NeverDt[ Dof{Iz} , {Iz} ] ; In Capacitance2_Cir ; }
        GlobalTerm { DtDof [ Capacitance[] * Dof{Uz} , {Iz} ] ; In Capacitance_Cir ; }

        GlobalEquation {
          Type Network ; NameOfConstraint ElectricalCircuit ;
          { Node {Is};  Loop {Us};  Equation {Us}; In DomainS ; }
          { Node {Iz};  Loop {Uz};  Equation {Uz}; In DomainZt_Cir ; }
        }
      EndIf
    }
  }

  { Name MagDyn_a_Homog_Time ; Type FemEquation ;
    Quantity {
      { Name a    ; Type Local  ; NameOfSpace Hcurl_a_2D ; }
      { Name a_1  ; Type LocalQuantity ; NameOfSpace Hcurl_a_2D[aH] ; }

      For i In {2:ORDER}
        { Name a~{i}  ; Type Local ; NameOfSpace Hcurl_a~{i} ; }
      EndFor

      { Name ir ; Type Local  ; NameOfSpace Hregion_i_2D ; }
      { Name Us ; Type Global ; NameOfSpace Hregion_i_2D[Us] ; }
      { Name Is ; Type Global ; NameOfSpace Hregion_i_2D[Is] ; }

      { Name Uz ; Type Global ; NameOfSpace Hregion_Z [Uz] ; }
      { Name Iz ; Type Global ; NameOfSpace Hregion_Z [Iz] ; }
    }

    Equation {

      Galerkin { [ nu[] * Dof{d a} , {d a} ]  ;
        In Domain_Lin_NoJs ; Jacobian Vol ; Integration II ; }

      // Proximity effect
      For i In {1:ORDER}
        Galerkin { [ Ca~{i}[] * Dof{d a~{i} } , {d a~{i}} ]  ;
          In DomainS; Jacobian Vol; Integration II ; }
        Galerkin { DtDof [ Cb~{i}[] * Dof{d a~{i}} , {d a~{i} } ] ;
          In DomainS; Jacobian Vol; Integration II ; }
      EndFor
      For i In {2:ORDER}
        Galerkin { DtDof [ Cc~{i-1}[] * Dof{d a~{i-1}} , {d a~{i}} ] ;
          In DomainS ; Jacobian Vol ; Integration II ; }
        Galerkin { DtDof [ Cc~{i-1}[] * Dof{d a~{i}} , {d a~{i-1}} ] ;
          In DomainS ; Jacobian Vol ; Integration II ; }
      EndFor

      If(Flag_NL)
        Galerkin { [ nu[{d a}] * Dof{d a} , {d a} ]  ;
          In Domain_NonLin ; Jacobian Vol ; Integration II ; }
        Galerkin { JacNL [ dhdb_NL[{d a}] * Dof{d a} , {d a} ] ;
          In Domain_NonLin ; Jacobian Vol ; Integration II ; }
      EndIf

      Galerkin { [ -1/AreaCell * Dof{ir}, {a} ] ;
        In DomainS ; Jacobian Vol ; Integration II ; }
      Galerkin { DtDof [ 1/AreaCell * Dof{a}, {ir} ] ;
        In DomainS ; Jacobian Vol ; Integration II ; }
      GlobalTerm { [ Dof{Us}/CoefGeo, {Is} ]     ; In DomainS ; }

      // Circuit equations
      If(Flag_Circuit)
        GlobalTerm { NeverDt[ Dof{Uz}                , {Iz} ] ; In Resistance_Cir ; }
        GlobalTerm { NeverDt[ Resistance[] * Dof{Iz} , {Iz} ] ; In Resistance_Cir ; }

        GlobalTerm { [ Dof{Uz}                      , {Iz} ] ; In Inductance_Cir ; }
        GlobalTerm { DtDof [ Inductance[] * Dof{Iz} , {Iz} ] ; In Inductance_Cir ; }

        GlobalTerm { [ Dof{Iz}        , {Iz} ] ; In Capacitance1_Cir ; }
        GlobalTerm { NeverDt[ Dof{Iz} , {Iz} ] ; In Capacitance2_Cir ; }
        GlobalTerm { DtDof [ Capacitance[] * Dof{Uz} , {Iz} ] ; In Capacitance_Cir ; }

        GlobalEquation {
          Type Network ; NameOfConstraint ElectricalCircuit ;
          { Node {Is};  Loop {Us};  Equation {Us}; In DomainS ; }
          { Node {Iz};  Loop {Uz};  Equation {Uz}; In DomainZt_Cir ; }
        }
      EndIf
    }
  }


}


Resolution {

  { Name Analysis ;
    System {
      If(Flag_HomogenisedModel==0)
        If(Flag_FD) // Frequency domain
          { Name A ; NameOfFormulation MagDyn_a ; Type ComplexValue ; Frequency Freq ; }
        Else // Time domain
          { Name A ; NameOfFormulation MagDyn_a ; }
        EndIf
      EndIf
      If(Flag_HomogenisedModel==1)
        If(Flag_FD) // Frequency domain
          { Name A ; NameOfFormulation MagDyn_a_Homog ; Type ComplexValue ; Frequency Freq ; }
        Else // Time domain
          { Name A ; NameOfFormulation MagDyn_a_Homog_Time ; }
        EndIf
      EndIf
    }
    Operation {
      CreateDir[DirRes];
      InitSolution[A]; SaveSolution[A];

      If(Flag_FD) // Frequency domain
        Generate[A] ; Solve[A] ; SaveSolution[A] ;
        If(Flag_HomogenisedModel==0)
          PostOperation [Map_local];
          PostOperation [Get_global];
        Else
          PostOperation [Map_local_Homog];
          PostOperation [Get_global_Homog];
        EndIf

      Else //Fine reference in time domain
        TimeLoopTheta[tinit, tend, deltat, thetav]{
          If(!Flag_NL)
            Generate[A] ; Solve[A] ;
          Else
            IterativeLoop[Nb_max_iter, stop_criterion, relaxation_factor] {
              GenerateJac[A]; SolveJac[A]; }
          EndIf
            SaveSolution[A] ;

            Test[ GetNumberRunTime[visu]{StrCat[mfem,"Visu/Real-time visualization"]} ]{
              If(Flag_HomogenisedModel==0)
                PostOperation[Map_local];
              Else
                PostOperation[Map_local_Homog_Time];
              EndIf
            }
          }
      EndIf
    }
  }


}// Resolution

PostProcessing {

  { Name MagDyn_a ; NameOfFormulation MagDyn_a ; NameOfSystem A;
    PostQuantity {
      { Name a ; Value { Term { [ {a} ] ; In Domain ; Jacobian Vol  ;} } }
      { Name az ; Value { Term { [ CompZ[{a}] ] ; In Domain ; Jacobian Vol  ;} } }
      { Name raz ; Value { Term { [ CompZ[{a}]*X[] ] ; In Domain ; Jacobian Vol  ;} } }

      { Name b ; Value { Term { [ {d a} ] ; In Domain ; Jacobian Vol ; } } }
      { Name h ; Value { Term { [ nu[{d a}]*{d a} ] ; In Domain ; Jacobian Vol ; } } }
      { Name j ; Value { Term { [ -sigma[]*(Dt[{a}]+{ur}/CoefGeo) ] ; In DomainC ; Jacobian Vol ; } } }
      { Name jz ; Value {
          Term { [ CompZ[ -sigma[]*(Dt[{a}]+{ur}/CoefGeo) ] ] ; In DomainC ; Jacobian Vol ; }
          Term { [ CompZ[ {ir}/AreaCond ] ] ; In DomainS ; Jacobian Vol ; }
        } }

      { Name b2av ; Value { Integral { [ CoefGeo*{d a}/AreaCell ] ;
            In Domain ; Jacobian Vol  ; Integration II ; } } }


      { Name j2F ; Value { Integral { // Joue losses
            [ CoefGeo*sigma[]*SquNorm[(Dt[{a}]+{ur}/CoefGeo)] ] ;
            In DomainC ; Jacobian Vol ; Integration II ; } } }

      { Name b2F ; Value { Integral { // Magnetic Energy
            [ CoefGeo*nu[{d a}]*SquNorm[{d a}] ] ;
            In Domain ; Jacobian Vol ; Integration II ; } } }

      { Name SoF ; Value {
          Integral {
            [ CoefGeo * Complex[ sigma[] * SquNorm[(Dt[{a}]+{ur}/CoefGeo)], nu[]*SquNorm[{d a}] ] ] ;
            In Domain ; Jacobian Vol ; Integration II ; }
        } }//Complex power

      { Name U ; Value {
          Term { [ {U} ]   ; In DomainC ; }
          Term { [ {Us} ]   ; In DomainS ; }
          Term { [ {Uz} ]  ; In DomainZt_Cir ; }
        } }
      { Name I ; Value {
          Term { [ {I} ]   ; In DomainC ; }
          Term { [ {Is} ]   ; In DomainS ; }
          Term { [ {Iz} ]  ; In DomainZt_Cir ; }
        } }


      { Name MagEnergy ; Value {
          Integral { [ CoefGeo*nu[{d a}]*({d a}*{d a})/2 ] ;
            In Domain ; Jacobian Vol ; Integration II ; } } }
     }
  }


  { Name MagDyn_a_Homog ; NameOfFormulation MagDyn_a_Homog ;
    PostQuantity {
      { Name a   ; Value { Term { [ {a} ] ; In Domain ; Jacobian Vol  ;} } }
      { Name az  ; Value { Term { [ CompZ[{a}] ] ; In Domain ; Jacobian Vol  ;} } }
      { Name raz ; Value { Term { [ CompZ[{a}]*X[] ] ; In Domain ; Jacobian Vol  ;} } }
      { Name b ; Value { Term { [ {d a} ] ; In Domain ; Jacobian Vol ; } } }

      { Name j  ; Value { Term { [ -1/AreaCell*{ir} ] ; In DomainS ; Jacobian Vol ; } } }
      { Name jz ; Value { Term { [ CompZ[ -1/AreaCell*{ir} ] ] ; In DomainS ; Jacobian Vol ; } } }


      { Name j2H ; Value { Integral { // Joule losses
            [ CoefGeo*(Re[{d a}*Conj[nuOm[]*{d a}]]+kkk[]*SquNorm[-1/AreaCell*{ir}]) ] ;
            In DomainS ; Jacobian Vol ; Integration II ; } } }

      { Name SoH ; Value { Integral { // Complex power = Active power +j * Reactive power => S = P+j*Q
            [ CoefGeo * ({d a}*Conj[nuOm[{d a}]*{d a}] + kkk[]*SquNorm[-1/AreaCell*{ir}]) ] ;
            In Domain ; Jacobian Vol ; Integration II ; } } } //Complex power

      { Name U ; Value {
          Term { [ {Us} ]  ; In DomainS ; }
          Term { [ {Uz} ]  ; In DomainZt_Cir ; }
        } }
      { Name I ; Value {
          Term { [ {Is} ]  ; In DomainS ; }
          Term { [ {Iz} ]  ; In DomainZt_Cir ; }
        } }

      { Name conjI ; Value {
          Term { [ Conj[{Is}] ]  ; In DomainS ; }
          Term { [ Conj[{Iz}] ]  ; In DomainZt_Cir ; }
        } }
      { Name squnormI ; Value {
          Term { [ SquNorm[{Is}] ]  ; In DomainS ; }
          Term { [ SquNorm[{Iz}] ]  ; In DomainZt_Cir ; }
        } }
      { Name reI ; Value {
          Term { [ Re[{Is}] ]  ; In DomainS ; }
          Term { [ Re[{Iz}] ]  ; In DomainZt_Cir ; }
        } }
      { Name imI ; Value {
          Term { [ Im[{Is}] ]  ; In DomainS ; }
          Term { [ Im[{Iz}] ]  ; In DomainZt_Cir ; }
        } }

    }
  }

  { Name MagDyn_a_Homog_Time ; NameOfFormulation MagDyn_a_Homog_Time ;
    PostQuantity {
      { Name a  ; Value { Term { [ {a} ]           ; In Domain ; Jacobian Vol; } } }
      { Name az ; Value { Term { [ CompZ[{a}] ]    ; In Domain ; Jacobian Vol; } } }
      { Name raz; Value { Term { [ CompZ[{a}]*X[]] ; In Domain ; Jacobian Vol; } } }
      For i In {1:ORDER}
        { Name a~{i}  ; Value { Term { [ {a~{i}} ]        ; In DomainS ; Jacobian Vol; } } }
        { Name az~{i} ; Value { Term { [ CompZ[{a~{i}}] ] ; In DomainS ; Jacobian Vol; } } }
        { Name raz~{i}; Value { Term { [ CompZ[{a~{i}}]*X[] ] ; In DomainS ; Jacobian Vol; } } }
      EndFor

      { Name b ; Value { Term { [ {d a} ] ; In Domain ; Jacobian Vol ; } } }
      { Name j  ; Value { Term { [ -1/AreaCell*{ir} ] ; In DomainS ; Jacobian Vol ; } } }
      { Name jz ; Value { Term { [ CompZ[ -1/AreaCell*{ir} ] ] ; In DomainS ; Jacobian Vol ; } } }


      { Name jI ; Value { Term { [ Rdc*{Is}^2 ]  ; In DomainS ; } } }
      { Name j2HH ; Value { // Joule Losses -- total proximity losses
          For i In {1:ORDER}
            Integral { [ CoefGeo * Cb~{i}[] * Dt[{d a~{i}}] * Dt[{d a~{i}}] ];
              In Winding ; Jacobian Vol ; Integration II ; }
          EndFor
          For i In {1:ORDER-1}
            Integral { [ CoefGeo * 2 * Cc~{i}[] * Dt[{d a~{i}}] * Dt[{d a~{i+1}}] ];
              In Winding ; Jacobian Vol ; Integration II ; }
          EndFor
        }
      }

      { Name j2HH_skin ; Value { // Joule Losses -- total skin losses
          If(Flag_FD)
              Term { [ SymFactor*Zskin[] * SquNorm[{Iz}] ]; In Zh~{1}; }
          EndIf
          If(Flag_TD)
            For k In {1:ORDER}
              Term { [ SymFactor*Zskin~{k}[] * SquNorm[{Iz}] ]; In Zh~{k}; }
            EndFor
          EndIf
        }
      }

      { Name j2F ; Value { // Joule losses
          Integral { [ CoefGeo *SquNorm[1/AreaCell*{ir}]/(Fill*sigma[]) ] ;
            In Winding ; Jacobian Vol ; Integration II ; }
        } }

      { Name MagEnergy ; Value {
          For i In {1:ORDER}
            Integral { [ CoefGeo * nu0/2 * {d a~{i}}*{d a~{i}} ] ;
              In Winding ; Jacobian Vol ; Integration II ; }
          EndFor
        }
      }

      { Name U ; Value {
          Term { [ {Us} ]  ; In DomainS ; }
          Term { [ {Uz} ]  ; In DomainZt_Cir ; }
        } }
      { Name I ; Value {
          Term { [ {Is} ]  ; In DomainS ; }
          Term { [ {Iz} ]  ; In DomainZt_Cir ; }
        } }


    }
  }

}

//---------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------

PostOperation Map_local UsingPost MagDyn_a {
  Print[ jz, OnElementsOf Region[{DomainC,DomainS}], File StrCat[DirRes, "jz", ExtGmsh], LastTimeStepOnly ] ;
  Print[ b,  OnElementsOf Domain,  File StrCat[DirRes, "b", ExtGmsh],  LastTimeStepOnly ] ;
  Print[ raz,OnElementsOf Domain,  File StrCat[DirRes, "a", ExtGmsh], LastTimeStepOnly ] ;

  Echo[ Str["View[PostProcessing.NbViews-1].Light=0;
             View[PostProcessing.NbViews-1].LineWidth = 2;
             View[PostProcessing.NbViews-1].RangeType=3;
             View[PostProcessing.NbViews-1].IntervalsType=1;
             View[PostProcessing.NbViews-1].NbIso = 25;"],
           File "res/option.pos"];
  // RangeType = 1; // Value scale range type (1=default, 2=custom, 3=per time step)
  // IntervalsType = 2; // Type of interval display (1=iso, 2=continuous, 3=discrete, 4=numeric)

  If(!Flag_Circuit)
    Print[ I, OnRegion Winding, Format TimeTable, >File Sprintf("res/I_f%g.dat", Freq),
      SendToServer StrCat[po,"I [A]"], Color "Pink", LastTimeStepOnly];
    Print[ U, OnRegion Winding, Format TimeTable, >File Sprintf("res/U_f%g.dat", Freq),
      SendToServer StrCat[po,"V [V]"], Color "LightGreen", LastTimeStepOnly];
  Else
    Print[ I, OnRegion Input, Format TimeTable, File >Sprintf("res/I_f%g.dat", Freq),
      SendToServer StrCat[po,"I [A]"], Color "Pink", LastTimeStepOnly];
    Print[ U, OnRegion Input, Format TimeTable, File >Sprintf("res/U_f%g.dat", Freq),
      SendToServer StrCat[po,"V [V]"], Color "LightGreen", LastTimeStepOnly];
  EndIf
}

PostOperation Get_global UsingPost MagDyn_a {
  Print[ j2F[ Winding ], OnGlobal, Format TimeTable, File > Sprintf("res/j2F_iron%g.dat", Flag_IronCore)] ;// Joule losses
  Print[ SoF[ Domain ], OnGlobal, Format TimeTable,  File > Sprintf("res/SF_iron%g.dat", Flag_IronCore)] ; // Complex power
}


PostOperation Get_allTS UsingPost MagDyn_a {
  If(!Flag_Circuit)
    Print[ I, OnRegion Winding, Format TimeTable, File Sprintf("res/I_f%g.dat", Freq) ];
    Print[ U, OnRegion Winding, Format TimeTable, File Sprintf("res/U_f%g.dat", Freq) ];
  Else
    Print[ I, OnRegion Input, Format TimeTable, File Sprintf("res/I_f%g.dat", Freq),
      SendToServer StrCat[po,"I [A]"]{0}, Color "Pink"];
    Print[ U, OnRegion Input, Format TimeTable, File Sprintf("res/U_f%g.dat", Freq),
      SendToServer StrCat[po,"V [V]"]{0}, Color "LightGreen"];
    Print[ I, OnRegion Turn~{nbturns} , Format TimeTable, File Sprintf("res/Iw_f%g.dat", Freq) ];
  EndIf

  Print[ j2F[Winding], OnGlobal, Format TimeTable, File Sprintf("res/jl_f%g.dat", Freq) ] ; // Joule losses
}



//---------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------

PostOperation Map_local_Homog UsingPost MagDyn_a_Homog {
  // Print[ jz,   OnElementsOf DomainS, File StrCat[DirRes,"jH",ExtGmsh] ] ;
  Print[ b,   OnElementsOf Domain, File StrCat[DirRes,"bH",ExtGmsh] ] ;
  Print[ raz, OnElementsOf Domain, File StrCat[DirRes,"aH",ExtGmsh] ] ;
  Echo[ Str["View[PostProcessing.NbViews-1].Light=0;
             View[PostProcessing.NbViews-1].LineWidth = 2;
             View[PostProcessing.NbViews-1].RangeType=3;
             View[PostProcessing.NbViews-1].IntervalsType=1;
             View[PostProcessing.NbViews-1].NbIso = 25;"],
           File "res/option.pos"];
}

PostOperation Get_global_Homog UsingPost MagDyn_a_Homog {
  // Complex power: S = P + i*Q, P=active power, Q=reactive power

  Print[ SoH[Domain], OnGlobal, Format TimeTable,
    File Sprintf("res/SH_f%g.dat", Freq) ] ;
  Print[ j2H[Winding], OnGlobal, Format Table,
    File Sprintf("res/j2H_f%g.dat", Freq) ] ;

  If(!Flag_Circuit)
    Print[ U, OnRegion Winding, Format Table, File Sprintf("res/U_f%g.dat", Freq) ] ;
    Print[ I, OnRegion Winding, Format Table, File Sprintf("res/I_f%g.dat", Freq) ] ;
  Else
    Print[ U, OnRegion Input, Format Table, File Sprintf("res/U_f%g.dat", Freq) ];
    Print[ I, OnRegion Input, Format Table, File Sprintf("res/I_f%g.dat", Freq) ];
  EndIf

}

PostOperation Map_local_Homog_Time UsingPost MagDyn_a_Homog_Time {
  Print[ j,   OnElementsOf DomainS, File StrCat[DirRes,"jH_time",ExtGmsh], LastTimeStepOnly ] ;
  Print[ b,   OnElementsOf Domain, File StrCat[DirRes,"bH_time",ExtGmsh], LastTimeStepOnly ] ;
  Print[ raz, OnElementsOf Domain, File StrCat[DirRes,"aH_time",ExtGmsh], LastTimeStepOnly ] ;
  Echo[ Str["View[PostProcessing.NbViews-1].Light=0;
             View[PostProcessing.NbViews-1].LineWidth = 2;
             View[PostProcessing.NbViews-1].RangeType=3;
             View[PostProcessing.NbViews-1].IntervalsType=1;
             View[PostProcessing.NbViews-1].NbIso = 25;"],
           File "res/option.pos"];
}

PostOperation Get_global_Homog_Time UsingPost MagDyn_a_Homog_Time {
  Print[ j2HH[Winding], OnGlobal, Format Table, LastTimeStepOnly,
    File Sprintf("res/j2H_time_f%g.dat", Freq) ] ;
  Print[ MagEnergy[Winding], OnGlobal, Format Table, LastTimeStepOnly,
    File Sprintf("res/ME_time_f%g.dat", Freq) ] ;

  If(!Flag_Circuit)
    Print[ U, OnRegion Winding, Format Table, File Sprintf("res/Uh_time_f%g.dat", Freq), LastTimeStepOnly ] ;
    Print[ I, OnRegion Winding, Format Table, File Sprintf("res/Ih_time_f%g.dat", Freq), LastTimeStepOnly ] ;
  Else
    Print[ U, OnRegion Input, Format Table, File Sprintf("res/Uh_time_f%g.dat", Freq), LastTimeStepOnly ];
    Print[ I, OnRegion Input, Format Table, File Sprintf("res/Ih_time_f%g.dat", Freq), LastTimeStepOnly ];
  EndIf

}


PostOperation Get_allTS_Homog_Time UsingPost MagDyn_a_Homog_Time {
  If(!Flag_Circuit)
    Print[ I, OnRegion Winding, Format TimeTable, File Sprintf("res/Ih_time_f%g_o%g.dat", Freq, ORDER) ];
    Print[ U, OnRegion Winding, Format TimeTable, File Sprintf("res/Uh_time_f%g_o%g.dat", Freq, ORDER) ];
  Else
    Print[ I, OnRegion Input, Format TimeTable, File Sprintf("res/Ih_time_f%g_o%g.dat", Freq, ORDER),
      SendToServer StrCat[po,"I [A]"]{0}, Color "Pink"];
    Print[ U, OnRegion Input, Format TimeTable, File Sprintf("res/Uh_time_f%g_o%g.dat", Freq, ORDER),
      SendToServer StrCat[po,"V [V]"]{0}, Color "LightGreen"];
  EndIf

  Print[ j2HH[Winding], OnGlobal, Format TimeTable, File Sprintf("res/jlh_time_f%g_o%g.dat", Freq, ORDER) ] ; // Joule losses (proximity)
}



// This is only for Onelab
DefineConstant[
  R_ = {"Analysis", Name "GetDP/1ResolutionChoices", Visible 1, Closed 1}
  C_ = {"-solve -v 4 -v2 -bin", Name "GetDP/9ComputeCommand", Visible 1}
  P_ = {"", Name "GetDP/2PostOperationChoices", Visible 1}
];
