
2D model for the homogenisation of windings.
Models developed by R. V. Sabariego and J. Gyselinck.


Quick start
-----------

Open 'ind_axi.pro' with Gmsh.

Additional info
---------------

The directory contains:
* The FE elementary model required for generating the coefficient files (frequency domain) in the directory 'coeff'.
You may open 'cell.pro' with Gmsh and choose e.g. square or hexagonal packing.
cell.pro
cell.geo
cell_dat.pro
cell_hexa_circ.geo
cell_square_circ.geo

The default case is square packing with a fill factor of 0.65.
The time domain coefficients can be obtained by least-squared matrix fitting or
synthesising an RL-Cauer ladder network e.g. in Matlab or Python.

* An axisymmetric inductor model with 120 turns:
ind_axi.pro
ind_axi.geo
ind_axi_dat.pro


The theory behind these files can be found in the following publications:

Frequency domain:
----------------
Gyselinck, J., Dular, P. Homogenisation of bundles of wires of arbitrary cross-section taking into
  account skin and proximity effect and their coupling. Proceedings of the Fifth International Conference
  on Computation in Electromagnetics (2004).

Gyselinck, J., Dular, P.. Frequency-domain homogenization of bundles of wires in 2-D magnetodynamic FE calculations.
  IEEE transactions on magnetics 41.5 (2005):1416-1419.


Time domain:
------------
Gyselinck, J., Sabariego, R. V., Dular, P. Time-domain homogenization of windings in 2-D finite element models.
  IEEE transactions on magnetics 43.4 (2007):1297-1300.

Sabariego, R. V., Dular, P., Gyselinck, Johan. Time-domain homogenization of windings in 3-D finite element models.
  IEEE transactions on magnetics, 44(6), 1302-1305, (2008).

  Gyselinck, J., Dular, P., Geuzaine, C., & Sabariego, R. V.
  Direct inclusion of proximity-effect losses in two-dimensional time-domain finite-element simulation of
  electrical machines. In Proceedings of the 8th International Symposium on Electric and Magnetic Fields
  (EMF2009). Mondovi, Italy.

Gyselinck, J., Dular, P., Sadowski, N., Kuo-Peng, P., & Sabariego, R. V. Homogenization of form-wound windings
  in frequency and time domain finite-element modeling of electrical machines.
  IEEE transactions on magnetics, 46(8), 2852-2855, (2010).

Niyomsatian, K., Van den Keybus, J., Sabariego, R., & Gyselinck, J. (2016).
  Time-Domain Homogenization of Litz-Wire Bundles in 2-D FE Calculations.
  In 8th IEEE Energy Conversion Congress and Exposition.

Niyomsatian, K., Van den Keybus, J., Sabariego, R. V. and Gyselinck, J..
  Time-domain homogenization of litz-wire bundles in FE calculations.
  In Proceedings of the 8th Annual IEEE Energy Conversion Congress & Exposition (ECCE2016),
  Milwaukee, WI, USA, September 18–22 2016.

K. Niyomsatian, J. Gyselinck, and R. V. Sabariego. Time-domain homogenization of multiturn windings
  based on RL Cauer ladder networks. International Journal of Numerical Modelling: Electronic Networks,
  Devices and Fields, pages 1-13, e2649, 2019.


Harmonic Balance/Multiharmonics:
-------------------------------
R. V. Sabariego and J. Gyselinck. Eddy-current-effect homogenization of windings in harmonic balance
  finite element models. IEEE Transactions on Magnetics, 53(6):1–4, Art. No.: 7206304, June 2017.

R. V. Sabariego, K. Niyomsatian, and J. Gyselinck.
  Eddy-current-effect homogenization of windings in harmonic-balance finite-element models
  coupled to nonlinear circuits.IEEE Transactions on Magnetics, 54(3):1–4, Art. No.: 7205004, March 2018.
