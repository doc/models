#!/bin/sh

homog=1
getmesh=0

if [ $homog = 0 ]; then
    mymesh="fine.msh"; nn=1; else
    mymesh="homog.msh"; nn=3;
fi
if [ $getmesh  = 1 ]; then
    gmsh lam2d.geo -nt 4 -v 4 -3 -setnumber Flag_HomogType $homog -setnumber Flag_Symmetry 2 -o $mymesh
fi

ff=5000
ordermin=1 ordermax=$nn;
law=3 # 0 linear; 1 3kw machine; 2 anhysteretic law; 3 hysteretic law
nbt=20 # number of periods
nbs=240 # number of steps per period

runthis=1
if [ $runthis = 1 ]; then
for (( i=$ordermin ; i<=$ordermax ; i++ )) ; do
    echo "====================== ORDER $i ==========================" ;
    getdp -v 3 -cpu lam2d.pro -msh $mymesh \
          -setnumber visu 0 \
          -setnumber Flag_HomogType $homog \
          -setnumber ORDER $i \
          -setnumber Flag_Symmetry 2 \
          -setnumber Flag_AnalysisType 1 \
          -setnumber Flag_NL 1 \
          -setnumber Flag_NonlinearLawType $law \
          -setnumber Freq $ff \
          -setnumber NbT $nbt \
          -setnumber NbSteps $nbs \
          -sol Analysis
done
fi


# frequency loop for losses figure as function of frequency

fmin=5
fmax=5000
nbf=20

runthis=0
if [ $runthis = 1 ]; then
    for (( k=1 ; k<=$nbf ; k++ )) ; do
        echo "e(l($fmin)+($k-1)*l($fmax/$fmin)/($nbf-1))" | bc -l
        fk=`echo "e(l($fmin)+($k-1)*l($fmax/$fmin)/($nbf-1))" | bc -l`
        echo "======================> $k freq $fk <==========================" ;
        for (( i=$ordermin ; i<=$ordermax ; i++ )) ; do
            echo "====================== ORDER $i ==========================" ;
            getdp -v 3 -cpu lam2d.pro -msh $mymesh \
                  -setnumber visu 0 \
                  -setnumber Flag_HomogType $homog \
                  -setnumber ORDER $i \
                  -setnumber Flag_Symmetry 2 \
                  -setnumber Flag_AnalysisType 1 \
                  -setnumber Flag_NL 1 \
                  -setnumber Flag_NonlinearLawType $law \
                  -setnumber Freq $fk \
                  -setnumber NbT $nbt \
                  -setnumber NbSteps $nbs \
                  -sol Analysis
        done
    done
fi
