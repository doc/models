
Include "BH.pro"; // nonlinear materials

DefineConstant[
  // Symmetry
  AxialLength = 1,
  SymmetryFactor = 1,
  // Simulation parameters
  visu = {1, Choices{0, 1}, AutoCheck 0,
    Name StrCat[mfem,"Visu/Real-time maps"], Highlight "LawnGreen"}
  Clean_Results = {0, Choices {0,1},
    Name StrCat[mfem,"000Remove previous result files"],  Highlight "Red"},

  // Nonlinear iterations
  Nb_max_iter = {50, Name StrCat[mfem,"8Nonlinear solver/Max. num. iterations"], Visible Flag_NL, Highlight "AliceBlue", Closed}
  relaxation_factor = {1., Name StrCat[mfem,"8Nonlinear solver/Relaxation factor"], Visible Flag_NL, Highlight "AliceBlue"}
  stop_criterion = {1e-4, Name StrCat[mfem,"8Nonlinear solver/Stopping criterion"], Visible Flag_NL, Highlight "AliceBlue"}

  // time loop, defaut values
  time0      = {0, Name StrCat[mfem,"7Time solver/00initial instant"], Visible Flag_AnalysisType==1, Closed}
  NbT        = {1, Name StrCat[mfem,"7Time solver/02nbr of periods"],  Visible Flag_AnalysisType==1, Highlight "AliceBlue"}
  timemax    = {NbT*T, Name StrCat[mfem,"7Time solver/01final instant"], ReadOnly, Visible Flag_AnalysisType==1, Highlight "LightGrey"}
  NbSteps    = {2*120, Name StrCat[mfem,"7Time solver/03steps per period"], Visible Flag_AnalysisType==1, Highlight "AliceBlue"}
  delta_time = {T/NbSteps, Name StrCat[mfem,"7Time solver/04step or deltatime"], ReadOnly, Visible Flag_AnalysisType==1, Highlight "LightGrey"}

  // ResDir = "resFloop/",
  ResDir = "res_cost/",
  ExtGmsh = ".pos",
  ExtGnuplot = ".dat"

  R_ = {"Analysis", Name "GetDP/1ResolutionChoices", Visible 1}
  C_ = {"-solve -v 3 -v2", Name "GetDP/9ComputeCommand", Visible 1}
  P_ = {"", Name "GetDP/2PostOperationChoices", Visible 1}
];

Group{
  DefineGroup[DomainLam, DomainLamCC, DomainLamC];
}


Function {

  mu0 = 4.e-7 * Pi ;
  nu0 = 1./mu0;

  mu_fe = mu0*mur_fe ;
  nu_fe = nu0/mur_fe ;

  nu[ #{Air,Ind} ]  = 1./mu0 ;
  fillfactor = (Flag_HomogType==0) ? 1.:fillfactor;

  Printf("===> Checking fillfactor %g", fillfactor);

  sigma [ #{Ind} ]  = 5.9e7 ;
  sigma [ #{Iron} ] = sigma_fe ;
  rho[] = 1/sigma[] ;

  nu_lin[] = nu_fe/fillfactor;

  If(!NbrRegions[Domain_NL]) // Linear case
    If(!Flag_ComplexReluctivity)
      Printf("===> using LINEAR law");
      nu[#{Iron}] = nu_lin[] ;
      dhdb_NL[ #{Iron} ]   = dhdb_lin_NL[$1] ;
    EndIf
  Else
    Printf("===> using NON LINEAR law");
    If(!Flag_Hysteresis)
      If(Flag_NonlinearLawType==0)
        Printf("===> testing NON LINEAR case with LINEAR law");
        nu[#{Iron}] = nu_lin[] ;
        dhdb_NL[ #{Iron} ]   = dhdb_lin_NL[$1] ;
      EndIf
      If(Flag_NonlinearLawType==1)
        Printf("===> 3kW machine NON LINEAR law");
        nu[ #{Iron} ] = nu_3kW[$1] ;
        h[ #{Iron} ] = h_3kW[$1] ;
        dhdb_NL[ #{Iron} ]= dhdb_3kW_NL[$1] ;
        dhdb[ #{Iron} ]   = dhdb_3kW[$1] ;
      EndIf
      If(Flag_NonlinearLawType==2)
        Printf("===> anhysteretic NON LINEAR law corresponding to Jiles-Atherton params");
        nu[ #{Iron} ] = nu_anhys[$1] ;
        h[ #{Iron} ] = h_anhys[$1] ;
        dhdb_NL[ #{Iron} ]= dhdb_anhys_NL[$1] ;
        dhdb[ #{Iron} ]   = dhdb_anhys[$1] ;
      EndIf
    Else
      Printf("===> Hysteretic NON LINEAR law => Jiles-Atherton model");
    EndIf
  EndIf

  relaxation_function[] = ($Iteration<Nb_max_iter/2) ? relaxation_factor : 0.2 ;
  relax_max = 1;
  relax_min = 1/10;
  relax_numtest=10;
  test_all_factors=0;
  relaxation_factors_lin() = LinSpace[relax_max,relax_min,relax_numtest];

}


Jacobian {
  { Name Vol ; Case { { Region All ; Jacobian Vol ; } } }
  { Name Sur ; Case { { Region All ; Jacobian Sur ; } } }
  { Name Lin ; Case { { Region All ; Jacobian Lin ; } } }
}

Integration {
  { Name II ; Case {
      { Type Gauss ;
        Case {
          { GeoElement Line        ; NumberOfPoints  4 ; } // 1:20
          { GeoElement Triangle    ; NumberOfPoints  6 ; } // 1, 3, 4, 6, 7, 12, 13, 16
          { GeoElement Quadrangle  ; NumberOfPoints  7 ; } // 1, 3, 4, 7
        }
      }
    }
  }

  { Name I1p ;
    Case {
      { Type Gauss ;
        Case {
          { GeoElement Triangle    ; NumberOfPoints  1 ; }
          { GeoElement Quadrangle  ; NumberOfPoints  1 ; } // 1, 3, 4, 7
        }
      }
    }
  }
}



FunctionSpace {

  { Name Hcurl_a; Type Form1;
    BasisFunction {
      { Name se ; NameOfCoef ae ; Function BF_Edge ;
        Support Domain ; Entity EdgesOf[ All ] ; }
    }
    Constraint {
      { NameOfCoef ae; EntityType EdgesOf ; NameOfConstraint MagneticVectorPotential; }
      { NameOfCoef ae; EntityType EdgesOfTreeIn ; EntitySubType StartingOn ;
        NameOfConstraint Gauge_av ; }
    }
  }

  { Name Hgrad_u; Type Form0;
    BasisFunction {
      { Name su ; NameOfCoef un ; Function BF_Node ;
        Support DomainC ; Entity NodesOf[ All ] ; }
    }
    Constraint {
      { NameOfCoef un; EntityType NodesOf ; NameOfConstraint ElectricScalarPotential; }
    }
  }

  // Current in stranded coil (3D)
  { Name Hregion_i ; Type Scalar ;
    BasisFunction {
      { Name sr ; NameOfCoef ir ; Function BF_Region ;
        Support DomainS0 ; Entity DomainS0 ; }
    }
    GlobalQuantity {
      { Name Is ; Type AliasOf        ; NameOfCoef ir ; }
      { Name Us ; Type AssociatedWith ; NameOfCoef ir ; }
    }
    Constraint {
      { NameOfCoef Us ; EntityType Region ; NameOfConstraint Voltage ; }
      { NameOfCoef Is ; EntityType Region ; NameOfConstraint Current ; }
    }
  }

  // BFs for case with hysteresis
  { Name H_hysteresis ; Type Vector;
    BasisFunction {
      { Name sex ; NameOfCoef aex ; Function BF_VolumeX ; Support Domain ; Entity VolumesOf[ All ] ; }
      { Name sey ; NameOfCoef aey ; Function BF_VolumeY ; Support Domain ; Entity VolumesOf[ All ] ; }
      { Name sez ; NameOfCoef aez ; Function BF_VolumeZ ; Support Domain ; Entity VolumesOf[ All ] ; }
    }
  }

}


If(Flag_HomogType)
  Include "Macro_homogenisation_laminations.pro";
EndIf


Formulation {

  { Name MagStaDyn_av ; Type FemEquation ;
    Quantity {
      { Name a ; Type Local  ; NameOfSpace Hcurl_a ; }

      // Massive conductor (DomainC)
      { Name v ; Type Local  ; NameOfSpace Hgrad_u ; }

      // Source: Stranded conductor (DomainB)
      { Name ir ; Type Local  ; NameOfSpace Hregion_i ; }
      { Name Us ; Type Global ; NameOfSpace Hregion_i[Us] ; }
      { Name Is ; Type Global ; NameOfSpace Hregion_i[Is] ; }

      If(Flag_Hysteresis)
        { Name h ; Type Local ; NameOfSpace H_hysteresis ; }
      EndIf


      If(Flag_HomogType>0)
        // Homogenized laminations
        For k In {2:ORDERMAX} // b_2, b_4, b_6
          { Name b~{2*k-2}  ; Type Local ; NameOfSpace Hb~{2*k-2} ; }
        EndFor
        If(Flag_Hysteresis)
          For i In {1:nbrQuadraturePnts}
            { Name h~{i} ; Type Local ; NameOfSpace H_hysteresis~{i} ; }
          EndFor
        EndIf
      EndIf
    }

    Equation {
      Galerkin { [ nu[] * Dof{d a} , {d a} ]     ;
        In Domain_L  ; Jacobian Vol ; Integration II ; }


      If(NbrRegions[Domain_NL])
        If( (Flag_HomogType==0) || (Flag_HomogType && ORDER==1))
          If(!Flag_Hysteresis)
            Galerkin { [ nu[{d a}/fillfactor] * Dof{d a} , {d a} ]     ;
              In Domain_NL  ; Jacobian Vol ; Integration II ; }
            Galerkin { JacNL [ 1/fillfactor*dhdb_NL[{d a}] * Dof{d a} , {d a} ] ;
              In Domain_NL ; Jacobian Vol ; Integration II ; }
          Else
            Galerkin { [ SetVariable[
                  h_Jiles[ {h}[1],
                    {d a}[1]/fillfactor,
                    {d a}/fillfactor ]{List[hyst_FeSi]},
                  QuadraturePointIndex[] ]{$hjiles}, {d a} ] ;
              In Domain_NL ; Jacobian Vol ; Integration I1p ; }
            Galerkin { JacNL[ 1/fillfactor*dhdb_Jiles[
                  {h},
                  {d a}/fillfactor,
                  {h}-{h}[1] ]{List[hyst_FeSi]} * Dof{d a} , {d a} ] ;
              In Domain_NL ; Jacobian Vol ; Integration I1p ; }

            // h_Jiles saved in local quantity {h}
            // BF is constant per element => 1 integration point is enough
            Galerkin { [ Dof{h}   , {h} ]  ; // saving h_Jiles in local quantity h
              In Domain_NL ; Jacobian Vol ; Integration I1p ; }
            Galerkin { [ -GetVariable[QuadraturePointIndex[]]{$hjiles} , {h} ]  ;
              In Domain_NL ; Jacobian Vol ; Integration I1p ; }
          EndIf
        EndIf
      EndIf

      Galerkin { DtDof[ sigma[] * Dof{a} , {a} ] ;
        In DomainC ; Jacobian Vol ; Integration II ; }
      Galerkin { [ sigma[] * Dof{d v} , {a} ] ;
        In DomainC ; Jacobian Vol ; Integration II ; }
      Galerkin { DtDof[ sigma[] * Dof{a} , {d v} ] ;
        In DomainC ; Jacobian Vol ; Integration II ; }
      Galerkin { [ sigma[] * Dof{d v} , {d v} ] ;
        In DomainC ; Jacobian Vol ; Integration II ; }

      // Galerkin { [ -js[], {a} ] ;
      //  In DomainS0 ; Jacobian Vol ; Integration II ; }
      Galerkin { [ -Idir[] * Dof{ir}, {a} ] ;
        In DomainS0 ; Jacobian Vol ; Integration II ; }

      If(Flag_HomogType>0)
        If(!Flag_ComplexReluctivity && Flag_AnalysisType>0)// Either additional terms or complex nu in DomainLam
          Call Macro_Terms_DomainLamC_sigma;
          If(!Flag_Hysteresis)
            Call Macro_Terms_DomainLam_nu;
          Else
            Call Macro_Terms_DomainLam_hysteresis;
          EndIf
        EndIf
      EndIf

    }
  }



}

//======================================================================

Resolution {

  { Name Analysis ;
    System {
      If( (Flag_AnalysisType<2) || NbrRegions[Domain_NL])
        { Name Sys_Mag ; NameOfFormulation MagStaDyn_av ;}
      Else
        { Name Sys_Mag ; NameOfFormulation MagStaDyn_av ; Frequency Freq; }
      EndIf
    }
    Operation {

      If(Clean_Results)
        // SystemCommand[StrCat["rm -rf ", ResDir]];
        SystemCommand[StrCat["rm -rf ", ResDir, "*.pos"]];
        SystemCommand[StrCat["rm -rf ", ResDir, "*.dat"]];
      EndIf
      CreateDir[ResDir];

      If(Flag_AnalysisType!=1)
        If(!NbrRegions[Domain_NL])
          Generate[Sys_Mag] ; Solve[Sys_Mag] ;
        Else
          IterativeLoop[Nb_max_iter, stop_criterion, relaxation_factor]{
            GenerateJac[Sys_Mag] ; SolveJac[Sys_Mag] ; }
        EndIf
        SaveSolution [Sys_Mag] ;

        Test[ GetNumberRunTime[visu]{StrCat[mfem,"Visu/Real-time maps"]} ]{
          PostOperation[Get_LocalFields];
        }
        PostOperation[Get_GlobalQuantities];
      Else // time domain

        InitSolution[Sys_Mag];
        TimeLoopTheta[time0, timemax, delta_time , 1.]{
          If(!NbrRegions[Domain_NL])
            Generate[Sys_Mag] ; Solve[Sys_Mag] ;
          Else
          IterativeLoop[Nb_max_iter, stop_criterion, relaxation_function[]]{
              GenerateJac[Sys_Mag] ;
              //If(!Flag_Hysteresis)
                SolveJac[Sys_Mag] ;
                //Else
                //SolveJac_AdaptRelax[Sys_Mag, relaxation_factors_lin(), test_all_factors ] ;
                //EndIf
            }
          EndIf
          SaveSolution [Sys_Mag] ;
          Test[ GetNumberRunTime[visu]{StrCat[mfem,"Visu/Real-time maps"]} ]{
            PostOperation[Get_LocalFields];
          }
          PostOperation[Get_GlobalQuantities];
        }

      EndIf


    }
  }

}


PostProcessing {

  { Name MagStaDyn_av ; NameOfFormulation MagStaDyn_av ;
    PostQuantity {
      { Name a  ; Value { Local { [ {a} ]            ; In Domain ; Jacobian Vol ; } } }
      { Name b  ; Value { Local { [ {d a} ]          ; In Domain ; Jacobian Vol ; } } }
      { Name bz ; Value { Local { [ CompZ[{d a}] ]   ; In Domain ; Jacobian Vol ; } } }
      { Name h  ; Value {
          Local { [ nu[]*{d a} ] ; In Domain_L ; Jacobian Vol ; }
          If(!Flag_Hysteresis)
            Local { [ nu[{d a}]*{d a} ] ; In Domain_NL ; Jacobian Vol ; }
          Else
            Local { [ {h} ] ; In Domain_NL ; Jacobian Vol ; }
          EndIf
        }
      }
      If(Flag_Hysteresis)
        { Name hdb  ; Value { // Useful for computing losses in hysteretic case, by integrating in one period (steady state)
            Local { [ {h}*({d a}-{d a}[1]) ] ; In Domain_NL  ; Jacobian Vol ; }
          }
        }
      EndIf

      { Name v ; Value { Term { [ {v} ]              ; In DomainC ; Jacobian Vol ;} } }
      { Name e ; Value { Term { [ -(Dt[{a}]+{d v}) ] ; In DomainC ; Jacobian Vol ;} } }

      { Name j ; Value { Term { [ -sigma[]*(Dt[{a}]+{d v}) ] ; In DomainC ; Jacobian Vol ;} } }
      { Name normj ; Value { Term { [ Norm[-sigma[]*(Dt[{a}]+{d v})] ] ; In DomainC ; Jacobian Vol ;} } }

      { Name Jtot ; Value {
          Integral { [ SymmetryFactor*AxialLength*(-sigma[]*(Dt[{a}]+{d v})) ] ;
            In DomainC ; Jacobian Vol ; Integration II; } } }

      { Name ir ; Value { Local { [ {ir} ]  ; In DomainS0 ; Jacobian Vol ; } } } // Source
      { Name Isrc ; Value { Integral { [ SymmetryFactor*AxialLength*{ir} ]  ;
            In DomainS0 ; Jacobian Vol ; Integration II ; } } } // Source
      // { Name js ; Value { Local { [ js[] ]       ; In DomainS0 ; Jacobian Vol ; } } } // Source
      { Name js ; Value { Local { [ Idir[] * {ir} ]  ; In DomainS0 ; Jacobian Vol ; } } } // Source

      { Name Flux ; Value { Integral { [ SymmetryFactor*AxialLength * Idir[] * {a} ] ; // /Sc[] => to add => now in matlab files
            In DomainS0 ; Jacobian Vol ; Integration II ; } } }

      { Name ComplexPower ; Value { // real part -> eddy current losses; imag part-> magnetic energy
          Integral { [ SymmetryFactor*AxialLength*Complex[ SquNorm[sigma[]*(Dt[{a}]+{d v}/SymmetryFactor)]/sigma[], nu[]*SquNorm[{d a}] ] ] ;
            In Iron; Jacobian Vol ; Integration II ; }
        }
      }

      { Name EddyCurrentLosses ;
        Value {
          Integral { [ SymmetryFactor*AxialLength*sigma[]*SquNorm[Dt[{a}]+{d v}/SymmetryFactor] ] ;
            In Iron ; Jacobian Vol ; Integration II ; }
        }
      }

      { Name MagneticEnergy ;
        Value {
          If(!Flag_Hysteresis)
            Integral {
              [ SymmetryFactor*AxialLength* nu[{d a}]*SquNorm[{d a}] ] ;
              In Domain ; Jacobian Vol ; Integration II ; }
          Else
            Integral {
              [ SymmetryFactor*AxialLength* nu[{d a}]*SquNorm[{d a}] ] ;
              In Domain_L ; Jacobian Vol ; Integration II ; }
            Integral {
              [ SymmetryFactor*AxialLength* {h}*{d a} ] ;
              In Domain_NL ; Jacobian Vol ; Integration II ; }
          EndIf
        }
      }

      If(Flag_HomogType>0)
        Call PostQuantities_Homog;
      EndIf

    }
  }


}



PostOperation ViewTree UsingPost MagStaDyn_av {
  // Print the tree for debugging purposes.
  PrintGroup[ EdgesOfTreeIn[ { DomainGauge }, StartingOn { Surface_NoGauge_av } ],
    In DomainGauge, File StrCat[ResDir,"Tree.pos"] ];
    Echo[ Str["l=PostProcessing.NbViews-1;","View[l].ColorTable = { DarkRed };","View[l].LineWidth = 5;"] ,
      File StrCat[ResDir,"tmp.geo"], LastTimeStepOnly] ;
}
