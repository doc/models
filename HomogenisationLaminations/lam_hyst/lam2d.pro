Include "lam2d_data.pro" ;

//--------------------------------------------------------------------------

// gmsh lam2d.geo -2 -setnumber Flag_HomogType 0 -o fine.msh
// gmsh lam2d.geo -2 -setnumber Flag_HomogType 1 -o block.msh (homogenized block)
// gmsh lam2d.geo -2 -setnumber Flag_HomogType 2 -o hom2.msh  (homogenized lamination)

//--------------------------------------------------------------------------
//--------------------------------------------------------------------------

DefineConstant[
  Flag_AnalysisType = {1,
    Choices{0="Static",  1="Time domain", 2="Frequency domain"},
    ServerAction Str["Reset", StrCat[mfem, "00Approx. order"]],
    Name StrCat[mfem, "100Type of analysis"], Highlight "Blue",
    Help Str["- Use 'Static' to compute static fields",
             "- Use 'Time domain' to compute the dynamic response (transient)",
             "- Use 'Frequency domain' to compute the dynamic response (steady state)"]}

  Freq = {100, Min 0.1, Max 500e3, Name StrCat[mfem,"101Frequency [Hz]"], Visible Flag_AnalysisType>0, Highlight "LightGreen"}
  Omega  = 2*Pi*Freq
  T = 1/Freq

  Flag_NL = {!(Flag_AnalysisType==2), Choices{0,1},
    Name StrCat[mfem,"20Nonlinear BH-curve?"], Highlight "LightPink", ReadOnly (Flag_AnalysisType==2)}
  Flag_NonlinearLawType = {1,
    Choices{
      0="linear law (testing)",
      1="3 kW machine law",
      2="anhysteretic law",
      3="Jiles-Atherton hysteretic law"},
    Name StrCat[mfem,"21 Choose BH-curve?"], Highlight "Magenta", Visible Flag_NL,
    ReadOnly (Flag_AnalysisType==2) }
  Flag_Hysteresis = {(Flag_NonlinearLawType==3)?1:0, Choices{0,1},
    Name StrCat[mfem,"22Jiles-Atherton hysteretic law?"],
    Highlight "LightPink", ReadOnly, Visible Flag_NL}

  ORDER = {(Flag_AnalysisType==0 || (Flag_Hysteresis==1))?1:(Flag_AnalysisType==2)?-2:2,
    Choices {
        0="exact",
       -1="-1 (homog FD zero order)",
       -2="-2 (homog FD 2nd order)",
       -3="-3 (homog FD 4th order)",
        1="1 (homog TD zero order)",
        2="2 (homog TD 2nd order)",
        3="3 (homog TD 4th order)",
        4="4 (homog TD 6th order)" },
      Name StrCat[mfem, "00Approx. order"],
      Help Str["- With 'Time domain', use order>0",
        "- With 'Frequency domain' all possibities hold"],
      Highlight "Red", Visible Flag_HomogType, ReadOnly Flag_AnalysisType==0}

    Flag_ComplexReluctivity  = Flag_HomogType && !(ORDER>0) // 0 3D fine reference model OR Homog real; 1 Homog with nu complex
];


Group{

  NN = (Flag_HomogType==1)? 1 : nlam/(Flag_Symmetry==0 ? 1:2) ; // Testing complete geometry

  For k In {0:NN-1}
    Iron~{k}     = Region[{(IRON+k)}] ;
    Iron        += Region[{(IRON+k)}] ;

    ElecIron~{k} = Region[{(ELECIRON+k)}] ;
    ElecIron    += Region[{ElecIron~{k}}] ;
  EndFor

  Ind     = Region[{IND}];
  AirOut  = Region[{AIR}];
  AirIn   = Region[{ISOL}]; // between laminations

  SymX0 = Region[{}];
  SurfaceElec = Region[{}]; // av-formulation

  SymMiddle = Region[{SYMMETRY_Y0}];
  If(1)
    OuterBnd  = Region[{}];
  Else
    // Equivalent to no BC at outer boundary (with or without air outside ind)
    // Using this condition or the previous gives exactly the same result
    OuterBnd0 = #OUTERBND ;
    PntOuterBnd = #CORNER;
    OutLayerElements = ElementsOf[OuterBnd0, OnOneSideOf PntOuterBnd];
    OuterBnd = Region[{OuterBnd0, -OutLayerElements}];
  EndIf

  If(Flag_Symmetry==2) // 1/4 of the model
    SymX0 += Region[{SYMMETRY_X0}];
    SymX0 += Region[{ElecIron}];
    SurfaceElec += Region[{ElecIron}]; // av-formulation
  EndIf

  SurfaceGe0 = Region[{SymMiddle, SymX0}];
  Surface_FixedMVP  = Region[{SurfaceGe0, OuterBnd}];

  If (Flag_HomogType<2)
    Air = Region[{AirOut, AirIn}];
  Else
    // laminated structure is there but min mesh
    // for testing purposes
    Air   = Region[{AirOut}];
    Iron += Region[{AirIn}];
  EndIf

  DomainS0 = Region[{Ind}];
  DomainCC = Region[{Air, Ind}];

  If(Flag_HomogType==0) // Fine reference model
    If(Flag_AnalysisType)
      DomainC   = Region[{Iron}];
    Else
      DomainCC += Region[{Iron}];
      DomainC   = Region[{}];
    EndIf
  Else
    DomainLamC  = Region[{Iron}];
    DomainLamCC = Region[{}];
    DomainLam   = Region[{DomainLamC, DomainLamCC}];

    DomainNoLamC  = Region[ {} ] ;
    DomainC   = Region[ {DomainNoLamC} ] ;
    // SkinDomainC = Region[{SkinMetal}]; //+++ for gauging => not needed when using av formulation
  EndIf


  If(!Flag_NL)
    Domain_NL = Region[{}];
    Domain_L = Region[{Air, Ind, Iron}];
  Else
    Domain_NL = Region[{Iron}];
    Domain_L  = Region[{Air, Ind}];
  EndIf
  Domain = Region[{Domain_L, Domain_NL}];

  DomainGauge = Region[{Domain}];
  Surface_NoGauge_av = Region[{Surface_FixedMVP}];

}


Function {
  DefineConstant[
    mur_fe   = {2000, Name StrCat[mfem,"31relative mu (iron)"], Highlight "Ivory", Visible !Flag_NL}
    sigma_fe = {2e6, Name StrCat[mfem,"32conductivity (iron)"], Highlight "Ivory"}
    // sigma_sc = {sigma_fe/1e3, Name StrCat[mfem,"{23conductivity (sc)"], Highlight "Cyan"}
  ];

  //Fct_Src[] = F_Cos_wt_p[]{2*Pi*Freq, (Flag_AnalysisType==1) ? Pi/2 : 0};

  // With hysteresis: Damped start necessary
  Trelax = 1/Freq/8;
  Frelax[] = 1;//($Time < Trelax) ? 0.5 * (1. - Cos [Pi*$Time/Trelax] ) : 1. ;

  Fct_Src[] = (Flag_Hysteresis==1 ? Frelax[]:1) * ((Flag_AnalysisType==1) ? F_Sin_wt_p[]{2*Pi*Freq, 0} : F_Cos_wt_p[]{2*Pi*Freq, 0})  ;

  xcen = xlam;
  ycen = ylam;

  phi0[] = Atan2[Y[]-(Y[]>0.?1.:-1.)*ycen, X[]-(X[]>0?1:-1)*xcen];
  Idir[] = (Fabs[X[]]>=xcen && Fabs[Y[]]>=ycen) ?
                              Vector[ -Sin[phi0[]#1], Cos[#1], 0. ] :
          ((Fabs[X[]]<xcen) ? Vector[(Y[]>0?-1:1), 0., 0.] : Vector[0.,(X[]>0?1:-1)*1., 0.]);

  Sc[] = SurfaceArea[]{IND};

  val_current = 8e5; //anhysteretic law is saturated but converges well
  //js[] = val_current * Idir[] * Fct_Src[] ; // --- 21/12/2018

}


// Constraints
//============================================================
Constraint {
  { Name MagneticVectorPotential  ;
    Case {
      { Region SurfaceGe0 ; Value 0. ; }
      { Region OuterBnd   ; Value 0. ; }
    }
  }
  { Name Gauge_av; Type Assign;
    Case {
      { Region DomainGauge ; SubRegion Surface_NoGauge_av ; Value 0. ; }
    }
  }

  { Name ElectricScalarPotential  ;
    Case {
      For k In {0:NN-1}
        { Region ElecIron~{k} ; Value 0. ; }
      EndFor
    }
  }

  { Name Current ; Type Assign ;
    Case {
      { Region Ind ; Value val_current; TimeFunction Fct_Src[] ; }
    }
  }
  { Name Voltage ; Type Assign ;
    Case {
    }
  }

}


//======================================================================

dist_cen = 1e-4;
//dist_cen = w_lam/2-w_lam/2/8;

x0 = dist_cen; y0 = 0.;
x1 = w_lam/2;  y1 = h/2;

NptsLam = 10*3 ;
//Npts = NptsLam * nlam/2;

newappend = Sprintf("_TD%g_nl%g_m%g", (Flag_AnalysisType==1),  Flag_NonlinearLawType, Flag_HomogType);
If(Flag_HomogType==1)
  newappend = Sprintf("_TD%g_nl%g_m%g_o%g", (Flag_AnalysisType==1),  Flag_NonlinearLawType, Flag_HomogType, ORDER);
EndIf

ExtGmsh = StrCat[ newappend, Sprintf("_de%g_f%g.pos", de/mm, Freq) ];
ExtGplt = StrCat[ newappend, Sprintf("_de%g_f%g.dat", de/mm, Freq) ];
ExtGpltGlobal = StrCat[ newappend, Sprintf("_de%g_f%g.dat", de/mm, Freq)];






Include "homog_laminations.pro";

ResId="";
po_mag   = StrCat["{9Output - Magnetics/", ResId];

//===========================================================================================

PostOperation {

  { Name Get_LocalFields ; NameOfPostProcessing MagStaDyn_av ; LastTimeStepOnly ;
    Operation {
      Print[ js, OnElementsOf Ind,    File StrCat[ ResDir, "js", ExtGmsh] ];
      // Print[ a, OnElementsOf Domain,  File StrCat[ ResDir, "a", ExtGmsh] ];
      If(NbrRegions[DomainC])
        Print[ v, OnElementsOf DomainC, File StrCat[ ResDir, "v", ExtGmsh] ];
        Print[ j, OnElementsOf DomainC, File StrCat[ ResDir, "j", ExtGmsh] ];
        Echo[Str["k=PostProcessing.NbViews-1;","View[k].RangeType = 3;",
            "View[k].CenterGlyphs = 0;","View[k].GlyphLocation = 1;"], File "res/maps.opt"];
      EndIf
      // If(Flag_Hysteresis)
      //   Print[ h, OnElementsOf Iron,  File StrCat[ ResDir, "h", ExtGmsh] ];
      // EndIf
      Print[ bz, OnElementsOf Iron,  File StrCat[ ResDir, "bz", ExtGmsh] ];
      Echo[Str["k=PostProcessing.NbViews-1;","View[k].RangeType = 3;"], File "res/maps.opt"];

      /*
      If(Flag_HomogType>0)
        If (ORDER>0) // Only time domain (or FD with b_k)
          For k In {1:ORDER}
            Print[ bz~{2*k-2}, OnElementsOf DomainLam, File StrCat[ResDir,Sprintf("bz%g",2*k-2),ExtGmsh] ] ;
            Echo[ Str["k=PostProcessing.NbViews-1;","View[k].RangeType = 3;"], File "res/maps.opt"];
            Print[ j~{2*k-2}, OnElementsOf DomainLam, File StrCat[ResDir,Sprintf("j%g",2*k-2),ExtGmsh] ] ;
            Echo[ Str["k=PostProcessing.NbViews-1;","View[k].RangeType = 3;"], File "res/maps.opt"];
          EndFor
        EndIf
      EndIf
      */
      If(Flag_AnalysisType==2) // a choice

        If(Flag_HomogType==0)
          For ll In {1:nlam-1:2}
            y0~{ll} = e*ll/2-dlam/2;
            y1~{ll} = y0~{ll} + dlam;
            Print[ b, OnLine {{x0, y0~{ll}, 0.}{x0, y1~{ll}, 0.}}{NptsLam}, Format TimeTable, LastTimeStepOnly,
              File >> StrCat[ ResDir, "bl", ExtGplt] ];
            Print[ j, OnLine {{x0, y0~{ll}, 0.}{x0, y1~{ll}, 0.}}{NptsLam}, Format TimeTable, LastTimeStepOnly,
              File >> StrCat[ ResDir, "jl", ExtGplt] ];
          EndFor
        EndIf

        If(Flag_HomogType>0)
          If (ORDER>0) // Only time domain (or FD with b_k)
            For k In {1:ORDER}
              For ll In {1:nlam-1:2}
                y0~{ll} = e*ll/2-dlam/2;
                y1~{ll} = y0~{ll} + dlam;
                Print[ bz~{2*k-2}, OnLine {{x0, y0~{ll}, 0.}{x0, y1~{ll}, 0.}}{NptsLam}, Format TimeTable, LastTimeStepOnly,
                  File >> StrCat[ ResDir, Sprintf("bl%g_o%g",2*k-2,ORDER), ExtGplt] ];
                Print[ j~{2*k-2}, OnLine {{x0, y0~{ll}, 0.}{x0, y1~{ll}, 0.}}{NptsLam}, Format TimeTable, LastTimeStepOnly,
                  File >> StrCat[ ResDir, Sprintf("jl%g_o%g",2*k-2,ORDER), ExtGplt] ];
              EndFor
            EndFor
          EndIf
        EndIf

      EndIf
    }
  }

  { Name Get_GlobalQuantities ; NameOfPostProcessing MagStaDyn_av ; Format TimeTable; LastTimeStepOnly ;
    Operation {
      Print[ Flux[DomainS0], OnGlobal, SendToServer StrCat[po_mag, "Flux linkage [Wb]"], Color "Ivory",
        File > StrCat[ ResDir, "fl", ExtGpltGlobal] ];
      Print[ EddyCurrentLosses[Iron], OnGlobal, SendToServer StrCat[po_mag, "Joule losses [W]"], Color "Ivory",
        File > StrCat[ ResDir, "ecl", ExtGpltGlobal] ];
      // Print[ MagneticEnergy[Iron], OnGlobal, SendToServer StrCat[po_mag, "Magnetic energy [J]"], Color "Ivory",
      //  File > StrCat[ ResDir, "me", ExtGpltGlobal] ];
    }
  }

}
