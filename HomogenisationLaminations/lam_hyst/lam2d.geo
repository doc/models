Include "lam2d_data.pro" ;

ff = 1 ;

Mesh.CharacteristicLengthFactor = 0.85/ff ;

Mesh.Algorithm = 1; // 2D mesh algorithm (1=MeshAdapt, 2=Automatic, 5=Delaunay, 6=Frontal, 7=bamg, 8=delquad)
Geometry.CopyMeshingMethod = 1; // Copy meshing method when duplicating geometrical entities?

//Mesh.SurfaceFaces = 1; // Display faces of surface mesh?

// Some common characteristic lengths
//====================================

pind = h/10 ;
lca = pind; // lc for air

Lay1 = ff*14; Pro1 = 0.8; // 14 horizontal divisions of the laminations
Lay3 = (Flag_Fine>1) ? 2/2+1 : ff*11;
Bwidth=1;  // vertical divisions of one lamination, no bump if Bwidth == 1
Lay3_tot = 2/2*(nlam/2)+1 ;  // 22 vertical divisions of the stack ==> homogenised model

Lay_isol_between_iron =(!Flag_HomogType) ? 4/2 : 2; //+++ only one division for isol

plam = dlam/Lay3 ;

Lay1_isol1 = ff*4 ; Bisolal1 = 1; // Isol with slight conductivity



//=====================================
// First lamination -- up to down
//=====================================

pl0[] += newp; Point(newp) = {xlam, ylam, 0, plam};
pl0[] += newp; Point(newp) = {xlam, (Flag_Fine) ? ylam-dlam : 0, 0, plam};
pl0[] += newp; Point(newp) = {0,    (Flag_Fine) ? ylam-dlam : 0, 0, plam};
pl0[] += newp; Point(newp) = {0,    ylam, 0, plam};

For k In {0:3}
  ll0[] += newl; Line(newl) = {pl0[k], pl0[(k==3?0:k+1)]};
EndFor

Line Loop(newll) = {ll0[]};
surfIron[] +=news; Plane Surface(news) = {newll-1};
laxis_iron[] += ll0[2] ;
lborder_iron[] +=ll0[0];

// Iron
Transfinite Line{-ll0[1],ll0[3]} = Lay1 Using Progression Pro1 ;
Transfinite Line{ll0[{0,2}]} = (Flag_Fine?Lay3:Lay3_tot) Using Bump Bwidth;

// pli[] += newp; Point(newp) = {xlam+w_sc, ylam, 0, plam};
// pli[] += newp; Point(newp) = {xlam+w_sc, (Flag_Fine) ? ylam-dlam : 0, 0, plam};

// lli[] += newl; Line(newl) = {pl0[0], pli[0]};
// lli[] += newl; Line(newl) = {pli[0], pli[1]};
// lli[] += newl; Line(newl) = {pli[1], pl0[1]};

// Line Loop(newll) = {-ll0[0],lli[]};
// surfIsol_side1[] +=news; Plane Surface(news) = {newll-1};

//short circuit bar
// Transfinite Line{lli[{0,2}]} = Lay1_isol1 Using Progression Bisolal1;
// Transfinite Line{lli[{1}]} = (Flag_Fine?Lay3:Lay3_tot) Using Bump Bwidth;

//=====================================
// Inductor
//=====================================
phi = Pi/4;

dla_ind = rla_ind * Cos[phi] ;

xind1 = xlam + dla_ind ;
xind2 = xlam + dla_ind + w_ind ;
yind1 = ylam ;
yind2 = ylam + dla_ind;

pi[]+=newp ; Point(newp) = { xind1, 0,      0, pind};
pi[]+=newp ; Point(newp) = { xind2, 0,      0, pind};
pi[]+=newp ; Point(newp) = { xind2, yind1,  0, pind};
pi[]+=newp ; Point(newp) = { xind1, yind1,  0, pind};

li[]+=newl ; Line(newl) = {pi[0],pi[1]};
li[]+=newl ; Line(newl) = {pi[1],pi[2]};
li[]+=newl ; Line(newl) = {pi[2],pi[3]};
li[]+=newl ; Line(newl) = {pi[3],pi[0]};
Line Loop(newll) = {li[{0:3}]};
surfind[] += news ; Plane Surface(news) = {newll-1};

pi[]+=newp ; Point(newp) = { xlam, yind2,       0, pind};
pi[]+=newp ; Point(newp) = { xlam, yind2+w_ind, 0, pind};
pi[]+=newp ; Point(newp) = { 0, yind2+w_ind, 0, pind};
pi[]+=newp ; Point(newp) = { 0, yind2,       0, pind};

li[]+=newl ; Line(newl) = {pi[0+4],pi[1+4]};
li[]+=newl ; Line(newl) = {pi[1+4],pi[2+4]};
li[]+=newl ; Line(newl) = {pi[2+4],pi[3+4]};
li[]+=newl ; Line(newl) = {pi[3+4],pi[0+4]};
Line Loop(newll) = {li[{4:7}]};
surfind[] += news ; Plane Surface(news) = {newll-1};

ci[]+=newl ; Circle(newl) = {pi[3],pl0[0],pi[4]};
ci[]+=newl ; Circle(newl) = {pi[2],pl0[0],pi[5]};

Line Loop(newll) = {ci[0],li[4],-ci[1],li[2]};
surfind[] += news ; Plane Surface(news) = {newll-1};

// Inductor
Transfinite Line{li[{0,2,4,6}]} = 2; // 3width inductor //Ceil[w_ind/pind] ;
Transfinite Line{ci[]} = 7; // rounded part of inductor //Ceil[w_ind/pind]+1 ;
Transfinite Line{li[{1,3}]} = nlam ;
Transfinite Line{li[{5,7}]} = Lay1+3*2 ;

Transfinite Surface "*" ;
If(Flag_Recombine)
  Recombine Surface "*" ;
EndIf


bndind[] += Abs(CombinedBoundary{Surface{surfind[]};});
bndind_out[]+= {-bndind[3],bndind[7],-bndind[1]};
bndind_in[]+= {bndind[5], -bndind[6], bndind[2]};

cen0 = newp ; Point(cen0) = {0, 0, 0, pind};



If(Flag_Fine)
  //=====================================
  // First isolation -- up to down
  //=====================================
  pe0[] += newp; Point(newp) = {xlam, ylam-e, 0, plam};
  pe0[] += newp; Point(newp) = {0,    ylam-e, 0, plam};

  le0[] += newl; Line(newl) = {pl0[1], pe0[0]};
  le0[] += newl; Line(newl) = {pe0[0], pe0[1]};
  le0[] += newl; Line(newl) = {pe0[1], pl0[2]};

  laxis[] += le0[2];

  Line Loop(newll) = {le0[], -ll0[1]};
  surfIsol[] +=news; Plane Surface(news) = {newll-1};

  // Isolation between iron
  Transfinite Line{le0[{0,2}]} = Lay_isol_between_iron ;
  Transfinite Line{-le0[1]} = Lay1 Using Progression Pro1 ;

  If(nlam==2)
    ldown[] = le0[1];
    ledge[] = le0[0];
  EndIf

  For ii In {1:nlam/2-1}
    surfIron[] += Translate {0, -ii*e,0} { Duplicata { Surface{surfIron[0]}; } };
    aux[] = Boundary{Surface{surfIron[ii]};};
    skinIron[]   += aux[] ;
    laxis_iron[] += aux[2] ;

    lborder_iron[] += aux[0];
  EndFor
  For ii In {1:nlam/2-1}
    surfIsol[] += Translate {0, -ii*e,0} { Duplicata { Surface{surfIsol[0]}; } };
    aux[] = Boundary{Surface{surfIsol[ii]};};
    laxis[] += aux[2] ;
    ldown[] += aux[1] ;
    ledge[] += aux[0];
  EndFor

  nn=#ldown[];
  pstomv[] = Boundary{Line{ldown[{nn-1}]};};

  Translate {0, de/2,0} { Point{pstomv[]}; }


  pntedge[] = Boundary{Line{ledge[#ledge[]-1]};};
  lai[]+=newl; Line(newl) = {pntedge[1], pi[0]};
  lai[]+=newl; Line(newl) = {pi[7], pl0[3]};


  bndironisol[] = CombinedBoundary{
    Surface{surfIron[],surfIsol[]};};

  Transfinite Surface "*";
  If(Flag_Recombine)
    Recombine Surface "*" ;
  EndIf

  bndironisol[] -={laxis[], laxis_iron[],ldown[{#ldown[]-3:#ldown[]-1}]};
  llairout = newll ; Line Loop(newll) = {bndironisol[], lai[], -bndind_in[]} ;

  surfair[]   += news ; Plane Surface(news) = {llairout};

  lines_sym[] += {ldown[{#ldown[]-1}],lai[0]};
EndIf


If(!Flag_Fine)
  surfIsol[]={};
  volisol[]={};
  laxis[]={};
  lai[]+= newl; Line(newl) = {pl0[1], pi[0]};
  lai[]+= newl; Line(newl) = {pi[7],  pl0[3]};

llairout   = newll ; Line Loop(newll) = {bndind_in[], -lai[{0}],-ll0[{3,0}],-lai[1]} ;
  surfair[] += news ; Plane Surface(news) = {llairout};
  lines_sym[] += {ll0[1],lai[0]};
EndIf


lines_symy0[] = {lines_sym[],li[0]}; // middle
lines_symx0[] = {laxis[], laxis_iron[], lai[1], li[6]}; // axis
lines_bnd[]   = {li[{1,5}],ci[1]}; // without air outside the coil


// Symmetry with regard to y-axisO
//====================================================
surfIronD[] = {}; lborder_ironD[]={};

If(!Flag_Symmetry || Flag_Symmetry==1)
  // For convenience, symmetry of the lines where bnd conditions are imposed
  lborder_iron[] += Symmetry {1,0,0,0} { Duplicata{Line{lborder_iron[]};} };

  lines_symy0[] += Symmetry {1,0,0,0} { Duplicata{Line{lines_symy0[]};} };

  lines_bnd[] += Symmetry {1,0,0,0} { Duplicata{Line{lines_bnd[]};} };

  nnk = (Flag_Fine)?nlam/2:1;
  For k In {0:nnk-1}
    surfIron[] += Symmetry {1,0,0,0} { Duplicata{Surface{surfIron[k]};} };
    If(Flag_Fine)
      surfIsol[] += Symmetry {1,0,0,0} { Duplicata{Surface{surfIsol[k]};} };
    EndIf
  EndFor

  surfind[]  += Symmetry {1,0,0,0} { Duplicata{Surface{surfind[]};} };
  surfair[]  += Symmetry {1,0,0,0} { Duplicata{Surface{surfair[]};} };

  // Complete model => Symmetry of the half model
  // ==============

  If(Flag_Symmetry==0)
    lborder_ironD[] = Symmetry {0,1,0,0} { Duplicata{Line{lborder_iron[]};} };
    lines_bnd[] += Symmetry {0,1,0,0} { Duplicata{Line{lines_bnd[]};} };

    kk = #surfIron[];
    For k In {0:kk-1}
      surfIronD[]      += Symmetry {0,1,0,0} { Duplicata{Surface{surfIron[k]};} };

      If(Flag_Fine)
        surfIsol[] += Symmetry {0,1,0,0} { Duplicata{Surface{surfIsol[k]};} };
       EndIf
    EndFor

    surfind[]  += Symmetry {0,1,0,0} { Duplicata{Surface{surfind[]};} };
    surfair[]  += Symmetry {0,1,0,0} { Duplicata{Surface{surfair[]};} };
  EndIf

EndIf


// Physical regions
//====================================================

Physical Surface("air",AIR) = {surfair[]} ;
Physical Surface("inductor",IND) = {surfind[]} ;
Physical Surface("insulation",ISOL) = {surfIsol[]} ;


If(Flag_Fine)
  nni = nlam/2;
  For ii In {0:nni-1}
    // 1/4 of geometry => common to all cases (Flag_Symmetry==2)
    Physical Surface(Sprintf("iron %g", ii),IRON+ii)  = surfIron[{ii}];
    Physical Line(Sprintf("skin iron (right side) %g",ii), SKINIRON_R+ii) = lborder_iron[{ii}];

    If(Flag_Symmetry==2)
      Physical Line(Sprintf("elec iron (middle) %g",ii), ELECIRON+ii) = laxis_iron[{ii}] ;
    EndIf

    If(Flag_Symmetry<2) // Complete ==0 or half geometry ==1
      Physical Surface(Sprintf("iron %g", ii), IRON+ii) += surfIron[{ii+nni}]; // left side
      Physical Line(Sprintf("skin iron (left side) %g",ii), SKINIRON_L+ii) = lborder_iron[{ii+nni}];

      If(Flag_Symmetry==0)
        Physical Surface(Sprintf("iron %g",nlam-ii-1),IRON+nlam-ii-1)  = surfIronD[{ii, ii+nni}];
        Physical Line(Sprintf("skin iron (right side) %g",nlam-ii-1), SKINIRON_R+nlam-ii-1) = lborder_ironD[{ii}];
        Physical Line(Sprintf("skin iron (left side) %g",nlam-ii-1), SKINIRON_L+nlam-ii-1) = lborder_ironD[{ii+nni}];
      EndIf
    EndIf

    allSkinIron_i[] =  Abs( CombinedBoundary{ Physical Surface{IRON+ii}; } );
    allSkinIron_i[] -= {laxis_iron[],lborder_iron[]};
    Physical Line(Sprintf("skin iron %g", ii), SKINIRON+ii) = allSkinIron_i[] ;

    If(Flag_Symmetry==0)
      allSkinIron_iD[] =  Abs( CombinedBoundary{ Physical Surface{IRON+nlam-ii-1}; } );
      allSkinIron_iD[] -= {laxis_iron[],lborder_iron[], lborder_ironD[]};
      Physical Line(Sprintf("skin iron %g", nlam-ii-1), SKINIRON+nlam-ii-1) = allSkinIron_iD[] ;
    EndIf
  EndFor

  allIronIsol[] =  Abs( CombinedBoundary{Surface{surfIron[],surfIronD[],surfIsol[]};} );
  allIronIsol[] -= {lines_symy0[],lines_symx0[]};
  Physical Line(Sprintf("skin iron + isol"), SKINMETAL) = allIronIsol[] ;
EndIf

If(!Flag_Fine)
  Physical Surface("iron (homog)", IRON)  = {surfIron[],surfIronD[]};
  allSkinIron[]  = Abs(CombinedBoundary{Surface{surfIron[],surfIronD[]};});

  skinmetal[] = allSkinIron[];
  skinmetal[] -= {lines_symy0[], lines_symx0[]};
  Physical Line(Sprintf("skin metal"), SKINMETAL) = skinmetal[] ;

  allSkinIron[] -= {laxis_iron[],lborder_iron[],lborder_ironD[]};

  Physical Line("elec iron", ELECIRON) = laxis_iron[] ;

  If(Flag_Symmetry==0) // Complete model model
    Physical Line("skin iron (top, bottom)", SKINIRON) = allSkinIron[];
    Physical Line("skin iron (right)", SKINIRON_R) = {lborder_iron[0], lborder_ironD[0]} ;
    Physical Line("skin iron (left)" , SKINIRON_L) = {lborder_iron[1], lborder_ironD[1]} ;
  EndIf
  If(Flag_Symmetry==1) //1/2 of the model
    Physical Line("skin iron (top)", SKINIRON) = allSkinIron[{1,3}];
    Physical Line("skin iron (right)", SKINIRON_R) = lborder_iron[0] ;
    Physical Line("skin iron (left)", SKINIRON_L) = lborder_iron[1] ;
  EndIf
  If(Flag_Symmetry==2) //1/4 of the model
    Physical Line("skin iron (top)", SKINIRON) = allSkinIron[{1}];
    Physical Line("skin iron (right)", SKINIRON_R) = lborder_iron[0] ;
  EndIf

EndIf


//======================================================
Physical Line("outer boundary", OUTERBND) = lines_bnd[];




If(Flag_Symmetry!=0) // Flag_Symmetry==0 => Complete model: middle does not have to be define in case of complete geo
  Physical Line("sym y=0", SYMMETRY_Y0)  = lines_symy0[]; // All but inductor and conductors
EndIf

If(Flag_Symmetry==2)
  lines_symx0[] -= laxis_iron[]; // Avoiding repetition of elements in physicals...
  Physical Line("symmetry at x=0, not iron", SYMMETRY_X0) = {lines_symx0[]};
EndIf

// Not used... just testing purposes (gauging)
Physical Point(CORNER) = pi[1+1]; // 1 is at x-axis

Color Red { Surface{ surfind[] };}
Color Cyan { Surface{ surfair[] };}
Color Cyan { Surface{ surfIsol[] };}
Color SteelBlue { Surface{ surfIron[], surfIronD[] };}

// Orchid, LightBlue, Gold, LightGreen

// Inverting normals
Reverse Surface{surfIron[],surfIsol[],surfind[2]};


// Gauss integration points along the thickness of the lamination
//===============================================================
If(0)
  Ngp = 5; // nbrQuadraturePoints (half lamination)
  y_1 = 0.07443716949081559;
  y_2 = 0.2166976970646236;
  y_3 = 0.3397047841495122;
  y_4 = 0.4325316833444923;
  y_5 = 0.4869532642585858;

  aaa = 0.5;

  For ii In {0:nlam/2-1}
    For kk In {1:Ngp}
      pg[]+=newp ; Point(newp) = { aaa*xlam, h/2-d/2+y~{kk}*d-ii*e, 0};
      pg[]+=newp ; Point(newp) = { aaa*xlam, h/2-d/2-y~{kk}*d-ii*e, 0};
    EndFor
  EndFor
EndIf

// Checking line for post-processing
//==================================
If(0)
  dist_cen = 1e-3;
  x0 = dist_cen; y0 = 0.;
  x1 = w_lam/2;  y1 = h/2;

  npts = 30 ;

  For ll In {1:nlam-1:2}
    For ii In {1:npts-1}
      If(ll==3)
      Printf("nlam %g x1=%g",nlam, e*ll/2-dlam/2+ii*dlam/npts);
      EndIf
      pl[]+=newp ; Point(newp) = { x0, e*ll/2-dlam/2+ii*dlam/npts ,0};
    EndFor
  EndFor
EndIf


// Improving the mesh of the air around the laminations
//=============================================================

ddl = 0.3*dla_ind;

pmesh[]+=newp ; Point(newp) = {xlam/8,        ylam + ddl, 0., plam};
pmesh[]+=newp ; Point(newp) = {xlam/4,        ylam + ddl, 0., plam};
pmesh[]+=newp ; Point(newp) = {xlam/2-xlam/8, ylam + ddl, 0., plam};
pmesh[]+=newp ; Point(newp) = {xlam/2,        ylam + ddl, 0., plam};
pmesh[]+=newp ; Point(newp) = {xlam/2+xlam/8, ylam + ddl, 0., plam};
Point{pmesh[]} In Surface {surfair[0]};

Characteristic Length pl0[3] = (Flag_Fine==0 ? 4:1) * plam;


If(!Flag_Symmetry || Flag_Symmetry==1)
  pmesh_[]  += Symmetry {1,0,0,0} { Duplicata{Point{pmesh[]};} };
  Point{pmesh_[]} In Surface {surfair[1] };

  If(!Flag_Symmetry) // Just testing
    pmeshD[]  += Symmetry {0,1,0,0} { Duplicata{Point{pmesh[]};} };
    pmeshD_[] += Symmetry {0,1,0,0} { Duplicata{Point{pmesh_[]};} };

    Point{pmeshD[]}  In Surface {surfair[2]};
    Point{pmeshD_[]} In Surface {surfair[3]};
  EndIf
EndIf


//-------------------------------------------------------------------------------
// For nice visualization
//-------------------------------------------------------------------------------

// Hide { Point{ Point {:} }; Line{ Line {:} }; }
// Hide { Point{ Point {:} }; Line{9,13};}

// Show { Line{ linStator[], linRotor[] }; }
