mgeo = "{0Geometrical parameters/";
mgeo2 = "{0Geometrical parameters/dimensions/";

mfem = "{1Analysis parameters/";

DefineConstant[
  Flag_HomogType = {0, Choices{
      0="Fine reference model",
      1="Homogenized block",
      2="Homogenized lamination"}, Name StrCat[mgeo,"Model"] }
  nlam = {2*10, Name StrCat[mgeo,"Number of laminations"]}

  Flag_Symmetry = {2, Choices{
      0="No symmetry",
      1="1/2 of the model (at y=0)",
      2="1/4 of the model (at y=0 & x=0)"},
    Name StrCat[mgeo,"Symmetry at x=0"]}

  Flag_Recombine = {1, Choices{0,1}, Name StrCat[mgeo, "Recombine"]}
];

// in geo file => Flag_Fine =0 homog. block,  =1 ref. fine, =2 homog. lamination,
Flag_Fine = (Flag_HomogType==2) ? Flag_HomogType : !Flag_HomogType ;

SymmetryFactor = (!Flag_Symmetry) ? 1:Flag_Symmetry*2 ;



mm = 1e-3 ;

DefineConstant[
  dlam = { 0.50, Name StrCat[mgeo2,"0lamination thickness"], Units 'mm', Closed 1}
  de   = { 0.05, Name StrCat[mgeo2,"1isolation thickness"], Units 'mm'} // 0.07
  fillfactor = { dlam/(dlam+de), Name StrCat[mgeo2,"2fill factor"], Units '', ReadOnly 1}
  w_lam = { 10, Name StrCat[mgeo2,"3lamination width"], Units 'mm'}
];


dlam = dlam*mm;
de   = de  *mm;
e    = dlam+de;

h = nlam*dlam + (nlam-1)*(e-dlam); // height lamination stack

w_lam = w_lam*mm;

// inductor dimensions
r1 = 8.4*mm;
r2 = 9*mm;
w_ind = r2-r1; //1*mm ;

rla_ind = 2*e; // minimum distance between lamination and inductor (radius)

//---------------------------------------------------------------------------

xlam = w_lam/2 ; //h/2 ;
ylam = h/2 ;

x_air = 1.5*(h/2+rla_ind+w_ind);
y_air = 1.5*(h/2+rla_ind+w_ind) ;

area_ind = w_ind*w_lam/2 + w_ind*h/2 + Pi/4*((rla_ind+w_ind)^2-rla_ind^2);
Printf("Area of 1/4 of the inductor %g", area_ind);


// Some material characteristics
// ==================================
//sigmaLam  = 5e6 ;
//sigmaCu   = 5.9e7  ; // conductivity of copper [S/m]
//sigmaIsol = sigmaLam/1e6 ; // slightly conducting, as if we had a shortcircuit

//murIron = 2500 ; // To use in linear case

//============================
// Physical numbers
//============================
IRON     = 1000 ; // all laminations or complete block (if homog); first lamination 10001 and so on.
ELECIRON = 1500 ;

SKINIRON   =1100 ; // skin of all lamination or of complete block (if homog); skin of first lamination 20001 and so on

SKINIRON_R =1300; // right side
SKINIRON_L =1400; // left side

IND =       2000;
SKININD =   2100;
SKININDIN = 2101;

AIR   = 6000;

ISOL  = 3000; // Between laminations

SYMMETRY_Y0 = 11111; // middle
SYMMETRY_X0 = 22222; // axis

OUTERBND  = 33333;

CORNER = 4444;


SKINMETAL = 5555; // Homog cases
