Group {
  Domain_Lin = Region[{Domain_L}] ;
  Domain_NonLin = Region[{Domain_NL}] ;
}

Include "matlab/PQ.pro" ;
Include "matlab/gausspoints_Ngp5.pro";

Function {

  nuIron = nu_fe;
  sigmaLam = sigma_fe;

  //lambda = fillfactor ; //la = lambda ;
  sigmaH[] = sigmaLam/fillfactor;


  //Legendre Polynomials (used in post-processing)
  yy[] = ((Fmod[$Y,e]-e/2)*2/dlam) ; //x = z/(dlam/2), e = dlam+de

  alpha_0[] = 1 ; // zero order
  alpha_2[] = 1/2*(-1+3*yy[]^2) ; //2nd order
  alpha_4[] = 1/8*(35*yy[]^4-30*yy[]^2+3); //4th order
  alpha_6[] = 1/16*(231*yy[]^6-315*yy[]^4+105*yy[]^2-5);
  alpha_8[] = 1/128*(6435*yy[]^8-12012*yy[]^6+6930*yy[]^4-1260*yy[]^2+35);

  dalpha_0[] = 0. ;
  dalpha_2[] = 3*yy[]*2/dlam ;
  dalpha_4[] = 1/2*(35*yy[]^3-15*yy[])*2/dlam; //4th order
  dalpha_6[] = 1/8*(231*3*yy[]^5-315*2*yy[]^3+105*yy[])*2/dlam;
  dalpha_8[] = 1/64*(6435*4*yy[]^7-12012*3*yy[]^5+6930*2*yy[]^3-1260*yy[])*2/dlam;

  ialpha_0[] = yy[]*dlam/2 ;
  ialpha_2[] = 1/2*(yy[]^3-yy[])*dlam/2 ;
  ialpha_4[] = 1/8*(7*yy[]^5-10*yy[]^3+3*yy[])*dlam/2;
  ialpha_6[] = 1/16*(33*yy[]^7-63*yy[]^5+35*yy[]^3-5*yy[])*dlam/2;
  ialpha_8[] = 1/128*(6435*yy[]^9/9-12012*yy[]^7/7+6930*yy[]^5/5-1260*yy[]^3/3+35*yy[])*dlam/2;


  If(Flag_ComplexReluctivity) // complex stuff
    delta = Sqrt[2*nuIron/sigmaLam/Omega] ;
    ds = dlam/delta ;
    Printf("delta = %g mm",delta*1000) ;
    Printf("ds = %g",ds) ;
    Printf("Flag_ComplexReluctivity = %g ; ORDER = %g", Flag_ComplexReluctivity, ORDER);

    //Definition of Sinh, Cosh and Tanh in terms of the real and imaginary parts of a complex argument
    Sinh_[] = Complex[ Cos[Im[$1]]*Sinh[Re[$1]], Sin[Im[$1]]*Cosh[Re[$1]] ];
    Cosh_[] = Complex[ Cos[Im[$1]]*Cosh[Re[$1]], Sin[Im[$1]]*Sinh[Re[$1]] ];
    Tanh_[] = Sinh_[$1]/Cosh_[$1];

    // G[] => Function used in L.Krahenbuhl's article IEEE Trans. Mag. vol.40, no. 2, pp. 912-915, 2004.
    G[] = Tanh_[ds/2*Complex[1,1]]/(ds/2*Complex[1,1]);

    // Gr = ds/2 * (Sinh[ds]+Sin[ds])/(Cosh[ds]-Cos[ds]) ;
    // Gi = ds/2 * (Sinh[ds]-Sin[ds])/(Cosh[ds]-Cos[ds]) ;
    // G[Iron] = Complex[Gr,Gi];

    If(ORDER==0) //Analytical
      nuCplx[] = nuIron/G[] ;
    EndIf
    If(ORDER==-1)   // Approx ORDER = 0
      nuCplx[] = Complex[ nuIron, sigmaLam * dlam^2/12. * Omega ];
    EndIf
    If(ORDER==-2)   // Approx ORDER = 2
      // nuIron_r2 = nuIron * (1+ 49*ds^4/(8820+20*ds^4) ) ;
      // nuIron_i2 = nuIron * (ds^2/6-7*ds^6/(26460+60*ds^4) ) ;
      // nu[#{DomainLam}] = Complex[nuIron_r2, nuIron_i2]*fillfactor ;
      // From matlab: nu1s = (ds^6*i + 69*ds^4 + ds^2*(1470*i) + 8820)/(20*(ds^4 + 441))
      nuCplx[] = nuIron * Complex[
        (69*ds^4 + 8820)/(8820+20*ds^4), (ds^6 + ds^2*1470)/(8820+20*ds^4) ];
    EndIf
    If(ORDER==-3)   // Approx ORDER = 4
      // From matlab:
      // collect(nu2s) =
      // (ds^10*1i + 300*ds^8 + ds^6*29520i + 1353240*ds^4 + ds^2*27442800i + 164656800)/(42*ds^8 + 438480*ds^4 + 164656800)
      // collect(real(nu2s)) = (50*ds^8 + 225540*ds^4 + 27442800)/(7*ds^8 + 73080*ds^4 + 27442800)
      // collect(imag(nu2s)) = (ds^10 + 29520*ds^6 + 27442800*ds^2)/(42*ds^8 + 438480*ds^4 + 164656800)
      nuCplx[] = nuIron * Complex[
        (50*ds^8 + 225540*ds^4 + 27442800)/(7*ds^8 + 73080*ds^4 + 27442800),
        (ds^10 + 29520*ds^6 + 27442800*ds^2)/(42*ds^8 + 438480*ds^4 + 164656800)];
    EndIf
    muCplx[] = 1/nuCplx[];
    nu[#{DomainLam}] = nuCplx[]/fillfactor ; //with homog block

    nuOm[] = Complex[ Omega*Im[nuCplx[]], -Re[nuCplx[]] ];
    muOm[] = Complex[-Omega*Im[muCplx[]], -Re[muCplx[]] ];

    // from Patrick's files
    Ytransf[] = Fmod[Y[]-de/2., dlam+de] -dlam/2. ;
    FACJ[] =
    (Ytransf[]>=-dlam/2. && Ytransf[]<=dlam/2.)?
    sigmaLam*dlam*2*Pi*Freq*Complex[0,1] / 2 / Sinh_[Complex[1,1]/delta*dlam/2]*
    Sinh_[Complex[1,1]/delta* Ytransf[] ] : 0. ;

    FACB[] =
    (Ytransf[]>=-dlam/2. && Ytransf[]<=dlam/2.)?
    sigmaLam*dlam*2*Pi*Freq*Complex[0,1] / 2 / Sinh_[Complex[1,1]/delta*dlam/2]*
    delta*Complex[1,-1]/2 /nuIron *
    Cosh_[Complex[1,1]/delta* Ytransf[] ] : 0. ;

  EndIf

}

FunctionSpace {


  ORDERMAX=4; //1 (zero order); 2 (2nd order); 3 (4th order); 4 (6th order); homogenization approx
  // Auxiliary BFs {d a_i}
  For i In {2:ORDERMAX}
    { Name Hcurl_a~{2*i-2} ; Type Form1 ;
      BasisFunction {
        { Name se ; NameOfCoef ae ; Function BF_Edge ;
          Support DomainLam ; Entity EdgesOf[ All ] ; }
      }
      Constraint {
        { NameOfCoef ae  ; EntityType EdgesOf ; NameOfConstraint MagneticVectorPotential ; }
      }
    }
  EndFor
  // Auxiliary BFs {b_i}
  For i In {2:ORDERMAX}
    { Name Hb~{2*i-2} ; Type Form2 ;
      BasisFunction {
        { Name se ; NameOfCoef ae ; Function BF_Facet ;
          Support DomainLam ; Entity FacetsOf[ All ] ; }
      }
    }
  EndFor


  // Store hystory for every integration point along thickness
  For i In {1:nbrQuadraturePnts}
    { Name H_hysteresis~{i} ; Type Vector;
      BasisFunction {
        // { Name sex ; NameOfCoef aex ; Function BF_VolumeX ; Support DomainLam ; Entity VolumesOf[ DomainLam ] ; }
        // { Name sey ; NameOfCoef aey ; Function BF_VolumeY ; Support DomainLam ; Entity VolumesOf[ DomainLam ] ; }
        { Name sez ; NameOfCoef aez ; Function BF_VolumeZ ; Support DomainLam ; Entity VolumesOf[ DomainLam ] ; }
      }
    }
  EndFor

}


// -------------------------------------------------------
// Terms in homogenized formulations for laminations
// -------------------------------------------------------

Macro Macro_Terms_DomainLamC_sigma

  If(ORDER >= 1) //ORDER = 0
    Galerkin { DtDof[ sigmaH[]*dlam^2 * q_0_0 * Dof{d a}, {d a} ] ;
      In DomainLamC ; Jacobian Vol ; Integration II ; }
  EndIf
  If (ORDER > 1) //ORDER = 2
    Galerkin { DtDof[ sigmaH[]*dlam^2 * q_0_2 * Dof{b_2}, {d a} ] ;
      In DomainLamC ; Jacobian Vol ; Integration II ; }
    Galerkin { DtDof[ sigmaH[]*dlam^2 * q_0_2 * Dof{d a}, {b_2} ] ;
      In DomainLamC ; Jacobian Vol ; Integration II ; }
    Galerkin { DtDof[ sigmaH[]*dlam^2 * q_2_2 * Dof{b_2}, {b_2} ] ;
      In DomainLamC ; Jacobian Vol ; Integration II ; }
  EndIf

  If(ORDER > 2) //ORDER = 4
    // 0 = nu*b_2/5 + sigma*d^2*(-dt(b0)/60   + dt(b_2)/210 - dt(b_4)/1260)
    // 0 = nu*b_4/9 + sigma*d^2*(-dt(b_2)/1260 + dt(b_4)/1386)

    Galerkin { DtDof[ sigmaH[]*dlam^2 * q_2_4 *Dof{b_4},  {b_2} ];
      In DomainLamC; Jacobian Vol ; Integration II; }
    Galerkin { DtDof[ sigmaH[]*dlam^2 * q_2_4 *Dof{b_2} , {b_4} ];
      In DomainLamC; Jacobian Vol ; Integration II; }
    Galerkin { DtDof[ sigmaH[]*dlam^2 * q_4_4 *Dof{b_4} , {b_4} ];
      In DomainLamC; Jacobian Vol ; Integration II; }
  EndIf

  If(ORDER > 3) //ORDER = 6
    Galerkin { DtDof[ sigmaH[]*dlam^2 * q_4_6 * Dof{b_6}, {b_4} ];
      In DomainLamC; Jacobian Vol ; Integration II; }
    Galerkin { DtDof[ sigmaH[]*dlam^2 * q_4_6 * Dof{b_4}, {b_6} ];
      In DomainLamC; Jacobian Vol ; Integration II; }
    Galerkin { DtDof[ sigmaH[]*dlam^2 * q_6_6 * Dof{b_6}, {b_6} ];
      In DomainLamC; Jacobian Vol ; Integration II; }
  EndIf

Return

//---------------------------------------------------------------
//---------------------------------------------------------------

Macro Macro_Terms_DomainLam_nu

If(!NbrRegions[Domain_NonLin]) // linear case
  For k In {2:ORDER} // ORDER=2 => p_2;  ORDER=3 => p_4; ORDER=4 => p_6;
    Galerkin { [ p~{2*k-2} * nu[] * Dof{b~{2*k-2}}, {b~{2*k-2}} ] ;
      In DomainLam ; Jacobian Vol ; Integration II; }
  EndFor
  /*
  Galerkin { [ p_2 * nu[] * Dof{b_2} , {b_2} ]; In DomainLam; Jacobian Vol ; Integration II; }
  Galerkin { [ p_4 * nu[] * Dof{b_4} , {b_4} ]; In DomainLam; Jacobian Vol ; Integration II; }
  Galerkin { [ p_6 * nu[] * Dof{b_6} , {b_6} ]; In DomainLam; Jacobian Vol ; Integration II; }
  */

Else // Directly using gauss points, nu[] and dhdb_NL[]

  If (ORDER == 2)
    For i In{1:nbrQuadraturePnts}
      Galerkin { [
          SetVariable[ nu[ SetVariable[({d a}+c_2~{i}*{b_2})/fillfactor]{$bprev} ] ]{$nuprev} * Dof{d a} * w~{i} , {d a} ] ; // h_00
        In DomainLam ; Jacobian Vol ; Integration I1p ; }
      Galerkin { [  $nuprev * c_2~{i}   * Dof{b_2} * w~{i} , {d a} ] ; In DomainLam ; Jacobian Vol ; Integration I1p ; }// h_02
      Galerkin { [  $nuprev * c_2~{i}   * Dof{d a} * w~{i} , {b_2} ] ; In DomainLam ; Jacobian Vol ; Integration I1p ; }// h_20
      Galerkin { [  $nuprev * c_2~{i}^2 * Dof{b_2} * w~{i} , {b_2} ] ; In DomainLam ; Jacobian Vol ; Integration I1p ; }// h_22

      Galerkin { JacNL[ SetVariable[1/fillfactor * dhdb_NL[$bprev]* w~{i}]{$dhdb_wi} * Dof{d a} , {d a} ] ; In DomainLam ; Jacobian Vol ; Integration I1p ; } //dhdb_00
      Galerkin { JacNL[ $dhdb_wi * c_2~{i}   * Dof{b_2} , {d a} ] ; In DomainLam ; Jacobian Vol ; Integration I1p ; } //dhdb_02
      Galerkin { JacNL[ $dhdb_wi * c_2~{i}   * Dof{d a} , {b_2} ] ; In DomainLam ; Jacobian Vol ; Integration I1p ; } //dhdb_20
      Galerkin { JacNL[ $dhdb_wi * c_2~{i}^2 * Dof{b_2} , {b_2} ] ; In DomainLam ; Jacobian Vol ; Integration I1p ; } //dhdb_22
    EndFor
  EndIf

  If( ORDER == 3 ) //ORDER = 4
    //Galerkin { [nu[]/9 *Dof{b_4} , {b_4} ]; In DomainLam ; Jacobian Vol ; Integration I; }
    For i In{1:nbrQuadraturePnts}
      Galerkin { [
          SetVariable[ nu[ SetVariable[({d a}+c_2~{i}*{b_2}+c_4~{i}*{b_4})/fillfactor]{$bprev} ] ]{$nuprev} * Dof{d a} * w~{i} , {d a} ] ; // h_00
        In DomainLam ; Jacobian Vol ; Integration I1p ; }
      Galerkin { [ $nuprev * c_2~{i} * Dof{b_2} * w~{i} , {d a} ] ; In DomainLam ; Jacobian Vol ; Integration I1p ; } // h_02
      Galerkin { [ $nuprev * c_4~{i} * Dof{b_4} * w~{i} , {d a} ] ; In DomainLam ; Jacobian Vol ; Integration I1p ; } // h_04

      Galerkin { [ $nuprev * c_2~{i}   * Dof{d a} * w~{i} , {b_2} ] ; In DomainLam ; Jacobian Vol ; Integration I1p ; } // h_20
      Galerkin { [ $nuprev * c_2~{i}^2 * Dof{b_2} * w~{i} , {b_2} ] ; In DomainLam ; Jacobian Vol ; Integration I1p ; } // h_22
      Galerkin { [ $nuprev * c_2~{i} * c_4~{i} * Dof{b_4} * w~{i} , {b_2} ] ; In DomainLam ; Jacobian Vol ; Integration I1p ; } // h_24

      Galerkin { [ $nuprev * c_4~{i} * Dof{d a} * w~{i} , {b_4} ] ; In DomainLam ; Jacobian Vol ; Integration I1p ; } // h_40
      Galerkin { [ $nuprev * c_4~{i} * c_2~{i} * Dof{b_2} * w~{i} , {b_4} ] ; In DomainLam ; Jacobian Vol ; Integration I1p ; } // h_42
      Galerkin { [ $nuprev * c_4~{i}^2 * Dof{b_4} * w~{i} , {b_4} ] ; In DomainLam ; Jacobian Vol ; Integration I1p ; } // h_44

      Galerkin { JacNL[ SetVariable[ 1/fillfactor * dhdb_NL[$bprev]* w~{i}]{$dhdb_wi}   * Dof{d a} , {d a} ] ;In DomainLam ; Jacobian Vol ; Integration I1p ; } //dhdb_00
      Galerkin { JacNL[ $dhdb_wi *          c_2~{i} * Dof{b_2} , {d a} ] ; In DomainLam ; Jacobian Vol ; Integration I1p ; } //dhdb_02
      Galerkin { JacNL[ $dhdb_wi *          c_4~{i} * Dof{b_4} , {d a} ] ; In DomainLam ; Jacobian Vol ; Integration I1p ; } //dhdb_04

      Galerkin { JacNL[ $dhdb_wi * c_2~{i} *           Dof{d a} , {b_2} ] ; In DomainLam ; Jacobian Vol ; Integration I1p ; } //dhdb_02
      Galerkin { JacNL[ $dhdb_wi * c_2~{i} * c_2~{i} * Dof{b_2} , {b_2} ] ; In DomainLam ; Jacobian Vol ; Integration I1p ; } //dhdb_22
      Galerkin { JacNL[ $dhdb_wi * c_2~{i} * c_4~{i} * Dof{b_4} , {b_2} ] ; In DomainLam ; Jacobian Vol ; Integration I1p ; } //dhdb_24

      Galerkin { JacNL[ $dhdb_wi * c_4~{i} *           Dof{d a} , {b_4} ] ; In DomainLam ; Jacobian Vol ; Integration I1p ; } //dhdb_04
      Galerkin { JacNL[ $dhdb_wi * c_4~{i} * c_4~{i} * Dof{b_4} , {b_4} ] ; In DomainLam ; Jacobian Vol ; Integration I1p ; } //dhdb_44
      Galerkin { JacNL[ $dhdb_wi * c_4~{i} * c_2~{i} * Dof{b_2} , {b_4} ] ; In DomainLam ; Jacobian Vol ; Integration I1p ; } //dhdb_24
    EndFor
  EndIf

    If( ORDER == 4 ) //ORDER = 6
    //Galerkin { [nu[]/13 *Dof{b_6} , {b_6} ]; In DomainLam ; Jacobian Vol ; Integration I; }
    For i In{1:nbrQuadraturePnts}
      Galerkin { [
          SetVariable[ nu[ SetVariable[({d a}+c_2~{i}*{b_2}+c_4~{i}*{b_4}+c_6~{i}*{b_6})/fillfactor]{$bprev} ] ]{$nuprev} * Dof{d a} * w~{i} , {d a} ] ; // h_00
        In DomainLam ; Jacobian Vol ; Integration I1p ; }
      Galerkin { [ $nuprev * c_2~{i} * Dof{b_2} * w~{i} , {d a} ] ; In DomainLam ; Jacobian Vol ; Integration I1p ; } // h_02
      Galerkin { [ $nuprev * c_4~{i} * Dof{b_4} * w~{i} , {d a} ] ; In DomainLam ; Jacobian Vol ; Integration I1p ; } // h_04
      Galerkin { [ $nuprev * c_6~{i} * Dof{b_6} * w~{i} , {d a} ] ; In DomainLam ; Jacobian Vol ; Integration I1p ; } // h_06

      Galerkin { [ $nuprev * c_2~{i}   * Dof{d a} * w~{i} , {b_2} ] ; In DomainLam ; Jacobian Vol ; Integration I1p ; } // h_20
      Galerkin { [ $nuprev * c_2~{i}^2 * Dof{b_2} * w~{i} , {b_2} ] ; In DomainLam ; Jacobian Vol ; Integration I1p ; } // h_22
      Galerkin { [ $nuprev * c_2~{i} * c_4~{i} * Dof{b_4} * w~{i} , {b_2} ] ; In DomainLam ; Jacobian Vol ; Integration I1p ; } // h_24
      Galerkin { [ $nuprev * c_2~{i} * c_6~{i} * Dof{b_6} * w~{i} , {b_2} ] ; In DomainLam ; Jacobian Vol ; Integration I1p ; } // h_26

      Galerkin { [ $nuprev * c_4~{i} * Dof{d a} * w~{i} , {b_4} ] ; In DomainLam ; Jacobian Vol ; Integration I1p ; } // h_40
      Galerkin { [ $nuprev * c_4~{i} * c_2~{i} * Dof{b_2} * w~{i} , {b_4} ] ; In DomainLam ; Jacobian Vol ; Integration I1p ; } // h_42
      Galerkin { [ $nuprev * c_4~{i}^2 * Dof{b_4} * w~{i} , {b_4} ] ; In DomainLam ; Jacobian Vol ; Integration I1p ; } // h_44
      Galerkin { [ $nuprev * c_4~{i} * c_6~{i} * Dof{b_6} * w~{i} , {b_4} ] ; In DomainLam ; Jacobian Vol ; Integration I1p ; } // h_46

      Galerkin { JacNL[ SetVariable[ 1/fillfactor * dhdb_NL[$bprev]* w~{i}]{$dhdb_wi}   * Dof{d a} , {d a} ] ;
        In DomainLam ; Jacobian Vol ; Integration I1p ; } //dhdb_00
      Galerkin { JacNL[ $dhdb_wi *          c_2~{i} * Dof{b_2} , {d a} ] ; In DomainLam ; Jacobian Vol ; Integration I1p ; } //dhdb_02
      Galerkin { JacNL[ $dhdb_wi *          c_4~{i} * Dof{b_4} , {d a} ] ; In DomainLam ; Jacobian Vol ; Integration I1p ; } //dhdb_04
      Galerkin { JacNL[ $dhdb_wi *          c_6~{i} * Dof{b_6} , {d a} ] ; In DomainLam ; Jacobian Vol ; Integration I1p ; } //dhdb_06

      Galerkin { JacNL[ $dhdb_wi * c_2~{i} *           Dof{d a} , {b_2} ] ; In DomainLam ; Jacobian Vol ; Integration I1p ; } //dhdb_20
      Galerkin { JacNL[ $dhdb_wi * c_2~{i} * c_2~{i} * Dof{b_2} , {b_2} ] ; In DomainLam ; Jacobian Vol ; Integration I1p ; } //dhdb_22
      Galerkin { JacNL[ $dhdb_wi * c_2~{i} * c_4~{i} * Dof{b_4} , {b_2} ] ; In DomainLam ; Jacobian Vol ; Integration I1p ; } //dhdb_24
      Galerkin { JacNL[ $dhdb_wi * c_2~{i} * c_6~{i} * Dof{b_6} , {b_2} ] ; In DomainLam ; Jacobian Vol ; Integration I1p ; } //dhdb_26

      Galerkin { JacNL[ $dhdb_wi * c_4~{i} *           Dof{d a} , {b_4} ] ; In DomainLam ; Jacobian Vol ; Integration I1p ; } //dhdb_40
      Galerkin { JacNL[ $dhdb_wi * c_4~{i} * c_2~{i} * Dof{b_2} , {b_4} ] ; In DomainLam ; Jacobian Vol ; Integration I1p ; } //dhdb_42
      Galerkin { JacNL[ $dhdb_wi * c_4~{i} * c_4~{i} * Dof{b_4} , {b_4} ] ; In DomainLam ; Jacobian Vol ; Integration I1p ; } //dhdb_44
      Galerkin { JacNL[ $dhdb_wi * c_4~{i} * c_6~{i} * Dof{b_6} , {b_4} ] ; In DomainLam ; Jacobian Vol ; Integration I1p ; } //dhdb_46

      Galerkin { JacNL[ $dhdb_wi * c_6~{i} *           Dof{d a} , {b_6} ] ; In DomainLam ; Jacobian Vol ; Integration I1p ; } //dhdb_60
      Galerkin { JacNL[ $dhdb_wi * c_6~{i} * c_2~{i} * Dof{b_2} , {b_6} ] ; In DomainLam ; Jacobian Vol ; Integration I1p ; } //dhdb_62
      Galerkin { JacNL[ $dhdb_wi * c_6~{i} * c_4~{i} * Dof{b_4} , {b_6} ] ; In DomainLam ; Jacobian Vol ; Integration I1p ; } //dhdb_64
      Galerkin { JacNL[ $dhdb_wi * c_6~{i} * c_6~{i} * Dof{b_6} , {b_6} ] ; In DomainLam ; Jacobian Vol ; Integration I1p ; } //dhdb_66
    EndFor
  EndIf

EndIf

Return

//---------------------------------------------------------------
//---------------------------------------------------------------

Macro Macro_Terms_DomainLam_h

If(!NbrRegions[Domain_NonLin]) // linear case

  For k In {2:ORDER} // ORDER=2 => p_2;  ORDER=3 => p_4; ORDER=4 => p_6;
    Galerkin { [ p~{2*k-2} * nu[] * Dof{b~{2*k-2}}, {b~{2*k-2}} ] ;
      In DomainLam ; Jacobian Vol ; Integration II; }
  EndFor

Else // Directly using gauss points, h[] and dhdb[]

  If (ORDER == 2) //ORDER = 2
    For i In{1:nbrQuadraturePnts}
      Galerkin { [ // h_0
          SetVariable[ h[ SetVariable[({d a}+c_2~{i}*{b_2})/fillfactor]{$bprev} ] ]{$hprev} * w~{i} , {d a} ] ;
        In DomainLam ; Jacobian Vol ; Integration I1p ; }
      Galerkin { [ $hprev * c_2~{i} * w~{i} , {b_2} ] ;In DomainLam ; Jacobian Vol ; Integration I1p ; } // h_2

      Galerkin { JacNL[ SetVariable[1/fillfactor * dhdb[$bprev]* w~{i}]{$dhdb_wi} * Dof{d a} , {d a} ] ;
        In DomainLam ; Jacobian Vol ; Integration I1p ; } //dhdb_00
      Galerkin { JacNL[ $dhdb_wi *           c_2~{i} * Dof{b_2} , {d a} ] ; In DomainLam ; Jacobian Vol ; Integration I1p ; } //dhdb_02
      Galerkin { JacNL[ $dhdb_wi * c_2~{i} * c_2~{i} * Dof{b_2} , {b_2} ] ; In DomainLam ; Jacobian Vol ; Integration I1p ; } //dhdb_22
      Galerkin { JacNL[ $dhdb_wi * c_2~{i} *           Dof{d a} , {b_2} ] ; In DomainLam ; Jacobian Vol ; Integration I1p ; } //dhdb_20
    EndFor
  EndIf

  If( ORDER == 3 ) //ORDER = 4
    //Galerkin { [nu[]/9 *Dof{b_4} , {b_4} ]; In DomainLam ; Jacobian Vol ; Integration I; }
    For i In{1:nbrQuadraturePnts}
      Galerkin { [
          SetVariable[ h[ SetVariable[({d a}+c_2~{i}*{b_2}+c_4~{i}*{b_4})/fillfactor]{$bprev} ] ]{$hprev} * w~{i} , {d a} ] ; // h_0
        In DomainLam ; Jacobian Vol ; Integration I1p ; }
      Galerkin { [ $hprev * c_2~{i} * w~{i} , {b_2} ] ;  In DomainLam ; Jacobian Vol ; Integration I1p ; }// h_2
      Galerkin { [ $hprev * c_4~{i} * w~{i} , {b_4} ] ;  In DomainLam ; Jacobian Vol ; Integration I1p ; }// h_4

      Galerkin { JacNL[ SetVariable[ 1/fillfactor * dhdb[$bprev]* w~{i}]{$dhdb_wi}   * Dof{d a} , {d a} ] ;
        In DomainLam ; Jacobian Vol ; Integration I1p ; } //dhdb_00
      Galerkin { JacNL[ $dhdb_wi *          c_2~{i} * Dof{b_2} , {d a} ] ; In DomainLam ; Jacobian Vol ; Integration I1p ; } //dhdb_02
      Galerkin { JacNL[ $dhdb_wi *          c_4~{i} * Dof{b_4} , {d a} ] ; In DomainLam ; Jacobian Vol ; Integration I1p ; } //dhdb_04

      Galerkin { JacNL[ $dhdb_wi * c_2~{i} *           Dof{d a} , {b_2} ] ; In DomainLam ; Jacobian Vol ; Integration I1p ; } //dhdb_02
      Galerkin { JacNL[ $dhdb_wi * c_2~{i} * c_2~{i} * Dof{b_2} , {b_2} ] ; In DomainLam ; Jacobian Vol ; Integration I1p ; } //dhdb_22
      Galerkin { JacNL[ $dhdb_wi * c_2~{i} * c_4~{i} * Dof{b_4} , {b_2} ] ; In DomainLam ; Jacobian Vol ; Integration I1p ; } //dhdb_24

      Galerkin { JacNL[ $dhdb_wi * c_4~{i} *           Dof{d a} , {b_4} ] ; In DomainLam ; Jacobian Vol ; Integration I1p ; } //dhdb_04
      Galerkin { JacNL[ $dhdb_wi * c_4~{i} * c_4~{i} * Dof{b_4} , {b_4} ] ; In DomainLam ; Jacobian Vol ; Integration I1p ; } //dhdb_44
      Galerkin { JacNL[ $dhdb_wi * c_4~{i} * c_2~{i} * Dof{b_2} , {b_4} ] ; In DomainLam ; Jacobian Vol ; Integration I1p ; } //dhdb_24
    EndFor
  EndIf

    If( ORDER == 4 ) //ORDER = 6
    //Galerkin { [nu[]/13 *Dof{b_6} , {b_6} ]; In DomainLam ; Jacobian Vol ; Integration I; }
    For i In{1:nbrQuadraturePnts}
      Galerkin { [
          SetVariable[ h[ SetVariable[({d a}+c_2~{i}*{b_2}+c_4~{i}*{b_4}+c_6~{i}*{b_6})/fillfactor]{$bprev} ] ]{$hprev} * w~{i} , {d a} ] ; // h_0
        In DomainLam ; Jacobian Vol ; Integration I1p ; }
      Galerkin { [ $hprev * c_2~{i} * w~{i} , {b_2} ] ;  In DomainLam ; Jacobian Vol ; Integration I1p ; }// h_2
      Galerkin { [ $hprev * c_4~{i} * w~{i} , {b_4} ] ;  In DomainLam ; Jacobian Vol ; Integration I1p ; }// h_4
      Galerkin { [ $hprev * c_6~{i} * w~{i} , {b_6} ] ;  In DomainLam ; Jacobian Vol ; Integration I1p ; }// h_6

      Galerkin { JacNL[ SetVariable[ 1/fillfactor * dhdb[$bprev]* w~{i}]{$dhdb_wi}   * Dof{d a} , {d a} ] ;
        In DomainLam ; Jacobian Vol ; Integration I1p ; } //dhdb_00
      Galerkin { JacNL[ $dhdb_wi *          c_2~{i} * Dof{b_2} , {d a} ] ; In DomainLam ; Jacobian Vol ; Integration I1p ; } //dhdb_02
      Galerkin { JacNL[ $dhdb_wi *          c_4~{i} * Dof{b_4} , {d a} ] ; In DomainLam ; Jacobian Vol ; Integration I1p ; } //dhdb_04
      Galerkin { JacNL[ $dhdb_wi *          c_6~{i} * Dof{b_6} , {d a} ] ; In DomainLam ; Jacobian Vol ; Integration I1p ; } //dhdb_06

      Galerkin { JacNL[ $dhdb_wi * c_2~{i} *           Dof{d a} , {b_2} ] ; In DomainLam ; Jacobian Vol ; Integration I1p ; } //dhdb_20
      Galerkin { JacNL[ $dhdb_wi * c_2~{i} * c_2~{i} * Dof{b_2} , {b_2} ] ; In DomainLam ; Jacobian Vol ; Integration I1p ; } //dhdb_22
      Galerkin { JacNL[ $dhdb_wi * c_2~{i} * c_4~{i} * Dof{b_4} , {b_2} ] ; In DomainLam ; Jacobian Vol ; Integration I1p ; } //dhdb_24
      Galerkin { JacNL[ $dhdb_wi * c_2~{i} * c_6~{i} * Dof{b_6} , {b_2} ] ; In DomainLam ; Jacobian Vol ; Integration I1p ; } //dhdb_26

      Galerkin { JacNL[ $dhdb_wi * c_4~{i} *           Dof{d a} , {b_4} ] ; In DomainLam ; Jacobian Vol ; Integration I1p ; } //dhdb_40
      Galerkin { JacNL[ $dhdb_wi * c_4~{i} * c_2~{i} * Dof{b_2} , {b_4} ] ; In DomainLam ; Jacobian Vol ; Integration I1p ; } //dhdb_42
      Galerkin { JacNL[ $dhdb_wi * c_4~{i} * c_4~{i} * Dof{b_4} , {b_4} ] ; In DomainLam ; Jacobian Vol ; Integration I1p ; } //dhdb_44
      Galerkin { JacNL[ $dhdb_wi * c_4~{i} * c_6~{i} * Dof{b_6} , {b_4} ] ; In DomainLam ; Jacobian Vol ; Integration I1p ; } //dhdb_46

      Galerkin { JacNL[ $dhdb_wi * c_6~{i} *           Dof{d a} , {b_6} ] ; In DomainLam ; Jacobian Vol ; Integration I1p ; } //dhdb_60
      Galerkin { JacNL[ $dhdb_wi * c_6~{i} * c_2~{i} * Dof{b_2} , {b_6} ] ; In DomainLam ; Jacobian Vol ; Integration I1p ; } //dhdb_62
      Galerkin { JacNL[ $dhdb_wi * c_6~{i} * c_4~{i} * Dof{b_4} , {b_6} ] ; In DomainLam ; Jacobian Vol ; Integration I1p ; } //dhdb_64
      Galerkin { JacNL[ $dhdb_wi * c_6~{i} * c_6~{i} * Dof{b_6} , {b_6} ] ; In DomainLam ; Jacobian Vol ; Integration I1p ; } //dhdb_66
    EndFor
  EndIf

EndIf

Return

//-----------------------------------------------------------------------------------

Macro Macro_Terms_DomainLam_hysteresis

If(NbrRegions[Domain_NL]) // nonlinear hysteretic case
  If (ORDER == 2) //ORDER = 2
    // for NR iterations
    // History of h to be kept for every quadrature point along lamination thickness
    For i In{1:nbrQuadraturePnts}
      Galerkin { [ Dof{h~{i}} , {h~{i}} ] ; In DomainLam ; Jacobian Vol ; Integration I1p ; }
      Galerkin { [ -h_Jiles[
            {h~{i}}[1],
            ({d a}[1]+ c_2~{i}*{b_2}[1])/fillfactor,
            ({d a}   + c_2~{i}*{b_2}   )/fillfactor]{List[hyst_FeSi]} , {h~{i}} ] ;
        In DomainLam ; Jacobian Vol ; Integration I1p ; }
      Galerkin { [ Dof{h~{i}} * w~{i}          , {d a} ] ; In DomainLam ; Jacobian Vol ; Integration I1p ; } // h_Jiles0
      Galerkin { [ Dof{h~{i}} * c_2~{i} * w~{i}, {b_2} ] ; In DomainLam ; Jacobian Vol ; Integration I1p ; } // h_Jiles2
    EndFor

    For i In{1:nbrQuadraturePnts} //dhdb_Jiles00, dhdb_Jiles02, dhdb_Jiles22, dhdb_Jiles20
      Galerkin { JacNL[ SetVariable[ 1/fillfactor*
            dhdb_Jiles[ {h~{i}}, ({d a}+c_2~{i}*{b_2})/fillfactor, {h~{i}}-{h~{i}}[1] ]{List[hyst_FeSi]}*w~{i} ]{$dhdb_jiles} * Dof{d a} , {d a} ] ;
        In DomainLam ; Jacobian Vol ; Integration I1p ; }
      Galerkin { JacNL[ $dhdb_jiles * c_2~{i}           * Dof{b_2} , {d a} ] ;
        In DomainLam ; Jacobian Vol ; Integration I1p ; }
      Galerkin { JacNL[ $dhdb_jiles * c_2~{i} * c_2~{i} * Dof{b_2} , {b_2} ] ;
        In DomainLam ; Jacobian Vol ; Integration I1p ; }
      Galerkin { JacNL[ $dhdb_jiles           * c_2~{i} * Dof{d a} , {b_2} ] ;
        In DomainLam ; Jacobian Vol ; Integration I1p ; }
    EndFor
  EndIf

  If (ORDER == 3) //ORDER = 4
    For i In{1:nbrQuadraturePnts}
      Galerkin { [ Dof{h~{i}} , {h~{i}} ] ;
        In DomainLam ; Jacobian Vol ; Integration I1p ; }
      Galerkin { [ -h_Jiles[{h~{i}}[1],
            ({d a}[1]+c_2~{i}*{b_2}[1]+c_4~{i}*{b_4}[1])/fillfactor,
            ({d a}+   c_2~{i}*{b_2}   +c_4~{i}*{b_4}   )/fillfactor]{List[hyst_FeSi]}, {h~{i}} ] ;
        In DomainLam ; Jacobian Vol ; Integration I1p ; }

      Galerkin { [ Dof{h~{i}}           * w~{i}, {d a} ] ; In DomainLam ; Jacobian Vol ; Integration I1p ; }
      Galerkin { [ Dof{h~{i}} * c_2~{i} * w~{i}, {b_2} ] ; In DomainLam ; Jacobian Vol ; Integration I1p ; }
      Galerkin { [ Dof{h~{i}} * c_4~{i} * w~{i}, {b_4} ] ; In DomainLam ; Jacobian Vol ; Integration I1p ; }
    EndFor

    For i In{1:nbrQuadraturePnts}
      //dhdb_Jiles 00, 02, 04, 22, 20, 24, 44, 40, 42
      Galerkin { JacNL[ SetVariable[ 1/fillfactor*dhdb_Jiles[
              {h~{i}},
              ({d a}+c_2~{i}*{b_2}+c_4~{i}*{b_4})/fillfactor,
              {h~{i}}-{h~{i}}[1] ]{List[hyst_FeSi]} * w~{i} ]{$dhdb_jiles} * Dof{d a} , {d a} ] ;
        In DomainLam ; Jacobian Vol ; Integration I1p ; }
      Galerkin { JacNL[ $dhdb_jiles * c_2~{i}          * Dof{b_2} , {d a} ] ; In DomainLam ; Jacobian Vol ; Integration I1p ; }
      Galerkin { JacNL[ $dhdb_jiles * c_4~{i}          * Dof{b_4} , {d a} ] ; In DomainLam ; Jacobian Vol ; Integration I1p ; }

      Galerkin { JacNL[ $dhdb_jiles * c_2~{i} * c_2~{i} * Dof{b_2} , {b_2} ] ; In DomainLam ; Jacobian Vol ; Integration I1p ; }
      Galerkin { JacNL[ $dhdb_jiles           * c_2~{i} * Dof{d a} , {b_2} ] ; In DomainLam ; Jacobian Vol ; Integration I1p ; }
      Galerkin { JacNL[ $dhdb_jiles * c_4~{i} * c_2~{i} * Dof{b_4} , {b_2} ] ; In DomainLam ; Jacobian Vol ; Integration I1p ; }

      Galerkin { JacNL[ $dhdb_jiles * c_4~{i} * c_4~{i} * Dof{b_4} , {b_4} ] ; In DomainLam ; Jacobian Vol ; Integration I1p ; }
      Galerkin { JacNL[ $dhdb_jiles           * c_4~{i} * Dof{d a} , {b_4} ] ; In DomainLam ; Jacobian Vol ; Integration I1p ; }
      Galerkin { JacNL[ $dhdb_jiles * c_2~{i} * c_4~{i} * Dof{b_2} , {b_4} ] ; In DomainLam ; Jacobian Vol ; Integration I1p ; }
    EndFor
  EndIf

  If (ORDER == 4) //ORDER = 6
    For i In {1:nbrQuadraturePnts}
      Galerkin { [ Dof{h~{i}} , {h~{i}} ] ;
        In DomainLam  ; Jacobian Vol ; Integration I1p ; }
      Galerkin { [ -h_Jiles[
            {h~{i}}[1],
            ({d a}[1]+c_2~{i}*{b_2}[1]+c_4~{i}*{b_4}[1]+c_6~{i}*{b_6}[1])/fillfactor,
            ({d a}+ c_2~{i}*{b_2}+c_4~{i}*{b_4}+c_6~{i}*{b_6})/fillfactor]{List[hyst_FeSi]}, {h~{i}} ] ;
        In DomainLam ; Jacobian Vol ; Integration I1p ; }

      Galerkin { [ Dof{h~{i}}           * w~{i}, {d a} ]  ; In DomainLam ; Jacobian Vol ; Integration I1p ; }
      Galerkin { [ Dof{h~{i}} * c_2~{i} * w~{i}, {b_2} ]  ; In DomainLam ; Jacobian Vol ; Integration I1p ; }
      Galerkin { [ Dof{h~{i}} * c_4~{i} * w~{i}, {b_4} ]  ; In DomainLam ; Jacobian Vol ; Integration I1p ; }
      Galerkin { [ Dof{h~{i}} * c_6~{i} * w~{i}, {b_6} ]  ; In DomainLam ; Jacobian Vol ; Integration I1p ; }
    EndFor

    For i In{1:nbrQuadraturePnts}
      //dhdb_Jiles 00, 02, 04, 06, 22, 20, 24, 26, 44, 40, 42, 46, 66, 60, 62, 64
      Galerkin { JacNL[ SetVariable[ 1/fillfactor*dhdb_Jiles[
              {h~{i}},
              ({d a}+c_2~{i}*{b_2}+c_4~{i}*{b_4}+c_6~{i}*{b_6})/fillfactor,
              {h~{i}}-{h~{i}}[1]]{List[hyst_FeSi]}*w~{i} ]{$dhdb_jiles} * Dof{d a} , {d a} ] ;
        In DomainLam ; Jacobian Vol ; Integration I1p ; }
      Galerkin { JacNL[ $dhdb_jiles * c_2~{i}          * Dof{b_2} , {d a} ] ; In DomainLam ; Jacobian Vol ; Integration I1p ; }
      Galerkin { JacNL[ $dhdb_jiles * c_4~{i}          * Dof{b_4} , {d a} ] ; In DomainLam ; Jacobian Vol ; Integration I1p ; }
      Galerkin { JacNL[ $dhdb_jiles * c_6~{i}          * Dof{b_6} , {d a} ] ; In DomainLam ; Jacobian Vol ; Integration I1p ; }

      Galerkin { JacNL[ $dhdb_jiles * c_2~{i} * c_2~{i} * Dof{b_2} , {b_2} ] ; In DomainLam ; Jacobian Vol ; Integration I1p ; }
      Galerkin { JacNL[ $dhdb_jiles           * c_2~{i} * Dof{d a} , {b_2} ] ; In DomainLam ; Jacobian Vol ; Integration I1p ; }
      Galerkin { JacNL[ $dhdb_jiles * c_4~{i} * c_2~{i} * Dof{b_4} , {b_2} ] ; In DomainLam ; Jacobian Vol ; Integration I1p ; }
      Galerkin { JacNL[ $dhdb_jiles * c_6~{i} * c_2~{i} * Dof{b_6} , {b_2} ] ; In DomainLam ; Jacobian Vol ; Integration I1p ; }

      Galerkin { JacNL[ $dhdb_jiles * c_4~{i} * c_4~{i} * Dof{b_4} , {b_4} ] ; In DomainLam ; Jacobian Vol ; Integration I1p ; }
      Galerkin { JacNL[ $dhdb_jiles           * c_4~{i} * Dof{d a} , {b_4} ] ; In DomainLam ; Jacobian Vol ; Integration I1p ; }
      Galerkin { JacNL[ $dhdb_jiles * c_2~{i} * c_4~{i} * Dof{b_2} , {b_4} ] ; In DomainLam ; Jacobian Vol ; Integration I1p ; }
      Galerkin { JacNL[ $dhdb_jiles * c_6~{i} * c_4~{i} * Dof{b_6} , {b_4} ] ; In DomainLam ; Jacobian Vol ; Integration I1p ; }

      Galerkin { JacNL[ $dhdb_jiles * c_4~{i} * c_6~{i} * Dof{b_4} , {b_6} ] ; In DomainLam ; Jacobian Vol ; Integration I1p ; }
      Galerkin { JacNL[ $dhdb_jiles           * c_6~{i} * Dof{d a} , {b_6} ] ; In DomainLam ; Jacobian Vol ; Integration I1p ; }
      Galerkin { JacNL[ $dhdb_jiles * c_2~{i} * c_6~{i} * Dof{b_2} , {b_6} ] ; In DomainLam ; Jacobian Vol ; Integration I1p ; }
      Galerkin { JacNL[ $dhdb_jiles * c_6~{i} * c_6~{i} * Dof{b_6} , {b_6} ] ; In DomainLam ; Jacobian Vol ; Integration I1p ; }
    EndFor

  EndIf



EndIf


Return



//-----------------------------------------------------------------------------------
// PostQuantities
//-----------------------------------------------------------------------------------

Macro PostQuantities_Homog

  { Name b~{0} ; Value { Local { [ {d a}/fillfactor ] ; In DomainLam ; Jacobian Vol ; } } }
  For k In{2:ORDER}
    { Name b~{2*k-2}  ; Value { Term { [ alpha~{2*k-2}[]*{b~{2*k-2}}/fillfactor ]  ; In DomainLam ; Jacobian Vol ; } } }
  EndFor

  { Name bz~{0} ; Value { Local { [ CompZ[{d a}] ] ; In DomainLam ; Jacobian Vol ; } } }
  For k In{2:ORDER}
    { Name bz~{2*k-2}  ; Value { Term { [ alpha~{2*k-2}[]*CompZ[{b~{2*k-2}}]/fillfactor ]  ; In DomainLam ; Jacobian Vol ; } } }
  EndFor

  { Name btot  ; Value {
      Term { [ {d a}/fillfactor ]  ; In DomainLam ; Jacobian Vol ; }
      For k In{2:ORDER}
        Term { [ alpha~{2*k-2}[]*{b~{2*k-2}}/fillfactor ]  ; In DomainLam ; Jacobian Vol ; }
      EndFor
    }
  }

  { Name j~{0} ;
    Value { Term { [ sigmaH[]*ialpha~{0}[]*Dt[{d a}]/\Vector[0,-1,0] ] ; In DomainLam ; Jacobian Vol ; } } }
  For k In{2:ORDER}
    { Name j~{2*k-2} ;
      Value { Term { [ sigmaH[]*ialpha~{2*k-2}[]*Dt[{b~{2*k-2}}]/\Vector[0,-1,0] ] ; In DomainLam ; Jacobian Vol ; } } }
  EndFor

  For k In {0:ORDERMAX:2}
    { Name  aa~{k} ; Value { Local { [  alpha~{k}[] ] ; In DomainLam ; Jacobian Vol ; } } }
    { Name daa~{k} ; Value { Local { [ dalpha~{k}[] ] ; In DomainLam ; Jacobian Vol ; } } }
    { Name iaa~{k} ; Value { Local { [ ialpha~{k}[] ] ; In DomainLam ; Jacobian Vol ; } } }
  EndFor


  If(Flag_ComplexReluctivity) // Frequency domain
    { Name j_beta  ; Value { Term { [ (Vector[0,1,0]*^{d a})*FACJ[] ] ; In DomainLam ; Jacobian Vol ;} } }
    { Name b_alpha ; Value { Term { [ {d a}*FACB[] ] ; In DomainLam ; Jacobian Vol ; } } }

    { Name ComplexPowerH ; Value { // real part -> eddy current losses; imag part-> magnetic energy
        Integral { [ SymmetryFactor*AxialLength*{d a}*Conj[nuOm[]*{d a} ] ];
          In DomainLam; Jacobian Vol ; Integration II ; }
      }
    }
  EndIf

  { Name JouleLossesH_local ;
    Value {
      If(Flag_ComplexReluctivity) // Frequency domain
        Term { [ Re[ {d a}*Conj[ nuOm[]*{d a} ] ] ];
          In DomainLamC ; Jacobian Vol ; }

      Else // Time domain

        Term { [ dlam^2*sigmaH[]*q_0_0*SquNorm[Dt[{d a}]] ] ;
          In DomainLamC ; Jacobian Vol ; }
        If (ORDER>1)
          Term { [   dlam^2*sigmaH[]*q_2_2*SquNorm[Dt[{b_2}]] ] ;
            In DomainLamC ; Jacobian Vol ; }
          Term { [ 2*dlam^2*sigmaH[]*q_0_2*Dt[{d a}]*Dt[{b_2}] ] ;
            In DomainLamC ; Jacobian Vol ; }
        EndIf
        If (ORDER>2)
          Term { [   dlam^2*sigmaH[]*q_4_4*SquNorm[Dt[{b_4}] ] ];
            In DomainLamC ; Jacobian Vol ; }
          Term { [ 2*dlam^2*sigmaH[]*q_2_4*Dt[{b_2}]*Dt[{b_4}] ] ;
            In DomainLamC ; Jacobian Vol ; }
        EndIf
        If (ORDER>3)
          Term { [   dlam^2*sigmaH[]*q_6_6*SquNorm[Dt[{b_6}] ] ];
            In DomainLamC ; Jacobian Vol ; }
          Term { [ 2*dlam^2*sigmaH[]*q_4_6*Dt[{b_4}]*Dt[{b_6}] ] ;
            In DomainLamC ; Jacobian Vol ; }
        EndIf

      EndIf

    } }

  { Name JouleLossesH ;
    Value {
      If(Flag_ComplexReluctivity) // Frequency domain
        Integral { [ SymmetryFactor*AxialLength*Re[ {d a}*Conj[ nuOm[]*{d a} ] ] ];
          In DomainLamC ; Jacobian Vol ; Integration II;}
      Else // Time domain
        Integral { [ SymmetryFactor*AxialLength* dlam^2*sigmaH[]*q_0_0*SquNorm[Dt[{d a}]] ] ;
          In DomainLamC ; Jacobian Vol ; Integration II; }
        If (ORDER>1)
          Integral { [   SymmetryFactor*AxialLength* dlam^2*sigmaH[]*q_2_2*SquNorm[Dt[{b_2}]] ] ;
            In DomainLamC ; Jacobian Vol ; Integration II; }
          Integral { [ SymmetryFactor*AxialLength* 2*dlam^2*sigmaH[]*q_0_2*Dt[{d a}]*Dt[{b_2}] ] ;
            In DomainLamC ; Jacobian Vol ; Integration II; }
        EndIf
        If (ORDER>2)
          Integral { [   SymmetryFactor*AxialLength* dlam^2*sigmaH[]*q_4_4*SquNorm[Dt[{b_4}] ] ];
            In DomainLamC ; Jacobian Vol ; Integration II; }
          Integral { [ SymmetryFactor*AxialLength* 2*dlam^2*sigmaH[]*q_2_4*Dt[{b_2}]*Dt[{b_4}] ] ;
            In DomainLamC ; Jacobian Vol ; Integration II; }
        EndIf
        If (ORDER>3)
          Integral { [   SymmetryFactor*AxialLength* dlam^2*sigmaH[]*q_6_6*SquNorm[Dt[{b_6}] ] ];
            In DomainLamC ; Jacobian Vol ; Integration II; }
          Integral { [ SymmetryFactor*AxialLength* 2*dlam^2*sigmaH[]*q_4_6*Dt[{b_4}]*Dt[{b_6}] ] ;
            In DomainLamC ; Jacobian Vol ; Integration II; }
        EndIf
      EndIf
    } }

  { Name MagEnergyH ; // this is only ok in the linear case
    Value {
      If(!NbrRegions[Domain_NL])
        Integral { [ SymmetryFactor * AxialLength * nu[] * {d a} * Dt[{d a}] ] ;
          In DomainLam ; Jacobian Vol ; Integration II; }
        If (ORDER>1)
          Integral { [ SymmetryFactor * AxialLength * p_2 * nu[] * {b_2} * Dt[Dt[{b_2}]] ] ;
            In DomainLam ; Jacobian Vol ; Integration II; }
        EndIf
        If (ORDER>2)
          Integral { [ SymmetryFactor * AxialLength * p_4 * nu[] * {b_4} * Dt[Dt[{b_4}]] ] ;
            In DomainLam ; Jacobian Vol ; Integration II; }
        EndIf
      Else
        // +++ What follows have to be checked... I think the crossed terms should appear, like in the formulation
        // quid of the integration points in the thickness of the lamination ...
        If(ORDER==0 || ORDER==1)
          Integral { [ SymmetryFactor * AxialLength * nu[{d a}] * {d a} * Dt[{d a}] ] ; In DomainLam ; Jacobian Vol ; Integration II; }
        EndIf
        If (ORDER == 2)
          Integral { [ SymmetryFactor * AxialLength * nu[({d a}+{b_2})/fillfactor] * {d a} * Dt[{d a}] ] ; In DomainLam ; Jacobian Vol ; Integration II; }
          Integral { [ SymmetryFactor * AxialLength * nu[({d a}+{b_2})/fillfactor] * {b_2} * Dt[Dt[{b_2}]] ] ; In DomainLam ; Jacobian Vol ; Integration II; }
        EndIf
        If (ORDER == 3)
          Integral { [ SymmetryFactor * AxialLength * nu[({d a}+{b_2}+{b_4})/fillfactor] * {d a} * Dt[{d a}] ] ; In DomainLam ; Jacobian Vol ; Integration II; }
          Integral { [ SymmetryFactor * AxialLength * nu[({d a}+{b_2}+{b_4})/fillfactor] * {b_2} * Dt[Dt[{b_2}]] ] ; In DomainLam ; Jacobian Vol ; Integration II; }
          Integral { [ SymmetryFactor * AxialLength * nu[({d a}+{b_2}+{b_4})/fillfactor] * {b_4} * Dt[Dt[{b_4}]] ] ; In DomainLam ; Jacobian Vol ; Integration II; }
        EndIf

      EndIf

    }
  }

Return
