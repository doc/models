//--------------------------------------------------------------------------------
// SRM stator
//--------------------------------------------------------------------------------

// Create all stator points
Rsmid = Rsout-YWs ;
For N In {0:Ns/2-1}
  th0s=N*dths+th0ss;
  p1sox=Rsout*Cos(-dths/2.+th0s); p1soy=Rsout*Sin(-dths/2.+th0s);
  p1six=Rsmid*Cos(-dths/2.+th0s); p1siy=Rsmid*Sin(-dths/2.+th0s);

  th4s=Asin(Rsin/Rsmid*Sin(Betas/2.));
  p2six=Rsmid*Cos(-th4s+th0s);    p2siy=Rsmid*Sin(-th4s+th0s);
  p3six=Rsin*Cos(-Betas/2.+th0s); p3siy=Rsin*Sin(-Betas/2.+th0s);
  p4six=Rsin*Cos( Betas/2.+th0s); p4siy=Rsin*Sin( Betas/2.+th0s);
  p5six=Rsmid*Cos(th4s+th0s);     p5siy=Rsmid*Sin(th4s+th0s);

  p1so[N]=newp; Point(p1so[N])={p1sox,p1soy,0.,8*Lc} ;
  p1si[N]=newp; Point(p1si[N])={p1six,p1siy,0.,8*Lc} ;
  p2si[N]=newp; Point(p2si[N])={p2six,p2siy,0.,8*Lc} ;
  p3si[N]=newp; Point(p3si[N])={p3six,p3siy,0.,1*Lc} ;
  p4si[N]=newp; Point(p4si[N])={p4six,p4siy,0.,1*Lc} ;
  p5si[N]=newp; Point(p5si[N])={p5six,p5siy,0.,8*Lc} ;
EndFor

N = Ns/2 ;
th0s=N*dths+th0ss;
p1sox=Rsout*Cos(-dths/2.+th0s);  p1soy=Rsout*Sin(-dths/2.+th0s);
p1six=Rsmid*Cos(-dths/2.+th0s);  p1siy=Rsmid*Sin(-dths/2.+th0s);
p1so[]+=newp; Point(newp)={p1sox,p1soy,0.,8*Lc} ;
p1si[]+=newp; Point(newp)={p1six,p1siy,0.,8*Lc} ;

// Create Stator Lines, arcs and regions
// outer stator surface
For N In {0:Ns/2-1}
 arcso[]+=newl; Circle(newl)={p1so[N],pAxe,p1so[(N+1)%Ns]};
EndFor

// outer surface of N-th stator tooth
For N In {0:Ns/2-1}
 clsi1[N]=newl; Circle(clsi1[N])={p1si[N],pAxe,p2si[N]};
 clsi2[N]=newl; Line(clsi2[N])={p2si[N],p3si[N]};
 clsi3[N]=newl; Circle(clsi3[N])={p3si[N],pAxe,p4si[N]};
 clsi4[N]=newl; Line(clsi4[N])={p4si[N],p5si[N]};
 clsi5[N]=newl; Circle(clsi5[N])={p5si[N],pAxe,p1si[(N+1)%Ns]};
EndFor

ss1=newl; Line(newl)={p1si[0], p1so[0]};
ss2=newl; Line(newl)={p1si[Ns/2], p1so[Ns/2]};
llStator=newll;
statorin[] = {clsi1[0]:clsi5[Ns/2-1]} ;

llstator = newll ; Line Loop (llstator)={-ss1,clsi1[0]:clsi5[Ns/2-1],ss2,-arcso[{2:0:-1}]};
Stator[] += news ; Plane Surface(Stator[0]) = {llstator} ;

// Create Coil regions
For N In {0:Ns/2}
 th0s=N*dths+th0ss;
 p1cx=Rsin*Cos(-dths/2.+th0s);
 p1cy=Rsin*Sin(-dths/2.+th0s);
 p1c[N]=newp; Point(p1c[N])={p1cx,p1cy,0.,2*Lc} ;
EndFor
For N In {0:Ns/2}
 clci1[N]=newl; Line(clci1[N])={p1si[N],p1c[N]};
EndFor
For N In {0:Ns/2-1}
  clci2[N]=newl; Circle(clci2[N])={p1c[N],pAxe,p3si[N]};
  clci3[N]=newl; Circle(clci3[N])={p4si[N],pAxe,p1c[(N+1)%Ns]};
EndFor

For N In {0:Ns/2-1}
 Coillln[N]=newll; Line Loop (Coillln[N])={clci1[N],clci2[N],-clsi2[N],-clsi1[N]};
 Coiln[N]=news ;   Plane Surface(Coiln[N])= {Coillln[N]};
 Coilllp[N]=newll; Line Loop (Coilllp[N])={-clsi4[N],clci3[N],-clci1[(N+1)%Ns],-clsi5[N]};
 Coilp[N]=news ;   Plane Surface(Coilp[N])= {Coilllp[N]};
EndFor

// Lines limiting the stator and coils (outer airgap)
For N In {0:Ns/2-1}
  airgco[3*N]  =clci2[N] ;
  airgco[3*N+1]=clsi3[N] ;
  airgco[3*N+2]=clci3[N] ;
EndFor

// Create Airgap Region
Rair1=Rag1 ;
Rair2=Rag2 ; // closing the airgap
For N In {0:Ns/2-1}
  th0s=N*Pi/2+th0ss;
  p1aircox = Rair1*Cos(-dths/2.+th0s);
  p1aircoy = Rair1*Sin(-dths/2.+th0s);
  p1aircox_= Rair2*Cos(-dths/2.+th0s);
  p1aircoy_= Rair2*Sin(-dths/2.+th0s);
  p1airco[N] = newp; Point(p1airco[N]) ={p1aircox,p1aircoy,0.,  Lc/2} ;
  p1airco_[N]= newp; Point(p1airco_[N])={p1aircox_,p1aircoy_, SlidingSurfaceShift,Lc/2} ;
EndFor

// Moving band
For N In {0:1}
  airG1[N]=newl; Circle(airG1[N]) = {p1airco[N], pAxe, p1airco[N+1]};
  airG2[N]=newl; Circle(airG2[N]) = {p1airco_[N],pAxeShift, p1airco_[N+1]};

  airG12[N] = newl ; Line(airG12[N]) = {p1airco[N], p1airco_[N]} ;
EndFor
airG12[2] = newl ; Line(airG12[2]) = {p1airco[2], p1airco_[2]} ;

lmbs[]+=newl; Line(newl)={p1airco[0],p1c[0]};
lmbs[]+=newl; Line(newl)={p1c[Ns/2],p1airco[2]};

ll_inmbs=newll; Line Loop(ll_inmbs)={lmbs[0], airgco[], lmbs[1],-airG1[{1,0}]};
surairgapS[0]=news ; Plane Surface(surairgapS[0]) = {ll_inmbs};

For N In {0:1}
  surmbstator[N]=news ; Line Loop (surmbstator[N]) = {-airG2[N],-airG12[N],airG1[N],airG12[N+1]};
  Plane Surface(surmbstator[N])= surmbstator[N];
EndFor

Transfinite Line {airG1[], airG2[]} = ndiv_mb ;
Transfinite Line {airG12[]} = 1 ;
Transfinite Surface {surmbstator[]}; Recombine Surface {surmbstator[]};


If(Flag_AirLayer)
  For N In {0:Ns/2}
    th0s=N*dths+th0ss;
    pax=Rair*Cos(-dths/2.+th0s); pay=Rair*Sin(-dths/2.+th0s);
    pa[N]=newp; Point(pa[N])={pax,pay,0.,8*Lc} ;
  EndFor

  For N In {0:Ns/2-1}
    arca[]+=newl; Circle(newl)={pa[N],pAxe,pa[(N+1)%Ns]};
  EndFor

  lair0=newl; Line(newl)={3, pa[0]};
  lair1=newl; Line(newl)={21, pa[3]};

  Curve Loop(newll) = {58, 59, 60, -62, -3, -2, -1, 61};
  surf_airlayer_out(0)=news; Plane Surface(surf_airlayer_out(0)) = {newll-1};
EndIf



//============================================================
//============================================================
If(Flag_Symmetry==0)
  // FULL MODEL
  // Rotation of Pi + duplication of all surfaces
  NN = #arcso[]-1 ;
  For N In {0:NN} // For simplicity (those lines would appear naturally when rotating Stator[0])
    arcso[]+= Rotate {{0, 0, 1}, {0, 0, 0}, Pi} { Duplicata{ Line{arcso[N]};} };
  EndFor

  Stator[]+= Rotate {{0, 0, 1}, {0, 0, 0}, Pi} { Duplicata{ Surface{Stator[0]};} };

  For N In {0:Ns/2-1}
    Coiln[]+= Rotate {{0, 0, 1}, {0, 0, 0}, Pi} { Duplicata{ Surface{Coiln[N]};} };
    Coilp[]+= Rotate {{0, 0, 1}, {0, 0, 0}, Pi} { Duplicata{ Surface{Coilp[N]};} };
  EndFor


  airG2[]+= Rotate {{0, 0, 1}, {0, 0, 0}, Pi} { Duplicata{ Line{airG2[{0,1}]};} };

  surairgapS[]+= Rotate {{0, 0, 1}, {0, 0, 0}, Pi} { Duplicata{ Surface{surairgapS[{0}]};} };
  surmbstator[]+= Rotate {{0, 0, 1}, {0, 0, 0}, Pi} { Duplicata{ Surface{surmbstator[{0,1}]};} };

  If(Flag_AirLayer)
    arca[]+= Rotate {{0, 0, 1}, {0, 0, 0}, Pi} { Duplicata{ Line{arca[{0,1,2}]};} };
    surf_airlayer_out()+= Rotate {{0, 0, 1}, {0, 0, 0}, Pi} { Duplicata{ Surface{surf_airlayer_out(0)};} };
  EndIf

EndIf





//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
// Physical regions
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------

allsurfaces[]= Surface {:};
allsurfaces[] -= {surairgapS[],surmbstator[]};
Reverse Surface {allsurfaces[]};

Physical Surface("stator iron", STATOR) = {Stator[]} ;
Physical Surface("stator air", AIR_STATOR) = {surairgapS[]}; // part of airgap which is not regular
Physical Surface("stator air (touching MB)", AIRGAP_STATOR) = {surmbstator[]}; // regular airgap

NN = (!Flag_Symmetry)?Ns-1:(Ns/2-1);
For N In {0:NN}
 Physical Surface(Sprintf('coil N %g', N), COILN+N) = {Coiln[N]} ;
 Physical Surface(Sprintf('coil P %g', N), COILP+N) = {Coilp[N]} ;
EndFor

Physical Line("skin stator (outer side)", BND_STATOR) = arcso[] ;
Physical Line("line MB stator", MB_STATOR) = airG2[] ; // lines to connect

If(Flag_Symmetry)
  Physical Point("reference point MB stator", REF_PNT_MB_STATOR) = {p1airco_[2]} ;
  //Lines for symmetry link
  If(!Flag_AirLayer)
    Physical Line("bnd stator A0", BND_A0_STATOR) = {ss1, clci1[0], airG12[0],lmbs[0]} ;
    Physical Line("bnd stator A1", BND_A1_STATOR) = {ss2, clci1[#clci1[]-1], airG12[2],lmbs[1]} ;
  Else
    Physical Line("bnd stator A0", BND_A0_STATOR) = {ss1, clci1[0], airG12[0],lmbs[0], lair0} ;
    Physical Line("bnd stator A1", BND_A1_STATOR) = {ss2, clci1[#clci1[]-1], airG12[2],lmbs[1],lair1} ;
  EndIf
EndIf

If(Flag_AirLayer)
  Physical Surface("air layer", AIRLAYER) = {surf_airlayer_out()} ;
  Physical Line("bnd airlayer", BND_AIRLAYER) = arca[] ;
EndIf



//-------------------------------------------------------------------------------
// For nice visualization
//-------------------------------------------------------------------------------
linStator[] = Abs(CombinedBoundary{ Surface{Stator[]};});
linStator[] += Abs(Boundary{Surface{Coiln[], Coilp[]};});

Color SteelBlue {Surface{Stator[]};}
Color SkyBlue {Surface{surairgapS[]};}
Color Cyan {Surface{surmbstator[]};}

Color Red        {Surface{ Coilp[{0:NN:Ns/2}] };} // A+
Color SpringGreen{Surface{ Coiln[{2:NN:Ns/2}] };} // C-
Color Gold       {Surface{ Coilp[{1:NN:Ns/2}] };} // B+
Color Red         {Surface{ Coiln[{0:NN:Ns/2}] };} // A-  Pink
Color SpringGreen  {Surface{ Coilp[{2:NN:Ns/2}] };} // C+ ForestGreen
Color Gold {Surface{ Coiln[{1:NN:Ns/2}] };} // B- PaleGoldenrod
