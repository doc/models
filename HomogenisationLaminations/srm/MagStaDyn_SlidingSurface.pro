// --------------------------------------------------------------------------
// Magnetodynamics - Magnetic vector potential (a) formulation
// --------------------------------------------------------------------------


DefineConstant[
  Freq,
  Val_Rint, Val_Rext,
  SymmetryFactor     = (Flag_Symmetry?2:1), //*(Flag_HalfLam?2:1),
  Nb_max_iter        = 100,
  relaxation_factor  = 1,
  stop_criterion     = 1e-4, // 1e-5
  T = 1/Freq, // Fundamental period in s
  // Mechanical equation
  Inertia = 0
  Flag_ImposedSpeed = 1,
  // time loop, defaut values
  time0      = 0,
  NbT        = 1,
  timemax    = NbT*T,
  NbSteps    = 100,
  delta_time = T/NbSteps
] ;

Group {
  DefineGroup[
    Domain, DomainCC, DomainC, DomainInf,
    SkinDomainC,
    DomainS, DomainB,
    SurfaceElec, Surf_EC_bn0
  ] ;
}

Function {
  DefineFunction[
    js0, mu, nu, sigma, rho,
    h, dhdb, dhdb_NL,
    Friction, Torque_mec
  ] ;
}


Function {
  // Sliding surface:
  // a1 is the angular span of Region and RegionRef.
  // a0 is the mesh step of the meshes of
  // a2[] is the angular position (in radian) of the rotor
  // relative to its reference position (as in the msh file)
  // assumed to be aligned with the stator.
  // a3[] is the shear angle of region AIRBM
  // (smaller than half the discretization step of the sliding surface
  // to adapt to a2[] values that are not multiple of this step).
  // AlignWithMaster[] maps a point of coordinates {X[], Y[], Z[]} onto
  // its image in the open set RegionRef-SubRegionRef by the symmetry mapping.
  // Coef[] is evaluated on Master nodes
  // fFloor[] is a safe version of Floor[] for real represented int variables
  // fRem[a,b] substracts from a multiple of b so that the result is in [0,1[

  Periodicity = (NbrPolesInModel%2)?-1:1 ;  // -1 for antiperiodicity, 1 for periodicity
  RotatePZ[] = Rotate[ XYZ[], 0, 0, $1 ] ;
  Tol = 1e-8 ; fFloor[] = Floor[ $1 + Tol ] ; fRem[] = $1 - fFloor[ $1 / $2 ] * $2;
  a1 = 2.*Pi/(Flag_Symmetry?2:1) ; // total angular aperture MB [rad]
  NbrDiv = (nDivBand /(Flag_Symmetry?2:1));
  a0 = a1/NbrDiv ; // angular span of one moving band element
  a2[] = $RotorPosition ;
  a3[] = ( ( fRem[ a2[], a0 ]#2 <= 0.5*a0 ) ? #2 : #2-a0 ) ;

  AlignWithMaster[] = RotatePZ[ - fFloor[ (Atan2[ Y[], X[] ] - a3[] )/a1 ] * a1 ] ;
  RestoreRef[] = XYZ[] - Vector[ 0, 0, SlidingSurfaceShift ] ;
  Coef[] = ( fFloor[ ((Atan2[ Y[], X[] ] - a2[] )/a1) ] % 2 ) ? Periodicity : 1. ;

  // Maxwell stress tensor
  T_max[] = ( SquDyadicProduct[$1] - SquNorm[$1] * TensorDiag[0.5, 0.5, 0.5] ) / mu0 ;
  T_max_cplx[] = Re[0.5*(TensorV[CompX[$1]*Conj[$1], CompY[$1]*Conj[$1], CompZ[$1]*Conj[$1]]
      - $1*Conj[$1] * TensorDiag[0.5, 0.5, 0.5] ) / mu0] ;
  T_max_cplx_2f[] = 0.5*(TensorV[CompX[$1]*$1, CompY[$1]*$1, CompZ[$1]*$1] -
    $1*$1 * TensorDiag[0.5, 0.5, 0.5] ) / mu0 ; // To check ????

  AngularPosition[] = (Atan2[$Y,$X]#7 >= 0.)? #7 : #7+2*Pi ;

  // Torque computed in postprocessing
  Torque_mag[] = $Trotor ;
  //Torque_mag[] = $Tstator ;


  relaxation_function[] = ($Iteration<Nb_max_iter/2) ? relaxation_factor : 0.2 ;
  relax_max = 1;
  relax_min = 1/10;
  relax_numtest=10;
  test_all_factors=0;
  relaxation_factors_lin() = LinSpace[relax_max,relax_min,relax_numtest];

}


Jacobian {
  { Name Vol ;
    Case {
      { Region DomainInf ; Jacobian VolSphShell {Val_Rint, Val_Rext} ; }
      { Region All ;       Jacobian Vol ; }
    }
  }
  { Name Sur ;
    Case {
      { Region All ; Jacobian Sur ; }
    }
  }
}

Integration {
  { Name II ;
    Case {
      { Type Gauss ;
        Case {
          { GeoElement Triangle    ; NumberOfPoints  4 ; }
          { GeoElement Quadrangle  ; NumberOfPoints  4 ; }
          { GeoElement Tetrahedron ; NumberOfPoints  4 ; }
          { GeoElement Hexahedron  ; NumberOfPoints  6 ; }
          { GeoElement Prism       ; NumberOfPoints  6 ; } // 21
          { GeoElement Line        ; NumberOfPoints  4 ; }
        }
      }
    }
  }

  { Name I1p ;
    Case {
      { Type Gauss ;
        Case {
          { GeoElement Triangle    ; NumberOfPoints  1 ; }
        }
      }
    }
  }

  { Name IIvol ;
    Case {
      { Type Gauss ;
        Case {
          { GeoElement Hexahedron ; NumberOfPoints  14 ; } // Gauss points for Hexahedron: valid choices: 6, 14, 34, 77
          { GeoElement Prism      ; NumberOfPoints  21 ; }  // Prism: valid choices: 6, 9, 21, 42

        }
      }
    }
  }

}

// --------------------------------------------------------------------------


Constraint {

  { Name MVP ;
    Case {
      { Type Assign; Region Sur_Dirichlet_bn  ; Value 0. ; }
      If(Flag_Symmetry)
        { Type Link ; Region Sur_StatorPerSlave ; RegionRef Sur_StatorPerMaster ;
          ToleranceFactor 1e-8;	Coefficient Periodicity ; Function RotatePZ[ -a1 ] ; }

        { Type Link ; Region Sur_RotorPerSlave ; RegionRef Sur_RotorPerMaster ;
          ToleranceFactor 1e-8; Coefficient Periodicity ; Function RotatePZ[ -a1 ] ; }
      EndIf

      // Sliding surface
      { Type Link ; Region Sur_SlidingSlave ; SubRegion Lin_SlidingSubslave ;
        RegionRef Sur_SlidingMaster ; SubRegionRef Lin_SlidingSubmaster ;
        ToleranceFactor 1e-8;
        Coefficient Coef[] ; Function AlignWithMaster[] ; FunctionRef RestoreRef[] ; }
    }
  }

  { Name u ;
    Case {
      { Type Assign ; Region Sur_Dirichlet_u ; Value 0. ; }
    }
  }


  If(_3Dmodel)
    // A correct spanning-tree is essential to the validity of the model.
    // The spanning-tree must be autosimilar by rotation on the sliding surfaces
    // (SubRegion2 clause, only the edges aligned with the Z axis are placed in
    // the tree), and be also a spanning-tree and Dirichlet and Link surfaces and
    // their boundaries (SubRegion clause).
    { Name GaugeCondition_MVP ; Type Assign ;
      Case {
        If(Flag_GaugeType==TREE_COTREE_GAUGE)
          { Region Vol_Tree ; Value 0. ;
            SubRegion Region[ {Lin_Tree, Sur_Tree} ] ;
            SubRegion2 Region[ Sur_Tree_Sliding, AlignedWith Z ] ;
          }
        EndIf
      }
    }
  EndIf


  //---------------------------------------------------------------------------------
  { Name CurrentPosition ; // [m]
    Case {
      //{ Region DomainKin ; Type Init ; Value ($RotorPosition = RotorPosition) ; }
      { Region DomainKin ; Type Init ; Value ($PreviousRotorPosition = 0) ; } // From machine_magstadyn
    }
  }

  { Name CurrentVelocity ; // [rad/s]
    Case {
      { Region DomainKin ; Type Init ; Value 0. ; }
    }
  }

}

FunctionSpace {
  If(_3Dmodel)
    // Magnetic vector potential a (b = curl a)
    { Name Hcurl_a ; Type Form1 ;
      BasisFunction {
        { Name se ; NameOfCoef ae ; Function BF_Edge ;
          Support Dom_Hcurl_a ; Entity EdgesOf[ DomainCC ] ; }
        { Name se2 ; NameOfCoef ae2 ; Function BF_Edge ;
          Support Dom_Hcurl_a ; Entity EdgesOf[ DomainC, Not SkinDomainC ] ; }

        // where fixing a=0 is too strong, rather fixing n x a = n x grad u !!!
        { Name su ; NameOfCoef un ; Function BF_GradNode ;
          Support DomainC ; Entity NodesOf[ SkinDomainC ] ; }
      }
      Constraint {
        { NameOfCoef ae  ; EntityType EdgesOf ; NameOfConstraint MVP ; }
        { NameOfCoef ae2 ; EntityType EdgesOf ; NameOfConstraint MVP ; }
        { NameOfCoef ae  ; EntityType EdgesOfTreeIn ; EntitySubType StartingOn ;
          NameOfConstraint GaugeCondition_MVP ; }
        { NameOfCoef un;  EntityType NodesOf ; NameOfConstraint u; }
      }
    }

    // Electric scalar potential
    { Name Hregion_u ; Type Form0 ;
      BasisFunction {
        { Name sr ; NameOfCoef ur ; Function BF_GroupOfNodes ;
          Support DomainC ; Entity GroupsOfNodesOf[ SurfaceElec ] ; }
      }
      GlobalQuantity {
        { Name U ; Type AliasOf        ; NameOfCoef ur ; }
        { Name I ; Type AssociatedWith ; NameOfCoef ur ; }
      }
      Constraint {
        { NameOfCoef U ; EntityType GroupsOfNodesOf ; NameOfConstraint V_massive ; }
        { NameOfCoef I ; EntityType GroupsOfNodesOf ; NameOfConstraint I_massive ; }
      }
    }
  Else
    // 2D model => with homogenization
    { Name Hcurl_a ; Type Form1P ;
      BasisFunction {
        { Name se ; NameOfCoef ae ; Function BF_PerpendicularEdge ;
          Support Domain ; Entity NodesOf[ All ] ; }
      }
      Constraint {
        { NameOfCoef ae  ; EntityType NodesOf ; NameOfConstraint MVP ; }
      }
    }
    { Name Hregion_u ; Type Vector ;
      BasisFunction {
        { Name sr ; NameOfCoef vr ; Function BF_RegionZ ;
          Support DomainC ; Entity DomainC ; } }
      GlobalQuantity {
        { Name U ; Type AliasOf        ; NameOfCoef vr ; }
        { Name I ; Type AssociatedWith ; NameOfCoef vr ; } }
      Constraint {
        { NameOfCoef U ; EntityType Region ; NameOfConstraint V_massive ; }
        { NameOfCoef I ; EntityType Region ; NameOfConstraint I_massive ; }
      }
    }

  EndIf

  { Name Is1 ; Type Vector ;
    BasisFunction {
      { Name sr ; NameOfCoef ir ; Function BF_RegionZ ;
        Support DomainB ; Entity DomainB ; }
    }
    GlobalQuantity {
      { Name Ib ; Type AliasOf        ; NameOfCoef ir ; }
      { Name Ub ; Type AssociatedWith ; NameOfCoef ir ; }
    }
    Constraint {
      { NameOfCoef Ub ; EntityType Region ; NameOfConstraint V_stranded ; }
      { NameOfCoef Ib ; EntityType Region ; NameOfConstraint I_stranded ; }
    }
  }

  // Uz et Iz for circuit relations
  { Name Hregion_Cir ; Type Scalar ;
    BasisFunction {
      { Name sr ; NameOfCoef ir ; Function BF_Region ;
        Support DomainZtot_Cir ; Entity DomainZtot_Cir ; }
    }
    GlobalQuantity {
      { Name Iz ; Type AliasOf        ; NameOfCoef ir ; }
      { Name Uz ; Type AssociatedWith ; NameOfCoef ir ; }
    }
    Constraint {
      //{ NameOfCoef Uz ; EntityType Region ; NameOfConstraint VInit_Cir ; }
      { NameOfCoef Uz ; EntityType Region ; NameOfConstraint V_circuit ; }
      { NameOfCoef Iz ; EntityType Region ; NameOfConstraint I_circuit ; }
    }
  }

  // BFs for case with hysteresis
  { Name H_hysteresis ; Type Vector;
    BasisFunction {
      { Name sex ; NameOfCoef aex ; Function BF_VolumeX ; Support Domain ; Entity VolumesOf[ All ] ; }
      { Name sey ; NameOfCoef aey ; Function BF_VolumeY ; Support Domain ; Entity VolumesOf[ All ] ; }
      { Name sez ; NameOfCoef aez ; Function BF_VolumeZ ; Support Domain ; Entity VolumesOf[ All ] ; }
    }
  }

  // For the mechanical equation
  { Name Position ; Type Scalar ;
    BasisFunction {
      { Name sr ; NameOfCoef pr ; Function BF_Region ;
        Support DomainKin ; Entity DomainKin ; } }
    GlobalQuantity {
      { Name P ; Type AliasOf  ; NameOfCoef pr ; } }
    Constraint {
      { NameOfCoef P ; EntityType Region ; NameOfConstraint CurrentPosition ; } }
  }

  { Name Velocity ; Type Scalar ;
    BasisFunction {
      { Name sr ; NameOfCoef vr ; Function BF_Region ;
        Support DomainKin ; Entity DomainKin ; } }
    GlobalQuantity {
      { Name V ; Type AliasOf ; NameOfCoef vr ; } }
    Constraint {
      { NameOfCoef V ; EntityType Region ; NameOfConstraint CurrentVelocity ; }
    }
  }


}

If(!_3Dmodel)
  Include "Macro_homogenisation_laminations.pro";
EndIf

Formulation {

  { Name MagStaDyn_a ; Type FemEquation ;
    Quantity {
      { Name a ; Type Local ; NameOfSpace Hcurl_a ; }

      // Massive conductor (DomainC)
      { Name v ; Type Local ; NameOfSpace Hregion_u ; }
      { Name U ; Type Global ; NameOfSpace Hregion_u [U] ; }
      { Name I ; Type Global ; NameOfSpace Hregion_u [I] ; }

      // Source: Stranded conductor (DomainB)
      { Name ir ; Type Local  ; NameOfSpace Is1 ; }
      { Name Ub ; Type Global ; NameOfSpace Is1 [Ub] ; }
      { Name Ib ; Type Global ; NameOfSpace Is1 [Ib] ; }

      // Circuit quantities (DomainZtot_Cir)
      { Name Uz ; Type Global ; NameOfSpace Hregion_Cir [Uz] ; }
      { Name Iz ; Type Global ; NameOfSpace Hregion_Cir [Iz] ; }

      If(Flag_Hysteresis)
        { Name h ; Type Local ; NameOfSpace H_hysteresis ; }
      EndIf

      If(!_3Dmodel)
        // Homog laminations 2D
        For k In {2:ORDERMAX} // b_2, b_4, b_6
          { Name b~{2*k-2}  ; Type Local ; NameOfSpace Hb~{2*k-2} ; }
        EndFor
        If(Flag_Hysteresis)
          For i In {1:nbrQuadraturePnts}
            { Name h~{i} ; Type Local ; NameOfSpace H_hysteresis~{i} ; }
          EndFor
        EndIf
      EndIf

    }
    Equation {
      Galerkin { [ nu[] * Dof{d a} , {d a} ]     ;
        In Domain_L  ; Jacobian Vol ; Integration II ; }

      If(NbrRegions[Domain_NL])
        If( _3Dmodel || (_2Dmodel && ORDER==1))
          If(!Flag_Hysteresis)
            Galerkin { [ nu[{d a}/fillfactor] * Dof{d a} , {d a} ]     ;
              In Domain_NL  ; Jacobian Vol ; Integration II ; }
            Galerkin { JacNL [ 1/fillfactor*dhdb_NL[{d a}] * Dof{d a} , {d a} ] ;
              In Domain_NL ; Jacobian Vol ; Integration II ; }
          Else
            Galerkin { [ SetVariable[
                  h_Jiles[ {h}[1],
                    {d a}[1]/fillfactor,
                    {d a}/fillfactor ]{List[hyst_FeSi]},
                  QuadraturePointIndex[] ]{$hjiles}, {d a} ] ;
              In Domain_NL ; Jacobian Vol ; Integration I1p ; }
            Galerkin { JacNL[ 1/fillfactor*dhdb_Jiles[
                  {h},
                  {d a}/fillfactor,
                  {h}-{h}[1] ]{List[hyst_FeSi]} * Dof{d a} , {d a} ] ;
              In Domain_NL ; Jacobian Vol ; Integration I1p ; }

            // h_Jiles saved in local quantity {h}
            // BF is constant per element => 1 integration point is enough
            Galerkin { [ Dof{h}   , {h} ]  ; // saving h_Jiles in local quantity h
              In Domain_NL ; Jacobian Vol ; Integration I1p ; }
            Galerkin { [ -GetVariable[QuadraturePointIndex[]]{$hjiles} , {h} ]  ;
              In Domain_NL ; Jacobian Vol ; Integration I1p ; }
          EndIf
        EndIf
      EndIf

      // Source in stranded coil imposed via function
      Galerkin { [ -js0[], {a} ] ; In DomainS ;
        Jacobian Vol ; Integration II ; }

      Galerkin { DtDof[ sigma[] * Dof{a} , {a} ] ;
        In DomainC ; Jacobian Vol ; Integration II ; }

      // voltage-current equation for massive conductors (DomainC)
      If(_3Dmodel)
        Galerkin { [ sigma[] * Dof{d v}, {a} ] ;
          In DomainC ; Jacobian Vol ; Integration II ; }

        Galerkin { DtDof[ sigma[] * Dof{a} , {d v} ] ;
          In DomainC ; Jacobian Vol ; Integration II ; }
        Galerkin { [ sigma[] * Dof{d v} , {d v} ] ;
          In DomainC ; Jacobian Vol ; Integration II ; }
        GlobalTerm { [ Dof{I} , {U} ] ; In SurfaceElec ; }
      Else
        Galerkin {  [ -sigma[]/AxialLength * Dof{v} , {a} ] ;
          In DomainC ; Jacobian Vol ; Integration II ; }
        Galerkin { DtDof [ sigma[] * Dof{a} , {v} ] ;
          In DomainC ; Jacobian Vol ; Integration II ; }
        Galerkin { [ -sigma[]/AxialLength * Dof{v} , {v} ] ;
          In DomainC ; Jacobian Vol ; Integration II ; }
        GlobalTerm { [ Dof{I} , {U} ] ; In DomainC ; }

        If(!Flag_ComplexReluctivity && !Flag_Statics)// Either additional terms or complex nu in DomainLam
          Call Macro_Terms_DomainLamC_sigma;
          If(!Flag_Hysteresis)
            Call Macro_Terms_DomainLam_nu;
          Else
            Call Macro_Terms_DomainLam_hysteresis;
          EndIf
        EndIf
      EndIf

      Galerkin { [ -NbrWires_Area[] * Dof{ir}, {a} ] ;
        In DomainB ; Jacobian Vol ; Integration II ; }
      Galerkin { DtDof [ (_3Dmodel ? FactorLam : AxialLength) * NbrWires_Area[] * Dof{a}, {ir} ] ; // AxialLength
        In DomainB ; Jacobian Vol ; Integration II ; }
      GlobalTerm { [ Dof{Ub}/(SymmetryFactor*(Flag_HalfLam?2:1)), {Ib} ] ; In DomainB ; } //+++ constants added

      If(Flag_Cir)
        //Circuit related terms
        GlobalTerm { NeverDt[ Dof{Uz}                , {Iz} ] ; In Resistance_Cir; }
        GlobalTerm { NeverDt[ Resistance[{Uz}] * Dof{Iz}, {Iz} ] ; In Resistance_Cir; }

        GlobalTerm { [ 0. * Dof{Iz} , {Iz} ] ; In DomainZtot_Cir ; }
        GlobalTerm { [ 0. * Dof{Uz} , {Iz} ] ; In DomainZtot_Cir ; }

        GlobalEquation {
          Type Network ; NameOfConstraint ElectricalCircuit ;
          If(_3Dmodel)
            { Node {I};  Loop {U};  Equation {I};  In SurfaceElec ; }
          Else
            { Node {I};  Loop {U};  Equation {I};  In DomainC ; }
          EndIf
          { Node {Ib}; Loop {Ub}; Equation {Ub}; In DomainB ; }
          { Node {Iz}; Loop {Uz}; Equation {Uz}; In DomainZtot_Cir ; }
        }
      EndIf

    }
  }

  // Mechanics-------------------------------------------------------------------------------------------

  { Name Mechanical ; Type FemEquation ;
    Quantity {
      { Name V ; Type Global ; NameOfSpace Velocity [V] ; } // velocity
      { Name P ; Type Global ; NameOfSpace Position [P] ; } // position
    }
    Equation {
      GlobalTerm { DtDof [ Inertia * Dof{V} , {V} ] ; In DomainKin ; }
      GlobalTerm { [ Friction[] * Dof{V} , {V} ] ; In DomainKin ; }
      GlobalTerm { [        Torque_mec[] , {V} ] ; In DomainKin ; }
      GlobalTerm { [       -Torque_mag[] , {V} ] ; In DomainKin ; }

      GlobalTerm { DtDof [ Dof{P} , {P} ] ; In DomainKin ; }
      GlobalTerm {       [-Dof{V} , {P} ] ; In DomainKin ; }
    }
  }


}


Resolution {

  { Name Analysis ;
    System {
      If( (Flag_AnalysisType<2) || NbrRegions[Domain_NL])
        { Name Sys_Mag ; NameOfFormulation  MagStaDyn_a  ;}
      Else
        { Name Sys_Mag ; NameOfFormulation  MagStaDyn_a ; Frequency Freq; }
      EndIf
      If(Flag_AnalysisType==1 && !Flag_ImposedSpeed)
        { Name Sys_Mec ; NameOfFormulation Mechanical ; }
      EndIf
    }
    Operation {

      If(Clean_Results)
        // SystemCommand[StrCat["rm -rf ", ResDir]];
        SystemCommand[StrCat["rm -rf ", ResDir, "*.pos"]];
        // SystemCommand[StrCat["rm -rf ", ResDir, "*.dat"]];
        SystemCommand[StrCat["rm -rf ", ResDir, "*m0*.dat"]]; // 2D
        // SystemCommand[StrCat["rm -rf ", ResDir, "*m1*.dat"]]; // 3D
      EndIf
      CreateDir[ResDir];

      If(_3Dmodel)
        Evaluate[ $vol_airgap  = Vol_Rotor_Airgap[] ];
        // Evaluate[ $vol_airgapS = Vol_Stator_Airgap[]];
        // Print[ {$vol_airgap, $vol_airgapS}, Format "===> Check airgap volumes: VagR=%e VagS=%e "] ;
      EndIf

      If(Flag_AnalysisType!=1)
        If(Flag_AnalysisType==2) // For convience, useful in postpro... not needed
          SetTime[Freq];
        EndIf

        Evaluate[ $RotorPosition = RotorPosition ];
        ChangeOfCoordinates[ NodesOf[ Vol_Moving ], RotatePZ[ a2[] ] ] ;
        ChangeOfCoordinates[ NodesOf[ Sur_SlidingMaster ], RotatePZ[ a3[] ] ] ;
        /*
        // checking
        Evaluate[ $a2 = a2[], $a3 = a3[] ];
        Evaluate[ $aa = Fmod[ a2[], a0 ], $bb = fRem[ a2[], a0 ] ] ;
        Print[ {$RotorPosition*rad2deg, $a2*rad2deg, $a3*rad2deg, $aa*rad2deg, $bb*rad2deg},
        Format "wt=%e a2=%e a3=%e %e %e"] ;
        */
        UpdateConstraint[Sys_Mag];

        ChangeOfCoordinates[ NodesOf[ Sur_SlidingMaster ], XYZ[] - Vector[ 0, 0, SlidingSurfaceShift ] ] ;

        If(!NbrRegions[Domain_NL])
          Generate[Sys_Mag] ; Solve[Sys_Mag] ;
        Else
          IterativeLoop[Nb_max_iter, stop_criterion, relaxation_factor]{
            GenerateJac[Sys_Mag] ; SolveJac[Sys_Mag] ; }
        EndIf
        SaveSolution [Sys_Mag] ;

        Test[ GetNumberRunTime[visu]{StrCat[main,"Visu/Real-time maps"]} ]{
          PostOperation[Get_FieldMaps];
        }
        PostOperation[Get_GlobalQuantities];


        ChangeOfCoordinates[ NodesOf[ Sur_SlidingMaster ], XYZ[] + Vector[ 0, 0, SlidingSurfaceShift ] ] ;

      Else // time domain

        Evaluate[ $RotorPosition = RotorPosition, $PreviousRotorPosition=0 ];
        InitSolution[Sys_Mag]; SaveSolution[Sys_Mag];
        If(!Flag_ImposedSpeed) // Full dynamics
          InitSolution[Sys_Mec];
          InitSolution[Sys_Mec]; // Twice for avoiding warning (a = d_t^2 x)
          Evaluate[ $Tstator = 0., $Trotor = 0. ];
        EndIf

        TimeLoopTheta[time0, timemax, delta_time , 1.]{

          If(Flag_ImposedSpeed)
            Evaluate[ $RotorPosition = RotorPosition + wr*($TimeStep-1)*$DTime ];
            // Print[ {$TimeStep, $Time, $RotorPosition*rad2deg}, Format "====> timestep=%e time=%e pos=%e"] ;
          Else
            Evaluate[ $RotorPosition = $RotorPosition + $PreviousRotorPosition ]; // This is not correct
          EndIf

          Test[ $TimeStep == 1 ]{
            ChangeOfCoordinates[ NodesOf[ Vol_Moving ], RotatePZ[ a2[] ] ] ;
          }
          {
            ChangeOfCoordinates[ NodesOf[ Vol_Moving ], RotatePZ[ delta_theta[] ] ] ; // either fixed or based on the mechanical computation
          }

          ChangeOfCoordinates[ NodesOf[ Sur_SlidingMaster ], RotatePZ[ a3[] ] ] ;

          UpdateConstraint[Sys_Mag];

          ChangeOfCoordinates[ NodesOf[ Sur_SlidingMaster ], XYZ[] - Vector[ 0, 0, SlidingSurfaceShift ] ] ;

          If(!NbrRegions[Domain_NL])
            Generate[Sys_Mag] ; Solve[Sys_Mag] ;
          Else
            IterativeLoop[Nb_max_iter, stop_criterion, relaxation_function[]]{
              GenerateJac[Sys_Mag] ;
              // If(!Flag_Hysteresis)
              SolveJac[Sys_Mag] ;
              // Else
              //SolveJac_AdaptRelax[Sys_Mag, relaxation_factors_lin(), test_all_factors ] ;
              //EndIf
            }
          EndIf
          SaveSolution [Sys_Mag] ;

          Test[ GetNumberRunTime[visu]{StrCat[main,"Visu/Real-time maps"]} ]{
            PostOperation[Get_FieldMaps];
          }
          PostOperation[Get_GlobalQuantities];
          //Test[ $TimeStep > 1 ]{ PostOperation[Get_GlobalQuantities] ; }

          If(!Flag_ImposedSpeed)
            Evaluate[ $PreviousRotorPosition = $RotorPosition ];

            Generate [Sys_Mec] ; Solve [Sys_Mec] ; SaveSolution [Sys_Mec] ;
            PostOperation[Get_MechanicalQs];
            Print[ {$RotorPosition, $PreviousRotorPosition}, Format "====> Pos=%e PrevPos=%e"] ;
          EndIf

          ChangeOfCoordinates[ NodesOf[ Sur_SlidingMaster ], XYZ[] + Vector[ 0, 0, SlidingSurfaceShift ] ] ;
          ChangeOfCoordinates[ NodesOf[ Sur_SlidingMaster ], RotatePZ[ -a3[] ] ] ;
        }

      EndIf


    }
  }

}


PostProcessing {

  { Name MagStaDyn_a ; NameOfFormulation MagStaDyn_a;
    PostQuantity {
      { Name a  ; Value { Term { [ {a} ]         ; In Domain ; Jacobian Vol ; } } }
      { Name az ; Value { Term { [ CompZ[{a}] ]  ; In Domain ; Jacobian Vol ; } } }
      { Name b ; Value { Term { [ {d a} ]        ; In Domain ; Jacobian Vol ; } } }
      { Name bsurf ; Value { Term { [ {d a} ]    ; In Sur_Link ; Jacobian Sur ; } } }
      { Name h  ; Value {
          Local { [ nu[]*{d a} ] ; In Domain_L ; Jacobian Vol ; }
          If(!Flag_Hysteresis)
            Local { [ nu[{d a}]*{d a} ] ; In Domain_NL ; Jacobian Vol ; }
          Else
            Local { [ {h} ] ; In Domain_NL ; Jacobian Vol ; }
          EndIf
        }
      }
      If(Flag_Hysteresis)
        { Name hdb  ; Value { // Useful for computing losses in hysteretic case, by integrating in one period (steady state)
            Local { [ {h}*({d a}-{d a}[1]) ] ; In Domain_NL  ; Jacobian Vol ; }
          }
        }
      EndIf

      { Name v ; Value { Term { [ {v} ] ; In DomainC ; Jacobian Vol ; } } }

      { Name e ; Value { Term { [ -(Dt[{a}] + {d v}) ]         ; In DomainC ; Jacobian Vol ; } } }
      { Name j ; Value { Term { [ -sigma[]*(Dt[{a}] + {d v}) ] ; In DomainC ; Jacobian Vol ; } } }

      { Name js0 ; Value { Term { [ js0[] ] ; In DomainS ; Jacobian Vol ; } } }

      { Name Flux ;
        Value {
          Integral {
            [ SymmetryFactor*(Flag_HalfLam?2:1)*(_3Dmodel?FactorLam:AxialLength)/Sc[]*Idir[]* Vector[0,0,1] * {a} ] ;
            In Region[{DomainS,DomainB}] ; Jacobian Vol ; Integration II ;
          }
        }
      }

      { Name rhoj2 ; // local losses
        Value {
          Term { [ sigma[]*SquNorm[ Dt[{a}]+{d v}] ] ; In DomainC ; Jacobian Vol ; }
          Term { [ 1./sigma[]*SquNorm[ IA[]*{ir} ] ] ; In Phase1  ; Jacobian Vol ; }
          Term { [ 1./sigma[]*SquNorm[ IB[]*{ir} ] ] ; In Phase2  ; Jacobian Vol ; }
          Term { [ 1./sigma[]*SquNorm[ IC[]*{ir} ] ] ; In Phase3  ; Jacobian Vol ; }
        }
      }

      { Name JouleLosses ; // global losses
        Value {
          Integral { [ SymmetryFactor * (Flag_HalfLam?2.:1.) * FactorLam * sigma[] * SquNorm[ -(Dt[{a}] + {d v}) ] ] ;
            In DomainC ; Jacobian Vol ; Integration II ; }
          Integral { [ 1./sigma[]*SquNorm[ IA[]*{ir} ] ] ; In Phase1 ; Jacobian Vol ; Integration II ; }
          Integral { [ 1./sigma[]*SquNorm[ IB[]*{ir} ] ] ; In Phase2 ; Jacobian Vol ; Integration II ; }
          Integral { [ 1./sigma[]*SquNorm[ IC[]*{ir} ] ] ; In Phase3 ; Jacobian Vol ; Integration II ; }
        }
      }

      { Name MagneticEnergy ;
        Value {
          If(!Flag_Hysteresis)
            Integral {
              [ SymmetryFactor*(Flag_HalfLam?2:1)*(_3Dmodel?FactorLam:AxialLength)*nu[{d a}]*SquNorm[{d a}] ] ;
              In Domain ; Jacobian Vol ; Integration II ; }
          Else
            Integral {
              [ SymmetryFactor*(Flag_HalfLam?2:1)*(_3Dmodel?FactorLam:AxialLength)*nu[{d a}]*SquNorm[{d a}] ] ;
              In Domain_L ; Jacobian Vol ; Integration II ; }
            Integral {
              [ SymmetryFactor*(Flag_HalfLam?2:1)*(_3Dmodel?FactorLam:AxialLength)*{h}*{d a} ] ;
              In Domain_NL ; Jacobian Vol ; Integration II ; }
          EndIf
        }
      }


      { Name ComplexPower ; Value { // real part -> eddy current losses; imag part-> magnetic energy
          Integral { [ SymmetryFactor * (Flag_HalfLam?2.:1.) * FactorLam *
              Complex[ SquNorm[sigma[]*(Dt[{a}]+{d v}/SymmetryFactor)]/sigma[], nu[]*SquNorm[{d a}] ] ] ;
            In DomainC; Jacobian Vol ; Integration II ; }
        }
      }

      //{ Name U ; Value { Term { [ {U} ]   ; In SurfaceElec ; } } }
      //{ Name I ; Value { Term { [ {I} ]   ; In SurfaceElec ; } } }

      { Name U ; Value {
          Term { [ {Ub} ]  ; In DomainB ; }
          Term { [ {Uz} ]  ; In DomainZtot_Cir ; }
        } }
      { Name I ; Value {
          Term { [ {Ib} ]  ; In DomainB ; }
          Term { [ {Iz} ]  ; In DomainZtot_Cir ; }
        } }

      // volume stored in variable $vol_airgap (postprocessing or Evaluate)
      { Name myVol ; Value { Integral { Type Global ; [ 1. ] ;
            In Domain; Jacobian Vol ; Integration II ; } }
      }
      { Name myVol_agR ; Value { Term { Type Global; [ Vol_Rotor_Airgap[] ] ; In DomainDummy ; } } }
      { Name myVol_agS ; Value { Term { Type Global; [ Vol_Stator_Airgap[] ] ; In DomainDummy ; } } }

      { Name Torque_Maxwell ; // Torque computation via Maxwell stress tensor
        Value {
          Integral {
            // \int_S (\vec{r} \times (T_max \vec{n}) ) / ep
            // with ep = |S| / (2\pi r_avg)
            [ CompZ [ XYZ[] /\ (T_max[{d a}] * XYZ[]) ] * 2*Pi*AxialLength/SurfaceArea[] ] ; // 2D case
            In Domain ; Jacobian Vol  ; Integration II; } }
      }

      { Name Torque3D_Maxwell ; // Torque computation via Maxwell stress tensor (3D)
        Value {
          Integral {
            [ CompZ [ XYZ[] /\ (T_max[{d a}] * XYZ[] ) ] * 2 * Pi * nblam*dlam/$vol_airgap ] ;
            In Domain; Jacobian Vol ; Integration II ; } }
      }

      If(!_3Dmodel)
        Call PostQuantities_Homog;
      EndIf

    }
  }

  { Name Mechanical ; NameOfFormulation Mechanical ;
    PostQuantity {
      { Name P    ; Value { Term { [ {P} ]  ; In DomainKin ; } } } // Position [rad]
      { Name Pdeg ; Value { Term { [ {P}*180/Pi ]  ; In DomainKin ; } } } // Position [deg]
      { Name V    ; Value { Term { [ {V} ]  ; In DomainKin ; } } } // Velocity [rad/s]
      { Name Vrpm ; Value { Term { [ {V}*30/Pi ]  ; In DomainKin ; } } } // Velocity [rpm]
    }
  }

}

// --------------------------------------------------------------------------

DefineConstant[
  R_ = {"Analysis", Name "GetDP/1ResolutionChoices", Visible 1}
  C_ = {"-solve -v 3 -v2 -cpu", Name "GetDP/9ComputeCommand", Visible 1}
  P_ = {"", Name "GetDP/2PostOperationChoices", Visible 1}
];
