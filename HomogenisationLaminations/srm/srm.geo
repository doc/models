// Switched Reluctance Motor Gmsh geometry file (2D)

Include "srm_data.geo" ;

Solver.AutoShowLastStep = 1;
Mesh.Algorithm = 1 ;
// 2D mesh algorithm (1: MeshAdapt, 2: Automatic, 5: Delaunay, 6: Frontal-Delaunay, 7: BAMG, 8: Frontal-Delaunay for Quads, 9: Packing of Parallelograms)
Geometry.CopyMeshingMethod = 1;


//------------------------------------------------------------
//------------------------------------------------------------
// Create origin
pAxe = newp ; Point(pAxe) = { 0. , 0. , 0., 3*Lc} ;
pAxeShift = newp; Point(newp) = {0,0,SlidingSurfaceShift, 3*Lc};


Include "srm_stator.geo";
Include "srm_rotor.geo";

//-------------------------------------------------------------------------------
// For nice visualization
//-------------------------------------------------------------------------------
/*
Hide { Point{ Point {:} }; Line{ Line {:} }; }
Show { Line{ linStator[], linRotor[] }; }
*/

//Physical Line(NICEPOS) = {linStator[],linRotor[] } ;

//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------

Coherence;


If(_3Dmodel)
  // Common to stator, rotor and mb...
  //====================================

  layer_top()={}; aux()={};

  If (alpha==1)
    a_top = 1/nllam;
  Else
    a_top = (alpha-1)/(alpha^nllam - 1); // nllam == number of divitions per half lamination
  EndIf

  one(0) = 1;
  layer_top(0) = a_top ;
  For k In {1:nllam-1}
    one(k) = 1;
    layer_top(k) = layer_top(k-1) + a_top * alpha^k;
  EndFor

  // complete lamination
  If(!Flag_HalfLam)
    one() ={one(),one()};
    If(nllam==1)
      layer_top() ={0.5,1};
    EndIf
    If(nllam > 1)
      For k In {0:nllam-1}
        aux(k) = layer_top(k)/2;
      EndFor
      For k In {0:nllam-1}
        If(k==0)
          layer_top(k) = aux(nllam-k-1)-aux(nllam-k-2);
          layer_top(nllam+k) = 0.5 + aux(k) ;
          Else
          If(k<nllam-1)
            layer_top(k) = layer_top(k-1) + aux(nllam-k-1)-aux(nllam-k-2);
            Else
            layer_top(k) = layer_top(k-1) + aux(nllam-k-1);
          EndIf
          layer_top(nllam+k) = layer_top(nllam+k-1) + aux(k)-aux(k-1);
        EndIf
      EndFor
    EndIf
  EndIf


  Include "generate_3d.geo";

  _indexVol     = _3Dmodel*_Offset;
  _indexVol_air = _3Dmodel*(_Offset*2+_Offset_top);

  If(!Flag_HalfLam && Flag_AirLayer)
    // There is a problem with the SlidingSurfaceShift => link not correct
    // probably the symmetry for some of the stator parts have to take that into account

    bbb() = Symmetry {0,0,1,-dlam/2} { Duplicata{Physical Surface{BND_AIRLAYER+_indexVol_air};} };
    Physical Surface(BND_AIRLAYER + _indexVol) += bbb();

    If(Flag_Symmetry)
      // Lin_SlidingSubslave and Lin_SlidingSubmaster
      bbb() = Symmetry {0,0,1,-dlam/2} { Duplicata{Physical Line{REF_PNT_MB_ROTOR+_indexVol_air};} };
      Physical Line(REF_PNT_MB_ROTOR  + _indexVol) += bbb();
      bbb() = Symmetry {0,0,1,-dlam/2-SlidingSurfaceShift} { Duplicata{Physical Line{REF_PNT_MB_STATOR+_indexVol_air};} };
      Physical Line(REF_PNT_MB_STATOR  + _indexVol)+= bbb();

       // Sur_StatorPerMaster, Sur_StatorPerSlave
      bbb() = Symmetry {0,0,1,-dlam/2-SlidingSurfaceShift} { Duplicata{Physical Surface{BND_A0_STATOR+_indexVol_air};} };
      Physical Surface(BND_A0_STATOR + _indexVol)+= bbb();
      bbb() = Symmetry {0,0,1,-dlam/2-SlidingSurfaceShift} { Duplicata{Physical Surface{BND_A1_STATOR+_indexVol_air};} };
      Physical Surface(BND_A1_STATOR + _indexVol)+= bbb();

      // Sur_RotorPerMaster, Sur_RotorPerSlave
      bbb() = Symmetry {0,0,1,-dlam/2} { Duplicata{Physical Surface{BND_A0_ROTOR+_indexVol_air};} };
      Physical Surface(BND_A0_ROTOR + _indexVol)+= bbb();
      bbb() = Symmetry {0,0,1,-dlam/2} { Duplicata{Physical Surface{BND_A1_ROTOR+_indexVol_air};} };
      Physical Surface(BND_A1_ROTOR + _indexVol)+= bbb();
    EndIf

    bbb() = Symmetry {0,0,1,-dlam/2-SlidingSurfaceShift} { Duplicata{Physical Surface{MB_STATOR+_indexVol_air};} };
    Physical Surface(MB_STATOR+_indexVol) += bbb();
    bbb() = Symmetry {0,0,1,-dlam/2} { Duplicata{Physical Surface{MB_ROTOR+_indexVol_air};} };
    Physical Surface(MB_ROTOR+_indexVol) += bbb();


    NN = (!Flag_Symmetry)?Ns-1:(Ns/2-1);
    For i In {0:NN}
      vvv() = Symmetry {0,0,1,-dlam/2} { Duplicata{Physical Volume{COILN+i+_indexVol_air};} };
      Physical Volume(COILN+i+_indexVol) += vvv() ;
      vvv() = Symmetry {0,0,1,-dlam/2} { Duplicata{Physical Volume{COILP+i+_indexVol_air};} };
      Physical Volume(COILP+i+_indexVol) += vvv() ;
      pv~{2}() -= {COILN+i+_indexVol_air, COILP+i+_indexVol_air};
    EndFor

    // Copying top layer to the bottom (all air minus coils)
    For k In {0:#pv~{2}()-1}
      aaa() += Symmetry {0,0,1,-dlam/2} { Duplicata{Physical Volume{pv~{2}(k)};} };
    EndFor
    Physical Volume(AIRLAYER + _indexVol) += aaa();
    cut_xy_bottom() = Surface In BoundingBox {-Rsout*1.25,-Rsout*1.25,-1.01*zair, 2*(Rsout*1.25), 2*(Rsout+1.25), -0.5*zair};
    Physical Surface(BND_AIRLAYER + _indexVol) += cut_xy_bottom();
  EndIf

  // Additional surfaces for 3D
  skinStatorIron_()  = Abs(CombinedBoundary{Physical Volume{(STATOR + _3Dmodel*_Offset)};});
  skinRotorIron_()   = Abs(CombinedBoundary{Physical Volume{(ROTOR + _3Dmodel*_Offset)};});
  If(Flag_HalfLam)
    skinStatorIron_() -= Physical Surface{(STATOR)}; // bottom
    skinRotorIron_() -= Physical Surface{(ROTOR)}; // bottom
  EndIf
  If(Flag_Symmetry)
    skinStatorIron_() -= Physical Surface{(BND_A0_STATOR + _3Dmodel*_Offset)};
    skinStatorIron_() -= Physical Surface{(BND_A1_STATOR + _3Dmodel*_Offset)};
    skinRotorIron_() -= Physical Surface{(BND_A0_ROTOR + _3Dmodel*_Offset)};
    skinRotorIron_() -= Physical Surface{(BND_A1_ROTOR + _3Dmodel*_Offset)};
  EndIf
  Physical Surface ("skin stator iron", SKIN_STATOR) = {skinStatorIron_()};
  Physical Surface ("skin rotor iron", SKIN_ROTOR) = {skinRotorIron_()};

  NN = (!Flag_Symmetry)?Ns-1:(Ns/2-1);
  For i In {0:NN}
    volCoilN() += Physical Volume{ (COILN+i+_3Dmodel*_Offset) } ;
    volCoilP() += Physical Volume{ (COILP+i+_3Dmodel*_Offset) } ;
  EndFor

  // Merging some Physicals when two slices... => needed for correct constraints
  If(Flag_AirLayer)
    // Sur_SlidingSlave and Sur_SlidingMaster
    Physical Surface(MB_STATOR+_indexVol) += Physical Surface{MB_STATOR+_indexVol_air};
    Physical Surface(MB_ROTOR +_indexVol) += Physical Surface{MB_ROTOR+_indexVol_air};
    If(Flag_Symmetry)
      // Sur_StatorPerMaster, Sur_StatorPerSlave
      Physical Surface(BND_A0_STATOR + _indexVol)+= Physical Surface{BND_A0_STATOR+_indexVol_air};
      Physical Surface(BND_A1_STATOR + _indexVol)+= Physical Surface{BND_A1_STATOR+_indexVol_air};

      // Sur_RotorPerMaster, Sur_RotorPerSlave
      Physical Surface(BND_A0_ROTOR + _indexVol)+= Physical Surface{BND_A0_ROTOR+_indexVol_air};
      Physical Surface(BND_A1_ROTOR + _indexVol)+= Physical Surface{BND_A1_ROTOR+_indexVol_air};

      // Lin_SlidingSubslave and Lin_SlidingSubmaster
      Physical Line(REF_PNT_MB_ROTOR  + _indexVol) += Physical Line{REF_PNT_MB_ROTOR  + _indexVol_air};
      Physical Line(REF_PNT_MB_STATOR  + _indexVol)+= Physical Line{REF_PNT_MB_STATOR  + _indexVol_air};
    EndIf

    // Outer boundary
    Physical Surface(BND_AIRLAYER + _3Dmodel*_Offset) += Physical Surface{BND_AIRLAYER+_indexVol_air};

    // Delete (not necessary but useful for checking)
    // ====================================
    Physical Surface("line MB stator (slice 2)") -= Physical Surface{MB_STATOR+_indexVol_air};
    Physical Surface("line MB rotor (slice 2)") -= Physical Surface{MB_ROTOR+_indexVol_air};
    If(Flag_Symmetry)
      Physical Surface("bnd stator A0 (slice 2)") -= Physical Surface{BND_A0_STATOR+_indexVol_air};
      Physical Surface("bnd stator A1 (slice 2)") -= Physical Surface{BND_A1_STATOR+_indexVol_air};
      Physical Surface("bnd rotor A0 (slice 2)")  -= Physical Surface{BND_A0_ROTOR+_indexVol_air};
      Physical Surface("bnd rotor A1 (slice 2)")  -= Physical Surface{BND_A1_ROTOR+_indexVol_air};
      Physical Line("reference point MB rotor (slice 2)")  -= Physical Line{REF_PNT_MB_ROTOR+_indexVol_air};
      Physical Line("reference point MB stator (slice 2)")  -= Physical Line{REF_PNT_MB_STATOR+_indexVol_air};
    EndIf
    Physical Surface("bnd airlayer (slice 2)") -= Physical Surface{BND_AIRLAYER+_indexVol_air};

  EndIf


  treelines() = {};
  If(!Flag_AirLayer)
    treelines() = CombinedBoundary{ Physical Surface{(BND_STATOR + _3Dmodel*_Offset)};};
  Else
    treelines() = CombinedBoundary{ Physical Surface{(BND_AIRLAYER + _3Dmodel*_Offset)};};
  EndIf

  If(Flag_Symmetry)
    treelines() += CombinedBoundary{ Physical Surface{(BND_A0_STATOR + _3Dmodel*_Offset)};};
    treelines() += CombinedBoundary{ Physical Surface{(BND_A1_STATOR + _3Dmodel*_Offset)};};
    treelines() += CombinedBoundary{ Physical Surface{(BND_A0_ROTOR + _3Dmodel*_Offset)};};
    treelines() += CombinedBoundary{ Physical Surface{(BND_A1_ROTOR + _3Dmodel*_Offset)};};
  EndIf
  treelines() += CombinedBoundary{ Physical Surface {
        MB_STATOR, MB_STATOR  + _3Dmodel*_Offset,
        MB_ROTOR, MB_ROTOR  + _3Dmodel*_Offset
      };};
  treelines() = Unique(Abs(treelines())); // removing duplicata
  Physical Line("TreeLines", TREELINES) = { treelines()};


  // Some colors for aesthetics...
  //=================================
  //Recursive
  Color SteelBlue { Physical Volume {(STATOR + _3Dmodel*_Offset)}; }
  //Recursive
  Color SteelBlue { Physical Volume {(ROTOR + _3Dmodel*_Offset)}; }
  //Recursive
  Color SkyBlue {Physical Volume {(AIR_STATOR + _3Dmodel*_Offset)};}
  //Recursive
  Color SkyBlue {Physical Volume {(AIRGAP_STATOR + _3Dmodel*_Offset)};} // Structured for sliding surface
  //Recursive
  Color SkyBlue {Physical Volume {(AIR_ROTOR + _3Dmodel*_Offset)};}
  //Recursive
  Color SkyBlue {Physical Volume {(AIRGAP_ROTOR + _3Dmodel*_Offset)};}// Structured for sliding surface

  //Recursive
  Color Red        {Volume{ volCoilP({0:NN:Ns/2}) };} // A+
  //Recursive
  Color SpringGreen{Volume{ volCoilN({2:NN:Ns/2}) };} // C-
  //Recursive
  Color Gold       {Volume{ volCoilP({1:NN:Ns/2}) };} // B+
  //Recursive
  Color Red         {Volume{ volCoilN({0:NN:Ns/2}) };} // A- Pink
  //Recursive
  Color SpringGreen  {Volume{ volCoilP({2:NN:Ns/2}) };} // C+ ForestGreen
  //Recursive
  Color Gold {Volume{ volCoilN({1:NN:Ns/2}) };} // B- PaleGoldenrod


EndIf
