Include "srm_data.geo";
Include "BH.pro";

_2Dmodel = !_3Dmodel;
TREE_COTREE_GAUGE=1;

DefineConstant[
  Flag_AnalysisType = {1,
    Choices{0="Static",  1="Time domain", 2="Frequency domain"},
    ServerAction Str["Reset", StrCat[main,"110Type of supply"], ',' ,StrCat[main, "00Approx. order"]],
    Name StrCat[main, "100Type of analysis"], Highlight "Red",
    Help Str["- Use 'Static' to compute static fields created in the machine",
             "- Use 'Time domain' to compute the dynamic response (transient) of the machine",
             "- Use 'Frequency domain' to compute the dynamic response (steady state) of the machine"]}

  Flag_NL      = {!(Flag_AnalysisType==2), Choices{0,1}, Name StrCat[main,"13Nonlinear material?"],
    Highlight "LightPink", ReadOnly (Flag_AnalysisType==2)}

  Flag_NonlinearLawType = {1,
    Choices{
      0="linear law (testing)",
      1="analytica BH-law",
      2="anhysteretic law",
      3="Jiles-Atherton hysteretic law"},
    ServerAction Str["Reset", StrCat[main, "43Nbr of time steps"]],
    Name StrCat[main,"21 Choose BH-curve?"], Highlight "Magenta", Visible Flag_NL,
    ReadOnly (Flag_AnalysisType==2) }
  Flag_Hysteresis = {(Flag_NonlinearLawType==3)?1:0, Choices{0,1},
    Name StrCat[main,"22Jiles-Atherton hysteretic law?"],
    Highlight "LightPink", ReadOnly, Visible Flag_NL}

  Flag_Statics = (Flag_AnalysisType==0) //|| (Flag_Hysteresis==1)

  Flag_GaugeType = { 1, Choices{0="No gauge", 1="Tree-cotree gauge"},
    Name StrCat[main,"05Type of gauge"], Highlight "Blue", Visible _3Dmodel}

  Type_Supply = { (Flag_AnalysisType!=1)?1:5, Choices{ // 1
      0="Constant current in phase 1",
      1="Sinusoidal current in phase 1",
      2="Pulse applied to phase 1",
      3="current imposed in phases acoording to rotor position (without circuits)",
      4="current imposed in phases acoording to rotor position (with circuits)",
      5="voltage +- Vdc according to rotor position (with diodes)"},
    Name StrCat[main,"110Type of supply"], Highlight "Blue", ReadOnly (Flag_AnalysisType==2)}
  Flag_Cir = (Type_Supply>3)

  Freq = {5e3, Name StrCat[main,"111Frequency [Hz]"], Highlight "AliceBlue"}
  Omega = 2*Pi*Freq

  mu0 = 4.e-7 * Pi
  SkinDepth = { 1e3*Sqrt[1/(Pi*Freq*mu0*murIron*sigmaLam)], Highlight Str[colorro],
    Visible !Flag_Statics && !Flag_NL, ReadOnly 1, Name StrCat[main,"112Skin depth in lamination [mm]"] }

  visu = {1, Choices{0, 1}, AutoCheck 0,
    Name StrCat[main,"Visu/Real-time maps"], Highlight "LawnGreen"}
  Clean_Results = {0, Choices {0,1},
    Name StrCat[main,'000Remove previous result files'], Highlight "Red"}

  ORDER = {(Flag_AnalysisType==0 || (Flag_Hysteresis==1))?1:2,
    Choices {
        0="exact",
       -1="-1 (homog FD zero order)",
       -2="-2 (homog FD 2nd order)",
       -3="-3 (homog FD 4th order)",
        1="1 (homog TD zero order)",
        2="2 (homog TD 2nd order)",
        3="3 (homog TD 4th order)",
        4="4 (homog TD 6th order)" },
      Name StrCat[main, "00Approx. order"],
      Help Str["- With 'Time domain', use order>0",
        "- With 'Frequency domain' all possibities hold"],
      Highlight "Red", Visible !_3Dmodel, ReadOnly Flag_Statics}

  Flag_ComplexReluctivity  = (!_3Dmodel) && !(ORDER>0) // 0 3D fine reference model OR Homog real; 1 Homog with nu complex

  Flag_ImposedSpeed = { (Flag_AnalysisType==1), Choices{0="None", 1="Choose speed"},
    ServerAction Str["Reset", StrCat[main, "43Nbr of time steps"], ',' , StrCat[main,"21Initial rotor pos. (pro) [deg]"]],
    Name StrCat[main,"30Imposed rotor speed [rpm]"], Highlight "Red", Visible (Flag_AnalysisType==1)},
  myrpm = { 5000, Name StrCat[main,"31Speed [rpm]"],
    Highlight "AliceBlue", Visible (Flag_AnalysisType==1 && Flag_ImposedSpeed==1)},

  Tmec = { 0, Name StrCat[main,"33Mechanical torque [Nm]"],
    Highlight "AliceBlue", Visible 0*(!Flag_ImposedSpeed && Flag_AnalysisType==1) },
  Frict = { 0, Name StrCat[main,"34Friction torque [Nm]"],
    Highlight "AliceBlue", Visible 0*(!Flag_ImposedSpeed && Flag_AnalysisType==1) }

  _thetaInit0 = (Flag_ImposedSpeed?-30:-15),  // if -40 => matching data from cmag2011
  _thetaSpan  = 180

];



thetaInit = deg2rad * DefineNumber[_thetaInit0, Name StrCat[main,"41Initial rotor pos. (pro) [deg]"],
  Visible 1, Highlight "Green", ReadOnly 0];
thetaMax  = deg2rad * DefineNumber[_thetaInit0+_thetaSpan,
  ServerAction Str["Reset", StrCat[main, "43Nbr of time steps"]],
  Name StrCat[main,"42End rotor pos. (pro) [deg]"],
  Visible (Flag_AnalysisType==1) && Flag_ImposedSpeed, Highlight "Red"];

// delta_theta = deg2rad * DefineNumber[ 1., Name StrCat[main,"23Angular step [deg]"], Visible (Flag_AnalysisType==1)];

DefineConstant[
  NbSteps = { (Flag_ImposedSpeed==1) ? (Flag_Hysteresis?2:1)*(thetaMax-thetaInit)/deg2rad : 1000,
    Min (thetaMax-thetaInit)/deg2rad, // by default a degree
    Name StrCat[main, "43Nbr of time steps"],
    Help Str["- If imposed speed, it is the number of angular divisions",
      "- If calculated speed, it is just a limit to stop the time loop"] ,
    Highlight "AliceBlue", Visible (Flag_AnalysisType==1), ReadOnly 0 }
];



ResDir = "res/";
ExtGmsh = ".pos";
ExtGplt = ".dat";

addtofile="_cost";

If(Flag_AnalysisType==1)
  ExtGplt = StrCat[addtofile,Sprintf("_f%g_rpm%g.dat", Freq, myrpm)];
  If(_2Dmodel)
    ExtGmsh = StrCat[addtofile,Sprintf("_f%g_rpm%g_n%g.pos", Freq, myrpm, ORDER)];
    ExtGplt = StrCat[addtofile,Sprintf("_f%g_rpm%g_n%g.dat", Freq, myrpm, ORDER)];
  EndIf
Else
  ExtGplt = StrCat[addtofile,Sprintf("_FD_th%g.dat", thetaInit/deg2rad)];
  If(_2Dmodel)
    ExtGmsh = StrCat[addtofile,Sprintf("_FD_th%g_n%g.pos", thetaInit/deg2rad, ORDER)];
    ExtGplt = StrCat[addtofile,Sprintf("_FD_th%g_n%g.dat", thetaInit/deg2rad, ORDER)];
  EndIf
EndIf


NbrPolesInModel = NbrStatorPoles ;
NbrPolesTot = NbrStatorPolesTot ;

_indexVol     = _3Dmodel*_Offset;
_indexVol_air = _3Dmodel*(_Offset*2+_Offset_top);

Group {
  Stator = Region[{(STATOR + _indexVol)}] ;
  Rotor  = Region[{(ROTOR  + _indexVol)}] ;
  Iron   = Region[{Rotor, Stator}];

  SkinStator = Region[{(SKIN_STATOR)}] ;
  SkinRotor  = Region[{(SKIN_ROTOR)}] ;

  Air = Region[{
      (AIR_STATOR + _indexVol), (AIRGAP_STATOR + _indexVol),
      (AIR_ROTOR  + _indexVol), (AIRGAP_ROTOR  + _indexVol)} ] ;

  // For torque computation
  Rotor_Airgap = Region[{(AIRGAP_ROTOR  + _indexVol)} ] ;
  Stator_Airgap =Region[{(AIRGAP_STATOR + _indexVol)} ] ;


  If(_3Dmodel && Flag_AirLayer)
    Air += Region[{
        (STATOR+_indexVol_air),
        (ROTOR +_indexVol_air),
        (AIR_STATOR + _indexVol_air), (AIRGAP_STATOR +_indexVol_air),
        (AIR_ROTOR  + _indexVol_air), (AIRGAP_ROTOR  +_indexVol_air)
      }];
    Air += Region[{// Air layer around stator
        (AIRLAYER + _indexVol),
        (AIRLAYER + _indexVol_air)
      }];
  EndIf

  For i In {1:Ns/SymmetryFactor}
    IndS~{i} = Region[{(COILP+i-1+_indexVol)}] ;
    CutIndS~{i} = Region[ {(COILP+i-1)} ] ;

    IndS~{Ns+i} = Region[{(COILN+i-1+_indexVol)}] ;
    CutIndS~{Ns+i} = Region[{(COILN+i-1)}] ;

    If(_3Dmodel && Flag_AirLayer)
      IndS~{i}    += Region[{(COILP+i-1+_indexVol_air)}] ;
      IndS~{Ns+i} += Region[{(COILN+i-1+_indexVol_air)}] ;
    EndIf

    IndsS += Region[{IndS~{i}, IndS~{Ns+i}}] ;
  EndFor
  If(!Flag_Symmetry)
    Phase1 = Region[ {IndS~{1}, IndS~{(Ns+1)}, IndS~{4}, IndS~{(Ns+4)}} ];
    Phase2 = Region[ {IndS~{2}, IndS~{(Ns+2)}, IndS~{5}, IndS~{(Ns+5)}} ];
    Phase3 = Region[ {IndS~{3}, IndS~{(Ns+3)}, IndS~{6}, IndS~{(Ns+6)}} ];
  Else
    Phase1 = Region[ {IndS~{1}, IndS~{(Ns+1)}} ];
    Phase2 = Region[ {IndS~{2}, IndS~{(Ns+2)}} ];
    Phase3 = Region[ {IndS~{3}, IndS~{(Ns+3)}} ];
  EndIf

  If(!Flag_AirLayer)
    Sur_Outer  = Region[{ (BND_STATOR + _indexVol) }] ;// In 3D, you need to add top (and bottom)
  Else
    Sur_Outer  = Region[{ (BND_AIRLAYER + _indexVol) }] ; // side airlayer (lam + air  level)
  EndIf

  _slice_lam = _3Dmodel*_Offset_top;
  _slice_air = _3Dmodel*(_Offset_top+_Offset_top*2);

  If(_3Dmodel)
    If(Flag_HalfLam)
      Bot_air = Region[{AIR_STATOR, AIRGAP_STATOR, AIR_ROTOR, AIRGAP_ROTOR}];
      If(Flag_AirLayer)
        Bot_air += Region[{AIRLAYER}];
      EndIf
      For i In {1:Ns}
        Bot_coils += Region[{ (COILP+i-1), (COILN+i-1) }] ;
      EndFor
    Else
      Bot_air  = Region[{}];
      Bot_coils = Region[{}];
    EndIf

    If(!Flag_AirLayer)
      Top_air = Region[{
          (AIR_STATOR+_slice_lam), (AIRGAP_STATOR+_slice_lam),
          (AIR_ROTOR +_slice_lam), (AIRGAP_ROTOR +_slice_lam) }];
      For i In {1:Ns}
        Top_coils += Region[{ (COILP+i-1+_slice_lam), (COILN+i-1+_slice_lam) }] ;
      EndFor
    Else
      Top_air = Region[{
          (AIR_STATOR+_slice_air), (AIRGAP_STATOR+_slice_air),
          (AIR_ROTOR +_slice_air), (AIRGAP_ROTOR +_slice_air)}];
      Top_air += Region[{(AIRLAYER+_slice_air)}];

      For i In {1:Ns}
        Top_coils += Region[{ (COILP+i-1+_slice_air), (COILN+i-1+_slice_air) }] ;
      EndFor
    EndIf

    Sur_Bottom = Region[ {Bot_air, Bot_coils} ];
    Sur_Top    = Region[ {Top_air, Top_coils} ];
  EndIf

  Bnd_A0_Stator = Region[{(BND_A0_STATOR + _indexVol)}] ;
  Bnd_A1_Stator = Region[{(BND_A1_STATOR + _indexVol)}] ;
  Bnd_A0_Rotor  = Region[{(BND_A0_ROTOR  + _indexVol)}] ;
  Bnd_A1_Rotor  = Region[{(BND_A1_ROTOR  + _indexVol)}] ;

  Sur_StatorPerMaster = Region[{Bnd_A0_Stator}];
  Sur_StatorPerSlave  = Region[{Bnd_A1_Stator}];
  Sur_RotorPerMaster = Region[{Bnd_A0_Rotor}];
  Sur_RotorPerSlave  = Region[{Bnd_A1_Rotor}];


  DefineGroup[DomainLamC, DomainLamCC, DomainLam];
  If(Flag_Statics)
    DomainCC = Region[ {Air, IndsS, Rotor, Stator} ] ;
    DomainC  = Region[ {} ] ;
    SkinDomainC = Region[{}];
    DomainLam  = Region[{Rotor, Stator}];
  Else
    DomainCC = Region[ {Air, IndsS} ] ;
    If(_3Dmodel)
      DomainC  = Region[ {Rotor, Stator} ] ;
      SkinDomainC = Region[{SkinRotor, SkinStator}];
    Else
      // Homogenization domains (used in 2D)
      DomainLamC = Region[ {Rotor, Stator} ] ; // Treated with homogenization
      DomainLamCC = Region[{}];
      DomainLam   = Region[{DomainLamC, DomainLamCC}];

      DomainNoLamC  = Region[ {} ] ;
      SkinDomainNoLamC  = Region[ {} ] ;
      DomainC = Region[ {DomainNoLamC} ] ;
      SkinDomainC = Region[{SkinDomainNoLamC}];
    EndIf
  EndIf

  If(_3Dmodel)
    Sur_Bottom += Region[ {STATOR, ROTOR} ];
    If(!Flag_AirLayer)
      Sur_Top += Region[ {(STATOR+_slice_lam),(ROTOR+_slice_lam)} ];
    Else
      Sur_Top += Region[ {(STATOR+_slice_air),(ROTOR+_slice_air)} ];
    EndIf
  EndIf

  If(!Flag_NL)
    Domain_NL = #{};
    Domain_L = Region[ {DomainCC, DomainC, DomainLamC} ] ;
    Domain   = Region[ {Domain_L} ] ;
  Else
    Domain_L  = Region[ {Air, IndsS} ] ;
    Domain_NL = Region[ {Stator, Rotor} ] ;
    Domain    = Region[ {Domain_L, Domain_NL} ] ;
  EndIf

  If(!Flag_Cir)
    DomainS = Region[ {IndsS} ] ;
    DomainB = Region[ {} ] ;
  Else
    DomainS = Region[ {} ] ;
    DomainB = Region[ {IndsS} ] ;
  EndIf

  DomainMap = Region[{Rotor, Stator, Air, IndsS}];


  // Dummy numbers for circuit definition
  R1 = #551 ;
  R2 = #552 ;
  R3 = #553 ;

  E1 = #771 ;
  E2 = #772 ;
  E3 = #773 ;

  If(Flag_Cir)
    DomainZtot_Cir = #{R1,R2,R3, E1,E2,E3} ;
    Resistance_Cir = #{R1,R2,R3} ;
  Else
    DomainZtot_Cir = #{} ;
    Resistance_Cir = #{} ;
  EndIf


  If(_3Dmodel)
    Sur_Dirichlet_bn = Region[{Sur_Outer, Sur_Top, Sur_Bottom}];
  Else
    Sur_Dirichlet_bn = Region[{Sur_Outer}];
  EndIf

  Sur_Dirichlet_u  = Region[{}];
  If(_3Dmodel && Flag_HalfLam)
    Sur_Dirichlet_u += Region[{Sur_Bottom}];
  EndIf

  // For the moving band:
  Sur_SlidingSlave  = Region[{(MB_ROTOR  + _indexVol)}] ;
  Sur_SlidingMaster = Region[{(MB_STATOR + _indexVol)}] ;

  Lin_SlidingSubslave = #{}; Lin_SlidingSubmaster = #{};
  If(Flag_Symmetry)
    If(_3Dmodel)
      Lin_SlidingSubslave  = Region[{(REF_PNT_MB_ROTOR  + _indexVol)}] ;
      Lin_SlidingSubmaster = Region[{(REF_PNT_MB_STATOR + _indexVol)}] ;
    Else
      Lin_SlidingSubslave  = Region[{(REF_PNT_MB_ROTOR )}] ;
      Lin_SlidingSubmaster = Region[{(REF_PNT_MB_STATOR)}] ;
    EndIf
  EndIf


  // Moving region
  Vol_Moving = Region[ {Rotor, (AIR_ROTOR+_3Dmodel*_Offset), (AIRGAP_ROTOR+_indexVol)} ] ;
  If(Flag_AirLayer)
    Vol_Moving += Region[ {
        (ROTOR +_indexVol_air),
        (AIR_ROTOR   +_indexVol_air),
        (AIRGAP_ROTOR+_indexVol_air) } ] ;
  EndIf

  Sur_Link = Region[ {Sur_SlidingMaster, Sur_SlidingSlave,
      Sur_StatorPerMaster, Sur_StatorPerSlave,
      Sur_RotorPerMaster, Sur_RotorPerSlave} ] ;

  Lin_TreeLines        = Region[{TREELINES}];
  Sur_Tree_Sliding = Region[ { Sur_SlidingMaster, Sur_SlidingSlave } ];

  Vol_Tree = Region[{Domain}];
  Sur_Tree  = Region [ {Sur_Link, Sur_Dirichlet_bn} ] ;
  Lin_Tree = Region[ Lin_TreeLines ] ;

  Dom_Hcurl_a = Region[ {Domain, Sur_Dirichlet_bn, Sur_Link} ] ;


  DomainKin = Region[1234] ;    // Dummy region number for mechanical equation
  DomainDummy = Region[12345] ; // Dummy region number for postpro with functions

}

Function {

  rpm = (Flag_ImposedSpeed==0) ? 0.: myrpm ; // speed [rpm]
  wr = rpm/60*2*Pi ;   // angular rotor speed in rad_mec/s

  RotorPosition = thetaInit; // [rad] initial rotor position
  // time step
  If(Flag_ImposedSpeed)
    // Imposed movement with fixed wr + time domain
    delta_time = (thetaMax-thetaInit)/NbSteps/wr;
  Else
    delta_time = 1/Freq/100; // .5e-3 It should be linked to Freq
  EndIf

  delta_theta[] = (Flag_ImposedSpeed) ? wr*$DTime : ($RotorPosition-$PreviousRotorPosition) ; // angle step (in rad)
  time0 = 0.;                       // initial time [s]
  timemax = NbSteps * delta_time ;  // final   time [s]

}

// --------------------------------------------------------------------------

Function {

  mu [ #{Air, IndsS} ]  = mu0 ;
  nu [ #{Air, IndsS} ]  = 1. / mu0 ;

  sigma [ #{IndsS} ] = 5.9e7 ;
  sigma [ #{Rotor, Stator} ] = sigmaLam ;
  rho[] = 1/sigma[] ;

  // Linear case or 'Frequency domain' computation
  muIron = murIron * mu0 ;
  nuIron = 1./(mu0*murIron) ;
  nu_lin[] = nuIron ;

  If(!NbrRegions[Domain_NL]) // Linear case
    If(!Flag_ComplexReluctivity)
      Printf("===> using LINEAR law");
      nu[#{Iron}] = nu_lin[] ;
      dhdb_NL[ #{Iron} ]   = dhdb_lin_NL[$1] ;
    EndIf
  Else
        Printf("===> using NON LINEAR law");
    If(!Flag_Hysteresis)
      If(Flag_NonlinearLawType==0)
        Printf("===> testing NON LINEAR case with LINEAR law");
        nu[#{Iron}] = nu_lin[] ;
        dhdb_NL[ #{Iron} ]   = dhdb_lin_NL[$1] ;
      EndIf
      If(Flag_NonlinearLawType==1)
        Printf("===> analytical NON LINEAR law");
        nu[#{Iron}]     = nu_1a[$1];
        dhdb_NL[#{Iron}] = dhdb_1a_NL[$1];

        h [#{Iron}]    = h_1a[$1];
        dhdb [#{Iron}] = dhdb_1a[$1];
      EndIf
      If(Flag_NonlinearLawType==2)
        Printf("===> anhysteretic NON LINEAR law corresponding to Jiles-Atherton params");
        nu[ #{Iron} ] = nu_anhys[$1] ;
        h[ #{Iron} ] = h_anhys[$1] ;
        dhdb_NL[ #{Iron} ]= dhdb_anhys_NL[$1] ;
        dhdb[ #{Iron} ]   = dhdb_anhys[$1] ;
      EndIf
    Else
      Printf("===> Hysteretic NON LINEAR law => Jiles-Atherton model");
    EndIf
  EndIf

  FactorLam = nblam ; // One (half) lamination in 3D model... AxialLength set to 1 in 3D, FactorLam = AxialLength/dlam=nblam
  Imax = 1./2 ; Umax = 1. ;
  Nw=226.;
  Sc[] = SurfaceArea[]{COILN} ;
  NbrWires_Area[] = Nw/Sc[] ;

  If(!Flag_Symmetry)
    Idir[#{ IndS~{1},IndS~{(Ns+4)},IndS~{2},IndS~{(Ns+5)},IndS~{3},IndS~{(Ns+6)} }] =  Nw ;
    Idir[#{ IndS~{4},IndS~{(Ns+1)},IndS~{5},IndS~{(Ns+2)},IndS~{6},IndS~{(Ns+3)} }] = -Nw ;
  Else
    Idir[#{ IndS~{1},     IndS~{2},     IndS~{3} }] =  Nw ;
    Idir[#{ IndS~{(Ns+1)},IndS~{(Ns+2)},IndS~{(Ns+3)} }] = -Nw ;
  EndIf

  Vol_Rotor_Airgap[] = SurfaceArea[]{AIRGAP_ROTOR}*dlam/(Flag_HalfLam?2:1);
  Vol_Stator_Airgap[] = SurfaceArea[]{AIRGAP_STATOR}*dlam/(Flag_HalfLam?2:1);

  Vdc = 300 ;
  Resistance[#DomainB]  = 1 ;
  Resistance[#{551,552,553}] = (Type_Supply==5)? (($1 <= 0.) ?  1.2 : 1e5) : 1e-5;

  T = 1/Freq ;
  wPulse = T/2 ;
  F_Pulse[] = (F_Period[$Time]{T} <  wPulse )?  1 : -1 ;

  If(Type_Supply == 0)
    UI[] = 1;
    UI1[Phase1] = UI[] ;
    UI3[Phase3] = 0. ;
    UI2[Phase2] = 0. ;
  EndIf
  If(Type_Supply == 1)
    UI[] = F_Cos_wt_p[]{2*Pi*Freq, 0 };
    UI1[Phase1] = UI[] ;
    UI3[Phase3] = 0. ;
    UI2[Phase2] = 0. ;
  EndIf
  If(Type_Supply == 2)
    UI[] = F_Pulse[];
    UI1[Phase1] = UI[] ;
    UI3[Phase3] = 0. ;
    UI2[Phase2] = 0. ;
  EndIf

  shift = Pi/6; // with regard to previous implementation
  TH[] = $RotorPosition + shift; // Originally TH[] = (TH0+Om*$Time) ;
  TH_phase[] = TH[] + $1 ;
  F_PulseTH[] = (F_Period[TH_phase[$1]]{Pi/2} < Pi/6) ?  1 : 0 ; // Pi/12 +++

  If(Type_Supply == 3)
    Imax = 1 ;
    UI1[Phase1] = F_PulseTH[ Pi/6] ;
    UI3[Phase3] = F_PulseTH[ 0.  ] ;
    UI2[Phase2] = F_PulseTH[-Pi/6] ;
  EndIf

  If(!Flag_Cir)
    // Imposed current + Flag_Cir = 0: Type_Supply = 0, 1, 2
    Iphase[Phase1] = Imax * Idir[] * UI1[] ;
    Iphase[Phase2] = Imax * Idir[] * UI2[] ;
    Iphase[Phase3] = Imax * Idir[] * UI3[] ;
    js0[] = Iphase[] * Vector[0,0,1] / Sc[] ; // Used without circuit
  EndIf

  If(Type_Supply == 4) //Circuit with imposed current
    UI1[] = F_PulseTH[ Pi/6] ;
    UI3[] = F_PulseTH[ 0.  ] ;
    UI2[] = F_PulseTH[-Pi/6] ;
  EndIf

  If(Type_Supply == 5) // Circuit with imposed voltage
    Ext_Angle = Pi/3; // = 60
    a_on  = Pi/6;
    a_off = a_on + Ext_Angle;
    UI1[] = (F_Period[TH[]+a_on]{Pi/2} < a_on) ? 1. : ( (F_Period[TH[]+a_on]{Pi/2} <= a_off )? -1. : 0.) ;
    UI3[] = (F_Period[TH[]     ]{Pi/2} < a_on) ? 1. : ( (F_Period[TH[]     ]{Pi/2} <= a_off )? -1. : 0.) ;
    UI2[] = (F_Period[TH[]-a_on]{Pi/2} < a_on) ? 1. : ( (F_Period[TH[]-a_on]{Pi/2} <= a_off )? -1. : 0.) ;
  EndIf

  DefineFunction[IA, IB, IC];// Post-processing in Phase1, Phase2 and Phase3 with imposed current
  If(Type_Supply<5)
    IA[] = UI1[];
    IB[] = UI2[];
    IC[] = UI3[];
  EndIf

  Friction[] = Frict ;
  Torque_mec[] = Tmec ;
  Inertia = inertia_fe ;

}



// --------------------------------------------------------------------------

Constraint {

  { Name ElectricalCircuit ; Type Network ;

    If(!Flag_Symmetry)
      Case Circuit1 {
        { Region E1 ;             Branch {100,101} ; }
        { Region R1 ;             Branch {101,102} ; }
        { Region IndS~{1} ;       Branch {102,103} ; }
        { Region IndS~{4} ;       Branch {104,103} ; }
        { Region IndS~{(Ns+1)} ;  Branch {105,104} ; }
        { Region IndS~{(Ns+4)} ;  Branch {105,100} ; }
      }
      Case Circuit2 {
        { Region E2 ;             Branch {200,201} ; }
        { Region R2 ;             Branch {201,202} ; }
        { Region IndS~{2} ;       Branch {202,203} ; }
        { Region IndS~{5} ;       Branch {204,203} ; }
        { Region IndS~{(Ns+2)} ;  Branch {205,204} ; }
        { Region IndS~{(Ns+5)} ;  Branch {205,200} ; }
      }
      Case Circuit3 {
        { Region E3 ;             Branch {300,301} ; }
        { Region R3 ;             Branch {301,302} ; }
        { Region IndS~{3} ;       Branch {302,303} ; }
        { Region IndS~{6} ;       Branch {304,303} ; }
        { Region IndS~{(Ns+3)} ;  Branch {305,304} ; }
        { Region IndS~{(Ns+6)} ;  Branch {305,300} ; }
      }
      Else
        Case Circuit1 {
          { Region E1 ;             Branch {100,101} ; }
          { Region R1 ;             Branch {101,102} ; }
          { Region IndS~{1} ;       Branch {102,103} ; }
          { Region IndS~{(Ns+1)} ;  Branch {100,103} ; }
        }
        Case Circuit2 {
          { Region E2 ;             Branch {200,201} ; }
          { Region R2 ;             Branch {201,202} ; }
          { Region IndS~{2} ;       Branch {202,203} ; }
          { Region IndS~{(Ns+2)} ;  Branch {200,203} ; }
        }
        Case Circuit3 {
          { Region E3 ;             Branch {300,301} ; }
          { Region R3 ;             Branch {301,302} ; }
          { Region IndS~{3} ;       Branch {302,303} ; }
          { Region IndS~{(Ns+3)} ;  Branch {300,303} ; }
      }
      EndIf

  }

  { Name V_circuit ;
    Case {
      If(Flag_Cir && Type_Supply==5) // ??? /(Flag_HalfLam?2:1)
        { Region E1 ; Value Vdc ; TimeFunction  UI1[] ;}
        { Region E2 ; Value Vdc ; TimeFunction  UI2[] ;}
        { Region E3 ; Value Vdc ; TimeFunction  UI3[] ;}
      EndIf
    }
  }

  { Name I_circuit ;
    Case {
      If(Flag_Cir && Type_Supply==4)
        { Region E1 ; Value Imax ; TimeFunction  UI1[] ;}
        { Region E2 ; Value Imax ; TimeFunction  UI2[] ;}
        { Region E3 ; Value Imax ; TimeFunction  UI3[] ;}
      EndIf
    }
  }

  { Name I_stranded ; // Not necessary if circuit
    Case {
      If(!Flag_Cir)
        { Region Phase1 ; Value Imax*Idir[]/Nw; TimeFunction F_Cos_wt_p[]{2*Pi*Freq, 0 } ; }
        { Region Phase2 ; Value Imax*Idir[]/Nw; TimeFunction 0. ; }
        { Region Phase3 ; Value Imax*Idir[]/Nw; TimeFunction 0. ; }
      EndIf
    }
  }
  { Name V_stranded ;
    Case {
    }
  }

  { Name I_massive ;
    Case {
      If(!Flag_Statics)
        { Region Rotor  ; Value 0. ; }
        { Region Stator ; Value 0. ; }
      EndIf
    }
  }
  { Name V_massive ;
    Case {
    }
  }


}
// --------------------------------------------------------------------------
// --------------------------------------------------------------------------

Include "MagStaDyn_SlidingSurface.pro"

ResId = "" ;
po_mec  = StrCat["{9Output - Mechanics/", ResId];
po_mecT = StrCat[po_mec,"0Torque [Nm]/"];
po_mecP = StrCat[po_mec,"1Position [rad]/"];
po_mecV = StrCat[po_mec,"2Speed [rpm]/"];

po_mag   = StrCat["{9Output - Magnetics/", ResId];
po_magF  = StrCat[po_mag, "1Flux [Wb]/"];
po_magS  = StrCat[po_mag, "0I [A] and V [V]/"];
po_magJL = StrCat[po_mag, "2Joule losses [W]/"];


PostOperation Get_FieldMaps UsingPost MagStaDyn_a {
  If(NbrRegions[DomainC])
    Print[ j, OnElementsOf DomainC, LastTimeStepOnly, File StrCat[ResDir,"j",ExtGmsh] ] ;
    Echo[Str["k=PostProcessing.NbViews-1;","View[k].RangeType = 3;"], File "res/maps.opt"];
  EndIf
  Print[ b, OnElementsOf DomainMap, LastTimeStepOnly, File StrCat[ResDir,"b",ExtGmsh] ] ;
  Echo[Str["k=PostProcessing.NbViews-1;","View[k].RangeType = 3;"], File "res/maps.opt"];

  If(_3Dmodel)
    // Print[ rhoj2, OnElementsOf DomainC, LastTimeStepOnly, File StrCat[ResDir,"jl3d",ExtGmsh] ] ;
    // Echo[ Str["k=PostProcessing.NbViews-1;","View[k].RangeType = 3; View[k].ScaleType = 2;"], File "res/maps.opt"];
  EndIf
  If(_2Dmodel)
    For k In{2:ORDER}
      Print[ b~{2*k-2}, OnElementsOf DomainLam, LastTimeStepOnly, File StrCat[ResDir,Sprintf("b%g",2*k-2),ExtGmsh] ] ;
      Echo[ Str["k=PostProcessing.NbViews-1;","View[k].RangeType = 3;"], File "res/maps.opt"];
    EndFor
    If(!Flag_Statics)
      Print[ JouleLossesH_local, OnElementsOf DomainLamC, LastTimeStepOnly, File StrCat[ResDir,"jll",ExtGmsh] ] ;
      Echo[ Str["k=PostProcessing.NbViews-1;","View[k].RangeType = 3; View[k].ScaleType = 2;"], File "res/maps.opt"];
      // Value scale type (1: linear, 2: logarithmic, 3: double logarithmic)
    EndIf
    Print[ az, OnElementsOf DomainMap, LastTimeStepOnly, File StrCat[ResDir,"a",ExtGmsh] ] ;
    Echo[Str["k=PostProcessing.NbViews-1;", "View[k].RangeType = 3;",// per timestep
        "View[k].IntervalsType = 3;","View[k].NbIso = 25; View[k].LineWidth = 2; View[k].ScaleType = 1;"], File "res/maps.opt"];
  EndIf
}

PostOperation Get_GlobalQuantities UsingPost MagStaDyn_a {
  Print[ Flux[Phase1], OnGlobal , Format TimeTable , SendToServer StrCat[po_magF, "Ph 1"]{0}, Color "Ivory",
    LastTimeStepOnly, File  > StrCat[ResDir,Sprintf("Fl1_m%g",_3Dmodel),ExtGplt]] ;
  Print[ Flux[Phase2], OnGlobal , Format TimeTable , SendToServer StrCat[po_magF, "Ph 2"]{0}, Color "Pink",
    LastTimeStepOnly, File  > StrCat[ResDir,Sprintf("Fl2_m%g",_3Dmodel),ExtGplt]] ;
  Print[ Flux[Phase3], OnGlobal , Format TimeTable , SendToServer StrCat[po_magF, "Ph 3"]{0}, Color "LightGreen",
    LastTimeStepOnly, File  > StrCat[ResDir,Sprintf("Fl3_m%g",_3Dmodel),ExtGplt]] ;

  If(Flag_Cir) // Table does not write down # xxx on 771 772 773 => better for matlab
    Print[ U, OnRegion Region[{E1,E2,E3}], Format Table, LastTimeStepOnly,
      File > StrCat[ResDir,Sprintf("U_m%g",_3Dmodel), ExtGplt] ];
    Print[ I, OnRegion Region[{E1,E2,E3}], Format Table, LastTimeStepOnly,
      File > StrCat[ResDir,Sprintf("I_m%g",_3Dmodel), ExtGplt] ];

    Print[ U, OnRegion E1, Format Table, SendToServer StrCat[po_magS, "U Ph 1"]{0}, Color "Ivory",
      LastTimeStepOnly, File > StrCat[ResDir,"Uaux",ExtGplt]] ;
    Print[ I, OnRegion E1, Format Table, SendToServer StrCat[po_magS, "I Ph 1"]{0}, Color "Ivory",
      LastTimeStepOnly, File > StrCat[ResDir,"Iaux",ExtGplt]] ;
    Print[ U, OnRegion E2, Format Table, SendToServer StrCat[po_magS, "U Ph 2"]{0}, Color "Pink",
      LastTimeStepOnly, File > StrCat[ResDir,"Uaux",ExtGplt]] ;
    Print[ I, OnRegion E2, Format Table, SendToServer StrCat[po_magS, "I Ph 2"]{0}, Color "Pink",
      LastTimeStepOnly, File > StrCat[ResDir,"Iaux",ExtGplt]] ;
    Print[ U, OnRegion E3, Format Table, SendToServer StrCat[po_magS, "U Ph 3"]{0}, Color "LightGreen",
      LastTimeStepOnly, File > StrCat[ResDir,"Uaux",ExtGplt]] ;
    Print[ I, OnRegion E3, Format Table, SendToServer StrCat[po_magS, "I Ph 3"]{0}, Color "LightGreen",
      LastTimeStepOnly, File > StrCat[ResDir,"Iaux",ExtGplt]] ;
  EndIf
  If(!Flag_Statics)
    If(_3Dmodel)
      Print[ JouleLosses[Stator], OnGlobal , Format TimeTable , SendToServer StrCat[po_mag, "9Losses stator"]{0}, Color "Ivory",
        LastTimeStepOnly, File > StrCat[ResDir, Sprintf("JLstator_m%g", _3Dmodel),ExtGplt]] ;
      Print[ JouleLosses[Rotor], OnGlobal , Format TimeTable , SendToServer StrCat[po_mag, "9Losses rotor"]{0}, Color "Ivory",
        LastTimeStepOnly, File > StrCat[ResDir, Sprintf("JLrotor_m%g", _3Dmodel),ExtGplt]] ;

      If(Flag_AnalysisType==2)
        Print[ ComplexPower[Stator], OnGlobal , Format Table ,
          LastTimeStepOnly, File > StrCat[ResDir, Sprintf("Sstator_m%g", _3Dmodel),ExtGplt]] ;
        Print[ ComplexPower[Rotor], OnGlobal , Format Table ,
          LastTimeStepOnly, File > StrCat[ResDir, Sprintf("Srotor_m%g", _3Dmodel),ExtGplt]] ;
      EndIf
    Else
      // Homog case
      Print[ JouleLossesH[Stator], OnGlobal , Format TimeTable , SendToServer StrCat[po_mag, "9Losses stator H"]{0}, Color "Ivory",
        LastTimeStepOnly, File > StrCat[ResDir, Sprintf("JLstator_m%g", _3Dmodel),ExtGplt]] ;
      Print[ JouleLossesH[Rotor], OnGlobal , Format TimeTable , SendToServer StrCat[po_mag, "9Losses rotor H"]{0}, Color "Ivory",
        LastTimeStepOnly, File > StrCat[ResDir, Sprintf("JLrotor_m%g", _3Dmodel),ExtGplt]] ;

      If(Flag_AnalysisType==2 && Flag_ComplexReluctivity)
        Print[ ComplexPowerH[Stator], OnGlobal , Format Table ,
          LastTimeStepOnly, File > StrCat[ResDir, Sprintf("Sstator_m%g", _3Dmodel),ExtGplt]] ;
        Print[ ComplexPowerH[Rotor], OnGlobal , Format Table ,
          LastTimeStepOnly, File > StrCat[ResDir, Sprintf("Srotor_m%g", _3Dmodel),ExtGplt]] ;
      EndIf

    EndIf
  EndIf

  If(Flag_AnalysisType<2) // FD requires another expression
    If(_3Dmodel)
      // Print[ myVol[Rotor_Airgap], OnGlobal,   Format Table, LastTimeStepOnly, StoreInVariable $vol_airgap, File >StrCat[ResDir,"Vaux", ExtGplt] ] ;
      // Print[ myVol_agR, OnRegion DomainDummy, Format Table, LastTimeStepOnly, StoreInVariable $vol_airgap, File >StrCat[ResDir,"Vaux", ExtGplt] ] ;
      Print[ Torque3D_Maxwell[Rotor_Airgap], OnGlobal, Format TimeTable, LastTimeStepOnly, StoreInVariable $Trotor,
        File > StrCat[ResDir,"Tr",ExtGplt], SendToServer StrCat[po_mecT, "rotor"]{0}, Color "Ivory" ];
      /*
      // Print[ myVol_agS, OnRegion DomainDummy, Format Table, LastTimeStepOnly, StoreInVariable $vol_airgap, File >StrCat[ResDir,"Vaux", ExtGplt] ] ;
      Print[ Torque3D_Maxwell[Stator_Airgap], OnGlobal, Format TimeTable, LastTimeStepOnly, StoreInVariable $Tstator,
      File > StrCat[ResDir,"Ts",ExtGplt], SendToServer StrCat[po_mecT, "stator"]{0}, Color "Ivory" ];
      */
    Else
      Print[ Torque_Maxwell[Rotor_Airgap], OnGlobal, Format TimeTable, LastTimeStepOnly, StoreInVariable $Trotor,
        File > StrCat[ResDir,Sprintf("Tr_2d"),ExtGplt], SendToServer StrCat[po_mecT, "rotor"]{0}, Color "Ivory" ];
      // Print[ Torque_Maxwell[Stator_Airgap], OnGlobal, Format TimeTable, LastTimeStepOnly, StoreInVariable $Tstator,
      //  File > StrCat[ResDir,Sprintf("Ts_2d"),ExtGplt], SendToServer StrCat[po_mecT, "stator"]{0}, Color "Ivory" ];
    EndIf
  EndIf
}

PostOperation Get_MechanicalQs UsingPost Mechanical {
  Print[ P, OnRegion DomainKin, Format Table,
    File > StrCat[ResDir,"P", ExtGplt], LastTimeStepOnly, StoreInVariable $RotorPosition,
    SendToServer StrCat[po_mec,"11Position [rad]"]{0}, Color "Ivory"] ;
  Print[ Pdeg, OnRegion DomainKin, Format Table, File > StrCat[ResDir,"P_deg", ExtGplt], LastTimeStepOnly,
    SendToServer StrCat[po_mec,"10Position [deg]"]{0}, Color "Ivory"] ;
  // Print[ V, OnRegion DomainKin, Format Table, File > StrCat[ResDir,"V", ExtGnplt], LastTimeStepOnly,
  // SendToServer StrCat[po_mec,"21Velocity [rad\s]"]{0}, Color "Ivory"] ;//MediumPurple1
  Print[ Vrpm, OnRegion DomainKin, Format Table, File > StrCat[ResDir,"Vrpm", ExtGplt], LastTimeStepOnly,
    SendToServer StrCat[po_mec,"20Velocity [rpm]"]{0}, Color "Ivory"] ;//MediumPurple1
}



PostOperation ViewTree UsingPost MagStaDyn_a {
 If(Flag_GaugeType == TREE_COTREE_GAUGE)
    // Print the tree for debugging purposes.
    PrintGroup[ EdgesOfTreeIn[ { Vol_Tree }, StartingOn { Sur_Tree, Lin_Tree } ],
      In Vol_Tree, File StrCat[ResDir,"Tree.pos"] ];
    Echo[ Str["l=PostProcessing.NbViews-1;","View[l].ColorTable = { DarkRed };","View[l].LineWidth = 5;"] ,
      File StrCat[ResDir,"tmp.geo"], LastTimeStepOnly] ;
  EndIf

  Print[ bsurf, OnElementsOf Region[ Sur_Link ],
  	 LastTimeStepOnly, File "bsl.pos"] ;
  Echo[ Str["l=PostProcessing.NbViews-1;",
  	    "View[l].ArrowSizeMax = 100;",
  	    "View[l].CenterGlyphs = 0;",
  	    "View[l].GlyphLocation = 1;",
  	    "View[l].ScaleType = 1;",
  	    "View[l].LineWidth = 3;",
  	    "View[l].VectorType = 4;" ] ,
          File StrCat[ResDir,"tmp.geo"], LastTimeStepOnly] ;

}
