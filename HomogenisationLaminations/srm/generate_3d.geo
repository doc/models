DefineConstant[
  NumSlices = (Flag_AirLayer)?2:1,
  SlicePhysOffset = _Offset,
  SlicePhysOffset_aux = _Offset_top
];

AxialLength~{1} = dlam/(Flag_HalfLam?2:1); // width of 1 lamination (or axial length)
one~{1}[]       = one[];
layer_top~{1}[] = layer_top[];

If(Flag_AirLayer)
  AxialLength~{2} = zair; // adding air on top
  one~{2}[]       = 1;
  layer_top~{2}[] = 1;
EndIf

Geometry.AutoCoherence = 0;

ps~{0}[] = Physical Surface {:};
pl~{0}[] = Physical Line {:};
pp~{0}[] = Physical Point {:};



For slice In{1:NumSlices}
  For s In {0:#ps~{0}[]-1}
    pv~{slice}[] += ps~{slice-1}[s] + SlicePhysOffset * slice;
    ps~{slice}[] += ps~{slice-1}[s] + SlicePhysOffset_aux * slice;
    ele[] = Physical Surface{ps~{slice-1}[s]};
    name = StrCat( Physical Surface{ps~{0}[s]}, Sprintf(" (slice %g)", slice) );
    Physical Volume(pv~{slice}[s]) = {};
    Physical Surface(ps~{slice}[s]) = {};
    For e In {0:#ele[]-1}
      p[] = Extrude{0,0, AxialLength~{slice}}{
        Surface{ ele[e] }; Recombine; Layers{one~{slice}[],layer_top~{slice}[]};
      };
      Physical Volume(Str(name), pv~{slice}[s]) += p[1];
      Physical Surface(Str(name), ps~{slice}[s]) += p[0];
    EndFor
  EndFor
  For l In {0:#pl~{0}[]-1}
    psl~{slice}[] += pl~{slice-1}[l] + SlicePhysOffset * slice;
    pl~{slice}[] += pl~{slice-1}[l] + SlicePhysOffset_aux * slice;
    ele[] = Physical Line{pl~{slice-1}[l]};
    name = StrCat( Physical Line{pl~{0}[l]}, Sprintf(" (slice %g)", slice) );
    Physical Surface(psl~{slice}[l]) = {};
    Physical Line(pl~{slice}[l]) = {};
    For e In {0:#ele[]-1}
      p[] = Extrude{0,0,AxialLength~{slice}}{
        Line{ ele[e] }; Recombine; Layers{one~{slice}[], layer_top~{slice}[]};
      };
      Physical Surface(Str(name), psl~{slice}[l]) += p[1];
      Physical Line(Str(name), pl~{slice}[l]) += p[0];
    EndFor
  EndFor

    // Extruding points
  For k In {0:#pp~{0}[]-1}
    plp~{slice}[] += pp~{slice-1}[k] + SlicePhysOffset * slice; // lines from physical points
    pp~{slice}[]  += pp~{slice-1}[k] + SlicePhysOffset_aux * slice; // pnts -> aux for next layer

    ele[] = Physical Point{pp~{slice-1}[k]};
    name = StrCat( Physical Point{pp~{0}[k]}, Sprintf(" (slice %g)", slice) );

    Physical Line(plp~{slice}[k]) = {};
    Physical Point(pp~{slice}[k]) = {};
    For e In {0:#ele[]-1}
      p[] = Extrude{0,0,AxialLength~{slice}}{
        Point{ ele[e] }; Recombine; Layers{one~{slice}[], layer_top~{slice}[]};
      };
      Physical Line(Str(name), plp~{slice}[k]) += p[1];
      Physical Point(Str(name), pp~{slice}[k]) += p[0];
    EndFor
  EndFor

EndFor





Geometry.AutoCoherence = 1;
Coherence;
