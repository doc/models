clear all
close all

syms z

alpha0 = z^0 ;
alpha1 = 2*z ;
alpha2 = 1/2*(-1+12*z^2) ;
alpha3 = 1/2*(5*(2*z)^3-3*(2*z)) ;
alpha4 = 1/8*(3-120*z^2+560*z^4) ;
alpha5 = 1/8*(63*(2*z)^5-70*(2*z)^3+15*(2*z));
alpha6 = 1/16*(-5+105*(2*z)^2-315*(2*z)^4+231*(2*z)^6);

beta2 = -int(int(alpha0,z),z) + subs(int(int(alpha0,z),z),1/2);
beta3 = -int(int(alpha1,z),z) + 2*z*subs(int(int(alpha1,z),z),1/2);
beta4 = -int(int(alpha2)) + subs(int(int(alpha2)),1/2);
beta5 = -int(int(alpha3)) + 2*z*subs(int(int(alpha3)),1/2);
beta6 = -int(int(alpha4)) + subs(int(int(alpha4)),1/2);
beta7 = -int(int(alpha5)) + 2*z*subs(int(int(alpha5)),1/2);
beta8 = -int(int(alpha6)) + subs(int(int(alpha6)),1/2);

%beta2 = 1/8*(1-4*z^2);
%beta4 = 1/32*(-1+8*z^2-16*z^4);
%beta6 = 1/192*(1-4*9*z^2+16*15*z^4-64*7*z^6);

d = [-1/2:0.02:1/2];

a0 = subs(alpha0,z,d);
a1 = subs(alpha1,z,d);
a2 = subs(alpha2,z,d);
a3 = subs(alpha3,z,d);
a4 = subs(alpha4,z,d);
a5 = subs(alpha5,z,d);

%A = [d' a1' a2' a3' a4' a5']; 
%save legendre.dat -ascii -double A

cc=hsv(6) ;
figure(1),
plot(d, a0, 'color', cc(1,:), 'LineWidth',3,'DisplayName','a_0'), hold on
plot(d, a1, 'color', cc(2,:), 'LineWidth',3,'DisplayName','a_1'), 
plot(d, a2, 'color', cc(3,:), 'LineWidth',3,'DisplayName','a_2'),
plot(d, a3, 'color', cc(4,:), 'LineWidth',3,'DisplayName','a_3'),
plot(d, a4, 'color', cc(5,:), 'LineWidth',3,'DisplayName','a_4'),
plot(d, a5, 'color', cc(6,:), 'LineWidth',3,'DisplayName','a_5'),

h=legend('-DynamicLegend','Location','Best'); legend boxoff
set(h,'FontName','Symbol','FontSize',18),
axis([-0.5 0.5 -1 1.05])
set(gca,'FontName','Helvetica','FontSize',18),
set(gca,'XTick',[-0.5:0.5:0.5])
set(gca,'YTick',[-1:.5:1])
set(gca,'XTickLabel',{'-d/2','0','d/2'})
saveas(figure(1), 'alpha.eps', 'epsc')
eps2pdf('alpha.eps', 'alpha.pdf')

b2 = subs(beta2,z,d);
b3 = subs(beta3,z,d);
b4 = subs(beta4,z,d);
b5 = subs(beta5,z,d);
b6 = subs(beta6,z,d);
b7 = subs(beta7,z,d);

figure(2),
plot(d, b2, 'color', cc(1,:), 'LineWidth',3,'DisplayName','b_2'), hold on
plot(d, b3, 'color', cc(2,:), 'LineWidth',3,'DisplayName','b_3'), 
plot(d, b4, 'color', cc(3,:), 'LineWidth',3,'DisplayName','b_4'),
plot(d, b5, 'color', cc(4,:), 'LineWidth',3,'DisplayName','b_5'),
plot(d, b6, 'color', cc(5,:), 'LineWidth',3,'DisplayName','b_6'),
plot(d, b7, 'color', cc(6,:), 'LineWidth',3,'DisplayName','b_7'),

h=legend('-DynamicLegend','Location','Best'); legend boxoff
set(h,'FontName','Symbol','FontSize',18),
%axis([-0.5 0.5 -1 1.05])
set(gca,'FontName','Helvetica','FontSize',18),
set(gca,'XTick',[-0.5:0.5:0.5])
%set(gca,'YTick',[-1:.5:1])
set(gca,'XTickLabel',{'-d/2','0','d/2'})
saveas(figure(2), 'beta.eps', 'epsc')
eps2pdf('beta.eps', 'beta.pdf')


% curl h = j
gamma1 = diff(beta2,z);
gamma2 = diff(beta3,z);
gamma3 = diff(beta4,z);
gamma4 = diff(beta5,z);
gamma5 = diff(beta6,z);
gamma6 = diff(beta7,z);

g1 = subs(gamma1,z,d);
g2 = subs(gamma2,z,d);
g3 = subs(gamma3,z,d);
g4 = subs(gamma4,z,d);
g5 = subs(gamma5,z,d);
g6 = subs(gamma6,z,d);

figure(3),
plot(d, g1, 'color', cc(1,:), 'LineWidth',3,'DisplayName','g_1'), hold on
plot(d, g2, 'color', cc(2,:), 'LineWidth',3,'DisplayName','g_2'), 
plot(d, g3, 'color', cc(3,:), 'LineWidth',3,'DisplayName','g_3'),
plot(d, g4, 'color', cc(4,:), 'LineWidth',3,'DisplayName','g_4'),
plot(d, g5, 'color', cc(5,:), 'LineWidth',3,'DisplayName','g_5'),
plot(d, g6, 'color', cc(6,:), 'LineWidth',3,'DisplayName','g_6'),

h=legend('-DynamicLegend','Location','Best'); legend boxoff
set(h,'FontName','Symbol','FontSize',18),
%axis([-0.5 0.5 -1 1.05])
set(gca,'FontName','Helvetica','FontSize',18),
set(gca,'XTick',[-0.5:0.5:0.5])
%set(gca,'YTick',[-1:.5:1])
set(gca,'XTickLabel',{'-d/2','0','d/2'})
saveas(figure(3), 'gamma.eps', 'epsc')
eps2pdf('gamma.eps', 'gamma.pdf')


%figure,plot(d,b2,d,b3,d,b4,d,b5,d,b6,'LineWidth',2); legend('b2','b3','b4','b5','b6');
%B = [d' b2' b3' b4' b5' b6'];
%save beta.dat -ascii -double B   

a = -1/2 ;
b = 1/2 ;

alpha = [alpha0 alpha1 alpha2 alpha3 alpha4] ;
beta = [beta2 beta3 beta4 beta5 beta6] ;

P = int(alpha.'*alpha,a,b)
Q = int(beta.'*alpha,a,b)

P0 = int(alpha(1:2).'*alpha(1:2),a,b);
Q0 = int(alpha(1:2).'*beta(1:2),a,b);

P2 = int(alpha(1:3).'*alpha(1:3),a,b);
Q2 = int(alpha(1:3).'*beta(1:3),a,b);


n = size(alpha,2);
P_e = int(alpha(1:2:n)'*alpha(1:2:n),a,b);
Q_e = int(alpha(1:2:n)'*beta(1:2:n),a,b);


%===============================================================================
%{
N = 6;

fid = fopen('PQ.pro', 'wt');
fprintf(fid, 'Function { \n\n');

for i = 1:N
    %fprintf(fid, 'p_%d = %.16g ; \n', i-1, double(P(i,i)) );
    fprintf(fid, 'p_%d = %s ; \n', i-1, char(P(i,i)) );
end

fprintf(fid, '\n');

for i = 1:N
    for j = i:N
        %fprintf(fid, 'q_%d_%d = %.16g ; \n', i-1, j-1, double(Q(i,j)));
        fprintf(fid, 'q_%d_%d = %s ; \n', i-1, j-1, char(Q(i,j)));
    end
end
fprintf(fid, '\n');

fprintf(fid, '} \n');
fclose(fid)
%}




%figure,plot(d,g1,d,g2,d,g3,d,g4,d,g5,'LineWidth',2); 
%legend('g1','g2','g3','g4','g5');


% curl e = -d_t b
gamma1_ = int(alpha0,z);
gamma2_ = int(alpha1,z);
gamma3_ = int(alpha2,z);
gamma4_ = int(alpha3,z);
gamma5_ = int(alpha4,z);
gamma6_ = int(alpha5,z);
g1_ = subs(gamma1_,z,d);
g2_ = subs(gamma2_,z,d);
g3_ = subs(gamma3_,z,d);
g4_ = subs(gamma4_,z,d);
g5_ = subs(gamma5_,z,d);
g6_ = subs(gamma6_,z,d);

%figure,plot(d,g1_,d,g2_,d,g3_,d,g4_,d,g5_,'LineWidth',2); 
%legend('g1_','g2_','g3_','g4_','g5_');

