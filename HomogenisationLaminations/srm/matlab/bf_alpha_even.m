clear
close all

myfontsize=16;
myfontsizelegend=16;

set(0,'DefaultAxesFontName', 'Helvetica')
set(0,'DefaultAxesFontSize', 18)

syms z

alpha0 = z^0 ;
alpha2 = 1/2*(-1+12*z^2) ;
alpha4 = 1/8*(3-120*z^2+560*z^4) ;
alpha6 = 1/16*(-5+105*(2*z)^2-315*(2*z)^4+231*(2*z)^6);
alpha8 = 1/128*(6435*(2*z)^8-12012*(2*z)^6+6930*(2*z)^4-1260*(2*z)^2+35);


% Define Legendre polynomials ------------------------------------------
% Legendre polynomial basis functions (Matlab provides three different versions)
% P is symbolic; default=row vectors
n = 9;
syms x Pl;
x = 2*z;
Pl(1)=1;    % Start with P0(x)=1
Pl(2)=x;    % Start with P1(x)=x
for i=3:n  % Apply generation formula: P(i,x)=(2*i-1)/i*x*P(i-1,x)-(i-1)/i*P(i-2,x) for i=3,4,...
    Pl(i)=(2*i-3)/(i-1)*x*Pl(i-1)-(i-2)/(i-1)*Pl(i-2) ; % for lower index starting from 1
end
Pl = simplify(Pl);
even_alphas = Pl(1:2:end);
even_betas = -int(int(even_alphas,z),z) + subs(int(int(even_alphas,z),z),1/2);

beta2 = -int(int(alpha0,z),z) + subs(int(int(alpha0,z),z),1/2);
beta4 = -int(int(alpha2)) + subs(int(int(alpha2)),1/2);
beta6 = -int(int(alpha4)) + subs(int(int(alpha4)),1/2);
beta8 = -int(int(alpha6)) + subs(int(int(alpha6)),1/2);
beta10 = -int(int(alpha8)) + subs(int(int(alpha8)),1/2);

d = (-1/2:0.01:1/2);

a0 = subs(alpha0,z,d);
a2 = subs(alpha2,z,d);
a4 = subs(alpha4,z,d);
a6 = subs(alpha6,z,d);
a8 = subs(alpha8,z,d);

b2 = subs(beta2,z,d);
b4 = subs(beta4,z,d);
b6 = subs(beta6,z,d);
b8 = subs(beta8,z,d);
b10 = subs(beta10,z,d);

%A = [d'a0' a2' a4' a6' a8']; 
%B = [d' b2' b4' b6' b8'];
%save legendre.dat -ascii -double A
%save beta.dat -ascii -double B 

A = [a0' a2' a4' a6' a8']; 
B = [b2' b4' b6' b8' b10'];

nn = 4; % number of polynomials to plot < size(A,2)

cc=hsv(nn) ;
%set(0,'defaulttextinterpreter','latex');
figure, hold on 
%axis square, axis([-41 41 -2.1 2.1]) 
%title('With Jiles-Atherton model','FontName','Times New Roman','FontSize',myfontsize), hold on
for k=1:nn
    plot(d', A(:,k),'color',cc(k,:),...
         'LineWidth',2,'DisplayName',['\alpha',sprintf('%g',2*(k-1))]),
    xlabel('normalized lamination width','FontName','Times New Roman','FontSize',myfontsize), 
    %ylabel('$\alpha$','FontName','Symbol','FontSize',myfontsize), 
    h=legend('-DynamicLegend','Location','NorthWest'); legend boxoff
    set(h,'FontName','Helvetica','FontSize',myfontsizelegend), 
end

figure, hold on 
%axis square, axis([-41 41 -2.1 2.1]) 
%title('With Jiles-Atherton model','FontName','Times New Roman','FontSize',myfontsize), hold on
for k=1:nn
    plot(d', B(:,k),'color',cc(k,:),...
         'LineWidth',2,'DisplayName',['\beta',sprintf('%g',2*(k+1))])
    xlabel('normalized lamination width','FontName','Times New Roman','FontSize',myfontsize), 
    %ylabel('$\beta$','FontName','Times New Roman','FontSize',myfontsize), 
    h=legend('-DynamicLegend','Location','NorthWest'); legend boxoff
    set(h,'FontName','Helvetica','FontSize',myfontsizelegend), 
end
saveas(figure(1), 'alphas.eps', 'eps2c')
saveas(figure(2), 'betas.eps', 'eps2c')

  

%figure,plot(d,a0,d,a2,d,a4,d,a6,d,a8,'LineWidth',2);legend('a0','a2','a4','a6','a8');
%figure,plot(d,a0,d,a2,d,a4,d,a6,d,a8,'LineWidth',2);legend('a0','a1','a2','a3','a4'); %new notation
%figure,plot(d,b2,d,b4,d,b6,d,b8,d,b10,'LineWidth',2); legend('b2','b4','b6','b8','b10');
%figure,plot(d,b2,d,b4,d,b6,d,b8,d,b10,'LineWidth',2); legend('b0','b1','b2','b3','b4'); %new notation



a = -1/2 ;
b = 1/2 ;

alpha = [alpha0 alpha2 alpha4 alpha6 alpha8] ;
beta =  [beta2 beta4 beta6 beta8 beta10] ;

P = int(alpha.'*alpha,a,b)
Q = int(beta.'*alpha,a,b)


%===============================================================================

N = 8;

fid = fopen('PQ.pro', 'wt');
fprintf(fid, 'Function { \n\n');

for i = 1:2:N+1
    ii = ceil(i/2);
    %fprintf(fid, 'p_%d = %.16g ; \n', i-1, double(P(i,i)) );
    fprintf(fid, 'p_%d = %s ; \n', i-1, char(P(ii,ii)) );
end

fprintf(fid, '\n');

for i = 1:2:N+1
    ii = ceil(i/2); 
    for j = i:2:N+1
        jj = ceil(j/2);
        %fprintf(fid, 'q_%d_%d = %.16g ; \n', i-1, j-1, double(Q(i,j)));
        fprintf(fid, 'q_%d_%d = %s ; \n', i-1, j-1, char(Q(ii,jj)));
    end
end
fprintf(fid, '\n');

fprintf(fid, '} \n');
fclose(fid);



