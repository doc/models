function [P, Q] = build_PQmatrices(k, flag_all)
% n is the maximum number of terms considered in the expansion of b or h
% flag_all => if 1, we keep even and odd polynomials
% This functions generates the Legendre polynomials and its derivates

if nargin < 2
    flag_all = 0;
    k = 2*k;
end

% Define Legendre polynomials ------------------------------------------
% Legendre polynomial basis functions (Matlab provides three different versions)
% P is symbolic; default=row vectors

syms x z Pl

x = 2*z;
Pl(1)=1;    % Start with P0(x)=1
Pl(2)=x;    % Start with P1(x)=x
for ii=3:k  % Apply generation formula: P(i,x)=(2*i-1)/i*x*P(i-1,x)-(i-1)/i*P(i-2,x) for i=3,4,...
    Pl(ii)=(2*ii-3)/(ii-1)*x*Pl(ii-1)-(ii-2)/(ii-1)*Pl(ii-2) ; % for lower index starting from 1
end

Pl = simplify(Pl) ; % alphas
Pb = -int(int(Pl,z),z) + subs(int(int(Pl,z),z),1/2) ; % betas

if flag_all==0 
    step = 2 ;
end

if flag_all==1 % all polynomials 
    step = 1;
end

alpha = Pl(1:step:k) ; 
beta  = Pb(1:step:k) ; 

% alpha0 = z^0 ;
% alpha2 = 1/2*(-1+12*z^2) ;
% alpha4 = 1/8*(3-120*z^2+560*z^4) ;
% alpha6 = 1/16*(-5+105*(2*z)^2-315*(2*z)^4+231*(2*z)^6);
% alpha8 = 1/128*(6435*(2*z)^8-12012*(2*z)^6+6930*(2*z)^4-1260*(2*z)^2+35);

d = (-1/2:0.01:1/2);

% Constructing matrices P and Q
a = -1/2 ;
b = 1/2 ;

P = int(alpha.'*alpha,a,b);
Q = int(beta.'*alpha,a,b);
