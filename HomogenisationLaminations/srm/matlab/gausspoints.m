clear
close all
format long

syms x z 
x = 2*z ;

%Even legendre polynomials defined between [-1/2,1/2]
alpha0 = x^0 ;
alpha2 = 1/2*(3*x^2-1) ;
alpha4 = 1/8*(35*x^4-30*x^2+3);
alpha6 = 1/16*(231*x^6-315*x^4+105*x^2-5);
alpha8 = 1/128*(6435*x^8-12012*x^6+6930*x^4-1260*x^2+35);
alpha10 = 1/256*(46189*x^10-109395*x^8+90090*x^6-30030*x^4+3465*x^2-63);

N = 10 ; % Watch out! It must be even, as we are integrating on
         % half the width of the lamination, i.e. taking half the
         % number of gauss points

% Using Legendre-Gauss quadrature: N points in [-1,1]
[lx,lp] = lgwt(N,-1,1) ; 

% Shell defined in [-1/2,1/2] ==> Divide by 2 the Gauss pnts
% lx_ = lx/2; lp_ = lp/2;

% for taking advantage of symmetry: only half of the
% gauss points... ==> watch out N must be pair 
lx_ = lx(lx>=0)/2 ;
lp_ = 2*lp(lx>=0)/2 ;

use_myalpha=1;
if(use_myalpha)
    a2_ = double(subs(alpha2,z,lx_));
    a4_ = double(subs(alpha4,z,lx_));
    a6_ = double(subs(alpha6,z,lx_));
    a8_ = double(subs(alpha8,z,lx_));
    a10_ = double(subs(alpha10,z,lx_));
else
    % using the legendre function from Matlab
    a2_  = legendreP(2,2*lx_) ;
    a4_  = legendreP(4,2*lx_) ;
    a6_  = legendreP(6,2*lx_) ;
    a8_  = legendreP(8,2*lx_) ;
    a10_ = legendreP(10,2*lx_) ;
end

%zth = [-1/2:0.01:1/2];
zth = (0:0.005:1/2);

a2 = legendreP(2,2*zth) ;
a4 = legendreP(4,2*zth) ;
a6 = legendreP(6,2*zth) ;
a8 = legendreP(8,2*zth) ;
a10 = legendreP(10,2*zth) ;

aa = [a2 ; a4 ; a6 ; a8 ; a10] ;
aa_ = [a2_  a4_  a6_  a8_  a10_] ;

order_max=10;
cc=hsv(order_max/2) ;
markers = {'+','o','*','.','x','s','d','^','v','>','<','p','h'};

figure, hold on
legend('-DynamicLegend', 'Location','Best')
for k=1:order_max/2 
    plot(zth,aa(k,:),'color',cc(k,:),'LineWidth',2,'DisplayName', ...
         sprintf('alpha%g',2*k)), 
    plot(lx_,aa_(:,k),markers{mod(k,numel(markers))},'color',cc(k,:),...
         'DisplayName',sprintf('alpha%g',2*k),'LineWidth',2, 'MarkerSize',8);
end



A = aa_.*aa_.*(lp_*ones(1,size(aa_,2)));
Pnum = sum(A);

orders = (2:2:order_max);
P = 1./(2*orders+1);
%P = [1/5 1/9 1/13 1/17 1/21]

RelativeError = (Pnum-P)./P;


%===============================================================================
if(0) % one for creating file

N = ceil(N/2); % if symmetry taken into account    
filename = sprintf('gausspoints_Ngp%g.pro',N);    
fid = fopen(filename, 'wt');

fprintf(fid, 'Function { \n\n');

fprintf(fid, 'Nqp = %.16g ; // nbrQuadraturePoints \n', N );

fprintf(fid, 'y_%d = %.16g ; \n', [1:N; lx_']);% gauss point lamination                                                  
fprintf(fid, '\n');

fprintf(fid, 'w_%d = %.16g ; \n', [1:N; lp_']);
fprintf(fid, '\n');

fprintf(fid, 'a_2_%d = %.16g ; \n', [1:N; a2_']);
fprintf(fid, '\n');

fprintf(fid, 'a_4_%d = %.16g ; \n', [1:N; a4_']);
fprintf(fid, '\n');

fprintf(fid, 'a_6_%d = %.16g ; \n', [1:N; a6_']);
fprintf(fid, '\n');

fprintf(fid, 'a_8_%d = %.16g ; \n', [1:N; a8_']);
fprintf(fid, '\n');

fprintf(fid, '} \n');
%================================================================
fclose(fid)

end
%===============================================================================
