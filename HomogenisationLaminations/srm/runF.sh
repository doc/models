#!/bin/sh

m3d=1

if [ $m3d = 1 ]; then
    mymesh="srm3d.msh"
else
    mymesh="srm2d.msh"
fi
echo $mymesh

getmesh=0
if [ $getmesh  = 1 ]; then
    gmsh srm.geo -nt 4 -v 4 -3 -setnumber _3Dmodel $m3d -setnumber Flag_Symmetry 1 -setnumber Flag_HalfLam $m3d -o $mymesh
fi

nn=40; 
fmin=1;
fmax=10000;

# th=-15 #rotor and stator teeth aligned 
# th=-30 #rotor and stator teeth misaligned (half tooth)
th=15 #rotor and stator teeth fully misaligned

if [ $m3d = 1 ]; then
    for (( i=1 ; i<=$nn ; i++ )) ; do
        echo "====================== FE $i ==========================" ;
        echo "e(l($fmin)+($i-1)*l($fmax/$fmin)/($nn-1))" | bc -l
        ff=`echo "e(l($fmin)+($i-1)*l($fmax/$fmin)/($nn-1))" | bc -l`
        getdp -v 3 -cpu srm.pro -msh $mymesh \
              -setnumber visu 0 \
              -setnumber _3Dmodel $m3d \
              -setnumber Flag_Symmetry 1 \
              -setnumber Flag_AnalysisType 2 \
              -setnumber Type_Supply 1 \
              -setnumber Freq $ff \
              -setnumber _thetaInit0 $th \
              -sol Analysis
    done
else
    for (( i=1 ; i<=$nn ; i++ )) ; do
        #echo "e(l($fmin)+($i-1)*l($fmax/$fmin)/($nn-1))" | bc -l
        ff=`echo "e(l($fmin)+($i-1)*l($fmax/$fmin)/($nn-1))" | bc -l`
        for (( o=0 ; o<=3 ; o++ )) ; do
            if [ $o = 0 ]; then
                order=$o
            else
                order=`echo "(-1*$o)" | bc -l`
            fi
            echo "====================== FE $i ORDER $order ==========================" ;           
            getdp -v 3 -cpu srm.pro -msh $mymesh \
                  -setnumber visu 0 \
                  -setnumber _3Dmodel $m3d \
                  -setnumber Flag_Symmetry 1 \
                  -setnumber Flag_AnalysisType 2 \
                  -setnumber Type_Supply 1 \
                  -setnumber Freq $ff \
                  -setnumber ORDER $order \
                  -setnumber _thetaInit0 $th \
                  -sol Analysis
        done
    done
fi

