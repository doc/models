#!/bin/sh

m3d=0
if [ $m3d = 1 ]; then
    mymesh="srm3d.msh"
    i0=1
    nn=1 # ORDER is not used
else
    mymesh="srm2d.msh"
    i0=3
    nn=3 #order: 1,2,3
fi

getmesh=0
if [ $getmesh  = 1 ]; then
    gmsh srm.geo -nt 4 -v 4 -3 -setnumber _3Dmodel $m3d -setnumber Flag_Symmetry 1 -setnumber Flag_HalfLam $m3d -o $mymesh
fi

ff=2000
rpm=20000
nbs=360 #default is 180 => 1 degree per step

runthis=1
if [ $runthis = 1 ]; then
for (( i=$i0 ; i<=$nn ; i++ )) ; do
    echo "====================== ORDER $i ==========================" ;
    getdp -v 3 -cpu srm.pro -msh $mymesh \
          -setnumber visu 0 \
          -setnumber _3Dmodel $m3d \
          -setnumber ORDER $i \
          -setnumber Flag_Symmetry 1 \
          -setnumber Flag_AnalysisType 1 \
          -setnumber Type_Supply 5 \
          -setnumber Freq $ff \
          -setnumber myrpm $rpm \
          -setnumber NbSteps $nbs \
          -sol Analysis
done
fi

