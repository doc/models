// Switched Reluctance Motor Parameter File (2D)
// All dimensions in meters and rads
// Ruth V. Sabariego (August, 2013)

mm = 1e-3 ;
deg2rad = Pi/180 ;
rad2deg = 1/deg2rad;

pp   = "{0Geo. & mesh parameters/";
main = "{1Sim. parameters/";

close_menu = 1;
colorro  = "LightGrey";
colorpp = "Ivory";

StatorDim = "1Stator dimensions/" ;
RotorDim = "2Rotor dimensions/" ;

DefineConstant[
  _3Dmodel = {0, Choices {0, 1}, ServerAction Str["Reset", StrCat[main,"02Half lamination width?"]],
    Name StrCat[main,"00 3D model?"], Highlight "Blue"}
  Flag_Symmetry = {1, Choices {0="Full geo", 1="Half geo"},
    Name StrCat[main,"01Type of FE model"], Highlight "Blue"}
  Flag_AirLayer = {0, Choices{0,1},
    Name StrCat[main,"02Adding air layer?"], Highlight "Yellow", Visible _3Dmodel*0} // Not used

  Flag_HalfLam = {_3Dmodel, Choices{0,1},
    Name StrCat[main,"02Half lamination width?"],
    Highlight "Yellow", Visible _3Dmodel, ReadOnly Flag_AirLayer} //if Lam complete, model not ready with airlayer

  NbrStatorPoles = { Flag_Symmetry ? 3:6, Choices {3="3", 6="6"},
    Name StrCat[main,"03Number of stator poles"], ReadOnly 1, Highlight "Black", Visible 1}
  NbrRotorPoles = { Flag_Symmetry ? 2:4, Choices {2="2", 4="4"},
    Name StrCat[main,"04Number of rotor poles"], ReadOnly 1, Highlight "Black"}

  //--------------------------------------------------------------------------------

  ag     = {1*0.29, Name StrCat[pp, "0Air gap width"], Units 'mm', Highlight Str[colorpp], Closed close_menu},

  Rsout  = {60, Name StrCat[pp, StatorDim, "11Outer radius"], Units 'mm', Highlight Str[colorpp]},
  Rsin   = {30, Name StrCat[pp,  StatorDim, "12Inner radius"], Units 'mm', Highlight Str[colorpp]},
  YWs    = {12, Name StrCat[pp,  StatorDim, "13Yoke width"], Units 'mm', Highlight Str[colorpp]},
  Betas_deg = {32, Name StrCat[pp,  StatorDim, "14Pole arc"], Units 'deg', Highlight Str[colorpp]},

  Rrout  = {Rsin-ag, Name StrCat[pp,  RotorDim, "11Outer radius"], Units 'mm',ReadOnly 1, Highlight Str[colorro]},
  Rrin  = {20, Name StrCat[pp,  RotorDim, "12Outer yoke radius"], Units 'mm',Highlight Str[colorpp]},
  Rshaft  = {6, Name StrCat[pp,  RotorDim, "13Shaft radius"], Units 'mm',Highlight Str[colorpp]},
  Betar_deg = {32, Name StrCat[pp,  RotorDim, "14Pole arc"], Units 'deg', Highlight Str[colorpp]}

  clFactor = {1, Name StrCat[pp,"03mesh density factor"], Highlight "Ivory"}
  dlam   = { 0.5, Name StrCat[pp,"02lamination width"], Units 'mm', Highlight "Ivory" }
  de     = { 0*0.05, Name StrCat[pp,"03isolation thickness"], Units 'mm'}
  fillfactor = { _3Dmodel ? 1. : dlam/(dlam+de), Name StrCat[pp,"03fill factor"], Units '', ReadOnly 1}
  nllam  = { 5, Name StrCat[pp,"03nb layers in half lamination"], Highlight "Yellow"}
  _adapt_dlam_mesh = {1, Choices {0, 1}, Name StrCat[pp,"04 non uniform distribution of layers in lamination thickness"], Highlight "Red"}
  alpha = { (_adapt_dlam_mesh) ? 0.5:1.,
    Name StrCat[pp,"05ratio for progression in lamination"], Highlight "Yellow", Visible _adapt_dlam_mesh, ReadOnly !_adapt_dlam_mesh}
  // 1 for uniform distribution of layers, < 1 for thinner close to boundary, > 1 for thinner in the middle

  // AxialLength = {60, Name StrCat[pp, "0Axial length"], Units 'mm', Highlight Str[colorpp]} // to correct for 1 lamination cases, different in 2D and 3D

  nblam = {60/dlam, Name StrCat[pp, "04Number of laminations"],Highlight Str[colorpp]}
  AxialLength = {!_3Dmodel?(nblam*dlam):1/mm, Name StrCat[pp, "05Axial length"],
    Units 'mm', Highlight Str[colorro], ReadOnly}
];

ag=ag*mm;
AxialLength = AxialLength*mm;
dlam = dlam*mm;
de = de*mm;
e  = dlam+de;

Rsout = Rsout*mm;
Rsin=Rsin*mm;
YWs =YWs*mm;

Rrout =Rrout*mm;
Rrin =Rrin*mm;
Rshaft = Rshaft*mm;

Betas = Betas_deg * deg2rad ;
Betar = Betar_deg * deg2rad ;

zair = 1/8*dlam;
Rair = Rsout+zair;

Lc = 7.5e-4/2*clFactor ; // base characteristic length

SlidingSurfaceShift = 1e-5;

//------------------------------------------------------------------------------
// moving band
// transfine meshing in moving band
// some layers in airgap
frac_ag = 1/4;
Rag1 = Rsin-frac_ag*ag; // Outer layer from stator
Rag2 = Rsin-2*frac_ag*ag;

Ragr1 = Rrout+frac_ag*ag;
Ragr2 = Rrout+2*frac_ag*ag; // Outer layer from rotor

Rair2= Rag2;
Rair3= Ragr2;

Rag_rotor  = Rair3;
Rag_stator = Rair2;

// Printf("Rag_rotor %g %g", Ragr1, Ragr2);
// Printf("Rag_stator %g %g", Rag1, Rag2);

// Vol_agR = 0.5*Pi*(Ragr2^2-Ragr1^2)*dlam/(Flag_HalfLam?2.:1.);
// Vol_agS = 0.5*Pi*(Rag1^2-Rag2^2)*dlam/(Flag_HalfLam?2.:1.);
// Printf("Vol airgap Rotor %g Vol airgap Stator= %g", Vol_agR, Vol_agS);


// ndiv_mb  = Ceil[2*Pi*Rag2/2/(Lc)/3] + 1 ; // number of divisions = ndiv_mb-1 ;
// nDivBand = 4 * (ndiv_mb-1) ;
ndiv_mb  = 90 + 1; // 90 => 1 degree
nDivBand = 4 * (ndiv_mb-1) ;
// Printf("====> ndiv_mb %g, nDivBand = %g", nDivBand/4, nDivBand);

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// Stator:
NbrStatorPolesTot = 6 ;
SymmetryFactor = NbrStatorPolesTot/NbrStatorPoles ;
Ns = NbrStatorPolesTot;
th0ss = Pi/Ns;    // +++ 0 Angular pos. stator
dths = 2.*Pi/Ns;  // Ang. shift between 2 stator poles

aperture0 = 2*Pi/nDivBand ; // angular aperture of one single mesh element

// Rotor
NbrRotorPolesTot = 4 ;  // Num. of Rotor Poles
Nr = NbrRotorPolesTot;
th0rs = Pi/Nr; // horizontal InitialRotorAngle; // Angular pos. rotor
dthr   = 2.*Pi/Nr ;	// Ang. shift between 2 rotor poles

DefineConstant[
  murIron  = {1000, Name StrCat[pp, "Relative permeability for linear case"]}
  sigmaLam = {5e6,  Name StrCat[pp, "iron conductivity"]}
];

VA = 220 ; // rms values
IA = 1/Sqrt[2] ;
inertia_fe = 8.3e-3 ;


//------------------------------------------------------------
// Physical regions
//------------------------------------------------------------

STATOR = 1000 ; SKIN_STATOR = 10000 ;
ROTOR  = 2000 ; SKIN_ROTOR  = 20000 ;
SHAFT  = 2100 ;

COILN  = 1100 ; SKIN_COILN  = 11000 ; CUT_COILN  = 11100 ;
COILP  = 1200 ; SKIN_COILP  = 12000 ; CUT_COILP  = 12100 ;

BND_STATOR = 5555 ;
BND_A0_STATOR = 5600;
BND_A1_STATOR = 5601;

TOP_STATOR = 5560 ; BOT_STATOR = 5561 ;
TOP_ROTOR  = 5570 ; BOT_ROTOR  = 5571 ;
TOP_AIR    = 5580 ; BOT_AIR = 5581;
TOP_COILS  = 5590 ; BOT_COILS = 5591;

AIRGAP_STATOR = 3000 ;
AIR_STATOR  = 3001 ;
AIRGAP_ROTOR  = 3100 ;
AIR_ROTOR  = 3101 ;

AIRLAYER = 4000; BND_AIRLAYER = 4444;

BND_A0_ROTOR = 6600;
BND_A1_ROTOR = 6601;

MB_STATOR  = 1111 ; MB_ROTOR   = 2222 ;

MB_STATOR_SURF = 3330 ;
MB_ROTOR_SURF  = 3340 ;

REF_PNT_ROTOR     = 8800;
REF_PNT_MB_ROTOR  = 8801;
REF_PNT_MB_STATOR = 8802;

NICEPOS = 111111 ;

_Offset=1e6;
_Offset_top=1e7;

TREELINES= 22;
