Include "domDisk.dat";

Point(0) = {0, 0, 0};

Point(1) = {-R_SCA,     0, 0};
Point(2) = {     0,-R_SCA, 0};
Point(3) = { R_SCA,     0, 0};
Point(4) = {     0, R_SCA, 0};

Circle(1) = {1, 0, 2};
Circle(2) = {2, 0, 3};
Circle(3) = {3, 0, 4};
Circle(4) = {4, 0, 1};

If((R_DOM-R_SCA) > LC)
  Point(5) = {-R_DOM,     0, 0};
  Point(6) = {     0,-R_DOM, 0};
  Point(7) = { R_DOM,     0, 0};
  Point(8) = {     0, R_DOM, 0};

  Circle(5) = {5, 0, 6};
  Circle(6) = {6, 0, 7};
  Circle(7) = {7, 0, 8};
  Circle(8) = {8, 0, 5};

  Line Loop(1) = {1, 2, 3, 4, -5, -6, -7, -8};
  Plane Surface(1) = {1};

  If(BND_TYPE == BND_PML)
  If(Npml > 0)
    Extrude { Line{5, 6, 7, 8}; Layers{Npml,Npml*LC}; Recombine; }
    Physical Line(BND_PmlExt) = {9, 13, 17, 21};
    Physical Surface(DOM_PML) = {12, 16, 20, 24};
  Else
    Physical Line(BND_PmlExt) = {5, 6, 7, 8};
    Physical Surface(DOM_PML) = {};
  EndIf
  EndIf

  Physical Line(BND_Dom) = {5, 6, 7, 8};
  Physical Surface(DOM) = {1};
Else

  If(BND_TYPE == BND_PML)
    Extrude { Line{1, 2, 3, 4}; Layers{Npml,Npml*LC}; Recombine; }
    Physical Line(BND_PmlExt) = {5, 9, 13, 17};
    Physical Surface(DOM_PML) = {8, 12, 16, 20};
  EndIf

  Physical Line(BND_Dom) = {};
  Physical Surface(DOM) = {};
EndIf

Physical Line(BND_Scatt) = {1, 2, 3, 4};
