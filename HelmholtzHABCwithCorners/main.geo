Include "main.dat" ;

CreateDir Str(DIR);

SetOrder ORDER;
Mesh.ElementOrder = ORDER;
Mesh.SecondOrderLinear = 0;

Mesh.CharacteristicLengthMax = LC;
Mesh.CharacteristicLengthFactor = 1;
Mesh.Optimize = 1;

// Solver.AutoMesh = 1;

Include Str[LinkGeo];
