Include "domPolygon.dat";

//==================================================================================================
// OPTIONS and PARAMETERS
//==================================================================================================

BND_Neumann       = 0;
BND_Sommerfeld    = 1;
BND_Second        = 2;
BND_PadeCont      = 3;
BND_PadeDisc      = 4;
CRN_Nothing       = 0;
CRN_Damping       = 1;
CRN_DampingNum    = 2;
CRN_Compatibility = 3;
CRN_Sommerfeld    = 4;
CRN_DampingNum2   = 5;

DefineConstant[
  BND_TYPE = {BND_PadeCont,
    Name "Input/5Model/03Boundary condition (edges)", Highlight "Blue",
    Choices {BND_Neumann    = "Homogeneous Neumann",
             BND_Sommerfeld = "Sommerfeld ABC",
             BND_Second     = "Second-order ABC",
             BND_PadeDisc   = "Pade ABC (disc at corners)",
             BND_PadeCont   = "Pade ABC (cont at corners)"}},
  CRN_TYPE = {CRN_DampingNum,
    Name "Input/5Model/04Boundary condition (corners)", Highlight "Red",
    Visible ((BND_TYPE == BND_Second) || (BND_TYPE == BND_PadeDisc) || (BND_TYPE == BND_PadeCont)),
    Choices {CRN_Nothing       = "Nothing",
             CRN_DampingNum2   = "Damping with num curv (without aux terms)",
             CRN_DampingNum    = "Damping with num curv",
             CRN_Damping       = "Damping with formula curv",
             CRN_Compatibility = "Compatibility",
             CRN_Sommerfeld    = "Sommerfeld ABC at Corners"}}
];

DefineConstant[
  nPade = {4, Choices {0, 1, 2, 3, 4, 5, 6, 7, 8},
    Name "Input/5Model/05Pade: Number of fields",
    Visible ((BND_TYPE == BND_PadeDisc) || (BND_TYPE == BND_PadeCont))},
  thetaPadeInput = {3, Min 0, Step 1, Max 4,
    Name "Input/5Model/06Pade: Rotation of branch cut",
    Visible ((BND_TYPE == BND_PadeDisc) || (BND_TYPE == BND_PadeCont))}
];

If((BND_TYPE == BND_PadeDisc) || (BND_TYPE == BND_PadeCont))
  If(thetaPadeInput == 0)
    thetaPade = 0;
  EndIf
  If(thetaPadeInput == 1)
    thetaPade = Pi/8;
  EndIf
  If(thetaPadeInput == 2)
    thetaPade = Pi/4;
  EndIf
  If(thetaPadeInput == 3)
    thetaPade = Pi/3;
  EndIf
  If(thetaPadeInput == 4)
    thetaPade = Pi/2;
  EndIf
  mPade = 2*nPade+1;
  For j In{1:nPade}
    cPade~{j} = Tan[j*Pi/mPade]^2;
  EndFor
Else
  nPade = 0;
  thetaPadeInput = 0;
EndIf

Group {
  Dom    = Region[{DOM}];
  BndSca = Region[{BND_SCA}];

  CrnAll = {};
  EdgAll = {};
  For iCrn In{1:DOM_Num}
    Crn~{iCrn} = Region[{CRN~{iCrn}}];
    CrnAll += Region[{CRN~{iCrn}}];
  EndFor
  For iEdg In{1:DOM_Num}
    iCrn1 = (iEdg == 1) ? DOM_Num : iEdg-1;
    iCrn2 = iEdg;
    Edg~{iEdg} = Region[{BND~{iEdg}}];
    EdgClo~{iEdg} = Region[{BND~{iEdg},CRN~{iCrn1},CRN~{iCrn2}}];
    EdgAll += Region[{BND~{iEdg}}];
  EndFor

  DomAll = Region[{Dom,BndSca,EdgAll,CrnAll}];

If(FLAG_REF == REF_Ana)
  DomRef    = Region[{DOM}];
  DomRefAll = Region[{DomRef,BndSca,EdgAll,CrnAll}];
Else
  CrnRefAll = {};
  EdgRefAll = {};
  For iCrn In{1:4}
    CrnRef~{iCrn} = Region[{CRN_EXT~{iCrn}}];
    CrnRefAll += Region[{CRN_EXT~{iCrn}}];
  EndFor
  For iEdg In{1:4}
    iCrn1 = (iEdg == 1) ? 4 : iEdg-1;
    iCrn2 = iEdg;
    EdgRef~{iEdg} = Region[{BND_EXT~{iEdg}}];
    EdgRefClo~{iEdg} = Region[{BND_EXT~{iEdg},CRN_EXT~{iCrn1},CRN_EXT~{iCrn2}}];
    EdgRefAll += Region[{BND_EXT~{iEdg}}];
  EndFor

  DomRef    = Region[{DOM,DOM_EXT}];
  DomRefAll = Region[{DomRef,BndSca,EdgRefAll,CrnRefAll}];
EndIf
}

Function {

  NormalNum[] = VectorField[XYZ[]]{1001};
  CurvNum[]   = ScalarField[XYZ[]]{1002};

  For iEdg In{1:DOM_Num}
    AngleNormal = (iEdg-1)*angleInterior*Pi/180 - Pi/2;
    NormalGeo[Edg~{iEdg}] = Vector[Cos[AngleNormal], Sin[AngleNormal], 0.];
  EndFor

If(CRN_TYPE == CRN_Damping)
  //CurvCorner = (2)^(1/2)/LC;
  alphaDom = Pi - 2*Pi/DOM_Num;
  CurvCorner = 1/(Tan[alphaDom/2.]*LC);
  distMin~{0}[] = L_EXT;
For i In{1:DOM_Num}
  dist~{i}[] = Sqrt[(X[]-X~{i})^2 + (Y[]-Y~{i})^2];
  distMin~{i}[] = (distMin~{i-1}[] < dist~{i}[]) ? distMin~{i-1}[] : dist~{i}[];
EndFor
  Curv[EdgAll] = (distMin~{DOM_Num}[] < LC) ? CurvCorner : 0;
ElseIf((CRN_TYPE == CRN_DampingNum) || (CRN_TYPE == CRN_DampingNum2))
If(DOM_Num == 1)
  Curv[EdgAll] = 1/DOM_R;
Else
  Curv[EdgAll] = CurvNum[];
EndIf
Else
  Curv[EdgAll] = 0.;
EndIf

  kEps[EdgAll] = WAVENUMBER + I[] * 0.39 * WAVENUMBER^(1/3) * (Curv[])^(2/3);
  alphaBTPade[EdgAll] = Curv[]/2 + (Curv[])^2 * 1/(8.*(I[]*k[] - Curv[]));
  betaBTPade[EdgAll]  = - Curv[]/(2*k[]*k[]);

If((BND_TYPE == BND_PadeDisc) || (BND_TYPE == BND_PadeCont))
  ExpPTheta[]  = Complex[Cos[ thetaPade],Sin[ thetaPade]];
  ExpMTheta[]  = Complex[Cos[-thetaPade],Sin[-thetaPade]];
  ExpPTheta2[] = Complex[Cos[thetaPade/2.],Sin[thetaPade/2.]];
EndIf

If(FLAG_REF == REF_Num)
  nPadeRef = 8;
  mPadeRef = 2*nPadeRef+1;
  thetaPadeRef = Pi/3;
  For j In{1:nPadeRef}
    cPadeRef~{j} = Tan[j*Pi/mPadeRef]^2;
  EndFor
  ExpPThetaRef[]  = Complex[Cos[ thetaPadeRef],Sin[ thetaPadeRef]];
  ExpMThetaRef[]  = Complex[Cos[-thetaPadeRef],Sin[-thetaPadeRef]];
  ExpPThetaRef2[] = Complex[Cos[thetaPadeRef/2.],Sin[thetaPadeRef/2.]];
EndIf
}

//==================================================================================================
// FONCTION SPACES with CONSTRAINTS
//==================================================================================================

If(FLAG_SIGNAL_BC == SIGNAL_Dirichlet)
Constraint {
  { Name DirichletBC; Case {{ Region BndSca; Value -f_inc[]; }}}
}
EndIf

FunctionSpace {
  { Name H_nx;   Type Form0; BasisFunction {{ Name si; NameOfCoef pi; Function BF_Node; Support Region[{EdgAll}]; Entity NodesOf[All]; }}}
  { Name H_ny;   Type Form0; BasisFunction {{ Name si; NameOfCoef pi; Function BF_Node; Support Region[{EdgAll}]; Entity NodesOf[All]; }}}
  { Name H_cur;  Type Form0; BasisFunction {{ Name si; NameOfCoef pi; Function BF_Node; Support Region[{EdgAll}]; Entity NodesOf[All]; }}}
  { Name H_proj; Type Form0; BasisFunction {{ Name si; NameOfCoef pi; Function BF_Node; Support Region[{Dom}];    Entity NodesOf[All]; }}}
If(FLAG_REF == REF_Num)
  { Name H_ref; Type Form0; BasisFunction {{ Name si; NameOfCoef pi; Function BF_Node; Support Region[{DomRefAll}]; Entity NodesOf[All]; }}
If(FLAG_SIGNAL_BC == SIGNAL_Dirichlet)
    Constraint {{ NameOfCoef pi; EntityType NodesOf; NameOfConstraint DirichletBC; }}
EndIf
  }
  For iEdg In {1:4}
  For i In {1:nPadeRef}
  { Name H_ref~{iEdg}~{i}; Type Form0; BasisFunction {{ Name sn; NameOfCoef un; Function BF_Node; Support Region[{EdgRefClo~{iEdg}}]; Entity NodesOf[All]; }}}
  EndFor
  EndFor
  For iCrn In {1:4}
  For i In {1:nPadeRef}
  For j In {1:nPadeRef}
  { Name H_ref~{iCrn}~{i}~{j}; Type Form0; BasisFunction {{ Name sn; NameOfCoef un; Function BF_Node; Support Region[{CrnRef~{iCrn}}]; Entity NodesOf[All]; }}}
  EndFor
  EndFor
  EndFor
EndIf
  { Name H_num; Type Form0; BasisFunction {{ Name si; NameOfCoef pi; Function BF_Node; Support Region[{DomAll}]; Entity NodesOf[All]; }}
If(FLAG_SIGNAL_BC == SIGNAL_Dirichlet)
    Constraint {{ NameOfCoef pi; EntityType NodesOf; NameOfConstraint DirichletBC; }}
EndIf
  }
If(BND_TYPE == BND_PadeCont)
  For i In {1:nPade}
  { Name H~{i}; Type Form0; BasisFunction {{ Name sn; NameOfCoef un; Function BF_Node; Support Region[{EdgAll}]; Entity NodesOf[All]; }}}
  EndFor
EndIf
If(BND_TYPE == BND_PadeDisc)
  For iEdg In {1:DOM_Num}
  For i In {1:nPade}
  { Name H~{iEdg}~{i}; Type Form0;
    BasisFunction {{ Name sn; NameOfCoef un; Function BF_Node; Support Region[{EdgClo~{iEdg}}]; Entity NodesOf[All]; }}
  }
  EndFor
  EndFor
If(CRN_TYPE == CRN_Compatibility)
  For iCrn In {1:DOM_Num}
  For i In {1:nPade}
  For j In {1:nPade}
  { Name H~{iCrn}~{i}~{j}; Type Form0;
    BasisFunction {{ Name sn; NameOfCoef un; Function BF_Node; Support Region[{Crn~{iCrn}}]; Entity NodesOf[All]; }}
  }
  EndFor
  EndFor
  EndFor
EndIf
EndIf
}

//==================================================================================================
// FORMULATIONS
//==================================================================================================

Formulation {
  { Name NumNormal; Type FemEquation;
    Quantity {
      { Name u_nx; Type Local; NameOfSpace H_nx; }
      { Name u_ny; Type Local; NameOfSpace H_ny; }
    }
    Equation {
      Galerkin{ [ Dof{u_nx}           , {u_nx} ]; In Region[{EdgAll}]; Jacobian JSur; Integration I1; }
      Galerkin{ [ Dof{u_ny}           , {u_ny} ]; In Region[{EdgAll}]; Jacobian JSur; Integration I1; }
      Galerkin{ [ -CompX[NormalGeo[]] , {u_nx} ]; In Region[{EdgAll}]; Jacobian JSur; Integration I1; }
      Galerkin{ [ -CompY[NormalGeo[]] , {u_ny} ]; In Region[{EdgAll}]; Jacobian JSur; Integration I1; }
    }
  }

  { Name NumCur; Type FemEquation;
    Quantity {
      { Name u_nx; Type Local; NameOfSpace H_nx; }
      { Name u_ny; Type Local; NameOfSpace H_ny; }
      { Name u_cur; Type Local; NameOfSpace H_cur; }
    }
    Equation {
      Galerkin{ [ Dof{u_nx}           , {u_nx} ]; In Region[{EdgAll}]; Jacobian JSur; Integration I1; }
      Galerkin{ [ Dof{u_ny}           , {u_ny} ]; In Region[{EdgAll}]; Jacobian JSur; Integration I1; }
      Galerkin{ [ -CompX[NormalNum[]] , {u_nx} ]; In Region[{EdgAll}]; Jacobian JSur; Integration I1; }
      Galerkin{ [ -CompY[NormalNum[]] , {u_ny} ]; In Region[{EdgAll}]; Jacobian JSur; Integration I1; }

      Galerkin{ [ Dof{u_cur}                   , {u_cur} ]; In Region[{EdgAll}]; Jacobian JSur; Integration I1; }
      Galerkin{ [ -Vector[1,0,0] * Dof{d u_nx} , {u_cur} ]; In Region[{EdgAll}]; Jacobian JSur; Integration I1; }
      Galerkin{ [ -Vector[0,1,0] * Dof{d u_ny} , {u_cur} ]; In Region[{EdgAll}]; Jacobian JSur; Integration I1; }
    }
  }

If(FLAG_REF == REF_Num)
  { Name NumRef; Type FemEquation;
    Quantity {
      { Name u_ref; Type Local; NameOfSpace H_ref; }
    For iEdg In {1:4}
    For i In {1:nPadeRef}
      { Name u_ref~{iEdg}~{i}; Type Local; NameOfSpace H_ref~{iEdg}~{i}; }
    EndFor
    EndFor
    For iCrn In {1:4}
    For i In {1:nPadeRef}
    For j In {1:nPadeRef}
      { Name u_ref~{iCrn}~{i}~{j}; Type Local; NameOfSpace H_ref~{iCrn}~{i}~{j}; }
    EndFor
    EndFor
    EndFor
    }
    Equation {

      Galerkin{ [ Dof{d u_ref}    , {d u_ref} ]; In DomRef; Jacobian JVol; Integration I1; }
      Galerkin{ [ -k[]^2*Dof{u_ref} , {u_ref} ]; In DomRef; Jacobian JVol; Integration I1; }
If(FLAG_SIGNAL_BC == SIGNAL_Neumann)
      Galerkin{ [ -df_inc[]         , {u_ref} ]; In BndSca; Jacobian JSur; Integration I1; }
EndIf

    For iEdg In {1:4}
      Galerkin { [ -I[]*k[]*ExpPThetaRef2[] * Dof{u_ref} , {u_ref} ]; In EdgRef~{iEdg}; Jacobian JSur; Integration I1; }
    For i In{1:nPadeRef}
      Galerkin { [ -I[]*k[]*ExpPThetaRef2[] * 2./mPadeRef * cPadeRef~{i} * Dof{u_ref~{iEdg}~{i}} , {u_ref} ]; In EdgRef~{iEdg}; Jacobian JSur; Integration I1; }
      Galerkin { [ -I[]*k[]*ExpPThetaRef2[] * 2./mPadeRef * cPadeRef~{i} * Dof{u_ref}            , {u_ref} ]; In EdgRef~{iEdg}; Jacobian JSur; Integration I1; }

      Galerkin { [ Dof{d u_ref~{iEdg}~{i}}                                      , {d u_ref~{iEdg}~{i}} ]; In EdgRef~{iEdg}; Jacobian JSur; Integration I1; }
      Galerkin { [ -k[]^2*(ExpPThetaRef[]*cPadeRef~{i}+1) * Dof{u_ref~{iEdg}~{i}} , {u_ref~{iEdg}~{i}} ]; In EdgRef~{iEdg}; Jacobian JSur; Integration I1; }
      Galerkin { [ -k[]^2*ExpPThetaRef[]*(cPadeRef~{i}+1) * Dof{u_ref}            , {u_ref~{iEdg}~{i}} ]; In EdgRef~{iEdg}; Jacobian JSur; Integration I1; }
    EndFor
    EndFor
    For iCrn In {1:4}
      iEdg1 = iCrn;
      iEdg2 = (iCrn == 4) ? 1 : iCrn+1;
    For i In{1:nPadeRef}
      Galerkin { [ -I[]*k[]*ExpPThetaRef2[] * Dof{u_ref~{iEdg1}~{i}} , {u_ref~{iEdg1}~{i}} ]; In CrnRef~{iCrn}; Jacobian JLin; Integration I1; }
      Galerkin { [ -I[]*k[]*ExpPThetaRef2[] * Dof{u_ref~{iEdg2}~{i}} , {u_ref~{iEdg2}~{i}} ]; In CrnRef~{iCrn}; Jacobian JLin; Integration I1; }
    For j In{1:nPadeRef}
      Galerkin { [ -I[]*k[]*ExpPThetaRef2[] * 2./mPadeRef * cPadeRef~{j} * Dof{u_ref~{iEdg1}~{i}}    , {u_ref~{iEdg1}~{i}} ]; In CrnRef~{iCrn}; Jacobian JLin; Integration I1; }
      Galerkin { [ -I[]*k[]*ExpPThetaRef2[] * 2./mPadeRef * cPadeRef~{j} * Dof{u_ref~{iEdg2}~{i}}    , {u_ref~{iEdg2}~{i}} ]; In CrnRef~{iCrn}; Jacobian JLin; Integration I1; }
      Galerkin { [ -I[]*k[]*ExpPThetaRef2[] * 2./mPadeRef * cPadeRef~{j} * Dof{u_ref~{iCrn}~{i}~{j}} , {u_ref~{iEdg1}~{i}} ]; In CrnRef~{iCrn}; Jacobian JLin; Integration I1; }
      Galerkin { [ -I[]*k[]*ExpPThetaRef2[] * 2./mPadeRef * cPadeRef~{j} * Dof{u_ref~{iCrn}~{j}~{i}} , {u_ref~{iEdg2}~{i}} ]; In CrnRef~{iCrn}; Jacobian JLin; Integration I1; }

      Galerkin { [ (cPadeRef~{i}+cPadeRef~{j}+ExpMThetaRef[]) * Dof{u_ref~{iCrn}~{i}~{j}} , {u_ref~{iCrn}~{i}~{j}} ]; In CrnRef~{iCrn}; Jacobian JLin; Integration I1; }
      Galerkin { [ (cPadeRef~{j}+1) * Dof{u_ref~{iEdg1}~{i}}                              , {u_ref~{iCrn}~{i}~{j}} ]; In CrnRef~{iCrn}; Jacobian JLin; Integration I1; }
      Galerkin { [ (cPadeRef~{i}+1) * Dof{u_ref~{iEdg2}~{j}}                              , {u_ref~{iCrn}~{i}~{j}} ]; In CrnRef~{iCrn}; Jacobian JLin; Integration I1; }
    EndFor
    EndFor
    EndFor

    }
  }
EndIf

  { Name NumSol; Type FemEquation;
    Quantity {
If(FLAG_REF == REF_Num)
      { Name u_ref; Type Local; NameOfSpace H_ref; }
EndIf
      { Name u_num; Type Local; NameOfSpace H_num; }
If(BND_TYPE == BND_PadeCont)
    For i In {1:nPade}
      { Name u~{i}; Type Local; NameOfSpace H~{i}; }
    EndFor
EndIf
If(BND_TYPE == BND_PadeDisc)
    For iEdg In {1:DOM_Num}
    For i In {1:nPade}
      { Name u~{iEdg}~{i}; Type Local; NameOfSpace H~{iEdg}~{i}; }
    EndFor
    EndFor
If(CRN_TYPE == CRN_Compatibility)
    For iCrn In {1:DOM_Num}
    For i In {1:nPade}
    For j In {1:nPade}
      { Name u~{iCrn}~{i}~{j}; Type Local; NameOfSpace H~{iCrn}~{i}~{j}; }
    EndFor
    EndFor
    EndFor
EndIf
EndIf
    }
    Equation {

// Helmholtz
      Galerkin{ [ Dof{d u_num}    , {d u_num} ]; In Dom;    Jacobian JVol; Integration I1; }
      Galerkin{ [ -k[]^2*Dof{u_num} , {u_num} ]; In Dom;    Jacobian JVol; Integration I1; }
If(FLAG_SIGNAL_BC == SIGNAL_Neumann)
      Galerkin{ [ -df_inc[]         , {u_num} ]; In BndSca; Jacobian JSur; Integration I1; }
EndIf

// Sommerfeld ABC
If(BND_TYPE == BND_Sommerfeld)
      Galerkin{ [ -I[]*k[]*Dof{u_num} , {u_num} ]; In EdgAll; Jacobian JSur; Integration I1; }
EndIf

// Second-order ABC
If(BND_TYPE == BND_Second)
      Galerkin { [ - I[]*k[] * Dof{u_num}              , {u_num} ]; In EdgAll; Jacobian JSur; Integration I1; }
      Galerkin { [ - 1/(2*I[]*kEps[]) * Dof{d u_num} , {d u_num} ]; In EdgAll; Jacobian JSur; Integration I1; }
If(CRN_TYPE == CRN_Compatibility)
      Galerkin { [ 3./4. * Dof{u_num}                  , {u_num} ]; In CrnAll; Jacobian JLin; Integration I1; }
EndIf
EndIf

// HABC (continuity at corners)
If(BND_TYPE == BND_PadeCont)
If((CRN_TYPE == CRN_Damping) || (CRN_TYPE == CRN_DampingNum))
      Galerkin { [ alphaBTPade[] * Dof{u_num}         , {u_num} ]; In EdgAll; Jacobian JSur; Integration I1; }
      Galerkin { [ betaBTPade[] * Dof{d u_num}      , {d u_num} ]; In EdgAll; Jacobian JSur; Integration I1; }
EndIf
      Galerkin { [ -I[]*k[]*ExpPTheta2[] * Dof{u_num} , {u_num} ]; In EdgAll; Jacobian JSur; Integration I1; }
    For i In{1:nPade}
      Galerkin { [ -I[]*k[]*ExpPTheta2[] * 2./mPade * cPade~{i} * Dof{u~{i}} , {u_num} ]; In EdgAll; Jacobian JSur; Integration I1; }
      Galerkin { [ -I[]*k[]*ExpPTheta2[] * 2./mPade * cPade~{i} * Dof{u_num} , {u_num} ]; In EdgAll; Jacobian JSur; Integration I1; }

      Galerkin { [ Dof{d u~{i}}                                   , {d u~{i}} ]; In EdgAll; Jacobian JSur; Integration I1; }
      Galerkin { [ -kEps[]^2*(ExpPTheta[]*cPade~{i}+1) * Dof{u~{i}} , {u~{i}} ]; In EdgAll; Jacobian JSur; Integration I1; }
      Galerkin { [ -kEps[]^2*ExpPTheta[]*(cPade~{i}+1) * Dof{u_num} , {u~{i}} ]; In EdgAll; Jacobian JSur; Integration I1; }
    EndFor
EndIf

// HABC (discontinuity at corners)

If(BND_TYPE == BND_PadeDisc)
    For iEdg In {1:DOM_Num}
      Galerkin { [ -I[]*k[]*ExpPTheta2[] * Dof{u_num} , {u_num} ]; In Edg~{iEdg}; Jacobian JSur; Integration I1; }
    For i In{1:nPade}
      Galerkin { [ -I[]*k[]*ExpPTheta2[] * 2./mPade * cPade~{i} * Dof{u~{iEdg}~{i}} , {u_num} ]; In Edg~{iEdg}; Jacobian JSur; Integration I1; }
      Galerkin { [ -I[]*k[]*ExpPTheta2[] * 2./mPade * cPade~{i} * Dof{u_num}        , {u_num} ]; In Edg~{iEdg}; Jacobian JSur; Integration I1; }

      Galerkin { [ Dof{d u~{iEdg}~{i}}                                   , {d u~{iEdg}~{i}} ]; In Edg~{iEdg}; Jacobian JSur; Integration I1; }
      Galerkin { [ -kEps[]^2*(ExpPTheta[]*cPade~{i}+1) * Dof{u~{iEdg}~{i}} , {u~{iEdg}~{i}} ]; In Edg~{iEdg}; Jacobian JSur; Integration I1; }
      Galerkin { [ -kEps[]^2*ExpPTheta[]*(cPade~{i}+1) * Dof{u_num}        , {u~{iEdg}~{i}} ]; In Edg~{iEdg}; Jacobian JSur; Integration I1; }
    EndFor
    EndFor

If(CRN_TYPE == CRN_Sommerfeld)
    For iCrn In {1:DOM_Num}
      iEdg1 = iCrn;
      iEdg2 = (iCrn == DOM_Num) ? 1 : iCrn+1;
    For i In{1:nPade}
      Galerkin { [ -I[]*k[] * Dof{u~{iEdg1}~{i}} , {u~{iEdg1}~{i}} ]; In Crn~{iCrn}; Jacobian JLin; Integration I1; }
      Galerkin { [ -I[]*k[] * Dof{u~{iEdg2}~{i}} , {u~{iEdg2}~{i}} ]; In Crn~{iCrn}; Jacobian JLin; Integration I1; }
    EndFor
    EndFor
EndIf

If(CRN_TYPE == CRN_Compatibility)
    For iCrn In {1:DOM_Num}
      iEdg1 = iCrn;
      iEdg2 = (iCrn == DOM_Num) ? 1 : iCrn+1;
    For i In{1:nPade}
      Galerkin { [ -I[]*k[]*ExpPTheta2[] * Dof{u~{iEdg1}~{i}} , {u~{iEdg1}~{i}} ]; In Crn~{iCrn}; Jacobian JLin; Integration I1; }
      Galerkin { [ -I[]*k[]*ExpPTheta2[] * Dof{u~{iEdg2}~{i}} , {u~{iEdg2}~{i}} ]; In Crn~{iCrn}; Jacobian JLin; Integration I1; }
    For j In{1:nPade}
      Galerkin { [ -I[]*k[]*ExpPTheta2[] * 2./mPade * cPade~{j} * Dof{u~{iEdg1}~{i}}    , {u~{iEdg1}~{i}} ]; In Crn~{iCrn}; Jacobian JLin; Integration I1; }
      Galerkin { [ -I[]*k[]*ExpPTheta2[] * 2./mPade * cPade~{j} * Dof{u~{iEdg2}~{i}}    , {u~{iEdg2}~{i}} ]; In Crn~{iCrn}; Jacobian JLin; Integration I1; }
      Galerkin { [ -I[]*k[]*ExpPTheta2[] * 2./mPade * cPade~{j} * Dof{u~{iCrn}~{i}~{j}} , {u~{iEdg1}~{i}} ]; In Crn~{iCrn}; Jacobian JLin; Integration I1; }
      Galerkin { [ -I[]*k[]*ExpPTheta2[] * 2./mPade * cPade~{j} * Dof{u~{iCrn}~{j}~{i}} , {u~{iEdg2}~{i}} ]; In Crn~{iCrn}; Jacobian JLin; Integration I1; }

      Galerkin { [ (cPade~{i}+cPade~{j}+ExpMTheta[]) * Dof{u~{iCrn}~{i}~{j}} , {u~{iCrn}~{i}~{j}} ]; In Crn~{iCrn}; Jacobian JLin; Integration I1; }
      Galerkin { [ (cPade~{j}+1) * Dof{u~{iEdg1}~{i}}                        , {u~{iCrn}~{i}~{j}} ]; In Crn~{iCrn}; Jacobian JLin; Integration I1; }
      Galerkin { [ (cPade~{i}+1) * Dof{u~{iEdg2}~{j}}                        , {u~{iCrn}~{i}~{j}} ]; In Crn~{iCrn}; Jacobian JLin; Integration I1; }
    EndFor
    EndFor
    EndFor
EndIf
EndIf

    }
  }

  { Name ProjSol; Type FemEquation;
    Quantity {
      { Name u_proj; Type Local; NameOfSpace H_proj; }
    }
    Equation {
      Galerkin{ [ Dof{u_proj} , {u_proj} ]; In Dom; Jacobian JVol; Integration I1; }
      Galerkin{ [ -f_ref[]    , {u_proj} ]; In Dom; Jacobian JVol; Integration I1; }
    }
  }
}

//==================================================================================================
// RESOLUTION
//==================================================================================================

Resolution {
  { Name NumNormal;
    System {
      { Name A; NameOfFormulation NumNormal; Type Real; NameOfMesh "mainCurv.msh"; }
    }
    Operation {
      Generate[A]; Solve[A]; SaveSolution[A];
    }
  }
  { Name NumCur;
    System {
      { Name A; NameOfFormulation NumNormal; Type Real; NameOfMesh "mainCurv.msh"; }
      { Name B; NameOfFormulation NumCur;    Type Real; NameOfMesh "mainCurv.msh"; }
    }
    Operation {
      Generate[A]; Solve[A]; SaveSolution[A]; PostOperation[NumNormal];
      Generate[B]; Solve[B]; SaveSolution[B];
    }
  }
  { Name NumSol;
    System {
If((CRN_TYPE == CRN_DampingNum) || (CRN_TYPE == CRN_DampingNum2))
      { Name A; NameOfFormulation NumNormal; Type Real; NameOfMesh "mainCurv.msh"; }
      { Name B; NameOfFormulation NumCur;    Type Real; NameOfMesh "mainCurv.msh"; }
EndIf
If(FLAG_REF == REF_Num)
      { Name C; NameOfFormulation NumRef; Type Complex; }
EndIf
      { Name D; NameOfFormulation NumSol; Type Complex; }
    }
    Operation {
If((CRN_TYPE == CRN_DampingNum) || (CRN_TYPE == CRN_DampingNum2))
      Generate[A]; Solve[A]; SaveSolution[A]; PostOperation[NumNormal];
      Generate[B]; Solve[B]; SaveSolution[B]; PostOperation[NumCur];
EndIf
If(FLAG_REF == REF_Num)
      Generate[C]; Solve[C]; SaveSolution[C];
EndIf
      Generate[D]; Solve[D]; SaveSolution[D];
    }
  }
  { Name ProjSol;
    System {{ Name A; NameOfFormulation ProjSol; Type Complex; }}
    Operation { Generate[A]; Solve[A]; SaveSolution[A]; }
  }
}

//==================================================================================================
// POSTPRO / POSTOP
//==================================================================================================

PostProcessing {
  { Name NumNormal; NameOfFormulation NumNormal;
    Quantity {
      { Name u_normal; Value { Local { [ Vector[{u_nx},{u_ny},0] / Sqrt[{u_nx}*{u_nx}+{u_ny}*{u_ny}] ]; In Region[{EdgAll}]; Jacobian JSur; }}}
    }
  }
  { Name NumCur; NameOfFormulation NumCur;
    Quantity {
      { Name u_cur; Value { Local { [ {u_cur} ]; In Region[{EdgAll}]; Jacobian JSur; }}}
    }
  }
  { Name NumSol; NameOfFormulation NumSol;
    Quantity {
If(FLAG_REF == REF_Ana)
      { Name u_ref1~{FLAG_SIGNAL_BC}~{BND_TYPE}~{CRN_TYPE}~{nPade}~{thetaPadeInput}~{DOM_Num}; Value { Local { [ f_ref[] ];         In DomRef; Jacobian JVol; }}}
      { Name u_ref2~{FLAG_SIGNAL_BC}~{BND_TYPE}~{CRN_TYPE}~{nPade}~{thetaPadeInput}~{DOM_Num}; Value { Local { [ f_ref[] ];         In Dom; Jacobian JVol; }}}
      { Name u_num~{FLAG_SIGNAL_BC}~{BND_TYPE}~{CRN_TYPE}~{nPade}~{thetaPadeInput}~{DOM_Num};  Value { Local { [ {u_num} ];         In Dom; Jacobian JVol; }}}
      { Name u_err~{FLAG_SIGNAL_BC}~{BND_TYPE}~{CRN_TYPE}~{nPade}~{thetaPadeInput}~{DOM_Num};  Value { Local { [ f_ref[]-{u_num} ]; In Dom; Jacobian JVol; }}}
ElseIf(FLAG_REF == REF_Num)
      { Name u_ref1~{FLAG_SIGNAL_BC}~{BND_TYPE}~{CRN_TYPE}~{nPade}~{thetaPadeInput}~{DOM_Num}; Value { Local { [ {u_ref} ];         In DomRef; Jacobian JVol; }}}
      { Name u_ref2~{FLAG_SIGNAL_BC}~{BND_TYPE}~{CRN_TYPE}~{nPade}~{thetaPadeInput}~{DOM_Num}; Value { Local { [ {u_ref} ];         In Dom; Jacobian JVol; }}}
      { Name u_num~{FLAG_SIGNAL_BC}~{BND_TYPE}~{CRN_TYPE}~{nPade}~{thetaPadeInput}~{DOM_Num};  Value { Local { [ {u_num} ];         In Dom; Jacobian JVol; }}}
      { Name u_err~{FLAG_SIGNAL_BC}~{BND_TYPE}~{CRN_TYPE}~{nPade}~{thetaPadeInput}~{DOM_Num};  Value { Local { [ {u_ref}-{u_num} ]; In Dom; Jacobian JVol; }}}
EndIf
If(FLAG_REF == REF_Ana)
      { Name error2;   Value { Integral { [ Norm[f_ref[]-{u_num}]^2 ]; In Dom; Jacobian JVol; Integration I1; }}}
      { Name energy2;  Value { Integral { [ Norm[f_ref[]]^2 ];         In Dom; Jacobian JVol; Integration I1; }}}
ElseIf(FLAG_REF == REF_Num)
      { Name error2;   Value { Integral { [ Norm[{u_ref}-{u_num}]^2 ]; In Dom; Jacobian JVol; Integration I1; }}}
      { Name energy2;  Value { Integral { [ Norm[{u_ref}]^2 ];         In Dom; Jacobian JVol; Integration I1; }}}
EndIf
      { Name errorAbs; Value { Term { Type Global; [Sqrt[$error2Var]] ;             In Dom; Jacobian JVol; }}}
      { Name errorRel; Value { Term { Type Global; [Sqrt[$error2Var/$energy2Var]] ; In Dom; Jacobian JVol; }}}
    }
  }
  { Name Errors; NameOfFormulation NumSol;
    Quantity {
If(FLAG_REF == REF_Ana)
      { Name error2;   Value { Integral { [ Norm[f_ref[]-{u_num}]^2 ]; In Dom; Jacobian JVol; Integration I1; }}}
      { Name energy2;  Value { Integral { [ Norm[f_ref[]]^2 ];         In Dom; Jacobian JVol; Integration I1; }}}
ElseIf(FLAG_REF == REF_Num)
      { Name error2;   Value { Integral { [ Norm[{u_ref}-{u_num}]^2 ]; In Dom; Jacobian JVol; Integration I1; }}}
      { Name energy2;  Value { Integral { [ Norm[{u_ref}]^2 ];         In Dom; Jacobian JVol; Integration I1; }}}
EndIf
      { Name errorAbs; Value { Term { Type Global; [Sqrt[$error2Var]] ;             In Dom; Jacobian JVol; }}}
      { Name errorRel; Value { Term { Type Global; [Sqrt[$error2Var/$energy2Var]] ; In Dom; Jacobian JVol; }}}
    }
  }
  { Name ProjErrors; NameOfFormulation ProjSol;
    Quantity {
      { Name u_ref ; Value { Local { [  f_ref[] ];         In Dom; Jacobian JVol; }}}
      { Name u_proj; Value { Local { [ {u_proj} ];         In Dom; Jacobian JVol; }}}
      { Name u_diff; Value { Local { [ {u_proj}-f_ref[] ]; In Dom; Jacobian JVol; }}}
      { Name error2;   Value { Integral { [ Norm[f_ref[]-{u_proj}]^2 ];             In Dom; Jacobian JVol; Integration I1; }}}
      { Name energy2;  Value { Integral { [ Norm[f_ref[]]^2 ];                      In Dom; Jacobian JVol; Integration I1; }}}
      { Name errorAbs; Value { Term { Type Global; [Sqrt[$error2Var]] ;             In Dom; Jacobian JVol; }}}
      { Name errorRel; Value { Term { Type Global; [Sqrt[$error2Var/$energy2Var]] ; In Dom; Jacobian JVol; }}}
    }
  }
}

PostOperation{
  { Name NumNormal; NameOfPostProcessing NumNormal;
    Operation {
      Print [u_normal, OnElementsOf Region[{EdgAll}], StoreInField (1001)];
    }
  }
  { Name NumCur; NameOfPostProcessing NumCur;
    Operation {
      tmp0 = Sprintf("out/numCur_%g_%g_%g_%g_%g.pos", FLAG_SIGNAL_BC, BND_TYPE, CRN_TYPE, nPade, thetaPadeInput);
      Print [u_cur, OnElementsOf Region[{EdgAll}], StoreInField (1002), File tmp0];
    }
  }
  { Name NumSol; NameOfPostProcessing NumSol;
    Operation {
      tmp1 = Sprintf("out/solRef1_%g_%g_%g_%g_%g_%g.pos", FLAG_SIGNAL_BC, BND_TYPE, CRN_TYPE, nPade, thetaPadeInput, DOM_Num);
      tmp2 = Sprintf("out/solRef2_%g_%g_%g_%g_%g_%g.pos", FLAG_SIGNAL_BC, BND_TYPE, CRN_TYPE, nPade, thetaPadeInput, DOM_Num);
      tmp3 = Sprintf("out/solNum_%g_%g_%g_%g_%g_%g.pos", FLAG_SIGNAL_BC, BND_TYPE, CRN_TYPE, nPade, thetaPadeInput, DOM_Num);
      tmp4 = Sprintf("out/solErr_%g_%g_%g_%g_%g_%g.pos", FLAG_SIGNAL_BC, BND_TYPE, CRN_TYPE, nPade, thetaPadeInput, DOM_Num);
      Print [u_ref1~{FLAG_SIGNAL_BC}~{BND_TYPE}~{CRN_TYPE}~{nPade}~{thetaPadeInput}~{DOM_Num}, OnElementsOf DomRef, File tmp1];
      Print [u_ref2~{FLAG_SIGNAL_BC}~{BND_TYPE}~{CRN_TYPE}~{nPade}~{thetaPadeInput}~{DOM_Num}, OnElementsOf Dom, File tmp2];
      Print [u_num~{FLAG_SIGNAL_BC}~{BND_TYPE}~{CRN_TYPE}~{nPade}~{thetaPadeInput}~{DOM_Num}, OnElementsOf Dom, File tmp3];
      Print [u_err~{FLAG_SIGNAL_BC}~{BND_TYPE}~{CRN_TYPE}~{nPade}~{thetaPadeInput}~{DOM_Num}, OnElementsOf Dom, File tmp4];
    }
  }
  { Name Errors; NameOfPostProcessing Errors;
    Operation {
      tmp5 = Sprintf("out/errorAbs_poly_%g_%g_%g_%g_%g.dat", FLAG_SIGNAL_BC, BND_TYPE, CRN_TYPE, nPade, thetaPadeInput);
      tmp6 = Sprintf("out/errorRel_poly_%g_%g_%g_%g_%g.dat", FLAG_SIGNAL_BC, BND_TYPE, CRN_TYPE, nPade, thetaPadeInput);
      Print [error2[Dom],  OnRegion Dom, Format Table, StoreInVariable $error2Var];
      Print [energy2[Dom], OnRegion Dom, Format Table, StoreInVariable $energy2Var];
      Print [errorAbs,     OnRegion Dom, Format Table, SendToServer "Output/1L2-Error (absolute)", File > tmp5];
      Print [errorRel,     OnRegion Dom, Format Table, SendToServer "Output/2L2-Error (relative)", File > tmp6];
    }
  }
  { Name ProjErrors; NameOfPostProcessing ProjErrors;
    Operation {
      tmpA = Sprintf("out/solRef.pos");
      tmpB = Sprintf("out/solProj.pos");
      tmpC = Sprintf("out/solDiff.pos");
      tmp1 = Sprintf("out/errorAbs_%g.dat", FLAG_SIGNAL_BC);
      tmp2 = Sprintf("out/errorRel_%g.dat", FLAG_SIGNAL_BC);
      Print [u_ref , OnElementsOf Dom, File tmpA];
      Print [u_proj, OnElementsOf Dom, File tmpB];
      Print [u_diff, OnElementsOf Dom, File tmpC];
      Print [error2[Dom],  OnRegion Dom, Format Table, SendToServer "Output/2Error2 (absolute)", StoreInVariable $error2Var];
      Print [energy2[Dom], OnRegion Dom, Format Table, SendToServer "Output/3Energy2 (absolute)", StoreInVariable $energy2Var];
      Print [errorAbs,     OnRegion Dom, Format Table, SendToServer "Output/1L2-Error (absolute)", File > tmp1];
      Print [errorRel,     OnRegion Dom, Format Table, SendToServer "Output/2L2-Error (relative)", File > tmp2];
    }
  }
}

DefineConstant[
  R_ = {"NumSol", Name "GetDP/1ResolutionChoices",    Visible 1, Choices {"NumNormal", "NumCur", "NumSol", "ProjSol"}},
  P_ = {"NumSol, Errors", Name "GetDP/2PostOperationChoices", Visible 1, Choices {"NumSol", "Errors", "ProjErrors"}}
];
