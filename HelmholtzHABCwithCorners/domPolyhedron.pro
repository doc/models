Include "domPolyhedron.dat";

Function {
If((FLAG_DIM == 3) && (FLAG_SIGNAL == SIGNAL_Scatt))
If((FAC_Num == 4) || (FAC_Num == 12) || (FAC_Num == 20) || (FAC_Num == 1))
  incDir = {1., 0., 0.};
Else
  incDir = {1./Sqrt[2.], 1./Sqrt[2.], 0.};
EndIf
  incPha[] = k[] * (incDir(0)*X[] + incDir(1)*Y[] + incDir(2)*Z[]);
If(FLAG_SIGNAL_BC == SIGNAL_Dirichlet)
  f_inc2[] = Complex[Cos[incPha[]], Sin[incPha[]]];
  f_ref2[] = AcousticFieldSoftSphere[XYZ[]]{WAVENUMBER, R_SCA, incDir(0), incDir(1), incDir(2)};
EndIf
If(FLAG_SIGNAL_BC == SIGNAL_Neumann)
  df_inc2[] = Complex[0,1] * incPha[]/R[] * Complex[Cos[incPha[]], Sin[incPha[]]];
  f_ref2[] = AcousticFieldHardSphere[XYZ[]]{WAVENUMBER, R_SCA, incDir(0), incDir(1), incDir(2)};
EndIf
EndIf
}

//==================================================================================================
// OPTIONS and PARAMETERS
//==================================================================================================

BND_Neumann            = 0;
BND_Sommerfeld         = 1;
BND_PadeCont           = 3;
BND_PadeDisc           = 4;
CRN_Nothing            = 0;
CRN_Curvature          = 2;
CRN_Curvature2         = 3;
CRN_SommerfeldOnEdge   = 4;
CRN_ApproxCompOnEdge   = 5;
CRN_SommerfeldAtCorner = 6;
CRN_ApproxCompAtCorner = 7;

DefineConstant[
  BND_TYPE = {BND_PadeDisc,
    Name "Input/5Model/03Boundary condition (faces)", Highlight "Blue",
    Choices {BND_Neumann    = "Homogeneous Neumann",
             BND_Sommerfeld = "Sommerfeld ABC",
             BND_PadeDisc   = "Pade ABC (disc at corners)",
             BND_PadeCont   = "Pade ABC (cont at corners)"}},
  CRN_TYPE = {CRN_ApproxCompAtCorner,
    Name "Input/5Model/04Boundary condition (edges-corners)", Highlight "Red",
    Visible ((BND_TYPE == BND_PadeDisc) || (BND_TYPE == BND_PadeCont)),
    Choices {CRN_Nothing            = "Nothing",
             CRN_Curvature          = "Curvature (kEps)",
             CRN_Curvature2         = "Curvature (kEps+AddTerms)",
             CRN_SommerfeldOnEdge   = "Sommerfeld on Edges",
             CRN_ApproxCompOnEdge   = "2D-compatibility on Edges",
             CRN_SommerfeldAtCorner = "Sommerfeld at Corners",
             CRN_ApproxCompAtCorner = "3D-compatibility at Corners"}}
];

DefineConstant[
  nPade = {1, Choices {1, 2, 3, 4},
    Name "Input/5Model/05Pade: Number of fields",
    Visible ((BND_TYPE == BND_PadeDisc) || (BND_TYPE == BND_PadeCont))},
  thetaPadeInput = {0, Min 0, Step 1, Max 4,
    Name "Input/5Model/06Pade: Rotation of branch cut",
    Visible ((BND_TYPE == BND_PadeDisc) || (BND_TYPE == BND_PadeCont))}
];

If((BND_TYPE == BND_PadeDisc) || (BND_TYPE == BND_PadeCont))
  If(thetaPadeInput == 0)
    thetaPade = 0;
  EndIf
  If(thetaPadeInput == 1)
    thetaPade = Pi/8;
  EndIf
  If(thetaPadeInput == 2)
    thetaPade = Pi/4;
  EndIf
  If(thetaPadeInput == 3)
    thetaPade = Pi/3;
  EndIf
  If(thetaPadeInput == 4)
    thetaPade = Pi/2;
  EndIf
  mPade = 2*nPade+1;
  For j In{1:nPade}
    cPade~{j} = Tan[j*Pi/mPade]^2;
  EndFor
Else
  nPade = 0;
  thetaPadeInput = 0;
EndIf

Group {
  Dom = Region[{VOL}];
  BndSca = Region[{SUR_Scatt}];
  BndExt = {};
  For iFac In {1:FAC_Num}
    BndExt        += Region[{SUR~{iFac}}];
    Face~{iFac}    = Region[{SUR~{iFac}}];
    BndFace~{iFac} = {};
  EndFor
  For iEdg In {1:EDG_Num}
    Edg~{iEdg} = Region[{EDG~{iEdg}}];
    BndEdg~{iEdg} = {};
    For iNeigh In {1:EDG_NumFacPerEdg}
      iFac = EdgNeigh~{iEdg}~{iNeigh};
      BndFace~{iFac} += Region[{EDG~{iEdg}}];
    EndFor
  EndFor
  For iCrn In {1:CRN_Num}
    Crn~{iCrn} = Region[{CRN~{iCrn}}];
    For iNeigh In {1:CRN_NumEdgPerCrn}
      iEdg = CrnNeigh~{iCrn}~{iNeigh};
      BndEdg~{iEdg} += Region[{CRN~{iCrn}}];
    EndFor
  EndFor
  DomAll = Region[{Dom,BndSca,BndExt}];
}

Function {
  NormalNum[]  = VectorField[XYZ[]]{1001};
  CurvNum[]    = ScalarField[XYZ[]]{1002};

If((CRN_TYPE == CRN_Curvature) || (CRN_TYPE == CRN_Curvature2))
If(FAC_Num == 1)
  Curv[BndExt] = 1/dimRmean;
Else
  Curv[BndExt] = CurvNum[];
EndIf
Else
  Curv[BndExt] = 0.;
EndIf

  kEps[BndExt] = WAVENUMBER + I[] * 0.4 * WAVENUMBER^(1/3) * (Curv[])^(2/3);

If((BND_TYPE == BND_PadeDisc) || (BND_TYPE == BND_PadeCont))
  ExpPTheta[]  = Complex[Cos[ thetaPade],Sin[ thetaPade]];
  ExpMTheta[]  = Complex[Cos[-thetaPade],Sin[-thetaPade]];
  ExpPTheta2[] = Complex[Cos[thetaPade/2.],Sin[thetaPade/2.]];
EndIf
}

//==================================================================================================
// FONCTION SPACES with CONSTRAINTS
//==================================================================================================

If(FLAG_SIGNAL_BC == SIGNAL_Dirichlet)
Constraint {
  { Name DirichletBC; Case {{ Region BndSca; Value -f_inc2[]; }}}
}
EndIf

FunctionSpace {
  { Name H_bndInt; Type Form0; BasisFunction {{ Name si; NameOfCoef pi; Function BF_Node; Support Region[{BndExt}]; Entity NodesOf[All]; }}}
  { Name H_bndExt; Type Form0; BasisFunction {{ Name si; NameOfCoef pi; Function BF_Node; Support Region[{BndSca}]; Entity NodesOf[All]; }}}
  { Name H_nx;   Type Form0; BasisFunction {{ Name si; NameOfCoef pi; Function BF_Node; Support Region[{BndExt}]; Entity NodesOf[All]; }}}
  { Name H_ny;   Type Form0; BasisFunction {{ Name si; NameOfCoef pi; Function BF_Node; Support Region[{BndExt}]; Entity NodesOf[All]; }}}
  { Name H_nz;   Type Form0; BasisFunction {{ Name si; NameOfCoef pi; Function BF_Node; Support Region[{BndExt}]; Entity NodesOf[All]; }}}
  { Name H_cur;  Type Form0; BasisFunction {{ Name si; NameOfCoef pi; Function BF_Node; Support Region[{BndExt}]; Entity NodesOf[All]; }}}
  { Name H_proj; Type Form0; BasisFunction {{ Name si; NameOfCoef pi; Function BF_Node; Support Region[{Dom}];    Entity NodesOf[All]; }}}
  { Name H_num;  Type Form0; BasisFunction {{ Name sn; NameOfCoef pn; Function BF_Node; Support Region[{DomAll}]; Entity NodesOf[All]; }}
If(FLAG_SIGNAL_BC == SIGNAL_Dirichlet)
    Constraint {{ NameOfCoef pn; EntityType NodesOf; NameOfConstraint DirichletBC; }}
EndIf
  }
If(BND_TYPE == BND_PadeCont)
  For m In {1:nPade}
    { Name H~{m}; Type Form0; BasisFunction {{ Name sn; NameOfCoef un; Function BF_Node; Support Region[{BndExt}]; Entity NodesOf[All]; }}}
  EndFor
EndIf
If(BND_TYPE == BND_PadeDisc)
  For iFac In {1:FAC_Num}
  For m In {1:nPade}
    { Name H~{iFac}~{m}; Type Form0; BasisFunction {{ Name sn; NameOfCoef un; Function BF_Node; Support Region[{Face~{iFac},BndFace~{iFac}}]; Entity NodesOf[All]; }}}
  EndFor
  EndFor
If((CRN_TYPE == CRN_ApproxCompOnEdge) || (CRN_TYPE == CRN_SommerfeldAtCorner) || (CRN_TYPE == CRN_ApproxCompAtCorner))
  For iEdg In {1:EDG_Num}
  For m In {1:nPade}
  For n In {1:nPade}
    { Name H~{iEdg}~{m}~{n}; Type Form0; BasisFunction {{ Name sn; NameOfCoef un; Function BF_Node; Support Region[{Edg~{iEdg},BndEdg~{iEdg}}]; Entity NodesOf[All]; }}}
  EndFor
  EndFor
  EndFor
If(CRN_TYPE == CRN_ApproxCompAtCorner)
  For iCrn In {1:CRN_Num}
  For m In {1:nPade}
  For n In {1:nPade}
  For o In {1:nPade}
    { Name H~{iCrn}~{m}~{n}~{o}; Type Form0; BasisFunction {{ Name sn; NameOfCoef un; Function BF_Node; Support Region[{Crn~{iCrn}}]; Entity NodesOf[All]; }}}
  EndFor
  EndFor
  EndFor
  EndFor
EndIf
EndIf
EndIf
}

//==================================================================================================
// FORMULATIONS
//==================================================================================================

Formulation {
  { Name NumNormal; Type FemEquation;
    Quantity {
      { Name u_nx; Type Local; NameOfSpace H_nx; }
      { Name u_ny; Type Local; NameOfSpace H_ny; }
      { Name u_nz; Type Local; NameOfSpace H_nz; }
    }
    Equation {
      Galerkin{ [ Dof{u_nx}        , {u_nx} ]; In Region[{BndExt}]; Jacobian JSur; Integration I1; }
      Galerkin{ [ Dof{u_ny}        , {u_ny} ]; In Region[{BndExt}]; Jacobian JSur; Integration I1; }
      Galerkin{ [ Dof{u_nz}        , {u_nz} ]; In Region[{BndExt}]; Jacobian JSur; Integration I1; }
      Galerkin{ [ -CompX[Normal[]] , {u_nx} ]; In Region[{BndExt}]; Jacobian JSur; Integration I1; }
      Galerkin{ [ -CompY[Normal[]] , {u_ny} ]; In Region[{BndExt}]; Jacobian JSur; Integration I1; }
      Galerkin{ [ -CompZ[Normal[]] , {u_nz} ]; In Region[{BndExt}]; Jacobian JSur; Integration I1; }
    }
  }

  { Name NumCur; Type FemEquation;
    Quantity {
      { Name u_nx;  Type Local; NameOfSpace H_nx; }
      { Name u_ny;  Type Local; NameOfSpace H_ny; }
      { Name u_nz;  Type Local; NameOfSpace H_nz; }
      { Name u_cur; Type Local; NameOfSpace H_cur; }
    }
    Equation {
      Galerkin{ [ Dof{u_nx}           , {u_nx} ]; In Region[{BndExt}]; Jacobian JSur; Integration I1; }
      Galerkin{ [ Dof{u_ny}           , {u_ny} ]; In Region[{BndExt}]; Jacobian JSur; Integration I1; }
      Galerkin{ [ Dof{u_nz}           , {u_nz} ]; In Region[{BndExt}]; Jacobian JSur; Integration I1; }
      Galerkin{ [ -CompX[NormalNum[]] , {u_nx} ]; In Region[{BndExt}]; Jacobian JSur; Integration I1; }
      Galerkin{ [ -CompY[NormalNum[]] , {u_ny} ]; In Region[{BndExt}]; Jacobian JSur; Integration I1; }
      Galerkin{ [ -CompZ[NormalNum[]] , {u_nz} ]; In Region[{BndExt}]; Jacobian JSur; Integration I1; }

      Galerkin{ [ Dof{u_cur}                     , {u_cur} ]; In Region[{BndExt}]; Jacobian JSur; Integration I1; }
      Galerkin{ [ -Vector[0.5,0,0] * Dof{d u_nx} , {u_cur} ]; In Region[{BndExt}]; Jacobian JSur; Integration I1; }
      Galerkin{ [ -Vector[0,0.5,0] * Dof{d u_ny} , {u_cur} ]; In Region[{BndExt}]; Jacobian JSur; Integration I1; }
      Galerkin{ [ -Vector[0,0,0.5] * Dof{d u_nz} , {u_cur} ]; In Region[{BndExt}]; Jacobian JSur; Integration I1; }
    }
  }

  { Name NumSol; Type FemEquation;
    Quantity {
      { Name u_num; Type Local; NameOfSpace H_num; }
If(BND_TYPE == BND_PadeCont)
    For m In {1:nPade}
      { Name u~{m}; Type Local; NameOfSpace H~{m}; }
    EndFor
EndIf
If(BND_TYPE == BND_PadeDisc)
    For iFac In {1:FAC_Num}
    For m In {1:nPade}
      { Name u~{iFac}~{m}; Type Local; NameOfSpace H~{iFac}~{m}; }
    EndFor
    EndFor
If((CRN_TYPE == CRN_ApproxCompOnEdge) || (CRN_TYPE == CRN_SommerfeldAtCorner) || (CRN_TYPE == CRN_ApproxCompAtCorner))
    For iEdg In {1:EDG_Num}
    For m In {1:nPade}
    For n In {1:nPade}
      { Name u~{iEdg}~{m}~{n}; Type Local; NameOfSpace H~{iEdg}~{m}~{n}; }
    EndFor
    EndFor
    EndFor
If(CRN_TYPE == CRN_ApproxCompAtCorner)
    For iCrn In {1:CRN_Num}
    For m In {1:nPade}
    For n In {1:nPade}
    For o In {1:nPade}
      { Name u~{iCrn}~{m}~{n}~{o}; Type Local; NameOfSpace H~{iCrn}~{m}~{n}~{o}; }
    EndFor
    EndFor
    EndFor
    EndFor
EndIf
EndIf
EndIf
    }
    Equation {

// Helmholtz
      Galerkin{ [ Dof{d u_num}    , {d u_num} ]; In Dom; Jacobian JVol; Integration I1; }
      Galerkin{ [ -k[]^2*Dof{u_num} , {u_num} ]; In Dom; Jacobian JVol; Integration I1; }
If(FLAG_SIGNAL_BC == SIGNAL_Neumann)
      Galerkin{ [ -df_inc2[]         , {u_num} ]; In BndSca; Jacobian JSur; Integration I1; }
EndIf

// Sommerfeld ABC
If(BND_TYPE == BND_Sommerfeld)
      Galerkin{ [ -I[]*k[] * Dof{u_num} , {u_num} ]; In BndExt; Jacobian JSur; Integration I1; }
EndIf

// HABC (continuity at corners)
If(BND_TYPE == BND_PadeCont)
      Galerkin { [ -I[]*k[]*ExpPTheta2[] * Dof{u_num}                           , {u_num} ]; In BndExt; Jacobian JSur; Integration I1; }
    If(CRN_TYPE == CRN_Curvature2)
      Galerkin { [ Curv[] * Dof{u_num}                                          , {u_num} ]; In BndExt; Jacobian JSur; Integration I1; }
      Galerkin { [ -Curv[]/(2*k[]*k[]) * Dof{d u_num}                         , {d u_num} ]; In BndExt; Jacobian JSur; Integration I1; }
    EndIf
    For m In {1:nPade}
      Galerkin { [ -I[]*k[]*ExpPTheta2[] * 2./mPade * cPade~{m} * Dof{u~{m}}    , {u_num} ]; In BndExt; Jacobian JSur; Integration I1; }
      Galerkin { [ -I[]*k[]*ExpPTheta2[] * 2./mPade * cPade~{m} * Dof{u_num}    , {u_num} ]; In BndExt; Jacobian JSur; Integration I1; }

      Galerkin { [ Dof{d u~{m}}                                               , {d u~{m}} ]; In BndExt; Jacobian JSur; Integration I1; }
      Galerkin { [ -kEps[]^2*ExpPTheta[]*(cPade~{m}+ExpMTheta[]) * Dof{u~{m}}   , {u~{m}} ]; In BndExt; Jacobian JSur; Integration I1; }
      Galerkin { [ -kEps[]^2*ExpPTheta[]*(cPade~{m}+1) * Dof{u_num}             , {u~{m}} ]; In BndExt; Jacobian JSur; Integration I1; }
    EndFor
EndIf // End HABC Cont

// HABC (discontinuity at corners)
If(BND_TYPE == BND_PadeDisc)
    For iFac In {1:FAC_Num}
      Galerkin { [ -I[]*k[]*ExpPTheta2[] * Dof{u_num}                                      , {u_num} ]; In Face~{iFac}; Jacobian JSur; Integration I1; }
    For m In {1:nPade}
      Galerkin { [ -I[]*k[]*ExpPTheta2[] * 2./mPade * cPade~{m} * Dof{u~{iFac}~{m}}        , {u_num} ]; In Face~{iFac}; Jacobian JSur; Integration I1; }
      Galerkin { [ -I[]*k[]*ExpPTheta2[] * 2./mPade * cPade~{m} * Dof{u_num}               , {u_num} ]; In Face~{iFac}; Jacobian JSur; Integration I1; }

      Galerkin { [ Dof{d u~{iFac}~{m}}                                            , {d u~{iFac}~{m}} ]; In Face~{iFac}; Jacobian JSur; Integration I1; }
      Galerkin { [ -k[]^2*ExpPTheta[]*(cPade~{m}+ExpMTheta[]) * Dof{u~{iFac}~{m}}   , {u~{iFac}~{m}} ]; In Face~{iFac}; Jacobian JSur; Integration I1; }
      Galerkin { [ -k[]^2*ExpPTheta[]*(cPade~{m}+1) * Dof{u_num}                    , {u~{iFac}~{m}} ]; In Face~{iFac}; Jacobian JSur; Integration I1; }

    If(CRN_TYPE == CRN_SommerfeldOnEdge)
      Galerkin { [ -I[]*k[] * Dof{u~{iFac}~{m}}                                  , {u~{iFac}~{m}} ]; In BndFace~{iFac}; Jacobian JLin; Integration I1; }
    EndIf
    EndFor
    EndFor

If((CRN_TYPE == CRN_ApproxCompOnEdge) || (CRN_TYPE == CRN_SommerfeldAtCorner) || (CRN_TYPE == CRN_ApproxCompAtCorner)) /////
    For iEdg In {1:EDG_Num}
      iFace1 = EdgNeigh~{iEdg}~{1};
      iFace2 = EdgNeigh~{iEdg}~{2};
    For m In{1:nPade}
      Galerkin { [ -I[]*k[]*ExpPTheta2[] * Dof{u~{iFace1}~{m}} , {u~{iFace1}~{m}} ]; In Edg~{iEdg}; Jacobian JLin; Integration I1; }
      Galerkin { [ -I[]*k[]*ExpPTheta2[] * Dof{u~{iFace2}~{m}} , {u~{iFace2}~{m}} ]; In Edg~{iEdg}; Jacobian JLin; Integration I1; }

    For n In{1:nPade}
      Galerkin { [ -I[]*k[]*ExpPTheta2[] * 2./mPade * cPade~{n} * Dof{u~{iFace1}~{m}}   , {u~{iFace1}~{m}} ]; In Edg~{iEdg}; Jacobian JLin; Integration I1; }
      Galerkin { [ -I[]*k[]*ExpPTheta2[] * 2./mPade * cPade~{n} * Dof{u~{iFace2}~{m}}   , {u~{iFace2}~{m}} ]; In Edg~{iEdg}; Jacobian JLin; Integration I1; }
      Galerkin { [ -I[]*k[]*ExpPTheta2[] * 2./mPade * cPade~{n} * Dof{u~{iEdg}~{m}~{n}} , {u~{iFace1}~{m}} ]; In Edg~{iEdg}; Jacobian JLin; Integration I1; }
      Galerkin { [ -I[]*k[]*ExpPTheta2[] * 2./mPade * cPade~{n} * Dof{u~{iEdg}~{n}~{m}} , {u~{iFace2}~{m}} ]; In Edg~{iEdg}; Jacobian JLin; Integration I1; }

    If((CRN_TYPE == CRN_SommerfeldAtCorner) || (CRN_TYPE == CRN_ApproxCompAtCorner))
      Galerkin { [ Dof{d u~{iEdg}~{m}~{n}}                                                    , {d u~{iEdg}~{m}~{n}} ]; In Edg~{iEdg}; Jacobian JLin; Integration I1; }
    EndIf
      Galerkin { [ -k[]^2*ExpPTheta[]*(cPade~{m}+cPade~{n}+ExpMTheta[]) * Dof{u~{iEdg}~{m}~{n}} , {u~{iEdg}~{m}~{n}} ]; In Edg~{iEdg}; Jacobian JLin; Integration I1; }
      Galerkin { [ -k[]^2*ExpPTheta[]*(cPade~{n}+1) * Dof{u~{iFace1}~{m}}                       , {u~{iEdg}~{m}~{n}} ]; In Edg~{iEdg}; Jacobian JLin; Integration I1; }
      Galerkin { [ -k[]^2*ExpPTheta[]*(cPade~{m}+1) * Dof{u~{iFace2}~{n}}                       , {u~{iEdg}~{m}~{n}} ]; In Edg~{iEdg}; Jacobian JLin; Integration I1; }

    If(CRN_TYPE == CRN_SommerfeldAtCorner)
      Galerkin { [ -I[]*k[] * Dof{u~{iEdg}~{m}~{n}}                                          , {u~{iEdg}~{m}~{n}} ]; In BndEdg~{iEdg}; Jacobian JVol; Integration I1; }
    EndIf
    EndFor
    EndFor
    EndFor
EndIf

If(CRN_TYPE == CRN_ApproxCompAtCorner)
    For iCrn In {1:CRN_Num}
      iEdg1 = CrnNeigh~{iCrn}~{1}; // 1-2  m-n
      iEdg2 = CrnNeigh~{iCrn}~{2}; // 1-3  m-o
      iEdg3 = CrnNeigh~{iCrn}~{3}; // 2-3  n-o
    For m In {1:nPade}
    For n In {1:nPade}
      Galerkin { [ -I[]*k[]*ExpPTheta2[] * Dof{u~{iEdg1}~{m}~{n}} , {u~{iEdg1}~{m}~{n}} ]; In Crn~{iCrn}; Jacobian JVol; Integration I1; }
      Galerkin { [ -I[]*k[]*ExpPTheta2[] * Dof{u~{iEdg2}~{m}~{n}} , {u~{iEdg2}~{m}~{n}} ]; In Crn~{iCrn}; Jacobian JVol; Integration I1; }
      Galerkin { [ -I[]*k[]*ExpPTheta2[] * Dof{u~{iEdg3}~{m}~{n}} , {u~{iEdg3}~{m}~{n}} ]; In Crn~{iCrn}; Jacobian JVol; Integration I1; }
      
    For o In {1:nPade}
      Galerkin { [ -I[]*k[]*ExpPTheta2[] * 2./mPade * cPade~{o} * Dof{u~{iCrn}~{m}~{n}~{o}} , {u~{iEdg1}~{m}~{n}} ]; In Crn~{iCrn}; Jacobian JVol; Integration I1; }
      Galerkin { [ -I[]*k[]*ExpPTheta2[] * 2./mPade * cPade~{o} * Dof{u~{iEdg1}~{m}~{n}}    , {u~{iEdg1}~{m}~{n}} ]; In Crn~{iCrn}; Jacobian JVol; Integration I1; }

      Galerkin { [ -I[]*k[]*ExpPTheta2[] * 2./mPade * cPade~{n} * Dof{u~{iCrn}~{m}~{n}~{o}} , {u~{iEdg2}~{m}~{o}} ]; In Crn~{iCrn}; Jacobian JVol; Integration I1; }
      Galerkin { [ -I[]*k[]*ExpPTheta2[] * 2./mPade * cPade~{n} * Dof{u~{iEdg2}~{m}~{o}}    , {u~{iEdg2}~{m}~{o}} ]; In Crn~{iCrn}; Jacobian JVol; Integration I1; }

      Galerkin { [ -I[]*k[]*ExpPTheta2[] * 2./mPade * cPade~{m} * Dof{u~{iCrn}~{m}~{n}~{o}} , {u~{iEdg3}~{n}~{o}} ]; In Crn~{iCrn}; Jacobian JVol; Integration I1; }
      Galerkin { [ -I[]*k[]*ExpPTheta2[] * 2./mPade * cPade~{m} * Dof{u~{iEdg3}~{n}~{o}}    , {u~{iEdg3}~{n}~{o}} ]; In Crn~{iCrn}; Jacobian JVol; Integration I1; }

      Galerkin { [ (cPade~{m}+cPade~{n}+cPade~{o}+ExpMTheta[]) * Dof{u~{iCrn}~{m}~{n}~{o}} , {u~{iCrn}~{m}~{n}~{o}} ]; In Crn~{iCrn}; Jacobian JVol; Integration I1; }
      Galerkin { [ (cPade~{o}+1)                               * Dof{u~{iEdg1}~{m}~{n}}    , {u~{iCrn}~{m}~{n}~{o}} ]; In Crn~{iCrn}; Jacobian JVol; Integration I1; }
      Galerkin { [ (cPade~{n}+1)                               * Dof{u~{iEdg2}~{m}~{o}}    , {u~{iCrn}~{m}~{n}~{o}} ]; In Crn~{iCrn}; Jacobian JVol; Integration I1; }
      Galerkin { [ (cPade~{m}+1)                               * Dof{u~{iEdg3}~{n}~{o}}    , {u~{iCrn}~{m}~{n}~{o}} ]; In Crn~{iCrn}; Jacobian JVol; Integration I1; }
    EndFor
    EndFor
    EndFor
    EndFor
EndIf

EndIf // End HABC Cont

    }
  }

  { Name ProjSol; Type FemEquation;
    Quantity {
      { Name u_proj; Type Local; NameOfSpace H_proj; }
    }
    Equation {
      Galerkin{ [ Dof{u_proj} , {u_proj} ]; In Dom; Jacobian JVol; Integration I1; }
      Galerkin{ [ -f_ref2[]    , {u_proj} ]; In Dom; Jacobian JVol; Integration I1; }
    }
  }

  { Name NumBnd; Type FemEquation;
    Quantity {
      { Name u_int; Type Local; NameOfSpace H_bndInt; }
      { Name u_ext; Type Local; NameOfSpace H_bndExt; }
    }
    Equation {
      Galerkin{ [ Dof{u_int} , {u_int} ]; In Region[{BndSca}]; Jacobian JSur; Integration I1; }
      Galerkin{ [ Dof{u_ext} , {u_ext} ]; In Region[{BndExt}]; Jacobian JSur; Integration I1; }
      Galerkin{ [ -1 , {u_int} ]; In Region[{BndSca}]; Jacobian JSur; Integration I1; }
      Galerkin{ [ -1 , {u_ext} ]; In Region[{BndExt}]; Jacobian JSur; Integration I1; }
    }
  }
}

//==================================================================================================
// RESOLUTION
//==================================================================================================

Resolution {
  { Name NumBnd;
    System {{ Name A; NameOfFormulation NumBnd; Type Real; }}
    Operation { Generate[A]; Solve[A]; SaveSolution[A]; }
  }
  { Name NumNormal;
    System {
      { Name A; NameOfFormulation NumNormal; Type Real; }
    }
    Operation {
      Generate[A]; Solve[A]; SaveSolution[A];
    }
  }
  { Name NumCur;
    System {
      { Name A; NameOfFormulation NumNormal; Type Real; }
      { Name B; NameOfFormulation NumCur;    Type Real; }
    }
    Operation {
      Generate[A]; Solve[A]; SaveSolution[A]; PostOperation[NumNormal];
      Generate[B]; Solve[B]; SaveSolution[B];
    }
  }
  { Name NumSol;
    System {
If(((CRN_TYPE == CRN_Curvature) || (CRN_TYPE == CRN_Curvature2)) && (FAC_Num > 1))
      { Name A; NameOfFormulation NumNormal; Type Real; NameOfMesh "mainCurv.msh"; }
      { Name B; NameOfFormulation NumCur;    Type Real; NameOfMesh "mainCurv.msh"; }
EndIf
      { Name C; NameOfFormulation NumSol; Type Complex; }
    }
    Operation {
If(((CRN_TYPE == CRN_Curvature) || (CRN_TYPE == CRN_Curvature2)) && (FAC_Num > 1))
      Generate[A]; Solve[A]; SaveSolution[A]; PostOperation[NumNormal];
      Generate[B]; Solve[B]; SaveSolution[B]; PostOperation[NumCur];
EndIf
      Generate[C]; Solve[C]; SaveSolution[C];
    }
  }
  { Name ProjSol;
    System {{ Name A; NameOfFormulation ProjSol; Type Complex; }}
    Operation { Generate[A]; Solve[A]; SaveSolution[A]; }
  }
}

//==================================================================================================
// POSTPRO / POSTOP
//==================================================================================================

PostProcessing {
  { Name NumNormal; NameOfFormulation NumNormal;
    Quantity {
      { Name u_normal; Value { Local { [ Vector[{u_nx},{u_ny},{u_nz}] / Sqrt[{u_nx}*{u_nx}+{u_ny}*{u_ny}+{u_nz}*{u_nz}] ]; In Region[{BndExt}]; Jacobian JSur; }}}
    }
  }
  { Name NumCur; NameOfFormulation NumCur;
    Quantity {
      { Name u_cur; Value { Local { [ {u_cur} ]; In Region[{BndExt}]; Jacobian JSur; }}}
    }
  }
  { Name NumSol; NameOfFormulation NumSol;
    Quantity {
      { Name u_ref~{BND_TYPE}~{CRN_TYPE}~{nPade}~{thetaPadeInput}; Value { Local { [ f_ref2[] ];        In Dom; Jacobian JVol; }}}
      { Name u_num~{BND_TYPE}~{CRN_TYPE}~{nPade}~{thetaPadeInput}; Value { Local { [ {u_num} ];        In Dom; Jacobian JVol; }}}
      { Name u_err~{BND_TYPE}~{CRN_TYPE}~{nPade}~{thetaPadeInput}; Value { Local { [ f_ref2[]-{u_num}]; In Dom; Jacobian JVol; }}}
    }
  }
  { Name Errors; NameOfFormulation NumSol;
    Quantity {
      { Name error2;   Value { Integral { [ Norm[f_ref2[]-{u_num}]^2 ]; In Dom; Jacobian JVol; Integration I1; }}}
      { Name energy2;  Value { Integral { [ Norm[f_ref2[]]^2 ];         In Dom; Jacobian JVol; Integration I1; }}}
      { Name errorAbs; Value { Term { Type Global; [Sqrt[$error2Var]] ;             In Dom; Jacobian JVol; }}}
      { Name errorRel; Value { Term { Type Global; [Sqrt[$error2Var/$energy2Var]] ; In Dom; Jacobian JVol; }}}
    }
  }
  { Name ProjError; NameOfFormulation ProjSol;
    Quantity {
      { Name error2;   Value { Integral { [ Norm[f_ref2[]-{u_proj}]^2 ];             In Dom; Jacobian JVol; Integration I1; }}}
      { Name energy2;  Value { Integral { [ Norm[f_ref2[]]^2 ];                      In Dom; Jacobian JVol; Integration I1; }}}
      { Name errorAbs; Value { Term { Type Global; [Sqrt[$error2Var]] ;             In Dom; Jacobian JVol; }}}
      { Name errorRel; Value { Term { Type Global; [Sqrt[$error2Var/$energy2Var]] ; In Dom; Jacobian JVol; }}}
    }
  }
  { Name NumBnd; NameOfFormulation NumBnd;
    Quantity {
      { Name u_int; Value { Local { [ {u_int} ]; In BndSca; Jacobian JSur; }}}
      { Name u_ext; Value { Local { [ {u_ext} ]; In BndExt; Jacobian JSur; }}}
    }
  }
}

PostOperation{
  { Name NumNormal; NameOfPostProcessing NumNormal;
    Operation {
      Print [u_normal, OnElementsOf Region[{BndExt}], StoreInField (1001), File "out/u_normal.pos"];
    }
  }
  { Name NumCur; NameOfPostProcessing NumCur;
    Operation {
      Print [u_cur, OnElementsOf Region[{BndExt}], StoreInField (1002), File "out/u_cur.pos"];
    }
  }
  { Name NumSol; NameOfPostProcessing NumSol;
    Operation {
      tmp1 = StrCat(DIR, Sprintf("solRef_poly_%g_%g_%g_%g_%g_%g.pos", FLAG_SIGNAL_BC, BND_TYPE, CRN_TYPE, nPade, thetaPadeInput, FAC_Num));
      tmp2 = StrCat(DIR, Sprintf("solNum_poly_%g_%g_%g_%g_%g_%g.pos", FLAG_SIGNAL_BC, BND_TYPE, CRN_TYPE, nPade, thetaPadeInput, FAC_Num));
      tmp3 = StrCat(DIR, Sprintf("solErr_poly_%g_%g_%g_%g_%g_%g.pos", FLAG_SIGNAL_BC, BND_TYPE, CRN_TYPE, nPade, thetaPadeInput, FAC_Num));
      Print [u_ref~{BND_TYPE}~{CRN_TYPE}~{nPade}~{thetaPadeInput}, OnElementsOf Dom, File tmp1];
      Print [u_num~{BND_TYPE}~{CRN_TYPE}~{nPade}~{thetaPadeInput}, OnElementsOf Dom, File tmp2];
      Print [u_err~{BND_TYPE}~{CRN_TYPE}~{nPade}~{thetaPadeInput}, OnElementsOf Dom, File tmp3];
    }
  }
  { Name Errors; NameOfPostProcessing Errors;
    Operation {
      tmp4 = StrCat(DIR, Sprintf("errorAbs_poly_%g_%g_%g_%g_%g_%g.dat", FLAG_SIGNAL_BC, BND_TYPE, CRN_TYPE, nPade, thetaPadeInput, FAC_Num));
      tmp5 = StrCat(DIR, Sprintf("errorRel_poly_%g_%g_%g_%g_%g_%g.dat", FLAG_SIGNAL_BC, BND_TYPE, CRN_TYPE, nPade, thetaPadeInput, FAC_Num));
      Print [error2[Dom],  OnRegion Dom, Format Table, StoreInVariable $error2Var];
      Print [energy2[Dom], OnRegion Dom, Format Table, StoreInVariable $energy2Var];
      Print [errorAbs,     OnRegion Dom, Format Table, SendToServer "Output/1L2-Error (absolute)", File > tmp4];
      Print [errorRel,     OnRegion Dom, Format Table, SendToServer "Output/2L2-Error (relative)", File > tmp5];
    }
  }
  { Name ProjError; NameOfPostProcessing ProjError;
    Operation {
      tmp6 = StrCat(DIR, Sprintf("errorAbs_%g_%g_%g.dat", FLAG_SIGNAL_BC, FAC_Num, N_LAMBDA));
      tmp7 = StrCat(DIR, Sprintf("errorRel_%g_%g_%g.dat", FLAG_SIGNAL_BC, FAC_Num, N_LAMBDA));
      Print [error2[Dom],  OnRegion Dom, Format Table, StoreInVariable $error2Var];
      Print [energy2[Dom], OnRegion Dom, Format Table, StoreInVariable $energy2Var];
      Print [errorAbs,     OnRegion Dom, Format Table, SendToServer "Output/1L2-Error (absolute)", File > tmp6];
      Print [errorRel,     OnRegion Dom, Format Table, SendToServer "Output/2L2-Error (relative)", File > tmp7];
    }
  }
  { Name NumBnd; NameOfPostProcessing NumBnd;
    Operation {
      tmp1 = StrCat(DIR, Sprintf("bndInt_poly_%g_%g.pos", FLAG_SIGNAL_BC, FAC_Num));
      tmp2 = StrCat(DIR, Sprintf("bndExt_poly_%g_%g.pos", FLAG_SIGNAL_BC, FAC_Num));
      Print [u_int, OnElementsOf BndSca, File tmp1];
      Print [u_ext, OnElementsOf BndExt, File tmp2];
    }
  }
}

DefineConstant[
  R_ = {"NumSol", Name "GetDP/1ResolutionChoices", Visible 1, Choices {"NumNormal", "NumCur", "NumSol", "ProjSol", "NumBnd"}},
  P_ = {"NumSol, Errors", Name "GetDP/2PostOperationChoices", Visible 1, Choices {"NumNormal", "NumCur", "NumSol", "Errors", "ProjError", "NumBnd"}}
];
