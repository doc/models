Include "main.dat" ;

//==================================================================================================
// FUNCTIONS FOR SIGNAL
//==================================================================================================

Function {
  I[] = Complex[0,1];
  k[] = WAVENUMBER;
  R[] = Sqrt[X[]^2+Y[]^2+Z[]^2];
  THETA[] = Atan2[Y[],X[]];

If((FLAG_DIM == 2) && (FLAG_SIGNAL == SIGNAL_Harmonic))
  ExpMode[] = Complex[Cos[SIGNAL_MODE*THETA[]], Sin[SIGNAL_MODE*THETA[]]];
  JnR[] = Jn[SIGNAL_MODE,k[]*R_SCA];
  Jnr[] = Jn[SIGNAL_MODE,k[]*R[]  ];
  YnR[] = Yn[SIGNAL_MODE,k[]*R_SCA];
  Ynr[] = Yn[SIGNAL_MODE,k[]*R[]  ];
  dJnR[] = k[]*dJn[SIGNAL_MODE,k[]*R_SCA];
  dJnr[] = k[]*dJn[SIGNAL_MODE,k[]*R[]  ];
  dYnR[] = k[]*dYn[SIGNAL_MODE,k[]*R_SCA];
  dYnr[] = k[]*dYn[SIGNAL_MODE,k[]*R[]  ];
  HnkR[] = Complex[JnR[], YnR[]];
  Hnkr[] = Complex[Jnr[], Ynr[]];
  dHnkR[] = Complex[dJnR[], dYnR[]];
  dHnkr[] = Complex[dJnr[], dYnr[]];
If(FLAG_SIGNAL_BC == SIGNAL_Dirichlet)
  f_inc[] = Jnr[] * ExpMode[];
  f_ref[] = -(JnR[]/HnkR[]) * Hnkr[] * ExpMode[];
EndIf
If(FLAG_SIGNAL_BC == SIGNAL_Neumann)
  df_inc[] = dJnr[] * ExpMode[];
  f_ref[] = -(dJnR[]/dHnkR[]) * Hnkr[] * ExpMode[];
EndIf
EndIf

If((FLAG_DIM == 2) && (FLAG_SIGNAL == SIGNAL_Scatt))
  f_inc[] = Complex[Cos[k[]*X[]], Sin[k[]*X[]]];
If(FLAG_SIGNAL_BC == SIGNAL_Dirichlet)
  f_ref[] = AcousticFieldSoftCylinder[XYZ[]]{WAVENUMBER, R_SCA};
EndIf
If(FLAG_SIGNAL_BC == SIGNAL_Neumann)
  df_inc[] = k[] * Complex[-Sin[k[]*X[]], Cos[k[]*X[]]] * (X[]/R[]);  // Grad * \hat{r}
  // df2_inc[] = k[] * Complex[-Sin[k[]*X[]], Cos[k[]*X[]]];             //  CompX[Grad]
  f_ref[] = AcousticFieldHardCylinder[XYZ[]]{WAVENUMBER, R_SCA, 0, 0, 50};
EndIf
EndIf

If((FLAG_DIM == 3) && (FLAG_SIGNAL == SIGNAL_Harmonic))
  JnSphR[] = JnSph[SIGNAL_MODE,k[]*R_SCA];
  JnSphr[] = JnSph[SIGNAL_MODE,k[]*R[]  ];
  YnSphR[] = YnSph[SIGNAL_MODE,k[]*R_SCA];
  YnSphr[] = YnSph[SIGNAL_MODE,k[]*R[]  ];
  dJnSphR[] = k[]*dJnSph[SIGNAL_MODE,k[]*R_SCA];
  dJnSphr[] = k[]*dJnSph[SIGNAL_MODE,k[]*R[]  ];
  dYnSphR[] = k[]*dYnSph[SIGNAL_MODE,k[]*R_SCA];
  dYnSphr[] = k[]*dYnSph[SIGNAL_MODE,k[]*R[]  ];
  HnSphkR[] = Complex[JnSphR[], YnSphR[]];
  HnSphkr[] = Complex[JnSphr[], YnSphr[]];
  dHnSphkR[] = Complex[dJnSphR[], dYnSphR[]];
  dHnSphkr[] = Complex[dJnSphr[], dYnSphr[]];
If(FLAG_SIGNAL_BC == SIGNAL_Dirichlet)
  f_inc[] = JnSphr[];
  f_ref[] = -(JnSphR[]/HnSphkR[]) * HnSphkr[];
EndIf
If(FLAG_SIGNAL_BC == SIGNAL_Neumann)
  df_inc[] = dJnSphr[];
  f_ref[] = -(dJnSphR[]/dHnSphkR[]) * HnSphkr[];
EndIf
EndIf

If((FLAG_DIM == 3) && (FLAG_SIGNAL == SIGNAL_Scatt))
If(FLAG_SIGNAL_BC == SIGNAL_Dirichlet)
  f_inc[] = Complex[Cos[k[]*X[]], Sin[k[]*X[]]];
  f_ref[] = AcousticFieldSoftSphere[XYZ[]]{WAVENUMBER, R_SCA, 1, 0, 0};
EndIf
If(FLAG_SIGNAL_BC == SIGNAL_Neumann)
  df_inc[] = Complex[0,1] * k[]*X[]/R[] * Complex[Cos[k[]*X[]], Sin[k[]*X[]]];
  f_ref[] = AcousticFieldHardSphere[XYZ[]]{WAVENUMBER, R_SCA, 1, 0, 0};
EndIf
EndIf
}

//==================================================================================================
// JACOBIAN & INTEGRATION
//==================================================================================================

Jacobian {
  { Name JVol; Case {{ Region All; Jacobian Vol; }}}
  { Name JSur; Case {{ Region All; Jacobian Sur; }}}
  { Name JLin; Case {{ Region All; Jacobian Lin; }}}
}

Integration {
  { Name I1;
    Case {
      { Type Gauss;
        Case {
          { GeoElement Point;        NumberOfPoints  1; }
          { GeoElement Line;         NumberOfPoints  6; } // 4
          { GeoElement Line2;        NumberOfPoints  6; } // 4
          { GeoElement Quadrangle;   NumberOfPoints  4; } // 36
          { GeoElement Quadrangle2;  NumberOfPoints  7; } // 36
          { GeoElement Triangle;     NumberOfPoints  4; } // 6 // 1 3 4 6 7 12 13 16
          { GeoElement Triangle2;    NumberOfPoints  6; } // 6 // 1 3 4 6 7 12 13 16 // 12
          { GeoElement Tetrahedron;  NumberOfPoints  5; } // 15 // 1 4 5 15 16 17 29
          { GeoElement Tetrahedron2; NumberOfPoints 15; } // 15 // 1 4 5 15 16 17 29
        }
      }
    }
  }
}

//==================================================================================================
// LOAD SPECIFIC .PRO
//==================================================================================================

Include Str[LinkPro];

DefineConstant[
  C_ = {"-solve -pos -bin -v2", Name "GetDP/9ComputeCommand", Visible 0 }
];
