Include "domSquare.dat";

Point(0) = {0, 0, 0};

Point(1) = {X~{1}, Y~{1}, 0};
Point(2) = {X~{2}, Y~{2}, 0};
Point(3) = {X~{3}, Y~{3}, 0};
Point(4) = {X~{4}, Y~{4}, 0};

Point(5) = {-R_SCA,     0, 0};
Point(6) = {     0,-R_SCA, 0};
Point(7) = { R_SCA,     0, 0};
Point(8) = {     0, R_SCA, 0};

Line(1) = {4, 1};
Line(2) = {1, 2};
Line(3) = {2, 3};
Line(4) = {3, 4};

Circle(5) = {5, 0, 6};
Circle(6) = {6, 0, 7};
Circle(7) = {7, 0, 8};
Circle(8) = {8, 0, 5};

Line Loop(1) = {1, 2, 3, 4, -5, -6, -7, -8};
Plane Surface(1) = {1};

If(BND_TYPE == BND_PML)
  out0[] = Extrude {-Lpml,0,0} { Line{1}; Layers{Npml}; Recombine; };
  Printf("==== %g ====\n", out0[0]);
  Printf("==== %g ====\n", out0[1]);
  Printf("==== %g ====\n", out0[2]);
  Printf("==== %g ====\n", out0[3]);
  out1[] = Extrude { Lpml,0,0} { Line{3}; Layers{Npml}; Recombine; };
  Printf("==== %g ====\n", out1[0]);
  Printf("==== %g ====\n", out1[1]);
  Printf("==== %g ====\n", out1[2]);
  Printf("==== %g ====\n", out1[3]);
  out2[] = Extrude {0,-Lpml,0} { Line{11,2,14}; Layers{Npml}; Recombine; };
  Printf("==== %g ====\n", out2[0]);
  Printf("==== %g ====\n", out2[1]);
  Printf("==== %g ====\n", out2[2]);
  Printf("==== %g ====\n", out2[3]);
  out3[] = Extrude {0, Lpml,0} { Line{10,4,15}; Layers{Npml}; Recombine; };
  Printf("==== %g ====\n", out3[0]);
  Printf("==== %g ====\n", out3[1]);
  Printf("==== %g ====\n", out3[2]);
  Printf("==== %g ====\n", out3[3]);
EndIf

Physical Point(CRN_1) = {1};
Physical Point(CRN_2) = {2};
Physical Point(CRN_3) = {3};
Physical Point(CRN_4) = {4};

Physical Line(BND_1) = {1}; // Left
Physical Line(BND_2) = {2}; // Down
Physical Line(BND_3) = {3}; // Right
Physical Line(BND_4) = {4}; // Top
Physical Line(BND_Scatt) = {5,6,7,8};

Physical Surface(DOM) = {1};

If(BND_TYPE == BND_PML)
  Physical Surface(DOM_PML) = {12,16,20,24,28,32,36,40};
If(Npml == 0)
  Physical Line(BND_PmlExt) = {5,6,7,8};
Else
  Physical Line(BND_PmlExt) = {17,21,25,27,13,39,37,33,29,31,9,19};
EndIf
EndIf
