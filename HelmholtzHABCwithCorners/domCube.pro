Include "domCube.dat";

//==================================================================================================
// OPTIONS and PARAMETERS
//==================================================================================================

BND_Neumann        = 0;
BND_Sommerfeld     = 1;
BND_Second         = 2;
BND_Pade           = 3;
CRN_Regularization = 0;
CRN_Compatibility  = 1;
KEPS_Nothing       = 0;
KEPS_Analytic      = 1;
KEPS_Numeric       = 2;

DefineConstant[
  BND_TYPE = {BND_Pade,
    Name "Input/5Model/02Boundary condition (faces)", Highlight "Blue",
    Choices {BND_Neumann    = "Homogeneous Neumann",
             BND_Sommerfeld = "Sommerfeld ABC",
             BND_Second     = "Second-order ABC",
             BND_Pade       = "Pade ABC"}},
  CRN_TYPE = {CRN_Compatibility,
    Name "Input/5Model/03Boundary condition (edges, corners)", Highlight "Red",
    Visible ((BND_TYPE == BND_Second) || (BND_TYPE == BND_Pade)),
    Choices {CRN_Regularization = "Regularization",
             CRN_Compatibility  = "Compatibility"}},
  nPade = {4, Min 0, Step 1, Max 6,
    Name "Input/5Model/05Pade: Number of fields",
    Visible (BND_TYPE == BND_Pade)},
  thetaPadeInput = {3, Min 0, Step 1, Max 4,
    Name "Input/5Model/06Pade: Rotation of branch cut",
    Visible (BND_TYPE == BND_Pade)},
  KEPS_TYPE = {KEPS_Nothing,
    Name "Input/5Model/07Curvature for regularization", Highlight "Red",
    Visible ((BND_TYPE == BND_Pade) && (CRN_TYPE == CRN_Regularization)),
    Choices {KEPS_Nothing  = "Nothing",
             KEPS_Analytic = "Analytic formula",
             KEPS_Numeric  = "Numerical curvature"}}
];

If(BND_TYPE == BND_Pade)
  If(thetaPadeInput == 0)
    thetaPade = 0;
  EndIf
  If(thetaPadeInput == 1)
    thetaPade = Pi/8;
  EndIf
  If(thetaPadeInput == 2)
    thetaPade = Pi/4;
  EndIf
  If(thetaPadeInput == 3)
    thetaPade = Pi/3;
  EndIf
  If(thetaPadeInput == 4)
    thetaPade = Pi/2;
  EndIf
  mPade = 2*nPade+1;
  For j In{1:nPade}
    cPade~{j} = Tan[j*Pi/mPade]^2;
  EndFor
Else
  nPade = 0;
  thetaPadeInput = 0;
EndIf

Group {
  Dom = Region[{VOL}];
  BndSca = Region[{SUR_Scatt}];

  For i In {1:2}
    FacesAll += Region[{SUR~{i}~{0}~{0}}];
    FacesAll += Region[{SUR~{0}~{i}~{0}}];
    FacesAll += Region[{SUR~{0}~{0}~{i}}];
    FacesX += Region[{SUR~{i}~{0}~{0}}];
    FacesY += Region[{SUR~{0}~{i}~{0}}];
    FacesZ += Region[{SUR~{0}~{0}~{i}}];
  EndFor
  For i In {1:2}
    For j In {1:2}
      EdgesAll += Region[{LIN~{0}~{i}~{j}}];
      EdgesAll += Region[{LIN~{j}~{0}~{i}}];
      EdgesAll += Region[{LIN~{i}~{j}~{0}}];
      EdgesYZ += Region[{LIN~{0}~{i}~{j}}];
      EdgesZX += Region[{LIN~{j}~{0}~{i}}];
      EdgesXY += Region[{LIN~{i}~{j}~{0}}];
      BndFacesX += Region[{LIN~{j}~{0}~{i}}];
      BndFacesX += Region[{LIN~{i}~{j}~{0}}];
      BndFacesY += Region[{LIN~{0}~{i}~{j}}];
      BndFacesY += Region[{LIN~{i}~{j}~{0}}];
      BndFacesZ += Region[{LIN~{0}~{i}~{j}}];
      BndFacesZ += Region[{LIN~{j}~{0}~{i}}];
    EndFor
  EndFor
  For i In {1:2}
    For j In {1:2}
      For k In {1:2}
        CornersAll += Region[{PNT~{i}~{j}~{k}}];
        BndEdgesYZ += Region[{PNT~{i}~{j}~{k}}];
        BndEdgesZX += Region[{PNT~{i}~{j}~{k}}];
        BndEdgesXY += Region[{PNT~{i}~{j}~{k}}];
      EndFor
    EndFor
  EndFor

  BndExt = Region[{FacesAll,EdgesAll,CornersAll}];
  DomAll = Region[{Dom,BndSca,BndExt}];
}

Function {
  NormalNum[]  = VectorField[XYZ[]]{1001};
  CurvNum[]    = ScalarField[XYZ[]]{1002};

If((BND_TYPE == BND_Pade) && (CRN_TYPE == CRN_Regularization) && (KEPS_TYPE == KEPS_Numeric))
  kEps[BndExt] = WAVENUMBER + I[] * 0.39 * WAVENUMBER^(1/3) * (CurvNum[])^(2/3);
Else
  kEps[BndExt] = WAVENUMBER;
EndIf

If(BND_TYPE == BND_Pade)
  ExpPTheta[]  = Complex[Cos[ thetaPade],Sin[ thetaPade]];
  ExpMTheta[]  = Complex[Cos[-thetaPade],Sin[-thetaPade]];
  ExpPTheta2[] = Complex[Cos[thetaPade/2.],Sin[thetaPade/2.]];
EndIf
}

//==================================================================================================
// FONCTION SPACES with CONSTRAINTS
//==================================================================================================

Constraint {
  { Name DirichletBC;
    Case {{ Region BndSca; Value f_ref[]; }}
  }
}

FunctionSpace {
  { Name H_nx;  Type Form0; BasisFunction {{ Name si; NameOfCoef pi; Function BF_Node; Support Region[{FacesAll,BndSca}]; Entity NodesOf[All]; }}}
  { Name H_ny;  Type Form0; BasisFunction {{ Name si; NameOfCoef pi; Function BF_Node; Support Region[{FacesAll,BndSca}]; Entity NodesOf[All]; }}}
  { Name H_nz;  Type Form0; BasisFunction {{ Name si; NameOfCoef pi; Function BF_Node; Support Region[{FacesAll,BndSca}]; Entity NodesOf[All]; }}}
  { Name H_cur; Type Form0; BasisFunction {{ Name si; NameOfCoef pi; Function BF_Node; Support Region[{FacesAll,BndSca}]; Entity NodesOf[All]; }}}
  { Name H_num; Type Form0;
    BasisFunction {{ Name sn; NameOfCoef pn; Function BF_Node; Support Region[{DomAll}]; Entity NodesOf[All]; }}
    Constraint {{ NameOfCoef pn; EntityType NodesOf; NameOfConstraint DirichletBC; }}
  }
If(BND_TYPE == BND_Pade)
If(CRN_TYPE == CRN_Regularization)
  For m In {1:nPade}
    { Name H~{m}; Type Form0; BasisFunction {{ Name sn; NameOfCoef un; Function BF_Node; Support Region[{FacesAll}]; Entity NodesOf[All]; }}}
  EndFor
EndIf
If(CRN_TYPE == CRN_Compatibility)
  For m In {1:nPade}
    { Name H~{m}~{0}~{0}; Type Form0; BasisFunction {{ Name sn; NameOfCoef un; Function BF_Node; Support Region[{FacesX,BndFacesX}]; Entity NodesOf[All]; }}}
    { Name H~{0}~{m}~{0}; Type Form0; BasisFunction {{ Name sn; NameOfCoef un; Function BF_Node; Support Region[{FacesY,BndFacesY}]; Entity NodesOf[All]; }}}
    { Name H~{0}~{0}~{m}; Type Form0; BasisFunction {{ Name sn; NameOfCoef un; Function BF_Node; Support Region[{FacesZ,BndFacesZ}]; Entity NodesOf[All]; }}}
  EndFor
  For m In {1:nPade}
  For n In {1:nPade}
    { Name H~{0}~{m}~{n}; Type Form0; BasisFunction {{ Name sn; NameOfCoef un; Function BF_Node; Support Region[{EdgesYZ,BndEdgesYZ}]; Entity NodesOf[All]; }}}
    { Name H~{n}~{0}~{m}; Type Form0; BasisFunction {{ Name sn; NameOfCoef un; Function BF_Node; Support Region[{EdgesZX,BndEdgesZX}]; Entity NodesOf[All]; }}}
    { Name H~{m}~{n}~{0}; Type Form0; BasisFunction {{ Name sn; NameOfCoef un; Function BF_Node; Support Region[{EdgesXY,BndEdgesXY}]; Entity NodesOf[All]; }}}
  EndFor
  EndFor
  For m In {1:nPade}
  For n In {1:nPade}
  For o In {1:nPade}
    { Name H~{m}~{n}~{o}; Type Form0; BasisFunction {{ Name sn; NameOfCoef un; Function BF_Node; Support Region[{CornersAll}]; Entity NodesOf[All]; }}}
  EndFor
  EndFor
  EndFor
EndIf
EndIf
}

//==================================================================================================
// FORMULATIONS
//==================================================================================================

Formulation {
  { Name NumNormal; Type FemEquation;
    Quantity {
      { Name u_nx; Type Local; NameOfSpace H_nx; }
      { Name u_ny; Type Local; NameOfSpace H_ny; }
      { Name u_nz; Type Local; NameOfSpace H_nz; }
    }
    Equation {
      Galerkin{ [ Dof{u_nx}        , {u_nx} ]; In Region[{BndExt,BndSca}]; Jacobian JSur; Integration I1; }
      Galerkin{ [ Dof{u_ny}        , {u_ny} ]; In Region[{BndExt,BndSca}]; Jacobian JSur; Integration I1; }
      Galerkin{ [ Dof{u_nz}        , {u_nz} ]; In Region[{BndExt,BndSca}]; Jacobian JSur; Integration I1; }
      Galerkin{ [ -CompX[Normal[]] , {u_nx} ]; In Region[{BndExt,BndSca}]; Jacobian JSur; Integration I1; }
      Galerkin{ [ -CompY[Normal[]] , {u_ny} ]; In Region[{BndExt,BndSca}]; Jacobian JSur; Integration I1; }
      Galerkin{ [ -CompZ[Normal[]] , {u_nz} ]; In Region[{BndExt,BndSca}]; Jacobian JSur; Integration I1; }
    }
  }

  { Name NumCur; Type FemEquation;
    Quantity {
      { Name u_nx; Type Local; NameOfSpace H_nx; }
      { Name u_ny; Type Local; NameOfSpace H_ny; }
      { Name u_nz; Type Local; NameOfSpace H_nz; }
      { Name u_cur; Type Local; NameOfSpace H_cur; }
    }
    Equation {
      Galerkin{ [ Dof{u_nx}           , {u_nx} ]; In Region[{BndExt,BndSca}]; Jacobian JSur; Integration I1; }
      Galerkin{ [ Dof{u_ny}           , {u_ny} ]; In Region[{BndExt,BndSca}]; Jacobian JSur; Integration I1; }
      Galerkin{ [ Dof{u_nz}           , {u_nz} ]; In Region[{BndExt,BndSca}]; Jacobian JSur; Integration I1; }
      Galerkin{ [ -CompX[NormalNum[]] , {u_nx} ]; In Region[{BndExt,BndSca}]; Jacobian JSur; Integration I1; }
      Galerkin{ [ -CompY[NormalNum[]] , {u_ny} ]; In Region[{BndExt,BndSca}]; Jacobian JSur; Integration I1; }
      Galerkin{ [ -CompZ[NormalNum[]] , {u_nz} ]; In Region[{BndExt,BndSca}]; Jacobian JSur; Integration I1; }

      Galerkin{ [ Dof{u_cur}                     , {u_cur} ]; In Region[{BndExt,BndSca}]; Jacobian JSur; Integration I1; }
      Galerkin{ [ -Vector[0.5,0,0] * Dof{d u_nx} , {u_cur} ]; In Region[{BndExt,BndSca}]; Jacobian JSur; Integration I1; }
      Galerkin{ [ -Vector[0,0.5,0] * Dof{d u_ny} , {u_cur} ]; In Region[{BndExt,BndSca}]; Jacobian JSur; Integration I1; }
      Galerkin{ [ -Vector[0,0,0.5] * Dof{d u_nz} , {u_cur} ]; In Region[{BndExt,BndSca}]; Jacobian JSur; Integration I1; }
    }
  }

  { Name NumSol; Type FemEquation;
    Quantity {
      { Name u_num; Type Local; NameOfSpace H_num; }
If(BND_TYPE == BND_Pade)
If(CRN_TYPE == CRN_Regularization)
      For m In {1:nPade}
        { Name u~{m}; Type Local; NameOfSpace H~{m}; }
      EndFor
EndIf
If(CRN_TYPE == CRN_Compatibility)
      For m In {1:nPade}
        { Name u~{m}~{0}~{0}; Type Local; NameOfSpace H~{m}~{0}~{0}; }
        { Name u~{0}~{m}~{0}; Type Local; NameOfSpace H~{0}~{m}~{0}; }
        { Name u~{0}~{0}~{m}; Type Local; NameOfSpace H~{0}~{0}~{m}; }
      EndFor
      For m In {1:nPade}
      For n In {1:nPade}
        { Name u~{0}~{m}~{n}; Type Local; NameOfSpace H~{0}~{m}~{n}; }
        { Name u~{n}~{0}~{m}; Type Local; NameOfSpace H~{n}~{0}~{m}; }
        { Name u~{m}~{n}~{0}; Type Local; NameOfSpace H~{m}~{n}~{0}; }
      EndFor
      EndFor
      For m In {1:nPade}
      For n In {1:nPade}
      For o In {1:nPade}
        { Name u~{m}~{n}~{o}; Type Local; NameOfSpace H~{m}~{n}~{o}; }
      EndFor
      EndFor
      EndFor
EndIf
EndIf
    }
    Equation {

// Helmholtz

      Galerkin{ [ Dof{d u_num}    , {d u_num} ]; In Dom; Jacobian JVol; Integration I1; }
      Galerkin{ [ -k[]^2*Dof{u_num} , {u_num} ]; In Dom; Jacobian JVol; Integration I1; }

// Sommerfeld ABC

If(BND_TYPE == BND_Sommerfeld)
      Galerkin{ [ -I[]*k[] * Dof{u_num} , {u_num} ]; In FacesAll; Jacobian JSur; Integration I1; }
EndIf

// Second-order ABC

If(BND_TYPE == BND_Second)
      Galerkin { [ -I[]*k[] * Dof{u_num}                    , {u_num} ]; In FacesAll;   Jacobian JSur; Integration I1; }
      Galerkin { [ -1/(2*I[]*k[]) * Dof{d u_num}          , {d u_num} ]; In FacesAll;   Jacobian JSur; Integration I1; }
If(CRN_TYPE == CRN_Compatibility)
      Galerkin { [ 3/4 * Dof{u_num}                         , {u_num} ]; In EdgesAll;   Jacobian JLin; Integration I1; }
      Galerkin { [ 1/(2*I[]*k[]*2*I[]*k[]) * Dof{d u_num} , {d u_num} ]; In EdgesAll;   Jacobian JLin; Integration I1; }
      Galerkin { [ -1/(2*I[]*k[]) * Dof{u_num}              , {u_num} ]; In CornersAll; Jacobian JVol; Integration I1; }
EndIf
EndIf

// Pade ABC

If(BND_TYPE == BND_Pade)
If(CRN_TYPE == CRN_Regularization)

      Galerkin { [ -I[]*k[]*ExpPTheta2[] * Dof{u_num}                        , {u_num} ]; In FacesAll; Jacobian JSur; Integration I1; }

    For m In {1:nPade}
      Galerkin { [ -I[]*k[]*ExpPTheta2[] * 2./mPade * cPade~{m} * Dof{u~{m}} , {u_num} ]; In FacesAll; Jacobian JSur; Integration I1; }
      Galerkin { [ -I[]*k[]*ExpPTheta2[] * 2./mPade * cPade~{m} * Dof{u_num} , {u_num} ]; In FacesAll; Jacobian JSur; Integration I1; }

      Galerkin { [ Dof{d u~{m}}                                            , {d u~{m}} ]; In FacesAll; Jacobian JSur; Integration I1; }
      Galerkin { [ -kEps[]^2*ExpPTheta[]*(cPade~{m}+ExpMTheta[]) * Dof{u~{m}}   , {u~{m}} ]; In FacesAll; Jacobian JSur; Integration I1; }
      Galerkin { [ -kEps[]^2*ExpPTheta[]*(cPade~{m}+1) * Dof{u_num}             , {u~{m}} ]; In FacesAll; Jacobian JSur; Integration I1; }
    EndFor

EndIf
If(CRN_TYPE == CRN_Compatibility)

    // --- Faces

      Galerkin { [ -I[]*k[]*ExpPTheta2[] * Dof{u_num} , {u_num} ]; In FacesX; Jacobian JSur; Integration I1; }
      Galerkin { [ -I[]*k[]*ExpPTheta2[] * Dof{u_num} , {u_num} ]; In FacesY; Jacobian JSur; Integration I1; }
      Galerkin { [ -I[]*k[]*ExpPTheta2[] * Dof{u_num} , {u_num} ]; In FacesZ; Jacobian JSur; Integration I1; }

    For m In {1:nPade}
      Galerkin { [ -I[]*k[]*ExpPTheta2[] * 2./mPade * cPade~{m} * Dof{u~{m}~{0}~{0}} , {u_num} ]; In FacesX; Jacobian JSur; Integration I1; }
      Galerkin { [ -I[]*k[]*ExpPTheta2[] * 2./mPade * cPade~{m} * Dof{u_num}         , {u_num} ]; In FacesX; Jacobian JSur; Integration I1; }
      Galerkin { [ -I[]*k[]*ExpPTheta2[] * 2./mPade * cPade~{m} * Dof{u~{0}~{m}~{0}} , {u_num} ]; In FacesY; Jacobian JSur; Integration I1; }
      Galerkin { [ -I[]*k[]*ExpPTheta2[] * 2./mPade * cPade~{m} * Dof{u_num}         , {u_num} ]; In FacesY; Jacobian JSur; Integration I1; }
      Galerkin { [ -I[]*k[]*ExpPTheta2[] * 2./mPade * cPade~{m} * Dof{u~{0}~{0}~{m}} , {u_num} ]; In FacesZ; Jacobian JSur; Integration I1; }
      Galerkin { [ -I[]*k[]*ExpPTheta2[] * 2./mPade * cPade~{m} * Dof{u_num}         , {u_num} ]; In FacesZ; Jacobian JSur; Integration I1; }

      Galerkin { [ Dof{d u~{m}~{0}~{0}}                                          , {d u~{m}~{0}~{0}} ]; In FacesX; Jacobian JSur; Integration I1; }
      Galerkin { [ Dof{d u~{0}~{m}~{0}}                                          , {d u~{0}~{m}~{0}} ]; In FacesY; Jacobian JSur; Integration I1; }
      Galerkin { [ Dof{d u~{0}~{0}~{m}}                                          , {d u~{0}~{0}~{m}} ]; In FacesZ; Jacobian JSur; Integration I1; }
      Galerkin { [ -k[]^2*ExpPTheta[]*(cPade~{m}+ExpMTheta[]) * Dof{u~{m}~{0}~{0}} , {u~{m}~{0}~{0}} ]; In FacesX; Jacobian JSur; Integration I1; }
      Galerkin { [ -k[]^2*ExpPTheta[]*(cPade~{m}+ExpMTheta[]) * Dof{u~{0}~{m}~{0}} , {u~{0}~{m}~{0}} ]; In FacesY; Jacobian JSur; Integration I1; }
      Galerkin { [ -k[]^2*ExpPTheta[]*(cPade~{m}+ExpMTheta[]) * Dof{u~{0}~{0}~{m}} , {u~{0}~{0}~{m}} ]; In FacesZ; Jacobian JSur; Integration I1; }
      Galerkin { [ -k[]^2*ExpPTheta[]*(cPade~{m}+1) * Dof{u_num}                   , {u~{m}~{0}~{0}} ]; In FacesX; Jacobian JSur; Integration I1; }
      Galerkin { [ -k[]^2*ExpPTheta[]*(cPade~{m}+1) * Dof{u_num}                   , {u~{0}~{m}~{0}} ]; In FacesY; Jacobian JSur; Integration I1; }
      Galerkin { [ -k[]^2*ExpPTheta[]*(cPade~{m}+1) * Dof{u_num}                   , {u~{0}~{0}~{m}} ]; In FacesZ; Jacobian JSur; Integration I1; }
    EndFor

    // --- Edges

    For m In {1:nPade}
      Galerkin { [ -I[]*k[]*ExpPTheta2[] * Dof{u~{0}~{m}~{0}} , {u~{0}~{m}~{0}} ]; In EdgesYZ; Jacobian JLin; Integration I1; }
      Galerkin { [ -I[]*k[]*ExpPTheta2[] * Dof{u~{0}~{0}~{m}} , {u~{0}~{0}~{m}} ]; In EdgesZX; Jacobian JLin; Integration I1; }
      Galerkin { [ -I[]*k[]*ExpPTheta2[] * Dof{u~{m}~{0}~{0}} , {u~{m}~{0}~{0}} ]; In EdgesXY; Jacobian JLin; Integration I1; }
    EndFor
    For n In {1:nPade}
      Galerkin { [ -I[]*k[]*ExpPTheta2[] * Dof{u~{0}~{0}~{n}} , {u~{0}~{0}~{n}} ]; In EdgesYZ; Jacobian JLin; Integration I1; }
      Galerkin { [ -I[]*k[]*ExpPTheta2[] * Dof{u~{n}~{0}~{0}} , {u~{n}~{0}~{0}} ]; In EdgesZX; Jacobian JLin; Integration I1; }
      Galerkin { [ -I[]*k[]*ExpPTheta2[] * Dof{u~{0}~{n}~{0}} , {u~{0}~{n}~{0}} ]; In EdgesXY; Jacobian JLin; Integration I1; }
    EndFor

    For m In {1:nPade}
    For n In {1:nPade}
      Galerkin { [ -I[]*k[]*ExpPTheta2[] * 2./mPade * cPade~{n} * Dof{u~{0}~{m}~{n}} , {u~{0}~{m}~{0}} ]; In EdgesYZ; Jacobian JLin; Integration I1; }
      Galerkin { [ -I[]*k[]*ExpPTheta2[] * 2./mPade * cPade~{n} * Dof{u~{0}~{m}~{0}} , {u~{0}~{m}~{0}} ]; In EdgesYZ; Jacobian JLin; Integration I1; }
      Galerkin { [ -I[]*k[]*ExpPTheta2[] * 2./mPade * cPade~{m} * Dof{u~{0}~{m}~{n}} , {u~{0}~{0}~{n}} ]; In EdgesYZ; Jacobian JLin; Integration I1; }
      Galerkin { [ -I[]*k[]*ExpPTheta2[] * 2./mPade * cPade~{m} * Dof{u~{0}~{0}~{n}} , {u~{0}~{0}~{n}} ]; In EdgesYZ; Jacobian JLin; Integration I1; }
      Galerkin { [ -I[]*k[]*ExpPTheta2[] * 2./mPade * cPade~{n} * Dof{u~{n}~{0}~{m}} , {u~{0}~{0}~{m}} ]; In EdgesZX; Jacobian JLin; Integration I1; }
      Galerkin { [ -I[]*k[]*ExpPTheta2[] * 2./mPade * cPade~{n} * Dof{u~{0}~{0}~{m}} , {u~{0}~{0}~{m}} ]; In EdgesZX; Jacobian JLin; Integration I1; }
      Galerkin { [ -I[]*k[]*ExpPTheta2[] * 2./mPade * cPade~{m} * Dof{u~{n}~{0}~{m}} , {u~{n}~{0}~{0}} ]; In EdgesZX; Jacobian JLin; Integration I1; }
      Galerkin { [ -I[]*k[]*ExpPTheta2[] * 2./mPade * cPade~{m} * Dof{u~{n}~{0}~{0}} , {u~{n}~{0}~{0}} ]; In EdgesZX; Jacobian JLin; Integration I1; }
      Galerkin { [ -I[]*k[]*ExpPTheta2[] * 2./mPade * cPade~{n} * Dof{u~{m}~{n}~{0}} , {u~{m}~{0}~{0}} ]; In EdgesXY; Jacobian JLin; Integration I1; }
      Galerkin { [ -I[]*k[]*ExpPTheta2[] * 2./mPade * cPade~{n} * Dof{u~{m}~{0}~{0}} , {u~{m}~{0}~{0}} ]; In EdgesXY; Jacobian JLin; Integration I1; }
      Galerkin { [ -I[]*k[]*ExpPTheta2[] * 2./mPade * cPade~{m} * Dof{u~{m}~{n}~{0}} , {u~{0}~{n}~{0}} ]; In EdgesXY; Jacobian JLin; Integration I1; }
      Galerkin { [ -I[]*k[]*ExpPTheta2[] * 2./mPade * cPade~{m} * Dof{u~{0}~{n}~{0}} , {u~{0}~{n}~{0}} ]; In EdgesXY; Jacobian JLin; Integration I1; }

      Galerkin { [ Dof{d u~{0}~{m}~{n}}                                                    , {d u~{0}~{m}~{n}} ]; In EdgesYZ; Jacobian JLin; Integration I1; }
      Galerkin { [ Dof{d u~{n}~{0}~{m}}                                                    , {d u~{n}~{0}~{m}} ]; In EdgesZX; Jacobian JLin; Integration I1; }
      Galerkin { [ Dof{d u~{m}~{n}~{0}}                                                    , {d u~{m}~{n}~{0}} ]; In EdgesXY; Jacobian JLin; Integration I1; }
      Galerkin { [ -k[]^2*ExpPTheta[]*(cPade~{m}+cPade~{n}+ExpMTheta[]) * Dof{u~{0}~{m}~{n}} , {u~{0}~{m}~{n}} ]; In EdgesYZ; Jacobian JLin; Integration I1; }
      Galerkin { [ -k[]^2*ExpPTheta[]*(cPade~{m}+cPade~{n}+ExpMTheta[]) * Dof{u~{n}~{0}~{m}} , {u~{n}~{0}~{m}} ]; In EdgesZX; Jacobian JLin; Integration I1; }
      Galerkin { [ -k[]^2*ExpPTheta[]*(cPade~{m}+cPade~{n}+ExpMTheta[]) * Dof{u~{m}~{n}~{0}} , {u~{m}~{n}~{0}} ]; In EdgesXY; Jacobian JLin; Integration I1; }
      Galerkin { [ -k[]^2*ExpPTheta[]*(cPade~{m}+1)                     * Dof{u~{0}~{0}~{n}} , {u~{0}~{m}~{n}} ]; In EdgesYZ; Jacobian JLin; Integration I1; }
      Galerkin { [ -k[]^2*ExpPTheta[]*(cPade~{m}+1)                     * Dof{u~{n}~{0}~{0}} , {u~{n}~{0}~{m}} ]; In EdgesZX; Jacobian JLin; Integration I1; }
      Galerkin { [ -k[]^2*ExpPTheta[]*(cPade~{m}+1)                     * Dof{u~{0}~{n}~{0}} , {u~{m}~{n}~{0}} ]; In EdgesXY; Jacobian JLin; Integration I1; }
      Galerkin { [ -k[]^2*ExpPTheta[]*(cPade~{n}+1)                     * Dof{u~{0}~{m}~{0}} , {u~{0}~{m}~{n}} ]; In EdgesYZ; Jacobian JLin; Integration I1; }
      Galerkin { [ -k[]^2*ExpPTheta[]*(cPade~{n}+1)                     * Dof{u~{0}~{0}~{m}} , {u~{n}~{0}~{m}} ]; In EdgesZX; Jacobian JLin; Integration I1; }
      Galerkin { [ -k[]^2*ExpPTheta[]*(cPade~{n}+1)                     * Dof{u~{m}~{0}~{0}} , {u~{m}~{n}~{0}} ]; In EdgesXY; Jacobian JLin; Integration I1; }
    EndFor
    EndFor

    // --- Corners

    For m In {1:nPade}
    For n In {1:nPade}
      Galerkin { [ -I[]*k[]*ExpPTheta2[] * Dof{u~{0}~{m}~{n}} , {u~{0}~{m}~{n}} ]; In CornersAll; Jacobian JSur; Integration I1; }
      Galerkin { [ -I[]*k[]*ExpPTheta2[] * Dof{u~{n}~{0}~{m}} , {u~{n}~{0}~{m}} ]; In CornersAll; Jacobian JSur; Integration I1; }
      Galerkin { [ -I[]*k[]*ExpPTheta2[] * Dof{u~{m}~{n}~{0}} , {u~{m}~{n}~{0}} ]; In CornersAll; Jacobian JSur; Integration I1; }
    EndFor
    EndFor

    For m In {1:nPade}
    For n In {1:nPade}
    For o In {1:nPade}
      Galerkin { [ -I[]*k[]*ExpPTheta2[] * 2./mPade * cPade~{m} * Dof{u~{m}~{n}~{o}} , {u~{0}~{n}~{o}} ]; In CornersAll; Jacobian JSur; Integration I1; }
      Galerkin { [ -I[]*k[]*ExpPTheta2[] * 2./mPade * cPade~{m} * Dof{u~{0}~{n}~{o}} , {u~{0}~{n}~{o}} ]; In CornersAll; Jacobian JSur; Integration I1; }
      Galerkin { [ -I[]*k[]*ExpPTheta2[] * 2./mPade * cPade~{n} * Dof{u~{m}~{n}~{o}} , {u~{m}~{0}~{o}} ]; In CornersAll; Jacobian JSur; Integration I1; }
      Galerkin { [ -I[]*k[]*ExpPTheta2[] * 2./mPade * cPade~{n} * Dof{u~{m}~{0}~{o}} , {u~{m}~{0}~{o}} ]; In CornersAll; Jacobian JSur; Integration I1; }
      Galerkin { [ -I[]*k[]*ExpPTheta2[] * 2./mPade * cPade~{o} * Dof{u~{m}~{n}~{o}} , {u~{m}~{n}~{0}} ]; In CornersAll; Jacobian JSur; Integration I1; }
      Galerkin { [ -I[]*k[]*ExpPTheta2[] * 2./mPade * cPade~{o} * Dof{u~{m}~{n}~{0}} , {u~{m}~{n}~{0}} ]; In CornersAll; Jacobian JSur; Integration I1; }

      Galerkin { [ (cPade~{m}+cPade~{n}+cPade~{o}+ExpMTheta[]) * Dof{u~{m}~{n}~{o}}  , {u~{m}~{n}~{o}} ]; In CornersAll; Jacobian JSur; Integration I1; }
      Galerkin { [ (cPade~{m}+1)                               * Dof{u~{0}~{n}~{o}}  , {u~{m}~{n}~{o}} ]; In CornersAll; Jacobian JSur; Integration I1; }
      Galerkin { [ (cPade~{n}+1)                               * Dof{u~{m}~{0}~{o}}  , {u~{m}~{n}~{o}} ]; In CornersAll; Jacobian JSur; Integration I1; }
      Galerkin { [ (cPade~{o}+1)                               * Dof{u~{m}~{n}~{0}}  , {u~{m}~{n}~{o}} ]; In CornersAll; Jacobian JSur; Integration I1; }
    EndFor
    EndFor
    EndFor

EndIf
EndIf

    }
  }
}

//==================================================================================================
// RESOLUTION
//==================================================================================================

Resolution {
  { Name NumNormal;
    System {
      { Name A; NameOfFormulation NumNormal; Type Real; }
    }
    Operation {
      Generate[A]; Solve[A]; SaveSolution[A];
    }
  }
  { Name NumCur;
    System {
      { Name A; NameOfFormulation NumNormal; Type Real; }
      { Name B; NameOfFormulation NumCur;    Type Real; }
    }
    Operation {
      Generate[A]; Solve[A]; SaveSolution[A]; PostOperation[NumNormal];
      Generate[B]; Solve[B]; SaveSolution[B];
    }
  }
  { Name NumSol;
    System {
If((BND_TYPE == BND_Pade) && (CRN_TYPE == CRN_Regularization) && (KEPS_TYPE == KEPS_Numeric))
      { Name A; NameOfFormulation NumNormal; Type Real; }
      { Name B; NameOfFormulation NumCur;    Type Real; }
EndIf
      { Name C; NameOfFormulation NumSol; Type Complex; }
    }
    Operation {
If((BND_TYPE == BND_Pade) && (CRN_TYPE == CRN_Regularization) && (KEPS_TYPE == KEPS_Numeric))
      Generate[A]; Solve[A]; SaveSolution[A]; PostOperation[NumNormal];
      Generate[B]; Solve[B]; SaveSolution[B]; PostOperation[NumCur];
EndIf
      Generate[C]; Solve[C]; SaveSolution[C];
    }
  }
}

//==================================================================================================
// POSTPRO / POSTOP
//==================================================================================================

PostProcessing {
  { Name NumNormal; NameOfFormulation NumNormal;
    Quantity {
      { Name u_normal; Value { Local { [ Vector[{u_nx},{u_ny},{u_nz}] / Sqrt[{u_nx}*{u_nx}+{u_ny}*{u_ny}+{u_nz}*{u_nz}] ]; In Region[{FacesAll,BndSca}]; Jacobian JSur; }}}
    }
  }
  { Name NumCur; NameOfFormulation NumCur;
    Quantity {
      { Name u_cur; Value { Local { [ {u_cur} ]; In Region[{FacesAll,BndSca}]; Jacobian JSur; }}}
    }
  }
  { Name NumSol; NameOfFormulation NumSol;
    Quantity {
      { Name u_ref~{BND_TYPE}~{CRN_TYPE}~{nPade}~{thetaPadeInput}; Value { Local { [ f_ref[] ];              In Dom; Jacobian JVol; }}}
      { Name u_num~{BND_TYPE}~{CRN_TYPE}~{nPade}~{thetaPadeInput}; Value { Local { [ {u_num} ];              In Dom; Jacobian JVol; }}}
      { Name u_err~{BND_TYPE}~{CRN_TYPE}~{nPade}~{thetaPadeInput}; Value { Local { [ Norm[f_ref[]-{u_num}]]; In Dom; Jacobian JVol; }}}
    }
  }
  { Name Error; NameOfFormulation NumSol;
    Quantity {
      { Name error2;   Value { Integral { [ Abs[f_ref[]-{u_num}]^2 ];               In Dom; Jacobian JVol; Integration I1; }}}
      { Name energy2;  Value { Integral { [ Norm[f_ref[]]^2 ];                      In Dom; Jacobian JVol; Integration I1; }}}
      { Name errorAbs; Value { Term { Type Global; [Sqrt[$error2Var]] ;             In Dom; Jacobian JVol; }}}
      { Name errorRel; Value { Term { Type Global; [Sqrt[$error2Var/$energy2Var]] ; In Dom; Jacobian JVol; }}}
    }
  }
}

PostOperation{
  { Name NumNormal; NameOfPostProcessing NumNormal;
    Operation {
      Print [u_normal, OnElementsOf Region[{FacesAll,BndSca}], StoreInField (1001), File "out/u_normal.pos"];
    }
  }
  { Name NumCur; NameOfPostProcessing NumCur;
    Operation {
      Print [u_cur, OnElementsOf Region[{FacesAll,BndSca}], StoreInField (1002), File "out/u_cur.pos"];
    }
  }
  { Name NumSol; NameOfPostProcessing NumSol;
    Operation {
      tmp1 = Sprintf("out/solRef_cube_%g_%g_%g_%g_%g.pos", FLAG_SIGNAL_BC, BND_TYPE, CRN_TYPE, nPade, thetaPadeInput);
      tmp2 = Sprintf("out/solNum_cube_%g_%g_%g_%g_%g.pos", FLAG_SIGNAL_BC, BND_TYPE, CRN_TYPE, nPade, thetaPadeInput);
      tmp3 = Sprintf("out/solErr_cube_%g_%g_%g_%g_%g.pos", FLAG_SIGNAL_BC, BND_TYPE, CRN_TYPE, nPade, thetaPadeInput);
      Print [u_ref~{BND_TYPE}~{CRN_TYPE}~{nPade}~{thetaPadeInput}, OnElementsOf Dom, File tmp1];
      Print [u_num~{BND_TYPE}~{CRN_TYPE}~{nPade}~{thetaPadeInput}, OnElementsOf Dom, File tmp2];
      Print [u_err~{BND_TYPE}~{CRN_TYPE}~{nPade}~{thetaPadeInput}, OnElementsOf Dom, File tmp3];
    }
  }
  { Name Error; NameOfPostProcessing Error;
    Operation {
      tmp4 = Sprintf("out/errorAbs_cube_%g_%g_%g_%g_%g.dat", FLAG_SIGNAL_BC, BND_TYPE, CRN_TYPE, nPade, thetaPadeInput);
      tmp5 = Sprintf("out/errorRel_cube_%g_%g_%g_%g_%g.dat", FLAG_SIGNAL_BC, BND_TYPE, CRN_TYPE, nPade, thetaPadeInput);
      Print [error2[Dom],  OnRegion Dom, Format Table, StoreInVariable $error2Var];
      Print [energy2[Dom], OnRegion Dom, Format Table, StoreInVariable $energy2Var];
      Print [errorAbs,     OnRegion Dom, Format Table, SendToServer "Output/1L2-Error (absolute)", File > tmp4];
      Print [errorRel,     OnRegion Dom, Format Table, SendToServer "Output/2L2-Error (relative)", File > tmp5];
    }
  }
}

DefineConstant[
  R_ = {"NumSol", Name "GetDP/1ResolutionChoices",    Visible 1, Choices {"NumNormal", "NumCur", "NumSol"} },
  P_ = {"NumSol", Name "GetDP/2PostOperationChoices", Visible 1, Choices {"NumNormal", "NumCur", "NumSol", "Error"}}
];
