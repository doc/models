Include "domCube.dat";

/// SPHERE

Point(100) = {     0,     0,     0};
Point(101) = {-R_SCA,     0,     0};
Point(102) = { R_SCA,     0,     0};
Point(103) = {     0,-R_SCA,     0};
Point(104) = {     0, R_SCA,     0};
Point(105) = {     0,     0,-R_SCA};
Point(106) = {     0,     0, R_SCA};

Circle(21) = {102,100,104};
Circle(22) = {104,100,101};
Circle(23) = {101,100,103};
Circle(24) = {103,100,102};
Circle(25) = {104,100,105};
Circle(26) = {105,100,103};
Circle(27) = {103,100,106};
Circle(28) = {106,100,104};
Circle(29) = {102,100,106};
Circle(30) = {106,100,101};
Circle(31) = {101,100,105};
Circle(32) = {105,100,102};

Line Loop(41) = { 22, 28,-30};
Line Loop(42) = { 30, 23, 27};
Line Loop(43) = {-28,-29, 21};
Line Loop(44) = {-31,-22, 25};
Line Loop(45) = {-25,-32,-21};
Line Loop(46) = {-23, 31, 26};
Line Loop(47) = {-27, 24, 29};
Line Loop(48) = {-24, 32,-26};
Surface(41) = {41};
Surface(42) = {42};
Surface(43) = {43};
Surface(44) = {44};
Surface(45) = {45};
Surface(46) = {46};
Surface(47) = {47};
Surface(48) = {48};

/// CUBE

X_SCA = dimL/2;
Y_SCA = dimL/2;
Z_SCA = dimL/2;
Point(1) = {     -X_SCA,     -Y_SCA,     -Z_SCA};
Point(2) = { dimL-X_SCA,     -Y_SCA,     -Z_SCA};
Point(4) = {     -X_SCA,     -Y_SCA, dimL-Z_SCA};
Point(3) = { dimL-X_SCA,     -Y_SCA, dimL-Z_SCA};
Point(5) = {     -X_SCA, dimL-Y_SCA, dimL-Z_SCA};
Point(6) = { dimL-X_SCA, dimL-Y_SCA, dimL-Z_SCA};
Point(7) = {     -X_SCA, dimL-Y_SCA,     -Z_SCA};
Point(8) = { dimL-X_SCA, dimL-Y_SCA,     -Z_SCA};

Line(1) = {1, 2};
Line(2) = {4, 3};
Line(3) = {5, 6};
Line(4) = {7, 8};
Line(5) = {1, 7};
Line(6) = {2, 8};
Line(7) = {3, 6};
Line(8) = {4, 5};
Line(9) = {1, 4};
Line(10) = {7, 5};
Line(11) = {8, 6};
Line(12) = {2, 3};

Line Loop(1) = {9, 8, -10, -5};
Line Loop(2) = {6, 11, -7, -12};
Line Loop(3) = {1, 12, -2, -9};
Line Loop(4) = {10, 3, -11, -4};
Line Loop(5) = {5, 4, -6, -1};
Line Loop(6) = {2, 7, -3, -8};
Plane Surface(1) = {1};
Plane Surface(2) = {2};
Plane Surface(3) = {3};
Plane Surface(4) = {4};
Plane Surface(5) = {5};
Plane Surface(6) = {6};

/// VOLUME

Surface Loop(1) = {1, 2, 3, 4, 5, 6, -41, -42, -43, -44, -45, -46, -47, -48};
Volume(1) = {1};

/// PHYSICAL TAGS

Physical Surface(SUR_Scatt) = {-41, -42, -43, -44, -45, -46, -47, -48};

Physical Point(PNT_1_1_1) = {1};
Physical Point(PNT_2_1_1) = {2};
Physical Point(PNT_2_1_2) = {3};
Physical Point(PNT_1_1_2) = {4};
Physical Point(PNT_1_2_2) = {5};
Physical Point(PNT_2_2_2) = {6};
Physical Point(PNT_1_2_1) = {7};
Physical Point(PNT_2_2_1) = {8};

Physical Line(LIN_0_1_1) = {1};
Physical Line(LIN_0_1_2) = {2};
Physical Line(LIN_0_2_2) = {3};
Physical Line(LIN_0_2_1) = {4};
Physical Line(LIN_1_0_1) = {5};
Physical Line(LIN_2_0_1) = {6};
Physical Line(LIN_2_0_2) = {7};
Physical Line(LIN_1_0_2) = {8};
Physical Line(LIN_1_1_0) = {9};
Physical Line(LIN_1_2_0) = {10};
Physical Line(LIN_2_2_0) = {11};
Physical Line(LIN_2_1_0) = {12};

Physical Surface(SUR_1_0_0) = {1};
Physical Surface(SUR_2_0_0) = {2};
Physical Surface(SUR_0_1_0) = {3};
Physical Surface(SUR_0_2_0) = {4};
Physical Surface(SUR_0_0_1) = {5};
Physical Surface(SUR_0_0_2) = {6};

Physical Volume(VOL) = {1};
