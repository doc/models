Include "domSquare.dat";

//==================================================================================================
// OPTIONS and PARAMETERS
//==================================================================================================

CRN_Regularization = 0;
CRN_Compatibility  = 1;
KEPS_Nothing       = 0;
KEPS_Analytic      = 1;
KEPS_Numeric       = 2;

DefineConstant[
  CRN_TYPE = {CRN_Compatibility,
    Name "Input/5Model/03Boundary condition (corners)", Highlight "Red",
    Visible ((BND_TYPE == BND_Second) || (BND_TYPE == BND_Pade) || (BND_TYPE == BND_CRBC)),
    Choices {CRN_Regularization = "Regularization",
             CRN_Compatibility  = "Compatibility"}},
  nPade = {4, Choices {0, 1, 2, 3, 4, 5, 6},
    Name "Input/5Model/05Pade: Number of fields",
    Visible ((BND_TYPE == BND_Pade) || (BND_TYPE == BND_CRBC))},
  thetaPadeInput = {3, Choices {0, 1, 2, 3, 4},
    Name "Input/5Model/06Pade: Rotation of branch cut",
    Visible ((BND_TYPE == BND_Pade) || (BND_TYPE == BND_CRBC))},
  KEPS_TYPE = {KEPS_Numeric,
    Name "Input/5Model/07Curvature for regularization", Highlight "Red",
    Visible (((BND_TYPE == BND_Pade) || (BND_TYPE == BND_CRBC)) && (CRN_TYPE == CRN_Regularization)),
    Choices {KEPS_Nothing  = "Nothing",
             KEPS_Analytic = "Analytic formula",
             KEPS_Numeric  = "Numerical curvature"}}
];

If((BND_TYPE == BND_Pade) || (BND_TYPE == BND_CRBC))
  If(thetaPadeInput == 0)
    thetaPade = 0;
  EndIf
  If(thetaPadeInput == 1)
    thetaPade = Pi/8;
  EndIf
  If(thetaPadeInput == 2)
    thetaPade = Pi/4;
  EndIf
  If(thetaPadeInput == 3)
    thetaPade = Pi/3;
  EndIf
  If(thetaPadeInput == 4)
    thetaPade = Pi/2;
  EndIf
  mPade = 2*nPade+1;
  For j In{1:nPade}
    cPade~{j} = Tan[j*Pi/mPade]^2;
  EndFor
Else
  nPade = 0;
  thetaPadeInput = 0;
EndIf
If(BND_TYPE == BND_PML)
  nPade = Npml;
EndIf

Group {
  Dom    = Region[{DOM}];
If(BND_TYPE == BND_PML)
  DomPml = Region[{DOM_PML}];
  BndPml = Region[{BND_PmlExt}];
  DomPmlAll = Region[{DomPml,BndPml}];
Else
  DomPml = Region[{}];
  BndPml = Region[{}];
  DomPmlAll = Region[{}];
EndIf
  BndSca = Region[{BND_Scatt}];
  For iEdg In{1:4}
    iCrn1 = (iEdg == 1) ? 4 : iEdg-1;
    iCrn2 = iEdg;
    Edg~{iEdg} = Region[{BND~{iEdg}}];
    EdgClo~{iEdg} = Region[{BND~{iEdg},CRN~{iCrn1},CRN~{iCrn2}}];
  EndFor
  For iCrn In{1:4}
    Crn~{iCrn} = Region[{CRN~{iCrn}}];
  EndFor

  CrnAll = Region[{CRN~{1},CRN~{2},CRN~{3},CRN~{4}}];
  EdgAll = Region[{BND~{1},BND~{2},BND~{3},BND~{4}}];
  DomAll = Region[{Dom,BndSca,EdgAll,CrnAll}];
  BndExt = Region[{EdgAll}];
}

Function {

  For iEdg In{1:4}
    NormalGeo[Edg~{iEdg}] = Vector[Cos[(iEdg+1)*Pi/2], Sin[(iEdg+1)*Pi/2], 0.];
  EndFor

  NormalNum[] = VectorField[XYZ[]]{1001};
  CurvNum[]   = ScalarField[XYZ[]]{1002};

If(((BND_TYPE == BND_Pade) || (BND_TYPE == BND_CRBC)) && (CRN_TYPE == CRN_Regularization) && (KEPS_TYPE == KEPS_Analytic))
  KAPPA = (2)^(1/2)/LC;
  dist~{1}[] = Sqrt[(X[]-X~{1})^2 + (Y[]-Y~{1})^2];
  dist~{2}[] = Sqrt[(X[]-X~{2})^2 + (Y[]-Y~{2})^2];
  dist~{3}[] = Sqrt[(X[]-X~{3})^2 + (Y[]-Y~{3})^2];
  dist~{4}[] = Sqrt[(X[]-X~{4})^2 + (Y[]-Y~{4})^2];
  Curv[EdgAll] = ((dist~{1}[] < LC)||(dist~{2}[] < LC)||(dist~{3}[] < LC)||(dist~{4}[] < LC)) ? KAPPA : 0;
  kEps[] = WAVENUMBER + I[] * 0.39 * WAVENUMBER^(1/3) * (Curv[])^(2/3);
ElseIf(((BND_TYPE == BND_Pade) || (BND_TYPE == BND_CRBC)) && (CRN_TYPE == CRN_Regularization) && (KEPS_TYPE == KEPS_Numeric))
  kEps[] = WAVENUMBER + I[] * 0.39 * WAVENUMBER^(1/3) * (CurvNum[])^(2/3);
Else
  Curv[EdgAll] = 0.;
  kEps[EdgAll] = WAVENUMBER;
EndIf

If(BND_TYPE == BND_Pade)
  ExpPTheta[]  = Complex[Cos[ thetaPade],Sin[ thetaPade]];
  ExpMTheta[]  = Complex[Cos[-thetaPade],Sin[-thetaPade]];
  ExpPTheta2[] = Complex[Cos[thetaPade/2.],Sin[thetaPade/2.]];
  For i In{1:nPade}
  For j In{1:nPade}
    coefA~{i}~{j}[] = 2./mPade * cPade~{j} * (cPade~{i}-1+ExpMTheta[]) / (cPade~{i}+cPade~{j}+ExpMTheta[]);
    coefB~{i}~{j}[] = 2./mPade * cPade~{j} * (-1-cPade~{i}) / (cPade~{i}+cPade~{j}+ExpMTheta[]);
  EndFor
  EndFor
EndIf

If(BND_TYPE == BND_CRBC)
  For n In{0:nPade}
    Alpha~{n}[] = Complex[Cos[thetaPade/2.], Sin[thetaPade/2.]];
  EndFor
EndIf

If(BND_TYPE == BND_PML)
  xLoc[DomPml] = Fabs[X[]+X_SCA-dimL/2]-dimL/2;
  yLoc[DomPml] = Fabs[Y[]+Y_SCA-dimL/2]-dimL/2;
  //absFuncX[DomPml] = (xLoc[]>=0) ? sigmaPml*(Lpml-xLoc[])*(Lpml-xLoc[]) : 0;
  //absFuncY[DomPml] = (yLoc[]>=0) ? sigmaPml*(Lpml-yLoc[])*(Lpml-yLoc[]) : 0;
  absFuncX[DomPml] = (xLoc[]>=0) ? 1/(Lpml-xLoc[]) : 0;
  absFuncY[DomPml] = (yLoc[]>=0) ? 1/(Lpml-yLoc[]) : 0;
  If(rotPml < 91)
    rot[DomPml] = Complex[Sin[rotPml*Pi/180.], Cos[rotPml*Pi/180.]]; // I (rotPml=0, prop) - 1 (rotPml=Pi/2, evan)
  Else
    rot[DomPml] = Complex[1., 1.];
  EndIf
  hx[DomPml] = 1 + rot[] * absFuncX[]/k[];
  hy[DomPml] = 1 + rot[] * absFuncY[]/k[];
  pmlScal[DomPml] = hx[]*hy[];
  pmlTens[DomPml] = TensorDiag[hy[]/hx[], hx[]/hy[], 0];
EndIf
}

//==================================================================================================
// FONCTION SPACES with CONSTRAINTS
//==================================================================================================

If((FLAG_SIGNAL_BC == SIGNAL_Dirichlet) || (BND_TYPE == BND_PML))
Constraint {
  { Name DirichletBC; Case {
If(FLAG_SIGNAL_BC == SIGNAL_Dirichlet)
    { Region BndSca; Value -f_inc[]; }
EndIf
  }}
}
EndIf

FunctionSpace {
  { Name H_nx;  Type Form0; BasisFunction {{ Name si; NameOfCoef pi; Function BF_Node; Support Region[{BndExt}]; Entity NodesOf[All]; }}}
  { Name H_ny;  Type Form0; BasisFunction {{ Name si; NameOfCoef pi; Function BF_Node; Support Region[{BndExt}]; Entity NodesOf[All]; }}}
  { Name H_cur; Type Form0; BasisFunction {{ Name si; NameOfCoef pi; Function BF_Node; Support Region[{BndExt}]; Entity NodesOf[All]; }}}
  { Name H_ref; Type Form0; BasisFunction {{ Name si; NameOfCoef pi; Function BF_Node; Support Region[{DomAll}]; Entity NodesOf[All]; }}
If(FLAG_SIGNAL_BC == SIGNAL_Dirichlet)
    Constraint {{ NameOfCoef pi; EntityType NodesOf; NameOfConstraint DirichletBC; }}
EndIf
  }
  { Name H_num; Type Form0; BasisFunction {{ Name si; NameOfCoef pi; Function BF_Node; Support Region[{DomAll,DomPmlAll}]; Entity NodesOf[All]; }}
If((FLAG_SIGNAL_BC == SIGNAL_Dirichlet) || (BND_TYPE == BND_PML))
    Constraint {{ NameOfCoef pi; EntityType NodesOf; NameOfConstraint DirichletBC; }}
EndIf
  }
If((BND_TYPE == BND_Pade) || (BND_TYPE == BND_CRBC))
If(CRN_TYPE == CRN_Regularization)
  For i In {1:nPade}
  { Name H~{i}; Type Form0; BasisFunction {{ Name sn; NameOfCoef un; Function BF_Node; Support Region[{EdgAll}]; Entity NodesOf[All]; }}}
  EndFor
EndIf
If(CRN_TYPE == CRN_Compatibility)
  For iEdg In{1:4}
  For i In {1:nPade}
  { Name H~{iEdg}~{i}; Type Form0;BasisFunction {{ Name sn; NameOfCoef un; Function BF_Node; Support Region[{EdgClo~{iEdg}}]; Entity NodesOf[All]; }}}
  EndFor
  EndFor
  For iCrn In{1:4}
  For i In {1:nPade}
  For j In {1:nPade}
  { Name H~{iCrn}~{i}~{j}; Type Form0; BasisFunction {{ Name sn; NameOfCoef un; Function BF_Node; Support Region[{Crn~{iCrn}}]; Entity NodesOf[All]; }}}
  EndFor
  EndFor
  EndFor
EndIf
EndIf
}

//==================================================================================================
// FORMULATIONS
//==================================================================================================

Formulation {
  { Name NumNormal; Type FemEquation;
    Quantity {
      { Name u_nx; Type Local; NameOfSpace H_nx; }
      { Name u_ny; Type Local; NameOfSpace H_ny; }
    }
    Equation {
      Galerkin{ [ Dof{u_nx}           , {u_nx} ]; In Region[{BndExt}]; Jacobian JSur; Integration I1; }
      Galerkin{ [ Dof{u_ny}           , {u_ny} ]; In Region[{BndExt}]; Jacobian JSur; Integration I1; }
      Galerkin{ [ -CompX[NormalGeo[]] , {u_nx} ]; In Region[{BndExt}]; Jacobian JSur; Integration I1; }
      Galerkin{ [ -CompY[NormalGeo[]] , {u_ny} ]; In Region[{BndExt}]; Jacobian JSur; Integration I1; }
    }
  }

  { Name NumCur; Type FemEquation;
    Quantity {
      { Name u_nx; Type Local; NameOfSpace H_nx; }
      { Name u_ny; Type Local; NameOfSpace H_ny; }
      { Name u_cur; Type Local; NameOfSpace H_cur; }
    }
    Equation {
      Galerkin{ [ Dof{u_nx}           , {u_nx} ]; In Region[{BndExt}]; Jacobian JSur; Integration I1; }
      Galerkin{ [ Dof{u_ny}           , {u_ny} ]; In Region[{BndExt}]; Jacobian JSur; Integration I1; }
      Galerkin{ [ -CompX[NormalNum[]] , {u_nx} ]; In Region[{BndExt}]; Jacobian JSur; Integration I1; }
      Galerkin{ [ -CompY[NormalNum[]] , {u_ny} ]; In Region[{BndExt}]; Jacobian JSur; Integration I1; }

      Galerkin{ [ Dof{u_cur}                   , {u_cur} ]; In Region[{BndExt}]; Jacobian JSur; Integration I1; }
      Galerkin{ [ -Vector[1,0,0] * Dof{d u_nx} , {u_cur} ]; In Region[{BndExt}]; Jacobian JSur; Integration I1; }
      Galerkin{ [ -Vector[0,1,0] * Dof{d u_ny} , {u_cur} ]; In Region[{BndExt}]; Jacobian JSur; Integration I1; }
    }
  }

  { Name NumSol; Type FemEquation;
    Quantity {
      { Name u_num; Type Local; NameOfSpace H_num; }
If((BND_TYPE == BND_Pade) || (BND_TYPE == BND_CRBC))
If(CRN_TYPE == CRN_Regularization)
    For i In {1:nPade}
      { Name u~{i}; Type Local; NameOfSpace H~{i}; }
    EndFor
EndIf
If(CRN_TYPE == CRN_Compatibility)
    For iEdg In{1:4}
    For i In {1:nPade}
      { Name u~{iEdg}~{i}; Type Local; NameOfSpace H~{iEdg}~{i}; }
    EndFor
    EndFor
    For iCrn In{1:4}
    For i In {1:nPade}
    For j In {1:nPade}
      { Name u~{iCrn}~{i}~{j}; Type Local; NameOfSpace H~{iCrn}~{i}~{j}; }
    EndFor
    EndFor
    EndFor
EndIf
EndIf
    }
    Equation {

// Helmholtz

      Galerkin{ [ Dof{d u_num}    , {d u_num} ]; In Dom;    Jacobian JVol; Integration I1; }
      Galerkin{ [ -k[]^2*Dof{u_num} , {u_num} ]; In Dom;    Jacobian JVol; Integration I1; }
If(FLAG_SIGNAL_BC == SIGNAL_Neumann)
      Galerkin{ [ -df_inc[]         , {u_num} ]; In BndSca; Jacobian JSur; Integration I1; }
EndIf
If(BND_TYPE == BND_PML)
      Galerkin{ [ pmlTens[] * Dof{d u_num}      , {d u_num} ]; In DomPml; Jacobian JVol; Integration I1; }
      Galerkin{ [ -k[]^2 * pmlScal[] * Dof{u_num} , {u_num} ]; In DomPml; Jacobian JVol; Integration I1; }
//      Galerkin{ [ -I[]*k[]*Dof{u_num} , {u_num} ]; In BndPml; Jacobian JSur; Integration I1; }
EndIf

// Sommerfeld ABC

If(BND_TYPE == BND_Sommerfeld)
      Galerkin{ [ -I[]*k[]*Dof{u_num} , {u_num} ]; In EdgAll; Jacobian JSur; Integration I1; }
EndIf

// Second-order ABC

If(BND_TYPE == BND_Second)
      Galerkin { [ - I[]*k[] * Dof{u_num}              , {u_num} ]; In EdgAll; Jacobian JSur; Integration I1; }
      Galerkin { [ - 1/(2*I[]*kEps[]) * Dof{d u_num} , {d u_num} ]; In EdgAll; Jacobian JSur; Integration I1; }
If(CRN_TYPE == CRN_Compatibility)
      Galerkin { [ 3./4. * Dof{u_num}                  , {u_num} ]; In CrnAll; Jacobian JLin; Integration I1; }
EndIf
EndIf

// HABC (continuity at corners)

If((BND_TYPE == BND_Pade) && (CRN_TYPE == CRN_Regularization))
      Galerkin { [ -I[]*k[]*ExpPTheta2[] * Dof{u_num} , {u_num} ]; In EdgAll; Jacobian JSur; Integration I1; }
    For i In{1:nPade}
      Galerkin { [ -I[]*k[]*ExpPTheta2[] * 2./mPade * cPade~{i} * Dof{u~{i}} , {u_num} ]; In EdgAll; Jacobian JSur; Integration I1; }
      Galerkin { [ -I[]*k[]*ExpPTheta2[] * 2./mPade * cPade~{i} * Dof{u_num} , {u_num} ]; In EdgAll; Jacobian JSur; Integration I1; }

      Galerkin { [ Dof{d u~{i}}                                   , {d u~{i}} ]; In EdgAll; Jacobian JSur; Integration I1; }
      Galerkin { [ -kEps[]^2*(ExpPTheta[]*cPade~{i}+1) * Dof{u~{i}} , {u~{i}} ]; In EdgAll; Jacobian JSur; Integration I1; }
      Galerkin { [ -kEps[]^2*ExpPTheta[]*(cPade~{i}+1) * Dof{u_num} , {u~{i}} ]; In EdgAll; Jacobian JSur; Integration I1; }
    EndFor
EndIf

// HABC (discontinuity at corners)

If((BND_TYPE == BND_Pade) && (CRN_TYPE == CRN_Compatibility))

    For iEdg In{1:4}
      Galerkin { [ -I[]*k[]*ExpPTheta2[] * Dof{u_num} , {u_num} ]; In Edg~{iEdg}; Jacobian JSur; Integration I1; }
    For i In{1:nPade}
      Galerkin { [ -I[]*k[]*ExpPTheta2[] * 2./mPade * cPade~{i} * Dof{u~{iEdg}~{i}} , {u_num} ]; In Edg~{iEdg}; Jacobian JSur; Integration I1; }
      Galerkin { [ -I[]*k[]*ExpPTheta2[] * 2./mPade * cPade~{i} * Dof{u_num}        , {u_num} ]; In Edg~{iEdg}; Jacobian JSur; Integration I1; }

      Galerkin { [ Dof{d u~{iEdg}~{i}}                                   , {d u~{iEdg}~{i}} ]; In Edg~{iEdg}; Jacobian JSur; Integration I1; }
      Galerkin { [ -kEps[]^2*(ExpPTheta[]*cPade~{i}+1) * Dof{u~{iEdg}~{i}} , {u~{iEdg}~{i}} ]; In Edg~{iEdg}; Jacobian JSur; Integration I1; }
      Galerkin { [ -kEps[]^2*ExpPTheta[]*(cPade~{i}+1) * Dof{u_num}        , {u~{iEdg}~{i}} ]; In Edg~{iEdg}; Jacobian JSur; Integration I1; }
    EndFor
    EndFor

    For iCrn In{1:4}
      iEdg1 = iCrn;
      iEdg2 = (iCrn == 4) ? 1 : iCrn+1;
    For i In{1:nPade}
      Galerkin { [ -I[]*k[]*ExpPTheta2[] * Dof{u~{iEdg1}~{i}} , {u~{iEdg1}~{i}} ]; In Crn~{iCrn}; Jacobian JLin; Integration I1; }
      Galerkin { [ -I[]*k[]*ExpPTheta2[] * Dof{u~{iEdg2}~{i}} , {u~{iEdg2}~{i}} ]; In Crn~{iCrn}; Jacobian JLin; Integration I1; }
    For j In{1:nPade}
      Galerkin { [ -I[]*k[]*ExpPTheta2[] * 2./mPade * cPade~{j} * Dof{u~{iEdg1}~{i}}    , {u~{iEdg1}~{i}} ]; In Crn~{iCrn}; Jacobian JLin; Integration I1; }
      Galerkin { [ -I[]*k[]*ExpPTheta2[] * 2./mPade * cPade~{j} * Dof{u~{iEdg2}~{i}}    , {u~{iEdg2}~{i}} ]; In Crn~{iCrn}; Jacobian JLin; Integration I1; }
      Galerkin { [ -I[]*k[]*ExpPTheta2[] * 2./mPade * cPade~{j} * Dof{u~{iCrn}~{i}~{j}} , {u~{iEdg1}~{i}} ]; In Crn~{iCrn}; Jacobian JLin; Integration I1; }
      Galerkin { [ -I[]*k[]*ExpPTheta2[] * 2./mPade * cPade~{j} * Dof{u~{iCrn}~{j}~{i}} , {u~{iEdg2}~{i}} ]; In Crn~{iCrn}; Jacobian JLin; Integration I1; }

      Galerkin { [ (cPade~{i}+cPade~{j}+ExpMTheta[]) * Dof{u~{iCrn}~{i}~{j}} , {u~{iCrn}~{i}~{j}} ]; In Crn~{iCrn}; Jacobian JLin; Integration I1; }
      Galerkin { [ (cPade~{j}+1) * Dof{u~{iEdg1}~{i}}                        , {u~{iCrn}~{i}~{j}} ]; In Crn~{iCrn}; Jacobian JLin; Integration I1; }
      Galerkin { [ (cPade~{i}+1) * Dof{u~{iEdg2}~{j}}                        , {u~{iCrn}~{i}~{j}} ]; In Crn~{iCrn}; Jacobian JLin; Integration I1; }
    EndFor

//    For j In{1:nPade} // Alternative (equivalent) form
//      Galerkin { [ -I[]*k[]*ExpPTheta2[] * coefA~{i}~{j}[] * Dof{u~{iEdg1}~{i}} , {u~{iEdg1}~{i}} ]; In Crn~{iCrn}; Jacobian JLin; Integration I1; }
//      Galerkin { [ -I[]*k[]*ExpPTheta2[] * coefA~{i}~{j}[] * Dof{u~{iEdg2}~{i}} , {u~{iEdg2}~{i}} ]; In Crn~{iCrn}; Jacobian JLin; Integration I1; }
//      Galerkin { [ -I[]*k[]*ExpPTheta2[] * coefB~{i}~{j}[] * Dof{u~{iEdg2}~{j}} , {u~{iEdg1}~{i}} ]; In Crn~{iCrn}; Jacobian JLin; Integration I1; }
//      Galerkin { [ -I[]*k[]*ExpPTheta2[] * coefB~{i}~{j}[] * Dof{u~{iEdg1}~{j}} , {u~{iEdg2}~{i}} ]; In Crn~{iCrn}; Jacobian JLin; Integration I1; }
//    EndFor

    EndFor
    EndFor

EndIf

// CRBC

If((BND_TYPE == BND_CRBC) && (CRN_TYPE == CRN_Compatibility))

  For iEdg In{1:4}
      Galerkin { [ -I[]*k[] * Alpha~{nPade}[] * Dof{u_num}                       , {u_num} ]; In Edg~{iEdg}; Jacobian JSur; Integration I1; }
    For i1 In{1:nPade}
      Galerkin { [ -I[]*k[] * (Alpha~{i1}[]+Alpha~{i1-1}[]) * Dof{u_num}         , {u_num} ]; In Edg~{iEdg}; Jacobian JSur; Integration I1; }
    For i2 In{i1:nPade}
      Galerkin { [ -I[]*k[] * (Alpha~{i1}[]+Alpha~{i1-1}[]) * Dof{u~{iEdg}~{i2}} , {u_num} ]; In Edg~{iEdg}; Jacobian JSur; Integration I1; }
    EndFor
    EndFor

    For i In{1:nPade}
      Galerkin { [ Dof{d u~{iEdg}~{i}}                                                        , {d u~{iEdg}~{i}} ]; In Edg~{iEdg}; Jacobian JSur; Integration I1; }
      Galerkin { [ -kEps[]^2 * (1-Alpha~{i}[]^2) * Dof{u~{iEdg}~{i}}                            , {u~{iEdg}~{i}} ]; In Edg~{iEdg}; Jacobian JSur; Integration I1; }
    For i1 In{1:i}
      Galerkin { [ -2*kEps[]^2 * Alpha~{i}[]*(Alpha~{i1}[]+Alpha~{i1-1}[]) * Dof{u_num}         , {u~{iEdg}~{i}} ]; In Edg~{iEdg}; Jacobian JSur; Integration I1; }
    For i2 In{i1:nPade}
      Galerkin { [ -2*kEps[]^2 * Alpha~{i}[]*(Alpha~{i1}[]+Alpha~{i1-1}[]) * Dof{u~{iEdg}~{i2}} , {u~{iEdg}~{i}} ]; In Edg~{iEdg}; Jacobian JSur; Integration I1; }
    EndFor
    EndFor
    EndFor
  EndFor

  For iCrn In{1:4}
      iEdg1 = iCrn;
      iEdg2 = (iCrn == 4) ? 1 : iCrn+1;

    For i In{1:nPade}
      Galerkin { [ -I[]*k[] * Alpha~{nPade}[] * Dof{u~{iEdg1}~{i}}                 , {u~{iEdg1}~{i}} ]; In Crn~{iCrn}; Jacobian JLin; Integration I1; }
      Galerkin { [ -I[]*k[] * Alpha~{nPade}[] * Dof{u~{iEdg2}~{i}}                 , {u~{iEdg2}~{i}} ]; In Crn~{iCrn}; Jacobian JLin; Integration I1; }
    For i1 In{1:nPade}
      Galerkin { [ -I[]*k[] * (Alpha~{i1}[]+Alpha~{i1-1}[]) * Dof{u~{iEdg1}~{i}}     , {u~{iEdg1}~{i}} ]; In Crn~{iCrn}; Jacobian JLin; Integration I1; }
      Galerkin { [ -I[]*k[] * (Alpha~{i1}[]+Alpha~{i1-1}[]) * Dof{u~{iEdg2}~{i}}     , {u~{iEdg2}~{i}} ]; In Crn~{iCrn}; Jacobian JLin; Integration I1; }
    For i2 In{i1:nPade}
      Galerkin { [ -I[]*k[] * (Alpha~{i1}[]+Alpha~{i1-1}[]) * Dof{u~{iCrn}~{i}~{i2}} , {u~{iEdg1}~{i}} ]; In Crn~{iCrn}; Jacobian JLin; Integration I1; }
      Galerkin { [ -I[]*k[] * (Alpha~{i1}[]+Alpha~{i1-1}[]) * Dof{u~{iCrn}~{i2}~{i}} , {u~{iEdg2}~{i}} ]; In Crn~{iCrn}; Jacobian JLin; Integration I1; }
    EndFor
    EndFor
    EndFor

    For i In{1:nPade}
    For j In{1:nPade}
      Galerkin { [ Dof{u~{iCrn}~{i}~{j}}                      , {u~{iCrn}~{i}~{j}} ]; In Crn~{iCrn}; Jacobian JLin; Integration I1; }
      Galerkin { [ -(1-Alpha~{i}[]^2) * Dof{u~{iCrn}~{i}~{j}} , {u~{iCrn}~{i}~{j}} ]; In Crn~{iCrn}; Jacobian JLin; Integration I1; }
      Galerkin { [ -(1-Alpha~{i}[]^2) * Dof{u~{iCrn}~{j}~{i}} , {u~{iCrn}~{j}~{i}} ]; In Crn~{iCrn}; Jacobian JLin; Integration I1; }
    For i1 In{1:j}
      Galerkin { [ -2 * Alpha~{i}[]*(Alpha~{i1}[]+Alpha~{i1-1}[]) * Dof{u~{iEdg1}~{i}}     , {u~{iCrn}~{i}~{j}} ]; In Crn~{iCrn}; Jacobian JLin; Integration I1; }
      Galerkin { [ -2 * Alpha~{i}[]*(Alpha~{i1}[]+Alpha~{i1-1}[]) * Dof{u~{iEdg2}~{i}}     , {u~{iCrn}~{j}~{i}} ]; In Crn~{iCrn}; Jacobian JLin; Integration I1; }
    For i2 In{i1:nPade}
      Galerkin { [ -2 * Alpha~{i}[]*(Alpha~{i1}[]+Alpha~{i1-1}[]) * Dof{u~{iCrn}~{i}~{i2}} , {u~{iCrn}~{i}~{j}} ]; In Crn~{iCrn}; Jacobian JLin; Integration I1; }
      Galerkin { [ -2 * Alpha~{i}[]*(Alpha~{i1}[]+Alpha~{i1-1}[]) * Dof{u~{iCrn}~{i2}~{i}} , {u~{iCrn}~{j}~{i}} ]; In Crn~{iCrn}; Jacobian JLin; Integration I1; }
    EndFor
    EndFor

    EndFor
    EndFor

  EndFor

EndIf

    }
  }

  { Name ProjSol; Type FemEquation;
    Quantity {
      { Name u_refProj; Type Local; NameOfSpace H_ref; }
    }
    Equation {
      Galerkin{ [ Dof{u_refProj} , {u_refProj} ]; In Dom; Jacobian JVol; Integration I1; }
      Galerkin{ [ -f_ref[]       , {u_refProj} ]; In Dom; Jacobian JVol; Integration I1; }
    }
  }
}

//==================================================================================================
// RESOLUTION
//==================================================================================================

Resolution {
  { Name NumNormal;
    System {
      { Name A; NameOfFormulation NumNormal; Type Real; }
    }
    Operation {
      Generate[A]; Solve[A]; SaveSolution[A];
    }
  }
  { Name NumCur;
    System {
      { Name A; NameOfFormulation NumNormal; Type Real; }
      { Name B; NameOfFormulation NumCur;    Type Real; }
    }
    Operation {
      Generate[A]; Solve[A]; SaveSolution[A]; PostOperation[NumNormal];
      Generate[B]; Solve[B]; SaveSolution[B];
    }
  }
  { Name NumSol;
    System {
If(((BND_TYPE == BND_Pade) || (BND_TYPE == BND_CRBC)) && (CRN_TYPE == CRN_Regularization) && (KEPS_TYPE == KEPS_Numeric))
      { Name A; NameOfFormulation NumNormal; Type Real; }
      { Name B; NameOfFormulation NumCur;    Type Real; }
EndIf
      { Name C; NameOfFormulation NumSol; Type Complex; }
    }
    Operation {
If(((BND_TYPE == BND_Pade) || (BND_TYPE == BND_CRBC)) && (CRN_TYPE == CRN_Regularization) && (KEPS_TYPE == KEPS_Numeric))
      Generate[A]; Solve[A]; SaveSolution[A]; PostOperation[NumNormal];
      Generate[B]; Solve[B]; SaveSolution[B]; PostOperation[NumCur];
EndIf
      Generate[C]; Solve[C]; SaveSolution[C];
    }
  }
  { Name ProjSol;
    System {
      { Name A; NameOfFormulation ProjSol; Type Complex; }
    }
    Operation {
      Generate[A]; Solve[A]; SaveSolution[A];
    }
  }
}

//==================================================================================================
// POSTPRO / POSTOP
//==================================================================================================

PostProcessing {
  { Name NumNormal; NameOfFormulation NumNormal;
    Quantity {
      { Name u_normal; Value { Local { [ Vector[{u_nx},{u_ny},0] / Sqrt[{u_nx}*{u_nx}+{u_ny}*{u_ny}] ]; In Region[{BndExt}]; Jacobian JSur; }}}
    }
  }
  { Name NumCur; NameOfFormulation NumCur;
    Quantity {
      { Name u_cur; Value { Local { [ {u_cur} ]; In Region[{BndExt}]; Jacobian JSur; }}}
    }
  }
  { Name NumSol; NameOfFormulation NumSol;
    Quantity {
      { Name u_ref;                                                Value { Local { [ f_ref[] ];         In Dom; Jacobian JVol; }}}
      { Name u_num~{BND_TYPE}~{CRN_TYPE}~{nPade}~{thetaPadeInput}; Value { Local { [ {u_num} ];         In Region[{Dom,DomPml}]; Jacobian JVol; }}}
      { Name u_err~{BND_TYPE}~{CRN_TYPE}~{nPade}~{thetaPadeInput}; Value { Local { [ f_ref[]-{u_num} ]; In Dom; Jacobian JVol; }}}
    }
  }
  { Name Errors; NameOfFormulation NumSol;
    Quantity {
      { Name energy;       Value { Integral { [ Abs[f_ref[]]^2 ];                          In Dom; Jacobian JVol; Integration I1; }}}
      { Name error2;       Value { Integral { [ Abs[f_ref[]-{u_num}]^2 ];                  In Dom; Jacobian JVol; Integration I1; }}}
      { Name errorAbs;     Value { Term { Type Global; [Sqrt[$error2Var]] ;                In Dom; Jacobian JVol; }}}
      { Name errorRel;     Value { Term { Type Global; [Sqrt[$error2Var/$energyVar]] ;     In Dom; Jacobian JVol; }}}
    }
  }
  { Name ProjError; NameOfFormulation ProjSol;
    Quantity {
      { Name energy;       Value { Integral { [ Abs[f_ref[]]^2 ];                          In Dom; Jacobian JVol; Integration I1; }}}
      { Name errorProj2;   Value { Integral { [ Abs[f_ref[]-{u_refProj}]^2 ];              In Dom; Jacobian JVol; Integration I1; }}}
      { Name errorProjAbs; Value { Term { Type Global; [Sqrt[$errorProj2Var]] ;            In Dom; Jacobian JVol; }}}
      { Name errorProjRel; Value { Term { Type Global; [Sqrt[$errorProj2Var/$energyVar]] ; In Dom; Jacobian JVol; }}}
    }
  }
}

PostOperation{
  { Name NumNormal; NameOfPostProcessing NumNormal;
    Operation {
      Print [u_normal, OnElementsOf Region[{BndExt}], StoreInField (1001), Depth 10];
//      Print [u_normal, OnElementsOf Region[{BndExt}], File "out/u_normal.pos"];
    }
  }
  { Name NumCur; NameOfPostProcessing NumCur;
    Operation {
      Print [u_cur, OnElementsOf Region[{BndExt}], StoreInField (1002), Depth 10];
//      Print [u_cur, OnElementsOf Region[{BndExt}], File "out/u_cur.pos"];
    }
  }
  { Name NumSol; NameOfPostProcessing NumSol;
    Operation {
      tmp1 = Sprintf("out/solNum_%g_%g_%g_%g_%g.pos", FLAG_SIGNAL_BC, BND_TYPE, CRN_TYPE, nPade, thetaPadeInput);
      tmp2 = Sprintf("out/solErr_%g_%g_%g_%g_%g.pos", FLAG_SIGNAL_BC, BND_TYPE, CRN_TYPE, nPade, thetaPadeInput);
      Print [u_ref, OnElementsOf Dom, File "out/solRef.pos"];
      Print [u_num~{BND_TYPE}~{CRN_TYPE}~{nPade}~{thetaPadeInput}, OnElementsOf Region[{Dom,DomPml}], File tmp1];
      Print [u_err~{BND_TYPE}~{CRN_TYPE}~{nPade}~{thetaPadeInput}, OnElementsOf Dom, File tmp2];
    }
  }
  { Name Errors; NameOfPostProcessing Errors;
    Operation {
      tmp3 = Sprintf("out/errorAbs_%g_%g_%g_%g_%g.dat", FLAG_SIGNAL_BC, BND_TYPE, CRN_TYPE, nPade, thetaPadeInput);
      tmp4 = Sprintf("out/errorRel_%g_%g_%g_%g_%g.dat", FLAG_SIGNAL_BC, BND_TYPE, CRN_TYPE, nPade, thetaPadeInput);
      Print [energy[Dom],     OnRegion Dom, Format Table, StoreInVariable $energyVar];
      Print [error2[Dom],     OnRegion Dom, Format Table, StoreInVariable $error2Var];
      Print [errorAbs,        OnRegion Dom, Format Table, SendToServer "Output/1L2-Error (absolute)", File > tmp3];
      Print [errorRel,        OnRegion Dom, Format Table, SendToServer "Output/2L2-Error (relative)", File > tmp4];
    }
  }
  { Name ProjError; NameOfPostProcessing ProjError;
    Operation {
      tmp5 = Sprintf("out/errorProjAbs_%g_%g_%g_%g_%g.dat", FLAG_SIGNAL_BC, BND_TYPE, CRN_TYPE, nPade, thetaPadeInput);
      tmp6 = Sprintf("out/errorProjRel_%g_%g_%g_%g_%g.dat", FLAG_SIGNAL_BC, BND_TYPE, CRN_TYPE, nPade, thetaPadeInput);
      Print [energy[Dom],     OnRegion Dom, Format Table, StoreInVariable $energyVar];
      Print [errorProj2[Dom], OnRegion Dom, Format Table, StoreInVariable $errorProj2Var];
      Print [errorProjAbs,    OnRegion Dom, Format Table, SendToServer "Output/3L2-ErrorProj (absolute)", File > tmp5];
      Print [errorProjRel,    OnRegion Dom, Format Table, SendToServer "Output/4L2-ErrorProj (relative)", File > tmp6];
    }
  }
}

DefineConstant[
  R_ = {"NumSol", Name "GetDP/1ResolutionChoices",    Visible 1, Choices {"NumNormal", "NumCur", "NumSol"} },
  P_ = {"NumSol, Errors", Name "GetDP/2PostOperationChoices", Visible 1, Choices {"NumNormal", "NumCur", "NumSol", "Errors", "ProjError"}}
];
