Include "domPolygon.dat";

Point(0) = {0, 0, 0};
Point(1) = {-R_SCA,     0, 0};
Point(2) = {     0,-R_SCA, 0};
Point(3) = { R_SCA,     0, 0};
Point(4) = {     0, R_SCA, 0};
Circle(1) = {1, 0, 2};
Circle(2) = {2, 0, 3};
Circle(3) = {3, 0, 4};
Circle(4) = {4, 0, 1};
llScat[] = {-1, -2, -3, -4};

If(DOM_Num == 1)
  Point(11) = {-DOM_R,     0, 0};
  Point(12) = {     0,-DOM_R, 0};
  Point(13) = { DOM_R,     0, 0};
  Point(14) = {     0, DOM_R, 0};
  Circle(11) = {11, 0, 12};
  Circle(12) = {12, 0, 13};
  Circle(13) = {13, 0, 14};
  Circle(14) = {14, 0, 11};
  llBnd[] = {-11, -12, -13, -14};
Else
For iCrn In{1:DOM_Num}
  pBnd~{iCrn} = newp;
  Point(pBnd~{iCrn}) = {X~{iCrn}, Y~{iCrn}, 0};
EndFor
For iEdg In{1:DOM_Num}
  iCrn1 = (iEdg == 1) ? DOM_Num : iEdg-1;
  iCrn2 = iEdg;
  lBnd~{iEdg} = newl;
  Line(lBnd~{iEdg}) = {pBnd~{iCrn1}, pBnd~{iCrn2}};
  llBnd[] += lBnd~{iEdg};
EndFor
EndIf

Line Loop(1) = {llBnd[], llScat[]};
Plane Surface(1) = {1};

If(DOM_Num == 1)
  Physical Line(BND~{1}) = {11, 12, 13, 14};
Else
For i In{1:DOM_Num}
  Physical Point(CRN~{i}) = {pBnd~{i}};
  Physical Line(BND~{i})  = {lBnd~{i}};
EndFor
EndIf

SetOrder 1;
Mesh.ElementOrder = 1;
Mesh 1;
Save "mainCurv.msh";
SetOrder ORDER;
Mesh.ElementOrder = ORDER;

Physical Line(BND_SCA) = {1,2,3,4};
Physical Surface(DOM) = {1};

If(FLAG_REF == REF_Num)
  pExt~{1} = newp; Point(pExt~{1}) = {-L_EXT/2,-L_EXT/2, 0};
  pExt~{2} = newp; Point(pExt~{2}) = { L_EXT/2,-L_EXT/2, 0};
  pExt~{3} = newp; Point(pExt~{3}) = { L_EXT/2, L_EXT/2, 0};
  pExt~{4} = newp; Point(pExt~{4}) = {-L_EXT/2, L_EXT/2, 0};
  lExt~{1} = newl; Line(lExt~{1}) = {pExt~{4}, pExt~{1}};
  lExt~{2} = newl; Line(lExt~{2}) = {pExt~{1}, pExt~{2}};
  lExt~{3} = newl; Line(lExt~{3}) = {pExt~{2}, pExt~{3}};
  lExt~{4} = newl; Line(lExt~{4}) = {pExt~{3}, pExt~{4}};
  For i In{1:4}
    Physical Point(CRN_EXT~{i}) = {pExt~{i}};
    Physical Line(BND_EXT~{i}) = {lExt~{i}};
    llExt[] += lExt~{i};
  EndFor
  Line Loop(2) = {llBnd[], llExt[]};
  Plane Surface(2) = {2};
  Physical Surface(DOM_EXT) = {2};
EndIf
