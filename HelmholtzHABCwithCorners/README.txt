High-order absorbing boundary conditions (HABCs) with corner and edge treatment
for 2D and 3D Helmholtz problems.

Models developed by Axel Modave.


Quick start
-----------

Open `main.pro' with Gmsh.


Additional info
---------------

Used in:

  A. Modave, X. Geuzaine, X. Antoine (2020). Corner treatments for high-order
  absorbing boundary conditions in high-frequency acoustic scattering problems.
  J. Comput. Phys., 401, 109029.
  Preprint: https://hal.archives-ouvertes.fr/hal-01925160
