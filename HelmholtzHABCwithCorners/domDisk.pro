Include "domDisk.dat";

//==================================================================================================
// OPTIONS and PARAMETERS
//==================================================================================================

DefineConstant[
  nPade = {4, Choices {0, 1, 2, 3, 4, 5, 6},
    Name "Input/5Model/05Pade: Number of fields",
    Visible (BND_TYPE == BND_Pade)},
  thetaPadeInput = {3, Choices {0, 1, 2, 3, 4},
    Name "Input/5Model/06Pade: Rotation of branch cut",
    Visible (BND_TYPE == BND_Pade)}
];

If(BND_TYPE == BND_Pade)
  If(thetaPadeInput == 0)
    thetaPade = 0;
  ElseIf(thetaPadeInput == 1)
    thetaPade = Pi/8;
  ElseIf(thetaPadeInput == 2)
    thetaPade = Pi/4;
  ElseIf(thetaPadeInput == 3)
    thetaPade = Pi/3;
  ElseIf(thetaPadeInput == 4)
    thetaPade = Pi/2;
  EndIf
  mPade = 2*nPade+1;
  For j In{1:nPade}
    cPade~{j} = Tan[j*Pi/mPade]^2;
  EndFor
Else
  nPade = 0;
  thetaPadeInput = 0;
EndIf
If(BND_TYPE == BND_PML)
  nPade = Npml;
EndIf

Group {
  Dom    = Region[{DOM}];
  BndSca = Region[{BND_Scatt}];
If(BND_TYPE == BND_PML)
  DomPml = Region[{DOM_PML}];
  BndExt = Region[{BND_PmlExt}];
Else
  DomPml = Region[{}];
  BndExt = Region[{BND_Dom}];
EndIf
  DomAll = Region[{Dom,DomPml,BndSca,BndExt}];
}

Function {

If(BND_TYPE == BND_Pade)
  kEps[] = WAVENUMBER + I[] * 0.39 * WAVENUMBER^(1/3) * (1/R_DOM)^(2/3);
EndIf

If(BND_TYPE == BND_Pade)
  ExpPTheta[]  = Complex[Cos[ thetaPade],Sin[ thetaPade]];
  ExpMTheta[]  = Complex[Cos[-thetaPade],Sin[-thetaPade]];
  ExpPTheta2[] = Complex[Cos[thetaPade/2.],Sin[thetaPade/2.]];
  For i In{1:nPade}
  For j In{1:nPade}
    coefA~{i}~{j}[] = 2./mPade * cPade~{j} * (cPade~{i}-1+ExpMTheta[]) / (cPade~{i}+cPade~{j}+ExpMTheta[]);
    coefB~{i}~{j}[] = 2./mPade * cPade~{j} * (-1-cPade~{i}) / (cPade~{i}+cPade~{j}+ExpMTheta[]);
  EndFor
  EndFor
EndIf

If(BND_TYPE == BND_PML)
  rLoc[DomPml] = R[]-R_DOM;
  absFuncS[DomPml] = 1/(Lpml-rLoc[]);
  absFuncF[DomPml] = -Log[1-rLoc[]/Lpml];
  //absFuncS[DomPml] = 1/(Lpml-rLoc[]) - 1/Lpml;
  //absFuncF[DomPml] = -Log[1-rLoc[]/Lpml] - rLoc[]/Lpml;
  If(rotPml < 91)
    rot[DomPml] = Complex[Sin[rotPml*Pi/180.], Cos[rotPml*Pi/180.]]; // I (rotPml=0, prop) - 1 (rotPml=Pi/2, evan)
  Else
    rot[DomPml] = Complex[1., 1.];
  EndIf
  s1[DomPml] = 1 + rot[] * absFuncS[]/k[];
  s2[DomPml] = 1 + rot[] * (1/R[]) * absFuncF[]/k[];
  nVec[DomPml] = XYZ[]/R[];
  tVec[DomPml] = nVec[] /\ Vector[0,0,1];
  nTen[DomPml] = Tensor[ CompX[nVec[]]*CompX[nVec[]], CompX[nVec[]]*CompY[nVec[]], CompX[nVec[]]*CompZ[nVec[]],
                         CompY[nVec[]]*CompX[nVec[]], CompY[nVec[]]*CompY[nVec[]], CompY[nVec[]]*CompZ[nVec[]],
                         CompZ[nVec[]]*CompX[nVec[]], CompZ[nVec[]]*CompY[nVec[]], CompZ[nVec[]]*CompZ[nVec[]] ];
  tTen[DomPml] = Tensor[ CompX[tVec[]]*CompX[tVec[]], CompX[tVec[]]*CompY[tVec[]], CompX[tVec[]]*CompZ[tVec[]],
                         CompY[tVec[]]*CompX[tVec[]], CompY[tVec[]]*CompY[tVec[]], CompY[tVec[]]*CompZ[tVec[]],
                         CompZ[tVec[]]*CompX[tVec[]], CompZ[tVec[]]*CompY[tVec[]], CompZ[tVec[]]*CompZ[tVec[]] ];
  pmlScal[DomPml] = s1[]*s2[];
  pmlTens[DomPml] = (s2[]/s1[]) * nTen[] + (s1[]/s2[]) * tTen[];
EndIf
}

//==================================================================================================
// FONCTION SPACES with CONSTRAINTS
//==================================================================================================

If(FLAG_SIGNAL_BC == SIGNAL_Dirichlet)
Constraint {
  { Name DirichletBC; Case {
If(FLAG_SIGNAL_BC == SIGNAL_Dirichlet)
    { Region BndSca; Value -f_inc[]; }
EndIf
  }}
}
EndIf

FunctionSpace {
  { Name H_ref; Type Form0; BasisFunction {{ Name si; NameOfCoef pi; Function BF_Node; Support Region[{DomAll}]; Entity NodesOf[All]; }}
If(FLAG_SIGNAL_BC == SIGNAL_Dirichlet)
    Constraint {{ NameOfCoef pi; EntityType NodesOf; NameOfConstraint DirichletBC; }}
EndIf
  }
  { Name H_num; Type Form0; BasisFunction {{ Name si; NameOfCoef pi; Function BF_Node; Support Region[{DomAll}]; Entity NodesOf[All]; }}
If((FLAG_SIGNAL_BC == SIGNAL_Dirichlet) || (BND_TYPE == BND_PML))
    Constraint {{ NameOfCoef pi; EntityType NodesOf; NameOfConstraint DirichletBC; }}
EndIf
  }
If(BND_TYPE == BND_Pade)
  For i In {1:nPade}
  { Name H~{i}; Type Form0; BasisFunction {{ Name sn; NameOfCoef un; Function BF_Node; Support Region[{BndExt}]; Entity NodesOf[All]; }}}
  EndFor
EndIf
}

//==================================================================================================
// FORMULATIONS
//==================================================================================================

Formulation {

  { Name NumSol; Type FemEquation;
    Quantity {
      { Name u_num; Type Local; NameOfSpace H_num; }
If(BND_TYPE == BND_Pade)
    For i In {1:nPade}
      { Name u~{i}; Type Local; NameOfSpace H~{i}; }
    EndFor
EndIf
    }
    Equation {

// Helmholtz

      Galerkin{ [ Dof{d u_num}    , {d u_num} ]; In Dom;    Jacobian JVol; Integration I1; }
      Galerkin{ [ -k[]^2*Dof{u_num} , {u_num} ]; In Dom;    Jacobian JVol; Integration I1; }
If(FLAG_SIGNAL_BC == SIGNAL_Neumann)
      Galerkin{ [ -df_inc[]         , {u_num} ]; In BndSca; Jacobian JSur; Integration I1; }
EndIf
If(BND_TYPE == BND_PML)
      Galerkin{ [ pmlTens[] * Dof{d u_num}      , {d u_num} ]; In DomPml; Jacobian JVol; Integration I1; }
      Galerkin{ [ -k[]^2 * pmlScal[] * Dof{u_num} , {u_num} ]; In DomPml; Jacobian JVol; Integration I1; }
EndIf

// Sommerfeld ABC

If(BND_TYPE == BND_Sommerfeld)
      Galerkin{ [ -I[]*k[]*Dof{u_num} , {u_num} ]; In BndExt; Jacobian JSur; Integration I1; }
EndIf

// Second-order ABC

If(BND_TYPE == BND_Second)
      Galerkin { [ - I[]*k[] * Dof{u_num}           , {u_num} ]; In BndExt; Jacobian JSur; Integration I1; }
      Galerkin { [ - 1/(2*I[]*k[]) * Dof{d u_num} , {d u_num} ]; In BndExt; Jacobian JSur; Integration I1; }
EndIf

// HABC

If(BND_TYPE == BND_Pade)
      Galerkin { [ -I[]*k[]*ExpPTheta2[] * Dof{u_num} , {u_num} ]; In BndExt; Jacobian JSur; Integration I1; }
    For i In{1:nPade}
      Galerkin { [ -I[]*k[]*ExpPTheta2[] * 2./mPade * cPade~{i} * Dof{u~{i}} , {u_num} ]; In BndExt; Jacobian JSur; Integration I1; }
      Galerkin { [ -I[]*k[]*ExpPTheta2[] * 2./mPade * cPade~{i} * Dof{u_num} , {u_num} ]; In BndExt; Jacobian JSur; Integration I1; }

      Galerkin { [ Dof{d u~{i}}                                   , {d u~{i}} ]; In BndExt; Jacobian JSur; Integration I1; }
      Galerkin { [ -kEps[]^2*(ExpPTheta[]*cPade~{i}+1) * Dof{u~{i}} , {u~{i}} ]; In BndExt; Jacobian JSur; Integration I1; }
      Galerkin { [ -kEps[]^2*ExpPTheta[]*(cPade~{i}+1) * Dof{u_num} , {u~{i}} ]; In BndExt; Jacobian JSur; Integration I1; }
    EndFor
EndIf

    }
  }

  { Name ProjSol; Type FemEquation;
    Quantity {
      { Name u_refProj; Type Local; NameOfSpace H_ref; }
    }
    Equation {
      Galerkin{ [ Dof{u_refProj} , {u_refProj} ]; In Dom; Jacobian JVol; Integration I1; }
      Galerkin{ [ -f_ref[]       , {u_refProj} ]; In Dom; Jacobian JVol; Integration I1; }
    }
  }
}

//==================================================================================================
// RESOLUTION
//==================================================================================================

Resolution {
  { Name NumSol;
    System {
      { Name A; NameOfFormulation NumSol; Type Complex; }
    }
    Operation {
      Generate[A]; Solve[A]; SaveSolution[A];
    }
  }
  { Name ProjSol;
    System {
      { Name A; NameOfFormulation ProjSol; Type Complex; }
    }
    Operation {
      Generate[A]; Solve[A]; SaveSolution[A];
    }
  }
}

//==================================================================================================
// POSTPRO / POSTOP
//==================================================================================================

PostProcessing {
  { Name NumSol; NameOfFormulation NumSol;
    Quantity {
//      { Name param1; Value { Local { [ rLoc[] ]; In DomPml; Jacobian JVol; }}}
//      { Name param2; Value { Local { [ nVec[] ]; In DomPml; Jacobian JVol; }}}
//      { Name param3; Value { Local { [ tVec[] ]; In DomPml; Jacobian JVol; }}}
      { Name u_ref; Value { Local { [ f_ref[] ]; In Region[{Dom,BndSca}]; Jacobian JVol; }}}
      { Name u_num~{BND_TYPE}~{nPade}~{thetaPadeInput}; Value { Local { [ {u_num} ];         In Region[{Dom,DomPml}]; Jacobian JVol; }}}
      { Name u_err~{BND_TYPE}~{nPade}~{thetaPadeInput}; Value { Local { [ f_ref[]-{u_num} ]; In Region[{Dom,BndSca}]; Jacobian JVol; }}}
    }
  }
  { Name Errors; NameOfFormulation NumSol;
    Quantity {
      { Name energy;       Value { Integral { [ Abs[f_ref[]]^2 ];                          In Dom; Jacobian JVol; Integration I1; }}}
      { Name error2;       Value { Integral { [ Abs[f_ref[]-{u_num}]^2 ];                  In Dom; Jacobian JVol; Integration I1; }}}
      { Name errorAbs;     Value { Term { Type Global; [Sqrt[$error2Var]] ;                In Dom; Jacobian JVol; }}}
      { Name errorRel;     Value { Term { Type Global; [Sqrt[$error2Var/$energyVar]] ;     In Dom; Jacobian JVol; }}}
    }
  }
  { Name ProjError; NameOfFormulation ProjSol;
    Quantity {
      { Name energy;       Value { Integral { [ Abs[f_ref[]]^2 ];                          In Dom; Jacobian JVol; Integration I1; }}}
      { Name errorProj2;   Value { Integral { [ Abs[f_ref[]-{u_refProj}]^2 ];              In Dom; Jacobian JVol; Integration I1; }}}
      { Name errorProjAbs; Value { Term { Type Global; [Sqrt[$errorProj2Var]] ;            In Dom; Jacobian JVol; }}}
      { Name errorProjRel; Value { Term { Type Global; [Sqrt[$errorProj2Var/$energyVar]] ; In Dom; Jacobian JVol; }}}
    }
  }
  { Name DtNError; NameOfFormulation NumSol;
    Quantity {
      { Name energyDtN;       Value { Integral { [ Abs[f_ref[]]^2 ];                            In BndSca; Jacobian JLin; Integration I1; }}}
      { Name errorDtN2;       Value { Integral { [ Abs[f_ref[]-{u_num}]^2 ];                    In BndSca; Jacobian JLin; Integration I1; }}}
      { Name normDtN;         Value { Term { Type Global; [Sqrt[$energyDtNVar]] ;               In BndSca; Jacobian JLin; }}}
      { Name errorDtNAbs;     Value { Term { Type Global; [Sqrt[$errorDtN2Var]] ;               In BndSca; Jacobian JLin; }}}
      { Name errorDtNRel;     Value { Term { Type Global; [Sqrt[$errorDtN2Var/$energyDtNVar]] ; In BndSca; Jacobian JLin; }}}
    }
  }
}

PostOperation{
  { Name NumSol; NameOfPostProcessing NumSol;
    Operation {
//      Print [param1, OnElementsOf DomPml, File "out/param1.pos"];
//      Print [param2, OnElementsOf DomPml, File "out/param2.pos"];
//      Print [param3, OnElementsOf DomPml, File "out/param3.pos"];
      tmp1 = Sprintf("out/solNum_%g_%g_%g_%g.pos", FLAG_SIGNAL_BC, BND_TYPE, nPade, thetaPadeInput);
      tmp2 = Sprintf("out/solErr_%g_%g_%g_%g.pos", FLAG_SIGNAL_BC, BND_TYPE, nPade, thetaPadeInput);
      Print [u_ref, OnElementsOf Region[{DOM,BND_Scatt}], File "out/solRef.pos"];
      Print [u_num~{BND_TYPE}~{nPade}~{thetaPadeInput}, OnElementsOf Region[{Dom,DomPml}], File tmp1];
      Print [u_err~{BND_TYPE}~{nPade}~{thetaPadeInput}, OnElementsOf Region[{Dom,BndSca}], File tmp2];
    }
  }
  { Name Errors; NameOfPostProcessing Errors;
    Operation {
      tmp3 = Sprintf("out/errorAbs_%g_%g_%g_%g.dat", FLAG_SIGNAL_BC, BND_TYPE, nPade, thetaPadeInput);
      tmp4 = Sprintf("out/errorRel_%g_%g_%g_%g.dat", FLAG_SIGNAL_BC, BND_TYPE, nPade, thetaPadeInput);
      Print [energy[Dom], OnRegion Dom, Format Table, StoreInVariable $energyVar];
      Print [error2[Dom], OnRegion Dom, Format Table, StoreInVariable $error2Var];
      Print [errorAbs,    OnRegion Dom, Format Table, SendToServer "Output/1L2-Error (absolute)", File > tmp3];
      Print [errorRel,    OnRegion Dom, Format Table, SendToServer "Output/2L2-Error (relative)", File > tmp4];
    }
  }
  { Name ProjError; NameOfPostProcessing ProjError;
    Operation {
      tmp5 = Sprintf("out/errorProjAbs_%g_%g_%g_%g.dat", FLAG_SIGNAL_BC, BND_TYPE, nPade, thetaPadeInput);
      tmp6 = Sprintf("out/errorProjRel_%g_%g_%g_%g.dat", FLAG_SIGNAL_BC, BND_TYPE, nPade, thetaPadeInput);
      Print [energy[Dom],     OnRegion Dom, Format Table, StoreInVariable $energyVar];
      Print [errorProj2[Dom], OnRegion Dom, Format Table, StoreInVariable $errorProj2Var];
      Print [errorProjAbs,    OnRegion Dom, Format Table, SendToServer "Output/3L2-ErrorProj (absolute)", File > tmp5];
      Print [errorProjRel,    OnRegion Dom, Format Table, SendToServer "Output/4L2-ErrorProj (relative)", File > tmp6];
    }
  }
  { Name DtNError; NameOfPostProcessing DtNError;
    Operation {
      tmp7 = Sprintf("out/normDtNAbs_%g_%g_%g_%g.dat",  FLAG_SIGNAL_BC, BND_TYPE, nPade, thetaPadeInput);
      tmp8 = Sprintf("out/errorDtNAbs_%g_%g_%g_%g.dat", FLAG_SIGNAL_BC, BND_TYPE, nPade, thetaPadeInput);
      tmp9 = Sprintf("out/errorDtNRel_%g_%g_%g_%g.dat", FLAG_SIGNAL_BC, BND_TYPE, nPade, thetaPadeInput);
      Print [energyDtN[BndSca], OnRegion BndSca, Format Table, StoreInVariable $energyDtNVar];
      Print [errorDtN2[BndSca], OnRegion BndSca, Format Table, StoreInVariable $errorDtN2Var];
      Print [normDtN,           OnRegion BndSca, Format Table, SendToServer "Output/4L2-DtN-Norm", File > tmp7];
      Print [errorDtNAbs,       OnRegion BndSca, Format Table, SendToServer "Output/5L2-DtN-Error (absolute)", File > tmp8];
      Print [errorDtNRel,       OnRegion BndSca, Format Table, SendToServer "Output/6L2-DtN-Error (relative)", File > tmp9];
    }
  }
}

DefineConstant[
  R_ = {"NumSol", Name "GetDP/1ResolutionChoices",    Visible 1, Choices {"NumSol", "ProjSol"}},
  P_ = {"NumSol", Name "GetDP/2PostOperationChoices", Visible 1, Choices {"NumSol", "Errors", "ProjError", "DtNError"}}
];
