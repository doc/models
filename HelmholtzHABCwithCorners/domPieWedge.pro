Include "domPieWedge.dat";

//==================================================================================================
// OPTIONS and PARAMETERS
//==================================================================================================

BND_Neumann       = 0;
BND_Sommerfeld    = 1;
BND_Second        = 2;
BND_PadeCont      = 3;
BND_PadeDisc      = 4;
CRN_Nothing       = 0;
CRN_Damping       = 1;
CRN_DampingNum    = 2;
CRN_Compatibility = 3;
CRN_Sommerfeld    = 4;
CRN_DampingNum2   = 5;

DefineConstant[
  BND_TYPE = {BND_PadeCont,
    Name "Input/5Model/03Boundary condition (edges)", Highlight "Blue",
    Choices {BND_Neumann    = "Homogeneous Neumann",
             BND_Sommerfeld = "Sommerfeld ABC",
             BND_Second     = "Second-order ABC",
             BND_PadeDisc   = "Pade ABC (disc at corners)",
             BND_PadeCont   = "Pade ABC (cont at corners)"}},
  CRN_TYPE = {CRN_Nothing,
    Name "Input/5Model/04Boundary condition (corners)", Highlight "Red",
    Visible ((BND_TYPE == BND_Second) || (BND_TYPE == BND_PadeDisc) || (BND_TYPE == BND_PadeCont)),
    Choices {CRN_Nothing       = "Nothing",
             CRN_Damping       = "Damping with keps",
             CRN_DampingNum    = "Damping with keps num",
             CRN_Compatibility = "Compatibility",
             CRN_Sommerfeld    = "Sommerfeld ABC for Compatibility"}}
];

DefineConstant[
  nPade = {4, Min 0, Step 1, Max 6,
    Name "Input/5Model/05Pade: Number of fields",
    Visible ((BND_TYPE == BND_PadeDisc) || (BND_TYPE == BND_PadeCont))},
  thetaPadeInput = {3, Min 0, Step 1, Max 4,
    Name "Input/5Model/06Pade: Rotation of branch cut",
    Visible ((BND_TYPE == BND_PadeDisc) || (BND_TYPE == BND_PadeCont))}
];

If((BND_TYPE == BND_PadeDisc) || (BND_TYPE == BND_PadeCont))
  If(thetaPadeInput == 0)
    thetaPade = 0;
  EndIf
  If(thetaPadeInput == 1)
    thetaPade = Pi/8;
  EndIf
  If(thetaPadeInput == 2)
    thetaPade = Pi/4;
  EndIf
  If(thetaPadeInput == 3)
    thetaPade = Pi/3;
  EndIf
  If(thetaPadeInput == 4)
    thetaPade = Pi/2;
  EndIf
  mPade = 2*nPade+1;
  For j In{1:nPade}
    cPade~{j} = Tan[j*Pi/mPade]^2;
  EndFor
Else
  nPade = 0;
  thetaPadeInput = 0;
EndIf

Group {
  Dom    = Region[{DOM}];
  BndSca = Region[{BND_Scatt}];

If(BND_TYPE == BND_PadeDisc)
  Edg~{1} = Region[{BND~{1}}];
  Edg~{2} = Region[{BND~{2}}];
  Edg~{3} = Region[{BND~{3}}];
  EdgClo~{1} = Region[{BND~{1},CRN~{1},CRN~{2}}];
  EdgClo~{2} = Region[{BND~{2},CRN~{2},CRN~{3}}];
  EdgClo~{3} = Region[{BND~{3},CRN~{3},CRN~{1}}];
  maxEdg = 3;
  maxCrn = 3;
Else
  Edg~{1} = Region[{BND~{1}}];
  Edg~{2} = Region[{BND~{2},BND~{3}}];
  EdgClo~{1} = Region[{BND~{1},CRN~{1},CRN~{2}}];
  EdgClo~{2} = Region[{BND~{2},BND~{3},CRN~{2},CRN~{1}}];
  maxEdg = 2;
  maxCrn = 2;
EndIf
  Crn~{1} = Region[{CRN~{1}}];
  Crn~{2} = Region[{CRN~{2}}];
  Crn~{3} = Region[{CRN~{3}}];

  CrnAll = Region[{CRN~{1},CRN~{2},CRN~{3}}];
  EdgAll = Region[{BND~{1},BND~{2},BND~{3}}];
  DomAll = Region[{Dom,BndSca,EdgAll,CrnAll}];

If(FLAG_REF == REF_Ana)
  CurvAll   = Region[{EdgAll}];
  DomRef    = Region[{DOM}];
  DomRefAll = Region[{DomRef,BndSca,EdgAll}];
Else
  BndExt    = Region[{BND_EXT}];
  BndExtAll = Region[{BND~{1},BND_EXT}];
  CurvAll   = Region[{EdgAll,BndExt}];
  DomRef    = Region[{DOM,DOM_EXT}];
  DomRefAll = Region[{DomRef,BndSca,BndExtAll}];
EndIf
}

Function {
  RadiusDom[] = XYZ[] + Vector[X_SCA,Y_SCA,0.];
  NormalGeo[Region[{BND~{1}}]] = RadiusDom[] / Norm[RadiusDom[]];
  NormalGeo[Region[{BND~{2}}]] = Vector[Cos[alphaDom],-Sin[alphaDom],0.];
  NormalGeo[Region[{BND~{3}}]] = Vector[-1.,0.,0.];
If(FLAG_REF == REF_Num)
  NormalGeo[Region[{BND_EXT}]] = RadiusDom[] / Norm[RadiusDom[]];
EndIf
  NormalGeo[BndSca] = XYZ[] / Norm[XYZ[]];

  NumNormal[] = VectorField[XYZ[]]{1001};
  NumCurv[]   = ScalarField[XYZ[]]{1002};

  CurvGeo[Edg~{1}] = 1./dimL;
  CurvGeo[Edg~{2}] = 0;
If(BND_TYPE == BND_PadeDisc)
  CurvGeo[Edg~{3}] = 0;
EndIf
If(FLAG_REF == REF_Num)
  CurvGeo[BndExt] = 1./dimL;
EndIf

  DistCorner[] = Sqrt[(X[]-X~{3})^2 + (Y[]-Y~{3})^2];
If(CRN_TYPE == CRN_Damping)
  //CurvCorner = (2)^(1/2)/LC;
  //CurvCorner = 2.*Cos[alphaDom/2.]/LC;
  CurvCorner = 1/(Tan[alphaDom/2.]*LC);
  Curv[CurvAll]    = (DistCorner[] < LC) ? CurvCorner : CurvGeo[] ;
  CurvEps[CurvAll] = (DistCorner[] < LC) ? CurvCorner : CurvGeo[] ;
ElseIf(CRN_TYPE == CRN_DampingNum)
  Curv[CurvAll]    = (DistCorner[] < 10*LC) ? NumCurv[] : CurvGeo[] ;
  CurvEps[CurvAll] = (DistCorner[] < 10*LC) ? NumCurv[] : CurvGeo[] ;
ElseIf(CRN_TYPE == CRN_DampingNum2)
  Curv[CurvAll]    = CurvGeo[];
  CurvEps[CurvAll] = (DistCorner[] < 10*LC) ? NumCurv[] : CurvGeo[] ;
Else
  Curv[CurvAll]    = CurvGeo[];
  CurvEps[CurvAll] = CurvGeo[];
EndIf

  kEps[CurvAll] = WAVENUMBER + I[] * 0.39 * WAVENUMBER^(1/3) * (CurvEps[])^(2/3);

  alphaBT[CurvAll] = Curv[]/2 + (Curv[])^2 * 1/(8.*(I[]*k[] - Curv[]));
  betaBT[CurvAll]  = - 1/(2.*(I[]*k[] - Curv[]));

  alphaBTPade[CurvAll] = Curv[]/2 + (Curv[])^2 * 1/(8.*(I[]*k[] - Curv[]));
  betaBTPade[CurvAll]  = - Curv[]/(2*k[]*k[]);

If((BND_TYPE == BND_PadeDisc) || (BND_TYPE == BND_PadeCont))
  ExpPTheta[]  = Complex[Cos[ thetaPade],Sin[ thetaPade]];
  ExpMTheta[]  = Complex[Cos[-thetaPade],Sin[-thetaPade]];
  ExpPTheta2[] = Complex[Cos[thetaPade/2.],Sin[thetaPade/2.]];
EndIf
}

//==================================================================================================
// FONCTION SPACES with CONSTRAINTS
//==================================================================================================

If(FLAG_SIGNAL_BC == SIGNAL_Dirichlet)
Constraint {
  { Name DirichletBC; Case {{ Region BndSca; Value -f_inc[]; }}}
}
EndIf

FunctionSpace {
  { Name H_nx;   Type Form0; BasisFunction {{ Name si; NameOfCoef pi; Function BF_Node; Support Region[{CurvAll}]; Entity NodesOf[All]; }}}
  { Name H_ny;   Type Form0; BasisFunction {{ Name si; NameOfCoef pi; Function BF_Node; Support Region[{CurvAll}]; Entity NodesOf[All]; }}}
  { Name H_cur;  Type Form0; BasisFunction {{ Name si; NameOfCoef pi; Function BF_Node; Support Region[{CurvAll}]; Entity NodesOf[All]; }}}
  { Name H_proj; Type Form0; BasisFunction {{ Name si; NameOfCoef pi; Function BF_Node; Support Region[{Dom}];     Entity NodesOf[All]; }}}
If(FLAG_REF == REF_Num)
  { Name H_ref; Type Form0; BasisFunction {{ Name si; NameOfCoef pi; Function BF_Node; Support Region[{DomRefAll}]; Entity NodesOf[All]; }}
If(FLAG_SIGNAL_BC == SIGNAL_Dirichlet)
    Constraint {{ NameOfCoef pi; EntityType NodesOf; NameOfConstraint DirichletBC; }}
EndIf
  }
  For i In {1:nPade}
  { Name H_ref~{i}; Type Form0;BasisFunction {{ Name sn; NameOfCoef un; Function BF_Node; Support Region[{BndExtAll}]; Entity NodesOf[All]; }}}
  EndFor
EndIf
  { Name H_num; Type Form0; BasisFunction {{ Name si; NameOfCoef pi; Function BF_Node; Support Region[{DomAll}]; Entity NodesOf[All]; }}
If(FLAG_SIGNAL_BC == SIGNAL_Dirichlet)
    Constraint {{ NameOfCoef pi; EntityType NodesOf; NameOfConstraint DirichletBC; }}
EndIf
  }
If((BND_TYPE == BND_PadeDisc) || (BND_TYPE == BND_PadeCont))
  For iEdg In{1:maxEdg}
  For i In {1:nPade}
    { Name H~{iEdg}~{i}; Type Form0; BasisFunction {{ Name sn; NameOfCoef un; Function BF_Node; Support Region[{EdgClo~{iEdg}}]; Entity NodesOf[All]; }}}
  EndFor
  EndFor
  For iCrn In{1:maxCrn}
  For i In {1:nPade}
  For j In {1:nPade}
    { Name H~{iCrn}~{i}~{j}; Type Form0; BasisFunction {{ Name sn; NameOfCoef un; Function BF_Node; Support Region[{Crn~{iCrn}}]; Entity NodesOf[All]; }}}
  EndFor
  EndFor
  EndFor
EndIf
}

//==================================================================================================
// FORMULATIONS
//==================================================================================================

Formulation {
  { Name NumNormal; Type FemEquation;
    Quantity {
      { Name u_nx; Type Local; NameOfSpace H_nx; }
      { Name u_ny; Type Local; NameOfSpace H_ny; }
    }
    Equation {
      Galerkin{ [ Dof{u_nx}           , {u_nx} ]; In Region[{CurvAll}]; Jacobian JSur; Integration I1; }
      Galerkin{ [ Dof{u_ny}           , {u_ny} ]; In Region[{CurvAll}]; Jacobian JSur; Integration I1; }
      Galerkin{ [ -CompX[NormalGeo[]] , {u_nx} ]; In Region[{CurvAll}]; Jacobian JSur; Integration I1; }
      Galerkin{ [ -CompY[NormalGeo[]] , {u_ny} ]; In Region[{CurvAll}]; Jacobian JSur; Integration I1; }
    }
  }

  { Name NumCur; Type FemEquation;
    Quantity {
      { Name u_nx;  Type Local; NameOfSpace H_nx; }
      { Name u_ny;  Type Local; NameOfSpace H_ny; }
      { Name u_cur; Type Local; NameOfSpace H_cur; }
    }
    Equation {
      Galerkin{ [ Dof{u_nx}           , {u_nx} ]; In Region[{CurvAll}]; Jacobian JSur; Integration I1; }
      Galerkin{ [ Dof{u_ny}           , {u_ny} ]; In Region[{CurvAll}]; Jacobian JSur; Integration I1; }
      Galerkin{ [ -CompX[NumNormal[]] , {u_nx} ]; In Region[{CurvAll}]; Jacobian JSur; Integration I1; }
      Galerkin{ [ -CompY[NumNormal[]] , {u_ny} ]; In Region[{CurvAll}]; Jacobian JSur; Integration I1; }

      Galerkin{ [ Dof{u_cur}                   , {u_cur} ]; In Region[{CurvAll}]; Jacobian JSur; Integration I1; }
      Galerkin{ [ -Vector[1,0,0] * Dof{d u_nx} , {u_cur} ]; In Region[{CurvAll}]; Jacobian JSur; Integration I1; }
      Galerkin{ [ -Vector[0,1,0] * Dof{d u_ny} , {u_cur} ]; In Region[{CurvAll}]; Jacobian JSur; Integration I1; }
//      Galerkin{ [ -CompX[(Vector[0,0,1] /\ NormalGeo[])] * (Vector[0,0,1] /\ NormalGeo[]) * Dof{d u_nx} , {u_cur} ]; In Region[{CurvAll}]; Jacobian JSur; Integration I1; }
//      Galerkin{ [ -CompY[(Vector[0,0,1] /\ NormalGeo[])] * (Vector[0,0,1] /\ NormalGeo[]) * Dof{d u_ny} , {u_cur} ]; In Region[{CurvAll}]; Jacobian JSur; Integration I1; }
    }
  }

If(FLAG_REF == REF_Num)
  { Name NumRef; Type FemEquation;
    Quantity {
      { Name u_ref; Type Local; NameOfSpace H_ref; }
      For i In {1:nPade}
        { Name u_ref~{i}; Type Local; NameOfSpace H_ref~{i}; }
      EndFor
    }
    Equation {

      Galerkin{ [ Dof{d u_ref}    , {d u_ref} ]; In DomRef; Jacobian JVol; Integration I1; }
      Galerkin{ [ -k[]^2*Dof{u_ref} , {u_ref} ]; In DomRef; Jacobian JVol; Integration I1; }
If(FLAG_SIGNAL_BC == SIGNAL_Neumann)
      Galerkin{ [ -df_inc[]         , {u_ref} ]; In BndSca; Jacobian JSur; Integration I1; }
EndIf

      Galerkin { [ alphaBTPade[] * Dof{u_ref}         , {u_ref} ]; In BndExtAll; Jacobian JSur; Integration I1; }
      Galerkin { [ betaBTPade[] * Dof{d u_ref}      , {d u_ref} ]; In BndExtAll; Jacobian JSur; Integration I1; }
      Galerkin { [ -I[]*k[]*ExpPTheta2[] * Dof{u_ref} , {u_ref} ]; In BndExtAll; Jacobian JSur; Integration I1; }
    For i In{1:nPade}
      Galerkin { [ -I[]*k[]*ExpPTheta2[] * 2./mPade * cPade~{i} * Dof{u_ref~{i}} , {u_ref} ]; In BndExtAll; Jacobian JSur; Integration I1; }
      Galerkin { [ -I[]*k[]*ExpPTheta2[] * 2./mPade * cPade~{i} * Dof{u_ref}     , {u_ref} ]; In BndExtAll; Jacobian JSur; Integration I1; }

      Galerkin { [ Dof{d u_ref~{i}}                                   , {d u_ref~{i}} ]; In BndExtAll; Jacobian JSur; Integration I1; }
      Galerkin { [ -kEps[]^2*(ExpPTheta[]*cPade~{i}+1) * Dof{u_ref~{i}} , {u_ref~{i}} ]; In BndExtAll; Jacobian JSur; Integration I1; }
      Galerkin { [ -kEps[]^2*ExpPTheta[]*(cPade~{i}+1) * Dof{u_ref}     , {u_ref~{i}} ]; In BndExtAll; Jacobian JSur; Integration I1; }
    EndFor

    }
  }
EndIf

  { Name NumSol; Type FemEquation;
    Quantity {
If(FLAG_REF == REF_Num)
      { Name u_ref; Type Local; NameOfSpace H_ref; }
EndIf
      { Name u_num; Type Local; NameOfSpace H_num; }
If((BND_TYPE == BND_PadeDisc) || (BND_TYPE == BND_PadeCont))
      For iEdg In{1:maxEdg}
      For i In {1:nPade}
        { Name u~{iEdg}~{i}; Type Local; NameOfSpace H~{iEdg}~{i}; }
      EndFor
      EndFor
      For iCrn In{1:maxCrn}
      For i In {1:nPade}
      For j In {1:nPade}
        { Name u~{iCrn}~{i}~{j}; Type Local; NameOfSpace H~{iCrn}~{i}~{j}; }
      EndFor
      EndFor
      EndFor
EndIf
    }
    Equation {

// Helmholtz
      Galerkin{ [ Dof{d u_num}    , {d u_num} ]; In Dom; Jacobian JVol; Integration I1; }
      Galerkin{ [ -k[]^2*Dof{u_num} , {u_num} ]; In Dom; Jacobian JVol; Integration I1; }
If(FLAG_SIGNAL_BC == SIGNAL_Neumann)
      Galerkin{ [ -df_inc[]         , {u_num} ]; In BndSca; Jacobian JSur; Integration I1; }
EndIf

// Sommerfeld ABC
    If(BND_TYPE == BND_Sommerfeld)
      Galerkin{ [ -I[]*k[]*Dof{u_num} , {u_num} ]; In EdgAll; Jacobian JSur; Integration I1; }
    EndIf

// Second-order ABC
    If(BND_TYPE == BND_Second)
      Galerkin { [ - I[]*k[] * Dof{u_num}    , {u_num} ]; In EdgAll;  Jacobian JSur; Integration I1; }
      Galerkin { [ alphaBT[] * Dof{u_num}    , {u_num} ]; In EdgAll;  Jacobian JSur; Integration I1; }
      Galerkin { [ betaBT[] * Dof{d u_num} , {d u_num} ]; In EdgAll;  Jacobian JSur; Integration I1; }
    If(CRN_TYPE == CRN_Compatibility)
      Galerkin { [ 3./4. * Dof{u_num} , {u_num} ]; In CrnAll; Jacobian JLin; Integration I1; }
    EndIf
    EndIf

// HABC (auxiliary fields continuous/discontinuous at the corners)
If((BND_TYPE == BND_PadeDisc) || (BND_TYPE == BND_PadeCont))
    For iEdg In{1:maxEdg}
      Galerkin { [ alphaBTPade[] * Dof{u_num}         , {u_num} ]; In Edg~{iEdg}; Jacobian JSur; Integration I1; }
      Galerkin { [ betaBTPade[] * Dof{d u_num}      , {d u_num} ]; In Edg~{iEdg}; Jacobian JSur; Integration I1; }
      Galerkin { [ -I[]*k[]*ExpPTheta2[] * Dof{u_num} , {u_num} ]; In Edg~{iEdg}; Jacobian JSur; Integration I1; }
    For i In{1:nPade}
      Galerkin { [ -I[]*k[]*ExpPTheta2[] * 2./mPade * cPade~{i} * Dof{u~{iEdg}~{i}} , {u_num} ]; In Edg~{iEdg}; Jacobian JSur; Integration I1; }
      Galerkin { [ -I[]*k[]*ExpPTheta2[] * 2./mPade * cPade~{i} * Dof{u_num}        , {u_num} ]; In Edg~{iEdg}; Jacobian JSur; Integration I1; }

      Galerkin { [ Dof{d u~{iEdg}~{i}}                                   , {d u~{iEdg}~{i}} ]; In Edg~{iEdg}; Jacobian JSur; Integration I1; }
      Galerkin { [ -kEps[]^2*(ExpPTheta[]*cPade~{i}+1) * Dof{u~{iEdg}~{i}} , {u~{iEdg}~{i}} ]; In Edg~{iEdg}; Jacobian JSur; Integration I1; }
      Galerkin { [ -kEps[]^2*ExpPTheta[]*(cPade~{i}+1) * Dof{u_num}        , {u~{iEdg}~{i}} ]; In Edg~{iEdg}; Jacobian JSur; Integration I1; }
    EndFor
    EndFor

    For iCrn In{1:maxCrn}
      iEdg1 = (iCrn == 1) ? maxCrn : iCrn-1;
      iEdg2 = iCrn;
    If((iCrn < 3) || (CRN_TYPE == CRN_Compatibility))
    For i In{1:nPade}
      Galerkin { [ -I[]*k[]*ExpPTheta2[] * Dof{u~{iEdg1}~{i}} , {u~{iEdg1}~{i}} ]; In Crn~{iCrn}; Jacobian JLin; Integration I1; }
      Galerkin { [ -I[]*k[]*ExpPTheta2[] * Dof{u~{iEdg2}~{i}} , {u~{iEdg2}~{i}} ]; In Crn~{iCrn}; Jacobian JLin; Integration I1; }

    For j In{1:nPade}
      Galerkin { [ -I[]*k[]*ExpPTheta2[] * 2./mPade * cPade~{j} * Dof{u~{iEdg1}~{i}}    , {u~{iEdg1}~{i}} ]; In Crn~{iCrn}; Jacobian JLin; Integration I1; }
      Galerkin { [ -I[]*k[]*ExpPTheta2[] * 2./mPade * cPade~{j} * Dof{u~{iEdg2}~{i}}    , {u~{iEdg2}~{i}} ]; In Crn~{iCrn}; Jacobian JLin; Integration I1; }
      Galerkin { [ -I[]*k[]*ExpPTheta2[] * 2./mPade * cPade~{j} * Dof{u~{iCrn}~{i}~{j}} , {u~{iEdg1}~{i}} ]; In Crn~{iCrn}; Jacobian JLin; Integration I1; }
      Galerkin { [ -I[]*k[]*ExpPTheta2[] * 2./mPade * cPade~{j} * Dof{u~{iCrn}~{j}~{i}} , {u~{iEdg2}~{i}} ]; In Crn~{iCrn}; Jacobian JLin; Integration I1; }

      Galerkin { [ (cPade~{i}+cPade~{j}+ExpMTheta[]) * Dof{u~{iCrn}~{i}~{j}} , {u~{iCrn}~{i}~{j}} ]; In Crn~{iCrn}; Jacobian JLin; Integration I1; }
      Galerkin { [ (cPade~{j}+1) * Dof{u~{iEdg1}~{i}}                        , {u~{iCrn}~{i}~{j}} ]; In Crn~{iCrn}; Jacobian JLin; Integration I1; }
      Galerkin { [ (cPade~{i}+1) * Dof{u~{iEdg2}~{j}}                        , {u~{iCrn}~{i}~{j}} ]; In Crn~{iCrn}; Jacobian JLin; Integration I1; }
    EndFor
    EndFor
    EndIf
    If((iCrn == 3) && (CRN_TYPE == CRN_Sommerfeld))
    For i In{1:nPade}
      Galerkin { [ -I[]*k[] * Dof{u~{iEdg1}~{i}} , {u~{iEdg1}~{i}} ]; In Crn~{iCrn}; Jacobian JLin; Integration I1; } // *ExpPTheta2[]
      Galerkin { [ -I[]*k[] * Dof{u~{iEdg2}~{i}} , {u~{iEdg2}~{i}} ]; In Crn~{iCrn}; Jacobian JLin; Integration I1; } // *ExpPTheta2[]
    EndFor
    EndIf
    EndFor

EndIf
    }
  }

  { Name ProjSol; Type FemEquation;
    Quantity {
      { Name u_proj; Type Local; NameOfSpace H_proj; }
    }
    Equation {
      Galerkin{ [ Dof{u_proj} , {u_proj} ]; In Dom; Jacobian JVol; Integration I1; }
      Galerkin{ [ -f_ref[]    , {u_proj} ]; In Dom; Jacobian JVol; Integration I1; }
    }
  }
}

//==================================================================================================
// RESOLUTION
//==================================================================================================

Resolution {
  { Name NumNormal;
    System {
      { Name A; NameOfFormulation NumNormal; Type Real; NameOfMesh "mainCurv.msh"; }
    }
    Operation {
      Generate[A]; Solve[A]; SaveSolution[A];
    }
  }
  { Name NumCur;
    System {
      { Name A; NameOfFormulation NumNormal; Type Real; NameOfMesh "mainCurv.msh";  }
      { Name B; NameOfFormulation NumCur;    Type Real; NameOfMesh "mainCurv.msh";  }
    }
    Operation {
      Generate[A]; Solve[A]; SaveSolution[A]; PostOperation[NumNormal];
      Generate[B]; Solve[B]; SaveSolution[B];
    }
  }
  { Name NumSol;
    System {
If((CRN_TYPE == CRN_DampingNum) || (CRN_TYPE == CRN_DampingNum2))
      { Name A; NameOfFormulation NumNormal; Type Real; NameOfMesh "mainCurv.msh"; }
      { Name B; NameOfFormulation NumCur;    Type Real; NameOfMesh "mainCurv.msh"; }
EndIf
If(FLAG_REF == REF_Num)
      { Name C; NameOfFormulation NumRef; Type Complex; }
EndIf
      { Name D; NameOfFormulation NumSol; Type Complex; }
    }
    Operation {
If((CRN_TYPE == CRN_DampingNum) || (CRN_TYPE == CRN_DampingNum2))
      Generate[A]; Solve[A]; SaveSolution[A]; PostOperation[NumNormal];
      Generate[B]; Solve[B]; SaveSolution[B]; PostOperation[NumCur];
EndIf
If(FLAG_REF == REF_Num)
      Generate[C]; Solve[C]; SaveSolution[C];
EndIf
      Generate[D]; Solve[D]; SaveSolution[D];
    }
  }
  { Name ProjSol;
    System {{ Name A; NameOfFormulation ProjSol; Type Complex; }}
    Operation { Generate[A]; Solve[A]; SaveSolution[A]; }
  }
}

//==================================================================================================
// POSTPRO / POSTOP
//==================================================================================================

PostProcessing {
  { Name NumNormal; NameOfFormulation NumNormal;
    Quantity {
      { Name u_normal; Value { Local { [ Vector[{u_nx},{u_ny},0] / Sqrt[{u_nx}*{u_nx}+{u_ny}*{u_ny}] ]; In Region[{CurvAll}]; Jacobian JSur; }}}
    }
  }
  { Name NumCur; NameOfFormulation NumCur;
    Quantity {
      { Name u_cur; Value { Local { [ {u_cur} ]; In Region[{CurvAll}]; Jacobian JSur; }}}
    }
  }
  { Name NumSol; NameOfFormulation NumSol;
    Quantity {
If(FLAG_REF == REF_Ana)
      { Name u_refFull~{FLAG_SIGNAL_BC}~{BND_TYPE}~{CRN_TYPE}~{nPade}~{thetaPadeInput}~{alphaDomSave}; Value { Local { [ f_ref[] ];         In DomRef; Jacobian JVol; }}}
      { Name u_ref~{FLAG_SIGNAL_BC}~{BND_TYPE}~{CRN_TYPE}~{nPade}~{thetaPadeInput}~{alphaDomSave};     Value { Local { [ f_ref[] ];         In Dom; Jacobian JVol; }}}
      { Name u_num~{FLAG_SIGNAL_BC}~{BND_TYPE}~{CRN_TYPE}~{nPade}~{thetaPadeInput}~{alphaDomSave};     Value { Local { [ {u_num} ];         In Dom; Jacobian JVol; }}}
      { Name u_err~{FLAG_SIGNAL_BC}~{BND_TYPE}~{CRN_TYPE}~{nPade}~{thetaPadeInput}~{alphaDomSave};     Value { Local { [ f_ref[]-{u_num} ]; In Dom; Jacobian JVol; }}}
ElseIf(FLAG_REF == REF_Num)
      { Name u_refFull~{FLAG_SIGNAL_BC}~{BND_TYPE}~{CRN_TYPE}~{nPade}~{thetaPadeInput}~{alphaDomSave}; Value { Local { [ {u_ref} ];         In DomRef; Jacobian JVol; }}}
      { Name u_ref~{FLAG_SIGNAL_BC}~{BND_TYPE}~{CRN_TYPE}~{nPade}~{thetaPadeInput}~{alphaDomSave};     Value { Local { [ {u_ref} ];         In Dom; Jacobian JVol; }}}
      { Name u_num~{FLAG_SIGNAL_BC}~{BND_TYPE}~{CRN_TYPE}~{nPade}~{thetaPadeInput}~{alphaDomSave};     Value { Local { [ {u_num} ];         In Dom; Jacobian JVol; }}}
      { Name u_err~{FLAG_SIGNAL_BC}~{BND_TYPE}~{CRN_TYPE}~{nPade}~{thetaPadeInput}~{alphaDomSave};     Value { Local { [ {u_ref}-{u_num} ]; In Dom; Jacobian JVol; }}}
EndIf
      For iEdg In{1:maxEdg}
      For i In {1:nPade}
      { Name u_numAux~{iEdg}~{i}~{FLAG_SIGNAL_BC}~{BND_TYPE}~{CRN_TYPE}~{nPade}~{thetaPadeInput}~{alphaDomSave}; Value { Local { [ {u~{iEdg}~{i}} ]; In Edg~{iEdg}; Jacobian JSur; }}}
      EndFor
      EndFor
    }
  }
  { Name Errors; NameOfFormulation NumSol;
    Quantity {
If(FLAG_REF == REF_Ana)
      { Name error2;   Value { Integral { [ Norm[f_ref[]-{u_num}]^2 ];              In Dom; Jacobian JVol; Integration I1; }}}
      { Name energy2;  Value { Integral { [ Norm[f_ref[]]^2 ];                      In Dom; Jacobian JVol; Integration I1; }}}
ElseIf(FLAG_REF == REF_Num)
      { Name error2;   Value { Integral { [ Norm[{u_ref}-{u_num}]^2 ];              In Dom; Jacobian JVol; Integration I1; }}}
      { Name energy2;  Value { Integral { [ Norm[{u_ref}]^2 ];                      In Dom; Jacobian JVol; Integration I1; }}}
EndIf
      { Name errorAbs; Value { Term { Type Global; [Sqrt[$error2Var]] ;             In Dom; Jacobian JVol; }}}
      { Name errorRel; Value { Term { Type Global; [Sqrt[$error2Var/$energy2Var]] ; In Dom; Jacobian JVol; }}}
    }
  }
  { Name ProjErrors; NameOfFormulation ProjSol;
    Quantity {
      { Name error2;   Value { Integral { [ Norm[f_ref[]-{u_proj}]^2 ];             In Dom; Jacobian JVol; Integration I1; }}}
      { Name energy2;  Value { Integral { [ Norm[f_ref[]]^2 ];                      In Dom; Jacobian JVol; Integration I1; }}}
      { Name errorAbs; Value { Term { Type Global; [Sqrt[$error2Var]] ;             In Dom; Jacobian JVol; }}}
      { Name errorRel; Value { Term { Type Global; [Sqrt[$error2Var/$energy2Var]] ; In Dom; Jacobian JVol; }}}
    }
  }
}

PostOperation{
  { Name NumNormal; NameOfPostProcessing NumNormal;
    Operation {
      tmp1 = Sprintf("out/u_normal_%g_%g.pos", alphaDomSave, LC);
      Print [u_normal, OnElementsOf Region[{BND~{2}}], File tmp1];
      Print [u_normal, OnElementsOf Region[{CurvAll}], StoreInField (1001)];
    }
  }
  { Name NumCur; NameOfPostProcessing NumCur;
    Operation {
      tmp1 = Sprintf("out/u_cur_%g_%g.pos", alphaDomSave, LC);
      Print [u_cur, OnElementsOf Region[{BND~{2}}], File tmp1];
      Print [u_cur, OnElementsOf Region[{CurvAll}], StoreInField (1002)];
    }
  }
  { Name NumSol; NameOfPostProcessing NumSol;
    Operation {
      tmp1 = Sprintf("out/solRefFull_%g_%g_%g_%g_%g_%g.pos", FLAG_SIGNAL_BC, BND_TYPE, CRN_TYPE, nPade, thetaPadeInput, alphaDomSave);
      tmp2 = Sprintf("out/solRef_%g_%g_%g_%g_%g_%g.pos", FLAG_SIGNAL_BC, BND_TYPE, CRN_TYPE, nPade, thetaPadeInput, alphaDomSave);
      tmp3 = Sprintf("out/solNum_%g_%g_%g_%g_%g_%g.pos", FLAG_SIGNAL_BC, BND_TYPE, CRN_TYPE, nPade, thetaPadeInput, alphaDomSave);
      tmp4 = Sprintf("out/solErr_%g_%g_%g_%g_%g_%g.pos", FLAG_SIGNAL_BC, BND_TYPE, CRN_TYPE, nPade, thetaPadeInput, alphaDomSave);
//      Print [u_refFull~{FLAG_SIGNAL_BC}~{BND_TYPE}~{CRN_TYPE}~{nPade}~{thetaPadeInput}~{alphaDomSave}, OnElementsOf DomRef, File tmp1];
      Print [u_ref~{FLAG_SIGNAL_BC}~{BND_TYPE}~{CRN_TYPE}~{nPade}~{thetaPadeInput}~{alphaDomSave}, OnElementsOf Dom, File tmp2];
      Print [u_num~{FLAG_SIGNAL_BC}~{BND_TYPE}~{CRN_TYPE}~{nPade}~{thetaPadeInput}~{alphaDomSave}, OnElementsOf Dom, File tmp3];
      Print [u_err~{FLAG_SIGNAL_BC}~{BND_TYPE}~{CRN_TYPE}~{nPade}~{thetaPadeInput}~{alphaDomSave}, OnElementsOf Dom, File tmp4];
//      For iEdg In{1:maxEdg}
//      For i In {1:nPade}
//      tmp5 = Sprintf("out/solNum_%g_%g_%g_%g_%g_%g_%g_%g.pos", FLAG_SIGNAL_BC, BND_TYPE, CRN_TYPE, nPade, thetaPadeInput, alphaDomSave, iEdg, i);
//      Print [u_numAux~{iEdg}~{i}~{FLAG_SIGNAL_BC}~{BND_TYPE}~{CRN_TYPE}~{nPade}~{thetaPadeInput}~{alphaDomSave}, OnElementsOf Edg~{iEdg}, File tmp5];
//      EndFor
//      EndFor
    }
  }
  { Name Errors; NameOfPostProcessing Errors;
    Operation {
      tmp5 = Sprintf("out/errorAbs_%g_%g_%g_%g_%g.dat", FLAG_SIGNAL_BC, BND_TYPE, CRN_TYPE, nPade, thetaPadeInput);
      tmp6 = Sprintf("out/errorRel_%g_%g_%g_%g_%g.dat", FLAG_SIGNAL_BC, BND_TYPE, CRN_TYPE, nPade, thetaPadeInput);
      Print [error2[Dom],  OnRegion Dom, Format Table, StoreInVariable $error2Var];
      Print [energy2[Dom], OnRegion Dom, Format Table, StoreInVariable $energy2Var];
      Print [errorAbs,     OnRegion Dom, Format Table, SendToServer "Output/1L2-Error (absolute)", File > tmp5];
      Print [errorRel,     OnRegion Dom, Format Table, SendToServer "Output/2L2-Error (relative)", File > tmp6];
    }
  }
  { Name ProjErrors; NameOfPostProcessing ProjErrors;
    Operation {
      tmp1 = Sprintf("out/errorAbs_Proj.dat");
      tmp2 = Sprintf("out/errorRel_Proj.dat");
      Print [error2[Dom],  OnRegion Dom, Format Table, StoreInVariable $error2Var];
      Print [energy2[Dom], OnRegion Dom, Format Table, StoreInVariable $energy2Var];
      Print [errorAbs,     OnRegion Dom, Format Table, SendToServer "Output/1L2-Error (absolute)", File > tmp1];
      Print [errorRel,     OnRegion Dom, Format Table, SendToServer "Output/2L2-Error (relative)", File > tmp2];
    }
  }
}

DefineConstant[
  R_ = {"NumSol", Name "GetDP/1ResolutionChoices", Visible 1, Choices {"NumNormal", "NumCur", "NumSol", "ProjSol"} },
  P_ = {"NumSol, Errors", Name "GetDP/2PostOperationChoices", Visible 1, Choices {"NumNormal", "NumCur", "NumSol", "Errors", "ProjErrors"}}
];
