Include "domPieWedge.dat";

Point(0) = {0, 0, 0};

Point(1) = {X~{1}, Y~{1}, 0};
Point(2) = {X~{2}, Y~{2}, 0};
Point(3) = {X~{3}, Y~{3}, 0};
Point(100) = {X~{100}, Y~{100}, 0};

Point(5) = {-R_SCA,     0, 0};
Point(6) = {     0,-R_SCA, 0};
Point(7) = { R_SCA,     0, 0};
Point(8) = {     0, R_SCA, 0};

Circle(0) = {2, 3, 100};
Circle(1) = {100, 3, 1};
Line(2) = {3, 2};
Line(3) = {1, 3};

Circle(5) = {5, 0, 6};
Circle(6) = {6, 0, 7};
Circle(7) = {7, 0, 8};
Circle(8) = {8, 0, 5};

Line Loop(1) = {0, 1, 2, 3, -5, -6, -7, -8};
Plane Surface(1) = {1};

If(FLAG_REF == REF_Num)
  Point(200) = {X~{200}, Y~{200}, 0};
  Circle(10) = {1, 3, 200};
  Circle(11) = {200, 3, 2};
  Line Loop(2) = {-2, -3, 10, 11};
  Plane Surface(2) = {2};
EndIf

Physical Point(CRN_1) = {1}; // Hybrid1
Physical Point(CRN_2) = {2}; // Hybrid2
Physical Point(CRN_3) = {3}; // Pade

Physical Line(BND_1) = {0, 1}; // BGT
Physical Line(BND_2) = {2};    // Pade-Left
Physical Line(BND_3) = {3};    // Pade-Down
Physical Line(BND_Scatt) = {5,6,7,8};

SetOrder 1;
Mesh.ElementOrder = 1;
Mesh 1;
Save "mainCurv.msh";
SetOrder ORDER;
Mesh.ElementOrder = ORDER;

Physical Surface(DOM) = {1};

If(FLAG_REF == REF_Num)
  Physical Line(BND_EXT) = {10, 11};
  Physical Surface(DOM_EXT) = {2};
EndIf


