Include "domPolyhedron.dat";

SetFactory("OpenCASCADE");

/// TRUNCATED POLYHEDRAL DOMAIN

X_SCA = 0.;
Y_SCA = 0.;
Z_SCA = 0.;

If(FAC_Num == 4)
  dimL = dimRext/Sqrt[3];

  P_1 = newp; Point(P_1) = { dimL-X_SCA, dimL-Y_SCA, dimL-Z_SCA};
  P_2 = newp; Point(P_2) = { dimL-X_SCA,-dimL-Y_SCA,-dimL-Z_SCA};
  P_3 = newp; Point(P_3) = {-dimL-X_SCA, dimL-Y_SCA,-dimL-Z_SCA};
  P_4 = newp; Point(P_4) = {-dimL-X_SCA,-dimL-Y_SCA, dimL-Z_SCA};

  L_1 = newl; Line(L_1) = {P_1, P_2};
  L_2 = newl; Line(L_2) = {P_1, P_3};
  L_3 = newl; Line(L_3) = {P_1, P_4};
  L_4 = newl; Line(L_4) = {P_2, P_3};
  L_5 = newl; Line(L_5) = {P_2, P_4};
  L_6 = newl; Line(L_6) = {P_3, P_4};

  LL_1 = newll; Line Loop(LL_1) = {L_1, L_2, L_4};
  LL_2 = newll; Line Loop(LL_2) = {L_3, L_1, L_5};
  LL_3 = newll; Line Loop(LL_3) = {L_2, L_3, L_6};
  LL_4 = newll; Line Loop(LL_4) = {L_5, L_4, L_6};
EndIf

If(FAC_Num == 6)
  dimL = dimRext/Sqrt[3];

  P_1 = newp; Point(P_1) = {-dimL-X_SCA,-dimL-Y_SCA,-dimL-Z_SCA};
  P_2 = newp; Point(P_2) = { dimL-X_SCA,-dimL-Y_SCA,-dimL-Z_SCA};
  P_3 = newp; Point(P_3) = { dimL-X_SCA,-dimL-Y_SCA, dimL-Z_SCA};
  P_4 = newp; Point(P_4) = {-dimL-X_SCA,-dimL-Y_SCA, dimL-Z_SCA};
  P_5 = newp; Point(P_5) = {-dimL-X_SCA, dimL-Y_SCA, dimL-Z_SCA};
  P_6 = newp; Point(P_6) = { dimL-X_SCA, dimL-Y_SCA, dimL-Z_SCA};
  P_7 = newp; Point(P_7) = {-dimL-X_SCA, dimL-Y_SCA,-dimL-Z_SCA};
  P_8 = newp; Point(P_8) = { dimL-X_SCA, dimL-Y_SCA,-dimL-Z_SCA};

  L_1 = newl; Line(L_1) = {P_1, P_2};
  L_2 = newl; Line(L_2) = {P_4, P_3};
  L_3 = newl; Line(L_3) = {P_5, P_6};
  L_4 = newl; Line(L_4) = {P_7, P_8};
  L_5 = newl; Line(L_5) = {P_1, P_7};
  L_6 = newl; Line(L_6) = {P_2, P_8};
  L_7 = newl; Line(L_7) = {P_3, P_6};
  L_8 = newl; Line(L_8) = {P_4, P_5};
  L_9 = newl; Line(L_9) = {P_1, P_4};
  L_10 = newl; Line(L_10) = {P_7, P_5};
  L_11 = newl; Line(L_11) = {P_8, P_6};
  L_12 = newl; Line(L_12) = {P_2, P_3};

  LL_1 = newll; Line Loop(LL_1) = {L_9, L_8, L_10, L_5};
  LL_2 = newll; Line Loop(LL_2) = {L_6, L_11, L_7, L_12};
  LL_3 = newll; Line Loop(LL_3) = {L_1, L_12, L_2, L_9};
  LL_4 = newll; Line Loop(LL_4) = {L_10, L_3, L_11, L_4};
  LL_5 = newll; Line Loop(LL_5) = {L_5, L_4, L_6, L_1};
  LL_6 = newll; Line Loop(LL_6) = {L_2, L_7, L_3, L_8};
EndIf

If(FAC_Num == 8)
  dimL = dimRext;

  P_1 = newp; Point(P_1) = {-dimL-X_SCA,     -Y_SCA,     -Z_SCA};
  P_2 = newp; Point(P_2) = {     -X_SCA,-dimL-Y_SCA,     -Z_SCA};
  P_3 = newp; Point(P_3) = {     -X_SCA,     -Y_SCA,-dimL-Z_SCA};
  P_4 = newp; Point(P_4) = {     -X_SCA, dimL-Y_SCA,     -Z_SCA};
  P_5 = newp; Point(P_5) = {     -X_SCA,     -Y_SCA, dimL-Z_SCA};
  P_6 = newp; Point(P_6) = { dimL-X_SCA,     -Y_SCA,     -Z_SCA};

  L_1 = newl; Line(L_1) = {P_2, P_1};
  L_2 = newl; Line(L_2) = {P_3, P_1};
  L_3 = newl; Line(L_3) = {P_4, P_1};
  L_4 = newl; Line(L_4) = {P_5, P_1};
  L_5 = newl; Line(L_5) = {P_2, P_3};
  L_6 = newl; Line(L_6) = {P_3, P_4};
  L_7 = newl; Line(L_7) = {P_4, P_5};
  L_8 = newl; Line(L_8) = {P_5, P_2};
  L_9 = newl; Line(L_9) = {P_6, P_2};
  L_10 = newl; Line(L_10) = {P_6, P_3};
  L_11 = newl; Line(L_11) = {P_6, P_4};
  L_12 = newl; Line(L_12) = {P_6, P_5};

  LL_1 = newll; Line Loop(LL_1) = {L_1, L_2, L_5};
  LL_2 = newll; Line Loop(LL_2) = {L_2, L_3, L_6};
  LL_3 = newll; Line Loop(LL_3) = {L_3, L_4, L_7};
  LL_4 = newll; Line Loop(LL_4) = {L_4, L_1, L_8};
  LL_5 = newll; Line Loop(LL_5) = {L_9, L_10, L_5};
  LL_6 = newll; Line Loop(LL_6) = {L_10, L_11, L_6};
  LL_7 = newll; Line Loop(LL_7) = {L_11, L_12, L_7};
  LL_8 = newll; Line Loop(LL_8) = {L_12, L_9, L_8};
EndIf

If(FAC_Num == 12)
  dimL = dimRext/Tan[Pi/3];
  GoRat = 1.61803398875*dimL;
  invGR = 0.61803398875*dimL;

  P_1  = newp; Point(P_1)  = { -dimL-X_SCA, -dimL-Y_SCA, -dimL-Z_SCA};
  P_2  = newp; Point(P_2)  = {-invGR-X_SCA,      -Y_SCA,-GoRat-Z_SCA};
  P_3  = newp; Point(P_3)  = { invGR-X_SCA,      -Y_SCA,-GoRat-Z_SCA};
  P_4  = newp; Point(P_4)  = {  dimL-X_SCA, -dimL-Y_SCA, -dimL-Z_SCA};
  P_5  = newp; Point(P_5)  = {      -X_SCA,-GoRat-Y_SCA,-invGR-Z_SCA};
  P_6  = newp; Point(P_6)  = {-GoRat-X_SCA,-invGR-Y_SCA,      -Z_SCA};
  P_7  = newp; Point(P_7)  = { -dimL-X_SCA,  dimL-Y_SCA, -dimL-Z_SCA};
  P_8  = newp; Point(P_8)  = {  dimL-X_SCA,  dimL-Y_SCA, -dimL-Z_SCA};
  P_9  = newp; Point(P_9)  = { GoRat-X_SCA,-invGR-Y_SCA,      -Z_SCA};
  P_10 = newp; Point(P_10) = {      -X_SCA,-GoRat-Y_SCA, invGR-Z_SCA};
  P_11 = newp; Point(P_11) = {-GoRat-X_SCA, invGR-Y_SCA,      -Z_SCA};
  P_12 = newp; Point(P_12) = {      -X_SCA, GoRat-Y_SCA,-invGR-Z_SCA};
  P_13 = newp; Point(P_13) = { GoRat-X_SCA, invGR-Y_SCA,      -Z_SCA};
  P_14 = newp; Point(P_14) = {  dimL-X_SCA, -dimL-Y_SCA,  dimL-Z_SCA};
  P_15 = newp; Point(P_15) = { -dimL-X_SCA, -dimL-Y_SCA,  dimL-Z_SCA};
  P_16 = newp; Point(P_16) = { -dimL-X_SCA,  dimL-Y_SCA,  dimL-Z_SCA};
  P_17 = newp; Point(P_17) = {      -X_SCA, GoRat-Y_SCA, invGR-Z_SCA};
  P_18 = newp; Point(P_18) = {  dimL-X_SCA,  dimL-Y_SCA,  dimL-Z_SCA};
  P_19 = newp; Point(P_19) = { invGR-X_SCA,      -Y_SCA, GoRat-Z_SCA};
  P_20 = newp; Point(P_20) = {-invGR-X_SCA,      -Y_SCA, GoRat-Z_SCA};

  L_1 = newl;  Line(L_1)  = {P_1, P_2};
  L_2 = newl;  Line(L_2)  = {P_2, P_3};
  L_3 = newl;  Line(L_3)  = {P_3, P_4};
  L_4 = newl;  Line(L_4)  = {P_4, P_5};
  L_5 = newl;  Line(L_5)  = {P_5, P_1};
  L_6 = newl;  Line(L_6)  = {P_1, P_6};
  L_7 = newl;  Line(L_7)  = {P_2, P_7};
  L_8 = newl;  Line(L_8)  = {P_3, P_8};
  L_9 = newl;  Line(L_9)  = {P_4, P_9};
  L_10 = newl; Line(L_10) = {P_5, P_10};
  L_11 = newl; Line(L_11) = {P_6, P_15};
  L_12 = newl; Line(L_12) = {P_6, P_11};
  L_13 = newl; Line(L_13) = {P_7, P_11};
  L_14 = newl; Line(L_14) = {P_7, P_12};
  L_15 = newl; Line(L_15) = {P_8, P_12};
  L_16 = newl; Line(L_16) = {P_8, P_13};
  L_17 = newl; Line(L_17) = {P_9, P_13};
  L_18 = newl; Line(L_18) = {P_9, P_14};
  L_19 = newl; Line(L_19) = {P_10, P_14};
  L_20 = newl; Line(L_20) = {P_10, P_15};
  L_21 = newl; Line(L_21) = {P_11, P_16};
  L_22 = newl; Line(L_22) = {P_12, P_17};
  L_23 = newl; Line(L_23) = {P_13, P_18};
  L_24 = newl; Line(L_24) = {P_14, P_19};
  L_25 = newl; Line(L_25) = {P_15, P_20};
  L_26 = newl; Line(L_26) = {P_17, P_16};
  L_27 = newl; Line(L_27) = {P_18, P_17};
  L_28 = newl; Line(L_28) = {P_19, P_18};
  L_29 = newl; Line(L_29) = {P_20, P_19};
  L_30 = newl; Line(L_30) = {P_16, P_20};

  LL_1  = newll; Line Loop(LL_1 ) = {L_1, L_2, L_3, L_4, L_5};
  LL_2  = newll; Line Loop(LL_2 ) = {L_6, L_1, L_7, L_12, L_13};
  LL_3  = newll; Line Loop(LL_3 ) = {L_7, L_2, L_8, L_14, L_15};
  LL_4  = newll; Line Loop(LL_4 ) = {L_8, L_3, L_9, L_16, L_17};
  LL_5  = newll; Line Loop(LL_5 ) = {L_9, L_4, L_10, L_18, L_19};
  LL_6  = newll; Line Loop(LL_6 ) = {L_10, L_5, L_6, L_20, L_11};
  LL_7  = newll; Line Loop(LL_7 ) = {L_11, L_12, L_25, L_21, L_30};
  LL_8  = newll; Line Loop(LL_8 ) = {L_13, L_14, L_21, L_22, L_26};
  LL_9  = newll; Line Loop(LL_9 ) = {L_15, L_16, L_22, L_23, L_27};
  LL_10 = newll; Line Loop(LL_10) = {L_17, L_18, L_23, L_24, L_28};
  LL_11 = newll; Line Loop(LL_11) = {L_19, L_20, L_24, L_25, L_29};
  LL_12 = newll; Line Loop(LL_12) = {L_26, L_27, L_28, L_29, L_30};
EndIf

If(FAC_Num == 20)
  dimL = dimRext/(Tan[Pi/5]*2.61803398875);
  GoRat = 1.61803398875*dimL;

  P_1  = newp; Point(P_1)  = {-GoRat-X_SCA,      -Y_SCA, -dimL-Z_SCA};
  P_2  = newp; Point(P_2)  = {      -X_SCA,  dimL-Y_SCA,-GoRat-Z_SCA};
  P_3  = newp; Point(P_3)  = {      -X_SCA, -dimL-Y_SCA,-GoRat-Z_SCA};
  P_4  = newp; Point(P_4)  = { -dimL-X_SCA,-GoRat-Y_SCA,      -Z_SCA};
  P_5  = newp; Point(P_5)  = {-GoRat-X_SCA,      -Y_SCA,  dimL-Z_SCA};
  P_6  = newp; Point(P_6)  = { -dimL-X_SCA, GoRat-Y_SCA,      -Z_SCA};
  P_7  = newp; Point(P_7)  = { GoRat-X_SCA,      -Y_SCA, -dimL-Z_SCA};
  P_8  = newp; Point(P_8)  = {  dimL-X_SCA,-GoRat-Y_SCA,      -Z_SCA};
  P_9  = newp; Point(P_9)  = {      -X_SCA, -dimL-Y_SCA, GoRat-Z_SCA};
  P_10 = newp; Point(P_10) = {      -X_SCA,  dimL-Y_SCA, GoRat-Z_SCA};
  P_11 = newp; Point(P_11) = {  dimL-X_SCA, GoRat-Y_SCA,      -Z_SCA};
  P_12 = newp; Point(P_12) = { GoRat-X_SCA,      -Y_SCA,  dimL-Z_SCA};

  L_1  = newl; Line(L_1)  = {P_1, P_2};
  L_2  = newl; Line(L_2)  = {P_1, P_3};
  L_3  = newl; Line(L_3)  = {P_1, P_4};
  L_4  = newl; Line(L_4)  = {P_1, P_5};
  L_5  = newl; Line(L_5)  = {P_1, P_6};
  L_6  = newl; Line(L_6)  = {P_2, P_3};
  L_7  = newl; Line(L_7)  = {P_3, P_4};
  L_8  = newl; Line(L_8)  = {P_4, P_5};
  L_9  = newl; Line(L_9)  = {P_5, P_6};
  L_10 = newl; Line(L_10) = {P_6, P_2};
  L_11 = newl; Line(L_11) = {P_2, P_7};
  L_12 = newl; Line(L_12) = {P_7, P_3};
  L_13 = newl; Line(L_13) = {P_3, P_8};
  L_14 = newl; Line(L_14) = {P_8, P_4};
  L_15 = newl; Line(L_15) = {P_4, P_9};
  L_16 = newl; Line(L_16) = {P_9, P_5};
  L_17 = newl; Line(L_17) = {P_5, P_10};
  L_18 = newl; Line(L_18) = {P_10, P_6};
  L_19 = newl; Line(L_19) = {P_6, P_11};
  L_20 = newl; Line(L_20) = {P_11, P_2};
  L_21 = newl; Line(L_21) = {P_7, P_8};
  L_22 = newl; Line(L_22) = {P_8, P_9};
  L_23 = newl; Line(L_23) = {P_9, P_10};
  L_24 = newl; Line(L_24) = {P_10, P_11};
  L_25 = newl; Line(L_25) = {P_11, P_7};
  L_26 = newl; Line(L_26) = {P_7, P_12};
  L_27 = newl; Line(L_27) = {P_8, P_12};
  L_28 = newl; Line(L_28) = {P_9, P_12};
  L_29 = newl; Line(L_29) = {P_10, P_12};
  L_30 = newl; Line(L_30) = {P_11, P_12};

  LL_1  = newll; Line Loop(LL_1 ) = {L_1, L_2, L_6};
  LL_2  = newll; Line Loop(LL_2 ) = {L_2, L_3, L_7};
  LL_3  = newll; Line Loop(LL_3 ) = {L_3, L_4, L_8};
  LL_4  = newll; Line Loop(LL_4 ) = {L_4, L_5, L_9};
  LL_5  = newll; Line Loop(LL_5 ) = {L_5, L_1, L_10};
  LL_6  = newll; Line Loop(LL_6 ) = {L_11, L_6, L_12};
  LL_7  = newll; Line Loop(LL_7 ) = {L_13, L_7, L_14};
  LL_8  = newll; Line Loop(LL_8 ) = {L_15, L_8, L_16};
  LL_9  = newll; Line Loop(LL_9 ) = {L_17, L_9, L_18};
  LL_10 = newll; Line Loop(LL_10) = {L_19, L_10, L_20};
  LL_11 = newll; Line Loop(LL_11) = {L_21, L_12, L_13};
  LL_12 = newll; Line Loop(LL_12) = {L_22, L_14, L_15};
  LL_13 = newll; Line Loop(LL_13) = {L_23, L_16, L_17};
  LL_14 = newll; Line Loop(LL_14) = {L_24, L_18, L_19};
  LL_15 = newll; Line Loop(LL_15) = {L_25, L_20, L_11};
  LL_16 = newll; Line Loop(LL_16) = {L_26, L_21, L_27};
  LL_17 = newll; Line Loop(LL_17) = {L_27, L_22, L_28};
  LL_18 = newll; Line Loop(LL_18) = {L_28, L_23, L_29};
  LL_19 = newll; Line Loop(LL_19) = {L_29, L_24, L_30};
  LL_20 = newll; Line Loop(LL_20) = {L_30, L_25, L_26};
EndIf

/// EXTERIOR BOUNDARY

VOL_Ext = newv;
If(FAC_Num == 1)
  Sphere(VOL_Ext) = {0.,0.,0.,dimRmean};
  Physical Surface(SUR~{1}) = { CombinedBoundary{ Volume{VOL_Ext}; }};
Else
  For i In {1:FAC_Num}
    S~{i} = news; Plane Surface(S~{i}) = {LL~{i}};
    listS[] += S~{i};
  EndFor
  SL = newsl; Surface Loop(SL) = {listS[]};
  Volume(VOL_Ext) = {SL};

  For i In {1:CRN_Num}
    Physical Point(CRN~{i}) = {P~{i}};
  EndFor
  For i In {1:EDG_Num}
    Physical Line(EDG~{i}) = {L~{i}};
  EndFor
  For i In {1:FAC_Num}
    Physical Surface(SUR~{i}) = {S~{i}};
  EndFor
EndIf

/// SCATTERER BOUNDARY

VOL_Scatt = newv;
Sphere(VOL_Scatt) = {0.,0.,0.,R_SCA};
Physical Surface(SUR_Scatt) = { CombinedBoundary{ Volume{VOL_Scatt}; }};

/// MAIN DOMAIN

VOL_Dom = newv;
BooleanDifference(VOL_Dom) = { Volume{VOL_Ext}; }{ Volume{VOL_Scatt}; };
Physical Volume(VOL) = {VOL_Dom};
Delete{ Volume{VOL_Ext,VOL_Scatt}; }

/// GENERATE MESHES

SetOrder 1;
Mesh.ElementOrder = 1;
Mesh 2;
Save "mainCurv.msh";
SetOrder ORDER;
Mesh.ElementOrder = ORDER;
