//========================================================
// Benchmark "acoustic wave produced by a loudspeaker"
// File: GetDP simulation
//========================================================

Include "loudspeaker.dat";

Group {
  Omega = Region[{DOMAIN}];
  GammaD = Region[{GAMMA_SOURCE}];
  GammaN = Region[{GAMMA_WALL}];
  GammaR = Region[{GAMMA_DOM}];
}

Function {
  I[] = Complex[0, 1];
  
  // Incident signal
  kVec[] = Vector[ k*Cos[theta], k*Sin[theta], 0];
  pInc[] = Complex[ Cos[kVec[]*XYZ[]], Sin[kVec[]*XYZ[]] ];
  
  // BC: Neumann
  fNeumann[] = 0.;
}

Constraint {
  { Name pConstraint;
    Case {{ Region GammaD; Value pInc[]; }}
  }
  { Name uConstraint;
    Case {{ Region GammaN; Value 0.; }}
  }
}

Include "formulations_scalarWaves.pro";

DefineConstant[
  C_ = {"-solve -pos -v2", Name "GetDP/9ComputeCommand", Visible 0},
  R_ = {"WaveEqn_Reso", Name "GetDP/1ResolutionChoices", Visible 1, Choices {"WaveEqn_Reso", "MembraneSys_Reso", "EulerSys_Reso"} },
  P_ = {"WaveEqn_PostOp", Name "GetDP/2PostOperationChoices", Visible 1, Choices {"WaveEqn_PostOp", "MembraneSys_PostOp", "EulerSys_PostOp"}}
];
