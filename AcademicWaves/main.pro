Include "main.dat" ;

//===========================================
// JACOBIAN & INTEGRATION
//===========================================

Jacobian {
  { Name JVol; Case {{ Region All; Jacobian Vol; }}}
  { Name JSur; Case {{ Region All; Jacobian Sur; }}}
  { Name JLin; Case {{ Region All; Jacobian Lin; }}}
}

Integration {
  { Name I1;
    Case {
      { Type Gauss;
        Case {
          { GeoElement Point;        NumberOfPoints 1; }
          { GeoElement Line;         NumberOfPoints 4; }
          { GeoElement Line2;        NumberOfPoints 6; }
          { GeoElement Triangle;     NumberOfPoints 6; }
          { GeoElement Triangle2;    NumberOfPoints 12; }
          { GeoElement Tetrahedron;  NumberOfPoints 15; }
          { GeoElement Tetrahedron2; NumberOfPoints 15; }
        }
      }
    }
  }
}

//===========================================
// LOAD SPECIFIC .PRO
//===========================================

If (FLAG_PBM!=0)
  Include Str[LinkPro] ;
EndIf

DefineConstant[
  C_ = {"-solve -pos -bin -v2", Name "GetDP/9ComputeCommand", Visible 0 }
];
