//==============================================================================
// GetDP code for simulations with SCALAR WAVES
//  * Scalar-wave equation 1D/2D/3D
//    Nodal finite-elements (Form0 for p)
//  * Scalar-wave system 1D/2D (string/membrane analogy)
//    Nodal finite-elements (Form0 for p and v)
//  * Scalar-wave system 2D/3D (Euler's equations)
//    Raviart-Thomas finite-elements (Form0 for p and Form2P/Form2 for u)
//==============================================================================

Function {
  DefineFunction[ fNeumann, alphaBT, betaBT, pmlScal, pmlTens ];
  DefineVariable[ dt, beta, gamma, theta, tf, FREQ, k ];
}

Group{
  DefineGroup[ Omega, OmegaPml, GammaD, GammaN, GammaR, GammaBT ];
  SurAll = Region[{GammaD, GammaN, GammaR, GammaBT}];
  VolAll = Region[{Omega, OmegaPml}];
  TotAll = Region[{VolAll, SurAll}];
}

//==============================================================================

FunctionSpace {
  { Name pSpace; Type Form0;
    BasisFunction {
      { Name sn; NameOfCoef un; Function BF_Node;
        Support TotAll; Entity NodesOf[All]; }
    }
    Constraint {
      { NameOfCoef un; EntityType NodesOf; NameOfConstraint pConstraint; }
    }
  }
  { Name vSpace; Type Form0;
    BasisFunction {
      { Name sn; NameOfCoef vnA; Function BF_Node;
        Support TotAll; Entity NodesOf[All]; }
    }
    Constraint {
      { NameOfCoef vnA; EntityType NodesOf; NameOfConstraint uConstraint; }
    }
  }
If (FLAG_DIM==2)
  { Name uSpace; Type Form2P;
    BasisFunction {
      { Name se; NameOfCoef he; Function BF_PerpendicularFacet;
        Support TotAll; Entity EdgesOf[All]; }
    }
    Constraint {
      { NameOfCoef he; EntityType EdgesOf; NameOfConstraint uConstraint; }
    }
  }
EndIf
If (FLAG_DIM==3)
  { Name uSpace; Type Form2;
    BasisFunction {
      { Name se; NameOfCoef he; Function BF_Facet;
        Support TotAll; Entity FacetsOf[All]; }
    }
    Constraint {
      { NameOfCoef he; EntityType FacetsOf; NameOfConstraint uConstraint; }
    }
  }
EndIf
}

//==============================================================================

Formulation {
  { Name WaveEqn_Form; Type FemEquation;
    Quantity { 
      { Name p; Type Local; NameOfSpace pSpace; }
    }
    Equation {
      Galerkin { DtDtDof [ Dof{p} , {p} ];
                 In Omega; Jacobian JVol; Integration I1; }
      Galerkin { [ Dof{d p} , {d p} ];
                 In Omega; Jacobian JVol; Integration I1; }
      
      // BC: Neumann
      Galerkin { [ -fNeumann[] , {p} ];
                 In GammaN; Jacobian JSur; Integration I1; }
      
      // BC: Radiation
      Galerkin { DtDof [ Dof{p} , {p} ];
                 In GammaR; Jacobian JSur; Integration I1; }
      
      // BC: Bayliss-Turkel
      Galerkin { DtDof [ Dof{p} , {p} ];
                 In GammaBT; Jacobian JSur; Integration I1; }
      Galerkin { [ alphaBT[] * Dof{p} , {p} ];
                 In GammaBT; Jacobian JSur; Integration I1; }
      Galerkin { [ betaBT[] * Dof{d p} , {d p} ];
                 In GammaBT; Jacobian JSur; Integration I1; }
      
      // PML (only for resolutions in the frequency domain)
      Galerkin { [ pmlTens[] * Dof{d p} , {d p} ];
                 In OmegaPml; Jacobian JVol; Integration I1; }
      Galerkin { [ -k^2 * (1/pmlScal[]) * Dof{p} , {p} ];
                 In OmegaPml; Jacobian JVol; Integration I1; }
    }
  }
  { Name MembraneSys_Form; Type FemEquation;
    Quantity {
      { Name p; Type Local; NameOfSpace pSpace; }
      { Name v; Type Local; NameOfSpace vSpace; }
    }
    Equation {
      Galerkin { DtDof [ Dof{p} , {p} ];
                 In VolAll; Integration I1; Jacobian JVol; }
      Galerkin { [ -Dof{v} , {p} ];
                 In VolAll; Integration I1; Jacobian JVol; }
      
      Galerkin { DtDof [ Dof{v} , {v} ];
                 In VolAll; Integration I1; Jacobian JVol; }
      Galerkin { [ Dof{d p} , {d v} ];
                 In VolAll; Integration I1; Jacobian JVol; }
    }
  }
  { Name EulerSys_Form; Type FemEquation;
    Quantity {
      { Name p; Type Local; NameOfSpace pSpace; }
      { Name u; Type Local; NameOfSpace uSpace; }
    }
    Equation {
      Galerkin { DtDof [ Dof{p} , {p} ];
                 In VolAll; Integration I1; Jacobian JVol; }
      Galerkin { [ - Dof{u} , {d p} ];
                 In VolAll; Integration I1; Jacobian JVol; }
      
      Galerkin { DtDof [ Dof{u} , {u} ];
                 In VolAll; Integration I1; Jacobian JVol; }
      Galerkin { [ - Dof{p} , {d u} ];
                 In VolAll; Integration I1; Jacobian JVol; }
      
      // BC: Radiation
      Galerkin { [ Dof{p} , {p} ];
                 In GammaR; Integration I1; Jacobian JSur; }
      Galerkin { [ (Dof{u} * Normal[]) * Normal[] , {u} ];
                 In GammaR; Integration I1; Jacobian JSur; }
    }
  }
}

//==============================================================================

Resolution {
If(FLAG_RES == RES_FREQ)
  { Name WaveEqn_Reso;
    System {
      { Name A; NameOfFormulation WaveEqn_Form; Frequency FREQ; }
    }
    Operation {
      CreateDir["res/"];
      Generate[A]; Solve[A]; SaveSolution[A];
    }
  }
  { Name MembraneSys_Reso;
    System {
      { Name A; NameOfFormulation MembraneSys_Form; Frequency FREQ; }
    }
    Operation {
      CreateDir["res/"];
      Generate[A]; Solve[A]; SaveSolution[A];
    }
  }
  { Name EulerSys_Reso;
    System {
      { Name A; NameOfFormulation EulerSys_Form; Frequency FREQ; }
    }
    Operation {
      CreateDir["res/"];
      Generate[A]; Solve[A]; SaveSolution[A];
    }
  }
EndIf
If(FLAG_RES == RES_TIME)
  { Name WaveEqn_Reso;
    System {
      { Name A; NameOfFormulation WaveEqn_Form; }
    }
    Operation { 
      CreateDir["res/"];
      InitSolution[A];
      InitSolution[A];
      TimeLoopNewmark[0., tf, dt, beta, gamma] {
        Generate[A]; Solve[A]; SaveSolution[A]; }
    }
  }
  { Name MembraneSys_Reso;
    System {
      { Name A; NameOfFormulation MembraneSys_Form; }
    }
    Operation {
      CreateDir["res/"];
      InitSolution[A];
      TimeLoopTheta{
        Time0 0; DTime dt; Theta theta; TimeMax tf;
        Operation { Generate[A]; Solve[A]; SaveSolution[A]; }
      }
    }
  }
  { Name EulerSys_Reso;
    System {
      { Name A; NameOfFormulation EulerSys_Form; }
    }
    Operation {
      CreateDir["res/"];
      InitSolution[A];
      TimeLoopTheta{
        Time0 0; DTime dt; Theta theta; TimeMax tf;
        Operation { Generate[A]; Solve[A]; SaveSolution[A]; }
      }
    }
  }
EndIf
}

//==============================================================================

PostProcessing {
  { Name WaveEqn_PostPro; NameOfFormulation WaveEqn_Form; NameOfSystem A;
    Quantity {
      { Name p; Value{ Local{ [ {p} ]; In VolAll; Jacobian JVol; }}}
    }
  }
  { Name MembraneSys_PostPro; NameOfFormulation MembraneSys_Form; NameOfSystem A;
    Quantity {
      { Name p; Value{ Local{ [ {p} ]; In VolAll; Jacobian JVol; }}}
      { Name v; Value{ Local{ [ {v} ]; In VolAll; Jacobian JVol; }}}
    }
  }
  { Name EulerSys_PostPro; NameOfFormulation EulerSys_Form; NameOfSystem A;
    Quantity {
      { Name p; Value{ Local{ [ {p} ]; In VolAll; Jacobian JVol; }}}
      { Name u; Value{ Local{ [ {u} ]; In VolAll; Jacobian JVol; }}}
    }
  }
}

//==============================================================================

PostOperation {
  { Name WaveEqn_PostOp; NameOfPostProcessing WaveEqn_PostPro;
    Operation {
      Print[ p, OnElementsOf VolAll, File "res/p.pos"];
    }
  }
  { Name MembraneSys_PostOp; NameOfPostProcessing MembraneSys_PostPro;
    Operation {
      Print[ p, OnElementsOf VolAll, File "res/p.pos"];
      Print[ v, OnElementsOf VolAll, File "res/u.pos"];
    }
  }
  { Name EulerSys_PostOp; NameOfPostProcessing EulerSys_PostPro;
    Operation {
      Print[ p, OnElementsOf VolAll, File "res/p.pos"];
      Print[ u, OnElementsOf VolAll, File "res/u.pos"];
    }
  }
}
