//========================================================
// Benchmark "circular scalar waves generated by an initial pulse"
// File: GMSH geometry
//========================================================

Include "gaussInSquare.dat";

Mesh.CharacteristicLengthMax = LC;
Mesh.CharacteristicLengthFactor = 1;
Mesh.Optimize = 1;

Point(1) = {-ldom/2, -ldom/2, 0};
Point(2) = { ldom/2, -ldom/2, 0};
Point(3) = { ldom/2,  ldom/2, 0};
Point(4) = {-ldom/2,  ldom/2, 0};

Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};
Line(4) = {4, 1};

Line Loop(11) = {1, 2, 3, 4};
Plane Surface(1) = {11};

Physical Line(BOUNDARY) = {1,2,3,4};
Physical Surface(DOMAIN) = {1};
