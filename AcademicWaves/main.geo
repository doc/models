Include "main.dat" ;

ORDER = 1;
SetOrder ORDER;
Mesh.ElementOrder = ORDER;
Mesh.SecondOrderLinear = 0;

If (FLAG_PBM!=0)
  Include Str[LinkGeo] ;
EndIf
